
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblIDFCBank_Audit]')
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblIDFCBank_Audit];
   --GO
BEGIN

CREATE TABLE [dbo].tblIDFCBank_Audit
(
 transID                NUMERIC(18, 0) IDENTITY PRIMARY KEY, 
 s_Trans_Type           VARCHAR(5), 
 Id						INT,
 FirstName				VARCHAR(500),
 LastName				VARCHAR(500),
 Email					VARCHAR(500),
 PolicyStatus			BIT,
 dt_InserDate			DATETIME,
 ModifiedDate			DATETIME,
 CardLast4Digit			VARCHAR(50),
 UploadByID				VARCHAR(250),
 LastModifiedByID		VARCHAR(250)
);

END;

IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[AspNetUsers_Audit]')
          AND TYPE IN(N'U')
)

--  DROP TABLE [dbo].[AspNetUsers_Audit];
--GO
    BEGIN
        CREATE TABLE [dbo].AspNetUsers_Audit
        (transID							INT IDENTITY PRIMARY KEY, 
         s_Trans_Type						VARCHAR(5), 
         Id									NVARCHAR(128), 
         Email								NVARCHAR(256), 
         EmailConfirmed						BIT, 
         PasswordHash						NVARCHAR(MAX), 
         SecurityStamp						NVARCHAR(MAX), 
         PhoneNumber						NVARCHAR(MAX), 
         PhoneNumberConfirmed				BIT, 
         TwoFactorEnabled					BIT, 
         LockoutEndDateUtc					DATETIME, 
         LockoutEnabled						BIT, 
         AccessFailedCount					INT, 
         UserName							NVARCHAR(256), 
         DOB								DATETIME, 
         IsAdmin							BIT, 
         Name								VARCHAR(500), 
         IsActive							BIT, 
         Prefix								VARCHAR(10), 
         IsLocked							BIT, 
         DealerName							VARCHAR(500), 
         TotalUsedPlanAmount				DECIMAL(18, 2), 
         PickUpPoint						VARCHAR(500), 
         IsFunctionalityLocked				BIT, 
         CreatedDate						DATETIME, 
         LogoPath							VARCHAR(500), 
         IsBulkCertificationMenu			BIT, 
         PaymentReceivedAmount				DECIMAL(18, 2), 
         AccountNo							VARCHAR(100), 
         Ifsc_Code							VARCHAR(100), 
         BeneficiaryName					VARCHAR(100), 
         BankBranchName						VARCHAR(100), 
         GSTno								VARCHAR(100), 
         PANno								VARCHAR(100), 
         IsAgreementReceived				BIT, 
         TotalCDAmount						DECIMAL(18, 2), 
         TotalCDUsedAmount					DECIMAL(18, 2), 
         TotalCDBalanceAmount				DECIMAL(18, 2), 
         PayoutPercentage					VARCHAR(100), 
         IMDCode							VARCHAR(100), 
         TieUpCompanyID						NUMERIC(18, 0), 
         VendorType							INT, 
         IsApiUser							INT, 
         StoreName							VARCHAR(MAX), 
         MappingUserId						VARCHAR(MAX), 
         AccountHolderName					VARCHAR(250), 
         ChequeImage						NVARCHAR(MAX), 
         IsBankDetailsApproved				BIT, 
         OtherAdminId						VARCHAR(250), 
         ContactPersonName					VARCHAR(250), 
         ContactPersonNo					VARCHAR(250), 
         LastModifiedBy						VARCHAR(250), 
         LastModifiedDate					DATETIME,
		 RiskStartCount						INT,
		 BranchCode							VARCHAR(50),
		 ContactPersonCommisionAmount		DECIMAL(18, 2)
        );
END;
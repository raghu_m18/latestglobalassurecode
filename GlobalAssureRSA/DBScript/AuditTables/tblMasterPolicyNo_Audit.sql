
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblMasterPolicyNo_Audit]')
          AND TYPE IN(N'U')
)

--  DROP TABLE [dbo].[tblMasterPolicyNo_Audit];
--GO
    BEGIN
        CREATE TABLE [dbo].tblMasterPolicyNo_Audit
        (transID                 NUMERIC(18, 0) IDENTITY PRIMARY KEY, 
         s_Trans_Type            VARCHAR(5), 
         Id					     DECIMAL(18, 0),
		 ProductID			     NUMERIC(18, 0),
		 PlanID				     NUMERIC(18, 0),
		 MasterPolicyNo		     VARCHAR(200),
		 StartDate			     DATETIME,
		 EndDate			     DATETIME,
		 CreatedBy			     VARCHAR(200),
		 InsertDate			     DATETIME,
		 ModifyDate			     DATETIME
        );
END;

IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblUserPrivilegeDetail_Audit]')
          AND TYPE IN(N'U')
)

--  DROP TABLE [dbo].[AspNetUsers_Audit];
--GO
    BEGIN
        CREATE TABLE [dbo].tblUserPrivilegeDetail_Audit
        (transID             INT IDENTITY PRIMARY KEY, 
		 s_Trans_Type            VARCHAR(5), 
         PrivilegeDetailID   NUMERIC(18, 0), 
         UserID              NVARCHAR(128), 
         UserName            VARCHAR(500), 
         Email               VARCHAR(500), 
         Contact             VARCHAR(20), 
         DOB                 DATETIME, 
         Product             VARCHAR(250), 
         Payment             VARCHAR(250), 
         Plans               VARCHAR(250), 
         Make                VARCHAR(500), 
         Model               VARCHAR(250), 
         CreatedDate         DATETIME, 
         IsValid             BIT, 
         CreatedBy           NVARCHAR(128), 
         CommisionPercentage DECIMAL(18, 2), 
         LastModifyDateTime  DATETIME, 
         IsCommision         BIT, 
         Mode                VARCHAR(50), 
         LastModifiedBy      VARCHAR(250)
        );
END;
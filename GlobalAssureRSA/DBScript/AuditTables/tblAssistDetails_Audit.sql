
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblAssistDetails_Audit]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblAssistDetails_Audit];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblAssistDetails_Audit
        (
		  transID				Int Identity primary key,
		 s_Trans_Type			varchar(5),
		 id                     decimal(18,0),
         TransactionId          numeric(18, 0),
         RefNo					varchar(50),
         CreatedDate		    datetime,
         Issue					varchar(MAX),
         Remarks				varchar(MAX),
         Address				varchar(2000),
         PinCode				varchar(8),
         Landmark				varchar(2000),
         Lat					varchar(10),
         Lon					varchar(10),
         VendorId				numeric(18, 0),
         ActionId			    int,
         StatusId				int,
         IsValid				bit,
         StreetAddress			varchar(2000),
         Location				varchar(300), 
         City					varchar(200),
         State					varchar(100),
         IncidentDetailid		int,
         VehicleYear			varchar(50),
         VehicleColor			varchar(100),
         UserID					nvarchar(256),
         CreatedBy				varchar(1000),
         SummaryDetails			varchar(MAX),
         VendorMainId			numeric(18, 0),
         Remark1				varchar(250)					
        );
END;
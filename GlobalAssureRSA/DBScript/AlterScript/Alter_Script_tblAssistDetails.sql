IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'CreatedBy'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD CreatedBy VARCHAR(1000);
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'SummaryDetails'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD SummaryDetails VARCHAR(MAX);
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'VendorMainId'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD VendorMainId NUMERIC(18,0);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Remark1'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Remark1 VARCHAR(250);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'DropLocationAddress'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD DropLocationAddress VARCHAR(2000);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'DropLocationKM'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD DropLocationKM VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'AssistCompanyID'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD AssistCompanyID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'VendorAssignedDateTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD VendorAssignedDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'VenReachedIncLocDateTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD VenReachedIncLocDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'VenReachedDropLocDateTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD VenReachedDropLocDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'CustomerName'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD CustomerName VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'TieUpCompanyID'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD TieUpCompanyID NUMERIC(18, 0);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'AssistCreatedDate'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD AssistCreatedDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'AssistCreatedBy'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD AssistCreatedBy VARCHAR(1000);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'AssistOEMID'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD AssistOEMID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'IsByAPI'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD IsByAPI BIT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'AdditionalInfo'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD AdditionalInfo VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Ven_ReachedLocation'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Ven_ReachedLocation VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Ven_ReachedTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Ven_ReachedTime VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Ven_DropLocation'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Ven_DropLocation VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Ven_DropTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Ven_DropTime VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Ven_TotalRunningKMs'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Ven_TotalRunningKMs VARCHAR(25);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Vendor_Location'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Vendor_Location VARCHAR(2000);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Vendor_Lat'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Vendor_Lat VARCHAR(25);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'Vendor_Lon'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD Vendor_Lon VARCHAR(25);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'IsPlanned'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD IsPlanned BIT DEFAULT 0 NOT NULL;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'PlannedDateTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD PlannedDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'AssistCaseTypeID'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD AssistCaseTypeID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'AssistSubCategoryID'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD AssistSubCategoryID INT;
END;
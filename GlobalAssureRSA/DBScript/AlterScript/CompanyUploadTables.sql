IF EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTataAIGDetails'
          And COLUMN_NAME = 'PaymentStatus'
)
    BEGIN
        ALTER TABLE tblTataAIGDetails
        DROP COLUMN PaymentStatus
END;

IF EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblBhartiAXADetails'
          And COLUMN_NAME = 'PaymentStatus'
)
    BEGIN
        ALTER TABLE tblBhartiAXADetails
        DROP COLUMN PaymentStatus
END;

IF EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblIFFCODetails'
          And COLUMN_NAME = 'PaymentStatus'
)
    BEGIN
        ALTER TABLE tblIFFCODetails
        DROP COLUMN PaymentStatus
END;

-------------------------------------


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTataAIGDetails'
          And COLUMN_NAME = 'PolicyStatus'
)
    BEGIN
        ALTER TABLE tblTataAIGDetails
        ADD PolicyStatus BIT
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblBhartiAXADetails'
          And COLUMN_NAME = 'PolicyStatus'
)
    BEGIN
        ALTER TABLE tblBhartiAXADetails
        ADD PolicyStatus BIT
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblIFFCODetails'
          And COLUMN_NAME = 'PolicyStatus'
)
    BEGIN
        ALTER TABLE tblIFFCODetails
        ADD PolicyStatus BIT
END;
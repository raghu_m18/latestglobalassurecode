IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstTieUpCompany'
          AND COLUMN_NAME = 'IsPedalCycle'
)
    BEGIN
        ALTER TABLE tblMstTieUpCompany
        ADD IsPedalCycle BIT DEFAULT 0 NOT NULL;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstTieUpCompany'
          AND COLUMN_NAME = 'IsPCCompany'
)
    BEGIN
        ALTER TABLE tblMstTieUpCompany
        ADD IsPCCompany BIT DEFAULT 0 NOT NULL;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstTieUpCompany'
          AND COLUMN_NAME = 'IsWithoutPremium'
)
    BEGIN
        ALTER TABLE tblMstTieUpCompany
        ADD IsWithoutPremium BIT DEFAULT 0 NOT NULL;
END;
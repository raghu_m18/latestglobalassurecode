IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'LastModifiedBy'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD LastModifiedBy VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'LastModifiedDate'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD LastModifiedDate DateTime;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'RiskStartCount'
)
    BEGIN
        ALTER TABLE AspNetUsers
        Add RiskStartCount INT NOT NULL DEFAULT 0		
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'BranchCode'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD BranchCode VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'ContactPersonCommisionAmount'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD ContactPersonCommisionAmount DECIMAL(18, 2);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'CompanyID'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD CompanyID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'OEMID'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD OEMID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'IsDownloadCertificate'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD IsDownloadCertificate BIT NOT NULL DEFAULT 0;
END;
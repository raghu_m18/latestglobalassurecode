
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
          AND COLUMN_NAME = 'AccountNo'
)
    BEGIN
        ALTER TABLE tblMstVendor
        ADD AccountNo varchar(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
          AND COLUMN_NAME = 'IFSC_Code'
)
    BEGIN
        ALTER TABLE tblMstVendor
        ADD IFSC_Code Varchar(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
          AND COLUMN_NAME = 'Beneficiary_Name
'
		 
)
    BEGIN
        ALTER TABLE tblMstVendor
        ADD Beneficiary_Name Varchar(100);
		
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'BankBranchName'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add BankBranchName varchar(200);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'GSTNO'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add GSTNO varchar(20);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'ISAGREEMENTRECEIVEDWITHKYC'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add ISAGREEMENTRECEIVEDWITHKYC Bit;
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'ISCANCELLEDCHEQUERECIVED'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add ISCANCELLEDCHEQUERECIVED Bit;
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'VENDOR_CODE'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add VENDOR_CODE Varchar(100);
END;
--------------------------------------------------------------------------------

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'Vendor_Category'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add Vendor_Category Varchar(100);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'Service_Type'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add Service_Type Varchar(100);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'Base_Price'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add Base_Price decimal(18,2);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'KiloMeters'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add KiloMeters Varchar(100);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'Per_km_Price'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add Per_km_Price decimal(18,2);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstVendor'
           And COLUMN_NAME = 'Service_Time'
)
    BEGIN
        ALTER TABLE tblMstVendor
       Add Service_Time varchar(100);
END;

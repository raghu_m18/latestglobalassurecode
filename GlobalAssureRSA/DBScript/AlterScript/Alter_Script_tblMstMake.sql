
IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstMake'
          AND COLUMN_NAME = 'CompanyId'
)
    BEGIN
        ALTER TABLE tblMstMake
        ADD CompanyId NUMERIC(18,0);
END;
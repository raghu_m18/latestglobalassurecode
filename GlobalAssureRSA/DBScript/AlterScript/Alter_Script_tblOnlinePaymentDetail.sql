
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblOnlinePaymentDetail'
          AND COLUMN_NAME = 'Checksum'
)
    BEGIN
        ALTER TABLE tblOnlinePaymentDetail
        ADD Checksum NVARCHAR(MAX);
END;


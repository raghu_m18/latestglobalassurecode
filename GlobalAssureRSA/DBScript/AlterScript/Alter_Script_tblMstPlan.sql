
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstPlan'
          AND COLUMN_NAME = 'StartDate'
)
    BEGIN
        ALTER TABLE tblMstPlan
        ADD StartDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstPlan'
          AND COLUMN_NAME = 'EndDate'
)
    BEGIN
        ALTER TABLE tblMstPlan
        ADD EndDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstPlan'
          AND COLUMN_NAME = 'MinRange'
		 
)
    BEGIN
        ALTER TABLE tblMstPlan
        ADD MinRange decimal(18,2);
		
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstPlan'
           And COLUMN_NAME = 'MaxRange'
)
    BEGIN
        ALTER TABLE tblMstPlan
       Add MaxRange decimal(18,2);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstPlan'
           And COLUMN_NAME = 'PlanTypeID'
)
    BEGIN
        ALTER TABLE tblMstPlan
       Add PlanTypeID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstPlan'
           And COLUMN_NAME = 'IsOnHold'
)
    BEGIN
        ALTER TABLE tblMstPlan
       Add IsOnHold INT DEFAULT 0 NOT NULL;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstPlan'
          AND COLUMN_NAME = 'ValidityMonth'
)
    BEGIN
        ALTER TABLE tblMstPlan
        ADD ValidityMonth INT;
END;
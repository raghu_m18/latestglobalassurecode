
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEmailConfig'
          AND COLUMN_NAME = 'n_EmailLimitCount'
)
    BEGIN
        ALTER TABLE tblEmailConfig
        ADD n_EmailLimitCount INT DEFAULT 0;
END;



IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEmailConfig'
          AND COLUMN_NAME = 'n_SMSTriggerCount'
)
    BEGIN
        ALTER TABLE tblEmailConfig
        ADD n_SMSTriggerCount INT DEFAULT 0;
END;


sp_rename 'tblEmailLog.dt_Insert_tDate', 'dt_Insert_Date', 'COLUMN';

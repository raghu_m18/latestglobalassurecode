IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblCaseCharges'
          AND COLUMN_NAME = 'TotalKiloMeters'
)
    BEGIN
        ALTER TABLE tblCaseCharges
        ADD TotalKiloMeters DECIMAL(18, 2);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblCaseCharges'
          AND COLUMN_NAME = 'TotalKiloMetersCharge'
)
    BEGIN
        ALTER TABLE tblCaseCharges
        ADD TotalKiloMetersCharge DECIMAL(18, 2);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblCaseCharges'
          AND COLUMN_NAME = 'TotalAmount'
)
    BEGIN
        ALTER TABLE tblCaseCharges
        ADD TotalAmount DECIMAL(18, 2);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblCaseCharges'
          AND COLUMN_NAME = 'FinalAmount'
)
    BEGIN
        ALTER TABLE tblCaseCharges
        ADD FinalAmount DECIMAL(18, 2);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblCaseCharges'
          AND COLUMN_NAME = 'CustomerPaidAmount'
)
    BEGIN
        ALTER TABLE tblCaseCharges
        ADD CustomerPaidAmount DECIMAL(18, 2);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblCaseCharges'
          AND COLUMN_NAME = 'CustomerPaidDate'
)
    BEGIN
        ALTER TABLE tblCaseCharges
        ADD CustomerPaidDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblCaseCharges'
          AND COLUMN_NAME = 'ReferenceNo'
)
    BEGIN
        ALTER TABLE tblCaseCharges
        ADD ReferenceNo VARCHAR(250);
END;
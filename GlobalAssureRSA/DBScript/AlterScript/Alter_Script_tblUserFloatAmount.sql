
IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserFloatAmount'
          AND COLUMN_NAME = 'PaymentReceivedDate'
)
    BEGIN
        ALTER TABLE tblUserFloatAmount
        ADD PaymentReceivedDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserFloatAmount'
          AND COLUMN_NAME = 'FloatAssignDate'
)
    BEGIN
        ALTER TABLE tblUserFloatAmount
        ADD FloatAssignDate DATETIME;
END;
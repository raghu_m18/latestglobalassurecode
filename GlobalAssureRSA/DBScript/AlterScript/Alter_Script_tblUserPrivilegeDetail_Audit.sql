
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserPrivilegeDetail_Audit'
           And COLUMN_NAME = 'IsOnHold'
)
    BEGIN
        ALTER TABLE tblUserPrivilegeDetail_Audit
       Add IsOnHold INT DEFAULT 0 NOT NULL;
END;

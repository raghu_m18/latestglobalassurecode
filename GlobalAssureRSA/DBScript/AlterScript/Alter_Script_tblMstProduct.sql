
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstProduct'
          AND COLUMN_NAME = 'PolicyGeneration'
)
    BEGIN
        ALTER TABLE tblMstProduct
        ADD PolicyGeneration VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstProduct'
          AND COLUMN_NAME = 'InOtherMenu'
)
    BEGIN
        ALTER TABLE tblMstProduct
        ADD InOtherMenu BIT;
END;
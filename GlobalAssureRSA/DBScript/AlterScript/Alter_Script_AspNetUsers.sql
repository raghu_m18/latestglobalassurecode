
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'IsApiUser'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD IsApiUser INT;
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'StoreName'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD StoreName VARCHAR(MAX);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'MappingUserId'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD MappingUserId VARCHAR(MAX);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'AccountHolderName'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD AccountHolderName VARCHAR(250);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'ChequeImage'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD ChequeImage NVARCHAR(MAX);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'IsBankDetailsApproved'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD IsBankDetailsApproved BIT;
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'OtherAdminId'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD OtherAdminId VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'ContactPersonName'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD ContactPersonName VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers'
          AND COLUMN_NAME = 'ContactPersonNo'
)
    BEGIN
        ALTER TABLE AspNetUsers
        ADD ContactPersonNo VARCHAR(250);
END;
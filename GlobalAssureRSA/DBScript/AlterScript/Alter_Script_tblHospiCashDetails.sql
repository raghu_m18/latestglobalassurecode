IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblHospiCashDetails'
          AND COLUMN_NAME = 'CertificateFileName'
)
    BEGIN
        ALTER TABLE tblHospiCashDetails
        ADD CertificateFileName VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblHospiCashDetails'
          AND COLUMN_NAME = 'JSON_Request'
)
    BEGIN
        ALTER TABLE tblHospiCashDetails
        ADD JSON_Request VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblHospiCashDetails'
          AND COLUMN_NAME = 'JSON_Response'
)
    BEGIN
        ALTER TABLE tblHospiCashDetails
        ADD JSON_Response VARCHAR(MAX);
END;
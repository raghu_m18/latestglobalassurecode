
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistEmail'
          AND COLUMN_NAME = 'ContactNo'
)
    BEGIN
        ALTER TABLE tblAssistEmail
        ADD ContactNo VARCHAR(100);
END;
IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails'
          AND COLUMN_NAME = 'AssistSummary'
)
    BEGIN
        ALTER TABLE tblAssistDetails
        ADD AssistSummary VARCHAR(max);
END;
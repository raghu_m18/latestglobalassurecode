
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblIDFCBank_Audit'
          AND COLUMN_NAME = 'MobileNoLast4Digit'
)
    BEGIN
        ALTER TABLE tblIDFCBank_Audit
        ADD MobileNoLast4Digit VARCHAR(50);
END;
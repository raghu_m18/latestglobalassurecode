
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'DropLocationAddress'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD DropLocationAddress VARCHAR(2000);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'DropLocationKM'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD DropLocationKM VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'AssistCompanyID'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD AssistCompanyID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'VendorAssignedDateTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD VendorAssignedDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'VenReachedIncLocDateTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD VenReachedIncLocDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'VenReachedDropLocDateTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD VenReachedDropLocDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'CustomerName'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD CustomerName VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'TieUpCompanyID'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD TieUpCompanyID NUMERIC(18, 0);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'AssistCreatedDate'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD AssistCreatedDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'AssistCreatedBy'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD AssistCreatedBy VARCHAR(1000);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'AssistOEMID'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD AssistOEMID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'IsByAPI'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD IsByAPI BIT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'AdditionalInfo'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD AdditionalInfo VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'Ven_ReachedLocation'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD Ven_ReachedLocation VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'Ven_ReachedTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD Ven_ReachedTime VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'Ven_DropLocation'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD Ven_DropLocation VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'Ven_DropTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD Ven_DropTime VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'Ven_TotalRunningKMs'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD Ven_TotalRunningKMs VARCHAR(25);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'Vendor_Location'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD Vendor_Location VARCHAR(2000);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'Vendor_Lat'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD Vendor_Lat VARCHAR(25);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'Vendor_Lon'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD Vendor_Lon VARCHAR(25);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'IsPlanned'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD IsPlanned BIT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'PlannedDateTime'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD PlannedDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'AssistCaseTypeID'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD AssistCaseTypeID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAssistDetails_Audit'
          AND COLUMN_NAME = 'AssistSubCategoryID'
)
    BEGIN
        ALTER TABLE tblAssistDetails_Audit
        ADD AssistSubCategoryID INT;
END;
IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblVendorResponseUrl'
          AND COLUMN_NAME = 'ResponseFailedUrl'
)
    BEGIN
        ALTER TABLE tblVendorResponseUrl
        ADD ResponseFailedUrl VARCHAR(MAX);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblMstCommonType'
          AND COLUMN_NAME = 'IsBulkUpload'
)
    BEGIN
        ALTER TABLE tblMstCommonType
        ADD IsBulkUpload BIT DEFAULT 0 NOT NULL;
END;
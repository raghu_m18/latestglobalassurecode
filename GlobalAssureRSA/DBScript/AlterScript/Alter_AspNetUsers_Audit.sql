
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers_Audit'
          AND COLUMN_NAME = 'RiskStartCount'
)
    BEGIN
        ALTER TABLE AspNetUsers_Audit
        Add RiskStartCount INT;	
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers_Audit'
          AND COLUMN_NAME = 'BranchCode'
)
    BEGIN
        ALTER TABLE AspNetUsers_Audit
        ADD BranchCode VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers_Audit'
          AND COLUMN_NAME = 'ContactPersonCommisionAmount'
)
    BEGIN
        ALTER TABLE AspNetUsers_Audit
        ADD ContactPersonCommisionAmount DECIMAL(18, 2);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers_Audit'
          AND COLUMN_NAME = 'CompanyID'
)
    BEGIN
        ALTER TABLE AspNetUsers_Audit
        ADD CompanyID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers_Audit'
          AND COLUMN_NAME = 'OEMID'
)
    BEGIN
        ALTER TABLE AspNetUsers_Audit
        ADD OEMID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'AspNetUsers_Audit'
          AND COLUMN_NAME = 'IsDownloadCertificate'
)
    BEGIN
        ALTER TABLE AspNetUsers_Audit
        ADD IsDownloadCertificate BIT;
END;
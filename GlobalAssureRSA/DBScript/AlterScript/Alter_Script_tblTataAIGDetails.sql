IF EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTataAIGDetails'
		  And COLUMN_NAME = 'CancellationFlag'
)
    BEGIN
        ALTER TABLE tblTataAIGDetails
        Drop column CancellationFlag
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTataAIGDetails'
          And COLUMN_NAME = 'CancellationFlag'
)
    BEGIN
        ALTER TABLE tblTataAIGDetails
        ADD CancellationFlag VARCHAR(50)
END;
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblPedalPlanMapping'
          AND COLUMN_NAME = 'DiscountedPlanAmount'
)
    BEGIN
        ALTER TABLE tblPedalPlanMapping
        ADD DiscountedPlanAmount DECIMAL(18,2);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblPedalPlanMapping'
          AND COLUMN_NAME = 'TieUpCompanyID'
)
    BEGIN
        ALTER TABLE tblPedalPlanMapping
        ADD TieUpCompanyID NUMERIC(18,0);
END;
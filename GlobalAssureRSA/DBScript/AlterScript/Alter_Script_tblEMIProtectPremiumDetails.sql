
IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'ProductTypeID'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD ProductTypeID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'UserID'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD UserID VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'CertificateNo'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD CertificateNo VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'EngineNo'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD EngineNo VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'ChassisNo'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD ChassisNo VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'VehicleType'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD VehicleType VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'VehicleClass'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD VehicleClass VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'RegistrationNo'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD RegistrationNo VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'ProductID'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD ProductID VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'MakeID'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD MakeID VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'ModelID'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD ModelID VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Make'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Make VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Model'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Model VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Variant'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Variant VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'UploadedAmount'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD UploadedAmount VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'FirstName'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD FirstName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'MiddleName'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD MiddleName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'LastName'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD LastName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'MobileNo'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD MobileNo VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'EmailID'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD EmailID VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'CoverStartDate'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD CoverStartDate VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'CoverEndDate'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD CoverEndDate VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Address1'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Address1 VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Address2'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Address2 VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Address3'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Address3 VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Pincode'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Pincode VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Landmark'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Landmark VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'City'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD City VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'State'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD State VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'InsuredGender'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD InsuredGender VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'InsuredDOB'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD InsuredDOB VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'InsuredAge'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD InsuredAge VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'NomineeName'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD NomineeName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'NomineeGender'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD NomineeGender VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'NomineeDOB'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD NomineeDOB VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'NomineeAge'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD NomineeAge VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'NomineeRelationship'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD NomineeRelationship VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'PaymentType'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD PaymentType VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'GSTINNumber'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD GSTINNumber VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'RSALoanAmount'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD RSALoanAmount VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'EMIAmount'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD EMIAmount VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'EMILoanAmount'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD EMILoanAmount VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'EMITenure'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD EMITenure VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'EMIAccountNo'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD EMIAccountNo VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'EMIBankName'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD EMIBankName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'EMIBankBranch'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD EMIBankBranch VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'Tenure'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD Tenure VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblEMIProtectPremiumDetails'
          AND COLUMN_NAME = 'ContractNo'
)
    BEGIN
        ALTER TABLE tblEMIProtectPremiumDetails
        ADD ContractNo VARCHAR(500);
END;
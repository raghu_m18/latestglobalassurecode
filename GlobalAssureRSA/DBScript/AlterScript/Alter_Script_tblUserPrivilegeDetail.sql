IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserPrivilegeDetail'
          AND COLUMN_NAME = 'CommisionPercentage'
)
    BEGIN
        ALTER TABLE tblUserPrivilegeDetail
        ADD CommisionPercentage DECIMAL(18,2);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserPrivilegeDetail'
          AND COLUMN_NAME = 'LastModifyDateTime'
)
    BEGIN
        ALTER TABLE tblUserPrivilegeDetail
        ADD LastModifyDateTime DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserPrivilegeDetail'
          AND COLUMN_NAME = 'IsCommision'
)
    BEGIN
        ALTER TABLE tblUserPrivilegeDetail
        ADD IsCommision BIT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserPrivilegeDetail'
          AND COLUMN_NAME = 'Mode'
)
    BEGIN
        ALTER TABLE tblUserPrivilegeDetail
        ADD Mode VARCHAR(50);
END;

IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserPrivilegeDetail'
          AND COLUMN_NAME = 'LastModifiedBy'
)
    BEGIN
        ALTER TABLE tblUserPrivilegeDetail
        ADD LastModifiedBy varchar(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblUserPrivilegeDetail'
           And COLUMN_NAME = 'IsOnHold'
)
    BEGIN
        ALTER TABLE tblUserPrivilegeDetail
       Add IsOnHold INT DEFAULT 0 NOT NULL;
END;

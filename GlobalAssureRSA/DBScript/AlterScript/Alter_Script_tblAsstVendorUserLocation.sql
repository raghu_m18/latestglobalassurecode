IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblAsstVendorUserLocation'
          AND COLUMN_NAME = 'CreatedBy'
)
    BEGIN
        ALTER TABLE tblAsstVendorUserLocation
        ADD CreatedBy VARCHAR(1000);
END;
IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InsuredGenderId'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InsuredGenderId INT;
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InsuredDOB'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InsuredDOB datetime NULL;
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'NomineeName'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD NomineeName VARCHAR(250);
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'NomineeGenderId'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD NomineeGenderId INT;
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'NomineeDOB'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD NomineeDOB datetime NULL;
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'NomineeRelationshipId'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD NomineeRelationshipId INT ;
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'GSTINNumber'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD GSTINNumber  VARCHAR(50);
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'BranchCode'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD BranchCode VARCHAR(50);
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'CustomerMiddleName'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD CustomerMiddleName VARCHAR(250);
END;

IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'UploadByID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD UploadByID VARCHAR(max);
END;

--------- Pedal Fields ------------------


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Title'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Title VARCHAR(10);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'AadharFront'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD AadharFront VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'AadharBack'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD AadharBack VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Model'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Model VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Color'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Color VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'CycleCost'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD CycleCost Numeric(38);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InvoiceDate'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InvoiceDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InvoiceNumber'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InvoiceNumber VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InvoicePhoto'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InvoicePhoto VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Accessories'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Accessories VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'AccessoriesInvoice'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD AccessoriesInvoice VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'AccessoriesInvoiceDate'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD AccessoriesInvoiceDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'AccessoriesInvoiceNo'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD AccessoriesInvoiceNo VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Company'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Company VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InsuredState'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InsuredState NUMERIC(18,0);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InsuredCity'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InsuredCity NUMERIC(18,0);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InsuredAddressLine1'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InsuredAddressLine1 VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Remark1'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Remark1 VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'SumInsuredID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD SumInsuredID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Remark2'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Remark2 VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'PaymentRemarks'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD PaymentRemarks VARCHAR(MAX);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'AlternatePolicyNo'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD AlternatePolicyNo VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'PaymentReceivedDate'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD PaymentReceivedDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ProposerName'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ProposerName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ProposerRelationshipId'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ProposerRelationshipId INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ProposerDOB'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ProposerDOB DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ProposerGenderId'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ProposerGenderId INT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'IsDealerPayout'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD IsDealerPayout BIT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'CommisionPercentage'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD CommisionPercentage DECIMAL(18,2);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'CommisionAmount'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD CommisionAmount DECIMAL(18,2);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'LoanAmount'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD LoanAmount decimal(18,2);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'EMITenure'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD EMITenure int;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'EMIAmount'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD EMIAmount decimal(18,2);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'BankName'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD BankName VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'BankBranch'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD BankBranch VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'LifeCarePlanID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD LifeCarePlanID numeric(18,0) DEFAULT 0;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'AssistCompanyID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD AssistCompanyID INT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'InsuredAge'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD InsuredAge INT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'NomineeAge'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD NomineeAge INT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'IsCommision'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD IsCommision BIT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Mode'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Mode VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'AssistOEMID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD AssistOEMID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'HospiCashTypeID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD HospiCashTypeID INT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'IsByAPI'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD IsByAPI BIT;
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'VehicleClass'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD VehicleClass VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'Pincode'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD Pincode VARCHAR(50);
END;


IF NOT EXISTS
(
    SELECT *
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ProductTypeID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ProductTypeID INT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'CardLast4Digit'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD CardLast4Digit VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'NomineeTitle'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD NomineeTitle VARCHAR(25);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'PCCompanyID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD PCCompanyID NUMERIC(18, 0);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'APIUserTransactionID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD APIUserTransactionID NUMERIC(18, 0);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'MobileNoLast4Digit'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD MobileNoLast4Digit VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ContractNo'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ContractNo VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'MakeName'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD MakeName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ModelName'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ModelName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'VariantName'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD VariantName VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'State'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD State VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'City'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD City VARCHAR(500);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'PlatformID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD PlatformID INT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'PlanTypeID'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD PlanTypeID INT;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'MobileCost'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD MobileCost NUMERIC(38, 0);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'SerialNo'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD SerialNo VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'IMEI1'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD IMEI1 VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'IMEI2'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD IMEI2 VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'StorageCapacity'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD StorageCapacity VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'RAM'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD RAM VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ProductPurchaseDate'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ProductPurchaseDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'ProductInvoiceValue'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD ProductInvoiceValue VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'SalesInvoiceNo'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD SalesInvoiceNo VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'PlanPurchaseDate'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD PlanPurchaseDate DATETIME;
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblTransaction'
          AND COLUMN_NAME = 'CustomerContactAlternate'
)
    BEGIN
        ALTER TABLE tblTransaction
        ADD CustomerContactAlternate VARCHAR(20);
END;
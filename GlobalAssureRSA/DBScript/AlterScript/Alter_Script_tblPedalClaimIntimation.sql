IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblPedalClaimIntimation'
          AND COLUMN_NAME = 'Name'
)
    BEGIN
        ALTER TABLE tblPedalClaimIntimation
        ADD Name VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblPedalClaimIntimation'
          AND COLUMN_NAME = 'BankName'
)
    BEGIN
        ALTER TABLE tblPedalClaimIntimation
        ADD BankName VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblPedalClaimIntimation'
          AND COLUMN_NAME = 'IfscCode'
)
    BEGIN
        ALTER TABLE tblPedalClaimIntimation
        ADD IfscCode VARCHAR(100);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblPedalClaimIntimation'
          AND COLUMN_NAME = 'BankBranch'
)
    BEGIN
        ALTER TABLE tblPedalClaimIntimation
        ADD BankBranch VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblPedalClaimIntimation'
          AND COLUMN_NAME = 'AccountNumber'
)
    BEGIN
        ALTER TABLE tblPedalClaimIntimation
        ADD AccountNumber VARCHAR(250);
END;


IF NOT EXISTS
(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'tblPedalClaimIntimation'
          AND COLUMN_NAME = 'CancelledChequeCopy'
)
    BEGIN
        ALTER TABLE tblPedalClaimIntimation
        ADD CancelledChequeCopy VARCHAR(MAX);
END;
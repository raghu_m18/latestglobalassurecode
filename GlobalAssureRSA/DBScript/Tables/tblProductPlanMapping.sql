IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblProductPlanMapping]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblProductPlanMapping];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblProductPlanMapping
        (
			ID					    NUMERIC(18, 0) PRIMARY KEY IDENTITY,			
			ProductID				NUMERIC(18, 0),
			PlatformID				INT,
			PlanTypeID				INT,
			PlanID					NUMERIC(18, 0),
			RangeMin				DECIMAL(18, 2),
			RangeMax				DECIMAL(18, 2),
			IsValid					BIT,									
			CreatedDate				DATETIME DEFAULT(GETDATE())
        );
END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblMasterPolicyNo]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblMasterPolicyNo];
   --GO
BEGIN

CREATE TABLE [dbo].tblMasterPolicyNo
(
 Id								DECIMAL(18, 0) CONSTRAINT PK_tblMasterPolicyNo_Id PRIMARY KEY IDENTITY(101, 1),
 ProductID						NUMERIC(18, 0),
 PlanID						    NUMERIC(18, 0),
 MasterPolicyNo				    VARCHAR(200),
 StartDate						DATETIME,
 EndDate						DATETIME,
 CreatedBy				        VARCHAR(200),
 InsertDate					    DATETIME DEFAULT(GETDATE()),
 ModifyDate						DATETIME
);

END;

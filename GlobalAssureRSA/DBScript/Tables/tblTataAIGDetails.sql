
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblTataAIGDetails]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblTataAIGDetails];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblTataAIGDetails
        (
		ID int Primary key identity,
		CustomerName varchar(250),
		Surname varchar(250),
		PolicyCertificate Varchar(500),
		PolInceptDate DateTime,
		PolExpDate DateTime,
		CarSalesDate DateTime,
		City varchar(100),
		state varchar(100),
		RegistrationNo Varchar(500),
		Make Varchar(250),
		Model Varchar(250),
		Color Varchar(100),
		IssuanceDate DateTime,
		CancellationFlag bit,
		Branch Varchar(250),
		ProductName Varchar(250),
		Company Varchar(250),		
	    dt_Insert_Date          DATETIME DEFAULT(GETDATE()),
	    dt_Modify_Date          DATETIME NULL
        );
END;
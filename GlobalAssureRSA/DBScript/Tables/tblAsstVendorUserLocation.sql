
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblAsstVendorUserLocation]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblAsstVendorUserLocation];
   --GO
BEGIN

CREATE TABLE [dbo].tblAsstVendorUserLocation
(
 UniqueId         INT CONSTRAINT PK_tblAsstVendorUserLocation_UniqueId PRIMARY KEY IDENTITY(101, 1),
 UserId           VARCHAR(1000),
 ReferenceNo      VARCHAR(250),
 VendorLat		  VARCHAR(250),
 VendorLong		  VARCHAR(250),
 UserLat		  VARCHAR(250),
 UserLong		  VARCHAR(250),
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL,
 VendorId         VARCHAR(1000)
);

END;

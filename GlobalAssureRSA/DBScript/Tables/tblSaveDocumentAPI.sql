
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblSaveDocumentAPI]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblSaveDocumentAPI];
   --GO
BEGIN

CREATE TABLE [dbo].tblSaveDocumentAPI
(
 Id               INT CONSTRAINT PK_tblSaveDocumentAPI_UniqueId PRIMARY KEY IDENTITY(101, 1),
 UserId           VARCHAR(250),
 DocName		  VARCHAR(250),
 DocNo		      VARCHAR(250), 
 Notes            VARCHAR(1000), 
 ImageExtension   VARCHAR(25),
 Image            VARCHAR(MAX), 
 IsValid          BIT,
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL
);

END;


IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblEmailConfig]')
          AND TYPE IN(N'U')
)
--DROP TABLE [dbo].[tblEmailConfig];
--GO
    BEGIN
        CREATE TABLE [dbo].[tblEmailConfig]
        (n_Email_Config_Id  INT
         PRIMARY KEY NOT NULL, 
         s_SMTP_Host        VARCHAR(150) NOT NULL, 
         s_SMTP_Port        VARCHAR(10) NOT NULL, 
         s_SMTP_User_Name   VARCHAR(150) NOT NULL, 
         s_SMTP_Password    VARCHAR(250) NOT NULL, 
         s_From_Email       VARCHAR(150) NOT NULL, 
         s_From_Name        VARCHAR(150) NOT NULL, 
         n_SecureSMTP       BIT DEFAULT 0, 
         n_OrderNo          INT DEFAULT 0, 
		 n_EmailLimitCount          INT DEFAULT 0, 
		 n_SMSTriggerCount          INT DEFAULT 0, 
         n_Status           TINYINT DEFAULT 0, 
         s_Last_Modify_User VARCHAR(250) NOT NULL, 
         dt_Insert_tDate    DATETIME DEFAULT(GETDATE()), 
         dt_Modifiy_Date    DATETIME
        );
END;

CREATE SEQUENCE n_Email_Config_Id
     START WITH 100
     INCREMENT BY 1;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblIDFCBank]')
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblIDFCBank];
   --GO
BEGIN

CREATE TABLE [dbo].tblIDFCBank
(
 Id						INT IDENTITY Primary Key,
 FirstName	     VARCHAR(500),
 LastName	     VARCHAR(500),
 Email					VARCHAR(500),
 PolicyStatus bit,
 dt_InserDate		DATETIME DEFAULT(GETDATE()),
 ModifiedDate DATETIME
);

END;
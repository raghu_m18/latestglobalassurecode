
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblUniversalShampoo]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblUniversalShampoo];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblUniversalShampoo
        (
		ID int Primary key identity,
		CertificateNo varchar(250),
		EngineNo varchar(250),
		ChassisNo varchar(250),
		RegistrationNo varchar(250),
		Make Varchar(250),
		Model Varchar(250),
		Variant Varchar(250),
		FirstName Varchar(250),
		MiddleName Varchar(250),
		LastName Varchar(250),
		CertificateIssueDate Datetime,
		CoverStartDate DateTime,
		CoverEndDate DateTime,
		CustomerMobileNo Varchar(250),
		PermanentCity Varchar(250),
		PermanentState Varchar(250),
		PolicyStatus bit,
	    dt_Insert_Date          DATETIME DEFAULT(GETDATE()),
	    dt_Modify_Date          DATETIME NULL
        );
END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblPartnerAuthCodeMapping]')
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblPartnerAuthCodeMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblPartnerAuthCodeMapping
(
 Id						INT CONSTRAINT PK_tblPartnerAuthCodeMapping_ID PRIMARY KEY IDENTITY(101, 1),
 PartnerId				INT,
 PartnerAuthCode		VARCHAR(500),
 IsValid				BIT,
 CreatedDateTime		DATETIME DEFAULT(GETDATE())
);

END;
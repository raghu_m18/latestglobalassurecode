
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblAssistEmail]')
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblAssistEmail];
   --GO
BEGIN

CREATE TABLE [dbo].tblAssistEmail
(
 Id						INT CONSTRAINT PK_tblAssistEmail_ID PRIMARY KEY IDENTITY(101, 1),
 Email					VARCHAR(500),
 IsValid				BIT,
 CreatedDateTime		DATETIME DEFAULT(GETDATE())
);

END;
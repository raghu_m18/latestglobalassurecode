
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblMakeMapping]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblMakeMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblMakeMapping
(
 Id								DECIMAL(18, 0) CONSTRAINT PK_tblMakeMapping_Id PRIMARY KEY IDENTITY(101, 1),
 UserID							NVARCHAR(200),
 ProductID						NUMERIC(18, 0),
 MakeID							NUMERIC(18, 0),
 IsValid						BIT, 
 CreatedDate					DATETIME DEFAULT(GETDATE()),
 ModifyDate						DATETIME
);

END;


IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblEmailSquence]')
          AND TYPE IN(N'U')
)
--DROP TABLE [dbo].[tblEmailSquence];
--GO
    BEGIN
        CREATE TABLE [dbo].[tblEmailSquence]
        (n_Email_Config_Id     INT
         PRIMARY KEY NOT NULL, 
         n_CountNo       INT DEFAULT 0, 
         dt_Modifiy_Date DATETIME DEFAULT(GETDATE())
        );
END;

IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[tblGstNO]')
          AND TYPE IN(N'U')
)
-- DROP TABLE [tblGstNO];
--GO
    BEGIN
        CREATE TABLE tblGstNO
        (n_GstNoId        INT PRIMARY KEY IDENTITY(101, 1) NOT NULL,
	     s_GstNo          VARCHAR(MAX),                  
         n_Status         INT NOT NULL, 
		 d_StartDate      DATETIME DEFAULT(GETDATE()),
		 d_EndDate        DATETIME NULL,
         s_UserId         VARCHAR(250) NULL
        );
END;


IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblPedalPlanMapping]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblPedalPlanMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblPedalPlanMapping
(
 UniqueID				INT CONSTRAINT PK_tblPedalPlanMapping_UniqueID PRIMARY KEY IDENTITY(101, 1),
 ProductID				NUMERIC(18,0),
 PlanID					NUMERIC(18,0),
 CycleAmountMin			DECIMAL(18,2),
 CycleAmountMax			DECIMAL(18,2),
 PACover				BIT,
 DiscountedPlanAmount   DECIMAL(18,2),
 InsertDate		        DATETIME DEFAULT(GETDATE())
);

END;

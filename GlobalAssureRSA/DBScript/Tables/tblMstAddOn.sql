IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblMstAddOn]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblMstAddOn];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblMstAddOn
        (
			ID					    NUMERIC(18, 0) PRIMARY KEY IDENTITY,			
			TieUpCompanyID			NUMERIC(18, 0),
			Description				VARCHAR(1000),
			Value					VARCHAR(500),
			IsValid					BIT,
			StartDate				DATETIME,
			EndDate					DATETIME,		
			CreatedDate				DATETIME DEFAULT(GETDATE()),
			CreatedBy				VARCHAR(250),		
			LastModifiedDate		DATETIME,
			LastModifiedBy			VARCHAR(250)
        );
END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblCertificateMasterPolicyNoMappingLog]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblCertificateMasterPolicyNoMappingLog];
   --GO
BEGIN

CREATE TABLE [dbo].tblCertificateMasterPolicyNoMappingLog
(
 Id								DECIMAL(18, 0) CONSTRAINT PK_tblCertificateMasterPolicyNoMappingLog_Id PRIMARY KEY IDENTITY(101, 1),
 TransactionID					NUMERIC(18, 0),
 CertificateNo					VARCHAR(50),
 MasterPolicyNo				    VARCHAR(200),
 ErrorMessage				    VARCHAR(1000),
 InsertDate					    DATETIME DEFAULT(GETDATE()),
 ModifyDate						DATETIME
);

END;

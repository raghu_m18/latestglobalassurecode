
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblRoleMenuMappingMaster]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblRoleMenuMappingMaster];
   --GO
BEGIN

CREATE TABLE [dbo].tblRoleMenuMappingMaster
(
 Id               INT CONSTRAINT PK_tblRoleMenuMappingMaster_Id PRIMARY KEY IDENTITY(101, 1),
 RoleId           INT,
 MenuId           INT,
 IsValid		  BIT,
 InsertDate		  DATETIME DEFAULT(GETDATE())
);

END;

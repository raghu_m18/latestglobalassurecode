
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblPlansMapping]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblPlansMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblPlansMapping
(
 ID               INT CONSTRAINT PK_tblPlansMapping_Id PRIMARY KEY IDENTITY(101, 1),
 ProductID        NUMERIC(18, 0),
 SumInsuredID     INT,
 PlanID           NUMERIC(18, 0),
 Remark1          VARCHAR(250),
 Remark2          VARCHAR(250),
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL,
 IsValid		  BIT
);

END;

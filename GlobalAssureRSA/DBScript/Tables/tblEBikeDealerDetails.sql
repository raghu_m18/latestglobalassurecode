
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblEBikeDealerDetails]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblEBikeDealerDetails];
   --GO
BEGIN

CREATE TABLE [dbo].tblEBikeDealerDetails
(
 Id						INT CONSTRAINT PK_tblEBikeDealerDetails_ID PRIMARY KEY IDENTITY(101, 1),
 DealerName				VARCHAR(250),
 MobileNo				VARCHAR(20),
 IsValid				BIT,
 CreatedDate			DATETIME DEFAULT(GETDATE()),
 CreatedBy				VARCHAR(250)
);

END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblAPITransactionErrorLog]')
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblAPITransactionErrorLog];
   --GO
BEGIN

CREATE TABLE [dbo].tblAPITransactionErrorLog
(
 Id					NUMERIC(18, 0) CONSTRAINT PK_tblAPITransactionErrorLog_ID PRIMARY KEY IDENTITY(1, 1),
 TransactionID		NUMERIC(18, 0),
 ErrorMessage		VARCHAR(MAX),
 Status				VARCHAR(25),
 InsertDate			DATETIME DEFAULT(GETDATE())
);

END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblPedalClaimIntimation]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblPedalClaimIntimation];
   --GO
BEGIN

CREATE TABLE [dbo].tblPedalClaimIntimation
(
 Id								DECIMAL(18, 0) CONSTRAINT PK_tblPedalClaimIntimation_Id PRIMARY KEY IDENTITY(101, 1),
 UserId							VARCHAR(250),
 TransactionId					NUMERIC(18, 0),
 RefNo							VARCHAR(100),
 IsValid						BIT, 
 CreatedDate					DATETIME DEFAULT(GETDATE()),
 MobileNo						VARCHAR(15),
 ClaimType						VARCHAR(100),
 ClaimIntimationDate			DATETIME,
 ClaimDetails					VARCHAR(1000),
 FIRImageName					VARCHAR(100),
 FIRImage						VARCHAR(MAX),
 SupportedDoc1Name				VARCHAR(100),
 SupportedDoc1					VARCHAR(MAX),
 SupportedDoc2Name				VARCHAR(100),
 SupportedDoc2					VARCHAR(MAX),
 SupportedDoc3Name				VARCHAR(100),
 SupportedDoc3					VARCHAR(MAX),
 SupportedDoc4Name				VARCHAR(100),
 SupportedDoc4					VARCHAR(MAX),
 SupportedDoc5Name				VARCHAR(100),
 SupportedDoc5					VARCHAR(MAX),
 ModifyDate						DATETIME 
);

END;


IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblElectricBikeDetails]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblElectricBikeDetails];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblElectricBikeDetails
        (
			ID							INT PRIMARY KEY IDENTITY,
			FrameNo						VARCHAR(100),
			BatteryNo					VARCHAR(100),
			VehicleType					VARCHAR(100),
			Make						VARCHAR(100),
			Model						VARCHAR(100),
			PlanName					VARCHAR(100),
			Title						VARCHAR(25),
			InsuredFirstName			VARCHAR(100),
			InsuredLastName				VARCHAR(100),
			InsuredMobileNo				VARCHAR(20),
			InsuredEmailID				VARCHAR(100),
			InsuredAddress				VARCHAR(500),
			InsuredState				VARCHAR(100),
			InsuredCity					VARCHAR(100),
			Color						VARCHAR(100),	
			PolicyStatus				BIT,
			dt_Insert_Date				DATETIME DEFAULT(GETDATE()),
			dt_Modify_Date				DATETIME
        );
END;
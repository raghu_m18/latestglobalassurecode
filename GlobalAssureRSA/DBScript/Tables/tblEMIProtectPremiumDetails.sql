
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblEMIProtectPremiumDetails]')
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblEMIProtectPremiumDetails];
   --GO
BEGIN

CREATE TABLE [dbo].tblEMIProtectPremiumDetails
(
 Id					INT CONSTRAINT PK_tblEMIProtectPremiumDetails_ID PRIMARY KEY IDENTITY(101, 1),
 TransactionID		NUMERIC(18, 0),
 PlanID				NUMERIC(18, 0),
 Amount				DECIMAL(18, 2),
 GST				DECIMAL(18, 2),
 Total				DECIMAL(18, 2),
 IsValid			BIT,
 CreatedDate		DATETIME DEFAULT(GETDATE())
);

END;
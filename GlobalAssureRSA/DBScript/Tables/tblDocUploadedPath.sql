IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblDocUploadedPath]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblDocUploadedPath];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblDocUploadedPath
        (
			ID					    NUMERIC(18, 0) PRIMARY KEY IDENTITY,			
			TransactionID			NUMERIC(18, 0),
			IsValid					BIT,
			Doc1_Path				VARCHAR(500),
			Doc2_Path				VARCHAR(500),							
			CreatedDate				DATETIME DEFAULT(GETDATE()),
			CreatedBy				VARCHAR(250)
        );
END;
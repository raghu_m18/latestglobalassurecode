
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblVendorResponseUrl]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblVendorResponseUrl];
   --GO
BEGIN

CREATE TABLE [dbo].tblVendorResponseUrl
(
 Id               INT CONSTRAINT PK_tblVendorResponseUrl_UniqueId PRIMARY KEY IDENTITY(101, 1),
 UserId           VARCHAR(250),
 IsValid          BIT,
 ResponseUrl      VARCHAR(MAX), 
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL
);

END;


IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblOEMCompanyMapping]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblOEMCompanyMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblOEMCompanyMapping
(
 Id						INT CONSTRAINT PK_tblOEMCompanyMapping_ID PRIMARY KEY IDENTITY(101, 1),
 OEMId					INT,
 CompanyId				INT,
 IsValid				BIT,
 CreatedDateTime		DATETIME DEFAULT(GETDATE())
);

END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblCityProductMapping]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblCityProductMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblCityProductMapping
(
 ID               INT CONSTRAINT PK_tblCityProductMapping_ID PRIMARY KEY IDENTITY(101, 1),
 ProductID        NUMERIC(18, 0),
 StateID          NUMERIC(18, 0),
 CityID			  NUMERIC(18, 0),
 PlanID           NUMERIC(18, 0),
 IsValid		  BIT,
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL,
 UserID			  NVARCHAR(250)
);

END;

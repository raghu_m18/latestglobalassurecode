
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblPCCompanyLogo]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblPCCompanyLogo];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblPCCompanyLogo
        (
			ID					INT PRIMARY KEY IDENTITY (101, 1),
			CompanyID		    NUMERIC(18, 0),
			ImagePath			VARCHAR(250),
			IsValid				BIT,
			CreatedDate			DATETIME DEFAULT(GETDATE()),
			UploadedBy			VARCHAR(250),
			LastModifiedDate	DATETIME,
			LastModifiedBy		VARCHAR(250)
        );
END;

IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblCaseCharges]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblCaseCharges];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblCaseCharges
        (
		Id						int Primary key identity (101,1),
		AssistID				decimal(18,0),
		userId					varchar(500),
		TollCharges				decimal(18,2),
		WaitingHoursCharge		decimal(18,2),
		VehicleCustodyCharge	decimal(18,2),
		OtherCharges			decimal(18,2),
	    dt_Insert_Date          DATETIME DEFAULT(GETDATE()),
	    dt_Modify_Date          DATETIME NULL
        );
END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblPlanMasterTravelREL]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblPlanMasterTravelREL];
   --GO
BEGIN

CREATE TABLE [dbo].tblPlanMasterTravelREL
(
 PlanId           INT CONSTRAINT PK_tblPlanMasterTravelREL_UniqueId PRIMARY KEY IDENTITY(101, 1),
 PlanName         VARCHAR(MAX),
 AgeGroupId       INT,
 AgeGroup         VARCHAR(50),
 IsValid          BIT,
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL
);

END;

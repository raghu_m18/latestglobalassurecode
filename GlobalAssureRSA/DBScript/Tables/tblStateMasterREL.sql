
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblStateMasterREL]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblStateMasterREL];
   --GO
BEGIN

CREATE TABLE [dbo].tblStateMasterREL
(
 StateId          INT CONSTRAINT PK_tblStateMasterREL_UniqueId PRIMARY KEY IDENTITY(101, 1),
 StateName        VARCHAR(250),
 IsValid          BIT,
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL
);

END;


IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblCountryMasterTravelREL]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblCountryMasterTravelREL];
   --GO
BEGIN

CREATE TABLE [dbo].tblCountryMasterTravelREL
(
 CountryId        INT CONSTRAINT PK_tblCountryMasterTravelREL_UniqueId PRIMARY KEY IDENTITY(101, 1),
 CountryName      VARCHAR(250),
 IsValid          BIT,
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL
);

END;

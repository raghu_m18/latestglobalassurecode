
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblBhartiAXADetails]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblBhartiAXADetails];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblBhartiAXADetails
        (
		ID int Primary key identity,
		CertificateNo varchar(250),
		EngineNo varchar(250),
		ChassisNo varchar(250),
		RegistrationNo varchar(250),
		Make Varchar(250),
		Model Varchar(250),
		Variant Varchar(250),
		FirstName Varchar(250),
		CertificateIssueDate Datetime,
		CoverStartDate DateTime,
		CoverEndDate DateTime,
		CustomerMobileNo Varchar(250),
		PermanentCity Varchar(250),
		PermanentState Varchar(250),
	    dt_Insert_Date          DATETIME DEFAULT(GETDATE()),
	    dt_Modify_Date          DATETIME NULL
        );
END;
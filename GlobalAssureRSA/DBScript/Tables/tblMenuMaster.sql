
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblMenuMaster]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblMenuMaster];
   --GO
BEGIN

CREATE TABLE [dbo].tblMenuMaster
(
 Id               INT CONSTRAINT PK_tblMenuMaster_Id PRIMARY KEY IDENTITY(101, 1),
 Menu             VARCHAR(255),
 MenuURL          VARCHAR(255),
 Description      VARCHAR(255),
 IsValid		  BIT,
 OrderNo          INT,
 ParentId         INT,
 InsertDate		  DATETIME DEFAULT(GETDATE())
);

END;

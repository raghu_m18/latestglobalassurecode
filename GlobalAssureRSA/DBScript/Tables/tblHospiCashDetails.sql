
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblHospiCashDetails]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblHospiCashDetails];
   --GO
BEGIN

CREATE TABLE [dbo].tblHospiCashDetails
(
 Id						INT CONSTRAINT PK_tblHospiCashDetails_ID PRIMARY KEY IDENTITY(101, 1),
 TransactionID			NUMERIC(18, 0),
 IsValid				BIT,
 ProductName			VARCHAR(200),
 ProductCode			VARCHAR(200),
 PlanCode				VARCHAR(200),
 SumAssured				VARCHAR(100),
 StartDate				VARCHAR(50),
 ExpiryDate				VARCHAR(50),
 Premium				DECIMAL(18, 2),
 GST					DECIMAL(18, 2),
 TotalPremium			DECIMAL(18, 2),
 PolicyNumber			VARCHAR(100),
 Carrier				VARCHAR(150),
 Covers					VARCHAR(150),
 CoverCode				VARCHAR(50),
 COI					VARCHAR(MAX),
 CustomerRefNo			VARCHAR(100),
 Status					VARCHAR(50),
 ResponseStatus			VARCHAR(50),
 ResponseRemark			VARCHAR(MAX),
 CreatedDateTime		DATETIME DEFAULT(GETDATE())
);

END;
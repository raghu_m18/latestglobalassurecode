
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblRequestResponse]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblRequestResponse];
   --GO
BEGIN

CREATE TABLE [dbo].tblRequestResponse
(
 Id               INT CONSTRAINT PK_tblRequestResponse_UniqueId PRIMARY KEY IDENTITY(101, 1),
 TransactionId    BIGINT,
 Request		  VARCHAR(MAX),
 Response		  VARCHAR(MAX), 
 InsertDate		  DATETIME DEFAULT(GETDATE()), 
 ModifyDate	      DATETIME NULL
);

END;

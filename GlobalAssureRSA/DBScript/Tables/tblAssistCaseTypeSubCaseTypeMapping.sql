
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblAssistCaseTypeSubCaseTypeMapping]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblAssistCaseTypeSubCaseTypeMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblAssistCaseTypeSubCaseTypeMapping
(
 Id						INT CONSTRAINT PK_tblAssistCaseTypeSubCaseTypeMapping_ID PRIMARY KEY IDENTITY(101, 1),
 CaseTypeId				INT,
 SubCaseTypeId			INT,
 IsValid				BIT,
 CreatedDateTime		DATETIME DEFAULT(GETDATE())
);

END;
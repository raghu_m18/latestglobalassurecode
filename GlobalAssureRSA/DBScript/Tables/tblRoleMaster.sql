
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblRoleMaster]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblRoleMaster];
   --GO
BEGIN

CREATE TABLE [dbo].tblRoleMaster
(
 Id               INT CONSTRAINT PK_tblRoleMaster_ID PRIMARY KEY IDENTITY(101, 1),
 Name             VARCHAR(250),
 Description      VARCHAR(500),
 IsValid		  BIT,
 InsertDate		  DATETIME DEFAULT(GETDATE())
);

END;

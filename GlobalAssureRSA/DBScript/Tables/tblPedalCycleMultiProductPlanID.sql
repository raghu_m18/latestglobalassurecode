
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblPedalCycleMultiProductPlanID]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblPedalCycleMultiProductPlanID];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblPedalCycleMultiProductPlanID
        (
			ID					INT PRIMARY KEY IDENTITY (101, 1),
			TransactionID		NUMERIC(18, 0),
			IsValid				BIT,
			CreatedDate			DATETIME DEFAULT(GETDATE()),
			PlanID_Multi		NUMERIC(18, 0),
			PlanID_PC			NUMERIC(18, 0)			
        );
END;

IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblICICILombard]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblICICILombard];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblICICILombard
        (
			ID					INT PRIMARY KEY IDENTITY,
			LotRecdDate			DATETIME,
			POLICYNO			VARCHAR(250),
			InsuredName			VARCHAR(250),
			EngineNo			VARCHAR(250),
			ChassisNo			VARCHAR(250),
			RegistrationNo		VARCHAR(250),
			PolicyStartDate		DATETIME,
			PolicyEndDate		DATETIME,
			PolicyStatus		BIT,
			UploadedBy			VARCHAR(250),
			LastModifiedBy		VARCHAR(250),
			dt_Insert_Date		DATETIME DEFAULT(GETDATE()),
			dt_Modify_Date		DATETIME NULL
        );
END;
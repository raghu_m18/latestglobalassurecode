
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblPinnace]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblPinnace];
   --GO
    BEGIN
        CREATE TABLE [dbo].[tblPinnace]
        (
			[ID] [int] PRIMARY KEY IDENTITY,
	[ChassisNo] [varchar](250) NULL,
	[Make] [varchar](250) NULL,
	[Model] [varchar](250) NULL,
	[Plan] [varchar](250) NULL,
	[Title] [varchar](250) NULL,
	[InsuredFirstName] [varchar](250) NULL,
	[InsuredLastName] [varchar](250) NULL,
	[InsuredMobileNo] [varchar](250) NULL,
	[InsuredEmailID] [varchar](250) NULL,
	[InsuredDOB] [datetime] NULL,
	[InsuredAddress] [varchar](2500) NULL,
	[InsuredState] [varchar](250) NULL,
	[InsuredCity] [varchar](250) NULL,
	[CoverStartDate] [datetime] NULL,
	[Color] [varchar](250) NULL,
	[NomineeName] [varchar](250) NULL,
	[NomineeDOB] [datetime] NULL,
	[NomineeRelationship] [varchar](250) NULL,
	[InvoiceNo] [varchar](50) NULL,
	[InvoiceDate] [datetime] NULL,
	[PolicyStatus] [bit] NULL,
	[UploadedBy] [varchar](250) NULL,
	[LastModifiedBy] [varchar](250) NULL,
	[dt_Insert_Date] [datetime] NULL,
	[dt_Modify_Date] [datetime] NULL
        );
END;
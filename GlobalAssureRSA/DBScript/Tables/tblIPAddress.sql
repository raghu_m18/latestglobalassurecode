
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblIPAddress]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblIPAddress];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblIPAddress
        (
			ID					INT PRIMARY KEY IDENTITY,			
			EmailId			    VARCHAR(150),
			Created_DateTime	DATETIME DEFAULT(GETDATE()),
			IPAdress			VARCHAR(150),
			Status		 		BIT
        );
END;
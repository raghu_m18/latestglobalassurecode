
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblEmailLog]')
          AND TYPE IN(N'U')
)
--DROP TABLE [dbo].[tblEmailLog];
--GO
    BEGIN
        CREATE TABLE [dbo].[tblEmailLog]
        (n_Email_Log_Id    INT
         PRIMARY KEY IDENTITY(1, 1) NOT NULL, 
         s_Transaction_Id  VARCHAR(150), 
         n_Email_Config_Id INT NOT NULL, 
         s_SMTP_User_Name  VARCHAR(150) NOT NULL, 
         s_From_Email      VARCHAR(150) NOT NULL, 
         s_From_Name       VARCHAR(150), 
         s_Email_To        VARCHAR(150) NOT NULL, 
         s_Email_BCC       VARCHAR(1000), 
         s_Email_CC        VARCHAR(1000), 
         s_Email_Subject   VARCHAR(1000), 
         s_Email_Body      VARCHAR(4000), 
         s_Attachment      VARCHAR(1000), 
         s_Send_Status     VARCHAR(150), 
         s_Error           VARCHAR(1000), 
         dt_Insert_tDate   DATETIME DEFAULT(GETDATE())
        );
END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblHospiCashPlanMapping]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblHospiCashPlanMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblHospiCashPlanMapping
(
 Id						INT CONSTRAINT PK_tblHospiCashPlanMapping_ID PRIMARY KEY IDENTITY(101, 1),
 PlanID					NUMERIC,
 PlanCode				VARCHAR(150),
 ProductCode			VARCHAR(150),
 IsValid				BIT,
 CreatedDateTime		DATETIME DEFAULT(GETDATE())
);

END;
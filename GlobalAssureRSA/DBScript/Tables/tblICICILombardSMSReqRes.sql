
IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblICICILombardSMSReqRes]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblICICILombardSMSReqRes];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblICICILombardSMSReqRes
        (
			ID					INT PRIMARY KEY IDENTITY (101, 1),
			CreatedDate			DATETIME DEFAULT(GETDATE()),
			IsValid             BIT,
			AssistId		    DECIMAL(18, 0),
			APIAuthRequest		VARCHAR(MAX),
			APIAuthResponse		VARCHAR(MAX),	
			APIAuthStatus       VARCHAR(50),
			APISMSRequest		VARCHAR(MAX),
			APISMSResponse		VARCHAR(MAX),
			APISMSStatus       VARCHAR(50)
        );
END;
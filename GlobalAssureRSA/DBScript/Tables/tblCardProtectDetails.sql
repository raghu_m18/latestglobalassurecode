
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblCardProtectDetails]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblCardProtectDetails];
   --GO
BEGIN

CREATE TABLE [dbo].tblCardProtectDetails
(
 Id								DECIMAL(18, 0) CONSTRAINT PK_tblCardProtectDetails_Id PRIMARY KEY IDENTITY(101, 1),
 TransactionId					NUMERIC(18, 0),
 IsValid						BIT, 
 CreatedDate					DATETIME DEFAULT(GETDATE()),
 CardNo						    VARCHAR(50),
 CardType						VARCHAR(50),
 BankID						    NUMERIC(18, 0),
 ModifyDate						DATETIME
);

END;


IF NOT EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[tblIFFCODetails]')
          AND TYPE IN(N'U')
)

   --  DROP TABLE [dbo].[tblIFFCODetails];
   --GO
    BEGIN
        CREATE TABLE [dbo].tblIFFCODetails
        (
		ID int Primary key identity,
		Policy varchar(250),
		P400Policy varchar(250),
		TypeofPolicy Varchar(250),
		PolicyType varchar(250),
		Status Varchar(250),
		FirstName Varchar(250),
		LastName varchar(100),
		Policysubmitted DateTime,
		Created DateTime,
		Inceptiondate Datetime,
		Expirydate DateTime,
		Branch Varchar(250),
		Lateral Varchar(250),
		BimaKendraCode Varchar(250),
		Product Varchar(250),
		NetPremiumPayable Decimal,
		RegistrationNo Varchar(250),
		EngineNumber Varchar(250),
		ChassisNumber Varchar(250),
		Make Varchar(250),
		Manufacturer Varchar(250),
		Model Varchar(250),				
	    dt_Insert_Date          DATETIME DEFAULT(GETDATE()),
	    dt_Modify_Date          DATETIME NULL
        );
END;

IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblModelMapping]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblModelMapping];
   --GO
BEGIN

CREATE TABLE [dbo].tblModelMapping
(
 Id								DECIMAL(18, 0) CONSTRAINT PK_tblModelMapping_Id PRIMARY KEY IDENTITY(101, 1),
 UserID							NVARCHAR(200),
 MakeID							NUMERIC(18, 0),
 ModelID						NUMERIC(18, 0),
 IsValid						BIT, 
 CreatedDate					DATETIME DEFAULT(GETDATE()),
 ModifyDate						DATETIME
);

END;

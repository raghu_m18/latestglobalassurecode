
IF NOT EXISTS
(
SELECT 1 FROM sys.objects WHERE OBJECT_ID=OBJECT_ID(N'[dbo].[tblOTPProcess]') 
AND TYPE IN(N'U'))

   --  DROP TABLE [dbo].[tblOTPProcess];
   --GO
BEGIN

CREATE TABLE [dbo].tblOTPProcess
(
 UniqueID			INT CONSTRAINT PK_tblOTPProcess_UniqueID PRIMARY KEY IDENTITY(101, 1),
 Status             BIT,
 Application		VARCHAR(250),
 MobileNo			VARCHAR(25),
 OTP			    INT,          
 InsertDate		    DATETIME DEFAULT(GETDATE()),
 ModifyDate		    DATETIME
);

END;

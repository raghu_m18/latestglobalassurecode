IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_i_AspNetUsers]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_i_AspNetUsers;
GO
CREATE TRIGGER trg_i_AspNetUsers ON [dbo].[AspNetUsers]
FOR INSERT
AS
     BEGIN
         INSERT INTO AspNetUsers_Audit
         (s_Trans_Type, 
          Id, 
          Email, 
          EmailConfirmed, 
          PasswordHash, 
          SecurityStamp, 
          PhoneNumber, 
          PhoneNumberConfirmed, 
          TwoFactorEnabled, 
          LockoutEndDateUtc, 
          LockoutEnabled, 
          AccessFailedCount, 
          UserName, 
          DOB, 
          IsAdmin, 
          Name, 
          IsActive, 
          Prefix, 
          IsLocked, 
          DealerName, 
          TotalUsedPlanAmount, 
          PickUpPoint, 
          IsFunctionalityLocked, 
          CreatedDate, 
          LogoPath, 
          IsBulkCertificationMenu, 
          PaymentReceivedAmount, 
          AccountNo, 
          Ifsc_Code, 
          BeneficiaryName, 
          BankBranchName, 
          GSTno, 
          PANno, 
          IsAgreementReceived, 
          TotalCDAmount, 
          TotalCDUsedAmount, 
          TotalCDBalanceAmount, 
          PayoutPercentage, 
          IMDCode, 
          TieUpCompanyID, 
          VendorType, 
          IsApiUser, 
          StoreName, 
          MappingUserId, 
          AccountHolderName, 
          ChequeImage, 
          IsBankDetailsApproved, 
          OtherAdminId, 
          ContactPersonName, 
          ContactPersonNo, 
          LastModifiedBy, 
          LastModifiedDate,
		  RiskStartCount,
		  BranchCode,
		  ContactPersonCommisionAmount,
		  CompanyID,
		  OEMID,
		  IsDownloadCertificate
         )
                SELECT 'I', 
                       Id, 
                       Email, 
                       EmailConfirmed, 
                       PasswordHash, 
                       SecurityStamp, 
                       PhoneNumber, 
                       PhoneNumberConfirmed, 
                       TwoFactorEnabled, 
                       LockoutEndDateUtc, 
                       LockoutEnabled, 
                       AccessFailedCount, 
                       UserName, 
                       DOB, 
                       IsAdmin, 
                       Name, 
                       IsActive, 
                       Prefix, 
                       IsLocked, 
                       DealerName, 
                       TotalUsedPlanAmount, 
                       PickUpPoint, 
                       IsFunctionalityLocked, 
                       CreatedDate, 
                       LogoPath, 
                       IsBulkCertificationMenu, 
                       PaymentReceivedAmount, 
                       AccountNo, 
                       Ifsc_Code, 
                       BeneficiaryName, 
                       BankBranchName, 
                       GSTno, 
                       PANno, 
                       IsAgreementReceived, 
                       TotalCDAmount, 
                       TotalCDUsedAmount, 
                       TotalCDBalanceAmount, 
                       PayoutPercentage, 
                       IMDCode, 
                       TieUpCompanyID, 
                       VendorType, 
                       IsApiUser, 
                       StoreName, 
                       MappingUserId, 
                       AccountHolderName, 
                       ChequeImage, 
                       IsBankDetailsApproved, 
                       OtherAdminId, 
                       ContactPersonName, 
                       ContactPersonNo, 
                       LastModifiedBy, 
                       LastModifiedDate,
					   RiskStartCount,
					   BranchCode,
					   ContactPersonCommisionAmount,
					   CompanyID,
					   OEMID,
					   IsDownloadCertificate
                FROM INSERTED;
     END;
GO
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_U_AspNetUsers]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_U_AspNetUsers;
GO
CREATE TRIGGER trg_U_AspNetUsers ON [dbo].[AspNetUsers]
FOR UPDATE
AS
     BEGIN
         INSERT INTO AspNetUsers_Audit
         (s_Trans_Type, 
          Id, 
          Email, 
          EmailConfirmed, 
          PasswordHash, 
          SecurityStamp, 
          PhoneNumber, 
          PhoneNumberConfirmed, 
          TwoFactorEnabled, 
          LockoutEndDateUtc, 
          LockoutEnabled, 
          AccessFailedCount, 
          UserName, 
          DOB, 
          IsAdmin, 
          Name, 
          IsActive, 
          Prefix, 
          IsLocked, 
          DealerName, 
          TotalUsedPlanAmount, 
          PickUpPoint, 
          IsFunctionalityLocked, 
          CreatedDate, 
          LogoPath, 
          IsBulkCertificationMenu, 
          PaymentReceivedAmount, 
          AccountNo, 
          Ifsc_Code, 
          BeneficiaryName, 
          BankBranchName, 
          GSTno, 
          PANno, 
          IsAgreementReceived, 
          TotalCDAmount, 
          TotalCDUsedAmount, 
          TotalCDBalanceAmount, 
          PayoutPercentage, 
          IMDCode, 
          TieUpCompanyID, 
          VendorType, 
          IsApiUser, 
          StoreName, 
          MappingUserId, 
          AccountHolderName, 
          ChequeImage, 
          IsBankDetailsApproved, 
          OtherAdminId, 
          ContactPersonName, 
          ContactPersonNo, 
          LastModifiedBy, 
          LastModifiedDate,
		  RiskStartCount,
		  BranchCode,
		  ContactPersonCommisionAmount,
		  CompanyID,
		  OEMID,
		  IsDownloadCertificate
         )
                SELECT 'U', 
                       Id, 
                       Email, 
                       EmailConfirmed, 
                       PasswordHash, 
                       SecurityStamp, 
                       PhoneNumber, 
                       PhoneNumberConfirmed, 
                       TwoFactorEnabled, 
                       LockoutEndDateUtc, 
                       LockoutEnabled, 
                       AccessFailedCount, 
                       UserName, 
                       DOB, 
                       IsAdmin, 
                       Name, 
                       IsActive, 
                       Prefix, 
                       IsLocked, 
                       DealerName, 
                       TotalUsedPlanAmount, 
                       PickUpPoint, 
                       IsFunctionalityLocked, 
                       CreatedDate, 
                       LogoPath, 
                       IsBulkCertificationMenu, 
                       PaymentReceivedAmount, 
                       AccountNo, 
                       Ifsc_Code, 
                       BeneficiaryName, 
                       BankBranchName, 
                       GSTno, 
                       PANno, 
                       IsAgreementReceived, 
                       TotalCDAmount, 
                       TotalCDUsedAmount, 
                       TotalCDBalanceAmount, 
                       PayoutPercentage, 
                       IMDCode, 
                       TieUpCompanyID, 
                       VendorType, 
                       IsApiUser, 
                       StoreName, 
                       MappingUserId, 
                       AccountHolderName, 
                       ChequeImage, 
                       IsBankDetailsApproved, 
                       OtherAdminId, 
                       ContactPersonName, 
                       ContactPersonNo, 
                       LastModifiedBy, 
                       LastModifiedDate,
					   RiskStartCount,
					   BranchCode,
					   ContactPersonCommisionAmount,
					   CompanyID,
					   OEMID,
					   IsDownloadCertificate
                FROM INSERTED;
         INSERT INTO AspNetUsers_Audit
         (s_Trans_Type, 
          Id, 
          Email, 
          EmailConfirmed, 
          PasswordHash, 
          SecurityStamp, 
          PhoneNumber, 
          PhoneNumberConfirmed, 
          TwoFactorEnabled, 
          LockoutEndDateUtc, 
          LockoutEnabled, 
          AccessFailedCount, 
          UserName, 
          DOB, 
          IsAdmin, 
          Name, 
          IsActive, 
          Prefix, 
          IsLocked, 
          DealerName, 
          TotalUsedPlanAmount, 
          PickUpPoint, 
          IsFunctionalityLocked, 
          CreatedDate, 
          LogoPath, 
          IsBulkCertificationMenu, 
          PaymentReceivedAmount, 
          AccountNo, 
          Ifsc_Code, 
          BeneficiaryName, 
          BankBranchName, 
          GSTno, 
          PANno, 
          IsAgreementReceived, 
          TotalCDAmount, 
          TotalCDUsedAmount, 
          TotalCDBalanceAmount, 
          PayoutPercentage, 
          IMDCode, 
          TieUpCompanyID, 
          VendorType, 
          IsApiUser, 
          StoreName, 
          MappingUserId, 
          AccountHolderName, 
          ChequeImage, 
          IsBankDetailsApproved, 
          OtherAdminId, 
          ContactPersonName, 
          ContactPersonNo, 
          LastModifiedBy, 
          LastModifiedDate,
		  RiskStartCount,
		  BranchCode,
		  ContactPersonCommisionAmount,
		  CompanyID,
		  OEMID,
		  IsDownloadCertificate
         )
                SELECT 'D', 
                       Id, 
                       Email, 
                       EmailConfirmed, 
                       PasswordHash, 
                       SecurityStamp, 
                       PhoneNumber, 
                       PhoneNumberConfirmed, 
                       TwoFactorEnabled, 
                       LockoutEndDateUtc, 
                       LockoutEnabled, 
                       AccessFailedCount, 
                       UserName, 
                       DOB, 
                       IsAdmin, 
                       Name, 
                       IsActive, 
                       Prefix, 
                       IsLocked, 
                       DealerName, 
                       TotalUsedPlanAmount, 
                       PickUpPoint, 
                       IsFunctionalityLocked, 
                       CreatedDate, 
                       LogoPath, 
                       IsBulkCertificationMenu, 
                       PaymentReceivedAmount, 
                       AccountNo, 
                       Ifsc_Code, 
                       BeneficiaryName, 
                       BankBranchName, 
                       GSTno, 
                       PANno, 
                       IsAgreementReceived, 
                       TotalCDAmount, 
                       TotalCDUsedAmount, 
                       TotalCDBalanceAmount, 
                       PayoutPercentage, 
                       IMDCode, 
                       TieUpCompanyID, 
                       VendorType, 
                       IsApiUser, 
                       StoreName, 
                       MappingUserId, 
                       AccountHolderName, 
                       ChequeImage, 
                       IsBankDetailsApproved, 
                       OtherAdminId, 
                       ContactPersonName, 
                       ContactPersonNo, 
                       LastModifiedBy, 
                       LastModifiedDate,
					   RiskStartCount,
					   BranchCode,
					   ContactPersonCommisionAmount,
					   CompanyID,
					   OEMID,
					   IsDownloadCertificate
                FROM DELETED;
     END;
GO
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_D_AspNetUsers]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_D_AspNetUsers;
GO
CREATE TRIGGER trg_D_AspNetUsers ON [dbo].[AspNetUsers]
FOR DELETE
AS
     BEGIN
         INSERT INTO AspNetUsers_Audit
         (s_Trans_Type, 
          Id, 
          Email, 
          EmailConfirmed, 
          PasswordHash, 
          SecurityStamp, 
          PhoneNumber, 
          PhoneNumberConfirmed, 
          TwoFactorEnabled, 
          LockoutEndDateUtc, 
          LockoutEnabled, 
          AccessFailedCount, 
          UserName, 
          DOB, 
          IsAdmin, 
          Name, 
          IsActive, 
          Prefix, 
          IsLocked, 
          DealerName, 
          TotalUsedPlanAmount, 
          PickUpPoint, 
          IsFunctionalityLocked, 
          CreatedDate, 
          LogoPath, 
          IsBulkCertificationMenu, 
          PaymentReceivedAmount, 
          AccountNo, 
          Ifsc_Code, 
          BeneficiaryName, 
          BankBranchName, 
          GSTno, 
          PANno, 
          IsAgreementReceived, 
          TotalCDAmount, 
          TotalCDUsedAmount, 
          TotalCDBalanceAmount, 
          PayoutPercentage, 
          IMDCode, 
          TieUpCompanyID, 
          VendorType, 
          IsApiUser, 
          StoreName, 
          MappingUserId, 
          AccountHolderName, 
          ChequeImage, 
          IsBankDetailsApproved, 
          OtherAdminId, 
          ContactPersonName, 
          ContactPersonNo, 
          LastModifiedBy, 
          LastModifiedDate,
		  RiskStartCount,
		  BranchCode,
		  ContactPersonCommisionAmount,
		  CompanyID,
		  OEMID,
		  IsDownloadCertificate
         )
                SELECT 'D', 
                       Id, 
                       Email, 
                       EmailConfirmed, 
                       PasswordHash, 
                       SecurityStamp, 
                       PhoneNumber, 
                       PhoneNumberConfirmed, 
                       TwoFactorEnabled, 
                       LockoutEndDateUtc, 
                       LockoutEnabled, 
                       AccessFailedCount, 
                       UserName, 
                       DOB, 
                       IsAdmin, 
                       Name, 
                       IsActive, 
                       Prefix, 
                       IsLocked, 
                       DealerName, 
                       TotalUsedPlanAmount, 
                       PickUpPoint, 
                       IsFunctionalityLocked, 
                       CreatedDate, 
                       LogoPath, 
                       IsBulkCertificationMenu, 
                       PaymentReceivedAmount, 
                       AccountNo, 
                       Ifsc_Code, 
                       BeneficiaryName, 
                       BankBranchName, 
                       GSTno, 
                       PANno, 
                       IsAgreementReceived, 
                       TotalCDAmount, 
                       TotalCDUsedAmount, 
                       TotalCDBalanceAmount, 
                       PayoutPercentage, 
                       IMDCode, 
                       TieUpCompanyID, 
                       VendorType, 
                       IsApiUser, 
                       StoreName, 
                       MappingUserId, 
                       AccountHolderName, 
                       ChequeImage, 
                       IsBankDetailsApproved, 
                       OtherAdminId, 
                       ContactPersonName, 
                       ContactPersonNo, 
                       LastModifiedBy, 
                       LastModifiedDate,
					   RiskStartCount,
					   BranchCode,
					   ContactPersonCommisionAmount,
					   CompanyID,
					   OEMID,
					   IsDownloadCertificate
                FROM DELETED;
     END;
GO
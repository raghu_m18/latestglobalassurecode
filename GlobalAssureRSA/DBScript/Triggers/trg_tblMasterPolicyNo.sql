IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_i_tblMasterPolicyNo]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_i_tblMasterPolicyNo;
GO
CREATE TRIGGER trg_i_tblMasterPolicyNo ON [dbo].[tblMasterPolicyNo]
FOR INSERT
AS
     BEGIN
         INSERT INTO tblMasterPolicyNo_Audit
         (s_Trans_Type,   
		  Id,				
		  ProductID,	
		  PlanID,			
		  MasterPolicyNo,	
		  StartDate,		
		  EndDate,		
		  CreatedBy,		
		  InsertDate,		
		  ModifyDate		
         )
                SELECT 'I', 
                       Id,				
					   ProductID,	
					   PlanID,			
					   MasterPolicyNo,	
					   StartDate,		
					   EndDate,		
					   CreatedBy,		
					   InsertDate,		
					   ModifyDate
                FROM INSERTED;
     END;
GO
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_U_tblMasterPolicyNo]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_U_tblMasterPolicyNo;
GO
CREATE TRIGGER trg_U_tblMasterPolicyNo ON [dbo].[tblMasterPolicyNo]
FOR UPDATE
AS
     BEGIN
         INSERT INTO tblMasterPolicyNo_Audit
         (s_Trans_Type,   
		  Id,				
		  ProductID,	
		  PlanID,			
		  MasterPolicyNo,	
		  StartDate,		
		  EndDate,		
		  CreatedBy,		
		  InsertDate,		
		  ModifyDate		
         )
                SELECT 'U', 
                       Id,				
					   ProductID,	
					   PlanID,			
					   MasterPolicyNo,	
					   StartDate,		
					   EndDate,		
					   CreatedBy,		
					   InsertDate,		
					   ModifyDate
                FROM INSERTED;
         INSERT INTO tblMasterPolicyNo_Audit
         (s_Trans_Type,   
		  Id,				
		  ProductID,	
		  PlanID,			
		  MasterPolicyNo,	
		  StartDate,		
		  EndDate,		
		  CreatedBy,		
		  InsertDate,		
		  ModifyDate		
         )
                SELECT 'D', 
                       Id,				
					   ProductID,	
					   PlanID,			
					   MasterPolicyNo,	
					   StartDate,		
					   EndDate,		
					   CreatedBy,		
					   InsertDate,		
					   ModifyDate
                FROM DELETED;
     END;
GO
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_D_tblMasterPolicyNo]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_D_tblMasterPolicyNo;
GO
CREATE TRIGGER trg_D_tblMasterPolicyNo ON [dbo].[tblMasterPolicyNo]
FOR DELETE
AS
     BEGIN
         INSERT INTO tblMasterPolicyNo_Audit
         (s_Trans_Type,   
		  Id,				
		  ProductID,	
		  PlanID,			
		  MasterPolicyNo,	
		  StartDate,		
		  EndDate,		
		  CreatedBy,		
		  InsertDate,		
		  ModifyDate		
         )
                SELECT 'D', 
                       Id,				
					   ProductID,	
					   PlanID,			
					   MasterPolicyNo,	
					   StartDate,		
					   EndDate,		
					   CreatedBy,		
					   InsertDate,		
					   ModifyDate
                FROM DELETED;
     END;
GO
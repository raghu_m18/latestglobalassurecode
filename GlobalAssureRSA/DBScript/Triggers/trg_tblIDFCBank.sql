IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_i_tblIDFCBank]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_i_tblIDFCBank;
GO
CREATE TRIGGER trg_i_tblIDFCBank ON [dbo].[tblIDFCBank]
FOR INSERT
AS
     BEGIN
         INSERT INTO tblIDFCBank_Audit
         (s_Trans_Type,   
		  Id,				
		  FirstName,	
		  LastName,			
		  Email,	
		  PolicyStatus,		
		  dt_InserDate,		
		  ModifiedDate,
		  CardLast4Digit,
		  UploadByID,		
		  LastModifiedByID,
		  MobileNoLast4Digit
         )
                SELECT 'I', 
                       Id,				
					   FirstName,	
					   LastName,			
					   Email,	
					   PolicyStatus,		
					   dt_InserDate,		
					   ModifiedDate,
					   CardLast4Digit,
					   UploadByID,		
					   LastModifiedByID,
					   MobileNoLast4Digit
                FROM INSERTED;
     END;
GO
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_U_tblIDFCBank]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_U_tblIDFCBank;
GO
CREATE TRIGGER trg_U_tblIDFCBank ON [dbo].[tblIDFCBank]
FOR UPDATE
AS
     BEGIN
         INSERT INTO tblIDFCBank_Audit
         (s_Trans_Type,   
		  Id,				
		  FirstName,	
		  LastName,			
		  Email,	
		  PolicyStatus,		
		  dt_InserDate,		
		  ModifiedDate,
		  CardLast4Digit,
		  UploadByID,		
		  LastModifiedByID,
		  MobileNoLast4Digit
         )
                SELECT 'U', 
                       Id,				
					   FirstName,	
					   LastName,			
					   Email,	
					   PolicyStatus,		
					   dt_InserDate,		
					   ModifiedDate,
					   CardLast4Digit,
					   UploadByID,		
					   LastModifiedByID,
					   MobileNoLast4Digit
                FROM INSERTED;
         INSERT INTO tblIDFCBank_Audit
         (s_Trans_Type,   
		  Id,				
		  FirstName,	
		  LastName,			
		  Email,	
		  PolicyStatus,		
		  dt_InserDate,		
		  ModifiedDate,
		  CardLast4Digit,
		  UploadByID,		
		  LastModifiedByID,
		  MobileNoLast4Digit
         )
                SELECT 'D', 
                       Id,				
					   FirstName,	
					   LastName,			
					   Email,	
					   PolicyStatus,		
					   dt_InserDate,		
					   ModifiedDate,
					   CardLast4Digit,
					   UploadByID,		
					   LastModifiedByID,
					   MobileNoLast4Digit
                FROM DELETED;
     END;
GO
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_D_tblIDFCBank]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_D_tblIDFCBank;
GO
CREATE TRIGGER trg_D_tblIDFCBank ON [dbo].[tblIDFCBank]
FOR DELETE
AS
     BEGIN
         INSERT INTO tblIDFCBank_Audit
         (s_Trans_Type,   
		  Id,				
		  FirstName,	
		  LastName,			
		  Email,	
		  PolicyStatus,		
		  dt_InserDate,		
		  ModifiedDate,
		  CardLast4Digit,
		  UploadByID,		
		  LastModifiedByID,
		  MobileNoLast4Digit
         )
                SELECT 'D', 
                       Id,				
					   FirstName,	
					   LastName,			
					   Email,	
					   PolicyStatus,		
					   dt_InserDate,		
					   ModifiedDate,
					   CardLast4Digit,
					   UploadByID,		
					   LastModifiedByID,
					   MobileNoLast4Digit
                FROM DELETED;
     END;
GO
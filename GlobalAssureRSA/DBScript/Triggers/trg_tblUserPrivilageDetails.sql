
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_I_tblUserPrivilegeDetail]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_I_tblUserPrivilegeDetail;
GO
CREATE TRIGGER trg_I_tblUserPrivilegeDetail ON [dbo].[tblUserPrivilegeDetail]
FOR INSERT
AS
     BEGIN
         INSERT INTO tblUserPrivilegeDetail_Audit
         (s_Trans_Type, 
          PrivilegeDetailID, 
          UserID, 
          UserName, 
          Email, 
          Contact, 
          DOB, 
          Product, 
          Payment, 
          Plans, 
          Make, 
          Model, 
          CreatedDate, 
          IsValid, 
          CreatedBy, 
          CommisionPercentage, 
          LastModifyDateTime, 
          IsCommision, 
          Mode, 
          LastModifiedBy,
		  IsOnHold
         )
                SELECT 'I', 
                       PrivilegeDetailID, 
                       UserID, 
                       UserName, 
                       Email, 
                       Contact, 
                       DOB, 
                       Product, 
                       Payment, 
                       Plans, 
                       Make, 
                       Model, 
                       CreatedDate, 
                       IsValid, 
                       CreatedBy, 
                       CommisionPercentage, 
                       LastModifyDateTime, 
                       IsCommision, 
                       Mode, 
                       LastModifiedBy,
					   IsOnHold
                FROM INSERTED;
     END;
GO
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_U_tblUserPrivilegeDetail]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_U_tblUserPrivilegeDetail;
GO
CREATE TRIGGER trg_U_tblUserPrivilegeDetail ON [dbo].[tblUserPrivilegeDetail]
FOR UPDATE
AS
     BEGIN
         INSERT INTO tblUserPrivilegeDetail_Audit
         (s_Trans_Type, 
          PrivilegeDetailID, 
          UserID, 
          UserName, 
          Email, 
          Contact, 
          DOB, 
          Product, 
          Payment, 
          Plans, 
          Make, 
          Model, 
          CreatedDate, 
          IsValid, 
          CreatedBy, 
          CommisionPercentage, 
          LastModifyDateTime, 
          IsCommision, 
          Mode, 
          LastModifiedBy,
		  IsOnHold
         )
                SELECT 'U', 
                       PrivilegeDetailID, 
                       UserID, 
                       UserName, 
                       Email, 
                       Contact, 
                       DOB, 
                       Product, 
                       Payment, 
                       Plans, 
                       Make, 
                       Model, 
                       CreatedDate, 
                       IsValid, 
                       CreatedBy, 
                       CommisionPercentage, 
                       LastModifyDateTime, 
                       IsCommision, 
                       Mode, 
                       LastModifiedBy,
					   IsOnHold
                FROM INSERTED;
         INSERT INTO tblUserPrivilegeDetail_Audit
         (s_Trans_Type, 
          PrivilegeDetailID, 
          UserID, 
          UserName, 
          Email, 
          Contact, 
          DOB, 
          Product, 
          Payment, 
          Plans, 
          Make, 
          Model, 
          CreatedDate, 
          IsValid, 
          CreatedBy, 
          CommisionPercentage, 
          LastModifyDateTime, 
          IsCommision, 
          Mode, 
          LastModifiedBy,
		  IsOnHold
         )
                SELECT 'D', 
                       PrivilegeDetailID, 
                       UserID, 
                       UserName, 
                       Email, 
                       Contact, 
                       DOB, 
                       Product, 
                       Payment, 
                       Plans, 
                       Make, 
                       Model, 
                       CreatedDate, 
                       IsValid, 
                       CreatedBy, 
                       CommisionPercentage, 
                       LastModifyDateTime, 
                       IsCommision, 
                       Mode, 
                       LastModifiedBy,
					   IsOnHold
                FROM DELETED;
     END;
GO
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[trg_D_tblUserPrivilegeDetail]')
          AND TYPE IN(N'TR')
)
    DROP TRIGGER [dbo].trg_D_tblUserPrivilegeDetail;
GO
CREATE TRIGGER trg_D_tblUserPrivilegeDetail ON [dbo].[tblUserPrivilegeDetail]
FOR DELETE
AS
     BEGIN
         INSERT INTO tblUserPrivilegeDetail_Audit
         (s_Trans_Type, 
          PrivilegeDetailID, 
          UserID, 
          UserName, 
          Email, 
          Contact, 
          DOB, 
          Product, 
          Payment, 
          Plans, 
          Make, 
          Model, 
          CreatedDate, 
          IsValid, 
          CreatedBy, 
          CommisionPercentage, 
          LastModifyDateTime, 
          IsCommision, 
          Mode, 
          LastModifiedBy,
		  IsOnHold
         )
                SELECT 'D', 
                       PrivilegeDetailID, 
                       UserID, 
                       UserName, 
                       Email, 
                       Contact, 
                       DOB, 
                       Product, 
                       Payment, 
                       Plans, 
                       Make, 
                       Model, 
                       CreatedDate, 
                       IsValid, 
                       CreatedBy, 
                       CommisionPercentage, 
                       LastModifyDateTime, 
                       IsCommision, 
                       Mode, 
                       LastModifiedBy,
					   IsOnHold
                FROM DELETED;
     END;
GO
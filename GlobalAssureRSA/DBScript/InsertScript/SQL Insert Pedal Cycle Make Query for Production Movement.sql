
begin transaction

insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'ATLAS',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'FIREFOX',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'FROG',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'HERCULES',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'HERO',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'MACHCITY',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'MONTRA',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'ROADEO',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Author',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Ariel',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Aprilia',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Falcon Cycles',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Fat City Cycles',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Flying Pigeon',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Flying Scot',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Focus Bikes',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Fram',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Fuji Bikes',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'GT Bicycles',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Gocycle',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Haro Bikes',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Humber',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Ideal Bikes',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Kangaroo',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Kent',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'K2 Sports',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Kross',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Marin Bikes',1,GETDATE(),NULL,NULL,9)
insert into tblMstMake (ProductID,Name,IsValid,CreatedDate,HeroMakeCode,PolicyBazarCode,CompanyId) Values
(10,'Pacific Cycle',1,GETDATE(),NULL,NULL,9)

select * from tblMstMake where ProductID = 10 and CompanyId = 9

rollback transaction

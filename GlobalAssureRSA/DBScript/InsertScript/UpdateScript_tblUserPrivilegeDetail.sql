
SELECT * FROM tblUserPrivilegeDetail WHERE CommisionPercentage IS NULL

SELECT * FROM tblUserPrivilegeDetail WHERE CommisionPercentage IS NOT NULL

UPDATE tblUserPrivilegeDetail SET IsCommision = 0 WHERE CommisionPercentage IS NULL

UPDATE tblUserPrivilegeDetail SET IsCommision = 1 WHERE CommisionPercentage IS NOT NULL
INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('InsuredGender',
 'Male',
 'InsuredGender',
 1,
 GETDATE()
);

INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('InsuredGender',
 'Female',
 'InsuredGender',
 1,
 GETDATE()
);

INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('InsuredGender',
 'Other',
 'InsuredGender',
 1,
 GETDATE()
);

INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('NomineeGender',
 'Male',
 'NomineeGender',
 1,
 GETDATE()
);

INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('NomineeGender',
 'Female',
 'NomineeGender',
 1,
 GETDATE()
);

INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('NomineeGender',
 'Other',
 'NomineeGender',
 1,
 GETDATE()
);

INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('NomineeRelationship',
 'Father',
 'NomineeRelationship',
 1,
 GETDATE()
);

INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('NomineeRelationship',
 'Mother',
 'NomineeRelationship',
 1,
 GETDATE()
);

INSERT INTO tblMstCommonType
(Code,
 Description,
 MasterType,
 IsValid,
 CreatedDateTime
)
VALUES
('NomineeRelationship',
 'Other',
 'NomineeRelationship',
 1,
 GETDATE()
);
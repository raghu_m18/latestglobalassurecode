
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetRoleDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetRoleDetails;
GO
CREATE PROCEDURE usp_GetRoleDetails
(
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	     SELECT Distinct  [Name] as Text, id as Value
	     from tblRoleMaster	 
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetUserID')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetUserID;
GO
CREATE PROCEDURE usp_GetUserID
(
@UserName Varchar(250) = null,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    select Id from AspNetUsers
		where UserName = @UserName
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
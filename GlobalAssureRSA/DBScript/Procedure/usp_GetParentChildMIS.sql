
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetParentChildMIS')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetParentChildMIS;
GO
CREATE PROCEDURE usp_GetParentChildMIS
(
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    Select ROW_NUMBER()  OVER (ORDER BY  a.ID) As SrNo,
		a.Email as [Parent Email ID],
		b.Email as [Child Email ID] 		
		from AspNetUsers a, AspNetUsers b
		where b.MappingUserId = a.Id and a.IsActive = 'true' and b.MappingUserId is not null
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
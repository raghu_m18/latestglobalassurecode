
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_SaveEmailSendLog')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_SaveEmailSendLog;
GO
CREATE PROCEDURE usp_SaveEmailSendLog
(@s_Transaction_Id  VARCHAR(150)=NULL, 
 @n_Email_Config_Id INT, 
 @s_SMTP_User_Name  VARCHAR(150), 
 @s_From_Email      VARCHAR(150), 
 @s_From_Name       VARCHAR(150), 
 @s_Email_To        VARCHAR(1000), 
 @s_Email_BCC       VARCHAR(1000)=NULL, 
 @s_Email_CC        VARCHAR(1000)=NULL, 
 @s_Email_Subject   VARCHAR(1000), 
 @s_Email_Body      VARCHAR(4000)=NULL,
 @s_Attachment      VARCHAR(1000)=NULL, 
 @s_Send_Status     VARCHAR(150), 
 @s_Error           VARCHAR(1000)=NULL, 
 @s_Error_Message   VARCHAR(500)  = NULL OUTPUT
)
--WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
                INSERT INTO tblEmailLog
                (s_Transaction_Id, 
                 n_Email_Config_Id, 
                 s_SMTP_User_Name, 
                 s_From_Email, 
                 s_From_Name, 
                 s_Email_To, 
                 s_Email_BCC, 
                 s_Email_CC, 
                 s_Email_Subject, 
                 s_Email_Body, 
                 s_Attachment, 
                 s_Send_Status, 
                 s_Error
                )
                VALUES
                (@s_Transaction_Id, 
                 @n_Email_Config_Id, 
                 @s_SMTP_User_Name, 
                 @s_From_Email, 
                 @s_From_Name, 
                 @s_Email_To, 
                 @s_Email_BCC, 
                 @s_Email_CC, 
                 @s_Email_Subject, 
                 @s_Email_Body, 
                 @s_Attachment, 
                 @s_Send_Status, 
                 @s_Error
                );
            END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetModelFromMake')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetModelFromMake;
GO
CREATE PROCEDURE usp_GetModelFromMake
(
@MakeID	int = null,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    Select distinct Name from tblMstModel
		where MakeID = @MakeID and IsValid = 1
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
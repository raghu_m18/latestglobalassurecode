
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetVendorCount')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetVendorCount;
GO
CREATE PROCEDURE usp_GetVendorCount
(
@VendorEmail  VARCHAR(100), 
@ServiceType Varchar(50),
@City int,
@State int,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            SELECT COUNT(*)  as VendorCount
            FROM tblMstVendor MV
                 INNER JOIN tblVendorBranchDetails BD ON MV.VendorID = BD.VendorID
                 INNER JOIN tblMstCity MC ON MC.CityID = BD.City
            WHERE MV.VendorEMail = @VendorEmail
                  AND MV.Service_Type = @ServiceType
                  AND BD.City = @City
                  AND BD.State = @State;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetUserRole')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetUserRole;
GO
CREATE PROCEDURE usp_GetUserRole
(
@RoleID int = null,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    select Name,Email from AspNetUsers
		where OtherAdminId = @RoleID
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetPlan')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetPlan;
GO
CREATE PROCEDURE usp_GetPlan
(
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    select Distinct Name,PlanID from tblMstPlan
		where IsValid = 1
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
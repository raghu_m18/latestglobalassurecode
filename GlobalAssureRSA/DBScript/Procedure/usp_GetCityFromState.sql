
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetCityFromState')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetCityFromState;
GO
CREATE PROCEDURE usp_GetCityFromState
(
@StateID	int = null,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    Select distinct City from tblMstCity
		where StateID = @StateID and IsValid = 1
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
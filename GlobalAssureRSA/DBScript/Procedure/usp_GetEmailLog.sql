
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetEmailLog')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetEmailLog;
GO
CREATE PROCEDURE usp_GetEmailLog
(@n_PageIndex     INT          = 1, 
 @n_PageSize      INT          = 25, 
 @dt_FromDate     DATE, 
 @dt_ToDate       DATE, 
 @b_IsDownload    CHAR(1)      = '0', 
 @n_RecordCount   INT OUTPUT, 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            SET NOCOUNT ON;
            SELECT ROW_NUMBER() OVER(
                   ORDER BY n_Email_Log_Id ASC) AS RowNumber, 
                   n_Email_Log_Id, 
                   s_Transaction_Id, 
                   n_Email_Config_Id, 
                   s_SMTP_User_Name, 
                   s_From_Email, 
                   s_From_Name, 
                   s_Email_To, 
                   s_Email_BCC, 
                   s_Email_CC, 
                   s_Email_Subject, 
                   s_Email_Body, 
                   s_Attachment, 
                   s_Send_Status, 
                   s_Error, 
                   dt_Insert_Date
            INTO #Results
            FROM tblEmailLog
            WHERE CAST(dt_Insert_Date AS DATE) >= CAST(@dt_FromDate AS DATE)
                  AND CAST(dt_Insert_Date AS DATE) <= CAST(@dt_ToDate AS DATE);

            /**/

            SELECT @n_RecordCount = COUNT(*)
            FROM #Results;
            PRINT CAST(@dt_ToDate AS DATE);

            /**/

            IF @b_IsDownload = '1'
                BEGIN
                    SELECT
                           s_Transaction_Id as [Transaction Id], 
                           n_Email_Config_Id as [Email Config Id], 
                           s_SMTP_User_Name as  [SMTP User Name], 
                           s_From_Email as [From Email], 
                           s_From_Name as [From Name], 
                           s_Email_To as [Email To], 
                           s_Email_BCC as [Email BCC], 
                           s_Email_CC as [Email CC], 
                           s_Email_Subject as [Email Subject], 
                           s_Email_Body as [Email Body], 
                           s_Attachment as [Attachment], 
                           s_Send_Status as [Send Status], 
                           s_Error as [Error], 
                           dt_Insert_Date as [Insert Date]
                    FROM #Results;
            END;
                ELSE
                BEGIN
                    SELECT n_Email_Log_Id, 
                           s_Transaction_Id, 
                           n_Email_Config_Id, 
                           s_SMTP_User_Name, 
                           s_From_Email, 
                           s_From_Name, 
                           s_Email_To, 
                           s_Email_BCC, 
                           s_Email_CC, 
                           LEFT(s_Email_Subject, 100) AS s_Email_SubjectShort, 
                           s_Email_Subject, 
                           LEFT(s_Email_Body, 100) AS s_Email_BodyShort, 
                           s_Email_Body, 
                           s_Attachment, 
                           s_Send_Status, 
                           s_Error, 
                           dt_Insert_Date
                    FROM #Results
                    WHERE RowNumber BETWEEN(@n_PageIndex - 1) * @n_PageSize + 1 AND(((@n_PageIndex - 1) * @n_PageSize + 1) + @n_PageSize) - 1;
            END;
            DROP TABLE #Results;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
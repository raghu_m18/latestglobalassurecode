
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_DeleteChild')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_DeleteChild;
GO
CREATE PROCEDURE usp_DeleteChild
(
@EmailID varchar(100),
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    update AspNetUsers set
		MappingUserId = NULL		
		 where Email = @EmailID
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
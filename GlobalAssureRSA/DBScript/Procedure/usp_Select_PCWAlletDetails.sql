
CREATE PROCEDURE [dbo].[Select_PCWAlletDetails]

  @Email NVARCHAR(512), 
  @returnCode INT output 

AS 

  BEGIN 

    DECLARE @CreatedById NVARCHAR(256) 

	SELECT @CreatedById = Id 
	FROM   AspNetUsers 
	WHERE  Email = @Email

    SET nocount ON; 

    BEGIN try 

     BEGIN TRAN; 

        CREATE TABLE #temp
		(
			Description      VARCHAR(100) NULL, 
			Amount		     DECIMAL(18,2) NULL,
			Symbol           VARCHAR(10) NULL,
			CreatedDateTime  DATETIME
		)

		INSERT INTO #temp

		SELECT 'Amount Added' AS Description,ufa.Amount,'+' AS Symbol,ufa.CreatedDateTime FROM tblUserFloatAmount ufa
		WHERE ufa.UserID = @CreatedById and IsValid = 1
		UNION ALL
		SELECT trn.CertificateNo AS Description,opd.Amount,'-' AS Symbol,trn.CreatedDate FROM tblTransaction trn inner join tblOnlinePaymentDetail opd 
		ON trn.OnlinePaymentID = opd.OnlinePaymentID
		WHERE trn.UserID = @CreatedById and trn.StatusID = 1

		SELECT TOP 20 * FROM #temp ORDER BY CreatedDateTime DESC

      COMMIT TRAN; 

      SET @returnCode = 1 

    END try 

    BEGIN catch 

      IF @@TRANCOUNT>0 

      ROLLBACK TRANSACTION; 

      SELECT * 

      FROM   errorlog 

      INSERT INTO errorlog 

                  ( 

                              log, 

                              logdatetime 

                  ) 

                  VALUES 

                  ( 

                              Error_message(), 

                              Getdate() 

                  ) 

      SET @returnCode = 0 

    END catch 

    DROP TABLE #temp 

  END


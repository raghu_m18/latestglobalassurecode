
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetPlanDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetPlanDetails;
GO
CREATE PROCEDURE usp_GetPlanDetails
(@Plan           VARCHAR(250) = NULL,   
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            IF(@Plan != '--Select--')               
                BEGIN
                    select Distinct 
					PlanID,
					Name,
					ProductID,
					Amount
				    from tblMstPlan
					Where Name = @Plan
					and IsValid = 1
            END;
                ELSE
                BEGIN
                    select Distinct 
					PlanID,
					Name,
					ProductID,
					Amount
				    from tblMstPlan
					Where IsValid = 1
            END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
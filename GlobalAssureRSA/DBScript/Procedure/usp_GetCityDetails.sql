
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetCityDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetCityDetails;
GO
CREATE PROCEDURE usp_GetCityDetails
(
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    select Distinct City from tblMstCity
		where IsValid = 1
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
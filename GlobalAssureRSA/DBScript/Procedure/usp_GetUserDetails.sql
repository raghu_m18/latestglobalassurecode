
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetUserDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetUserDetails;
GO
CREATE PROCEDURE usp_GetUserDetails
(
@Username varchar(100),
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	     Select AP.Email,PD.PrivilegeDetailID,PD.Plans,PD.CommisionPercentage,PD.IsCommision,PD.Mode,PD.IsValid from tblUserPrivilegeDetail PD
		 Inner Join AspNetUsers AP on PD.UserID = AP.Id
		 where AP.Email = @Username
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_CancelCerti')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_CancelCerti;
GO
CREATE PROCEDURE usp_CancelCerti
(@n_TransId       INT, 
 @s_CertificateNo VARCHAR(250), 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
                UPDATE tblTransaction
                  SET 
                      StatusID = 14
                WHERE TransactionID = @n_TransId
                      AND CertificateNo = @s_CertificateNo;
            END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
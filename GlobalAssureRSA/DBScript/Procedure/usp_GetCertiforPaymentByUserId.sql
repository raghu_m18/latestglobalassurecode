USE [GlobalAssureRSA]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCertiforPaymentByUserId]    Script Date: 2021-01-29 3:42:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetCertiforPaymentByUserId]
(@s_UserId        VARCHAR(250), 
 @s_CertificateNo VARCHAR(250) = '', 
 @s_FromDate VARCHAR(250) = '', 
 @s_ToDate VARCHAR(250) = '', 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
--WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
                SELECT trn.*,MP.Total
                FROM tblTransaction trn
                     INNER JOIN AspNetUsers AU ON AU.Id = trn.UserID
					 INNER JOIN tblMstPlan MP on MP.PlanID=trn.PlanID
                WHERE AU.Id = @s_UserId
                      AND (trn.CertificateNo = @s_CertificateNo OR @s_CertificateNo = '')
					  AND (trn.CreatedDate >= (SELECT CONVERT(datetime, @s_FromDate, 120)) OR @s_FromDate = '')
					  AND (trn.CreatedDate <= (SELECT CONVERT(datetime, @s_ToDate, 120)) OR @s_ToDate = '')
					  AND trn.StatusID = 2
                ORDER BY trn.CertificateNo;
            END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
USE [GlobalAssureRSA]
GO
/****** Object:  StoredProcedure [dbo].[Insert_UserPermission]    Script Date: 6/9/2020 8:07:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[Insert_UserPermission] 

  @CreatedBy NVARCHAR(512), 

  @datatable USERPERMISSIONTYPE readonly, 

  @returnCode INT output 

AS 

  BEGIN 

    DECLARE @CreatedById NVARCHAR(256) 

    SELECT @CreatedById=id 

    FROM   aspnetusers 

    WHERE  username=@CreatedBy 

    SET nocount ON; 

    BEGIN try 

      BEGIN TRAN; 

      CREATE TABLE #temp 

                   ( 

                                email								VARCHAR(100), 

                                dob									DATETIME, 

                                NAME								VARCHAR(100), 

                                contact								VARCHAR(100), 

                                isactive							BIT, 

                                isadmin								BIT, 

                                prefix								VARCHAR(10), 

                                dealer								VARCHAR(500), 

                                pickup_point						VARCHAR(500), 

                                is_bulkcertificate_menu				BIT, 

                                accountno							VARCHAR(100) NULL, 

                                ifsc_code							VARCHAR(100) NULL, 

                                beneficiary_name					VARCHAR(100) NULL, 

                                bankbranchname						VARCHAR(100) NULL, 

                                gstno								VARCHAR(100) NULL, 

                                panno								VARCHAR(100) NULL, 

                                agreementreceived					BIT, 

                                payout_percentage					VARCHAR(100), 

                                tieupcompanyid						NUMERIC NULL, 

                                imdcode								VARCHAR(100) NULL,

								CDAMOUNT							decimal(18,2),

								BranchCode							VARCHAR(50) NULL,

								ContactPersonName					VARCHAR(250) NULL,

								ContactPersonNo						VARCHAR(250) NULL,

								ContactPersonCommisionAmount		DECIMAL(18, 2) NULL,

								RiskStartNoOfDays					INT NOT NULL,

								IsDownloadCertificate				BIT NOT NULL

                   ) 

      CREATE TABLE #tempforprivilege 

                   ( 

                                NAME								VARCHAR(100) NULL, 

                                dob									DATETIME NULL, 

                                email								VARCHAR(100) NULL, 

                                contact								VARCHAR(100) NULL, 

                                product								VARCHAR(100) NULL, 

                                payment								VARCHAR(100) NULL, 

                                [PLAN]								VARCHAR(MAX) NULL, 

                                make								VARCHAR(100) NULL, 

                                model								VARCHAR(100) NULL, 

                                isactive							BIT NULL, 

                                isadmin								BIT NULL, 

                                prefix								VARCHAR(10) NULL, 

                                dealer								VARCHAR(500) NULL, 

                                pickup_point						VARCHAR(500) NULL, 

                                is_bulkcertificate_menu				BIT, 

                                accountno							VARCHAR(100) NULL, 

                                ifsc_code							VARCHAR(100) NULL, 

                                beneficiary_name					VARCHAR(100) NULL, 

                                bankbranchname						VARCHAR(100) NULL, 

                                gstno								VARCHAR(100) NULL, 

                                panno								VARCHAR(100) NULL, 

                                agreementreceived					BIT, 

                                payout_percentage					VARCHAR(100), 

                                tieupcompanyid						NUMERIC NULL, 

                                imdcode								VARCHAR(100) NULL,
								
							    commisionpercentage					DECIMAL(18,2) NULL,
								
								iscommision							BIT NULL, 

								mode								VARCHAR(50) NULL,

								BranchCode							VARCHAR(50) NULL,

								ContactPersonName					VARCHAR(250) NULL,

								ContactPersonNo						VARCHAR(250) NULL,

								ContactPersonCommisionAmount		DECIMAL(18, 2) NULL,

								RiskStartNoOfDays					INT NOT NULL,

								IsDownloadCertificate				BIT NOT NULL

                   ) 

      INSERT INTO #tempforprivilege 

                  ( 

                              NAME, 

                              dob, 

                              email, 

                              contact, 

                              product, 

                              payment, 

                              [PLAN], 

                              make, 

                              model, 

                              isactive, 

                              isadmin, 

                              prefix, 

                              dealer, 

                              pickup_point, 

                              is_bulkcertificate_menu, 

                              accountno, 

                              ifsc_code, 

                              beneficiary_name, 

                              bankbranchname, 

                              gstno, 

                              panno, 

                              agreementreceived, 

                              payout_percentage, 

                              tieupcompanyid, 

                              imdcode,
							  
							  commisionpercentage,
							  
							  iscommision,

							  mode,

							  BranchCode,					
							  
							  ContactPersonName,			
							  
							  ContactPersonNo,				
							  
							  ContactPersonCommisionAmount,

							  RiskStartNoOfDays,

							  IsDownloadCertificate

                  ) 

      SELECT    h.NAME, 

                h.dob, 

                h.email, 

                h.contact, 

                h.product, 

                h.payment, 

                h.[PLAN] , 

                h.make, 

                h.model, 

                h.isactive, 

                h.isadmin, 

                h.prefix, 

                h.dealer, 

                h.pickup_point, 

                h.is_bulkcertificate_menu, 

                h.accountno, 

                h.ifsc_code, 

                h.beneficiary_name, 

                h.bankbranchname, 

                h.gstno, 

                h.panno, 

                h.agreementreceived, 

                h.payout_percentage, 

                h.tieupcompanyid, 

                h.imdcode,

				h.commisionpercentage,

				h.iscommision,

				h.mode,

				h.branchcode,					
				
				h.contactpersonname,			
				
				h.contactpersoncontactno,				
				
				h.contactpersoncommisionAmount,

				h.RiskStartNoOfDays,

				h.IsDownloadCertificate


      FROM      @datatable H 

      LEFT JOIN aspnetusers M 

      ON        h.email =m.email 

      WHERE     m.username IS NULL 

      AND       h.email IS NOT NULL 

      AND       h.email!='' 

    

	  INSERT INTO #temp 

      SELECT DISTINCT h.email, 

                      h.dob, 

                      h.NAME, 

                      h.contact, 

                      h.isactive, 

                      h.isadmin, 

                      h.prefix , 

                      h.dealer, 

                      h.pickup_point, 

                      h.is_bulkcertificate_menu, 

                      h.accountno, 

                      h.ifsc_code, 

                      h.beneficiary_name, 

                      h.bankbranchname, 

                      h.gstno, 

                      h.panno, 

                      h.agreementreceived, 

                      h.payout_percentage, 

                      h.tieupcompanyid, 

                      h.imdcode,

					  h.CDAMOUNT,
					  
					  h.branchcode,					
				
					  h.contactpersonname,			
					  
					  h.contactpersoncontactno,				
					  
					  h.contactpersoncommisionAmount,

					  h.RiskStartNoOfDays,

					  h.IsDownloadCertificate


      FROM            @datatable H 

      LEFT JOIN       aspnetusers M 

      ON              h.email =m.email 

      WHERE           m.username IS NULL 

      AND             h.email IS NOT NULL 

      AND             h.email!='' 

      INSERT INTO aspnetusers 

                  ( 

                              email, 

                              id, 

                              emailconfirmed, 

                              passwordhash, 

                              securitystamp, 

                              phonenumber, 

                              phonenumberconfirmed, 

                              twofactorenabled, 

                              lockoutenabled, 

                              accessfailedcount, 

                              username, 

                              dob, 

                              isadmin, 

     isactive, 

                              NAME, 

                              prefix, 

                              dealername, 

                              pickuppoint, 

                              createddate, 

                              isbulkcertificationmenu, 

                              accountno, 

                          ifsc_code, 

                   beneficiaryname, 

                              bankbranchname, 

                              gstno, 

                              panno, 

                              isagreementreceived, 

                              payoutpercentage, 

                              tieupcompanyid, 

                              imdcode,

							  TotalCDAmount ,
							  TotalCDUsedAmount,
							  TotalCDBalanceAmount,

							  OtherAdminId,

							  BranchCode,

							  ContactPersonName,

							  ContactPersonNo,

							  ContactPersonCommisionAmount,

							  RiskStartCount,

							  IsDownloadCertificate

                  ) 

      SELECT Ltrim(Rtrim(h.email)), 

             Newid(), 

             1, 

             'ACW4yfCCHVBM+KsZeCqq/9JEc60+fOZ8jolwe1PkcVzuauisevfodbJMsqOpUCUJLA==', 

             Newid(), 

             Ltrim(Rtrim(h.contact)), 

             0,0,0,0, 

             Ltrim(Rtrim(h.email)), 

             h.dob , 

             h.isadmin, 

             h.isactive, 

             Ltrim(Rtrim(h.NAME)), 

             h.prefix, 

             Ltrim(Rtrim(h.dealer)), 

             Ltrim(Rtrim(h.pickup_point)), 

             Getdate(), 

             h.is_bulkcertificate_menu, 

             Ltrim(Rtrim(h.accountno)), 

             Ltrim(Rtrim(h.ifsc_code)), 

             Ltrim(Rtrim(h.beneficiary_name)), 

             ltrim(rtrim(h.bankbranchname)), 

             ltrim(rtrim(h.gstno)), 

             ltrim(rtrim(h.panno)), 

             h.agreementreceived, 

             ltrim(rtrim(h.payout_percentage)), 

             h.tieupcompanyid, 

             ltrim(rtrim(h.imdcode)),

			 h.CDAMOUNT,
			 0,
			 h.CDAMOUNT,
			 
			 '109',

			 ltrim(rtrim(h.BranchCode)),

			 ltrim(rtrim(h.ContactPersonName)),

			 ltrim(rtrim(h.ContactPersonNo)),

			 h.ContactPersonCommisionAmount,

			 h.RiskStartNoOfDays,

			 h.IsDownloadCertificate
			 

      FROM   #temp h 

      INSERT INTO tblpasswordhistory 

                  ( 

                              userid, 

                              password, 

                              date 

                  ) 

      SELECT DISTINCT 

                      ( 

                             SELECT id 

                             FROM   aspnetusers 

                             WHERE  email = h.email) , 

                      'Assist@123', 

                      Getdate()-365 

      FROM            #temp H 

      WHERE           h.email IS NOT NULL 

      AND             h.email != '' 





	  ;WITH CTE(  product,  payment,  make, model, commisionpercentage, iscommision, mode,

                              email, DataItem, [PLAN]) AS

        (

    SELECT

         product,  payment,  make, model, commisionpercentage, iscommision, mode,

                              email,

        CAST( LEFT([PLAN], CHARINDEX(',', [PLAN] + ',') - 1)+'' as varchar(MAX)),

        STUFF([PLAN], 1, CHARINDEX(',', [PLAN] + ','), '')

    FROM #tempforprivilege



    UNION all

     SELECT

         product,  payment,  make, model, commisionpercentage, iscommision, mode,

                              email,

       CAST( LEFT([PLAN], CHARINDEX(',', [PLAN] + ',') - 1)+'' as varchar(MAX)),

        STUFF([PLAN], 1, CHARINDEX(',', [PLAN] + ','), '')

    FROM CTE

    WHERE

        [PLAN] > ''



     )





	 

	 



      INSERT INTO tbluserprivilegedetail 

                  ( 

                              userid, 

                              product, 

                              payment, 

                              plans, 

                              make, 

                              model, 

                              createddate, 

                              isvalid, 

                              createdby,

							  commisionpercentage,

							  iscommision,

							  mode

                  ) 

      SELECT  

                      ( 

                             SELECT top 1 id 

                             FROM   aspnetusers 

                             WHERE  email = h.email), 

                      Ltrim(Rtrim(h.product)), 

                      Ltrim(Rtrim(h.payment)), 

                      Ltrim(Rtrim(h.DataItem)), 

                      Ltrim(Rtrim(h.make)), 

                      Ltrim(Rtrim(h.model)), 

                      Getdate(), 

                      1, 

                      @CreatedById,
					  
					  Ltrim(Rtrim(h.commisionpercentage)),

					  Ltrim(Rtrim(h.iscommision)),

					  Ltrim(Rtrim(UPPER(h.mode))) 

      FROM            CTE h 

      WHERE           h.email IS NOT NULL  AND  h.email != '' 

	 order by h.email



      INSERT INTO tbluserauditlog 

                  ( 

                              NAME, 

 email, 

            dob, 

      contact, 

                              dealername, 

                              pickuppoint, 

                              auditdatetime, 

                              auditby 

                  ) 

      SELECT Ltrim(Rtrim(h.NAME)), 

             Ltrim(Rtrim(h.email)), 

             h.dob, 

             Ltrim(Rtrim(h.contact)), 

             Ltrim(Rtrim(h.dealer)), 

             Ltrim(Rtrim(h.pickup_point)), 

             Getdate(), 

             @CreatedById 

      FROM   #temp H 

      COMMIT TRAN; 

      SET @returnCode = 1 

    END try 

    BEGIN catch 

      IF @@TRANCOUNT>0 

      ROLLBACK TRANSACTION; 

      SELECT * 

      FROM   errorlog 

      INSERT INTO errorlog 

                  ( 

                              log, 

                              logdatetime 

                  ) 

                  VALUES 

                  ( 

                              Error_message(), 

                              Getdate() 

                  ) 

      SET @returnCode = 0 

    END catch 

    DROP TABLE #temp 

    DROP TABLE #tempforprivilege 

  END


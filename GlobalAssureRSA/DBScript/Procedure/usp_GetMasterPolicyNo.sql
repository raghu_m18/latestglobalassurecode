
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetMasterPolicyNo')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetMasterPolicyNo;
GO
CREATE PROCEDURE usp_GetMasterPolicyNo
(
 @ProductID         NUMERIC(18, 0)   = NULL,
 @PlanID			NUMERIC(18, 0)   = NULL,
 @CreatedDate		DATETIME		 = NULL,
 @s_Error_Message	VARCHAR(500)     = NULL OUTPUT
)
--WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
			
			SELECT 
			MasterPolicyNo 
			FROM tblMasterPolicyNo
			WHERE 
			ProductID = ISNULL(@ProductID, 0) and
			PlanID = ISNULL(@PlanID, 0) and 
			StartDate <= @CreatedDate and ISNULL(EndDate, GETDATE()) >= @CreatedDate

            END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
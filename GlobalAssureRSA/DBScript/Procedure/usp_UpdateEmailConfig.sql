
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_UpdateEmailConfig')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_UpdateEmailConfig;
GO
CREATE PROCEDURE usp_UpdateEmailConfig
(@n_Config_Id        INT, 
 @s_SMTP_Host        VARCHAR(150), 
 @s_SMTP_Port        VARCHAR(10), 
 @s_SMTP_User_Name   VARCHAR(150), 
 @s_SMTP_Password    VARCHAR(250), 
 @s_From_Email       VARCHAR(150), 
 @s_From_Name        VARCHAR(150), 
 @n_SecureSMTP       BIT, 
 @n_OrderNo          INT, 
 @n_EmailLimitCount  INT          = 0, 
 @n_SMSTriggerCount  INT          = 0, 
 @n_Status           TINYINT, 
 @s_Last_Modify_User VARCHAR(250), 
 @s_Error_Message    VARCHAR(500) = NULL OUTPUT
)
--WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
                UPDATE tblEmailConfig
                  SET 
                      s_SMTP_Host = @s_SMTP_Host, 
                      s_SMTP_Port = @s_SMTP_Port, 
                      s_SMTP_User_Name = @s_SMTP_User_Name, 
                      s_SMTP_Password = @s_SMTP_Password, 
                      s_From_Email = @s_From_Email, 
                      s_From_Name = @s_From_Name, 
                      n_SecureSMTP = @n_SecureSMTP, 
                      n_OrderNo = @n_OrderNo, 
                      n_EmailLimitCount = @n_EmailLimitCount, 
                      n_SMSTriggerCount = @n_SMSTriggerCount, 
                      n_Status = @n_Status, 
                      s_Last_Modify_User = @s_Last_Modify_User, 
                      dt_Modifiy_Date = GETDATE()
                WHERE n_Email_Config_Id = @n_Config_Id;
            END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
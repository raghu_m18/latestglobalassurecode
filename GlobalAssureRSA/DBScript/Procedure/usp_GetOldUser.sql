
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetOldUser')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetOldUser;
GO
CREATE PROCEDURE usp_GetOldUser
(
@OldUser varchar(250) = null,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    select Name,Email,UserName from AspNetUsers
		where UserName = @OldUser
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
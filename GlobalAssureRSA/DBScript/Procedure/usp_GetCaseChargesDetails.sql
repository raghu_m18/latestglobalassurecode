
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetCaseChargesDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetCaseChargesDetails;
GO
CREATE PROCEDURE usp_GetCaseChargesDetails
(
 @CompanyID       INT         = '',
 @RefNo           VARCHAR(50) = NULL,
 @FromDate        DATETIME, 
 @ToDate          DATETIME, 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY

		IF ISNULL(@RefNo, '') <> ''
		BEGIN
		
			SELECT a.id AS AssistID, 
                   a.RefNo AS RefNo, 
                   a.CreatedDate AS CloseTime, 
                   wfs.STATUS AS STATUS, 
                   ct.Description AS Remark,
				   mct.Description AS CompanyName
            FROM tblAssistDetails a
                 INNER JOIN tblMstWorkFlowStatus wfs ON a.StatusId = wfs.StatusID
                 LEFT JOIN tblMstCommonType ct ON CAST(a.Remark1 AS INT) = ct.CommonTypeID
				 LEFT JOIN tblMstCommonType mct ON a.AssistCompanyID = mct.CommonTypeID
            WHERE (a.AssistCompanyID = @CompanyID OR @CompanyID = '')
				  AND a.RefNo = @RefNo
			      AND TRY_CONVERT(DATE, a.CreatedDate) >= TRY_CONVERT(DATE, @FromDate)
                  AND TRY_CONVERT(DATE, a.CreatedDate) <= TRY_CONVERT(DATE, @ToDate)
                  AND a.StatusID = 10 order by a.CreatedDate desc;
        
		END
		ELSE
		BEGIN
		
			SELECT a.id AS AssistID, 
                   a.RefNo AS RefNo, 
                   a.CreatedDate AS CloseTime, 
                   wfs.STATUS AS STATUS, 
                   ct.Description AS Remark,
				   mct.Description AS CompanyName
            FROM tblAssistDetails a
                 INNER JOIN tblMstWorkFlowStatus wfs ON a.StatusId = wfs.StatusID
                 LEFT JOIN tblMstCommonType ct ON CAST(a.Remark1 AS INT) = ct.CommonTypeID
				 LEFT JOIN tblMstCommonType mct ON a.AssistCompanyID = mct.CommonTypeID
            WHERE (a.AssistCompanyID = @CompanyID OR @CompanyID = '')
				  AND TRY_CONVERT(DATE, a.CreatedDate) >= TRY_CONVERT(DATE, @FromDate)
                  AND TRY_CONVERT(DATE, a.CreatedDate) <= TRY_CONVERT(DATE, @ToDate)
                  AND a.StatusID = 10 order by a.CreatedDate desc;
		
		END            

        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
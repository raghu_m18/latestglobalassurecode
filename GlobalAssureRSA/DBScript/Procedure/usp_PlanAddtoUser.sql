
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_PlanAddtoUser')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_PlanAddtoUser;
GO
CREATE PROCEDURE usp_PlanAddtoUser
(@s_UserId        VARCHAR(250), 
 @s_Plans         VARCHAR(250), 
 @s_CreatedBy     VARCHAR(250), 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            IF NOT EXISTS
            (
                SELECT 1
                FROM tblUserPrivilegeDetail
                WHERE UserID = @s_UserId
                      AND Plans = @s_Plans
            )
                BEGIN
                    INSERT INTO [dbo].[tblUserPrivilegeDetail]
                    ([UserID], 
                     [Product], 
                     [Payment], 
                     [Plans], 
                     [Make], 
                     [Model], 
                     [CreatedDate], 
                     [IsValid], 
                     [CreatedBy]
                    )
                    VALUES
                    (@s_UserId, 
                     'ALL', 
                     'ALL', 
                     @s_Plans, 
                     'ALL', 
                     'ALL', 
                     GETDATE(), 
                     1, 
                     @s_CreatedBy
                    );
            END;
                ELSE
                SELECT @s_Error_Message = 'Plan: ' + @s_Plans + ' already exist with this user';
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
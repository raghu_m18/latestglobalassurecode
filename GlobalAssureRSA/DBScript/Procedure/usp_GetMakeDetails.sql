
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetMakeDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetMakeDetails;
GO
CREATE PROCEDURE usp_GetMakeDetails
(
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	    Select  distinct Name,MakeID from tblMstMake
		where IsValid = 1
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
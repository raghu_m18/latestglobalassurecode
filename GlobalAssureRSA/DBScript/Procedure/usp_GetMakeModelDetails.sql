
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetMakeModelDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetMakeModelDetails;
GO
CREATE PROCEDURE usp_GetMakeModelDetails
(@Make            VARCHAR(250) = NULL, 
 @Model           VARCHAR(250) = NULL, 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            IF(@Make != '--Select--'
               AND @Model != '--Select--')
                BEGIN
                    SELECT DISTINCT 
                           mk.MakeID, 
                           mk.ProductID, 
                           mk.Name AS MakeName, 
                           md.ModelID, 
                           md.Name AS ModelName
                    FROM tblMstModel md
                         LEFT JOIN tblMstMake mk ON md.MakeID = mk.MakeID
                    WHERE mk.Name = @Make AND md.Name = @Model and md.IsValid = 1;
            END;
                ELSE
                IF(ISNULL(@Make, '') != '--Select--')
                    BEGIN
                        SELECT DISTINCT 
                               mk.MakeID, 
                               mk.ProductID, 
                               mk.Name AS MakeName, 
                               md.ModelID, 
                               md.Name AS ModelName
                        FROM tblMstMake mk
                             INNER JOIN tblMstModel md ON mk.MakeID = md.MakeID
                        WHERE mk.Name = @Make and md.IsValid = 1;
                END;
                    ELSE
                    IF(ISNULL(@Model, '') != '--Select--')
                        BEGIN
                            SELECT DISTINCT 
                                   mk.MakeID, 
                                   mk.ProductID, 
                                   mk.Name AS MakeName, 
                                   md.ModelID, 
                                   md.Name AS ModelName
                            FROM tblMstMake mk
                                 INNER JOIN tblMstModel md ON mk.MakeID = md.MakeID
                            WHERE md.Name = @Model and md.IsValid = 1;
                    END;
                        ELSE
                        BEGIN
                            SELECT DISTINCT 
                                   mk.MakeID, 
                                   mk.ProductID, 
                                   mk.Name AS MakeName, 
                                   md.ModelID, 
                                   md.Name AS ModelName
                            FROM tblMstMake mk
                                 INNER JOIN tblMstModel md ON mk.MakeID = md.MakeID and md.IsValid = 1;
                    END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_UpdateCommision')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_UpdateCommision;
GO
CREATE PROCEDURE usp_UpdateCommision
(@PrivilageID         INT          = NULL, 
 @Mode                VARCHAR(30)  = NULL, 
 @CommisionPercentage DECIMAL      = NULL, 
 @IsCommision         INT          = NULL, 
 @Username                VARCHAR(30)  = NULL, 
 @s_Error_Message     VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            UPDATE tblUserPrivilegeDetail
              SET 
                  Mode = @Mode, 
                  CommisionPercentage = @CommisionPercentage, 
                  IsCommision = @IsCommision,
				  LastModifiedBy = @Username,
				  LastModifyDateTime = GETDATE()
            WHERE PrivilegeDetailID = @PrivilageID;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
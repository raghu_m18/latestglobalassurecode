
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_UpdatePaymentStatus')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_UpdatePaymentStatus;
GO
CREATE PROCEDURE usp_UpdatePaymentStatus
(@n_TransId       INT, 
 @s_CertificateNo VARCHAR(250), 
 @s_PayRemarks Varchar(250) = null,
 @n_TotalAmount   DECIMAL(18, 2), 
 @s_UserId        VARCHAR(250), 
 @s_Error_Message VARCHAR(500)   = NULL OUTPUT
)
--WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
                BEGIN TRAN;
                UPDATE tblTransaction
                  SET 
                      StatusID = 1,
					  PaymentRemarks = @s_PayRemarks,
					  PaymentReceivedDate=GETDATE()
                WHERE TransactionID = @n_TransId
                      AND CertificateNo = @s_CertificateNo;

                UPDATE AspNetUsers
                  SET 
                      PaymentReceivedAmount = (ISNULL(PaymentReceivedAmount,0) + @n_TotalAmount)
                WHERE Id = @s_UserId;
                COMMIT;
            END;
        END TRY
        BEGIN CATCH
            ROLLBACK;
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetAssistDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetAssistDetails;
GO
CREATE PROCEDURE usp_GetAssistDetails
(
 @AssistID        INT, 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY

	  SELECT 
	   ad.id AS AssistID,	 
	   ad.SummaryDetails AS Summary,
       ad.RefNo AS RefNo, 
       ad.CreatedDate AS CreatedDateTime, 
       au.UserName AS LoginUserName, 
       inc.IncidentDetail AS IncidentDetail, 
	   ven.VendorName AS VendorName,
       vbd.BranchName AS BranchName,
	   ad.Issue AS CustomerMobileNo,
       wfs.STATUS AS Status, 
       ct.Description AS Action,
	   ad.Address AS Location,
	   ad.Landmark AS Landmark,
	   ad.PinCode AS PinCode
     FROM tblAssistDetails_Audit ad
     INNER JOIN AspNetUsers au ON ad.UserID = au.Id
     LEFT JOIN tblMstIncidentDetail inc ON ad.IncidentDetailid = inc.Detailid
     LEFT JOIN tblVendorBranchDetails vbd ON ad.VendorId = vbd.BranchID
	 LEFT JOIN tblMstVendor ven ON vbd.VendorID = ven.VendorID
     INNER JOIN tblMstWorkFlowStatus wfs ON ad.StatusId = wfs.StatusID
     LEFT JOIN tblMstCommonType ct ON CAST(ad.Remark1 AS INT) = ct.CommonTypeID
     WHERE ad.id = @AssistID and (ad.s_Trans_Type = 'I' or ad.s_Trans_Type = 'U') 
	 ORDER BY ad.CreatedDate;   
	 
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetParentDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetParentDetails;
GO
CREATE PROCEDURE usp_GetParentDetails
(
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	     SELECT Distinct Email as Text, id as Value
	     from AspNetUsers	 
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
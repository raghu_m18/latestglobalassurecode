
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetChildDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetChildDetails;
GO
CREATE PROCEDURE usp_GetChildDetails
(
@Email varchar(100),
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	     SELECT Distinct Email,[Name]
	     from AspNetUsers
		 where Email LIKE @Email + '%'
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
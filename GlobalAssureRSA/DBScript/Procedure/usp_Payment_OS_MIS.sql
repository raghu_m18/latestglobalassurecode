
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_Payment_OS_MIS')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_Payment_OS_MIS;
GO
CREATE PROCEDURE usp_Payment_OS_MIS
(
 @s_UserId          VARCHAR(250)   = NULL,
 @dt_FromDate		DATETIME       = NULL,
 @dt_Todate			DATETIME       = NULL,
 @s_Error_Message	VARCHAR(500)   = NULL OUTPUT
)
--WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
                BEGIN TRAN;		
				
CREATE TABLE #temptblTransaction
		(
			UserID			NVARCHAR(128)	NULL, 
			IssueDate		DATETIME		NULL, 
			ProductID		NUMERIC(18, 0)  NULL, 
			PlanID			NUMERIC(18, 0)  NULL, 
			StatusID		INT				NULL
		)

		insert into #temptblTransaction  (UserID,IssueDate,ProductID,PlanID,StatusID) 
		select trn.UserID,trn.IssueDate,trn.ProductID,trn.PlanID,trn.StatusID from tblTransaction trn where 
		TRY_CONVERT(DATE,trn.IssueDate) >= TRY_CONVERT(DATE,@dt_FromDate) and  
		TRY_CONVERT(DATE,trn.IssueDate) <= TRY_CONVERT(DATE,@dt_Todate)
               
			   
CREATE TABLE #temp
		(
			AgentName				 VARCHAR(100)  NULL, 
			AgentMailID				 VARCHAR(100)  NULL, 
			ContactPersonName		 VARCHAR(100)  NULL, 
			ContactPersonNo			 VARCHAR(100)  NULL,
			fifteendays				 Decimal(18,2) NULL, 
			thirtydays				 Decimal(18,2) NULL,
			sixtydays				 Decimal(18,2) NULL, 
			Morethansixtydays		 Decimal(18,2) NULL,
			TotalAmount				 Decimal(18,2) NULL,

			SumFifteenDays           Decimal(18,2) NULL, 
			SumThirtyDays            Decimal(18,2) NULL,
			SumSixtyDays             Decimal(18,2) NULL, 
			SumMoreThanSixtyDays     Decimal(18,2) NULL,
			SumTotalAmount           Decimal(18,2) NULL
		)


		insert into #temp  (AgentName,AgentMailID,ContactPersonName,ContactPersonNo)  
		--select Name,Email,ContactPersonName,ContactPersonNo from AspNetUsers where IsActive = 1 
		select Name,Email,ContactPersonName,ContactPersonNo from AspNetUsers where (IsActive = 1 or IsActive is null) and (IsLocked = 0 or IsLocked is null)

-----------------------------------------
SET @dt_Todate = GETDATE()-1
-----------------------------------------

--#temp15days
CREATE TABLE #temp15days
(
AgentMailID		     VARCHAR(100) NULL, 
fifteendays          Decimal(18,2) NULL 
)
	
insert into  #temp15days (AgentMailID ,fifteendays)
select au.Email,SUM(MP.Total)  from #temptblTransaction temptrn inner join tblMstPlan MP on temptrn.PlanID=MP.PlanID
inner join AspNetUsers au on au.Id= temptrn.UserID
where temptrn.StatusID = 2 and (temptrn.ProductID != 11 and temptrn.ProductID != 12 or temptrn.ProductID IS NULL) and
TRY_CONVERT(DATE,temptrn.IssueDate) >= TRY_CONVERT(DATE,@dt_Todate-15) and  
TRY_CONVERT(DATE,temptrn.IssueDate) <= TRY_CONVERT(DATE,@dt_Todate)  group by au.Email

UPDATE #temp
SET
fifteendays = tc.fifteendays    
FROM 
#temp t
INNER JOIN #temp15days tc
ON t.AgentMailID = tc.AgentMailID


DECLARE @dt DATETIME;
SET @dt = @dt_Todate-15
SET @dt_Todate = @dt-1
-----------------------------------------

--#temp30days
CREATE TABLE #temp30days
(
AgentMailID		     VARCHAR(100) NULL, 
thirtydays           Decimal(18,2) NULL
)
	
insert into  #temp30days (AgentMailID ,thirtydays)
select au.Email,SUM(MP.Total)  from #temptblTransaction temptrn inner join tblMstPlan MP on temptrn.PlanID=MP.PlanID
inner join AspNetUsers au on au.Id= temptrn.UserID
where temptrn.StatusID = 2 and (temptrn.ProductID != 11 and temptrn.ProductID != 12 or temptrn.ProductID IS NULL) and
TRY_CONVERT(DATE,temptrn.IssueDate) >= TRY_CONVERT(DATE,@dt_Todate-15) and  
TRY_CONVERT(DATE,temptrn.IssueDate) <= TRY_CONVERT(DATE,@dt_Todate)  group by au.Email

UPDATE #temp
SET
thirtydays = tc.thirtydays    
FROM 
#temp t
INNER JOIN #temp30days tc
ON t.AgentMailID = tc.AgentMailID


SET @dt = @dt_Todate-15
SET @dt_Todate = @dt-1
-----------------------------------------

--#temp60days
CREATE TABLE #temp60days
(
AgentMailID		     VARCHAR(100) NULL, 
sixtydays            Decimal(18,2) NULL
)
	
insert into  #temp60days (AgentMailID ,sixtydays)
select au.Email,SUM(MP.Total)  from #temptblTransaction temptrn inner join tblMstPlan MP on temptrn.PlanID=MP.PlanID
inner join AspNetUsers au on au.Id= temptrn.UserID
where temptrn.StatusID = 2 and (temptrn.ProductID != 11 and temptrn.ProductID != 12 or temptrn.ProductID IS NULL) and
TRY_CONVERT(DATE,temptrn.IssueDate) >= TRY_CONVERT(DATE,@dt_Todate-30) and  
TRY_CONVERT(DATE,temptrn.IssueDate) <= TRY_CONVERT(DATE,@dt_Todate)  group by au.Email

UPDATE #temp
SET
sixtydays = tc.sixtydays    
FROM 
#temp t
INNER JOIN #temp60days tc
ON t.AgentMailID = tc.AgentMailID


SET @dt = @dt_Todate-30
SET @dt_Todate = @dt-1
-----------------------------------------

--#tempMT60days
CREATE TABLE #tempMT60days
(
AgentMailID		     VARCHAR(100) NULL, 
Morethansixtydays    Decimal(18,2) NULL
)
	
insert into  #tempMT60days (AgentMailID ,Morethansixtydays)
select au.Email,SUM(MP.Total)  from #temptblTransaction temptrn inner join tblMstPlan MP on temptrn.PlanID=MP.PlanID
inner join AspNetUsers au on au.Id= temptrn.UserID
where temptrn.StatusID = 2 and (temptrn.ProductID != 11 and temptrn.ProductID != 12 or temptrn.ProductID IS NULL) and
TRY_CONVERT(DATE,temptrn.IssueDate) >= TRY_CONVERT(DATE,@dt_FromDate) and
TRY_CONVERT(DATE,temptrn.IssueDate) <= TRY_CONVERT(DATE,@dt_Todate) group by au.Email

UPDATE #temp
SET
Morethansixtydays = tc.Morethansixtydays    
FROM 
#temp t
INNER JOIN #tempMT60days tc
ON t.AgentMailID = tc.AgentMailID
-----------------------------------------

Update #temp Set fifteendays = 0
Where fifteendays Is Null; 

Update #temp Set thirtydays = 0
Where thirtydays Is Null; 

Update #temp Set sixtydays = 0
Where sixtydays Is Null; 

Update #temp Set Morethansixtydays = 0
Where Morethansixtydays Is Null; 

Update #temp Set TotalAmount = 0
Where TotalAmount Is Null; 
-----------------------------------------

UPDATE #temp
SET
TotalAmount = t.TotalAmount + t.fifteendays    
FROM 
#temp t
INNER JOIN #temp15days tc
ON t.AgentMailID = tc.AgentMailID
where t.AgentMailID = tc.AgentMailID

UPDATE #temp
SET
TotalAmount = t.TotalAmount + t.thirtydays    
FROM 
#temp t
INNER JOIN #temp30days tc
ON t.AgentMailID = tc.AgentMailID
where t.AgentMailID = tc.AgentMailID

UPDATE #temp
SET
TotalAmount = t.TotalAmount + t.sixtydays    
FROM 
#temp t
INNER JOIN #temp60days tc
ON t.AgentMailID = tc.AgentMailID
where t.AgentMailID = tc.AgentMailID

UPDATE #temp
SET
TotalAmount = t.TotalAmount + t.Morethansixtydays    
FROM 
#temp t
INNER JOIN #tempMT60days tc
ON t.AgentMailID = tc.AgentMailID
where t.AgentMailID = tc.AgentMailID
-----------------------------------------

IF ISNULL(@s_UserId, '') <> ''
      BEGIN

		UPDATE #temp
		SET
		#temp.SumFifteenDays = (SELECT fifteendays FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumThirtyDays = (SELECT thirtydays FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumSixtyDays = (SELECT sixtydays FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumMoreThanSixtyDays = (SELECT Morethansixtydays FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumTotalAmount = (SELECT TotalAmount FROM #temp WHERE AgentMailID = @s_UserId)
		WHERE AgentMailID = @s_UserId


		Update #temp Set SumFifteenDays = 0
		Where SumFifteenDays Is Null; 
		
		Update #temp Set SumThirtyDays = 0
		Where SumThirtyDays Is Null; 
		
		Update #temp Set SumSixtyDays = 0
		Where SumSixtyDays Is Null; 
		
		Update #temp Set SumMoreThanSixtyDays = 0
		Where SumMoreThanSixtyDays Is Null; 
		
		Update #temp Set SumTotalAmount = 0
		Where SumTotalAmount Is Null;


	    select * from  #temp where AgentMailID = @s_UserId 
        
      END
ELSE
      BEGIN

		UPDATE #temp
		SET
		#temp.SumFifteenDays = (SELECT SUM(fifteendays) FROM #temp),
		#temp.SumThirtyDays = (SELECT SUM(thirtydays) FROM #temp),
		#temp.SumSixtyDays = (SELECT SUM(sixtydays) FROM #temp),
		#temp.SumMoreThanSixtyDays = (SELECT SUM(Morethansixtydays) FROM #temp),
		#temp.SumTotalAmount = (SELECT SUM(TotalAmount) FROM #temp)


		Update #temp Set SumFifteenDays = 0
		Where SumFifteenDays Is Null; 
		
		Update #temp Set SumThirtyDays = 0
		Where SumThirtyDays Is Null; 
		
		Update #temp Set SumSixtyDays = 0
		Where SumSixtyDays Is Null; 
		
		Update #temp Set SumMoreThanSixtyDays = 0
		Where SumMoreThanSixtyDays Is Null; 
		
		Update #temp Set SumTotalAmount = 0
		Where SumTotalAmount Is Null;


        select * from  #temp where fifteendays > 0 or thirtydays > 0 or sixtydays > 0 or Morethansixtydays > 0 order by AgentMailID
      
	  END
-----------------------------------------

		drop table #temptblTransaction
		drop table #temp
		drop table #temp15days
		drop table #temp30days
		drop table #temp60days
		drop table #tempMT60days

                COMMIT;
            END;
        END TRY
        BEGIN CATCH
            ROLLBACK;
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
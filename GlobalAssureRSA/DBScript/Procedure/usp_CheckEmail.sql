
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_CheckEmail')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_CheckEmail;
GO
CREATE PROCEDURE usp_CheckEmail
(
@Email Varchar(50) = null,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	     SELECT Distinct Email
	     from AspNetUsers where Email = @Email
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
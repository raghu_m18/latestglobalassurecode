
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetCertiByUserId')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetCertiByUserId;
GO
CREATE PROCEDURE usp_GetCertiByUserId
(@s_UserId        VARCHAR(250), 
 @s_CertificateNo VARCHAR(250), 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
                IF(@s_CertificateNo = '')
                    SET @s_CertificateNo = NULL;
                SELECT trn.*
                FROM tblTransaction trn
                     INNER JOIN AspNetUsers AU ON AU.Id = trn.UserID
                WHERE AU.Id = @s_UserId
                      AND trn.CertificateNo = ISNULL(@s_CertificateNo, trn.CertificateNo)
                ORDER BY trn.CertificateNo;
            END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
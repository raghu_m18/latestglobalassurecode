
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetStateCityDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetStateCityDetails;
GO
CREATE PROCEDURE usp_GetStateCityDetails
(@State           VARCHAR(250) = NULL, 
 @City            VARCHAR(250) = NULL, 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            IF(@State != '--Select--'
               AND @City != '--Select--')
                BEGIN
                    SELECT DISTINCT 
                           s.StateID, 
                           s.State, 
                           c.CityID, 
                           c.City
                    FROM tblMstState s
                         LEFT JOIN tblMstCity c ON s.StateID = c.StateID
                    WHERE s.State = @State
                          AND c.City = @City
                          AND c.IsValid = 1;
            END;
                ELSE
                IF(ISNULL(@State, '') != '--Select--')
                    BEGIN
                        SELECT DISTINCT 
                               s.StateID, 
                               s.State, 
                               c.CityID, 
                               c.City
                        FROM tblMstState s
                             LEFT JOIN tblMstCity c ON s.StateID = c.StateID
                        WHERE s.State = @State
                              AND c.IsValid = 1;
                END;
                    ELSE
                    IF(ISNULL(@City, '') != '--Select--')
                        BEGIN
                            SELECT DISTINCT 
                                   s.StateID, 
                                   s.State, 
                                   c.CityID, 
                                   c.City
                            FROM tblMstState s
                                 LEFT JOIN tblMstCity c ON s.StateID = c.StateID
                            WHERE c.City = @City
                                  AND c.IsValid = 1;
                    END;
                        ELSE
                        BEGIN
                            SELECT DISTINCT 
                                         S.StateID, 
                                          S.State, 
                                          C.CityID, 
                                          C.City
                                   FROM tblMstState S
                                        INNER JOIN tblMstCity C ON s.StateID = c.StateID
                                   WHERE S.IsValid = 1
                                         AND C.IsValid = 1;
                    END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
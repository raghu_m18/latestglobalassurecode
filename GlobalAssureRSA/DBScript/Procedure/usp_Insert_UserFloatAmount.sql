USE [GlobalAssureRSA]
GO
/****** Object:  StoredProcedure [dbo].[Insert_UserFloatAmount]    Script Date: 2021-01-19 9:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[Insert_UserFloatAmount]
	  @CreatedBy nvarchar(512),
      @datatable UserFloatAmountType READONLY,
	  @returnCode int Output
AS
BEGIN

Declare @CreatedById nvarchar(256) 

select @CreatedById=Id from AspNetUsers where UserName=@CreatedBy

      SET NOCOUNT ON;
	    BEGIN TRY
  begin tran;



     insert into tblUserFloatAmount(UserID,RefNo,Amount,IsValid,CreatedBy,PaymentReceivedDate,FloatAssignDate ) 
   select distinct (Select id from AspNetUsers where Username collate SQL_Latin1_General_CP1_CI_AS= LTRIM(RTRIM(H.USERID))),H.REFNO,H.FLOATAMOUNT,1,@CreatedById,H.PAYMENTRECEIVEDDATE,H.FLOATASSIGNDATE from  @datatable H
   where H.USERID is not null and H.USERID !='' 
   


    commit tran;
	 set @returnCode = 1
	END TRY
	BEGIN CATCH
	 IF @@TRANCOUNT>0
        ROLLBACK TRANSACTION;
	
    INSERT INTO ErrorLog(Log,LogDateTime)
    values(ERROR_MESSAGE(),GETDATE())
	  set @returnCode = 0
  END CATCH

 



END



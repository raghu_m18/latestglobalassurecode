
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetEmailConfig')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetEmailConfig;
GO
CREATE PROCEDURE usp_GetEmailConfig(@s_Error_Message VARCHAR(500) = NULL OUTPUT)
AS
    BEGIN
        BEGIN TRY
            DECLARE @ln_ConfigId INT;
            SELECT TOP 1 ec.n_Email_Config_Id, 
                         ec.s_SMTP_Host, 
                         ec.s_SMTP_Port, 
                         ec.s_SMTP_User_Name, 
                         ec.s_SMTP_Password, 
                         ec.s_From_Email, 
                         ec.s_From_Name, 
                         ec.n_SecureSMTP, 
                         ec.n_OrderNo, 
                         ec.n_Status, 
                         ec.s_Last_Modify_User, 
                         ec.n_EmailLimitCount, 
                         ec.n_SMSTriggerCount, 
                         es.n_CountNo AS n_CurrentCount


						
            FROM tblEmailConfig ec
                 INNER JOIN tblEmailSquence es ON ec.n_Email_Config_Id = es.n_Email_Config_Id
            WHERE n_Status = 1
            ORDER BY es.n_CountNo ASC;
            --
            SET @ln_ConfigId =
            (
                SELECT TOP 1 ec.n_Email_Config_Id
                FROM tblEmailConfig ec
                     INNER JOIN tblEmailSquence es ON ec.n_Email_Config_Id = es.n_Email_Config_Id
                WHERE n_Status = 1
                ORDER BY es.n_CountNo ASC
            );
            --
            UPDATE tblEmailSquence
              SET 
                  n_CountNo = (n_CountNo + 1)
            WHERE n_Email_Config_Id = @ln_ConfigId;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
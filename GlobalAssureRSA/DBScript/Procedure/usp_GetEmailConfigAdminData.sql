
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetEmailConfigAdminData')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetEmailConfigAdminData;
GO
CREATE PROCEDURE usp_GetEmailConfigAdminData(@s_Error_Message VARCHAR(500) = NULL OUTPUT)
AS
    BEGIN
        BEGIN TRY
            SELECT ec.* ,es.n_CountNo as n_CorrentCount
            FROM tblEmailConfig ec
                 INNER JOIN tblEmailSquence es ON ec.n_Email_Config_Id = es.n_Email_Config_Id
          order by ec.n_Email_Config_Id desc
			
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
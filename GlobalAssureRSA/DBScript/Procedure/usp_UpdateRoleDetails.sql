
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_UpdateRoleDetails')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_UpdateRoleDetails;
GO
CREATE PROCEDURE usp_UpdateRoleDetails
(
@Email Varchar(50) =null,
@LastModifiedBy Varchar(50),
@RoleID int =null ,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
  DECLARE @l_UserID Varchar(500)
     BEGIN
         BEGIN TRY
	     SET @l_UserID =
(
    SELECT DISTINCT 
           Id
    FROM AspNetUsers
    WHERE Email = @LastModifiedBy
);
UPDATE AspNetUsers
  SET 
      OtherAdminId = @RoleID, 
      LastModifiedBy = @l_UserID, 
      LastModifiedDate = GETDATE()
WHERE Email = @Email;
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;	
     END;
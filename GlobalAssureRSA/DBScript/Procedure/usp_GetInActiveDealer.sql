
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetInActiveDealer')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetInActiveDealer;
GO
CREATE PROCEDURE usp_GetInActiveDealer
(@ProductID     INT, 
 @FromDate       DateTime, 
  @ToDate       DateTime, 
 @s_ErrorMessage VARCHAR(500) = NULL OUTPUT
)
--WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            CREATE TABLE #tmptrans
            (Policycount INT, 
             userid      VARCHAR(150)
            );
            INSERT INTO #tmptrans
                   SELECT COUNT(1), 
                          userId
                   FROM tblTransaction
                   WHERE ProductID = @ProductID
                         AND CreatedDate BETWEEN cast(@FromDate as datetime) AND cast(@ToDate as datetime)
                   GROUP BY userId;
            SELECT Email, 
                   PhoneNumber, 
                   UserName, 
                   [name],
				   CreatedDate
            FROM AspNetUsers au
                 LEFT JOIN #tmptrans tmp ON au.id = tmp.userid
            --left join tblTransaction trxn on  trxn.UserID= au.id
            WHERE ISNULL(tmp.Policycount, 0) = 0; --and trxn.ProductID = 2

            DROP TABLE #tmptrans;
        END TRY
        BEGIN CATCH
            SELECT s_ErrorMessage = ERROR_MESSAGE();
        END CATCH;
    END;
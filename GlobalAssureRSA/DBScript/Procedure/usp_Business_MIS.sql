
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_Business_MIS')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_Business_MIS;
GO
CREATE PROCEDURE usp_Business_MIS
(
 @s_UserId          VARCHAR(250)   = NULL,
 @dt_FromDate		DATETIME       = NULL,
 @dt_Todate			DATETIME       = NULL,
 @s_Error_Message	VARCHAR(500)   = NULL OUTPUT
)
--WITH ENCRYPTION
AS
    BEGIN
        BEGIN TRY
            BEGIN
                BEGIN TRAN;			
               
			   
CREATE TABLE #temp
		(
			AgentName				 VARCHAR(100)  NULL, 
			AgentMailID				 VARCHAR(100)  NULL, 

			PolicyCountNew			 Decimal(18)   NULL,
			PolicyAmountNew			 Decimal(18,2) NULL,
			PolicyCountOld			 Decimal(18)   NULL,
			PolicyAmountOld			 Decimal(18,2) NULL,
			TotalPolicyCount		 Decimal(18)   NULL,
			TotalPolicyAmount		 Decimal(18,2) NULL,

			SumPolicyCountNew        Decimal(18)   NULL,
			SumPolicyAmountNew       Decimal(18,2) NULL,
			SumPolicyCountOld        Decimal(18)   NULL,
			SumPolicyAmountOld       Decimal(18,2) NULL,
			SumTotalPolicyCount	     Decimal(18)   NULL,
			SumTotalPolicyAmount     Decimal(18,2) NULL
		)


		insert into #temp  (AgentName,AgentMailID)  
		--select Name,Email,ContactPersonName,ContactPersonNo from AspNetUsers where IsActive = 1 
		select Name,Email from AspNetUsers where (IsActive = 1 or IsActive is null) and (IsLocked = 0 or IsLocked is null)

--#tempNew
CREATE TABLE #tempNew
(
AgentMailID		     VARCHAR(100)  NULL, 
PolicyCountNew       Decimal(18)   NULL,
PolicyAmountNew      Decimal(18,2) NULL
)
	
insert into  #tempNew (AgentMailID, PolicyCountNew, PolicyAmountNew)
select au.Email, COUNT(*), SUM(MP.Total) from tblTransaction trn inner join tblMstPlan MP on trn.PlanID = MP.PlanID
inner join AspNetUsers au on au.Id = trn.UserID
where (trn.TransactionTypeID = 5 or trn.TransactionTypeID is null) and trn.IsValid = 1 and
TRY_CONVERT(DATE,trn.IssueDate) >= TRY_CONVERT(DATE,@dt_FromDate) and  
TRY_CONVERT(DATE,trn.IssueDate) <= TRY_CONVERT(DATE,@dt_Todate) group by au.Email

UPDATE #temp
SET
PolicyCountNew = tc.PolicyCountNew,
PolicyAmountNew = tc.PolicyAmountNew
FROM 
#temp t
INNER JOIN #tempNew tc
ON t.AgentMailID = tc.AgentMailID
-----------------------------------------

--#tempOld
CREATE TABLE #tempOld
(
AgentMailID		     VARCHAR(100)  NULL, 
PolicyCountOld       Decimal(18)   NULL,
PolicyAmountOld      Decimal(18,2) NULL
)
	
insert into  #tempOld (AgentMailID, PolicyCountOld, PolicyAmountOld)
select au.Email, COUNT(*), SUM(MP.Total) from tblTransaction trn inner join tblMstPlan MP on trn.PlanID = MP.PlanID
inner join AspNetUsers au on au.Id = trn.UserID
where trn.TransactionTypeID = 6 and trn.IsValid = 1 and
TRY_CONVERT(DATE,trn.IssueDate) >= TRY_CONVERT(DATE,@dt_FromDate) and  
TRY_CONVERT(DATE,trn.IssueDate) <= TRY_CONVERT(DATE,@dt_Todate) group by au.Email

UPDATE #temp
SET
PolicyCountOld = tc.PolicyCountOld,
PolicyAmountOld = tc.PolicyAmountOld
FROM 
#temp t
INNER JOIN #tempOld tc
ON t.AgentMailID = tc.AgentMailID
-----------------------------------------

Update #temp Set PolicyCountNew = 0
Where PolicyCountNew Is Null; 

Update #temp Set PolicyAmountNew = 0
Where PolicyAmountNew Is Null; 

Update #temp Set PolicyCountOld = 0
Where PolicyCountOld Is Null; 

Update #temp Set PolicyAmountOld = 0
Where PolicyAmountOld Is Null; 
-----------------------------------------

UPDATE #temp
SET
TotalPolicyCount = t.PolicyCountNew + t.PolicyCountOld,
TotalPolicyAmount = t.PolicyAmountNew + t.PolicyAmountOld
FROM 
#temp t
LEFT JOIN #tempNew tnew
ON t.AgentMailID = tnew.AgentMailID
LEFT JOIN #tempOld told
ON t.AgentMailID = told.AgentMailID
-----------------------------------------

Update #temp Set TotalPolicyCount = 0
Where TotalPolicyCount Is Null; 

Update #temp Set TotalPolicyAmount = 0
Where TotalPolicyAmount Is Null; 
-----------------------------------------

IF ISNULL(@s_UserId, '') <> ''
      BEGIN

		UPDATE #temp
		SET
		#temp.SumPolicyCountNew = (SELECT PolicyCountNew FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumPolicyAmountNew = (SELECT PolicyAmountNew FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumPolicyCountOld = (SELECT PolicyCountOld FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumPolicyAmountOld = (SELECT PolicyAmountOld FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumTotalPolicyCount = (SELECT TotalPolicyCount FROM #temp WHERE AgentMailID = @s_UserId),
		#temp.SumTotalPolicyAmount = (SELECT TotalPolicyAmount FROM #temp WHERE AgentMailID = @s_UserId)
		WHERE AgentMailID = @s_UserId

		
		Update #temp Set SumPolicyCountNew = 0
		Where SumPolicyCountNew Is Null; 
		
		Update #temp Set SumPolicyAmountNew = 0
		Where SumPolicyAmountNew Is Null; 
		
		Update #temp Set SumPolicyCountOld = 0
		Where SumPolicyCountOld Is Null; 
		
		Update #temp Set SumPolicyAmountOld = 0
		Where SumPolicyAmountOld Is Null; 
		
		Update #temp Set SumTotalPolicyCount = 0
		Where SumTotalPolicyCount Is Null; 
		
		Update #temp Set SumTotalPolicyAmount = 0
		Where SumTotalPolicyAmount Is Null;


	    select * from  #temp where AgentMailID = @s_UserId 
        
      END
ELSE
      BEGIN

		UPDATE #temp
		SET
		#temp.SumPolicyCountNew = (SELECT SUM(PolicyCountNew) FROM #temp),
		#temp.SumPolicyAmountNew = (SELECT SUM(PolicyAmountNew) FROM #temp),
		#temp.SumPolicyCountOld = (SELECT SUM(PolicyCountOld) FROM #temp),
		#temp.SumPolicyAmountOld = (SELECT SUM(PolicyAmountOld) FROM #temp),
		#temp.SumTotalPolicyCount = (SELECT SUM(TotalPolicyCount) FROM #temp),
		#temp.SumTotalPolicyAmount = (SELECT SUM(TotalPolicyAmount) FROM #temp)

		
		Update #temp Set SumPolicyCountNew = 0
		Where SumPolicyCountNew Is Null; 
		
		Update #temp Set SumPolicyAmountNew = 0
		Where SumPolicyAmountNew Is Null; 
		
		Update #temp Set SumPolicyCountOld = 0
		Where SumPolicyCountOld Is Null; 
		
		Update #temp Set SumPolicyAmountOld = 0
		Where SumPolicyAmountOld Is Null; 
		
		Update #temp Set SumTotalPolicyCount = 0
		Where SumTotalPolicyCount Is Null; 
		
		Update #temp Set SumTotalPolicyAmount = 0
		Where SumTotalPolicyAmount Is Null; 

		
		select * from  #temp where PolicyCountNew > 0 or PolicyAmountNew > 0 or PolicyCountOld > 0 or PolicyAmountOld > 0 order by AgentMailID

	  END
-----------------------------------------

		drop table #temp
		drop table #tempNew
		drop table #tempOld

                COMMIT;
            END;
        END TRY
        BEGIN CATCH
            ROLLBACK;
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
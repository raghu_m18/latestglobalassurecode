
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetPlanByUserId')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetPlanByUserId;
GO
CREATE PROCEDURE usp_GetPlanByUserId
(@s_UserId        VARCHAR(250), 
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            BEGIN
                SELECT UPD.*
                FROM tblUserPrivilegeDetail UPD
                     INNER JOIN AspNetUsers AU ON AU.Id = UPD.UserID
                WHERE AU.Id = @s_UserId AND UPD.IsValid = 1
                ORDER BY UPD.Plans;
            END;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
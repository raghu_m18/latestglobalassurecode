
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_UpdateMappigUserID')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_UpdateMappigUserID;
GO
CREATE PROCEDURE usp_UpdateMappigUserID
(
@Email Varchar(50) =null,
@LastModifiedBy Varchar(50) = null,
@mappingUserID varchar(250) = null,
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
  DECLARE @l_UserID Varchar(500)
     BEGIN
         BEGIN TRY
	     SET @l_UserID =
(
    SELECT DISTINCT 
           Id
    FROM AspNetUsers
    WHERE Email = @LastModifiedBy
);
UPDATE AspNetUsers
  SET 
      MappingUserId = @mappingUserID, 
      LastModifiedBy = @l_UserID, 
      LastModifiedDate = GETDATE()
WHERE Email = @Email;
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;	
     END;

IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_UpdateUser')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_UpdateUser;
GO
CREATE PROCEDURE usp_UpdateUser
(@newUser         VARCHAR(250)          = NULL, 
 @oldUser                VARCHAR(250)  = NULL,  
 @LastModifiedBy				Varchar(250) = null,
 @s_Error_Message     VARCHAR(500) = NULL OUTPUT
)
AS
    BEGIN
        BEGIN TRY
            UPDATE AspNetUsers
              SET 
                  Email = @newUser, 
				  UserName = @newUser,
                  LastModifiedBy = @LastModifiedBy             
            WHERE UserName = @oldUser;
        END TRY
        BEGIN CATCH
            SELECT @s_Error_Message = ERROR_MESSAGE();
        END CATCH;
    END;
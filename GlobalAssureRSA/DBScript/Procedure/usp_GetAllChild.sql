
IF EXISTS
(
    SELECT 1
    FROM sys.objects
    WHERE OBJECT_ID = OBJECT_ID(N'usp_GetAllChild')
          AND TYPE IN(N'P')
)
    DROP PROCEDURE usp_GetAllChild;
GO
CREATE PROCEDURE usp_GetAllChild
(
@MappingID varchar(100),
 @s_Error_Message VARCHAR(500) = NULL OUTPUT
)
AS
     BEGIN
         BEGIN TRY
	     SELECT Distinct Email,[Name]
	     from AspNetUsers
		 where MappingUserId = @MappingID
         END TRY
         BEGIN CATCH
             SELECT @s_Error_Message = ERROR_MESSAGE();
         END CATCH;
     END;
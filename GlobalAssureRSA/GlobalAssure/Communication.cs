﻿using GlobalAssure.Models;
using EmailService;
using ExceptionTypes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using log4net;
using System.Net;
using GlobalAssure.Entity;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Newtonsoft.Json;

namespace GlobalAssure
{
    public class Communication
    {
        ILog logger = LogManager.GetLogger("OnlinePayment");
        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();

        public bool SendingMail(string UserName, string EmailID, string Password, string Type, string Link = null)
        {
            try
            {
                MailModel mm = new MailModel();
                mm.To = new List<string>();
                mm.To.Add(EmailID);
                mm.Subject = String.Empty;

                string path = String.Empty;

                switch (Type)
                {
                    case "ForgotPassword":
                        path = System.Web.HttpContext.Current.Server.MapPath(@"~/Content/EmailTemplates/ForgotPassword.html");
                        mm.Subject = "Reset Password : " + UserName;
                        break;
                    case "NewUser":
                        path = System.Web.HttpContext.Current.Server.MapPath(@"~/Content/EmailTemplates/LoginCreated.html");
                        mm.Subject = "Login Created for User : " + UserName;

                        break;
                }

                string readFile = "";
                using (System.IO.StreamReader reader = new System.IO.StreamReader(path))
                {
                    readFile = reader.ReadToEnd();
                }

                string body = string.Empty;
                body = readFile;
                body = body.Replace("$$UserName$$", UserName);
                body = body.Replace("$$EmailID$$", EmailID);
                body = body.Replace("$$Password$$", Password);
                body = body.Replace("$$Link$$", Link);

                mm.Body = body;
                Communication _this = new GlobalAssure.Communication();
                _this.SendMail(mm);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public bool SendingPaymentLinkMail(string EmailID, string Link = null, string cc_email = null)
        {
            try
            {
                MailModel mm = new MailModel();
                mm.To = new List<string>();
                mm.To.Add(EmailID);
                if (cc_email != null)
                {
                    mm.Cc = new List<string>();
                    mm.Cc.Add(cc_email);
                }
                mm.Subject = String.Empty;
                string path = String.Empty;
                mm.Subject = "Payment Link";
                string readFile = "";
                path = System.Web.HttpContext.Current.Server.MapPath(@"~/Content/EmailTemplates/PaymentLink.html");
                using (System.IO.StreamReader reader = new System.IO.StreamReader(path))
                {
                    readFile = reader.ReadToEnd();
                }

                string body = string.Empty;
                body = readFile;
                body = body.Replace("$$Link$$", Link);
                mm.Body = body;
                Communication _this = new GlobalAssure.Communication();
                _this.SendMail(mm);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public bool SendingAssistanceCreatedMail(decimal AssistId)
        {
            tblAssistDetail tblAssistDetail = new tblAssistDetail();
            List<string> LstEmail = new List<string>();

            string AssistanceRefNo = string.Empty;
            string CustomerName = string.Empty;
            string CustomerPhoneNo = string.Empty;
            string VehicleColor = string.Empty;
            string Incident = string.Empty;
            string CreatedDate = string.Empty;
            string Location = string.Empty;

            try
            {
                MailModel mm = new MailModel();

                tblAssistDetail = db.tblAssistDetails.Where(w => w.Id == AssistId).FirstOrDefault();
                if (tblAssistDetail != null)
                {
                    if (!string.IsNullOrEmpty(tblAssistDetail.RefNo))
                    {
                        AssistanceRefNo = tblAssistDetail.RefNo;
                    }

                    if (!string.IsNullOrEmpty(tblAssistDetail.CustomerName))
                    {
                        CustomerName = tblAssistDetail.CustomerName;
                    }
                    else
                    {
                        decimal TransId = Convert.ToDecimal(tblAssistDetail.TransactionId);
                        var tblTransaction = db.tblTransactions.AsNoTracking().Where(w => w.TransactionID == TransId).Select(s => new { s.CustomerName, s.CustomerMiddleName, s.CustomerLastName }).FirstOrDefault();
                        if (tblTransaction != null)
                        {
                            CustomerName = tblTransaction.CustomerName + " " + tblTransaction.CustomerMiddleName + " " + tblTransaction.CustomerLastName;
                        }
                    }

                    if (!string.IsNullOrEmpty(tblAssistDetail.Issue))
                    {
                        CustomerPhoneNo = tblAssistDetail.Issue;
                    }

                    if (!string.IsNullOrEmpty(tblAssistDetail.VehicleColor))
                    {
                        VehicleColor = tblAssistDetail.VehicleColor;
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(tblAssistDetail.IncidentDetailid)))
                    {
                        int IncidentDetailid = Convert.ToInt32(tblAssistDetail.IncidentDetailid);
                        string strIncident = db.tblMstIncidentDetails.AsNoTracking().Where(w => w.Detailid == IncidentDetailid).Select(s => s.IncidentDetail).FirstOrDefault();
                        if (!string.IsNullOrEmpty(strIncident))
                        {
                            Incident = strIncident;
                        }                     
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(tblAssistDetail.AssistCreatedDate)))
                    {
                        CreatedDate = Convert.ToString(tblAssistDetail.AssistCreatedDate);
                    }

                    if (!string.IsNullOrEmpty(tblAssistDetail.Address))
                    {
                        Location = tblAssistDetail.Address;
                    }
                }

                LstEmail = db.tblAssistEmail.AsNoTracking().Where(w => w.IsValid != false).Select(s => s.Email).ToList();

                int i = 1;
                foreach(var Email in LstEmail)
                {
                    if (i == 1)
                    {
                        mm.To = new List<string>();
                        mm.To.Add(Email);
                    }
                    else
                    {
                        mm.Cc = new List<string>();
                        mm.Cc.Add(Email);
                    }

                    i++;
                }                         

                mm.Subject = String.Empty;
                string path = String.Empty;
                mm.Subject = "Assistance Case Created";
                string readFile = "";
                path = System.Web.HttpContext.Current.Server.MapPath(@"~/Content/EmailTemplates/AssistanceCreated.html");
                using (System.IO.StreamReader reader = new System.IO.StreamReader(path))
                {
                    readFile = reader.ReadToEnd();
                }

                string body = string.Empty;
                body = readFile;
                body = body.Replace("$$AssistanceRefNo$$", AssistanceRefNo);
                body = body.Replace("$$CustomerName$$", CustomerName);
                body = body.Replace("$$CustomerPhoneNo$$", CustomerPhoneNo);
                body = body.Replace("$$VehicleColor$$", VehicleColor);
                body = body.Replace("$$Incident$$", Incident);
                body = body.Replace("$$CreatedDate$$", CreatedDate);
                body = body.Replace("$$Location$$", Location);             
                mm.Body = body;
                Communication _this = new GlobalAssure.Communication();
                _this.SendMail(mm);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SendingAssistanceCreatedSMSMessage(decimal AssistId)
        {
            string strContactNo = string.Empty;
            string SMSContent = string.Empty;
            bool MsgSent = false;
            string SendSMSConfig = Convert.ToString(ConfigurationManager.AppSettings["SendSMS"]);

            tblAssistDetail tblAssistDetail = new tblAssistDetail();
            List<string> LstContactNo = new List<string>();

            string AssistanceRefNo = string.Empty;
            string CustomerName = string.Empty;
            string CustomerPhoneNo = string.Empty;
            string VehicleColor = string.Empty;
            string Incident = string.Empty;
            string CreatedDate = string.Empty;
            string Location = string.Empty;

            try
            {
                tblAssistDetail = db.tblAssistDetails.Where(w => w.Id == AssistId).FirstOrDefault();
                if (tblAssistDetail != null)
                {
                    if (!string.IsNullOrEmpty(tblAssistDetail.RefNo))
                    {
                        AssistanceRefNo = tblAssistDetail.RefNo;
                    }

                    if (!string.IsNullOrEmpty(tblAssistDetail.CustomerName))
                    {
                        CustomerName = tblAssistDetail.CustomerName;
                    }
                    else
                    {
                        decimal TransId = Convert.ToDecimal(tblAssistDetail.TransactionId);
                        var tblTransaction = db.tblTransactions.AsNoTracking().Where(w => w.TransactionID == TransId).Select(s => new { s.CustomerName, s.CustomerMiddleName, s.CustomerLastName }).FirstOrDefault();
                        if (tblTransaction != null)
                        {
                            CustomerName = tblTransaction.CustomerName + " " + tblTransaction.CustomerMiddleName + " " + tblTransaction.CustomerLastName;
                        }
                    }

                    if (!string.IsNullOrEmpty(tblAssistDetail.Issue))
                    {
                        CustomerPhoneNo = tblAssistDetail.Issue;
                    }

                    if (!string.IsNullOrEmpty(tblAssistDetail.VehicleColor))
                    {
                        VehicleColor = tblAssistDetail.VehicleColor;
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(tblAssistDetail.IncidentDetailid)))
                    {
                        int IncidentDetailid = Convert.ToInt32(tblAssistDetail.IncidentDetailid);
                        string strIncident = db.tblMstIncidentDetails.AsNoTracking().Where(w => w.Detailid == IncidentDetailid).Select(s => s.IncidentDetail).FirstOrDefault();
                        if (!string.IsNullOrEmpty(strIncident))
                        {
                            Incident = strIncident;
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(tblAssistDetail.AssistCreatedDate)))
                    {
                        CreatedDate = Convert.ToString(tblAssistDetail.AssistCreatedDate);
                    }

                    if (!string.IsNullOrEmpty(tblAssistDetail.Address))
                    {
                        Location = tblAssistDetail.Address;
                    }
                }

                SMSContent = "Assist Case Created Details: " + "RefNo: " + AssistanceRefNo + "," + "CustomerName: " + CustomerName + "," + "CustomerPhoneNo: " + CustomerPhoneNo + "," + "VehicleColor: " + VehicleColor + "," + "Incident: " + Incident + "," + "CreatedDate: " + CreatedDate + "," + "Location: " + Location;

                LstContactNo = db.tblAssistEmail.AsNoTracking().Where(w => w.IsValid != false).Select(s => s.ContactNo).ToList();

                foreach (var ContactNo in LstContactNo)
                {
                    strContactNo = ContactNo;                   

                    if (SendSMSConfig == "1")
                    {
                        string SendOTPUrl = ConfigurationManager.AppSettings["SendOTPUrl"];
                        string OTPusername = ConfigurationManager.AppSettings["OTPusername"];
                        string OTPpassword = ConfigurationManager.AppSettings["OTPpassword"];
                        string MobileNo = strContactNo;
                        string OTPudh = "";
                        string OTPfrom = ConfigurationManager.AppSettings["OTPfrom"];

                        string OTPText = SMSContent;

                        string OTPdlrurl = ConfigurationManager.AppSettings["OTPdlr-url"];

                        string FullSendOtpUrl = SendOTPUrl + "username=" + OTPusername + "&password=" + OTPpassword + "&to=" + MobileNo + "&udh=" + OTPudh + "&from=" + OTPfrom
                            + "&text=" +HttpUtility.UrlEncode(OTPText) + "&dlr-url=" + OTPdlrurl;

                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        HttpWebRequest myReq = (System.Net.HttpWebRequest)WebRequest.Create(FullSendOtpUrl);
                        HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                        System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                        string responseString = respStreamReader.ReadToEnd();
                        respStreamReader.Close();
                        myResp.Close();

                        if (responseString.ToLower() == "sent.")
                        {
                            // Message Send on Customer Mobile. :: Success
                            MsgSent = true;
                            logger.Info("Message Send Success :: Request : " + FullSendOtpUrl + " :: Response : " + responseString);
                        }
                        else
                        {
                            // Message Not Send on Customer Mobile. :: Failed
                            MsgSent = false;
                            logger.Error("Message Send Failed :: Request : " + FullSendOtpUrl + " :: Response : " + responseString);
                        }
                    }
                    else
                    {
                        MsgSent = true;
                        logger.Error("Message Send Failed :: Web.Config Flag Is OFF");
                    }
                }            
            }
            catch (Exception ex)
            {
                // Failed
                MsgSent = false;
                logger.Error("Message Send Failed :: Exception : " + ex.Message);
            }

            return MsgSent;
        }

        public bool SendingSMSMessage(string ContactNo, string SMSContent)
        {
            bool MsgSent = false;
            string SendSMSConfig = Convert.ToString(ConfigurationManager.AppSettings["SendSMS"]);
            try
            {
                if (SendSMSConfig == "1")
                {
                    string SendOTPUrl = ConfigurationManager.AppSettings["SendOTPUrl"];
                    string OTPusername = ConfigurationManager.AppSettings["OTPusername"];
                    string OTPpassword = ConfigurationManager.AppSettings["OTPpassword"];
                    string MobileNo = ContactNo;
                    string OTPudh = "";
                    string OTPfrom = ConfigurationManager.AppSettings["OTPfrom"];

                    string OTPText = SMSContent;

                    string OTPdlrurl = ConfigurationManager.AppSettings["OTPdlr-url"];

                    string FullSendOtpUrl = SendOTPUrl + "username=" + OTPusername + "&password=" + OTPpassword + "&to=" + MobileNo + "&udh=" + OTPudh + "&from=" + OTPfrom
                        + "&text=" + OTPText + "&dlr-url=" + OTPdlrurl;

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    HttpWebRequest myReq = (System.Net.HttpWebRequest)WebRequest.Create(FullSendOtpUrl);
                    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                    string responseString = respStreamReader.ReadToEnd();
                    respStreamReader.Close();
                    myResp.Close();

                    if (responseString.ToLower() == "sent.")
                    {
                        // Message Send on Customer Mobile. :: Success
                        MsgSent = true;
                        logger.Info("Message Send Success :: Request : " + FullSendOtpUrl + " :: Response : " + responseString);
                    }
                    else
                    {
                        // Message Not Send on Customer Mobile. :: Failed
                        MsgSent = false;
                        logger.Error("Message Send Failed :: Request : " + FullSendOtpUrl + " :: Response : " + responseString);
                    }
                }
                else
                {
                    MsgSent = true;
                    logger.Error("Message Send Failed :: Web.Config Flag Is OFF");
                }
            }
            catch (Exception ex)
            {
                // Failed
                MsgSent = false;
                logger.Error("Message Send Failed :: Exception : " + ex.Message);
            }
            
            return MsgSent;
        }

        public bool SendingAssistanceCreatedICICILSMSMessage(tblAssistDetail tblAssist)
        {
            string strContactNo = string.Empty;
            string SMSContent = string.Empty;
            bool MsgSent = false;
            string SendSMSConfig = Convert.ToString(ConfigurationManager.AppSettings["SendICICILSMS"]);

            ICICILombardSMSRequest ICICILSMSReq = new ICICILombardSMSRequest();
            ICICILombardAuthentication ICICILAuthResp = new ICICILombardAuthentication();
            ICICILombardSMSResponse ICICILSMSResp = new ICICILombardSMSResponse();

            tblAssistDetail tblAssistDetail = new tblAssistDetail();
            List<string> LstContactNo = new List<string>();

            string AssistanceRefNo = string.Empty;
            string CustomerName = string.Empty;
            string CustomerPhoneNo = string.Empty;
            string VehicleColor = string.Empty;
            string Incident = string.Empty;
            string CreatedDate = string.Empty;
            string Location = string.Empty;

            try
            {
                if (tblAssist == null)
                {
                    // Failed
                    MsgSent = false;
                    logger.Error("Message Send Failed :: DataModel is NUll.");
                }
                else
                {
                    tblAssistDetail = db.tblAssistDetails.Where(w => w.Id == tblAssist.Id).FirstOrDefault();
                    if (tblAssistDetail != null)
                    {
                        if (!string.IsNullOrEmpty(tblAssistDetail.RefNo))
                        {
                            AssistanceRefNo = tblAssistDetail.RefNo;
                        }

                        if (!string.IsNullOrEmpty(tblAssistDetail.CustomerName))
                        {
                            CustomerName = tblAssistDetail.CustomerName;
                        }
                        else
                        {
                            decimal TransId = Convert.ToDecimal(tblAssistDetail.TransactionId);
                            var tblTransaction = db.tblTransactions.AsNoTracking().Where(w => w.TransactionID == TransId).Select(s => new { s.CustomerName, s.CustomerMiddleName, s.CustomerLastName }).FirstOrDefault();
                            if (tblTransaction != null)
                            {
                                CustomerName = tblTransaction.CustomerName + " " + tblTransaction.CustomerMiddleName + " " + tblTransaction.CustomerLastName;
                            }
                        }

                        if (!string.IsNullOrEmpty(tblAssistDetail.Issue))
                        {
                            CustomerPhoneNo = tblAssistDetail.Issue;
                        }

                        if (!string.IsNullOrEmpty(tblAssistDetail.VehicleColor))
                        {
                            VehicleColor = tblAssistDetail.VehicleColor;
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(tblAssistDetail.IncidentDetailid)))
                        {
                            int IncidentDetailid = Convert.ToInt32(tblAssistDetail.IncidentDetailid);
                            string strIncident = db.tblMstIncidentDetails.AsNoTracking().Where(w => w.Detailid == IncidentDetailid).Select(s => s.IncidentDetail).FirstOrDefault();
                            if (!string.IsNullOrEmpty(strIncident))
                            {
                                Incident = strIncident;
                            }
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(tblAssistDetail.AssistCreatedDate)))
                        {
                            CreatedDate = Convert.ToString(tblAssistDetail.AssistCreatedDate);
                        }

                        if (!string.IsNullOrEmpty(tblAssistDetail.Address))
                        {
                            Location = tblAssistDetail.Address;
                        }
                    }

                    SMSContent = "Assist Case Created Details: " + "RefNo: " + AssistanceRefNo + "," + "CustomerName: " + CustomerName + "," + "CustomerPhoneNo: " + CustomerPhoneNo + "," + "VehicleColor: " + VehicleColor + "," + "Incident: " + Incident + "," + "CreatedDate: " + CreatedDate + "," + "Location: " + Location;

                    LstContactNo = db.tblAssistEmail.AsNoTracking().Where(w => w.IsValid != false).Select(s => s.ContactNo).ToList();

                    var dict = new Dictionary<string, string>();
                    dict.Add("grant_type", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_grant_type"]));
                    dict.Add("username", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_username"]));
                    dict.Add("password", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_password"]));
                    dict.Add("scope", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_scope"]));
                    dict.Add("client_id", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_client_id"]));
                    dict.Add("client_secret", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_client_secret"]));

                    string strICICILAuthRequest = "grant_type : " + dict["grant_type"] + ", username : " + dict["username"] + ", password : " + dict["password"] + ", scope : " + dict["scope"] + ", client_id : " + dict["client_id"] + ", client_secret : " + dict["client_secret"];

                    HttpClient client = new HttpClient();
                    var content = new FormUrlEncodedContent(dict);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    Uri bs = new Uri(Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_URL"]));
                    client.BaseAddress = bs;
                    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                    var response = client.PostAsync(Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_URL"]), content).Result;

                    logger.Error(" ICICILombard Authentication Request Data:>response > #Status:-" + response.StatusCode + " #Respone.Is Success :- " + response.IsSuccessStatusCode
                                 + "#Request Message:-" + response.RequestMessage + "Response :-" + response.Headers);

                    var result = response.Content.ReadAsStringAsync().Result;

                    logger.Error("ICICILombard Authentication Response Data:" + result);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            // Success
                            MsgSent = true;
                            logger.Info("Message Send Success :: Response : " + result);

                            ICICILAuthResp = JsonConvert.DeserializeObject<ICICILombardAuthentication>(result);

                            tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                            tblICICILObj.CreatedDate = DateTime.Now;
                            tblICICILObj.IsValid = true;
                            tblICICILObj.AssistId = tblAssist.Id;
                            tblICICILObj.APIAuthRequest = strICICILAuthRequest;
                            tblICICILObj.APIAuthResponse = result;
                            tblICICILObj.APIAuthStatus = "Success";
                            tblICICILObj.APISMSRequest = null;
                            tblICICILObj.APISMSResponse = null;
                            tblICICILObj.APISMSStatus = null;

                            db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                            db.SaveChanges();
                        }
                        else
                        {
                            // Failed
                            MsgSent = false;
                            logger.Error("Message Send Failed :: Response : " + result);

                            tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                            tblICICILObj.CreatedDate = DateTime.Now;
                            tblICICILObj.IsValid = true;
                            tblICICILObj.AssistId = tblAssist.Id;
                            tblICICILObj.APIAuthRequest = strICICILAuthRequest;
                            tblICICILObj.APIAuthResponse = result;
                            tblICICILObj.APIAuthStatus = "Failed";
                            tblICICILObj.APISMSRequest = null;
                            tblICICILObj.APISMSResponse = null;
                            tblICICILObj.APISMSStatus = null;

                            db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        // Failed
                        MsgSent = false;
                        logger.Error("Message Send Failed :: Response : " + result);

                        tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                        tblICICILObj.CreatedDate = DateTime.Now;
                        tblICICILObj.IsValid = true;
                        tblICICILObj.AssistId = tblAssist.Id;
                        tblICICILObj.APIAuthRequest = strICICILAuthRequest;
                        tblICICILObj.APIAuthResponse = result;
                        tblICICILObj.APIAuthStatus = "Failed";
                        tblICICILObj.APISMSRequest = null;
                        tblICICILObj.APISMSResponse = null;
                        tblICICILObj.APISMSStatus = null;

                        db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                        db.SaveChanges();
                    }

                    foreach (var ContactNo in LstContactNo)
                    {
                        strContactNo = ContactNo;
                        string strCorrelationId = Convert.ToString(Guid.NewGuid());

                        if (SendSMSConfig == "1")
                        {
                            if (MsgSent == true)
                            {
                                ICICILSMSReq.CorrelationId = strCorrelationId;
                                int ICICILSms_UserId = 0;
                                int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_UserId"]), out ICICILSms_UserId);
                                ICICILSMSReq.UserId = ICICILSms_UserId;
                                ICICILSMSReq.SenderId = Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_SenderId"]);
                                ICICILSMSReq.MSISDN = strContactNo;
                                ICICILSMSReq.Message = SMSContent;
                                ICICILSMSReq.MTag = tblAssist.RefNo;
                                ICICILSMSReq.MsgType = (SMSContent.Length <= 160) ? 0 : 4;
                                ICICILSMSReq.Costcenter = Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_CostCenter"]);

                                HttpClient httpclient = new HttpClient();
                                var httpcontent = new StringContent(JsonConvert.SerializeObject(ICICILSMSReq), Encoding.UTF8, "application/json");
                                logger.Error("ICICILombard SMS Request Data:" + JsonConvert.SerializeObject(ICICILSMSReq));
                                httpcontent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                Uri smsbs = new Uri(Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_URL"]));
                                httpclient.BaseAddress = smsbs;

                                //Set Basic Auth
                                string Access_Token = ICICILAuthResp.access_token;
                                httpclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Access_Token);
                                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                                var SMSresponse = httpclient.PostAsync(Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_URL"]), httpcontent).Result;

                                logger.Error(" ICICILombard SMS Request Data:>response > #Status:-" + SMSresponse.StatusCode + " #Respone.Is Success :- " + SMSresponse.IsSuccessStatusCode
                                 + "#Request Message:-" + SMSresponse.RequestMessage + "Response :-" + SMSresponse.Headers);

                                var SMSresult = SMSresponse.Content.ReadAsStringAsync().Result;

                                logger.Error("ICICILombard SMS Response Data:" + SMSresult);

                                if (SMSresponse.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    if (SMSresponse.IsSuccessStatusCode)
                                    {
                                        // Success
                                        MsgSent = true;
                                        logger.Info("Message Send Success :: Response : ICICILombard SMS API Success : " + SMSresult);

                                        ICICILSMSResp = JsonConvert.DeserializeObject<ICICILombardSMSResponse>(SMSresult);

                                        tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                                        tblICICILObj.CreatedDate = DateTime.Now;
                                        tblICICILObj.IsValid = true;
                                        tblICICILObj.AssistId = tblAssist.Id;
                                        tblICICILObj.APIAuthRequest = null;
                                        tblICICILObj.APIAuthResponse = null;
                                        tblICICILObj.APIAuthStatus = null;
                                        tblICICILObj.APISMSRequest = JsonConvert.SerializeObject(ICICILSMSReq);
                                        tblICICILObj.APISMSResponse = SMSresult;
                                        tblICICILObj.APISMSStatus = "Success";

                                        db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        // Failed
                                        MsgSent = false;
                                        logger.Error("Message Send Failed :: Response : ICICILombard SMS API Failure (IsSuccessStatusCode != false)");

                                        tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                                        tblICICILObj.CreatedDate = DateTime.Now;
                                        tblICICILObj.IsValid = true;
                                        tblICICILObj.AssistId = tblAssist.Id;
                                        tblICICILObj.APIAuthRequest = null;
                                        tblICICILObj.APIAuthResponse = null;
                                        tblICICILObj.APIAuthStatus = null;
                                        tblICICILObj.APISMSRequest = JsonConvert.SerializeObject(ICICILSMSReq);
                                        tblICICILObj.APISMSResponse = SMSresult;
                                        tblICICILObj.APISMSStatus = "Failed";

                                        db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    // Failed
                                    MsgSent = false;
                                    logger.Error("Message Send Failed :: Response : ICICILombard SMS API Failure (StatusCode not OK)");

                                    tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                                    tblICICILObj.CreatedDate = DateTime.Now;
                                    tblICICILObj.IsValid = true;
                                    tblICICILObj.AssistId = tblAssist.Id;
                                    tblICICILObj.APIAuthRequest = null;
                                    tblICICILObj.APIAuthResponse = null;
                                    tblICICILObj.APIAuthStatus = null;
                                    tblICICILObj.APISMSRequest = JsonConvert.SerializeObject(ICICILSMSReq);
                                    tblICICILObj.APISMSResponse = SMSresult;
                                    tblICICILObj.APISMSStatus = "Failed";

                                    db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                // Failed
                                MsgSent = false;
                                logger.Error("Message Send Failed :: Response : Authorisation API Failure (Access Bearer Token Not Found or InValid)");
                            }
                        }
                        else
                        {
                            MsgSent = true;
                            logger.Error("Message Send Failed :: Web.Config Flag Is OFF");
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                // Failed
                MsgSent = false;
                logger.Error("Message Send Failed :: Exception : " + ex.Message);
            }

            return MsgSent;
        }

        public bool SendingICICILSMSMessage(tblAssistDetail tblAssist, string ContactNo, string SMSContent)
        {
            bool MsgSent = false;
            string SendSMSConfig = Convert.ToString(ConfigurationManager.AppSettings["SendICICILSMS"]);

            ICICILombardSMSRequest ICICILSMSReq = new ICICILombardSMSRequest();
            ICICILombardAuthentication ICICILAuthResp = new ICICILombardAuthentication();
            ICICILombardSMSResponse ICICILSMSResp = new ICICILombardSMSResponse();

            try
            {
                if (tblAssist == null || string.IsNullOrEmpty(ContactNo) || string.IsNullOrEmpty(SMSContent))
                {
                    // Failed
                    MsgSent = false;
                    logger.Error("Message Send Failed :: DataModel is NUll or ContactNo not Found or SMSMessageContent not Found.");
                }
                else
                {
                    if (SendSMSConfig == "1")
                    {
                        var dict = new Dictionary<string, string>();
                        dict.Add("grant_type", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_grant_type"]));
                        dict.Add("username", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_username"]));
                        dict.Add("password", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_password"]));
                        dict.Add("scope", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_scope"]));
                        dict.Add("client_id", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_client_id"]));
                        dict.Add("client_secret", Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_client_secret"]));

                        string strICICILAuthRequest = "grant_type : " + dict["grant_type"] + ", username : " + dict["username"] + ", password : " + dict["password"] + ", scope : " + dict["scope"] + ", client_id : " + dict["client_id"] + ", client_secret : " + dict["client_secret"];

                        HttpClient client = new HttpClient();
                        var content = new FormUrlEncodedContent(dict);
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                        Uri bs = new Uri(Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_URL"]));
                        client.BaseAddress = bs;
                        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                        var response = client.PostAsync(Convert.ToString(ConfigurationManager.AppSettings["ICICILAuthentication_URL"]), content).Result;

                        logger.Error(" ICICILombard Authentication Request Data:>response > #Status:-" + response.StatusCode + " #Respone.Is Success :- " + response.IsSuccessStatusCode
                                     + "#Request Message:-" + response.RequestMessage + "Response :-" + response.Headers);

                        var result = response.Content.ReadAsStringAsync().Result;

                        logger.Error("ICICILombard Authentication Response Data:" + result);

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                // Success
                                MsgSent = true;
                                logger.Info("Message Send Success :: Response : " + result);

                                ICICILAuthResp = JsonConvert.DeserializeObject<ICICILombardAuthentication>(result);

                                tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                                tblICICILObj.CreatedDate = DateTime.Now;
                                tblICICILObj.IsValid = true;
                                tblICICILObj.AssistId = tblAssist.Id;
                                tblICICILObj.APIAuthRequest = strICICILAuthRequest;
                                tblICICILObj.APIAuthResponse = result;
                                tblICICILObj.APIAuthStatus = "Success";
                                tblICICILObj.APISMSRequest = null;
                                tblICICILObj.APISMSResponse = null;
                                tblICICILObj.APISMSStatus = null;

                                db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                                db.SaveChanges();
                            }
                            else
                            {
                                // Failed
                                MsgSent = false;
                                logger.Error("Message Send Failed :: Response : " + result);

                                tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                                tblICICILObj.CreatedDate = DateTime.Now;
                                tblICICILObj.IsValid = true;
                                tblICICILObj.AssistId = tblAssist.Id;
                                tblICICILObj.APIAuthRequest = strICICILAuthRequest;
                                tblICICILObj.APIAuthResponse = result;
                                tblICICILObj.APIAuthStatus = "Failed";
                                tblICICILObj.APISMSRequest = null;
                                tblICICILObj.APISMSResponse = null;
                                tblICICILObj.APISMSStatus = null;

                                db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            // Failed
                            MsgSent = false;
                            logger.Error("Message Send Failed :: Response : " + result);

                            tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                            tblICICILObj.CreatedDate = DateTime.Now;
                            tblICICILObj.IsValid = true;
                            tblICICILObj.AssistId = tblAssist.Id;
                            tblICICILObj.APIAuthRequest = strICICILAuthRequest;
                            tblICICILObj.APIAuthResponse = result;
                            tblICICILObj.APIAuthStatus = "Failed";
                            tblICICILObj.APISMSRequest = null;
                            tblICICILObj.APISMSResponse = null;
                            tblICICILObj.APISMSStatus = null;

                            db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                            db.SaveChanges();
                        }

                        if (MsgSent == true)
                        {
                            string strCorrelationId = Convert.ToString(Guid.NewGuid());
                            ICICILSMSReq.CorrelationId = strCorrelationId;
                            int ICICILSms_UserId = 0;
                            int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_UserId"]), out ICICILSms_UserId);
                            ICICILSMSReq.UserId = ICICILSms_UserId;
                            ICICILSMSReq.SenderId = Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_SenderId"]);
                            ICICILSMSReq.MSISDN = ContactNo;
                            ICICILSMSReq.Message = SMSContent;
                            ICICILSMSReq.MTag = tblAssist.RefNo;
                            ICICILSMSReq.MsgType = (SMSContent.Length <= 160) ? 0 : 4;
                            ICICILSMSReq.Costcenter = Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_CostCenter"]);

                            HttpClient httpclient = new HttpClient();
                            var httpcontent = new StringContent(JsonConvert.SerializeObject(ICICILSMSReq), Encoding.UTF8, "application/json");
                            logger.Error("ICICILombard SMS Request Data:" + JsonConvert.SerializeObject(ICICILSMSReq));
                            httpcontent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                            Uri smsbs = new Uri(Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_URL"]));
                            httpclient.BaseAddress = smsbs;

                            //Set Basic Auth
                            string Access_Token = ICICILAuthResp.access_token;
                            httpclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Access_Token);
                            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                            var SMSresponse = httpclient.PostAsync(Convert.ToString(ConfigurationManager.AppSettings["ICICILSMS_URL"]), httpcontent).Result;

                            logger.Error(" ICICILombard SMS Request Data:>response > #Status:-" + SMSresponse.StatusCode + " #Respone.Is Success :- " + SMSresponse.IsSuccessStatusCode
                             + "#Request Message:-" + SMSresponse.RequestMessage + "Response :-" + SMSresponse.Headers);

                            var SMSresult = SMSresponse.Content.ReadAsStringAsync().Result;

                            logger.Error("ICICILombard SMS Response Data:" + SMSresult);

                            if (SMSresponse.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                if (SMSresponse.IsSuccessStatusCode)
                                {
                                    // Success
                                    MsgSent = true;
                                    logger.Info("Message Send Success :: Response : ICICILombard SMS API Success : " + SMSresult);

                                    ICICILSMSResp = JsonConvert.DeserializeObject<ICICILombardSMSResponse>(SMSresult);

                                    tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                                    tblICICILObj.CreatedDate = DateTime.Now;
                                    tblICICILObj.IsValid = true;
                                    tblICICILObj.AssistId = tblAssist.Id;
                                    tblICICILObj.APIAuthRequest = null;
                                    tblICICILObj.APIAuthResponse = null;
                                    tblICICILObj.APIAuthStatus = null;
                                    tblICICILObj.APISMSRequest = JsonConvert.SerializeObject(ICICILSMSReq);
                                    tblICICILObj.APISMSResponse = SMSresult;
                                    tblICICILObj.APISMSStatus = "Success";

                                    db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    // Failed
                                    MsgSent = false;
                                    logger.Error("Message Send Failed :: Response : ICICILombard SMS API Failure (IsSuccessStatusCode != false)");

                                    tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                                    tblICICILObj.CreatedDate = DateTime.Now;
                                    tblICICILObj.IsValid = true;
                                    tblICICILObj.AssistId = tblAssist.Id;
                                    tblICICILObj.APIAuthRequest = null;
                                    tblICICILObj.APIAuthResponse = null;
                                    tblICICILObj.APIAuthStatus = null;
                                    tblICICILObj.APISMSRequest = JsonConvert.SerializeObject(ICICILSMSReq);
                                    tblICICILObj.APISMSResponse = SMSresult;
                                    tblICICILObj.APISMSStatus = "Failed";

                                    db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                // Failed
                                MsgSent = false;
                                logger.Error("Message Send Failed :: Response : ICICILombard SMS API Failure (StatusCode not OK)");

                                tblICICILombardSMSReqRes tblICICILObj = new tblICICILombardSMSReqRes();
                                tblICICILObj.CreatedDate = DateTime.Now;
                                tblICICILObj.IsValid = true;
                                tblICICILObj.AssistId = tblAssist.Id;
                                tblICICILObj.APIAuthRequest = null;
                                tblICICILObj.APIAuthResponse = null;
                                tblICICILObj.APIAuthStatus = null;
                                tblICICILObj.APISMSRequest = JsonConvert.SerializeObject(ICICILSMSReq);
                                tblICICILObj.APISMSResponse = SMSresult;
                                tblICICILObj.APISMSStatus = "Failed";

                                db.tblICICILombardSMSReqRes.Add(tblICICILObj);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            // Failed
                            MsgSent = false;
                            logger.Error("Message Send Failed :: Response : Authorisation API Failure (Access Bearer Token Not Found or InValid)");
                        }
                    }
                    else
                    {
                        MsgSent = true;
                        logger.Error("Message Send Failed :: Web.Config Flag Is OFF");
                    }
                }                
            }
            catch (Exception ex)
            {
                // Failed
                MsgSent = false;
                logger.Error("Message Send Failed :: Exception : " + ex.Message);
            }

            return MsgSent;
        }

        public bool SendingCertificateMail(string EmailID, string CertificatePath, string AttachmentFileName, string Link = null, bool isLandMarkService = false, string BccEmailID = null)
        {
            try
            {
                if (string.IsNullOrEmpty(EmailID) || string.IsNullOrEmpty(CertificatePath) || string.IsNullOrEmpty(AttachmentFileName))
                {
                    // logger.Error(" Send Certificate mail () => Invalid  Params || emailid :" + EmailID + "  ||Certificate Path : " + CertificatePath + " ||Attach Name :" + AttachmentFileName);
                    return false;
                }
                else
                {
                    string CertificateNo = AttachmentFileName;
                    var tblTransaction = db.tblTransactions.Where(w => w.CertificateNo == CertificateNo).FirstOrDefault();

                    var PolicyGenerationFlag = db.tblMstProducts.Where(w => w.ProductID == tblTransaction.ProductID).Select(s => s.PolicyGeneration).FirstOrDefault();
                    if (PolicyGenerationFlag.ToLower().Equals("after") && tblTransaction.StatusID != 1)
                    {
                        return false;
                    }
                    else //if (PolicyGenerationFlag.ToLower().Equals("before"))
                    {
                        MailModel mm = new MailModel();
                        mm.To = new List<string>();
                        mm.To.Add(EmailID);
                        if (isLandMarkService)
                        {
                            string LandMark_cc_mailid = ConfigurationManager.AppSettings["LandMark_CC_MailId"];
                            if (!string.IsNullOrEmpty(LandMark_cc_mailid))
                            {
                                mm.Cc = new List<string>();
                                mm.Cc.Add(LandMark_cc_mailid);
                            }
                        }
                        else if (!isLandMarkService && BccEmailID != null)
                        {
                            mm.Bcc = new List<string>();
                            mm.Bcc.Add(BccEmailID);
                        }

                        mm.Subject = String.Empty;
                        string path = String.Empty;
                        string AttachmentPath = string.Empty;
                        string AttachmentName = AttachmentFileName + ".PDF";
                        mm.Subject = "Your Certificate has been issued";
                        string readFile = "";
                        path = System.Web.HttpContext.Current.Server.MapPath(@"~/Content/EmailTemplates/Certificate.html");
                        AttachmentPath = System.Web.HttpContext.Current.Server.MapPath(CertificatePath);
                        using (System.IO.StreamReader reader = new System.IO.StreamReader(path))
                        {
                            readFile = reader.ReadToEnd();
                        }

                        string body = string.Empty;
                        body = readFile;
                        mm.Body = body;
                        mm.AttachmentFileFullPath = AttachmentPath;
                        mm.AttachmentFileName = AttachmentName;
                        Communication _this = new GlobalAssure.Communication();
                        _this.SendMail(mm);
                        return true;
                    }                                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

       public bool SendRelianceAssistMail()
        {
            try
            {
                MailModel mm = new MailModel();
                mm.To = new List<String>();
                string to = System.Configuration.ConfigurationManager.AppSettings["GlobalAssureSupport"] ?? "Global Assure";
                mm.To.Add(to);
                    

                
                mm.Subject = String.Empty;
                string path = String.Empty;
                string AttachmentPath = string.Empty;
                
                mm.Subject = "New Reliance Request";
                string readFile = "";
                path = System.Web.HttpContext.Current.Server.MapPath(@"~/Content/EmailTemplates/RelianceAssist.html");
               // AttachmentPath = System.Web.HttpContext.Current.Server.MapPath(CertificatePath);
                using (System.IO.StreamReader reader = new System.IO.StreamReader(path))
                {
                    readFile = reader.ReadToEnd();
                }

                string body = string.Empty;
                body = readFile;
                mm.Body = body;
                mm.AttachmentFileFullPath =path;
               // mm.AttachmentFileName = AttachmentName;
                Communication _this = new GlobalAssure.Communication();
                _this.SendMail(mm);
                return true;
            }
            
            catch (Exception ex)
            {
                throw ex;
            }


}
public void SendMail(MailModel mailobj)
        {
            try
            {
                string from = System.Configuration.ConfigurationManager.AppSettings["FromEmailDefault"] ?? "Global Assure";
                string mailTo = String.Empty;
                string mailCC = String.Empty;
                string mailBcc = String.Empty;

                if (mailobj.To != null)
                {
                    foreach (string To in mailobj.To)
                    {
                        mailTo = To + ";";
                    }

                    if (mailTo != null)
                    {
                        mailTo = mailTo.Remove(mailTo.Length - 1);
                    }
                }

                if (mailobj.Cc != null)
                {
                    foreach (string cc in mailobj.Cc)
                    {
                        mailCC = cc + ";";
                    }

                    if (mailCC != null)
                    {
                        mailCC = mailCC.Remove(mailCC.Length - 1);
                    }
                }

                if (mailobj.Bcc != null)
                {
                    foreach (string bcc in mailobj.Bcc)
                    {
                        mailBcc = bcc + ";";
                    }

                    if (mailBcc != null)
                    {
                        mailBcc = mailBcc.Remove(mailBcc.Length - 1);
                    }
                }

                EmailNotification _ObjEmailNotification = new EmailNotification();
                Email objmail = new Email();
                objmail.fromName = from;
                if (mailBcc != "")
                    objmail.mailBcc.Add(mailBcc);
                objmail.mailBody = mailobj.Body;
                if (mailCC != "")
                    objmail.mailCc.Add(mailCC);
                objmail.mailFrom = from;
                objmail.mailSubject = mailobj.Subject;
                objmail.mailTo.Add(mailTo);

                if (mailobj.AttachmentFileName != null)
                {
                    objmail.Attachments = new List<AttachmentModel>();
                    AttachmentModel att = new AttachmentModel();
                    att.attachmentName = mailobj.AttachmentFileName;
                    att.attachmentFile = mailobj.AttachmentFileFullPath;
                    objmail.Attachments.Add(att);
                }
                _ObjEmailNotification.SendEmail(objmail, null, 0);

            }
            catch (Exception e)
            {
                throw e;
            }

        }


        internal void SendingMail(string v1, string v2, object defaultPassword, string v3)
        {
            throw new NotImplementedException();
        }

        public class NotificationResponse
        {
            public bool success { get; set; }
            public String AcknowledgementID { get; set; }
            public String ReturnCode { get; set; }
            public String ReturnDesc { get; set; }
            public DateTime MessageDateTime { get; set; }

        }


        public string GetIPAddressValue()
        {
            string ipAdd = string.Empty;

            try
            {
                ipAdd = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(ipAdd))
                {
                    ipAdd = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }

                if (ipAdd == "::1")
                {
                    using (WebClient client = new WebClient())
                    {
                        ipAdd = client.DownloadString("https://api.ipify.org");
                    }
                }
            }
            catch (Exception ex)
            {
                // throw ex;
            }

            return ipAdd;
        }
    }



    public static class UIExceptionHandler
    {
        public static bool HandleException(ref System.Exception ex)
        {
            bool rethrow = false;
            try
            {
                if (ex is BaseException)
                {
                    return false;
                }
                else
                {
                    rethrow = ExceptionPolicy.HandleException(ex, "UIPolicy");
                    return rethrow;
                }
            }
            catch (Exception exp)
            {
                ex = exp;
                return false;
            }
        }
    }





}
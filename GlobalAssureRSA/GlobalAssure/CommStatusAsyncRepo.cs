﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GlobalAssure
{
    public class CommStatusAsyncRepo
    {
        public static void UpdateCommunicationLogStatus(decimal AutoMailCommunicationLogID, bool status, string errorMessage)
        {
            ILog logger = log4net.LogManager.GetLogger("Email");
            try
            {
                string queryString =
                        "Update tblAutoMailCommunicationLog  " +
                        "SET IsDelivered = @isSent ,ErrorInfo = @ErrorInfo " +
                        "WHERE AutoMailCommunicationLogID = @AutoMailCommunicationLogID ";
                string conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
                SqlConnection cons = null;
                using (cons = new SqlConnection(conStr))
                {
                    cons.Open();
                    SqlCommand cmd = new SqlCommand(queryString, cons);
                    cmd.Parameters.Add(new SqlParameter("@isSent", status));
                    cmd.Parameters.Add(new SqlParameter("@ErrorInfo", errorMessage));
                    cmd.Parameters.Add(new SqlParameter("@AutoMailCommunicationLogID", AutoMailCommunicationLogID));
                    cmd.ExecuteNonQuery();
                    cons.Close();
                }
                logger.Info("Email Status updated for : " + AutoMailCommunicationLogID);
            }
            catch (Exception e)
            {
                logger.Error("Email Status update failed for : " + AutoMailCommunicationLogID + ":" + e.Message);
                logger.Error(e.StackTrace);
                //Dont handle because this method is a call back method.
            }
        }
    }
}
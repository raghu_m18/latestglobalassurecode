﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using GlobalAssure.Models;
using log4net;
using GlobalAssure.Admin;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace EmailService

{
    /// <summary>
    /// Index   Date        Modifier    Descriprion
    /// 1000    11/04/2020  Bikash      Attachment Extension Issue resolved  
    /// </summary>
    public class EmailNotification
    {


        public delegate void SetMailStatus(decimal communicationID, bool status, string errorMessage);
        public void SendEmail(Email mailDetail, SetMailStatus callBack, int moduleId = 0)
        {
            new System.Threading.Tasks.Task(
                () =>
                {
                    MailResponse resp = SendEmailAsync(mailDetail, moduleId, false);
                    if (callBack != null)
                    {
                        callBack(mailDetail.CommunicationID, resp.status, resp.errorMessage);
                    }
                }
                ).Start();
        }

        private MailResponse SendEmailAsync(Email mailDetail, int moduleId, bool deleteAttachmentsAfterSend = false)
        {
            ILog logger = log4net.LogManager.GetLogger("OnlinePayment");
            MailResponse resp = new MailResponse();
            try
            {
               

                string EmailServiceEnabled = System.Configuration.ConfigurationManager.AppSettings["EmailServiceEnabled"].ToString();
                if (!EmailServiceEnabled.Equals("True"))
                {
                    resp.status = true;
                    return resp;
                }

                clsEmailConfig objConfig = new clsEmailConfig();
                objConfig = GetEmailConfig();
                objConfig.ToEmail = mailDetail.mailTo[0]+";";
                objConfig.EmailSubject = mailDetail.mailSubject;
                objConfig.EmailBody = mailDetail.mailBody;
                //string domainName = ConfigurationManager.AppSettings["EmailDomain"].ToString();
                // string fromEmailID = ConfigurationManager.AppSettings["FromEmailDefault"].ToString();

                using (SmtpClient mailServer = new SmtpClient(objConfig.SMTPHost,Convert.ToInt32(objConfig.SMTPPort)))
                {
                    mailServer.EnableSsl = (objConfig.SecureSMTP == 1) ? true : false;
                    mailServer.UseDefaultCredentials = true;
                    NetworkCredential NetworkCred = new NetworkCredential(objConfig.SMTPUserName, objConfig.SMTPPassword);
                    mailServer.Credentials = NetworkCred;
                    string fromEmail = string.Format("{0}<{1}>", objConfig.FromName, objConfig.FromEmail);
                    MailMessage msg = new MailMessage(fromEmail, mailDetail.mailTo[0], mailDetail.mailSubject, mailDetail.mailBody);
                    for (int i = 1; i < mailDetail.mailTo.Count; i++)
                    {
                        objConfig.EmailSubject += mailDetail.mailTo[i] +";";
                        msg.To.Add(mailDetail.mailTo[i]);
                    }
                    msg.IsBodyHtml = true;

                    foreach (var cc in mailDetail.mailCc)
                    {
                        objConfig.CCEmail += cc + ";";
                        msg.CC.Add(cc);
                    }

                    // Adding CC Email if value configured in configuration
                    string _cc = ConfigurationManager.AppSettings["CCEmailID"];
                    if (!string.IsNullOrEmpty(_cc))
                    {
                        objConfig.CCEmail += _cc + ";";
                        msg.CC.Add(_cc);
                    }

                    foreach (var bcc in mailDetail.mailBcc)
                    {
                        objConfig.BCCEmail += bcc + ";";
                        msg.Bcc.Add(bcc);
                    }

                    // Adding BCC Email if value configured in configuration
                    string _bcc = ConfigurationManager.AppSettings["BCCEmailID"];
                    if (!string.IsNullOrEmpty(_bcc))
                    {
                        objConfig.BCCEmail += _bcc + ";";
                        msg.Bcc.Add(_bcc);
                    }

                    //Adding attachments
                    FileStream[] inp = null;
                    int fileCount = 0;
                    if (mailDetail.Attachments != null)
                    {
                        inp = new FileStream[mailDetail.Attachments.Count];

                        foreach (var attachment in mailDetail.Attachments)
                        {
                            objConfig.EmailAttachment += Path.GetFileName(attachment.attachmentFile) + ";";
                            inp[fileCount] = new FileStream(attachment.attachmentFile, FileMode.Open, FileAccess.Read, FileShare.Read);

                            //SU 1000
                            // Attachment attach = new Attachment(inp[fileCount], attachment.attachmentName + "." + attachment.attachmentType);
                            Attachment attach = new Attachment(inp[fileCount], attachment.attachmentName);
                            //EU100
                            msg.Attachments.Add(attach);
                            fileCount++;

                        }

                    }

                    logger.Error("EMAIL##Info##" + "Before Mail Sent");
                    try
                    {
                        mailServer.Send(msg);
                        objConfig.SendStatus = "Success";
                        logger.Error("EMAIL##Info##" + "After Mail Sent");
                    }
                    catch (Exception ex)
                    {
                        logger.Error("EMAIL##Error##" + ex.Message+"##"+ex.StackTrace);
                        objConfig.SendError = ex.Message + "##" + ex.StackTrace;
                        objConfig.SendStatus = "Fail";
                    }
                    
                    

                    if (mailDetail.Attachments != null)
                    {
                        fileCount--;
                        while (fileCount >= 0)
                        {
                            if (inp[fileCount] != null)
                            {
                                inp[fileCount].Close();
                            }
                            fileCount--;
                        }
                    }
                }
                string strMsg = "";
                objConfig.SaveEmailLog(ref strMsg);
                if (!string.IsNullOrEmpty(strMsg))
                {
                    logger.Error("EMAIL##Info##" + "Send Log Save Fail:"+ strMsg);

                }
                resp.status = true;
                return resp;
            }
            catch (Exception ex)
            {
                logger.Error("EMAIL##Failure##" + ex.Message);
                logger.Error("EMAIL##Failure##" + ex.StackTrace);
                resp.status = false;
                resp.errorMessage = ex.Message;
                return resp;
            }
        }

      private  clsEmailConfig GetEmailConfig()
        {
            ILog logger = log4net.LogManager.GetLogger("Email");
            clsEmailConfig objEmailconfig = new clsEmailConfig();
            try
            {
                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                string sql = "usp_GetEmailConfig";
                bool SendSMS = false;
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    DataTable dtEmailConfig = new DataTable();
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = new SqlCommand(sql, conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.Fill(dtEmailConfig);
                        if (dtEmailConfig != null && dtEmailConfig.Rows.Count > 0)
                        {
                            objEmailconfig.SMTPHost =Convert.ToString(dtEmailConfig.Rows[0]["s_SMTP_Host"]);
                            objEmailconfig.SMTPPort = Convert.ToString(dtEmailConfig.Rows[0]["s_SMTP_Port"]);
                            objEmailconfig.SMTPUserName = Convert.ToString(dtEmailConfig.Rows[0]["s_SMTP_User_Name"]);
                            objEmailconfig.SMTPPassword = Convert.ToString(dtEmailConfig.Rows[0]["s_SMTP_Password"]);
                            objEmailconfig.FromEmail = Convert.ToString(dtEmailConfig.Rows[0]["s_From_Email"]);
                            objEmailconfig.FromName = Convert.ToString(dtEmailConfig.Rows[0]["s_From_Name"]);
                            objEmailconfig.SecureSMTP = Convert.ToInt32(dtEmailConfig.Rows[0]["n_SecureSMTP"]);
                            objEmailconfig.EmailConfigId= Convert.ToInt32(dtEmailConfig.Rows[0]["n_Email_Config_Id"]);
                            if (Convert.ToInt32(dtEmailConfig.Rows[0]["n_SMSTriggerCount"]) >0 &&
                                (Convert.ToInt32(dtEmailConfig.Rows[0]["n_CurrentCount"]) > Convert.ToInt32(dtEmailConfig.Rows[0]["n_SMSTriggerCount"])))
                            {

                                GlobalAssure.Communication _c = new GlobalAssure.Communication();
                               
                                string[]  lstMobile =Convert.ToString( ConfigurationManager.AppSettings["EmailLimitTrigger"]).Split(';');
                                string SMSBody = Convert.ToString(ConfigurationManager.AppSettings["EmailLimitTriggerMsg"]);
                                SMSBody = SMSBody.Replace("#LimitCount#", Convert.ToString(dtEmailConfig.Rows[0]["n_EmailLimitCount"]));
                                SMSBody = SMSBody.Replace("#CurrentCount#", Convert.ToString(dtEmailConfig.Rows[0]["n_CurrentCount"]));
                                foreach (var item in lstMobile)
                                {
                                    _c.SendingSMSMessage(item, SMSBody);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {

                logger.Error("EMAIL##ConfigDataFetch##" + ex.Message);
                logger.Error("EMAIL##ConfigDataFetchStackTrace##" + ex.StackTrace);
            }

            return objEmailconfig;
        }



    }

   

    public class Email
    {
        public string fromName { get; set; }
        public List<string> toName { get; set; }
        public string mailFrom { get; set; }
        public List<string> mailTo { get; set; }
        public List<string> mailCc { get; set; }
        public List<string> mailBcc { get; set; }
        public string mailSubject { get; set; }
        public MailMessage mailFormat { get; set; }
        public string mailBody { get; set; }
        public List<AttachmentModel> Attachments { get; set; }
        public decimal CommunicationID { get; set; }
        // Added the property TranscttionID to capture while sending email.
        public string transctionId { get; set; }
        public string LoginUserID { get; set; }
        public Email()
        {
            mailTo = new List<string>();
            mailCc = new List<string>();
            mailBcc = new List<string>();
        }
    }

    public class AttachmentModel
    {
        public string attachmentName { get; set; }
        public string attachmentType { get; set; }
        public string attachmentFile { get; set; }
    }
    class MailResponse
    {
        public bool status { get; set; }
        public string errorMessage { get; set; }
        public MailResponse()
        {
            status = false;
            errorMessage = "";
        }
    }
}


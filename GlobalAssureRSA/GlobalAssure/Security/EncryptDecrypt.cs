﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace iNube.ClaimsLive.CrossCutting.Security
{
   public static class EncryptDecrypt
    {

       private static string Salt = "inubesolutions";
       private static string HashAlgorithm = "SHA1";
       private static  int PasswordIterations = 2;
       private static  string InitialVector = "OFRna73m*aze01xY";
       private static  int KeySize = 256;

       #region Methods to Encrypt and Decrypt the Text
       /// <summary>
       /// Using this method to encrypt the  given text
       /// </summary>
       /// <param name="toBeEncrypted">Text to be encrypted</param>
       /// <param name="password">password to encrypt text</param>
       /// <returns>return encrypted text</returns>
       public static string EncryptText(string toBeEncrypted, string password)
       {
           try
           {
               //check for null or empty of given text
               if (string.IsNullOrEmpty(toBeEncrypted))
                   return "";

               // Get the bytes of the string
               byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(toBeEncrypted);
               byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

               // Hash the password with SHA256
               // passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

               byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

               string result = Convert.ToBase64String(bytesEncrypted);

               return result;
           }catch(Exception){
               return "";
           }
       }

       /// <summary>
       /// Using this method to Decrypt the given text
       /// </summary>
       /// <param name="toBeDecrypted">text to be decrypted</param>
       /// <param name="password">password to decrypt text</param>
       /// <returns>return decrypted text</returns>
       public static string DecryptText(string toBeDecrypted, string password)
       {
           try
           {
               //check for null or empty of given text
               if (string.IsNullOrEmpty(toBeDecrypted))
                   return "";

               // Get the bytes of the string
               byte[] bytesToBeDecrypted = Convert.FromBase64String(toBeDecrypted);
               byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
               //passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

               byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

               string result = Encoding.UTF8.GetString(bytesDecrypted);

               return result;
           }catch(Exception){
               return "";
           }
       }

       #endregion

       #region Methods to Encrypt and Decrypt the File

       /// <summary>
       /// Method to Encrypt the File
       /// </summary>
       /// <param name="filePath">given file path</param>
       /// <param name="password">password to encrypt file</param>
       public static bool EncryptFile(string filePath, string password)
       {
           try
           {
               //check for null or empty of given text
               if (!string.IsNullOrEmpty(filePath))
               {
                   string fileName = Path.GetFileNameWithoutExtension(filePath);
                   string pathroot = filePath.Substring(0, filePath.LastIndexOf("\\"));
                   string fileExtension = Path.GetExtension(filePath);

                   byte[] bytesToBeEncrypted = File.ReadAllBytes(filePath);
                   byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

                   byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);
                   string newName = GetFilename(fileName, fileExtension, "enc");
                   string fileEncrypted = pathroot + "/" + newName;

                   File.WriteAllBytes(fileEncrypted, bytesEncrypted);
                   return true;
               }
               else
               {
                   return false;
               }

           }
           catch (Exception)
           {
               return false;
           }
       }

       /// <summary>
       /// Method to decrypt the File
       /// </summary>
       /// <param name="fileEncrypted">file need to be decrypted</param>
       /// <param name="password">password to decrypt the file</param>
       public static bool DecryptFile(string fileEncrypted, string password)
       {
           try
           {
               //check for null or empty of given text
               if (!string.IsNullOrEmpty(fileEncrypted))
               {
                   string fileName = Path.GetFileNameWithoutExtension(fileEncrypted);
                   string pathroot = fileEncrypted.Substring(0, fileEncrypted.LastIndexOf("\\"));
                   string fileExtension = Path.GetExtension(fileEncrypted);

                   byte[] bytesToBeDecrypted = File.ReadAllBytes(fileEncrypted);
                   byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

                   byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);
                   string newName = GetFilename(fileName, fileExtension, "dec");

                   string file = pathroot + "/" + newName;
                   File.WriteAllBytes(file, bytesDecrypted);
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception)
           {
               return false;
           }
       }

       public static string GetFilename(string fileName, string extension, string type)
       {

           DateTime date = DateTime.Now;
           string splitdate = date.ToString();
           string[] split = null;
           if (splitdate.Contains("/")) { split = splitdate.Split('/'); }
           if (splitdate.Contains("-")) { split = splitdate.Split('-'); }

           string[] timesplit = split[2].Split(':');
           split = null;
           split = timesplit[0].Split(' ');

           string name = date.Day + "-" + date.Month + "-" + split[0] + "-" + split[1] + "-" + timesplit[1];
           split = null;
           split = timesplit[2].Split(' ');
           name += "-" + split[0];
           splitdate = fileName;
           split = null;
           split = splitdate.Split('.');
           splitdate = string.Empty;
           splitdate = split[0] + "-" + name + "-" + type + extension;
           return splitdate;
       }

       #endregion
       #region Methods to Encrypt and Decrypt
       /// <summary>
       /// Using this method to encrypt the given format using AES algorithm
       /// </summary>
       /// <param name="toBeEncrypted">byte array to be encrypted</param>
       /// <param name="passwordBytes">password to encrypt with byte array</param>
       /// <returns>return the encrypted byte array</returns>
       private static byte[] AES_Encrypt(byte[] PlainTextBytes, byte[] passwordBytes)
       {
           byte[] CipherTextBytes = null;

               //The Initialization Vector (IV) is an unpredictable random number used to make sure that when the same message is encrypted twice, the ciphertext always different.
               byte[] InitialVectorBytes = Encoding.ASCII.GetBytes(InitialVector);

               //It is a random number that is needed to access the encrypted data, along with the password.
               //If an attacker does not know the password, and is trying to guess it with a brute-force attack, then every password he tries has to be tried with each salt value.
               byte[] SaltValueBytes = Encoding.ASCII.GetBytes(Salt);

              // byte[] PlainTextBytes = Encoding.UTF8.GetBytes(toBeEncrypted);

               //Derives a key from a password using an extension of the PBKDF1 algorithm.
               PasswordDeriveBytes DerivedPassword = new PasswordDeriveBytes(passwordBytes, SaltValueBytes, HashAlgorithm, PasswordIterations);
               
               byte[] KeyBytes = DerivedPassword.GetBytes(KeySize / 8);

               // Create a new instance of the RijndaelManaged 
               // class.  This generates a new key and initialization  
               // vector (IV). 
               using (RijndaelManaged SymmetricKey = new RijndaelManaged())
               {
                   //set the Cipher Mode as Cipher Block Chaining(CBC)
                   SymmetricKey.Mode = CipherMode.CBC;

                   // Create a encryptor to perform the stream transform.
                   using (ICryptoTransform Encryptor = SymmetricKey.CreateEncryptor(KeyBytes, InitialVectorBytes))
                   {
                       // Create the streams used for encryption. 
                       using (MemoryStream MemStream = new MemoryStream())
                       {
                           //It enables an application to encrypt and decrypt data to and from another stream
                           using (CryptoStream CryptoStream = new CryptoStream(MemStream, Encryptor, CryptoStreamMode.Write))
                           {
                               CryptoStream.Write(PlainTextBytes, 0, PlainTextBytes.Length);
                               CryptoStream.FlushFinalBlock();
                               CipherTextBytes = MemStream.ToArray();
                               MemStream.Close();
                               CryptoStream.Close();
                           }
                       }
                   }
                   SymmetricKey.Clear();
               }
               
               return CipherTextBytes;
       }

       /// <summary>
       /// Using this method to decrypt the given format using AES algorithm
       /// </summary>
       /// <param name="CipherText">byte array need to be decrypted</param>
       /// <param name="Password">password to decrypt the byte array</param>
       /// <returns>return decrypted byte array</returns>
       private static byte[] AES_Decrypt(byte[] CipherTextBytes, byte[] Password)
       {
           byte[] DecryptedTextBytes = null;
           //if (string.IsNullOrEmpty(CipherText))
           //    return "";
           //The Initialization Vector (IV) is an unpredictable random number used to make sure that when the same message is encrypted twice, the ciphertext always different.
           byte[] InitialVectorBytes = Encoding.ASCII.GetBytes(InitialVector);

           //It is a random number that is needed to access the encrypted data, along with the password.
           //If an attacker does not know the password, and is trying to guess it with a brute-force attack, then every password he tries has to be tried with each salt value.
           byte[] SaltValueBytes = Encoding.ASCII.GetBytes(Salt);

           //Derives a key from a password using an extension of the PBKDF1 algorithm.
           PasswordDeriveBytes DerivedPassword = new PasswordDeriveBytes(Password, SaltValueBytes, HashAlgorithm, PasswordIterations);
           
           byte[] KeyBytes = DerivedPassword.GetBytes(KeySize / 8);

           byte[] PlainTextBytes = new byte[CipherTextBytes.Length];
           int ByteCount = 0;

           // Create a new instance of the RijndaelManaged 
           // class.  This generates a new key and initialization  
           // vector (IV). 
           using (RijndaelManaged SymmetricKey = new RijndaelManaged())
           {
               //set the Cipher Mode as Cipher Block Chaining(CBC)
               SymmetricKey.Mode = CipherMode.CBC;

               // Create a decrytor to perform the stream transform.
               using (ICryptoTransform Decryptor = SymmetricKey.CreateDecryptor(KeyBytes, InitialVectorBytes))
               {
                   // Create the streams used for decryption.
                   using (MemoryStream MemStream = new MemoryStream(CipherTextBytes))
                   {
                       //It enables an application to encrypt and decrypt data to and from another stream
                       using (CryptoStream CryptoStream = new CryptoStream(MemStream, Decryptor, CryptoStreamMode.Read))
                       {
                           ByteCount = CryptoStream.Read(PlainTextBytes, 0, PlainTextBytes.Length);
                           MemStream.Close();
                           string text = Encoding.UTF8.GetString(PlainTextBytes,
                                                       0,
                                                       ByteCount);
                           DecryptedTextBytes = Encoding.ASCII.GetBytes(text);
                           CryptoStream.Close();
                       }
                   }
               }
               SymmetricKey.Clear();
           }
           return DecryptedTextBytes;
       }

       #endregion

    }
}

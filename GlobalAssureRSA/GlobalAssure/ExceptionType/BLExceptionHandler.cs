﻿using GlobalAssure;
using ExceptionTypes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.ExceptionHandling.ExceptionHandler
{
   public static class BLExceptionHandler
    {
        public static bool HandleException(ref System.Exception ex)
        {
            bool rethrow = false;

            if (ex is BaseException)
            {

                rethrow = ExceptionPolicy.HandleException(ex, CrossCutting_Constant.PassThroughPolicy);
               // ex = new PassThroughException(ex.Message);
            }
            else
            {
                rethrow = ExceptionPolicy.HandleException(ex, CrossCutting_Constant.BLPolicy);
                if (rethrow)
                {
                    throw ex;
                }
            }

            return rethrow;
        }

    }
}

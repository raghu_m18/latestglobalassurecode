﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using GlobalAssure;

namespace CrossCutting.ExceptionHandling.ExceptionHandler
{
     public static class BaseExceptionHandler
    {
        public static bool HandleException(ref System.Exception ex)
        {
            bool rethrow = false;

            if (ex is System.Exception)
            {
                try
                {
                    rethrow = ExceptionPolicy.HandleException(ex, CrossCutting_Constant.RepositoryPolicy);
                    if (rethrow)
                    {
                        throw ex;
                    }
                }
                catch(Exception)
                {
                    return false;
                }
            }
            return rethrow;
        }
    
    }
}

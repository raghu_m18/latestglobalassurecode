﻿using ExceptionTypes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
//using Microsoft.Practices.Unity.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.ExceptionHandling.ExceptionHandler
{
    public class SemanticLoggingErrorHandler : IExceptionHandler
    {
        private Action<string, string, string, string> writeToLog;
        private Func<TextWriter, Exception, Guid, ExceptionFormatter> formatterCreator;

        public SemanticLoggingErrorHandler(Action<string, string, string, string> writeToLog)
            : this(writeToLog, null)
        {
        }

        public SemanticLoggingErrorHandler(Action<string, string, string, string> writeToLog, Type formatterType)
        {
           // Guard.ArgumentNotNull(writeToLog, "writeToLog");

            if (formatterType != null)
            {
                if (!typeof(ExceptionFormatter).IsAssignableFrom(formatterType))
                {
                    throw new ArgumentOutOfRangeException("formatterType", "formatterType must be of type ExceptionFormatter");
                }

                this.formatterCreator = GetFormatterCreator(formatterType);
            }

            this.writeToLog = writeToLog;
        }

        public Exception HandleException(Exception exception, Guid handlingInstanceId)
        {
            writeToLog(exception.GetType().FullName,
            exception.Message,
            exception.StackTrace,
            CreateMessage(exception, handlingInstanceId));
            exception = new BaseException();
            return exception;
        }

        private string CreateMessage(Exception exception, Guid handlingInstanceID)
        {
            if (formatterCreator != null)
            {
                using (StringWriter writer = new StringWriter(CultureInfo.InvariantCulture))
                {
                    ExceptionFormatter formatter = formatterCreator(writer, exception, handlingInstanceID);

                    if (formatter != null)
                    {
                        formatter.Format();
                    }

                    return writer.ToString();
                }
            }
            else
            {
                return string.Empty;
            }
        }

        private Func<TextWriter, Exception, Guid, ExceptionFormatter> GetFormatterCreator(Type formatterType)
        {
            ConstructorInfo ctor = formatterType.GetConstructor(new Type[] { 
                typeof(TextWriter), typeof(Exception), typeof(Guid) });

            if (ctor == null)
            {
                throw new ExceptionHandlingException(
                    string.Format("The configured exception formatter '{0}' must expose a public constructor that takes a TextWriter object, an Exception object and a GUID instance as parameters.",
                        formatterType.AssemblyQualifiedName));
            }

            ParameterExpression[] argsExp = new ParameterExpression[]
            { 
                Expression.Parameter(typeof(TextWriter)),
                Expression.Parameter(typeof(Exception)),
                Expression.Parameter(typeof(Guid))
            };

            NewExpression newExp = Expression.New(ctor, argsExp);

            var lambda = Expression.Lambda<Func<TextWriter, Exception, Guid, ExceptionFormatter>>(newExp, argsExp);
            formatterCreator = lambda.Compile();

            return formatterCreator;
        }
    }
}

﻿using GlobalAssure;
using ExceptionTypes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.ExceptionHandling.ExceptionHandler
{
  public static  class FacadeExceptionHandler
    {
        public static bool HandleException(ref System.Exception ex)
        {
            bool rethrow = false;

            if (ex is BaseException)
            {

                rethrow = ExceptionPolicy.HandleException(ex, CrossCutting_Constant.PassThroughPolicy);
                //ex = new PassThroughException(ex.Message);
            }
            else
            {
               // ex = new BaseException();
                rethrow = ExceptionPolicy.HandleException(ex, CrossCutting_Constant.FacadePolicy);
                if (rethrow)
                {
                    throw ex;
                }
            }

            return rethrow;
        }
    }
}

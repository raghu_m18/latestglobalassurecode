﻿using GlobalAssure;
using ExceptionTypes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.ExceptionHandling.ExceptionHandler
{
   public static class UIExceptionHandler
    {
       public static bool HandleException(ref System.Exception ex)
       {
           bool rethrow = false;
           try
           {
               if (ex is BaseException)
               {
                   return false;
               }
               else
               {
                   rethrow = ExceptionPolicy.HandleException(ex, CrossCutting_Constant.UIPolicy);
                   return rethrow;
               }
           }
           catch (Exception exp)
           {
               ex = exp;
               return false;
           }
       }
    }
}

﻿using GlobalAssure;
using CrossCutting.ExceptionHandling.ExceptionHandler;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.SemanticLogging;
using Microsoft.Practices.EnterpriseLibrary.SemanticLogging.Sinks;
using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossCutting.ExceptionHandling.ExceptionHandler
{
    public static class ExceptionConfig
    {
        /// <summary>
        /// Need to register ExceptionManager and Initialize Semantic logging
        /// </summary>
        public static void Configuration()
        {
            //IConfigurationSource source = ConfigurationSourceFactory.Create();

            //var logwriterFactory = new LogWriterFactory(source);
            //var logWriter = logwriterFactory.Create();
            //Logger.SetLogWriter(logWriter);

            //var exceptionPolicyFactory = new ExceptionPolicyFactory(source);
            //var exceptionManager = exceptionPolicyFactory.CreateManager();
            //ExceptionPolicy.SetExceptionManager(exceptionManager);

            InitializeSemanticLogging(LogTo.RollingFlatFile);
            ExceptionManager exceptionManager = CreateExceptionManager();
            ExceptionPolicy.SetExceptionManager(exceptionManager);
        }

        /// <summary>
        /// Initialize the semantic logging
        /// </summary>
        /// <param name="LogTo">Need to give where to store log</param>
        public static void InitializeSemanticLogging(int LogTo)
        {
            // string connectionString = @"Data Source=ISSLT001\SQLEXPRESS;Initial Catalog=Logging;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";

            var listener1 = new ObservableEventListener();
            listener1.EnableEvents(ClaimsLiveEventSource.Log, EventLevel.LogAlways, ClaimsLiveEventSource.Keywords.DataBase);

            if (LogTo == 3)
            {
                listener1.LogToConsole();
            }
            //else if (LogTo == 1)
            //{
            //    var subscription = listener1.LogToRollingFlatFile("Claims Live", System.Configuration.ConfigurationManager.AppSettings["SemanticLogging"].ToString());
            //}
            else if (LogTo == 2)
            {
                var subscription1 = listener1.LogToRollingFlatFile(System.Configuration.ConfigurationManager.AppSettings["RollingFileErrorLog"].ToString(), 10000, "dd-mm-yyyy", RollFileExistsBehavior.Increment, RollInterval.Day);
                subscription1.Sink.FlushAsync().Wait();
            }
            else
            {
                listener1.LogToFlatFile(System.Configuration.ConfigurationManager.AppSettings["FlatFileErrorLog"].ToString());
            }

            //   listener1.Dispose();
            //   listener2.Dispose();

        }

        /// <summary>
        /// Register Exception manager and policy
        /// </summary>
        /// <returns></returns>
        public static ExceptionManager CreateExceptionManager()
        {
            var policies = new List<ExceptionPolicyDefinition>();

            //----------------------------------------------------------
            var policyEntries = new List<ExceptionPolicyEntry>
              {
                  {
                      new ExceptionPolicyEntry(typeof(Exception),
                          PostHandlingAction.None,
                          new IExceptionHandler[]
                            {
                              // Pass in Action<> for handler to use to invoke semantic logging
                              new SemanticLoggingErrorHandler(ClaimsLiveEventSource.Log.LogException
                                  , typeof(TextExceptionFormatter))
                            })
                  }
              };
            policies.Add(new ExceptionPolicyDefinition(
             CrossCutting_Constant.RepositoryPolicy, policyEntries));

            //----------------------------------------------------------
            var policyPassThrough = new List<ExceptionPolicyEntry>
              {
                  {
                      new ExceptionPolicyEntry(typeof(Exception),
                          PostHandlingAction.None,
                          new IExceptionHandler[]
                            {
                            })
                  }
              };

            policies.Add(new ExceptionPolicyDefinition(
             CrossCutting_Constant.PassThroughPolicy, policyPassThrough));
            //----------------------------------------------------------
            var policyUI = new List<ExceptionPolicyEntry>
              {
                  {
                      new ExceptionPolicyEntry(typeof(Exception),
                          PostHandlingAction.None,
                          new IExceptionHandler[]
                            {
                                 new SemanticLoggingErrorHandler(ClaimsLiveEventSource.Log.LogException
                                  , typeof(TextExceptionFormatter))
                            })
                  }
              };

            policies.Add(new ExceptionPolicyDefinition(
             CrossCutting_Constant.UIPolicy, policyUI));
            //------------------------------------------------------------

            var policyControllerLogic = new List<ExceptionPolicyEntry>
              {
                  {
                      new ExceptionPolicyEntry(typeof(Exception),
                          PostHandlingAction.None,
                          new IExceptionHandler[]
                            {
                                 new SemanticLoggingErrorHandler(ClaimsLiveEventSource.Log.LogException
                                  , typeof(TextExceptionFormatter))
                            })
                  }
              };

            policies.Add(new ExceptionPolicyDefinition(
           CrossCutting_Constant.ControllerLogicPolicy, policyControllerLogic));
            //------------------------------------------------------------

            var policyAM = new List<ExceptionPolicyEntry>
               {
                   {
                       new ExceptionPolicyEntry(typeof(Exception),
                           PostHandlingAction.None,
                           new IExceptionHandler[]
                             {
                                  new SemanticLoggingErrorHandler(ClaimsLiveEventSource.Log.LogException
                                   , typeof(TextExceptionFormatter))
                             })
                   }
               };

            policies.Add(new ExceptionPolicyDefinition(
              CrossCutting_Constant.AMPolicy, policyAM));
            //------------------------------------------------------------

            var policyBL = new List<ExceptionPolicyEntry>
               {
                   {
                       new ExceptionPolicyEntry(typeof(Exception),
                           PostHandlingAction.None,
                           new IExceptionHandler[]
                             {
                                  new SemanticLoggingErrorHandler(ClaimsLiveEventSource.Log.LogException
                                   , typeof(TextExceptionFormatter))
                             })
                   }
               };

            policies.Add(new ExceptionPolicyDefinition(
             CrossCutting_Constant.BLPolicy, policyBL));
            //------------------------------------------------------------
            var policyFacade = new List<ExceptionPolicyEntry>
               {
                   {
                       new ExceptionPolicyEntry(typeof(Exception),
                           PostHandlingAction.None,
                           new IExceptionHandler[]
                             {
                                  new SemanticLoggingErrorHandler(ClaimsLiveEventSource.Log.LogException
                                   , typeof(TextExceptionFormatter))
                             })
                   }
               };

            policies.Add(new ExceptionPolicyDefinition(
              CrossCutting_Constant.FacadePolicy, policyFacade));

            //-------------------------------------------------------------

            var policySI = new List<ExceptionPolicyEntry>
               {
                   {
                       new ExceptionPolicyEntry(typeof(Exception),
                           PostHandlingAction.None,
                           new IExceptionHandler[]
                             {
                                  new SemanticLoggingErrorHandler(ClaimsLiveEventSource.Log.LogException
                                   , typeof(TextExceptionFormatter))
                             })
                   }
               };

            policies.Add(new ExceptionPolicyDefinition(
              CrossCutting_Constant.ServiceSIPolicy, policySI));

            //-------------------------------------------------------------

            var policyHost = new List<ExceptionPolicyEntry>
               {
                   {
                       new ExceptionPolicyEntry(typeof(Exception),
                           PostHandlingAction.None,
                           new IExceptionHandler[]
                             {
                                  new SemanticLoggingErrorHandler(ClaimsLiveEventSource.Log.LogException
                                   , typeof(TextExceptionFormatter))
                             })
                   }
               };

            policies.Add(new ExceptionPolicyDefinition(
              CrossCutting_Constant.ServiceHostPolicy, policyHost));

            //------------------------------------------------------------
            ExceptionManager exceptionManager = new ExceptionManager(policies);

            return exceptionManager;
        }
    }

    public class LogTo
    {
        //int num = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Not_To_Log"]);
        // public const int NotToLog = (int)num;
        public const int DataBase = (int)1;
        public const int RollingFlatFile = (int)2;
        public const int DebugWindow = (int)3;
    }
}

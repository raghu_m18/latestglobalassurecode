﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ParentChildMISReport.aspx.cs" Inherits="GlobalAssure.Admin.ParentChildMISReport1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
      <div class="col-lg-12" style="margin-top: 10px">
        <div class="col-lg-12 text-center">
            <span>Download Parent Child MIS Report</span>            
        </div>    
           <div class="col-lg-12 text-center">
               <asp:Button Text="Download" ID="btnDownload" OnClick="btnDownload_Click" CssClass="btn btn-primary" runat="server" />
        </div>  
    </div>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="MakeModelMaster.aspx.cs" Inherits="GlobalAssure.Admin.MakeModelMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
    <div class="col-lg-12" style="margin-top:30px">
        <div class="col-lg-3">
            <span>Select Make</span>
            <asp:DropDownList ID="DDLMake" runat="server" CssClass="form-control" OnSelectedIndexChanged="DDLMake_SelectedIndexChanged" AutoPostBack="true">               
            </asp:DropDownList>
        </div>
        <div class="col-lg-3">
             <span>Select Model</span>
                <asp:DropDownList ID="DDlModel" runat="server" CssClass="form-control">                    
            </asp:DropDownList>
        </div>
          <div class="col-lg-3" style="margin-top:20px">             
              <asp:Button Text="Search" ID="btnSearch" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnSearch_Click" />
                <asp:Button Text="Reset" ID="btnReset" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnReset_Click" />
        </div>
         <div class="row align-items-center mb-30 pt-30" style="padding-top: 80px">
        <div class="col-lg-12" id="dvGridMakeModel" runat="server">
            <div class="bg-white table-responsive rounded shadow-sm mb-30" style="width: 100%">
                <asp:GridView ID="gvMakeModel" CssClass="table mb-0 table-striped" CellSpacing="0" Width="100%" runat="server" AutoGenerateColumns="false"
                    AllowPaging="True"  OnRowDataBound="gvMakeModel_RowDataBound" OnPageIndexChanging="gvMakeModel_PageIndexChanging"
                    DataKeyNames="MakeID,ProductID,MakeName,ModelID,ModelName" OnRowCommand="gvMakeModel_RowCommand" PageSize="20">
                    <Columns>       
                         <asp:BoundField DataField="ProductID" HeaderText="Product ID" />
                        <asp:BoundField DataField="MakeID" HeaderText="Make ID" />                       
                         <asp:BoundField DataField="MakeName" HeaderText="Make Name" />
                         <asp:BoundField DataField="ModelID" HeaderText="Model ID" />
                         <asp:BoundField DataField="ModelName" HeaderText="Model Name" />                        
                    </Columns>
                </asp:GridView>
            </div>
        </div>        
    </div>
         <div class="text-center" style="margin-top:20px">             
              <asp:Button Text="Download" ID="btnDownload" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnDownload_Click"/>

         </div>
    </div>
</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="StateCityMaster.aspx.cs" Inherits="GlobalAssure.Admin.StateCityMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
    <div class="col-lg-12" style="margin-top:30px">
        <div class="col-lg-3">
            <span>Select State</span>
            <asp:DropDownList ID="DDLState" runat="server" CssClass="form-control" OnSelectedIndexChanged="DDLState_SelectedIndexChanged" AutoPostBack="true">               
            </asp:DropDownList>
        </div>
        <div class="col-lg-3">
             <span>Select City</span>
                <asp:DropDownList ID="DDLCity" runat="server" CssClass="form-control">                    
            </asp:DropDownList>
        </div>
          <div class="col-lg-3" style="margin-top:20px">             
              <asp:Button Text="Search" ID="btnSearch" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnSearch_Click"/>
                <asp:Button Text="Reset" ID="btnReset" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnReset_Click" />
        </div>
         <div class="row align-items-center mb-30 pt-30" style="padding-top: 80px">
        <div class="col-lg-12" id="dvGridMakeModel" runat="server">
            <div class="bg-white table-responsive rounded shadow-sm mb-30" style="width: 100%">
                <asp:GridView ID="gvStateCity" CssClass="table mb-0 table-striped" CellSpacing="0" Width="100%" runat="server" AutoGenerateColumns="false"
                    AllowPaging="True"  OnRowDataBound="gvStateCity_RowDataBound" OnPageIndexChanging="gvStateCity_PageIndexChanging"
                    DataKeyNames="StateID,State,CityID,City" OnRowCommand="gvStateCity_RowCommand" PageSize="20">
                    <Columns>       
                         <asp:BoundField DataField="StateID" HeaderText="State ID" />
                        <asp:BoundField DataField="State" HeaderText="State Name" />                       
                         <asp:BoundField DataField="CityID" HeaderText="City ID" />
                         <asp:BoundField DataField="City" HeaderText="City" />                                              
                    </Columns>
                </asp:GridView>
            </div>
        </div>        
    </div>
        <div class="text-center" style="margin-top:20px">             
              <asp:Button Text="Download" ID="btnDownload" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnDownload_Click"/>

         </div>
    </div>
</asp:Content>

﻿

jQuery.fn.serializeObject = function () {
    var arrayData, objectData;
    arrayData = this.serializeArray();
    objectData = {};
    $.each(arrayData, function () {
        var value;

        if (this.value != null) {
            value = this.value;
        } else {
            value = '';
        }

        if (objectData[this.name] != null) {
            if (!objectData[this.name].push) {
                objectData[this.name] = [objectData[this.name]];
            }

            objectData[this.name].push(value);
        } else {
            objectData[this.name] = value;
        }
    });
    return objectData;
};

//added method for checknumbers,one dot 
function isNumberKeyWithDot(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46) {
        var value = event.srcElement.value;
        if ((value.indexOf('.') > -1 && event.keyCode == 46)) {//ChecKing wether '.' is eixst in value
            return false;
        }
        else {
            return true;
        }
    }
    return false;
}

//For Enable/Disable of controls
function EnableDisableAllControls(controlID, Enable) {
    EnableAllButtons(controlID, Enable);
    var divControls = $("#" + controlID + " input[type=text]").attr("readonly", !Enable);
    var divControls = $("#" + controlID + " input[type=checkbox]").attr("disabled", !Enable);
    $("#" + controlID + " input[type=radio]").attr("disabled", !Enable);
    $("#" + controlID + " [data-role=dropdownlist]").each(function (index) {
        //  $(this).css("backgroundColor", "#EEE");
        $(this).data("kendoDropDownList").enable(Enable);
    });
    $("#" + controlID + " [data-role=combobox]").each(function (index) {
        //  $(this).css("backgroundColor", "#EEE");
        $(this).data("kendoComboBox").enable(Enable);
    });
    $("#" + controlID + " [data-role=datepicker]").each(function (index) {
        //  $(this).css("backgroundColor", "#EEE");
        $(this).data("kendoDatePicker").enable(Enable);
    });
    $("#" + controlID + " [data-role=grid]").each(function (index) {
        //  $(this).css("backgroundColor", "#EEE");
        var id = $(this).attr('id');
        if (Enable) {
            enableKendoGrid(id);
        }
        else {
            disableKendoGrid(id);
        }
    });

    $("#" + controlID + " textarea").attr("disabled", "disabled");
    $("#" + controlID + "[data-role=listview]").each(function (index) {
        var id = $(this).attr('id');
        // $("#" + id).data("kendoListView").enable(Enable);
    });
}

//Enable.Disable buttons
function EnableAllButtons(controlID, Enable) {
    if (Enable == false) {
        $("#" + controlID + "  input[type=button]").attr("disabled", "disabled");
    }
    else {
        $("#" + controlID + "  input[type=button]").attr("disabled", false);
    }
}

function formatCurrency(e) {
    var controlId = e.id;
    var nStr = $("#" + controlId).val();
    if (nStr == "") {
        return;
    }
    var x3 = '';
    var str = nStr.toString();
    str = str.replace(/,/g, "");;
    var negVal = str.split('-');
    if (negVal[1] == undefined) {
        str = negVal[0];
    }
    else {
        str = negVal[1];
        x3 = '-';
    }

    str += '';
    x = str.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    var z = 0;
    var len = String(x1).length;
    var num = parseInt((len / 2) - 1);

    while (rgx.test(x1)) {
        if (z > 0) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        else {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
            rgx = /(\d+)(\d{2})/;
        }
        z++;
        num--;
        if (num == 0) {
            break;
        }
    }
    $("#" + controlId).val(x3 + x1 + x2);
    //return x3 + x1 + x2;
}

function isNumberKey(evt) {
    // debugger
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode < 48 || charCode > 57)) {
        // writeMultipleMessage("error", "Enter Number", "currency");
        return false;
    }

    return true;
}

$(document).on('keyup input', ".testClass", function (e) {
    this.value = this.value.replace(/[^0-9\.]/g, '');
})


function ispercentage(evt, element) {
    // debugger
    var text = $("#" + element.id).val();
    var length = text.length;
    var degit = text.slice(-3);
    var lastChar2 = degit.charAt(1); var count = 0;
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode == 46 || text.slice(-1) == "." || lastChar2 == ".") && text != "100")
        if (charCode == 46)
            for (var i = 0; i < text.length; i++) {
                if (text[i] == '.') count++;
                if (count == 1) return false;
            }
        else if ((charCode >= 48 && charCode <= 57) || charCode == 46)
            return true; else return false;
    else if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105))
        return false;
    else if ((text.charAt(0) == "." || text.charAt(0) == "") && length < 3 && text != "100")
        if ((charCode >= 48 && charCode <= 57) || charCode == 46)
            return true; else return false;
    else if (text < 99 && length < 3 && (charCode >= 48 && charCode <= 57))
        if (length >= 2)
            if (charCode == 48 && text == "10") return true; else return false; else return true;
    else { return false; }
}

function isDecimalOnly(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (
        (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function ValidateTextBox(Id, Message) {
    var result = document.getElementById(Id).value;
    if (result.trim() == "" || result.trim() == null) {
        alert(Message);
        Id.focus();
        return false;
    }
    return true;
}

function ValidateDropdown(Id, Message) {
    var result = document.getElementById(Id).value;
    if (result == "0" || result == null) {
        alert(Message);
        return false;
    }
    return true;
}

//Generic Ajax Call
function BindAjaxCall(e) {
    var Settings = e;
    var id = Settings.Id;
    var ajaxrequest = $.ajax({
        url: Settings.url,
        data: Settings.data,

        type: Settings.type,
        datatype: "json",
    });
    ajaxrequest.done(function (data) {
        id.empty().append(data);
        var count = 1;
        var res = $.parseHTML(data);
        $.each(res, function (i, root) {
            if (root.nodeName.toLowerCase() == "table") {
                $.each(root.children, function (j, child) {
                    if (child.nodeName.toLowerCase() == "tbody") {
                        count = child.children.length;
                    }
                });
            }
        });

        if (data == null || data == '' || count == 0) {
            $("#" + id.children("table").prop("id")).hide();
        }
        else {
            $("#" + id.children("table").prop("id")).show();
            id.empty().append(data);
        }
    });
    ajaxrequest.fail(function (jqXHR, textStatus, errorThrown) {
        alert(textStatus + ': ' + errorThrown);
    });
    return false;
}

//Generic Ajax Dropdown Call
function BindAjaxCallDropdown(e) {

    var Settings = e;
    var id = Settings.Id;
    var otherId = Settings.otherId;
    var ajaxrequest = $.ajax({
        url: Settings.url,
        datatype: "json",
        type: Settings.type,
        data: Settings.data
    });
    ajaxrequest.done(function (result) {
        var option = '<option value="">Select</option>';

        for (var j = 0; j < otherId.length; j++) {
            $(otherId[j]).empty().append(option);
        }

        for (var i = 0; i < result.length; i++) {
            option += '<option value="' + result[i].Value + '">' + result[i].Text + '</option>';
        }
        id.empty().append(option);
    });
    ajaxrequest.fail(function (jqXHR, textStatus, errorThrown) {
        alert(textStatus + ': ' + errorThrown);
    });
    return false;
}

function SaveData(e) {
    var Settings = e;
    var ajaxrequest = $.ajax({
        url: Settings.url,
        data: Settings.data,
        type: Settings.type,
        datatype: "json",
    });
    ajaxrequest.done(function (data) {

    });
    ajaxrequest.fail(function (jqXHR, textStatus, errorThrown) {
        alert(textStatus + ': ' + errorThrown);
    });
    return false;
}

function ValidationMessage(e) {
    this.Settings = e;
    $.ajax({
        url: this.Settings.apiurl,
        data: JSON.stringify(this.Settings.data),
        type: "POST",
        datatype: "json",
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $("#errorModal .modal-body").empty();
            $("#errorModal .modal-body").append(data);
            $("#errorModal").modal("show");
            return false;
        },
        error: function (xhr, status, err) {
        },
    });
}

function AlertMessage(msg, timeout, eventType) {

    var arr = msg.split('//');
    $('#divTransactionResult').html('');
    if (eventType == 'suc' || arr[0] == 'G')
        $('#divTransactionResult').append('<div class="alert alert-success" style="z-index:3000;position:fixed;width:85%;bottom:0%"><a class="close" data-dismiss="alert">×</a><strong>Success!</strong><p style="font-weight:bold">' + arr[1] + '</p></div>');
    else if (eventType == 'fail' || arr[0] == 'R')
        $('#divTransactionResult').append('<div class="alert alert-danger" style="z-index:3000;position:fixed;width:85%;bottom:0%"><a class="close" data-dismiss="alert">×</a><strong>Error!</strong><p style="font-weight:bold">' + arr[1] + '</p></div>');
    else if (eventType == 'warn' || arr[0] == 'W')
        $('#divTransactionResult').append('<div class="alert alert-warning" style="z-index:3000;position:fixed;width:85%;bottom:0%"><a class="close" data-dismiss="alert">×</a><strong>Warning!</strong><p style="font-weight:bold">' + arr[1] + '</p></div>');
    alertTimeout(timeout);

}

function alertTimeout(wait) {
    //debugger
    setTimeout(function () {
        $('#divTransactionResult').children('.alert:first-child').remove()
    }, wait);
}

function ShowValidationMessage(result) {
    if (result.Valid == false) {
        if (result.Errors != null) {
            $.each(result.Errors, function (key, value) {
                if (value != null) {
                    var container = $('span[data-valmsg-for="' + key + '"]');
                    container.removeClass("field-validation-valid").addClass("field-validation-error");
                    container.html(value[value.length - 1].ErrorMessage);
                    $('.field-validation-error').css('color', 'red');
                }
            });
        }
    }
}

function DisplayValidationMessages(result) {
    //debugger
    if (result.Errors != null) {
        $.each(result.Errors, function (key, value) {
            //debugger
            if (value != null) {
                // debugger
                var container = $('span[data-valmsg-for="' + value.Key + '"]');
                container.removeClass("field-validation-valid").addClass("field-validation-error");
                container.html(value.Value);
                $('.field-validation-error').css('color', 'red').show();
            }
        });
    }

}

function HideValidationMessages(result) {
    //debugger
    if (result.Errors != null) {
        $.each(result.Errors, function (key, value) {
            //debugger
            if (value != null) {
                // debugger
                var container = $('span[data-valmsg-for="' + value.Key + '"]');
                container.addClass("field-validation-valid").removeClass("field-validation-error");
                container.html('');
                $('.field-validation-error').css('color', 'red').hide();
            }
        });
    }

}

//// retain stored values in database for drop down when click on edit button for grids
function selectPreviousValuesForDropdown(array, controlId, controlRowId) {
    var previousValue = $("#" + controlId + controlRowId).text();
    $.each(array, function (index, optionData) {
        // debugger
        if (optionData.value == previousValue) {
            $("#" + controlId + controlRowId).closest("div").find(".input-sm").append('<option selected="selected" value= ' + optionData.text + ' >' + optionData.value + '</option>')
        }
        else {
            $("#" + controlId + controlRowId).closest("div").find(".input-sm").append('<option  value= ' + optionData.text + '>' + optionData.value + '</option>')
        }
    });
}

$(document).ready(function () {

    $("input[type=text],textarea").hover(function (e) {
        e.preventDefault();
        var val = $(this).val();
        $(this).prop("title", val);
    });

    $("select").hover(function (e) {
        e.preventDefault();
        var val = $('option:selected', $(this)).text();
        $(this).prop("title", val);
    });


    $('input[type=text], textarea').keyup(function (e) {

        if (this.id != "UserName" && !$(this).hasClass("capsnotrequired")) {
            var input = $(this);
            var start = input[0].selectionStart;
            $(this).val(function (_, val) {
                return val.toUpperCase();
            });
            input[0].selectionStart = input[0].selectionEnd = start;
        }
    });

    //debugger
    //Method to avoid copy paste in the textbox if any text
    $('.isNumberClass').keyup(function (evt) {
        // debugger
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
        return true;
    });
});


//// decimalupto2 with positive number (oldvalue attribute is required)
$(document).on('keyup input', ".decimalupto2", function (e) {
    var re = /^\d*\.?\d{0,2}$/;

    var text = $(this).val();
    var isValid = (text.match(re) !== null);

    if (!isValid) {
        $(this).val($(this).attr("oldvalue"));
    }
    else {
        if (text == "0" || text == "-0") {
            $(this).val($(this).attr("oldvalue"));
        }
        else {
            $(this).attr("oldvalue", text);
        }
    }
});


$(document).on('keydown', ".allow_only_numbers", function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A,Ctrl+C,Ctrl+V, Command+A
      ((e.keyCode == 65 || e.keyCode == 86 || e.keyCode == 67) && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});







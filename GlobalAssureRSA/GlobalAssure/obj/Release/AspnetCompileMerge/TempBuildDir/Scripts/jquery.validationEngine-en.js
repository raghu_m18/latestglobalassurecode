(function ($) {
    $.fn.validationEngineLanguage = function () {
    };
    $.validationEngineLanguage = {
        newLang: function () {
            //debugger
            $.validationEngineLanguage.allRules = {
                "required": { // Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "* This field is required",
                    "alertTextCheckboxMultiple": "* Please select an option",
                    "alertTextCheckboxe": "* This checkbox is required",
                    "alertTextDateRange": "* Both date range fields are required"
                },
                "requiredInFunction": {
                    "func": function (field, rules, i, options) {
                        return (field.val() == "test") ? true : false;
                    },
                    "alertText": "* Field must equal test"
                },


                "AlphaNumberAndSpecialCharacters":
                {
                    "regex": /^[ A-Za-z0-9:_@./#&+:-]*$/,
                    "alertText": "* Invalid Format"
                },
                "CharwithColon":
              {
                  "regex": /^\\d+(:[A-Za-z0-9]+){2}$/,
                  "alertText": "* Invalid Format"
              },

                "dateRange": {
                    "regex": "none",
                    "alertText": "* Invalid ",
                    "alertText2": "Date Range"
                },
                "dateTimeRange": {
                    "regex": "none",
                    "alertText": "* Invalid ",
                    "alertText2": "Date Time Range"
                },
                "minSize": {
                    "regex": "none",
                    "alertText": "* Minimum ",
                    "alertText2": " digits required"
                },
                "maxSize": {
                    "regex": "none",
                    "alertText": "* Maximum ",
                    "alertText2": " digits allowed"
                },
                "groupRequired": {
                    "regex": "none",
                    "alertText": "* You must fill one of the following fields"
                },
                "MaxChar": {
                    "regex": "none",
                    "alertText": "* Maximum ",
                    "alertText2": " character allowed"
                },
                "MinChar": {
                    "regex": "none",
                    "alertText": "* Minimum ",
                    "alertText2": " character allowed"
                },

                "min": {
                    "regex": "none",
                    "alertText": "* Minimum value is "
                },
                "max": {
                    "regex": "none",
                    "alertText": "* Maximum value is "
                },
                "past": {
                    "regex": "none",
                    "alertText": "* Date prior to "
                },
                "future": {
                    "regex": "none",
                    "alertText": "* Date past "
                },
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* Maximum ",
                    "alertText2": " options allowed"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* Please select ",
                    "alertText2": " options"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "* Fields do not match"
                },
                "creditCard": {
                    "regex": "none",
                    "alertText": "* Invalid credit card number"
                },
                "phone": {
                    // credit: jquery.h5validate.js / orefalo
                    "regex": /^([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$/,
                    "alertText": "* Invalid phone number"
                },

                "email": {
                    // HTML5 compatible email regex ( http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#    e-mail-state-%28type=email%29 )
                    "regex": /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    "alertText": "* Invalid email address"
                },
                "ratio": {
                    "regex": /^[0-9]+:[0-9]+$/,
                    "alertText": "* Invalid ratio format"
                },

                "integer": {
                    "regex": /^\d+$/,
                    "alertText": "* Not a valid integer"
                },
                "letters": {
                    "regex": /^[a-z]+$/,
                    "alertText": "* Letters only please"
                },
                "LandLine": {
                    "regex": /^\d+$/,
                    "alertText": "* Not a valid LandLine Number"
                },
                "number": {
                    // Number, including positive, negative, and floating decimal. credit: orefalo
                    "regex": /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
                    "alertText": "* Invalid floating decimal number"
                },


                "Positivenumber": {
                    // Number, including positive, negative, and floating decimal. credit: orefalo
                    "regex": /^([0-9]+)$/,
                    "alertText": "* Invalid number"
                },

                "pincode": {
                    "regex": /^[0-9]{6}|[0-9]{5}-[0-9]{4}$/,
                    "alertText": "Enter Valid Pin Code (XXXXXX or XXXXX-XXXX)"
                },

                "VehicleRegistrations": {
                    "regex": /^[A-Z]{2}[0-9a-zA-Z]{2,3}[A-Z]{2,3}[0-9]{4}$/,
                    "alertText": "Enter valid vehicle number (eg: DL10CB1234)"
                },
                //"pincode": {
                //    "regex": "/^[0-9]{6}|[0-9]{5}-[0-9]{4}$/",
                //    "alertText": "Enter Valid pincode Code (XXXXXX or XXXXX-XXXX)"
                //},
                "date": {
                    //  Check if date is valid by leap year
                    "func": function (field) {
                        //var pattern = new RegExp(/^(\d{4})[\/\-\.](0?[1-9]|1[012])[\/\-\.](0?[1-9]|[12][0-9]|3[01])$/);
                        var pattern = new RegExp(/^(0?[1-9]|[12][0-9]|3[01])[\/\-\.](0?[1-9]|1[012])[\/\-\.](\d{4})$/);
                        var match = pattern.exec(field.val());
                        if (match == null)
                            return false;

                        //var year = match[1];
                        //var month = match[2]*1;
                        //var day = match[3]*1;
                        var year = match[3];
                        var month = match[2] * 1;
                        var day = match[1] * 1;
                        var date = new Date(year, month - 1, day); // because months starts from 0.

                        return (date.getFullYear() == year && date.getMonth() == (month - 1) && date.getDate() == day);
                    },
                    "alertText": "* Invalid date, must be in DD-MM-YYYY format"
                },

                "sumIsZero": {
                    //  Check if date is valid by leap year
                    "func": function (field) {
                        var data = field.val();
                        var flag = true;
                        var obj = {};
                        for (var z = 0; z < data.length; ++z) {
                            var ch = data[z];
                            if (ch == '0' || ch == 0) {
                                flag = false;
                            }
                            else {
                                return true;
                            }
                        }
                        return flag;

                    },
                    "alertText": "* all character should not be Zero"
                },

                "ipv4": {
                    "regex": /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
                    "alertText": "* Invalid IP address"
                },
                "url": {
                    "regex": /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
                    "alertText": "* Invalid URL"
                },
                "onlyNumberSp": {
                    "regex": /^[0-9\ ]+$/,
                    "alertText": "* Numbers only"
                },
                "StartNonZeroNumber": {
                    "regex": /^[1-9][0-9]*$/,
                    "alertText": "* Numbers only, should not start with zero"
                },

                "Currnecy": {
                    "regex": /^[0-9\.]+$/,
                    "alertText": "* Numbers only"
                },
                "onlyNumberIIBD": {
                    "regex": /^[0-9]+$/,
                    "alertText": "* Numbers only"
                },

                "decimalUpto2": {
                    "regex": /^(\d{1,9}|\d{0,9}\.\d{1,2})$/,
                    "alertText": "* Number upto two precision only"
                },
                "decimalGreaterThanOne": {
                    "regex": /^[1-9]+\d*(?:\.\d{1,2})?\s*$/,
                    "alertText": "* Value should be greater than one and can be upto two precision only"
                },
                "onlyPercentage": {
                    "regex": /^(100(\.0{1,2})?|[1-9]?\d(\.\d{1,2})?)$/,
                    "alertText": "* Number upto two precision only"
                },
                "onlyLetterSp": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* Letters only"
                },
                "onlyLetterSQ": {
                    "regex": /^[a-zA-Z'.,-]{0,150}$/,
                    "alertText": "* Invalid Name"
                },
                "Letterswithdot":
                {
                    "regex": /^ [a-z A-Z]{1,30}[.][a-z A-z]{1,30}$|[a-z A-Z]{1,30}[.][a-z A-z]{1,30}[.][a-z A-Z]{1,30}[.][a-z A-z]{1,30}$|[a-z A-Z]{1,30}[.][a-z A-z]{1,30}[.][a-z A-Z]{1,30}$|[a-z A-Z]{1,30}[.][a-z A-z]{1,30}[.][a-z A-Z]{1,30}[.][a-z A-z]{1,30}[.][a-z A-Z]{1,30}$ |[a-z A-Z]{1,30}$/,
                    "alertText": "* Invalid Name "
                },
                "AlphaComaDotHyphenApostrophe": {
                    "regex": /^[A-Za-z\ \' \- \. \, \-]+$/,
                    "alertText": "* Invalid Characters"
                },
                "NumberAndSpecialCharacters":
                    {
                        "regex": /^[\d \ \' \- \. \, \-]+$/,
                        "alertText": "* Invalid Format"
                    },


                "AlphaNumberAndSpecialCharacters":
                 {
                     "regex": /^[ A-Za-z0-9_@.:/#&+-]*$/,
                     "alertText": "* Invalid Format"
                 },
                "LetterNumberwithsp": {
                    "regex": /^[0-9a-zA-Z\ ]+$/,
                    "alertText": "* No special characters allowed"
                },
                "LetterNumberwithHyDash": {
                    "regex": /^[0-9a-zA-Z\- \ ]+$/,
                    "alertText": "* No special characters allowed"
                },
                "AlphaComaDotHyphenApostropheAnd": {
                    "regex": /^[A-Za-z\ \' \- \. \, \- \&]+$/,
                    "alertText": "* Invalid Characters"
                },
                "onlyLetterNumber": {
                    "regex": /^[0-9a-zA-Z]+$/,
                    "alertText": "* No special characters allowed"
                },
                //Added: Manali
                "policyNumberValidation": {
                    "regex": /^[0-9a-zA-Z\/\-]+$/,
                    "alertText": "* No special characters allowed"
                },
                // Validations added as requested by BA team
                // sample PAN BKEPS7278H 
                "pan": {
                    "regex": /^[A-Z]{5}\d{4}[A-Z]{1}$/,
                    "alertText": "*Pan Format:AAAAA9999A"
                },
                "GSTUIN": {
                    "regex": /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9a-zA-Z]{1}$/,
                    "alertText": "*GST UIN Format:22AAAAA9999A1Z5"
                },
                // sample ST AAACT0627RST308  
                "st": {
                    "regex": /^[A-Z]{5}\d{4}[A-Z]{3}\d{3}$/,
                    "alertText": "* ST Number Format:AAAAA9999AAA999"
                },
                "comaseperationPhone":
                    {
                        "regex": /^\d{10,15}(,\d{10,15})*$/,
                        "alertText": "10 to 15 character with comma separation"
                    },
                "comaseperationMobile":
                    {
                        "regex": /^\d{10,10}(,\d{10,10})*$/,
                        "alertText": "10 character with comma separation"
                    },

                "commasepemail": {
                    "regex": /^(\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[,]?\b)*$/,
                    "alertText": "Enter Valid Email with comma separation"

                },
                // sampleTAN AAAA12345A 
                "tan": {
                    "regex": /^[A-Z]{4}\d{5}[A-Z]{1}$/,
                    "alertText": "* TAN Number Format:AAAA99999A"
                },
                // sample Passport no H1234567 
                "passport": {
                    "regex": /^[A-Za-z]{1}\d{7}$/,
                    "alertText": "* Invalid Passport Number"
                },
                "custtext": {
                    "regex": /^[A-Za-z]{1,}[A-Za-z-'.,]{1,}$/,
                    "alertText": "* Invalid Test support only -'.,"
                },
                // --- CUSTOM RULES -- Those are specific to the demos, they can be removed or changed to your likings
                "ajaxUserCall": {
                    "url": "ajaxValidateFieldUser",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    "alertText": "* This user is already taken abc",
                    "alertTextLoad": "* Validating, please wait"
                },
                "ajaxUserCallPhp": {
                    "url": "phpajax/ajaxValidateFieldUser.php",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* This username is available",
                    "alertText": "* This user is already taken",
                    "alertTextLoad": "* Validating, please wait"
                },
                "ajaxNameCall": {
                    // remote json service location
                    "url": "ajaxValidateFieldName",
                    // error
                    "alertText": "* This name is already taken",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* This name is available",
                    // speaks by itself
                    "alertTextLoad": "* Validating, please wait"
                },
                "ajaxNameCallPhp": {
                    // remote json service location
                    "url": "phpajax/ajaxValidateFieldName.php",
                    // error
                    "alertText": "* This name is already taken",
                    // speaks by itself
                    "alertTextLoad": "* Validating, please wait"
                },
                "validate2fields": {
                    "alertText": "* Please input HELLO"
                },
                //tls warning:homegrown not fielded 
                "dateFormat": {
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/,
                    "alertText": "* Invalid Date"
                },
                //tls warning:homegrown not fielded 
                "dateTimeFormat": {
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1}$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^((1[012]|0?[1-9]){1}\/(0?[1-9]|[12][0-9]|3[01]){1}\/\d{2,4}\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1})$/,
                    "alertText": "* Invalid Date or Date Format",
                    "alertText2": "Expected Format: ",
                    "alertText3": "mm/dd/yyyy hh:mm:ss AM|PM or ",
                    "alertText4": "yyyy-mm-dd hh:mm:ss AM|PM"
                },
                //Added:Manali
                "nameValidation": {
                    "regex": /^[a-zA-Z\ '.,-]{0,150}$/,
                    "alertText": "* Invalid Name"
                },
                "customerIdValidation": {
                    "regex": /^[a-zA-Z0-9]{0,20}$/,
                    "alertText": "* Special Characters not allowed"
                },
                //Added by Ganesh for Provider Name
                "AlphaComaDotHyphenAposSpaceAmb": {
                    "regex": /^[A-Za-z\ \' \- \. \, \- \  \&]+$/,
                    "alertText": "* Invalid Characters"

                },

                "AlphaNumeric": {
                    "regex": /^[a-zA-Z0-9]*$/,
                    "alertText": "* Invalid Characters"

                },
                //Added by Aviwant for IFSC code 
                "ifsc": {
                    "regex": /^[A-Z|a-z]{4}[0][\d]{6}$/,
                    "alertText": "*IFSC Format:CITI0344444"   //IFSC Code normally contains 11 characters.In that first 4 characters are alphabets,5th character is 0 and next 6 characters are numerics
                },
                // Added by Ahamad for Password
                "password": {
                    "regex": /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
                    "alertText": "*Invalid password"
                                + "\n*should contain at least one digit"
                                + "\n*should contain at least one lower case"
                                + "\n*should contain at least one upper case"
                                + "\n*should contain at least 8 from the mentioned characters"
                },

                "password_confirm": {
                    minlength: 5,
                    equalTo: "#NewPassword"
                },


                // Added by Ahamad for Password
                "chequeNo": {
                    "regex": /^[0-9 \- \/]+$/,
                    "alertText": "* Invalid cheque number"
                },

                "name": {
                    "regex": /^[A-Za-z\&\']+$/,
                    "alertText": "* Invalid name \n special character & and ' only allowed"
                },

                "Lat": {
                    "regex": /^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$/,
                    "alertText": "* Invalid latitude or longitude\n"
                },

                "Long": {
                    "regex": /^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$/,
                    "alertText": "* Invalid latitude or longitude\n"
                },


                "CheckPANNo":
            {
                "func": function (field, rules, i, option) {

                    var PanNo = field.val();
                    var ret;
                    $.ajax({
                        type: "GET",
                        url: '../../Provider/IsPanNoUnique?PanNo=' + PanNo,
                        contentType: "application/json; charset=utf-8",
                        async: false,
                        dataType: "Json",
                        success: function (data) {

                            ret = data;
                        }
                    });
                    return ret;
                },
                "alertText": "* PanCard No. Already exists"

            },
                "avglength": {
                    "regex": /^[0-9]{1,2}[-][0-9]{1,2}$/,
                    "alertText": "* Invalid Avg length of stay"
                }
            };
        }
    };

    $.validationEngineLanguage.newLang();

})(jQuery);

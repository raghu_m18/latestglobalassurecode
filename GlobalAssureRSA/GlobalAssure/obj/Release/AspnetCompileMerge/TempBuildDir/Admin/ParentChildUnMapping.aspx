﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ParentChildUnMapping.aspx.cs" Inherits="GlobalAssure.Admin.ParentChildUnMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <h2 class="h2 text-left">Parent Child UnMapping</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
    <div class="col-lg-12" style="margin-top:30px">
        <div class="col-lg-3">
            <asp:DropDownList ID="ddlParent" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlParent_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
        </div>
        <div class="col-lg-6" style="margin-top: 10px">
            <span style="font-size: 18px">Parent :- </span>&nbsp;&nbsp;
            <asp:Label ID="lblParent" Text="" runat="server" />
            <asp:HiddenField ID="hdnParent" runat="server" />
        </div>
    </div>
    <div class="col-lg-12" style="margin-top: 50px">       
        <div class="col-lg-6" style="margin-top: 2px">
            <asp:Button Text="Search" ID="btnSearch" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnSearch_Click" />
        </div>
    </div>


    <div class="row align-items-center mb-30 pt-30" style="padding-top: 80px">
        <div class="col-lg-6" id="dvGridChid" runat="server">
            <div style="height: 250px; overflow: scroll">
            <div class="bg-white table-responsive rounded shadow-sm mb-30" style="width: 100%">
                <asp:GridView ID="gvChild" CssClass="table mb-0 table-striped" CellSpacing="0" Width="100%" runat="server" AutoGenerateColumns="false"
                     OnRowDataBound="gvChild_RowDataBound"
                    DataKeyNames="Name,Email" OnRowCommand="gvChild_RowCommand">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:BoundField DataField="Email" HeaderText="Email" />
                    </Columns>
                </asp:GridView>
            </div>
                </div>
            <asp:Button Text="Delete" ID="btnDelete" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure to delete?')" />
        </div>        
    </div>
    <script type="text/javascript">
        function Confirm() {
            confirm("Do you want to Delete!");
            return false;
        }

    </script>
</asp:Content>


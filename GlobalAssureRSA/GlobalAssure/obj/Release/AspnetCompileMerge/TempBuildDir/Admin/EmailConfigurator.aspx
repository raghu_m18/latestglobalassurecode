﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="EmailConfigurator.aspx.cs" Inherits="GlobalAssure.Admin._EmailConfigurator" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .col-md-3,
        .col-md-2,
        .col-md-1 {
            padding-top: 15px;
        }

        .ml15 {
            margin-left: 15px;
        }

        textarea {
            max-width: 100%;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script src="../ckeditor/ckeditor.js"></script>
    <script src="../ckeditor/adapters/jquery.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">

    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">

            <div class="form-group">
                <div>
                    <label>Action</label>
                    <span class="mandatory">*</span>
                </div>

                <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlAction_SelectedIndexChanged" CssClass="form-control">
                    <asp:ListItem Value="0">---Select---</asp:ListItem>
                    <asp:ListItem Value="1">Add / Edit Email Configuration</asp:ListItem>
                    <asp:ListItem Value="2">View Email Send Report </asp:ListItem>
                    <asp:ListItem Value="3">Add/ Edit Template </asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

    </div>



    <div class="panel panel-default row">
        <div class="panel-heading">
            <p class="active-class" style="color: #fff"><strong>Email Configurator</strong></p>

        </div>
        <div class="width-0 panel-body" style="background-color: #fff">
            <asp:MultiView ID="mvEmail" runat="server">
                <asp:View ID="vwEmailConfig" runat="server">
                    <div style="margin: 5px 5px 3px 49px">
                        <div class="row no-padding">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>SMTP Host</label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <input class="form-control" id="txtSMTPHostName" maxlength="150" type="text" runat="server" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>SMTP Port</label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <input class="form-control" id="txtSMTPPortNo" maxlength="5" type="text" runat="server" required>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>SMTP User Name</label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <input class="form-control" id="txtSMTPUserName" maxlength="150" type="text" runat="server" required>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>SMTP Password</label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <input class="form-control" id="txtSMTPPassword" maxlength="150" type="text" runat="server" required>
                                </div>
                            </div>



                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>From Email</label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <input class="form-control" id="txtFromEmail" maxlength="150" type="text" runat="server" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>From Name</label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <input class="form-control" id="txtFrom" maxlength="150" type="text" runat="server" required>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>Order No</label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <input class="form-control" id="txtOrderNo" maxlength="3" type="text" runat="server" required>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div>
                                        <label>Status</label>
                                        <span class="mandatory">*</span>
                                    </div>
                                    <label for="rdoActive">Active</label>
                                    <input id="rdoActive" maxlength="3" type="radio" runat="server" name="status" checked>

                                    <label for="rdoInActive" style="padding-left: 15px;">Inactive</label>
                                    <input id="rdoInActive" maxlength="3" type="radio" runat="server" name="status">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <div>
                                        <label>Secure SMTP</label>
                                        <span class="mandatory">&nbsp;</span>
                                    </div>
                                    <input id="chkIsSecure" maxlength="150" type="checkbox" runat="server">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row" style="padding-top: 10px; padding-bottom: 30px;padding-left:50px; border-bottom: 1px solid silver;">
                      
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>Email Limit Count</label>
                                        <span class="mandatory"></span>
                                    </div>
                                    <input class="form-control" id="txtEmailLimitCount" maxlength="5" type="text" runat="server" >
                                </div>
                            </div>
                        
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div>
                                        <label>SMS Trigger Count</label>
                                        <span class="mandatory"></span>
                                    </div>
                                    <input class="form-control" id="txtSMSTriggerCount" maxlength="5" type="text" runat="server" >
                                </div>
                            </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div>
                                    &nbsp;
                                </div>

                                <asp:Button ID="btnSaveEmailConfig" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveEmailConfig_Click" />

                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="ml15 btn btn-warn" OnClick="btnCancel_Click" formnovalidate />

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <asp:GridView ID="gvEmailConfig" CssClass="table mb-0 table-striped table-responsive" CellSpacing="0"
                                Width="100%" runat="server" AutoGenerateColumns="false" DataKeyNames="n_Email_Config_Id,s_SMTP_Host, s_SMTP_Port, s_SMTP_User_Name, s_SMTP_Password , s_From_Email , s_From_Name, n_SecureSMTP , n_OrderNo , n_Status,n_EmailLimitCount,n_SMSTriggerCount" OnRowCommand="gvEmailConfig_RowCommand" OnRowDataBound="gvEmailConfig_RowDataBound">

                                <Columns>
                                    <asp:BoundField DataField="n_Email_Config_Id" HeaderText="Config Id"></asp:BoundField>
                                    <asp:BoundField DataField="s_SMTP_Host" HeaderText="SMTP Host Name"></asp:BoundField>
                                    <asp:BoundField DataField="s_SMTP_Port" HeaderText="SMTP Port"></asp:BoundField>
                                    <asp:BoundField DataField="s_SMTP_User_Name" HeaderText="SMTP User Name"></asp:BoundField>
                                    <asp:BoundField DataField="s_SMTP_Password" HeaderText="SMTP Password"></asp:BoundField>
                                    <asp:BoundField DataField="s_From_Email" HeaderText="From Email"></asp:BoundField>
                                    <asp:BoundField DataField="s_From_Name" HeaderText="From Name"></asp:BoundField>
                                    <asp:BoundField DataField="n_SecureSMTP" HeaderText="Is Secure SMTP"></asp:BoundField>
                                    <asp:BoundField DataField="n_OrderNo" HeaderText="Order No"></asp:BoundField>
                                    <asp:BoundField DataField="n_EmailLimitCount" HeaderText="Email Limit Count"></asp:BoundField>
                                     <asp:BoundField DataField="n_CorrentCount" HeaderText="Email Current Count"></asp:BoundField>
                                    <asp:BoundField DataField="n_SMSTriggerCount" HeaderText="SMS Trigger Count"></asp:BoundField>
                                    <asp:BoundField DataField="n_Status" HeaderText="Status"></asp:BoundField>
                                    <asp:BoundField DataField="s_Last_Modify_User" HeaderText="Last Modified User"></asp:BoundField>
                                    <asp:BoundField DataField="dt_Insert_tDate" HeaderText="Created On" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"></asp:BoundField>
                                    <asp:BoundField DataField="dt_Modifiy_Date" HeaderText="Modified On" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"></asp:BoundField>

                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkbtnEdit" runat="server" class="btn btn-info" CommandName="edt" formnovalidate><i class="icon-edit icon-white"></i>Edit</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>


                    </div>
                </asp:View>

                <asp:View ID="vwEmailReport" runat="server">
                    <div class="row" style="padding-bottom: 30px; border-bottom: 1px solid silver;">
                        <div class="col-md-2">
                            PageSize:
        <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PageSize_Changed" CssClass="form-control">

            <asp:ListItem Text="25" Value="25" />
            <asp:ListItem Text="50" Value="50" />
            <asp:ListItem Text="100" Value="100" />
            <asp:ListItem Text="500" Value="500" />
        </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            From Date:
      <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control" required></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="txtFromDate_CalendarExtender" runat="server" BehaviorID="txtFromDate_CalendarExtender" TargetControlID="txtFromDate" Format="dd/MM/yyyy" />
                        </div>
                        <div class="col-md-2">
                            To Date:
      <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" required></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" BehaviorID="txtToDate_CalendarExtender" TargetControlID="txtToDate" Format="dd/MM/yyyy" />
                        </div>
                        <div class="col-md-2" style="padding-top:35px">
                     <label>&nbsp;</label>       
       <asp:Button ID="btnSearchEmailLog" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="btnSearchEmailLog_Click" />
                             <asp:Button ID="btnEmailLogDownload" runat="server" Text="Download" CssClass="btn btn-primary" OnClick="btnEmailLogDownload_Click" />
                        </div>

                        
                        <%-- <div class="col-md-2">
                            <div class="form-group">
                                <div>
                                    &nbsp;
                                </div>

                                <asp:Button ID="btnSearch" runat="server" Text="Save" CssClass="btn btn-primary" />



                            </div>
                        </div>--%>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 ">
                            <div class="table-responsive">
                                <asp:GridView ID="gvEmailSendReport" CssClass="table mb-0 table-striped table-responsive"
                                    Width="100%" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:BoundField DataField="n_Email_Log_Id" HeaderText="n_Email_Log_Id"></asp:BoundField>
                                        <asp:BoundField DataField="s_Transaction_Id" HeaderText="s_Transaction_Id"></asp:BoundField>
                                        <asp:BoundField DataField="n_Email_Config_Id" HeaderText="n_Email_Config_Id"></asp:BoundField>
                                        <asp:BoundField DataField="s_SMTP_User_Name" HeaderText="SMTP User Name"></asp:BoundField>
                                        <asp:BoundField DataField="s_From_Email" HeaderText="s_From_Email"></asp:BoundField>
                                        <asp:BoundField DataField="s_From_Name" HeaderText="From Name"></asp:BoundField>
                                        <asp:BoundField DataField="s_Email_To" HeaderText="s_Email_To"></asp:BoundField>
                                        <asp:BoundField DataField="s_Email_BCC" HeaderText="s_Email_BCC"></asp:BoundField>
                                        <asp:BoundField DataField="s_Email_CC" HeaderText="s_Email_CC"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Email Subject">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("s_Email_SubjectShort") %>' ToolTip='<%# Bind("s_Email_Subject") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                      <%--  <asp:TemplateField HeaderText="Email_Body">

                                            <ItemTemplate>
                                                <asp:Label ID="lblEmailBody" runat="server" Text='<%# Bind("s_Email_BodyShort") %>' ToolTip='<%# Bind("s_Email_Body") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:BoundField DataField="s_Attachment" HeaderText="s_Attachment"></asp:BoundField>
                                        <asp:BoundField DataField="s_Send_Status" HeaderText="s_Send_Status"></asp:BoundField>
                                        <asp:BoundField DataField="s_Error" HeaderText="s_Error"></asp:BoundField>
                                        <asp:BoundField DataField="dt_Insert_Date" HeaderText="Created On" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy hh:mm:ss tt}"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Repeater ID="rptPager" runat="server">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>' Enabled='<%# Eval("Enabled") %>' OnClick="Page_Changed"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>


                    </div>
                </asp:View>

                <asp:View ID="vwEmailTemplate" runat="server">
                    <div style="margin: 5px 5px 3px 49px">
                        <div class="row no-padding">
                            <div class="col-md-12">
                                <h5 class="main-title text-center">
                                    <span style="padding-left: 50px;">Select Template 
                <asp:DropDownList ID="ddlEmailTemplate" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEmailTemplate_SelectedIndexChanged">
                </asp:DropDownList></span> </h5>
                            </div>
                            <div class="col-md-12">

                                <asp:TextBox ID="txtEmailTemplate" runat="server" TextMode="MultiLine"></asp:TextBox>
                                <script type="text/javascript" lang="javascript">
                                    CKEDITOR.replace('<%=txtEmailTemplate.ClientID%>');
                                </script>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-top: 10px; padding-bottom: 30px; border-bottom: 1px solid silver;">
                        <div class="col-md-5">
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div>
                                    &nbsp;
                                </div>

                                <asp:Button ID="btnSaveEmailTemplate" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveEmailTemplate_Click" Enabled="false" />

                                <asp:Button ID="btnCancelEmailTemplate" runat="server" Text="Cancel" CssClass="ml15 btn btn-warn" OnClick="btnCancelEmailTemplate_Click" />

                            </div>
                        </div>
                    </div>


                </asp:View>

            </asp:MultiView>

        </div>


    </div>


</asp:Content>

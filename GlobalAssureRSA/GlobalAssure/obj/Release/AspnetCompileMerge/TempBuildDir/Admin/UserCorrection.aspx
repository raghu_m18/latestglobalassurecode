﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="UserCorrection.aspx.cs" Inherits="GlobalAssure.Admin.UserCorrection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
    <div class="col-lg-12" style="margin-top: 30px">
        <div class="col-lg-3">
            <span>Enter Old Email</span>
            <asp:TextBox runat="server" ID="txtOlduser" CssClass="form-control" placeholder="Old Email" />
            <asp:HiddenField runat="server" ID="hdnUsername" />
        </div>
        <div class="col-lg-3" style="margin-top: 20px">
            <asp:Button Text="Search" ID="btnSearch" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnSearch_Click" />
            <asp:Button Text="Reset" ID="btnReset" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnReset_Click"/>
        </div>
        <div id="dvNewEmail" runat="server">
            <div class="col-lg-3">
                <span>Enter New Email</span>
                <asp:TextBox runat="server" ID="txtNewuser" CssClass="form-control" placeholder="New Email" />
            </div>
            <div class="col-lg-3" style="margin-top: 20px">
                <asp:Button Text="Update" ID="btnUpdate" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnUpdate_Click" />
            </div>
        </div>
        <div class="row align-items-center mb-30 pt-30" style="padding-top: 80px">
            <div class="col-lg-12" id="dvuser" runat="server">
                <div class="bg-white table-responsive rounded shadow-sm mb-30" style="width: 100%">
                    <asp:GridView ID="gvUsers" CssClass="table mb-0 table-striped" CellSpacing="0" Width="100%" runat="server" AutoGenerateColumns="false"
                        AllowPaging="True" OnRowDataBound="gvUsers_RowDataBound" OnPageIndexChanging="gvUsers_PageIndexChanging"
                        DataKeyNames="Name,Email,UserName" OnRowCommand="gvUsers_RowCommand" PageSize="20">
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="Email" HeaderText="Email" />
                            <asp:BoundField DataField="UserName" HeaderText="UserName" />                            
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

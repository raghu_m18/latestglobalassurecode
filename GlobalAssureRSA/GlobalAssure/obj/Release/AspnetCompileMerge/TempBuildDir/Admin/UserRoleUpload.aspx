﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="UserRoleUpload.aspx.cs" Inherits="GlobalAssure.Admin.UserRoleUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
    <div class="col-lg-12" style="margin-top: 10px">
        <div class="col-lg-3">
            <span>Select Role</span>
            <asp:DropDownList ID="ddlRole" CssClass="form-control" runat="server">
            </asp:DropDownList>
        </div>
        <div class="col-lg-3">
            <span>Upload File</span>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                    <asp:Button ID="btnUpload" runat="server" Text="Upload Role" CssClass="btn btn-primary" OnClick="btnUpload_Click" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpload" />
                </Triggers>
            </asp:UpdatePanel>
            <%-- <asp:FileUpload ID="FileUpload1" runat="server" />--%>
        </div>
    </div>

</asp:Content>

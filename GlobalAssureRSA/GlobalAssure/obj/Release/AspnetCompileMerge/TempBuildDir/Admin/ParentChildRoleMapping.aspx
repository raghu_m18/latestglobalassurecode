﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ParentChildRoleMapping.aspx.cs" Inherits="GlobalAssure.Admin.ParentChildRoleMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <h2 class="h2 text-left">Parent Child Mapping</h2>
    <style type="text/css">
        #lstChilds {
            height: 100%;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
    <div class="col-lg-12" style="margin-top:30px">
        <div class="col-lg-3">
            <asp:DropDownList ID="ddlParent" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlParent_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
        </div>
        <div class="col-lg-6" style="margin-top: 10px">
            <span style="font-size: 18px">Parent :- </span>&nbsp;&nbsp;
            <asp:Label ID="lblParent" Text="" runat="server" />
            <asp:HiddenField ID="hdnParent" runat="server" />
        </div>
    </div>
    <div class="col-lg-12" style="margin-top: 50px">
        <div class="col-lg-3">
            <asp:TextBox ID="txtChild" CssClass="form-control" runat="server" Placeholder="Enter Child" />
        </div>
        <div class="col-lg-6" style="margin-top: 2px">
            <asp:Button Text="Search" ID="btnSearch" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnSearch_Click" />
        </div>
    </div>


    <div class="row align-items-center mb-30 pt-30" style="padding-top: 80px">
        <div class="col-lg-6" id="dvGridChid" runat="server">
            <div style="height: 250px; overflow: scroll">
            <div class="bg-white table-responsive rounded shadow-sm mb-30" style="width: 100%">
                <asp:GridView ID="gvChild" CssClass="table mb-0 table-striped" CellSpacing="0" Width="100%" runat="server" AutoGenerateColumns="false"
                     OnRowDataBound="gvChild_RowDataBound"
                    DataKeyNames="Name,Email" OnRowCommand="gvChild_RowCommand">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:BoundField DataField="Email" HeaderText="Email" />
                    </Columns>
                </asp:GridView>
            </div>
                </div>
            <asp:Button Text="Add" ID="btnAdd" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnAdd_Click" />
        </div>
        <div class="col-lg-6" id="dvLstChild" runat="server">
            <div style="height: 250px; overflow: scroll">
                <div class="bg-white table-responsive rounded shadow-sm mb-30" style="width: 100%">
                    <asp:GridView ID="gvlstChilds" CssClass="table mb-0 table-striped" CellSpacing="0" Width="80%" runat="server" AutoGenerateColumns="false"
                        AllowPaging="True" OnRowDataBound="gvlstChilds_RowDataBound"
                        DataKeyNames="Email" OnRowCommand="gvlstChilds_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="Email" HeaderText="Child Email" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <asp:Button Text="Submit" ID="btnSubmit" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnSubmit_Click" />
        </div>
    </div>

</asp:Content>

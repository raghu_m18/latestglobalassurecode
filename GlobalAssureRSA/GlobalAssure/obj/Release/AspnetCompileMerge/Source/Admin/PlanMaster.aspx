﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="PlanMaster.aspx.cs" Inherits="GlobalAssure.Admin.PlanMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
     <div class="col-lg-12" style="margin-top:30px">
        <div class="col-lg-3">
            <span>Select Plan</span>
            <asp:DropDownList ID="DDLPlan" runat="server" CssClass="form-control" OnSelectedIndexChanged="DDLPlan_SelectedIndexChanged" AutoPostBack="true">               
            </asp:DropDownList>
        </div>        
          <div class="col-lg-3" style="margin-top:20px">             
              <%--<asp:Button Text="Search" ID="btnSearch" CssClass="btn btn-normal btn-primary" runat="server"/>--%>
                <%--<asp:Button Text="Reset" ID="btnReset" CssClass="btn btn-normal btn-primary" runat="server"/>--%>
        </div>
         <div class="row align-items-center mb-30 pt-30" style="padding-top: 80px">
        <div class="col-lg-12" id="dvPlan" runat="server">
            <div class="bg-white table-responsive rounded shadow-sm mb-30" style="width: 100%">
                <asp:GridView ID="gvPlan" CssClass="table mb-0 table-striped" CellSpacing="0" Width="100%" runat="server" AutoGenerateColumns="false"
                    AllowPaging="True"  OnRowDataBound="gvPlan_RowDataBound" OnPageIndexChanging="gvPlan_PageIndexChanging"
                    DataKeyNames="PlanID,ProductID,Name,Amount" OnRowCommand="gvPlan_RowCommand" PageSize="20">
                    <Columns>       
                         <asp:BoundField DataField="PlanID" HeaderText="Plan ID" />
                        <asp:BoundField DataField="ProductID" HeaderText="Product ID" />                       
                         <asp:BoundField DataField="Name" HeaderText="Plan Name" />
                         <asp:BoundField DataField="Amount" HeaderText="Amount" />                                                
                    </Columns>
                </asp:GridView>
            </div>
        </div>        
    </div>
         <div class="text-center" style="margin-top:20px">             
              <asp:Button Text="Download" ID="btnDownload" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnDownload_Click"/>
         </div>
    </div>
</asp:Content>

﻿
var errorArray = {};
var errorMessageCount = 0;
//Error message control: can write multiple error messages to the error message console
function writeMultipleMessage(type, message, elementId)
{
    var container = $("#containerMain");
    var i = 0;
    var errorFlag = true;
    var color = "";
    var imgSrc = "";
    if (type == "error")
    {
        color = "Red";
        imgSrc = "../../Images/error.jpg";
        $("#" + elementId).css("border-color", color);
        // if (errorArray[elementId] != "" && errorArray[elementId] != undefined && errorArray[elementId] != null) { Object.keys(errorArray).length
        errorArray[elementId] = message;
        //}
    }
    else if (type == "success")
    {
        color = "Green";
        imgSrc = "../../Images/success.png";
        $("#" + elementId).css("border-color", "#94C0D2");
    }
    else
    {
        color = "Yellow";
        imgSrc = "../../Images/warning.png";
        $("#" + elementId).css("border-color", color);
    }
    errorMessageCount = 0;
    for (element in errorArray)
    {
        if (element != "" && errorArray[element] != undefined && errorArray[element] != "")
        {
            errorFlag = false;
            errorMessageCount++;
        }
    }
    // Toggle the slide based on its current visibility.
    if (container.is(":visible") && message == "")
    {
        //        errorMessageCount = 0;
        //        for (element in errorArray) {
        //            if (element != "" && errorArray[element] != undefined && errorArray[element] != "") {
        //                errorFlag = false;
        //                errorMessageCount++;
        //            }
        //        }
        if (errorFlag)
        {
            // Hide - slide up.
            container.slideUp(500);
        }
        $("#" + elementId).css("border-color", "#94C0D2");
    } else if (message != "")
    {
        if (errorMessageCount > 2 && errorMessageCount < 4)
        {
            var containerHeight = container.height();
            if (containerHeight <= 50)
            {
                container.height(containerHeight * 2);
                //$("#divContent").height(containerHeight * 2);
            }
        }
        container.slideDown(500);
        if (type == "success")
        {
            setTimeout(function ()
            {
                container.slideUp(500);
            }, 2000);
        }
    }
    if (message == "")
    {
        $("#" + elementId).css("border-color", "#94C0D2");
    } //<div>Click on the error message to navigate to the field</div>padding-left:99%
    var listStart = "<div><div style='float:right;cursor:pointer;font-size:medium' onclick='closePopUp()' id='drag'>x</div>";
    var listEnd = "</div>";
    for (element in errorArray)
    {
        if (element != "" && errorArray[element] != undefined && errorArray[element] != "")
        {
            listStart += "<img src='" + imgSrc + "'height='24px' width='24px' style='vertical-align: middle;'/><span style='padding-left:5px;cursor:pointer' onclick='setFocus(\"" + element + "\")'>" + errorArray[element] + "</span><br/>";
            listStart += listEnd;
        }
    }
    container.css('color', color);
    //$("#divContent").html(listStart);
    container.html(listStart);
}
//Added to bind Address dropdowns on change
function checkPermanentCityByState(e)
{
    
    var ddlsource = $("#" + e + "ddlState");
    var ddltarget = $("#" + e + "ddlCity");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetCityByState?StateId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {
                $(ddltarget).empty();
                $(ddltarget).append("<option value=''>Select City</option>");
                for (i = 0; i < Data.length; i++)
                {

                    var optionhtml = '<option value="' +
                    Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select City</option>");
    }
}

function checkCountryNew(e) {
   
    var ddlsource = $("#" + e + "ddlCountry")[0];
    var ddltarget = $("#" + e + "ddlState")[0];
    var district = $("#" + e + "ddlDistrict")[0];
    var city = $("#" + e + "ddlCity")[0];
    var area = $("#" + e + "ddlArea")[0];
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "") {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetStateByCountry?CountryId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data) {
               
                    $(ddltarget).empty();
                    $(ddltarget).append("<option  value=''>Select State</option>");
                    $(district).empty();
                    $(district).append("<option value=''>Select District</option>");
                    $(city).empty();
                    $(city).append("<option value=''>Select City</option>");
                    $(area).empty();
                    $(area).append("<option value=''>Select Area</option>");
                    $("#" + e + "_PinCode").val("");
                    for (i = 0; i < Data.length; i++)
                    {
                        var optionhtml = '<option value="' +
                        Data[i].ID + '">' + Data[i].Value + '</option>';
                        $(ddltarget).append(optionhtml);
                    }
               
            },
            error: function (xhr, tStatus, err) {
            }
        });
    }
    else {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select State</option>");
    }
}

function checkStateNew(e)
{
   
    var ddlsource = $("#" + e + "ddlState")[0];
    var ddltarget = $("#" + e + "ddlDistrict")[0];
    var city = $("#" + e + "ddlCity")[0];
    var area = $("#" + e + "ddlArea")[0];
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetDistrictsByState?StateID=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {
                //debugger
                //if (Data.length > 0)
                //{
                    $(ddltarget).empty();
                    $(ddltarget).append("<option  value=''>Select District</option>");
                    $(city).empty();
                    $(city).append("<option value=''>Select City</option>");
                    $(area).empty();
                    $(area).append("<option value=''>Select Area</option>");
                    $("#" + e + "_PinCode").val("");
                    for (i = 0; i < Data.length; i++)
                    {
                        var optionhtml = '<option value="' +
                    Data[i].ID + '">' + Data[i].Value + '</option>';
                        $(ddltarget).append(optionhtml);
                    }
               // }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select District</option>");
    }
}

//Method to close the error message console
function closePopUp()
{
    //close the popup
    var container = $("#containerMain");
    if (container.is(":visible"))
    {
        // Hide - slide up.
        container.slideUp(500); 
    } else
    {
        // Show - slide down.
        // container.slideDown(500);
    }
}

function checkFileFormat(e)
{
    if (e == "tif" || e == "tiff"  || e == "jpg"  || e == "jpeg"  || e == "png" || e == "bmp" || e == "gif" || e == "doc" || e == "pdf" || e== "docx" || e == "msg" || e == "eml")
    {
        return true;
    }
    else
    {
        return false;
    }
}

function checkDistrictNew(e)
{
   
    var ddlsource = $("#" + e + "ddlDistrict")[0];
    var ddltarget = $("#" + e + "ddlCity")[0];
    var area = $("#" + e + "ddlArea")[0];
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetCitiesByDistrict?DistrictId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {
                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select City</option>");
                $(area).empty();
                $(area).append("<option value=''>Select Area</option>");
                $("#" + e + "_PinCode").val("");
                for (i = 0; i < Data.length; i++)
                {
                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select City</option>");
    }
}

    $('*[data-autocomplete-url]')
    .each(function ()
    {
        $(this).autocomplete({
            source: $(this).data("autocomplete-url")
        })
    });


//added For Country Dropdown
function checkCityNew(e)
{
   
    var ddlsource = $("#" + e + "ddlCity");
    var ddltarget = $("#" + e + "ddlArea");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetAreasByCity?CityId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {
                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select Area</option>");
                $("#" + e + "_PinCode").val("");
                for (i = 0; i < Data.length; i++)
                {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select Area</option>");
    }
}

function checkCityByPinCode(e) {
    //debugger
    var ddlsource = $("#" + e + "ddlCity");
    var ddltarget = $("#" + e + "ddlArea");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "") {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetAreasPinCodeByCity?CityId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data) {
                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select Pin Code</option>");
                for (i = 0; i < Data.length; i++) {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err) {
            }
        });
    }
    else {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select Pin Code</option>");
    }
}



//added to get both Area And pincode By CityId  priyanka
function checkAreaAndPincodeByCity(e) {
   
    var ddlsource = $("#" + e + "ddlCity");
    var ddltargetArea = $("#" + e + "ddlArea");
    var ddltargetPincode = $("#" + e + "ddlPincode");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "") {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetAreasPinCodeByCity?CityId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data) {
                $(ddltargetPincode).empty();
                $(ddltargetPincode).append("<option  value=''>Select PinCode</option>");
                for (i = 0; i < Data.length; i++) {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltargetPincode).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err) {
            }
        });
    }
    else {
        $(ddltargetPincode).empty();
        $(ddltargetPincode).append("<option value=''>Select PinCode</option>");
    }
    if ($(ddlsource).val() != "") {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetAreasByCity?CityId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data) {
                $(ddltargetArea).empty();
                $(ddltargetArea).append("<option  value=''>Select Area</option>");
               
                for (i = 0; i < Data.length; i++) {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltargetArea).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err) {
            }
        });
    }
    else {
        $(ddltargetArea).empty();
        $(ddltargetArea).append("<option value=''>Select Area</option>");
    }
}


function checkPincodeByArea(e)
{
    var ddlsource = $("#" + e + "ddlArea");
    var ddltargetPincode = $("#" + e + "ddlPincode");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "") {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetPinCode?AreaId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data) {
              
                $("#" + e + "ddlPincode"+" option").each(function () {
                    if ($(this).text() == Data) {
                        $(this).attr('selected', 'selected');
                    }
                });              
              
                
            },
            error: function (xhr, tStatus, err) {
            }
        });
    }
    else {
    }

}
function checkAreaByPincode(e) {
   
    var ddltarget = $("#" + e + "ddlArea");
    var ddlsource = $("#" + e + "ddlPincode");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "") {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetAreaByPinCode?PincodeId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data) {
                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select Area</option>");
              
                for (i = 0; i < Data.length; i++) {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }


            },
            error: function (xhr, tStatus, err) {
            }
        });
    }
    else {
        checkAreaAndPincodeByCity(e);
    }

}



function getFocus(element)
{
    // debugger;
    var address = element.name.split(".");
    var addname = address.slice(-1);
    if (addname == "Address1" || addname == "Address2")
    {
        // validateAddress(element);
    }
}


// For Enrollment-------------------------------

function checkPlanByProductName(e)
{    
    var ddlsource = $("#" + e + "ddlProduct");
    var ddltarget = $("#" + e + "ddlPlan");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetPlanByProductName?ProductID=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {
                
                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select Plan</option>");
                for (i = 0; i < Data.length; i++)
                {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select Plan</option>");
    }
}

function getOfficeCodeByOrganizationName(e)
{
  
    var ddlsource = $("#" + e + "ddlName");
    var ddltarget = $("#" + e + "ddlOfficeCode");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetOrgOfficeCodeByOrganization?OrganizationID=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {

                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select Policy Issued Office Code</option>");
                for (i = 0; i < Data.length; i++)
                {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });

        var ddltargetproduct = $("#objProddlProduct");

        //$.ajax({
        //    type: "GET",
        //    url: '/MasterAPI/GetAllBASAppProductNames?OrgID=' + Value.trim(),
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    async: false,
        //    success: function (Data)
        //    {

        //        $(ddltargetproduct).empty();
        //        $(ddltargetproduct).append("<option  value=''>Select Product</option>");
        //        for (i = 0; i < Data.length; i++)
        //        {

        //            var optionhtml = '<option value="' +
        //        Data[i].ID + '">' + Data[i].Value + '</option>';
        //            $(ddltargetproduct).append(optionhtml);
        //        }
        //    },
        //    error: function (xhr, tStatus, err)
        //    {
        //    }
        //});

    }
    else
    {
        $(ddltargetproduct).empty();
        $(ddltargetproduct).append("<option value=''>Select Product</option>");

        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select Policy Issued Office Code</option>");
    }
}

function getOfficeCodeByCoInsuranceOrganizationName(e)
{
    var ddlsource = $("#" + e + "ddlName");
    var ddltarget = $("#" + e + "ddlCode");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetOrgOfficeCodeByOrganization?OrganizationID=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {

                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select Office Code</option>");
                for (i = 0; i < Data.length; i++)
                {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select Policy Issued Office Code</option>");
    }
}

function getOfficeCodeByPortabilityOrganizationName(e)
{
    var ddlsource = $("#" + e + "ddlName");
    var ddltarget = $("#" + e + "ddlCode");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetOrgOfficeCodeByOrganization?OrganizationID=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {

                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select Office Code</option>");
                for (i = 0; i < Data.length; i++)
                {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select Policy Issued Office Code</option>");
    }
}

//for Permanent Address

function checkPermanentCountryNew(e) {
    // debugger;
    var ddlsource = $("#" + e + "ddlCountry")[0];
    var ddltarget = $("#" + e + "ddlState")[0];
    var district = $("#" + e + "ddlDistrict")[0];
    var city = $("#" + e + "ddlCity")[0];
    var area = $("#" + e + "ddlArea")[0];
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "") {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetStateByCountry?CountryId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data) {

                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select State</option>");
                $(district).empty();
                $(district).append("<option value=''>Select District</option>");
                $(city).empty();
                $(city).append("<option value=''>Select City</option>");
                $(area).empty();
                $(area).append("<option value=''>Select Area</option>");
                $("#" + e + "_PinCode").val("");
                for (i = 0; i < Data.length; i++) {
                    var optionhtml = '<option value="' +
                    Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }

            },
            error: function (xhr, tStatus, err) {
            }
        });
    }
    else {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select State</option>");
    }
}

function checkPermanentStateNew(e)
{
    //debugger
    var ddlsource = $("#" + e + "ddlState")[0];
    var ddltarget = $("#" + e + "ddlDistrict")[0];
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetDistrictsByState?StateID=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {
                //debugger
                if (Data.length > 0)
                {
                    $(ddltarget).empty();
                    $(ddltarget).append("<option  value=''>Select District</option>");
                    for (i = 0; i < Data.length; i++)
                    {
                        var optionhtml = '<option value="' +
                    Data[i].ID + '">' + Data[i].Value + '</option>';
                        $(ddltarget).append(optionhtml);
                    }
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select District</option>");
    }
}

function checkPermanentDistrictNew(e)
{

    var ddlsource = $("#" + e + "ddlDistrict")[0];
    var ddltarget = $("#" + e + "ddlCity")[0];
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetCitiesByDistrict?DistrictId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {
                $(ddltarget).empty();

                $(ddltarget).append("<option  value=''>Select City</option>");
                for (i = 0; i < Data.length; i++)
                {
                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select City</option>");
    }
}

function checkPermanentCityByPinCode(e)
{
    //debugger
    var ddlsource = $("#" + e + "ddlCity");
    var ddltarget = $("#" + e + "ddlArea");
    var Value = $(ddlsource).val();
    if ($(ddlsource).val() != "")
    {
        $.ajax({
            type: "GET",
            url: '/MasterAPI/GetAreasPinCodeByCity?CityId=' + Value.trim(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (Data)
            {
                $(ddltarget).empty();
                $(ddltarget).append("<option  value=''>Select PinCode</option>");
                for (i = 0; i < Data.length; i++)
                {

                    var optionhtml = '<option value="' +
                Data[i].ID + '">' + Data[i].Value + '</option>';
                    $(ddltarget).append(optionhtml);
                }
            },
            error: function (xhr, tStatus, err)
            {
            }
        });
    }
    else
    {
        $(ddltarget).empty();
        $(ddltarget).append("<option value=''>Select PinCode</option>");
    }
}

function GetPACoveredADDOnLimit(x) {
   
    var PlanID = $("#objProddlPlan").val();
    var amount = $("#"+x).val();
    $.ajax({
        type: "GET",
        url: '/MasterAPI/GetPACoveredADDOnLimit?PlanID=' + PlanID.trim(),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (Data)
        {
          
            if (parseFloat(amount) > parseFloat(Data)) {
                alert('PA sum insured must be lessthan or equal to' + " " + Data);
                $("#" + x).val('');
            }
            
        }
    });
}
//------------End------------------------------------




//--------Go back to the previous page
function GoBack()
{
    window.history.back();
}

//------Client side validation---------------
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {
        return false;
    }
    else
    {
        return true;
    }
}
function AlphabetsAndBackspace(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (!((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 8) || (charCode == 32) || (charCode == 46)))
    {
        return false;
    }
    else
    {
        return true;
    }
}
function AlphabetsAndBackspaceAndSpace(evt)
{
    //debugger;
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (!((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 8) || (charCode == 32)))
    {
        return false;
    }
    else
    {
        return true;
    }
}
function AlphaNumericSpaceBackspaceAndHyphen(evt)
{
    //debugger;
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (!((charCode > 47 && charCode < 58) || (charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 8) || (charCode == 32)))
    {
        return false;
    }
    else if (charCode == 35 || charCode == 38 || charCode == 44 || charCode == 45 || charCode == 46)
    {
        return false;
    }
    else if (charCode > 31 && (charCode >= 48 || charCode <= 57))
    {
        return true;
    }
    else
    {
        return true;
    }
}
function AlphaNumeric(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (!((charCode > 47 && charCode < 58) || (charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 8) || (charCode == 32)))
    {
        return false;
    }
    else if (charCode > 31 && (charCode >= 48 || charCode <= 57))
    {
        return true;
    }
    else
    {
        return true;
    }
}
//------Client side validation---------------

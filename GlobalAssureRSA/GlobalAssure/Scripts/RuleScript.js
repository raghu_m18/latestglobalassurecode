﻿function headertoggle(divname, spanname)
{
    if ($("#" + divname).is(':visible') == true)
    {
        $("#" + divname).parent().slideUp();
        //$("#" + divname).addClass("arrow-up");
        $("#" + spanname).removeClass("arrow");
        $("#" + spanname).addClass("arrow-up");
        //$(".arrow").addClass("arrow-up");
    }
    else
    {
        $("#" + divname).parent().slideDown();
        //$("#" + divname).removeClass("arrow-up");
        $("#" + spanname).removeClass("arrow-up");
        $("#" + spanname).addClass("arrow");
        //$(".arrow").removeClass("arrow-up");
    }

}

function fieldsValues(field, i)
{
     
    var Fields = $("#" + field).val();

    var valueName;

    if (Fields == "Dta" || Fields == "Dta1" || Fields == "Dtd")
    {

        valueName = "FromDate" + i;
    }

    else if (Fields == "txtbx" || Fields == "AC" || Fields == "AB" || Fields == "PerClmtxtbx")
    {

        valueName = "AmountTextbox" + i;
    }

    else if (Fields == "numerictxtbx" || Fields == "NOD" || Fields == "IncidentNOD" || Fields == "ProLongdAdmission" || Fields == "MultipleAdmissionSingleURN"
             || Fields == "RepeatedAdmission")
    {

        valueName = "AgeTextBox" + i;
    }

    else if (Fields == "txtarea")
    {

        valueName = "txtarea" + i;
    }

    else if (Fields == "URN")
    {
        valueName = "textbox" + i;
    }

    else if (Fields == "Diagnosis")
    {

        valueName = "diagnosis" + i;
    }

    else if (Fields == "PT")
    {

        valueName = "PackageTypeD" + i;

    }

    else if (Fields == "TC")
    {
        valueName = "TranCodeD" + i;
    }

    else if (Fields == "gender")
    {

        valueName = "genderD" + i;

    }

    else if (Fields == "PD" || Fields == "TD" || Fields == "TAC")
    {

        valueName = "integerTxtbx" + i;
    }

    else if (Fields == "AT")
    {
        valueName = "AdmissionTypD" + i;
    }

    else
    {
        //valueName = field;
        //alert("select the Field");
        var dropDownList = $("#" + field).val();

        var a1 = new Array();
        a1 = dropDownList.split("/");
       // var datatype = a1[0];
        var _fieldName = a1[0];
        //var tablename = a1[1];
        var _dataType = a1[1];
        //var key = a1[2];
        var _dataLength = a1[2];
        //var keytable = a1[3];
        var _schemaName = a1[3];
        //var text = a1[4];
        var text = a1[4];

        var myRegExpGender = /[Gg][Ee][Nn][Dd][Ee][Rr]/;
        var AgeRegExp = /[Aa][Gg][Ee]/;
        var DateRegExp = /[Dd][Aa][Tt][Ee]/;
        var TarnsactionCodeRegExp = /[Tt][Rr][Aa][Nn][Ss][Aa][Cc][Tt][Ii][Oo][Nn][Cc][Oo][Dd][Ee]/;
        var TarnsactionTypeRegExp = /[Tt][Rr][Aa][Nn][Ss][Aa][Cc][Tt][Ii][Oo][Nn][Tt][Yy][Pp][Ee]/;
        var DenailCodeRegExp = /[Dd][Ee][Nn][Aa][Ii][Ll][Cc][Oo][Dd][Ee]/;

        if (_dataType == 'integer')
        {
            valueName = "AmountTextbox" + i;
        }
        else if (_dataType == 'string')
        {
            valueName = "textbox" + i;
        }
        else if (_dataType == 'datetime')
        {
            valueName = "FromDate" + i;
        }
        else
        {
            valueName = "yesNoD" + i;
        }
        

        //if (text == "Diagnosis" && _schemaName != "" && _dataLength != "")
        //{
        //    valueName = "diagnosis" + i;
        //}
        //else if (TarnsactionCodeRegExp.test(text) && _schemaName != "" && _dataLength != "")
        //{
        //    valueName = "TranCodeD" + i;
        //}
        //else if (TarnsactionTypeRegExp.test(text) && _schemaName != "" && _dataLength != "")
        //{
        //    valueName = "TransactionType" + i;
        //}
        //else if (DenailCodeRegExp.test(text) && _schemaName != "" && _dataLength != "")
        //{
        //    valueName = "TransactionType" + i;
        //}
        //else if (myRegExpGender.test(text) && _schemaName == "" && _dataLength == "")
        //{
        //    valueName = "genderD" + i;
        //}
        //else if (text == "PackageType" && _schemaName == "" && _dataLength == "")
        //{
        //    valueName = "PackageTypeD" + i;
        //}
        //else if (AgeRegExp.test(text) && _schemaName == "" && _dataLength == "" && text != "PackageType")
        //{
        //    valueName = "AgeTextBox" + i;
        //}
        //else if (datatype == "datetime" && _schemaName == "" && _dataLength == "")
        //{
        //    valueName = "FromDate" + i;
        //}
        //else if (DateRegExp.test(text))
        //{
        //    valueName = "FromDate" + i;
        //}
        //else if (TarnsactionCodeRegExp.test(text))
        //{
        //    valueName = "TranCodeD" + i;
        //}
        //else
        //{
        //    valueName = "textbox" + i;
        //}
    }
    return valueName;
}




$(document).ready(function ()
{

    $("#addrul").click(function ()
    {
         debugger
        var fieldname;
        var operatorName;
        var flag = new Boolean();
        flag = true;
        var count;
        // checking if all values are selected
        var j = 1;

        for (j; j < 10; j++)
        {
            var row = "row" + j;
            var rowstyle = document.getElementById(row).style.display;

            if (rowstyle == "block" || row == "row1")
            {
                count = j;
                fieldname = "FieldsDropDownList" + j;
                operatorName = "operatorDropDownList" + j;

                var Fields = $("#" + fieldname).val();
                if (Fields == "")
                {
                    flag = false;
                    count = count--;
                    break;
                }

                var operator = $("#" + operatorName).val();

                if (operator == "")
                {
                    count = count--;
                    flag = false;
                    break;
                }

                var value = fieldsValues(fieldname, j);
                var valueoperator = $("#" + value).val();

                if (valueoperator == "" || valueoperator == undefined)
                {
                    flag = false;
                    count = count--;
                    break;
                }

            }

        }

        var action = $("#Action").val();

        //if (action == "")
        //{
        //    flag = false;

        //}
        if (action == "PI")
        {

            var val = $("#InvName").val();
            if (val == "")
            {

                flag = false;
            }

        }
        var RuleScore = $("#txtruleScore").val();
        var ruleDescription = $("#RuleDescrip").val();
        //if (ruleDescription == "")
        //{
        //    flag = false;

        //}
        var alertDescription = $("#AlertDescrip").val();
        //if (alertDescription == "")
        //{
        //    flag = false;

        //}
        var effdate = $("#effdate").val();

        if (flag)
        {

            document.getElementById('errmsg3').style.visibility = "hidden";
            document.getElementById('RowCount').value = count;
            //  document.getElementById('RulePreview').value = getPreviewValue();
            //            var ruleexists = json
            $('#ConfirmationWindow').data('tWindow').center().open();

        }

        else
        {
            document.getElementById('errmsg3').style.visibility = "visible";
        }

    });

    $("#addRule").click(function ()
    {
        $('#ConfirmationWindow').data('tWindow').center().close();
        $('#ScoreWindow').data('tWindow').center().open();
        //$('#NewRule').submit();
        //document.NewRule.submit();
    });

    $("#Cancel").click(function ()
    {
        $('#ConfirmationWindow').data('tWindow').center().close();
    });

    $("#No").click(function ()
    {
        $("#ScoreWindow").data('tWindow').center().close();
        $("#NewRule").submit();
    });

    $("#Yes").click(function ()
    {
         
        var score = $("#txtruleScore").val();
        document.getElementById("hdnRuleScore").value = $("#txtruleScore").val();
        $("#NewRule").submit();
    });

});


$(document).ready(function ()
{


    $("#divOptnParam").parent().slideUp();

    $(".Accordian").hover(function ()
    {
        $(this).addClass("Accordian-hover");
    },
function ()
{
    $(this).removeClass("Accordian-hover");
})

    $("#prevw").click(function ()
    {
         debugger
        var fieldname;
        var operatorName;
        var value;
        var logicaloperator;
        var flag = new Boolean();
        flag = true;

        // checking if all values are selected
        var j = 1;

        for (j; j < 10; j++)
        {
            var row = "row" + j;
            var rowstyle = document.getElementById(row).style.display;
            if (rowstyle == "block" || row == "row1")
            {

                fieldname = "FieldsDropDownList" + j;
                operatorName = "operatorDropDownList" + j;

                var Fields = $("#" + fieldname).val();
                if (Fields == "")
                {
                    flag = false;
                    break;
                }
                var operator = $("#" + operatorName).val();
                if (operator == "")
                {
                    flag = false;
                    break;
                }

                var value = fieldsValues(fieldname, j);
                var valueoperator = $("#" + value).val();


                //var valueoperator=null;
                //if (value != undefined)
                //{
                //    valueoperator = $("#" + value).val();
                //}
                //else
                //{
                //    valueoperator = $("#" + fieldname).val();
                //}

                if (valueoperator == "" || valueoperator == undefined)
                {
                    flag = false;
                    break;
                }

            }

        }

        var ruleDescription = $("#RuleDescrip").val();
        if (ruleDescription == "")
        {
            flag = false;

        }
        var alertDescription = $("#AlertDescrip").val();
        if (alertDescription == "")
        {
            flag = false;

        }
        //var action = $("#Action").val();

        //if (action == "")
        //{
        //    flag = false;

        //}
        //else if (action == "PI")
        //{

        //    var val = $("#9").val();
        //    if (val == "")
        //    {

        //        flag = false;
        //    }
        //    else
        //    {
        //        var action = $("#Action").data("tDropDownList");
        //        var actiontaken = action.text() + " : " + val + " ";

        //    }

        //}
        //else if (action == "IR")
        //{

        //    var RT = $("#ReqType").val();
        //    var IT = $("#InfoType").val();
        //    var value1, value2;
        //    if (RT == "" || IT == "")
        //    {
        //        flag = false;

        //    }
        //    else
        //    {
        //        if (RT == "Others")
        //        {
        //            var val1 = $("#RequestForOthers").val();

        //            if (val1 == "")
        //            {
        //                flag = false;
        //            }
        //            value1 = val1;
        //        }
        //        else
        //        {
        //            value1 = RT;
        //        }
        //        if (IT == "IR16")
        //        {
        //            var val2 = $("#InformationForOthers").val();
        //            if (val2 == "")
        //            {
        //                flag = false;
        //            }
        //            si
        //            value2 = val2;
        //        }
        //        else
        //        {
        //            var s = $("#InfoType").data("tDropDownList");
        //            value2 = s.text();
        //        }
        //        if (flag != false)
        //        {
        //            var action = $("#Action").data("tDropDownList");

        //            var actiontaken = action.text() + " : Request Type = " + value1 + " AND  Information Type = " + value2 + "<br/>";
        //        }

        //    }

        //}
        //else if (action == "AF")
        //{
        //    var remarks = $("#AppRemarks").val();
        //    if (remarks == "")
        //    {
        //        flag = false;
        //    }
        //    else
        //    {
        //        var action = $("#Action").data("tDropDownList");
        //        var actiontaken = action.text() + " : Remarks = " + remarks + "  <br/>";
        //    }
        //}

        //else
        //{

        //    var action = $("#Action").data("tDropDownList");
        //    var actiontaken = action.text();

        //}

        var effdate = $("#effdate").val();
        var efftodate = $("#effTodate").val();
        var ruleScope = $("#ExecutionTypo").val();

        // if all values are selected

        if (flag)
        {
             
            var Rule = "  "+"Claims where ";
            var i = 1;

            for (i; i < 10; i++)
            {
                var row = "row" + i;
                var RowStyle = document.getElementById(row).style.display;

                if (RowStyle == "block" || row == "row1")
                {

                    fieldname = "FieldsDropDownList" + i;
                    operatorName = "operatorDropDownList" + i;

                    var fieldNvalue = fieldsValues(fieldname, i);
                    var dropdownlst = $("#" + fieldname).data("tDropDownList");

                    Rule = Rule + dropdownlst.text().fontcolor("blue");
                    value = fieldNvalue;

                    var oper = $("#" + operatorName).val();

                    Rule = Rule + " " + oper.fontcolor("red") + " ";

                    var valueobtained = value.substring(0, value.length - 1);
                    var valuedropdown = value.substr(value.length - 2);
                    var valueddl = valuedropdown.charAt(0);
                    var valueSelected;
                    var e;
                    if (valueobtained == "txtarea")
                    {
                        // e = document.getElementById(value);
                        var selectedValues = [];
                        $("#" + value + " :selected").each(function ()
                        {
                            selectedValues.push($(this).text());
                        });

                        valueSelected = selectedValues;
                    }

                    else if (valueobtained == "diagnosis")
                    {
                        var selectedValues = [];
                        $("#" + value + " :selected").each(function ()
                        {
                            selectedValues.push($(this).text());
                        });

                        valueSelected = selectedValues;
                    }

                    else if (valueddl == "D")
                    {
                        var valuedropdownlist = $("#" + value).data('tDropDownList');
                        valueSelected = valuedropdownlist.text();
                    }

                    else
                    {

                        valueSelected = $("#" + value).val();
                    }
                     
                     
                    if (Object.prototype.toString.call(valueSelected) === '[object Array]')
                    {
                        for (var k = 0; k < valueSelected.length; k++)
                        {
                            if (k != 0)
                            {
                                Rule = Rule + "  &nbsp; and &nbsp; ";
                            }
                            Rule = Rule + valueSelected[k].fontcolor("green");
                        }
                    }
                    else
                    {
                        Rule = Rule + valueSelected.fontcolor("green");
                    }

                    var LOP = parseInt(i) + 1;
                    logicaloperator = "LogicaloperatorDropDownList" + LOP;

                    var logicOperator = $("#" + logicaloperator).data("tDropDownList");

                    Rule = Rule + " " + logicOperator.text().fontcolor("orange") + "<br/>";

                }

            }
            // 
            document.getElementById('errmsg3').style.visibility = "hidden";

            if (ruleScope == "FreshCases")
            {
                effdate = Date();
                Rule = Rule + "<br/>";
                Rule = Rule + "Action / Workflow: " + actiontaken.fontcolor("#663399") + "<br/>" + " Effective from " + effdate.fontcolor("#663399") + "<br/>" + " Effective to " +" future".fontcolor("#663399");
            }
            else if (ruleScope == "AllExistingCases")
            {
                efftodate = Date();
                Rule = Rule + "<br/>";
                Rule = Rule + "Action / Workflow: " + actiontaken.fontcolor("#663399") + "<br/>" + " Effective from " + "past".fontcolor("#663399") + "<br/>" + " Effective to " + efftodate.fontcolor("#663399");
            }
            else if (ruleScope=="SpecificCases")
            {
                Rule = Rule + "<br/>";
                Rule = Rule + "Action / Workflow: " + actiontaken.fontcolor("#663399") + "<br/>" + " Effective from " + effdate.fontcolor("#663399") + "<br/>" + " Effective to " + efftodate.fontcolor("#663399");
            }
            else if (ruleScope=="AllCases")
            {
                Rule = Rule + "<br/>";
                Rule = Rule + "Action / Workflow: " + actiontaken.fontcolor("#663399") + "<br/>" + " Effective from " + "past".fontcolor("#663399") + "<br/>" + " Effective to " + " future".fontcolor("#663399");
            }

            //if (efftodate == "" || efftodate == undefined)
            //{

            //}
            //else
            //{

            //    Rule = Rule + " and Effective to " + efftodate.fontcolor("#663399");
            //}

            var statecode = $("#StateName").data("tDropDownList");
            if (statecode.text() != "&nbsp;")
            {
                Rule = Rule + "<br/><br/>" + "Rule is applicable for ";
                Rule = Rule + statecode.text() + " state";

                var distcode = $("#DistrictName").data("tDropDownList");

                if (distcode.text() != "&nbsp;")
                {

                    Rule = Rule + ", " + distcode.text() + " district ";

                    var count = 0;
                    $('#EmpHosp :selected').each(function (i, selected)
                    {
                        // alert($(selected).val());
                        // alert($(selected).text());
                        count = count + 1;
                        Rule = Rule + ", " + $(selected).text() + " Hospital";
                        if (count > 1)
                        {
                            Rule = Rule + ", ";
                        }

                    });

                    //   var hosptextarea = document.getElementById('EmpHosp').value;
                    //     if (hosptextarea != undefined && hosptextarea != "") {
                    //        var HospName = document.getElementById('EmpHosp');
                    //       HospitalNames = HospName.options[HospName.selectedIndex].text;

                    //  Rule = Rule + ", " + HospitalNames + " Hospitals";

                    //}
                }

            }

            var occurrence = $("#occurrenceTxtbox").val();
            if (occurrence != "" && occurrence != undefined)
            {
                Rule = Rule + "<br/><br/>" + "Occurrence = ";
                Rule = Rule + occurrence;
            }

            $("#RulePreviewDisplay").html(Rule);
            $("#RuleDock").show('slow');
            //  $("#PreviewResult").html(Rule);

            //   $('#Window').data('tWindow').center().open();
        }

        else
        {

            document.getElementById('errmsg3').style.visibility = "visible";
        }
    });

    $(".spnOperationalSection").click(function ()
    {
        if ($("#divOptnParam").is(':visible') == true)
        {
            $("#divOptnParam").parent().slideUp();
            $(".spnOperationalSection").css("background", "none");
            $(".spnOperationalSection").css("background", "url(../Images/iRule/plus_alt.png) no-repeat");
            $(".spnOperationalSection").css("background-position", "right top");
            $(".spnOperationalSection").css("background-size", "15px");
        }
        else
        {
            $("#divOptnParam").parent().slideDown();
            $(".spnOperationalSection").css("background", "none");
            $(".spnOperationalSection").css("background", "url(../Images/iRule/minus_alt.png) no-repeat");
            $(".spnOperationalSection").css("background-position", "right top");
            $(".spnOperationalSection").css("background-size", "15px");
        }
    })
});


function setValueBasedOnField()
{
    var itemId = $(this).attr('id');
    var lastChar = itemId.substr(itemId.length - 1);
    var numerictxtbox = "divnumerictxtbox" + lastChar;
    var datepicker = "divdatepicker" + lastChar;
    var txtarea = "divtextarea" + lastChar;
    var amountpicker = "divamountpicker" + lastChar;
    var defauletxtbox = "divdefaulttxtbox" + lastChar;
    var packagetype = "divpackagetype" + lastChar;
    var gendertype = "divgen" + lastChar;
    var admissionType = "divAdmissionType" + lastChar;
    var transactioncode = "divTransactionCode" + lastChar;
    var integertxtbox = "divintxtbox" + lastChar;
    var diagnosislst = "divdiag" + lastChar;
    var txtBox = "divtxtbox" + lastChar;

    var dropDownList = $("#" + itemId).val();  //based upon the item id the displaying the value
    var stateid = $("#FieldsDropDownList2").attr('id');
    var statedropdown = $("#" + stateid).val();// based on state id package list is displayed 
    //    var radioselected = $("input:radio[name='TransactionType']:checked").val(); 


    document.getElementById(datepicker).style.display = "none";
    document.getElementById(numerictxtbox).style.display = "none";
    document.getElementById(amountpicker).style.display = "none";
    document.getElementById(txtarea).style.display = "none";
    document.getElementById(defauletxtbox).style.display = "none";
    document.getElementById(packagetype).style.display = "none";
    document.getElementById(gendertype).style.display = "none";
    document.getElementById(admissionType).style.display = "none";
    document.getElementById(transactioncode).style.display = "none";
    document.getElementById(integertxtbox).style.display = "none";
    document.getElementById(diagnosislst).style.display = "none";
    document.getElementById(txtBox).style.display = "none";

    var fundDropDown = $('#operatorDropDownList' + lastChar);
    var fundDropDownData = fundDropDown.data('tDropDownList')

    if (dropDownList == "Diagnosis" || dropDownList == "gender" || dropDownList == "AT" || dropDownList == "PT" || dropDownList == "TC")
    {
        $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data)
        {
            fundDropDownData.dataBind(data);
            fundDropDownData.loader.hideBusy();
            fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
        });
    }
    else if (dropDownList == "txtarea")
    {

        $.get('../RuleConfiguration/GetOperatorforTextArea', function (data)
        {
            fundDropDownData.dataBind(data);
            fundDropDownData.loader.hideBusy();
            fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
        });

    }
    else
    {
        $.ajax({
            type: "Get",
            url: '../RuleConfiguration/getGeneralOperator',

            dataType: "Json",
            success: function (data)
            {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0

            }
        });

    }

    if (dropDownList == "numerictxtbx" || dropDownList == "NOD" || dropDownList == "IncidentNOD" || dropDownList == "ProLongdAdmission" || dropDownList == "MultipleAdmissionSingleURN"
        || dropDownList == "RepeatedAdmission")
    {

        document.getElementById(numerictxtbox).style.display = "table-row";

    }

    else if (dropDownList == "PD" || dropDownList == "TD" || dropDownList == "TAC")
    {
        document.getElementById(integertxtbox).style.display = "table-row";

    }

    else if (dropDownList == "Dta" || dropDownList == "Dtd" || dropDownList == "Dta1")
    {


        document.getElementById(datepicker).style.display = "table-row";



    }

    else if (dropDownList == "txtbx" || dropDownList == "AC" || dropDownList == "AB" || dropDownList == "PerClmtxtbx")
    {

        document.getElementById(amountpicker).style.display = "table-row";

    }

    else if (dropDownList == "TC")
    {
        document.getElementById(transactioncode).style.display = "table-row";

    }

    else if (dropDownList == "txtarea")
    {

        document.getElementById(txtarea).style.display = "table-row";

        //populating the values in to listbox

        $.ajax({
            type: "Get",
            url: '../RuleConfiguration/getAilments',

            dataType: "Json",
            success: function (data)
            {
                
                var txtArea = "txtarea" + lastChar;
                var select = $("#" + txtArea);
                $.each(data, function (index, itemData)
                {

                    select.append($('<option/>', {
                        value: itemData.Value,
                        text: itemData.Text,
                        title: itemData.Text
                    }));

                });

            }

        });

    }

    else if (dropDownList == "Diagnosis")
    {
         
        
        $("#row" + lastChar).removeClass("row");
        $("#divFieldsDropDownList" + lastChar).removeClass("block");
        $("#divoperatorDropDownList" + lastChar).removeClass("block");
        $("#ValueField" + lastChar).removeClass("block");
        $("#divLogicaloperatorDropDownList" + (parseInt(lastChar) + 1)).removeClass("block");

        $("#row" + lastChar).addClass("Packagerow");
        $("#divFieldsDropDownList" + lastChar).addClass("block1");
        $("#divoperatorDropDownList" + lastChar).addClass("block1");
        $("#ValueField" + lastChar).addClass("blockPackage");
        $("#divLogicaloperatorDropDownList" + (parseInt(lastChar)+1)).addClass("block1");

        document.getElementById(diagnosislst).style.display = "table-row";

        $.ajax({
            type: "Get",
            url: '../RuleConfiguration/getDiagnosis?stCode=' + statedropdown,

            dataType: "Json",
            success: function (data)
            {

                var txtArea = "diagnosis" + lastChar;
                var select = $("#" + txtArea);
                $.each(data, function (index, itemData)
                {

                    select.append($('<option/>', {
                        value: itemData.Value,
                        text: itemData.Text,
                        title: itemData.Text
                    }));

                });

            }

        });



    }

    else if (dropDownList == "")
    {

        document.getElementById(defauletxtbox).style.display = "table-row";

    }

    else if (dropDownList == "URN")
    {
        document.getElementById(txtBox).style.display = "table-row";
        //document.getElementById(defauletxtbox).style.display = "table-row";
        //var splitdiv = defauletxtbox.split("div");
        //var numtext = $("#" + splitdiv[1]).data("tTextBox");
        //numtext.enable();
    }

    else if (dropDownList == "AT")
    {

        document.getElementById(admissionType).style.display = "table-row";

    }

    else if (dropDownList == "PT")
    {

        document.getElementById(packagetype).style.display = "table-row";
    }

    else if (dropDownList == "gender")
    {

        document.getElementById(gendertype).style.display = "table-row";
    }

    else
    {

    }
}



function setValueBasedOnFieldDynamically() {

   
    var myRegExpGender = /[Gg][Ee][Nn][Dd][Ee][Rr]/;
    var AgeRegExp = /[Aa][Gg][Ee]/;
    var DateRegExp = /[Dd][Aa][Tt][Ee]/;
    var TarnsactionCodeRegExp = /[Tt][Rr][Aa][Nn][Ss][Aa][Cc][Tt][Ii][Oo][Nn][Cc][Oo][Dd][Ee]/;
    var TarnsactionTypeRegExp = /[Tt][Rr][Aa][Nn][Ss][Aa][Cc][Tt][Ii][Oo][Nn][Tt][Yy][Pp][Ee]/;
    var DenailCodeRegExp=/[Dd][Ee][Nn][Aa][Ii][Ll][Cc][Oo][Dd][Ee]/;
    var itemId = $(this).attr('id');
    var lastChar = itemId.substr(itemId.length - 1);
    var numerictxtbox = "divnumerictxtbox" + lastChar;
    var datepicker = "divdatepicker" + lastChar;
    var txtarea = "divtextarea" + lastChar;
    var amountpicker = "divamountpicker" + lastChar;
    var defauletxtbox = "divdefaulttxtbox" + lastChar;
    var packagetype = "divpackagetype" + lastChar;
    var gendertype = "divgen" + lastChar;
    var admissionType = "divAdmissionType" + lastChar;
    var transactioncode = "divTransactionCode" + lastChar;
    var integertxtbox = "divintxtbox" + lastChar;
    var diagnosislst = "divdiag" + lastChar;
    var txtBox = "divtxtbox" + lastChar;
    var TransactionTypediv = "divTransactionType" + lastChar;
    
    var dropDownList = $("#" + itemId).val();

    var a1 = new Array();
    a1 = dropDownList.split("/");
    var datatype = a1[0];
    var tablename = a1[1];
    var key = a1[2];
    var keytable = a1[3];
    var text = a1[4];

    //based upon the item id the displaying the value
    var stateid = $("#FieldsDropDownList2").attr('id');
    var statedropdown = $("#" + stateid).val(); // based on state id package list is displayed 
    //    var radioselected = $("input:radio[name='TransactionType']:checked").val();
    
    document.getElementById(datepicker).style.display = "none";
    document.getElementById(numerictxtbox).style.display = "none";
    document.getElementById(amountpicker).style.display = "none";
    document.getElementById(txtarea).style.display = "none";
    document.getElementById(defauletxtbox).style.display = "none";
    document.getElementById(packagetype).style.display = "none";
    document.getElementById(gendertype).style.display = "none";
    document.getElementById(admissionType).style.display = "none";
    document.getElementById(transactioncode).style.display = "none";
    document.getElementById(integertxtbox).style.display = "none";
    document.getElementById(diagnosislst).style.display = "none";
    document.getElementById(txtBox).style.display = "none";
    document.getElementById(TransactionTypediv).style.display = "none"

    var fundDropDown = $('#operatorDropDownList' + lastChar);
    var fundDropDownData = fundDropDown.data('tDropDownList')


    var TransaType = $('#TransactionType' + lastChar);
    var TransaTypeData = TransaType.data('tDropDownList')


    if (dropDownList == "") {

        if (text == "Diagnosis" && keytable != "" && key != "") {
            document.getElementById(txtBox).style.display = "none";
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });



            document.getElementById(diagnosislst).style.display = "table-row";

            $.ajax({
                type: "Get",
                url: '../RuleConfiguration/getDiagnosisDynamic?TableName=' + keytable,

                dataType: "Json",
                success: function (data) {
                     
                    var txtArea = "diagnosis" + lastChar;
                    var select = $("#" + txtArea);
                    $.each(data, function (index, itemData) {


                        select.append($('<option/>', {
                            value: itemData.value,
                            text: itemData.Text,
                            title: itemData.Text
                        }));

                    });

                }

            });
            document.getElementById(txtarea).style.display = "none";
            document.getElementById(diagnosislst).style.display = "table-row";
        }


        else if (TarnsactionCodeRegExp.test(text) && keytable != "" && key != "") {



            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });



            document.getElementById(transactioncode).style.display = "table-row";
        }


        else if (TarnsactionTypeRegExp.test(text) && keytable != "" && key != "") {

            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });

            $.get('../RuleConfiguration/getTransactionTypeDynamically?TableName=' + keytable, function (data) {

                TransaTypeData.dataBind(data);
                TransaTypeData.loader.hideBusy();
                TransaType.select(0);

            });
            document.getElementById(TransactionTypediv).style.display = "table-row";


        }


        else if (DenailCodeRegExp.test(text) && keytable != "" && key != "") {
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });
            $.get('../RuleConfiguration/getDenialDynamic?TableName=' + keytable, function (data) {
                TransaTypeData.dataBind(data);
                TransaTypeData.loader.hideBusy();
                TransaType.select(0);
            });
            document.getElementById(TransactionTypediv).style.display = "table-row";

        }

        else if (myRegExpGender.test(text) && keytable == "" && key == "") {
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });

            document.getElementById(gendertype).style.display = "table-row";
            document.getElementById(txtBox).style.display = "none";
        }



        else if (text == "PackageType" && keytable == "" && key == "") {
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });
            document.getElementById(packagetype).style.display = "table-row";
        }


        else if (AgeRegExp.test(text) && keytable == "" && key == "" && text != "PackageType") {

            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });
            document.getElementById(numerictxtbox).style.display = "table-row";
            document.getElementById(txtBox).style.display = "none";
        }

        else if (datatype == "datetime" && keytable == "" && key == "") {

            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });
            document.getElementById(datepicker).style.display = "table-row";
            document.getElementById(txtBox).style.display = "none";
        }

        else if (DateRegExp.test(text)) {
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });
            document.getElementById(txtBox).style.display = "none";
            document.getElementById(datepicker).style.display = "table-row";
        }

        else if (TarnsactionCodeRegExp.test(text)) {
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0);
            });
            document.getElementById(txtBox).style.display = "none";
            document.getElementById(transactioncode).style.display = "table-row";

        }

        else if (dropDownList == "Diagnosis" || dropDownList == "gender" || dropDownList == "AT" || dropDownList == "PT" || dropDownList == "TC") {
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });
        }



        else if (dropDownList == "txtarea") {

            $.get('../RuleConfiguration/GetOperatorforTextArea', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });

        }


        else {
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0);
            });
            document.getElementById(txtBox).style.display = "table-row";
        }

    }

    else {
        if (dropDownList == "Diagnosis" || dropDownList == "gender" || dropDownList == "AT" || dropDownList == "PT" || dropDownList == "TC") {
            $.get('../RuleConfiguration/getGeneralOperatorsimplified', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });
        }
        else if (dropDownList == "txtarea") {

            $.get('../RuleConfiguration/GetOperatorforTextArea', function (data) {
                fundDropDownData.dataBind(data);
                fundDropDownData.loader.hideBusy();
                fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0
            });

        }
        else {
            $.ajax({
                type: "Get",
                url: '../RuleConfiguration/getGeneralOperator',

                dataType: "Json",
                success: function (data) {
                    fundDropDownData.dataBind(data);
                    fundDropDownData.loader.hideBusy();
                    fundDropDown.select(0); // select first combo item, or use fundDropDown.value('0') in case the first combo item has value set to 0

                }
            });

        }

        if (dropDownList == "numerictxtbx" || dropDownList == "NOD" || dropDownList == "IncidentNOD" || dropDownList == "ProLongdAdmission" || dropDownList == "MultipleAdmissionSingleURN"
        || dropDownList == "RepeatedAdmission") {

            document.getElementById(numerictxtbox).style.display = "table-row";

        }

        else if (dropDownList == "PD" || dropDownList == "TD" || dropDownList == "TAC") {
            document.getElementById(integertxtbox).style.display = "table-row";

        }

        else if (dropDownList == "Dta" || dropDownList == "Dtd" || dropDownList == "Dta1") {


            document.getElementById(datepicker).style.display = "table-row";



        }

        else if (dropDownList == "txtbx" || dropDownList == "AC" || dropDownList == "AB" || dropDownList == "PerClmtxtbx") {

            document.getElementById(amountpicker).style.display = "table-row";

        }

        else if (dropDownList == "TC") {
            document.getElementById(transactioncode).style.display = "table-row";

        }

        else if (dropDownList == "txtarea") {

            document.getElementById(txtarea).style.display = "table-row";

            //populating the values in to listbox

            $.ajax({
                type: "Get",
                url: '../RuleConfiguration/getAilments',

                dataType: "Json",
                success: function (data) {

                    var txtArea = "txtarea" + lastChar;
                    var select = $("#" + txtArea);
                    $.each(data, function (index, itemData) {

                        select.append($('<option/>', {
                            value: itemData.Value,
                            text: itemData.Text,
                            title: itemData.Text
                        }));

                    });

                }

            });

        }

        else if (dropDownList == "Diagnosis") {
             

            $("#row" + lastChar).removeClass("row");
            $("#divFieldsDropDownList" + lastChar).removeClass("block");
            $("#divoperatorDropDownList" + lastChar).removeClass("block");
            $("#ValueField" + lastChar).removeClass("block");
            $("#divLogicaloperatorDropDownList" + (parseInt(lastChar) + 1)).removeClass("block");

            $("#row" + lastChar).addClass("Packagerow");
            $("#divFieldsDropDownList" + lastChar).addClass("block1");
            $("#divoperatorDropDownList" + lastChar).addClass("block1");
            $("#ValueField" + lastChar).addClass("blockPackage");
            $("#divLogicaloperatorDropDownList" + (parseInt(lastChar) + 1)).addClass("block1");

            document.getElementById(diagnosislst).style.display = "table-row";

            $.ajax({
                type: "Get",
                url: '../RuleConfiguration/getDiagnosis?stCode=' + statedropdown,

                dataType: "Json",
                success: function (data) {

                    var txtArea = "diagnosis" + lastChar;
                    var select = $("#" + txtArea);
                    $.each(data, function (index, itemData) {

                        select.append($('<option/>', {
                            value: itemData.Value,
                            text: itemData.Text,
                            title: itemData.Text
                        }));

                    });

                }

            });



        }

        else if (dropDownList == "") {

            document.getElementById(defauletxtbox).style.display = "table-row";

        }

        else if (dropDownList == "URN") {
            document.getElementById(txtBox).style.display = "table-row";
            //document.getElementById(defauletxtbox).style.display = "table-row";
            //var splitdiv = defauletxtbox.split("div");
            //var numtext = $("#" + splitdiv[1]).data("tTextBox");
            //numtext.enable();
        }

        else if (dropDownList == "AT") {

            document.getElementById(admissionType).style.display = "table-row";

        }

        else if (dropDownList == "PT") {

            document.getElementById(packagetype).style.display = "table-row";
        }

        else if (dropDownList == "gender") {

            document.getElementById(gendertype).style.display = "table-row";
        }

        else {

        }
    
    
    
    }

}
















function GetDistrict()
{
     
    var stateCode = $("#StateName").val();
    var districtList = $('#DistrictName').data('tDropDownList');

    $.ajax({
        type: "Get",
        url: '../RuleConfiguration/GetDistNames?stCode=' + stateCode,

        dataType: "Json",
        success: function (data)
        {

            districtList.dataBind(data);

        }

    });

}

function GetEmpHospital()
{
    var stateCode = $("#StateName").val();
    var distCode = $("#DistrictName").val();

    $.ajax({
        type: "Get",
        url: '../RuleConfiguration/GetHospitalList?stCode=' + stateCode + "&disCode=" + distCode,

        dataType: "Json",
        success: function (data)
        {

            var select = $("#EmpHosp");
            select.empty();
            $.each(data, function (index, itemData)
            {

                select.append($('<option/>', {
                    value: itemData.Value,
                    text: itemData.Text
                }));

            });

        }

    });
}

function CheckValueForInvestigator()
{
     
    $("#investigatorName").hide();
    $("#divInformationrequestHeader").hide();
    $("#divInformationRequest").hide();
    $("#ApproverRemarks").hide();
    $("#lbleffdate").hide();
    $("#diveffdate").hide();
    $("#lbleffTodate").hide();
    $("#diveffTodate").hide();

    var value = $("#Action").val();
    var exevalue = $("#ExecutionTypo").val();
    if (value == "PI")
    {
        $("#investigatorName").show();
    }
    if (value == "IR")
    {
        $("#divInformationrequestHeader").show();
        $("#divInformationRequest").show();
    }
    if (value == "AF")
    {
        $("#ApproverRemarks").show();
    }
    if (exevalue == "SpecificCases")
    {
        $("#lbleffdate").show();
        $("#diveffdate").show();
        $("#lbleffTodate").show();
        $("#diveffTodate").show();
    }

}

function CheckValueForOtherFieldRT()
{
     
    //document.getElementById('others1').style.display = "none";
    //document.getElementById('others11').style.display = "none";

    //$("#infoLabel").css("padding-left", "7%");

    var value = $("#ReqType").val();
    if (value == "Others")
    {
        //document.getElementById('others1').style.display = "inline-block";
        //document.getElementById('others11').style.display = "inline-block";

        //$("#infoLabel").css("padding-left", "9%");
    }
}

function CheckValueForOtherFieldIT()
{
     
    //document.getElementById('others2').style.display = "none";
    //document.getElementById('others22').style.display = "none";

    var value = $("#InfoType").val();
    if (value == "IR16")
    {
        //document.getElementById('others2').style.display = "inline-block";
        //document.getElementById('others22').style.display = "inline-block"

    }

}

function getDatePicker()
{
    var dateValue = $('#effdate').data('tDatePicker').value();
    $('#effTodate').data('tDatePicker').min(dateValue);
}

function clkruleGroup(type)
{
    if (type=='new')
    {
        document.getElementById('OldruleGroup').style.display = 'none';
        document.getElementById('newruleGroup').style.display = 'block';
    }
    else
    {
        document.getElementById('OldruleGroup').style.display = 'block';
        document.getElementById('newruleGroup').style.display = 'none';
    }
}

function SaveTransactionType(id)
{
     
    $.ajax({
        type: "Get",
        url: '../RuleConfiguration/SetRadio?id=' + id,
        async: false,
        cache: false,
        dataType: "Json",
        success: function (data) { }

    });

    for (var i = 1; i <= 9; i++)
    {

        var FieldDropDown = $('#FieldsDropDownList' + i).data('tDropDownList');
        FieldDropDown.reload();

        $('#operatorDropDownList' + i).data('tDropDownList').value('');
        $('#operatorDropDownList' + i).data('tDropDownList').text('');

        $('#LogicaloperatorDropDownList' + parseInt(i + 1)).data('tDropDownList').value('');
        $('#LogicaloperatorDropDownList' + parseInt(i + 1)).data('tDropDownList').text('');

        if (i < 9)
        {
            var addrow = "row" + parseInt(i + 1);
            RowStyle = document.getElementById(addrow).style.display;
            if (RowStyle == "table-row")
            {

                document.getElementById(addrow).style.display = "none";
            }
        }

    }
    var action = $('#Action').data('tDropDownList');
    action.reload();

}
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function is2NumberKey(evt)
{
    var data = document.getElementById('txtruleScore').value;
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    {
        return false;
    }
    else
    {
        if (data.length >= 2)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

function addRowtoTable(actionID)
{
    // 
    var itemId = $(this).attr('id');
    var lastChar = actionID.substr(actionID.length - 1);

    var dropDownList = $("#" + itemId).val();
    var addedrow = "row" + lastChar;
    var currentrow = "row" + (parseInt(lastChar) - 1);
    var RowStyle;

    var styles = document.getElementById(currentrow);
    //var marLeft = document.getElementById(styles).style.marginLeft;
    var marLeft = getStyle(styles, 'margin-left');

    if (addedrow == "row0")
    {
        alert("Rule is too big. Please split the rule ");
    }

    else
    {

        if (dropDownList == "")
        {

            var i = parseInt(lastChar);
            i = i + 1;
            for (i; i < 9; i++)
            {
                var addrow = "row" + i;
                RowStyle = document.getElementById(addrow).style.display;
                if (RowStyle == "table-row")
                {

                    document.getElementById(addrow).style.display = "none";
                }

                else
                {

                }

            }

            document.getElementById(addedrow).style.display = "none";
        }

        else
        {

            RowStyle = document.getElementById(addedrow).style.display

            if (RowStyle == "none")
            {
                if (actionID.search("MainQuery") == -1)
                {
                    var pposition = marLeft.substring(0, marLeft.indexOf("px"));
                    document.getElementById(addedrow).style.display = "block";
                    //document.getElementByID(addedrow).style.setAttribute("marginTop","20px", false);
                    var tray_bar = document.getElementById(addedrow);
                    tray_bar.style.marginLeft = (parseInt(pposition) + 30) + "px";
                }
                else
                {
                    var pposition = marLeft.substring(0, marLeft.indexOf("px"));
                    document.getElementById(addedrow).style.display = "block";
                    //document.getElementByID(addedrow).style.setAttribute("marginTop","20px", false);
                    var tray_bar = document.getElementById(addedrow);
                    tray_bar.style.marginLeft = (parseInt(pposition)) + "px";
                    //for (var i = 1; i < 10; i++)
                    //{
                    //    var previousRowStyle = document.getElementById("row" + i).style.display;
                    //}
                }
            }
            else
            {
                for (var i = 1; i < 10; i++)
                {
                    var previousRowStyle = document.getElementById("row" + i).style.display;
                    if (previousRowStyle == 'none')
                    {
                        // 
                        if (actionID.search("MainQuery") != -1)
                        {
                            var pposition = marLeft.substring(0, marLeft.indexOf("px"));
                            document.getElementById("row" + i).style.display = "block";
                            var tray_bar = document.getElementById("row" + i);
                            tray_bar.style.marginLeft = (parseInt(pposition)) + "px";
                        }
                        break;
                    }
                }
            }
        }

    }
}

var getStyle = function (styles, styleName)
{
    // 
    var styleValue = "";
    if (document.defaultView && document.defaultView.getComputedStyle)
    {
        styleValue = document.defaultView.getComputedStyle(styles, "").getPropertyValue(styleName);
    }
    else if (styles.currentStyle)
    {
        styleName = styleName.replace(/\-(\w)/g, function (strMatch, p1)
        {
            return p1.toUpperCase();
        });
        styleValue = styles.currentStyle[styleName];
    }
    return styleValue;
}












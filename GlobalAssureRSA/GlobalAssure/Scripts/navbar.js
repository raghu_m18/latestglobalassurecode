$(document).ready(function() {
    $('.navbar a.dropdown-toggle').on('click', function (e) {
        //debugger
        var heights = 0;
        var elmnt = $(this).parent().parent();
        if (!elmnt.hasClass('nav')) {
            var li = $(this).parent();
            
            var heightParent = parseInt(elmnt.css('height').replace('px', '')) / 2;
          
            var widthParent = parseInt(elmnt.css('width').replace('px', '')) - 10;
           
            if (!li.hasClass('open'))
                li.addClass('open')
            else
                li.removeClass('open');     
            if (parseFloat(li[0].offsetTop) <= 0) {
                heights = parseFloat(li.offset().top) - parseFloat(li.offset().top);
            } else {
                heights = (parseFloat(li.offset().top) - parseFloat(li[0].offsetTop)-131);
            }
            $(this).next().css('top', heights + 'px');
            $(this).next().css('left', (widthParent+parseInt(10)) + 'px');
            
            return false;
        }
    });
});


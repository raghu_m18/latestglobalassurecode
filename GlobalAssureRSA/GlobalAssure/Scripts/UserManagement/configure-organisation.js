﻿
function ddlOrgCatagory() {
    var selectedText = $("#loadOrgCatId option:selected").text();
    if (selectedText == "Individual") {
        orgProfileCreation();
        $("#orgName").hide();
        $("#fullName").show();
        $("#orgCatCorp").hide();
        $("#orgCatIndividual").show();
        $("#CorpAddress").hide();
        $("#corpSameDiv").hide();

        $("#divOrglevel").show();
        $("#divFax").show();
        $("#divWebsite").show();

        $("#divOrglevelVal").hide();
        $("#divFaxVal").hide();
        $("#divWebsiteVal").hide();
    } else {
        $("#orgName").show();
        $("#fullName").hide();
        $("#orgCatCorp").show();
        $("#orgCatIndividual").hide();
        $("#CorpAddress").show();
        $("#corpSameDiv").show();

        $("#divOrglevel").hide();
        $("#divFax").hide();
        $("#divWebsite").hide();

        $("#divOrglevelVal").show();
        $("#divFaxVal").show();
        $("#divWebsiteVal").show();
    }
}

function ddlOrgType() {

    var selectedText = $("#loadOrgTypeId option:selected").text();
    if (selectedText == "External Investigation Agency") {

        investigatorCreation();
        getStates();

    } else {

        orgProfileCreation();

    }
}

function investigatorCreation() {

    $("#profile").text("Investigator Creation");
    $("#orgType").show();
    $("#addresses").hide();
    $("#accordion1").hide();
    $("#spocDetails").hide();
    $("#feeMaintenance").show();
    $("#bankDetails").show();
    $("#dvAreaOfOperation").show();
    $("#Reg_no_st_dv").hide();
    $("#PANno_dv").hide();

}
function orgProfileCreation() {

    $("#profile").text("Profile");
    $("#orgType").hide();
    $("#addresses").show();
    $("#accordion1").show();
    $("#spocDetails").show();
    $("#feeMaintenance").hide();
    $("#bankDetails").hide();
    $("#dvAreaOfOperation").hide();
    $("#_Reg_no_st_dv").hide();
    $("#_PANno_dv").hide();

}

function getStates() {
    $.ajax({
        url: '/MasterAPI/GetStateByCountry?CountryId=1',
        contentType: 'json',
        type: "GET",
        async: false,
        success: function (result) {

            $("#ddlAreaOfOperation").empty();
            $.each(result, function (index, optionData) {
                $("#ddlAreaOfOperation").append('<option value= ' + optionData.ID + '>' + optionData.Value + '</option>')
            });

            $('#ddlAreaOfOperation').multiselect({
                numberDisplayed: 0,
                includeSelectAllOption: true
            });

        }

    });
}

$("#txtCityId").autocomplete({
    minLength: 1,
    source: function (request, response) {
        $.ajax({
            //url: '@Url.Content("/UserManagement/GetCitiesAutoComplete")',  // works in .cshtml
            url: "/UserManagement/GetCitiesAutoComplete",
            type: "POST",
            dataType: "json",
            data: { cityName: request.term },
            success: function (data) {
                //debugger
                response($.map(data, function (name, val) {
                    return {
                        label: name.CityName,
                        value: val.CityName
                    }
                }));
            }
        });
    }
});

$("#txtBankName").autocomplete({
    minLength: 1,
    source: function (request, response) {
        $.ajax({
            //url: '@Url.Content("/UserManagement/GetBankNameAutoComplete")',  // works in .cshtml
            url: "/UserManagement/GetBankNameAutoComplete",
            type: "POST",
            dataType: "json",
            data: { bankName: request.term },
            success: function (data) {
                response($.map(data, function (item) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            }
        });
    }
});

$("#txtBranchName").autocomplete({
    minLength: 1,
    source: function (request, response) {
        $.ajax({
            //url: '@Url.Content("/UserManagement/GetBranchNameAutoComplete")',  // works in .cshtml
            url: "/UserManagement/GetBranchNameAutoComplete",
            type: "POST",
            dataType: "json",
            data: { branchName: request.term },
            success: function (data) {
                response($.map(data, function (item) {
                    return {
                        label: item,
                        value: item
                    }
                }));
            }
        });
    }
});

$(function () {

    $("#ddlAreaOfOperation").on('change', function () {
        //alert('ddlAreaOfOperation chnged');
        var selectedOptions = $('#ddlAreaOfOperation option:selected');
        var AOPIDs = $(this).val();
        $("#objAreaOfOperation").val(AOPIDs);

    });

});

var FilePaths = [];
var feeDetailIndex = 0;
var updateId = 0;
function addFeeMaintenanceDetail() {
    //debugger;
    //$("#feeGrid").show();
    
    var valid = $("#frmFeeMaintence").validationEngine('validate');
    var fee = $('#FeeMaintence_Fee').val();
    
    if (valid && fee != "") {
        //debugger       
        feeDetailIndex++;
        //alert(valid + " " + fee + " " + feeDetailIndex);  
        FilePaths.push(
            {
                "Fee": $('#FeeMaintence_Fee').val(),
                "ClaimServiceTypeText": $("#FeeMaintence_ClaimServiceType option:selected").text(),
                "ClaimServiceType": $("#FeeMaintence_ClaimServiceType option:selected").val(),
                "ProductType": $("#FeeMaintence_ProductType option:selected").val(),
                "CityId": $('#txtCityId').val(),
                "City": $('#txtCityId').val(),
                "StartDate": $('#feeStartDate').val(),
                "EndDate": $('#feeEndDate').val(),
                "TDSRule": $("#TDSruleId option:selected").val(),
                "TDSRate": $("#TDSRateId option:selected").val(),
                "TDSDefaultRule": $("#TDSDefaultRuleId option:selected").val(),
                "Srv_SalesTaxRule": $("#Srv_SalesTaxRuleId option:selected").val(),
                "LTDSCert": $('#LTDSCertId').val(),                
                "SNo": feeDetailIndex
            });
        //url: '@Url.Action("AddToFeeMaintenceGrid", "Organisational")'
        addToFeeMaintenceGrid();
    }
    else {
        $("#frmFeeMaintence").validationEngine();
    }
   
    //resetFeeMaintence();

}

function addToFeeMaintenceGrid() {
   // debugger;
        $.ajax({

            type: "POST",
            url: 'AddToFeeMaintenceGrid',
            data: JSON.stringify(FilePaths),
            datatype: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (result) {

                $("#feeDetailsGrid").html(result);

            },
            error: function (result) {

            }
        });
    }

function editFeeDetails(SNo) {
    //alert(SNo)
    //debugger;
    $("#addFeeMaintenanceDetail_dv").hide();
    $("#updateFeeMaintenanceDetail_dv").show();

    var feeDetails = JSON.stringify(FilePaths);
    //var jsonObj = $.parseJSON(feeDetails);
    var jsObj = JSON.parse(feeDetails);

    for (var i = 0; i < jsObj.length; ++i) {
        //do something with jsObj[i]
        if (i == (SNo - 1)) {
            var Fee = jsObj[i].Fee;
            var City = jsObj[i].City;
            var ClaimServiceType = jsObj[i].ClaimServiceType;            
            var ProductType = jsObj[i].ProductType;
            var StartDate = jsObj[i].StartDate;
            var EndDate = jsObj[i].EndDate;
            updateId = SNo;
            //alert(Fee + " " + City)
            $('#FeeMaintence_Fee').val(Fee);
            $("#FeeMaintence_ClaimServiceType option").filter(function () {
                return this.value == ClaimServiceType;
            }).prop('selected', true);
            $("#FeeMaintence_ProductType option").filter(function () {
                return this.value == ProductType;
            }).prop('selected', true);
            $('#txtCityId').val(City);
            $('#feeStartDate').val(StartDate);
            $('#feeEndDate').val(EndDate);

        }
    }
    return false;
}

function updateFeeMaintenanceDetail() {
    $("#addFeeMaintenanceDetail_dv").show();
    $("#updateFeeMaintenanceDetail_dv").hide();

    var feeDetails = JSON.stringify(FilePaths);
    var jsObj = JSON.parse(feeDetails);
    for (var i = 0; i < jsObj.length; ++i) {
        //debugger;
        if (i == (updateId - 1)) {
            
            findAndRemove(FilePaths, 'SNo', updateId);

            FilePaths.push(
                {
                    "Fee": $('#FeeMaintence_Fee').val(),
                    "ClaimServiceTypeText": $("#FeeMaintence_ClaimServiceType option:selected").text(),
                    "ClaimServiceType": $("#FeeMaintence_ClaimServiceType option:selected").val(),
                    "ProductType": $("#FeeMaintence_ProductType option:selected").val(),
                    "CityId": $('#txtCityId').val(),
                    "City": $('#txtCityId').val(),
                    "StartDate": $('#feeStartDate').val(),
                    "EndDate": $('#feeEndDate').val(),
                    
                    "SNo": updateId
                });
        }
    }
    addToFeeMaintenceGrid();

    }
   
function deleteFeeDetails(SNo) {
    //debugger;

    findAndRemove(FilePaths, 'SNo', SNo);
    
    addToFeeMaintenceGrid();

    return false;
}

function findAndRemove(array, property, value) {
    //debugger;
    $.each(array, function (index, result) {
        if (result[property] == value) {
            //Remove from array
            array.splice(index, 1);
        }
    });
}

function resetFeeMaintence() {
    $('#FeeMaintence_Fee').val('');
    $("#FeeMaintence_ClaimServiceType option:selected").removeAttr("selected");
    $("#FeeMaintence_ProductType option:selected").removeAttr("selected");
    $('#txtCityId').val('');
    $('#feeStartDate').val('');
    $('#feeEndDate').val('');
}



var today = new Date();

$("#feeStartDate").datepicker({
    format: 'dd/mm/yyyy',
    startDate: today
});

$("#feeStartDate").change(function () {
    //debugger;
    var selectedDate = $("#feeStartDate").val();

    $("#feeEndDate").datepicker({selectionStart:selectedDate,
        format: 'dd/mm/yyyy',
        startDate: selectedDate
    });

});

$("#AC_Current_from").datepicker({
    format: 'dd/mm/yyyy',
    endDate: today
});

function ddlTDDefaultRuleChange() {
    //alert('ddlTDDefaultRuleChange called');
    $.ajax({
        url: '/MasterAPI/GetStateByCountry?CountryId=1',
        contentType: 'json',
        type: "GET",
        async: false,
        success: function (result) {

            $("#ddlTaxRule").empty();
            $.each(result, function (index, optionData) {
                $("#ddlTaxRule").append('<option value= ' + optionData.ID + '>' + optionData.Value + '</option>')
            });
        }

    });
}

var isCompCodeExists = "no";
var modelErrors = '';
var status = '';
$(document).ready(function () {
    //debugger

    $("#btnCancelOrgData").click(function () {

        //window.location.href = '@Url.Content("~/Intranet/IntranetHome?Type=Intranet")'; // works in .cshtml
        window.location.href = "/Intranet/IntranetHome?Type=Intranet";
    });

    $("#orgConfigForm").validationEngine();

    if ('@ViewBag.DataExist' == '@CrossCutting_Constants.Exist') {
        //debugger
        $(document).ready(function () {
            var valid = $("#orgConfigForm").validationEngine('validate');
            
            $('#orgConfigForm input[type="Text"],input[type="DropDownList"],input[type="radio"],input[id="file"],select').prop("disabled", true);
            
            $('#btnSubmitOrgData').hide();
            $('#addSpocGrd').hide();
            $('#defineleveldiv').hide();            
            $('#logofororg').show();
            $("#Org_Logo").attr('src', "");
            $("#Org_Logo").attr('src', '@ViewBag.orglogo').css("width", "250px");

            if ('@ViewBag.IsRegistered' == 'True') {
                $("#yes").prop("checked", true);
                $('#corporateAddressDiv').hide();                
                $("#corpSameDiv").hide();
            }
            if ('@ViewBag.RegAddressID' == '@ViewBag.MailingAddressID') {
                $("#regAdd").prop("checked", true);
                $('#mailingAddressDiv').hide();
            }
            if ('@ViewBag.InsurerType' == '171') {
                $('#InsCOmpCode').show();
            }
          
        })
    }
    else {
        //$("#orgConfigForm").validationEngine();
        var date = new Date();
        d = new Date(date);
        d.setDate(d.getDate())
        var RegDate = new Date(d);
        $("#Reg_Date").datepicker({
            format: 'dd/mm/yyyy',
            endDate: RegDate
        });



        $("#btnSubmitOrgData").click(function () {
            
            //debugger
            var CodeExists = InsCompCodeExists();
            var valid = $("#orgConfigForm").validationEngine('validate')
            if (isCompCodeExists == "no") {
                if (valid) {
                    var insCompName = $("#insOrgName :selected").text();
                    if (insCompName == 'Select Organisation') {
                        insCompName = '';
                    }
                    var ddlTxt = $("#loadOrgTypeId option:selected").text();
                    if (ddlTxt == "Insurer") {
                        //$("#Org_Name").val(insCompName);
                    }

                    var formdata = new window.FormData($('#orgConfigForm')[0]);
                    
                    if (status == '') {
                        //debugger
                        //var url = '@Url.Content("~/Organisational/ConfigureOrganisationDetails?OrgInsName=")' + insCompName; // // Working in .cshtml file
                        var url = "/Organisational/ConfigureOrganisationDetails?OrgInsName=" + insCompName; // Working in .js file

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: formdata,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                //debugger
                                modelErrors = result;
                                var resType = typeof (result);

                                if (resType == 'object') {
                                    // DisplayValidationMessages(result);
                                }
                                else if (resType == 'string') {
                                    status = 'Success';
                                    var res = result.split('//');
                                    var resType = res[0];
                                    if (resType == 'G') {
                                        AlertMessage(result, 5000, null);
                                        window.location.href = 'ConfigureOrganisation?Type=Intranet';
                                        //window.location.href = '@Url.Content("~/ClaimsLive/Organisational/ConfigureOrganisation?Type=Intranet")';
                                    }
                                    else if (resType == 'R') {
                                        // AlertMessage(response, 5000, 'fail');
                                        AlertMessage(result, 5000, 'fail');
                                    }

                                }
                            }
                        })
                        //debugger
                        if (status == 'Success') {
                            return false;
                        }
                    }
                }
                else {
                    $("#orgConfigForm").validationEngine();
                }
            }
            else if (isCompCodeExists == "yes") {
                AlertMessage('//Insurance Company Code Already Exists!!', "5000", "fail");
            }
        })

    }

});
var SPOCFilePaths = [];
var SPOCIndex = 0;

function test() {
    //debugger
    var valid = $("#frmSpoc").validationEngine('validate')
    
    var SpocMobileContacts = [];
    var SpocEmailContacts = [];
    var SpocPhoneContacts = [];
    if (valid) {
        for (var i = 0; i <= MobileIndex; i++) {
            SpocMobileContacts.push({
                "Value": $('#SpocMobileContacts_' + i + '__Value').val(),
            });
        }
        for (var i = 0; i <= PhoneIndex; i++) {
            SpocPhoneContacts.push({
                "Value": $('#SpocPhoneContacts_' + i + '__Value').val(),
            });
        }
        for (var i = 0; i <= EmailIndex; i++) {
            SpocEmailContacts.push({
                "Value": $('#SpocEmailContacts_' + i + '__Value').val(),
            });
        }

        if ($('#Name').val() != "") {
            //debugger
            var name = $('#Name').val();
            var res = SpocDuplicate();
            if (res == false) {
                SPOCIndex++;
                SPOCFilePaths.push(
                    {
                        "Name": $('#Name').val(),
                        "PhoneNo": $('#SpocPhoneContacts_0__Value').val(),
                        "EmailId": $('#SpocEmailContacts_0__Value').val(),
                        "Mobile": $('#SpocMobileContacts_0__Value').val(),
                        "SpocMobileContacts": SpocMobileContacts,
                        "SpocPhoneContacts": SpocPhoneContacts,
                        "SpocEmailContacts": SpocEmailContacts,
                        "SpocAddress.CountryID": $('#SpocAddressobjAddressddlCountry option:selected').val(),
                        "SpocAddress.StateID": $('#SpocAddressddlState option:selected').val(),
                        "SpocAddress.DistrictID": $('#SpocAddressddlDistrict option:selected').val(),
                        "SpocAddress.CityID": $('#SpocAddressddlCity option:selected').val(),
                        "SpocAddress.AreaID": $('#SpocAddressddlArea option:selected').val(),
                        "SpocAddress.Address1": $('#SpocAddressAddress1').val(),
                        "SpocAddress.Address2": $('#SpocAddressAddress2').val(),
                        "SpocAddress.Address3": $('#SpocAddressAddress3').val(),
                    });
                //url: '@Url.Action("AddToGrid", "Organisational")',
                $.ajax({

                    type: "POST",                    
                    url: 'AddToGrid',
                    data: JSON.stringify(SPOCFilePaths),
                    datatype: 'json',
                    contentType: "application/json; charset=utf-8",
                    success: function (result) {

                        $("#GridSpoc").html(result);
                        
                    },
                    error: function (result) {
                        //debugger
                    }
                });
            }
            else {
                alert("Duplicate SPOC- PhoneNo & EmailId should not be duplicate");
            }
        }
    }
    else {
        $("#frmSpoc").validationEngine();
    }


}

function loadInsCodeForOrg(e) {
    //debugger
    var insCode = $("#insOrgName").val();
    $("#txtInsCoCode").val(insCode);
}

function loadOrgStructure() {

    var levels = $("#loadNoOfLevels").val();
    //debugger
    if (levels == "") {
        $("#divDefineLevels").hide();//Added by RajaShekar against BugID:16590
    }
    else {
        $("#divDefineLevels").show();
    }
    //$("#loadOrgStructureDiv").load('../Organisational/_LoadOrgStructureLevels?noOfLevels=' + levels);
    $.ajax({
        url: '../Organisational/_LoadOrgStructureLevels?noOfLevels=' + levels,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            //debugger
            $("#loadOrgStructureDiv").html(result);
        }
    })
}


function LoadInsCompany(type) {
    //debugger
    if (type.value == "Other")
        $("#insCompDiv").show();
    else if (type.value == "Self")
        $("#insCompDiv").hide();
}

function DisplayCorpAddress(corpType) {
    //debugger
    if (corpType.value == "True") {
        $("#corporateAddressDiv").hide('slow');
        $("#corpSameDiv").hide();
    }
    else if (corpType.value == "False") {
        $("#corporateAddressDiv").show('slow');
        $("#corpSameDiv").show();
    }
}

function IncCode(evt) {

    var txt = $("#txtInsCoCode").val().length;
    var charCode = (evt.which) ? evt.which : event.keyCode

    if ((charCode < 48 || charCode > 57)) {

        return false;
    }
    if (txt < 2) {
        return true;
    }
    else { return false; }

}
$("#txtInsCoCode").on('change', function () {
    //debugger
    InsCompCodeExists();
});
$("#txtInsCoCode").on('blur', function () {
    //debugger
    InsCompCodeExists();
});
function SpocDuplicate() {
    //debugger
    var PhoneNo = $('#SpocPhoneContacts_0__Value').val();
    var EmailId = $('#SpocEmailContacts_0__Value').val();

    if (SPOCIndex > 0) {
        var count = 0;
        for (var i = 0; i < SPOCFilePaths.length; i++) {
            if (SPOCFilePaths[i].PhoneNo == PhoneNo || SPOCFilePaths[i].EmailId == EmailId)
            {
                return true;
               
            }
            else
            {
                if (SPOCFilePaths.length == SPOCFilePaths.length - 1) {

                    return false;
                }
            }

        }
    }
    return false;
}
function InsCompCodeExists() {
    //debugger;
    var code = $("#txtInsCoCode").val();
    if (code != "") {
        $.ajax({
            url: '../Organisational/CheckInsCodeExits?code=' + code,
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                //debugger

                if (result == 'yes') {
                    $("#txtInsCoCode").validationEngine('showPrompt', 'This Code already exists!!', 'error', 'topRight', true);
                    isCompCodeExists = "yes";

                }
                else {
                    isCompCodeExists = "no";
                }
            }
        });
    }
}
function DisplayMailingAddress(mailingType) {
    //debugger

    if (mailingType.value == "Registered" || mailingType.value == "Corporate") {
        $("#mailingAddressDiv").hide('slow');
    }
    else if (mailingType.value == "Others") {
        $("#mailingAddressDiv").show('slow');
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace GlobalAssure.Admin
{
    public class clsEmailConfig
    {
        public string TransactionId { get; set; }
        public string ToEmail { get; set; }
        public string BCCEmail { get; set; }
        public string CCEmail { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string EmailAttachment { get; set; }
        public string SendStatus { get; set; }
        public string SendError { get; set; }
        public int EmailConfigId { get; set; }
        public string SMTPHost { get; set; }
        public string SMTPPort { get; set; }
        public string SMTPUserName { get; set; }
        public string SMTPPassword { get; set; }
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public int OrderNo { get; set; }
        public int Status { get; set; }
        public string LastModifyBy { get; set; }
        public int SecureSMTP { get; set; }
        public int EmailLimitCount { get; set; }
        public int SMSTriggerCount { get; set; }

        public bool SaveEmailConfig(ref string  Message)
        {
            bool isSaved = false;
            try
            {

                SqlCommand cmd = new SqlCommand("usp_SaveEmailConfig");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@s_SMTP_Host", SMTPHost);
                cmd.Parameters.AddWithValue("@s_SMTP_Port", SMTPPort);
                cmd.Parameters.AddWithValue("@s_SMTP_User_Name", SMTPUserName);               
                cmd.Parameters.AddWithValue("@s_SMTP_Password", SMTPPassword);
                cmd.Parameters.AddWithValue("@s_From_Email", FromEmail);
                cmd.Parameters.AddWithValue("@s_From_Name", FromName);
                cmd.Parameters.AddWithValue("@n_SecureSMTP", SecureSMTP);
                cmd.Parameters.AddWithValue("@n_OrderNo", OrderNo);
                cmd.Parameters.AddWithValue("@n_EmailLimitCount", EmailLimitCount);
                cmd.Parameters.AddWithValue("@n_SMSTriggerCount", SMSTriggerCount);
                cmd.Parameters.AddWithValue("@n_Status", Status);
                cmd.Parameters.AddWithValue("@s_Last_Modify_User", LastModifyBy);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);

                if (Result > 0)
                {
                    isSaved = true;
                }

            }
            catch (Exception ex )
            {
                Message = ex.Message;
            }

            return isSaved;
        }

        public bool SaveEmailLog(ref string Message)
        {
            bool isSaved = false;
            try
            {

                SqlCommand cmd = new SqlCommand("usp_SaveEmailSendLog");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@s_Transaction_Id", TransactionId);
                cmd.Parameters.AddWithValue("@n_Email_Config_Id", EmailConfigId);
                cmd.Parameters.AddWithValue("@s_SMTP_User_Name", SMTPUserName);
                cmd.Parameters.AddWithValue("@s_From_Email", FromEmail);
                cmd.Parameters.AddWithValue("@s_From_Name", FromName);
                cmd.Parameters.AddWithValue("@s_Email_To", ToEmail);
                cmd.Parameters.AddWithValue("@s_Email_BCC", BCCEmail);
                cmd.Parameters.AddWithValue("@s_Email_CC", CCEmail);
                cmd.Parameters.AddWithValue("@s_Email_Subject", EmailSubject);
                cmd.Parameters.AddWithValue("@s_Email_Body", EmailBody);
                cmd.Parameters.AddWithValue("@s_Attachment", EmailAttachment);
                cmd.Parameters.AddWithValue("@s_Send_Status", SendStatus);
                cmd.Parameters.AddWithValue("@s_Error", SendError);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);

                if (Result > 0)
                {
                    isSaved = true;
                }

            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSaved;
        }

        public bool UpdateEmailConfig(ref string Message)
        {
            bool isSaved = false;
            try
            {

                SqlCommand cmd = new SqlCommand("usp_UpdateEmailConfig");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@n_Config_Id", EmailConfigId);
                cmd.Parameters.AddWithValue("@s_SMTP_Host", SMTPHost);
                cmd.Parameters.AddWithValue("@s_SMTP_Port", SMTPPort);
                cmd.Parameters.AddWithValue("@s_SMTP_User_Name", SMTPUserName);
                cmd.Parameters.AddWithValue("@s_SMTP_Password", SMTPPassword);
                cmd.Parameters.AddWithValue("@s_From_Email", FromEmail);
                cmd.Parameters.AddWithValue("@s_From_Name", FromName);
                cmd.Parameters.AddWithValue("@n_SecureSMTP", SecureSMTP);
                cmd.Parameters.AddWithValue("@n_OrderNo", OrderNo);
                cmd.Parameters.AddWithValue("@n_EmailLimitCount", EmailLimitCount);
                cmd.Parameters.AddWithValue("@n_SMSTriggerCount", SMSTriggerCount);
                cmd.Parameters.AddWithValue("@n_Status", Status);
                cmd.Parameters.AddWithValue("@s_Last_Modify_User", LastModifyBy);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);

                if (Result > 0)
                {
                    isSaved = true;
                }

            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSaved;
        }

        public DataTable GetEmailConfigData(ref string Message) 
        {
            DataTable dtEmailConfig = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetEmailConfigAdminData");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                dtEmailConfig = DAL.ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return dtEmailConfig;

        }


        public DataTable GetEmailLogData(int PageIndex,int PageSize,string FromDate,string ToDate, out int RecordCount,ref string Message, char isDownload = '0')
        {
            DataTable dtEmailLog = new DataTable();
            RecordCount = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetEmailLog");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@n_PageIndex", PageIndex);
                cmd.Parameters.AddWithValue("@n_PageSize", PageSize);
                cmd.Parameters.AddWithValue("@dt_FromDate", FromDate);
                cmd.Parameters.AddWithValue("@dt_ToDate", ToDate);
                cmd.Parameters.AddWithValue("@b_IsDownload", isDownload);
                cmd.Parameters.Add("@n_RecordCount", SqlDbType.Int, 10);
                cmd.Parameters["@n_RecordCount"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                dtEmailLog = DAL.ExecuteDataTable(cmd);

                RecordCount = Convert.ToInt32(cmd.Parameters["@n_RecordCount"].Value);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return dtEmailLog;

        }

    }
}
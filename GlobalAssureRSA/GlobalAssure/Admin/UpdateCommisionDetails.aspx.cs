﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class UpdateCommisionDetails : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dvCommision.Visible = false;
            }
        }

        protected void btnRefersh_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/UpdateCommisionDetails.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdnUser.Value = txtUser.Text;
            BindGrid(hdnUser.Value);
        }

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{
        //    try
        //    {                   
        //        foreach (GridViewRow gvrow in gvCommision.Rows)
        //        {
        //            CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
        //            if (chk != null & chk.Checked)
        //            {
        //                clsAdmin objCommisionUpdate = new clsAdmin();
        //                //Getting the Details to update
        //                string PrivilageID = gvCommision.DataKeys[gvrow.RowIndex].Value.ToString();
        //                var CommisionPercentage = gvrow.FindControl("txtCommisionPercentage") as TextBox;
        //                var IsCommision = gvrow.FindControl("txtIsCommision") as TextBox;
        //                var Mode = gvrow.FindControl("txtMode") as TextBox;

        //                if (Mode.Text == "MONTHLY" || Mode.Text == "WEEKLY")
        //                {
        //                    if (IsCommision.Text == "1" || IsCommision.Text == "0")
        //                    {                               
        //                        //Saving the Update Value in Database                                
        //                        objCommisionUpdate.PrivilageID = Convert.ToInt32(PrivilageID);                               
        //                        if (CommisionPercentage.Text != "")
        //                        {
        //                            objCommisionUpdate.CommisionPercentage = Convert.ToDecimal(CommisionPercentage.Text);
        //                        }
        //                        else
        //                        {
        //                            objCommisionUpdate.CommisionPercentage = null;
        //                        }
        //                        if (IsCommision.Text == "1")
        //                        {
        //                            objCommisionUpdate.IsCommision = true;
        //                        }
        //                        else
        //                        {
        //                            objCommisionUpdate.IsCommision = false;
        //                        }                                
        //                        objCommisionUpdate.Mode = Mode.Text;
        //                        objCommisionUpdate.LastModifiedBy = User.Identity.Name;                                
        //                        objCommisionUpdate.UpdateCommision(ref Message);
        //                    }
        //                    else
        //                    {
        //                        clsCommon.ShowMessage("Is Commision Should be 1 or 0");
        //                        return;
        //                    }
                            
        //                }
        //                else
        //                {
        //                    clsCommon.ShowMessage("The Mode Should be WEEKLY or MONTHLY");
        //                    return;
        //                }

        //            }                  

        //        }
        //        clsCommon.ShowMessage("Update Successfully");
        //        dvCommision.Visible = false;
        //        txtUser.Text = "";
        //    }
        //    catch (Exception ex)
        //    {
        //        clsCommon.ShowMessage("Error! Please Try Again Later");
        //    }

        //}

        protected void gvCommision_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Status
                    TableCell StatusCell = e.Row.Cells[7];
                    Label status = (e.Row.FindControl("lblIsValid") as Label);
                    StatusCell.Font.Bold = true;
                    if (status.Text.ToLower() == "true")
                    {
                        Label Status = (e.Row.FindControl("lblIsValid") as Label);
                        Status.Text = "Active";
                        StatusCell.ForeColor = System.Drawing.Color.Green;
                    }
                    else if (status.Text.ToLower() == "false")
                    {
                        Label Status = (e.Row.FindControl("lblIsValid") as Label);
                        Status.Text = "InActive";
                        StatusCell.ForeColor = System.Drawing.Color.Red;
                    }
                    //Is Commision
                    //TableCell IsCommision = e.Row.Cells[5];
                    Label IsComm = (e.Row.FindControl("lblIsCommision") as Label);
                    //IsCommision.Font.Bold = true;
                    if (IsComm.Text.ToLower() == "true")
                    {
                        IsComm.Text = "1";
                    }
                    else if (IsComm.Text.ToLower() == "false")
                    {
                        IsComm.Text = "0";
                    }
                }
            }
            catch (Exception ex)
            {
               // clsCommon.ShowMessage("Error! Please Try Again Later");
            }

        }      

        protected void gvCommision_RowCommand(object sender, GridViewCommandEventArgs e)
        {


        }

        //protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        //{
        //    for (int i = 0; i < gvCommision.Rows.Count; i++)
        //    {
        //        CheckBox cb = (CheckBox)gvCommision.Rows[i].Cells[0].FindControl("chkSelect");
        //        TextBox txtCom = (TextBox)gvCommision.Rows[i].Cells[0].FindControl("txtCommisionPercentage");
        //        TextBox txtIsCom = (TextBox)gvCommision.Rows[i].Cells[0].FindControl("txtIsCommision");
        //        TextBox txtMode = (TextBox)gvCommision.Rows[i].Cells[0].FindControl("txtMode");
        //        if (cb.Checked)
        //        {
        //            txtCom.Enabled = true;
        //            txtIsCom.Enabled = true;
        //            txtMode.Enabled = true;
        //        }
        //        else
        //        {
        //            txtCom.Enabled = false;
        //            txtIsCom.Enabled = false;
        //            txtMode.Enabled = false;
        //        }
        //    }
        //}

        protected void gvCommision_RowEditing(object sender, GridViewEditEventArgs e)
        {            
            gvCommision.EditIndex = e.NewEditIndex;
            BindGrid(hdnUser.Value);
        }

        protected void gvCommision_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvCommision.EditIndex = -1;
            BindGrid(hdnUser.Value);
        }

        public void BindGrid(string UserName)
        {
            clsAdmin objUser = new clsAdmin();
            objUser.Username = UserName;
            DataTable dtUserDetails = objUser.GetUserDetails(ref Message);
            if (dtUserDetails.Rows.Count > 0)
            {
                gvCommision.DataSource = dtUserDetails;
                gvCommision.DataBind();
                dvCommision.Visible = true;
            }
            else
            {
                gvCommision.DataSource = null;
                gvCommision.DataBind();
                dvCommision.Visible = false;
                clsCommon.ShowMessage("No Records Found");
            }
        }

        protected void gvCommision_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            clsAdmin objCommisionUpdate = new clsAdmin();
            Label PrivilageID = gvCommision.Rows[e.RowIndex].FindControl("lblPrivilageID") as Label;
            TextBox CommisionPercentage = gvCommision.Rows[e.RowIndex].FindControl("txtCommisionPercentage") as TextBox;
            TextBox IsCommision = gvCommision.Rows[e.RowIndex].FindControl("txtIsCommision") as TextBox;
            TextBox Mode = gvCommision.Rows[e.RowIndex].FindControl("txtMode") as TextBox;

            string strCommisionPercentage = CommisionPercentage.Text;
            if (!string.IsNullOrEmpty(strCommisionPercentage))
            {
                var regexCommisionPercentage = new Regex("^[0-9.]*$");
                if (regexCommisionPercentage.IsMatch(strCommisionPercentage))
                {
                    if (Mode.Text == "MONTHLY" || Mode.Text == "WEEKLY" || Mode.Text == "CUTANDPAY")
                    {
                        if (IsCommision.Text == "1" || IsCommision.Text == "0")
                        {
                            //Saving the Update Value in Database                                
                            objCommisionUpdate.PrivilageID = Convert.ToInt32(PrivilageID.Text);
                            if (CommisionPercentage.Text != "")
                            {
                                objCommisionUpdate.CommisionPercentage = Convert.ToDecimal(CommisionPercentage.Text);
                            }
                            
                            if (IsCommision.Text == "1")
                            {
                                objCommisionUpdate.IsCommision = true;
                            }
                            else
                            {
                                objCommisionUpdate.IsCommision = false;
                            }
                            objCommisionUpdate.Mode = Mode.Text;
                            objCommisionUpdate.LastModifiedBy = User.Identity.Name;
                            objCommisionUpdate.UpdateCommision(ref Message);
                        }
                        else
                        {
                            clsCommon.ShowMessage("Is Commision Should be 1 or 0");
                            return;
                        }

                    }
                    else
                    {
                        clsCommon.ShowMessage("The Mode Should be WEEKLY or MONTHLY or CUTANDPAY");
                        return;
                    }
                }
                else
                {
                    clsCommon.ShowMessage("CommisionPercentage Should be Numeric Only");
                    return;
                }
            }
            else
            {
                clsCommon.ShowMessage("CommisionPercentage Is Mandatory");
                return;
            }
                
            gvCommision.EditIndex = -1;
            BindGrid(hdnUser.Value);
        }

        protected void gvCommision_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCommision.PageIndex = e.NewPageIndex;
            BindGrid(hdnUser.Value);
        }
    }
}
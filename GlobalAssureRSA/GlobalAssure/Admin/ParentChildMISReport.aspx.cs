﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class ParentChildMISReport1 : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager currPageScriptManager = ScriptManager.GetCurrent(Page) as ScriptManager;
            if (currPageScriptManager != null)
            {
                currPageScriptManager.RegisterPostBackControl(btnDownload);
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            clsAdmin objParentChild = new clsAdmin();
            string strFileName = "ParentChildMIS";
            DataTable dtParentChild = objParentChild.GetParentChildMIS(ref Message);
            if(dtParentChild.Rows.Count > 0)
            {                
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dtParentChild, strFileName);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    //  Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    strFileName += DateTime.Now.ToString("yyyymmddHHmmss") + ".xlsx";
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("content-disposition", "attachment;filename=" + strFileName);
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }

            }             
        }
        
    }
}
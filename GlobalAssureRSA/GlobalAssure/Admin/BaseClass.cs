﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace GlobalAssure.Admin
{
    public class BaseClass : System.Web.UI.Page
    {
        public bool ValidateAdmin()
        {
            bool IsValidAdmin = false;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["UsrType"])))
            {
                if (Convert.ToString(Session["UsrType"]) == "Admin" || Convert.ToString(Session["UsrType"]) == "Operations")
                {
                    IsValidAdmin = true;
                }
                else
                {
                    IsValidAdmin = false;
                }
            }
            else
            {
                IsValidAdmin = false;
            }
            //if (Request.Cookies["UserType"] != null)
            //{
            //    UserType = Request.Cookies["UserType"].Value;
            //    if (UserType.ToUpper().Split('=')[1] == "ADMIN")
            //    {
            //        IsValidAdmin = true;
            //    }
            //}
            //if (Request.Cookies["UsrId"] != null)
            //{
            //    UserId = Request.Cookies["UsrId"].value;
            //}


            return IsValidAdmin;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="Admin.aspx.cs" Inherits="GlobalAssure.Admin._Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        tr td, tr th {
            padding: 0px !important;
            vertical-align: middle !important;
            font-size: 12px !important;
        }

        .btn {
            padding: 2px 4px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">


    <h5 class="main-title text-center">
        <span>Select User 
            <asp:DropDownList ID="ddlUserId" runat="server"></asp:DropDownList></span> <span style="padding-left: 50px;">Select Action 
                <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlAction_SelectedIndexChanged">
                    <asp:ListItem Value="0">---Select---</asp:ListItem>
                    <asp:ListItem Value="1">Plan Mapping</asp:ListItem>
                    <asp:ListItem Value="2">Certificate Cancellation</asp:ListItem>
                    <asp:ListItem Value="3">Payment Status Update</asp:ListItem>
                </asp:DropDownList></span> </h5>




    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="active-class" style="color: #fff"><strong>Admin Panel</strong></p>

        </div>
        <div class="width-0 panel-body" style="background-color: #fff">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="viewPlanMapping" runat="server">
                    <div style="margin: 5px 5px 3px 49px">
                        <div class="col-md-4">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>Select Plan</label>
                                </div>
                                <asp:DropDownList ID="ddlPlan" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group" style="padding-top: 20px">
                                <asp:Button ID="btnAddPlantouser" runat="server" Text="Add Plan" CssClass="btn btn-primary" OnClientClick="return ConfirmPlanAdd();" OnClick="btnAddPlantouser_Click" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group" style="padding-top: 20px">
                                <asp:Button ID="btnViewMappedPlan" runat="server" Text="View Mapped Plan"
                                    CssClass="btn btn-primary" OnClick="btnViewMappedPlan_Click" />
                            </div>
                        </div>
                    </div>
                    <div>

                        <asp:GridView ID="gvMappedPlan" CssClass="table mb-0 table-striped" CellSpacing="0"
                            Width="100%" runat="server" AutoGenerateColumns="false"
                            DataKeyNames="PrivilegeDetailID,Plans" OnRowCommand="gvMappedPlan_RowCommand" OnRowDataBound="gvMappedPlan_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="Plans" HeaderText="Plan Name"></asp:BoundField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <%--<asp:LinkButton ID="lnkbtnEdit" runat="server" class="btn btn-info" CommandName="sel"><i class="icon-edit icon-white"></i>Edit</asp:LinkButton>--%>
                                        <asp:LinkButton ID="lnkbtnDelete" runat="server" class="btn btn-info" CommandName="del"><i class="icon-edit icon-white"></i>Delete</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>

                </asp:View>

                <asp:View ID="viewCertificateCancellation" runat="server">
                    <div style="margin: 5px 5px 3px 49px">
                        <div class="col-md-4">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>Certificate No</label>
                                </div>
                                <asp:TextBox ID="txtCertificateNo" runat="server" CssClass="form-control"></asp:TextBox>

                            </div>
                        </div>

                        <div class="col-md-2">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group" style="padding-top: 20px">
                                <asp:Button ID="btnSearchCertificate" runat="server" Text="Search Certificate" CssClass="btn btn-primary" OnClick="btnSearchCertificate_Click" />
                            </div>
                        </div>

                    </div>


                    <asp:GridView ID="gvCerti" CssClass="table mb-0 table-striped table-responsive" CellSpacing="0"
                        Width="100%" runat="server" AutoGenerateColumns="false"
                        DataKeyNames="CertificateNo,TransactionID,StatusID" OnRowCommand="gvCerti_RowCommand" OnRowDataBound="gvCerti_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="CertificateNo" HeaderText="Certificate No"></asp:BoundField>
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer Name"></asp:BoundField>
                            <asp:BoundField DataField="CustomerEmail" HeaderText="Customer Email"></asp:BoundField>
                            <asp:BoundField DataField="CustomerContact" HeaderText="Customer Contact"></asp:BoundField>
                            <asp:BoundField DataField="RegistrationNo" HeaderText="Registration No"></asp:BoundField>
                            <asp:BoundField DataField="EngineNo" HeaderText="Engine No"></asp:BoundField>
                            <asp:BoundField DataField="ChassisNo" HeaderText="Chassis No"></asp:BoundField>
                            <asp:BoundField DataField="CoverStartDate" HeaderText="Cover Start Date" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:BoundField DataField="CoverEndDate" HeaderText="Cover End Date" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:BoundField DataField="IssueDate" HeaderText="Issue Date" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:BoundField DataField="StatusID" HeaderText="Status"></asp:BoundField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnCertDelete" runat="server" class="btn btn-info" CommandName="cancl"><i class="icon-edit icon-white"></i>Cancel</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </asp:View>

                <asp:View ID="viewPaymentStatusUpdate" runat="server">
                    <div style="margin: 5px 5px 3px 49px">
                        <div class="col-md-4">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>Certificate No</label>
                                </div>
                                <asp:TextBox ID="txtCerificateNoForPayment" runat="server" CssClass="form-control"></asp:TextBox>

                            </div>
                        </div>

                        <div class="col-md-2">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>From Date</label>
                                </div>                   
                                <asp:UpdatePanel ID="UpdatePanelCalendarFromDate" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>                                        

                                        <asp:Calendar ID="CalendarFromDate" runat="server"
                                            BackColor="#FFFFCC"
                                            BorderColor="#FFCC66"
                                            BorderWidth="1px"
                                            DayNameFormat="Shortest"
                                            Font-Names="Verdana"
                                            Font-Size="8pt"
                                            ForeColor="#663399"
                                            Height="200px"
                                            ShowGridLines="True"
                                            Visible="false"
                                            Width="220px" OnSelectionChanged="CalendarFromDate_SelectionChanged" >
                                            <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                                            <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                                            <OtherMonthDayStyle ForeColor="#CC9966" />
                                            <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                                            <SelectorStyle BackColor="#FFCC66" />
                                            <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
                                            <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                                        </asp:Calendar>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>&nbsp;</label>
                                </div>
                                <asp:ImageButton ID="ImageButtonCalendarFromDate" runat="server" style="height: 30px" ImageUrl="../Image/calendar.gif" OnClick="ImageButtonCalendarFromDate_Click" />
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>To Date</label>
                                </div>                   
                                <asp:UpdatePanel ID="UpdatePanelCalendarToDate" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>                                        

                                        <asp:Calendar ID="CalendarToDate" runat="server"
                                            BackColor="#FFFFCC"
                                            BorderColor="#FFCC66"
                                            BorderWidth="1px"
                                            DayNameFormat="Shortest"
                                            Font-Names="Verdana"
                                            Font-Size="8pt"
                                            ForeColor="#663399"
                                            Height="200px"
                                            ShowGridLines="True"
                                            Visible="false"
                                            Width="220px" OnSelectionChanged="CalendarToDate_SelectionChanged" >
                                            <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px" />
                                            <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                                            <OtherMonthDayStyle ForeColor="#CC9966" />
                                            <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                                            <SelectorStyle BackColor="#FFCC66" />
                                            <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
                                            <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                                        </asp:Calendar>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>&nbsp;</label>
                                </div>
                                <asp:ImageButton ID="ImageButtonCalendarToDate" runat="server" style="height: 30px" ImageUrl="../Image/calendar.gif" OnClick="ImageButtonCalendarToDate_Click" />
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group" style="padding-top: 20px">
                                <asp:Button ID="btnSearchCertificateforPayment" runat="server" Text="Search Certificate" CssClass="btn btn-primary" OnClick="btnSearchCertificateforPayment_Click" Style="height: 40px; width: 200px;" />
                            </div>
                        </div>

                        <div class="col-md-4" id="dvUpdateButton" runat="server">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group" style="padding-top: 20px">
                                <asp:Button ID="btnUpdatePaymentStatus" runat="server" Text="Update Payment Status" CssClass="btn btn-primary" OnClientClick="return ConfirmMsg('Are you sure to update the payment status');" OnClick="btnUpdatePaymentStatus_Click" Style="height: 40px; width: 200px;" />
                            </div>
                        </div>

                        <div class="col-md-4" id="dvUpdatellTextBox" runat="server">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group" style="padding-top: 20px">
                                <asp:TextBox runat="server" ID="txtUpdateAllRemark" CssClass="form-control" TextMode="MultiLine" />
                            </div>
                        </div>

                        <div class="col-md-4" id="dvUpdateAllButton" runat="server">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group" style="padding-top: 20px">
                                <asp:Button ID="btnUpdateAllPayment" runat="server" Text="Update All Payment Status" CssClass="btn btn-primary" OnClientClick="return ConfirmMsg('Are you sure to update the payment status');" OnClick="btnUpdateAllPayment_Click" Style="height: 40px; width: 200px;" />
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="col-md-4" style="margin-left: 50px; margin-bottom: 20px">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>Total Count</label>
                                </div>
                                <asp:TextBox ID="txtTotalCount" runat="server" CssClass="form-control" disabled="disabled"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-4" style="margin-left: -20px; margin-bottom: 20px">
                            <label><span>&nbsp;</span></label>
                            <div class="form-group">
                                <div>
                                    <label>Total Amount</label>
                                </div>
                                <asp:TextBox ID="txtTotalAmount" runat="server" CssClass="form-control" disabled="disabled"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <asp:GridView ID="gvStatusUpdate" CssClass="table mb-0 table-striped table-responsive" CellSpacing="0"
                        Width="100%" runat="server" AutoGenerateColumns="false"
                        DataKeyNames="CertificateNo,TransactionID,StatusID,Total">
                        <Columns>
                            <asp:TemplateField HeaderText="Action">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="CheckBox_Header" runat="server" Text="Action" OnCheckedChanged="CheckBox_Header_CheckedChanged" AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPayment" runat="server" OnCheckedChanged="chkPayment_CheckedChanged" AutoPostBack="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Payment Remarks">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPayRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CertificateNo" HeaderText="Certificate No"></asp:BoundField>
                            <asp:BoundField DataField="Total" HeaderText="Amount"></asp:BoundField>
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer Name"></asp:BoundField>
                            <asp:BoundField DataField="CustomerEmail" HeaderText="Customer Email"></asp:BoundField>
                            <asp:BoundField DataField="CustomerContact" HeaderText="Customer Contact"></asp:BoundField>
                            <asp:BoundField DataField="RegistrationNo" HeaderText="Registration No"></asp:BoundField>
                            <asp:BoundField DataField="EngineNo" HeaderText="Engine No"></asp:BoundField>
                            <asp:BoundField DataField="ChassisNo" HeaderText="Chassis No"></asp:BoundField>
                            <asp:BoundField DataField="CoverStartDate" HeaderText="Cover Start Date" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:BoundField DataField="CoverEndDate" HeaderText="Cover End Date" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:BoundField DataField="IssueDate" HeaderText="Issue Date" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:BoundField DataField="StatusID" HeaderText="Status"></asp:BoundField>
                        </Columns>
                    </asp:GridView>

                    <asp:HiddenField ID="hdn_FromDate" runat="server" />
                    <asp:HiddenField ID="hdn_ToDate" runat="server" />
                </asp:View>

            </asp:MultiView>

        </div>


    </div>

    <script type="text/javascript">  

        function ConfirmPlanAdd() {
            if (document.getElementById("cpMain_ddlUserId").selectedIndex > 0 && document.getElementById("cpMain_ddlPlan").selectedIndex > 0) {
                if (confirm("Are you sure to add/update the plan")) {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                alert('Please Select User id and Plan Name')
                return false;
            }
        }
        function ConfirmMsg(Msg) {
            if (confirm(Msg)) {
                return true;
            } else {
                return false;
            }
        }
    </script>
</asp:Content>

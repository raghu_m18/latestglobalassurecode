﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class PlanMaster : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager currPageScriptManager = ScriptManager.GetCurrent(Page) as ScriptManager;
                if (currPageScriptManager != null)
                {
                    currPageScriptManager.RegisterPostBackControl(btnDownload);
                }
                if (!IsPostBack)
                {
                    BindDDl();
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }
           
        }

        protected void gvPlan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvPlan.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }
        }

        protected void gvPlan_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvPlan_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
       
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                string strFileName = "PlanMIS";
                DataTable dtMakeModel = (DataTable)(Session["Plans"]);

                if (dtMakeModel.Rows.Count > 0)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dtMakeModel, strFileName);
                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        //  Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        strFileName += DateTime.Now.ToString("yyyymmddHHmmss") + ".xlsx";
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName);
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }
            
        }

        protected void DDLPlan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }
           
        }

        public void BindDDl()
        {
            clsAdmin objDDlPlan = new clsAdmin();
            DataTable dtDDLPlan = objDDlPlan.GetPlanDDl(ref Message);
            if (dtDDLPlan.Rows.Count > 0)
            {
                DDLPlan.DataSource = dtDDLPlan;
                DDLPlan.DataTextField = dtDDLPlan.Columns[0].ColumnName;
                DDLPlan.DataValueField = dtDDLPlan.Columns[1].ColumnName;
                DDLPlan.DataBind();
                DDLPlan.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                DDLPlan.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        public void BindGrid()
        {
            clsAdmin objPlan = new clsAdmin();
            objPlan.Plan = DDLPlan.SelectedItem.Text;
            DataTable dtPlan = objPlan.GetPlanDetails(ref Message);
            if (dtPlan.Rows.Count > 0)
            {
                gvPlan.DataSource = dtPlan;
                gvPlan.DataBind();
                Session["Plans"] = dtPlan;
            }
            else
            {
                gvPlan.DataSource = null;
                gvPlan.DataBind();
                Session["Plans"] = null;
            }

        }


    }
}
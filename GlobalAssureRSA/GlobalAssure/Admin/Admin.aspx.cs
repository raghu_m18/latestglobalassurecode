﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using GlobalAssure.Entity;
using System.Configuration;

namespace GlobalAssure.Admin
{
    public partial class _Admin : BaseClass
    {
        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();
        clsAdmin objAdmin = new clsAdmin();
        string strMsg = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ValidateAdmin() == true)
            {
                if (!IsPostBack)
                {
                    BindUserId();
                    dvUpdateAllButton.Visible = false;
                    dvUpdatellTextBox.Visible = false;
                }
            }
            else
            {
                Response.Redirect("~/Account/Login");

            }
        }

        protected void ddlAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAction.SelectedIndex > 0)
            {
                if (ddlUserId.SelectedIndex == 0)
                {
                    clsCommon.ShowMessage("Please select user");
                    return;
                }
                MultiView1.ActiveViewIndex = Convert.ToInt32(ddlAction.SelectedItem.Value) - 1;
                if (Convert.ToInt32(ddlAction.SelectedItem.Value) == 1)//plan Mapping
                {
                    BindPlan();
                }
            }
           
        }

        void BindUserId()
        {
            DataTable dtCusr = objAdmin.GetActiveUserId();
            ddlUserId.DataSource = dtCusr;
            ddlUserId.DataTextField = dtCusr.Columns[0].ColumnName;
            ddlUserId.DataValueField = dtCusr.Columns[1].ColumnName;
            ddlUserId.DataBind();
            ddlUserId.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        void BindPlan()
        {
            DataTable dtCusr = objAdmin.GetActivePlan();
            ddlPlan.DataSource = dtCusr;
            ddlPlan.DataTextField = dtCusr.Columns[0].ColumnName;
            ddlPlan.DataValueField = dtCusr.Columns[1].ColumnName;
            ddlPlan.DataBind();
            ddlPlan.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        protected void btnAddPlantouser_Click(object sender, EventArgs e)
        {
            string UserID = string.Empty;
            string PlanName = string.Empty;
            string CreatedBy = string.Empty;

            try
            {
                string UserEmail = Convert.ToString(Session["UsrId"]);
                AspNetUser userdata = db.AspNetUsers.Where(w => w.Email == UserEmail).FirstOrDefault();
                if (userdata != null)
                {
                    CreatedBy = userdata.Id;
                }

                if (btnAddPlantouser.Text == "Add Plan")
                {
                    UserID = ddlUserId.SelectedItem.Value;
                    PlanName = ddlPlan.SelectedItem.Text;
                    bool Added = objAdmin.AddPlantoUser(UserID, PlanName, CreatedBy, ref strMsg);
                    if (Added)
                    {
                        BindAddPlanGrid();
                        clsCommon.ShowMessage("Plan Added Successfully.");               
                    }
                    else
                    {
                        string Msg = ("Plan: " + PlanName + " already exist with this user").ToLower();
                        string ErrMsg = strMsg.TrimEnd().ToLower();
                        if (ErrMsg.Equals(Msg))
                        {
                            int tblUserPrivilegeDetailsIsvalidTCount = db.tblUserPrivilegeDetails.Where(w => w.UserID == UserID && w.Plans == PlanName && w.IsValid == true).Count();
                            if (tblUserPrivilegeDetailsIsvalidTCount > 0)
                            {
                                strMsg = "Plan: '" + PlanName + "' already exist with this user";
                                clsCommon.ShowMessage(strMsg);
                            }
                            else
                            {
                                int tblUserPrivilegeDetailsIsvalidFCount = db.tblUserPrivilegeDetails.Where(w => w.UserID == UserID && w.Plans == PlanName && w.IsValid == false).Count();
                                if (tblUserPrivilegeDetailsIsvalidFCount > 0)
                                {
                                    tblUserPrivilegeDetail tblUserPrivilegeDetailObj = new tblUserPrivilegeDetail();
                                    tblUserPrivilegeDetailObj.UserID = UserID;
                                    tblUserPrivilegeDetailObj.Product = "ALL";
                                    tblUserPrivilegeDetailObj.Payment = "ALL";
                                    tblUserPrivilegeDetailObj.Plans = PlanName;
                                    tblUserPrivilegeDetailObj.Make = "ALL";
                                    tblUserPrivilegeDetailObj.Model = "ALL";
                                    tblUserPrivilegeDetailObj.CreatedDate = DateTime.Now;
                                    tblUserPrivilegeDetailObj.IsValid = true;
                                    tblUserPrivilegeDetailObj.CreatedBy = CreatedBy;
                                    var result = db.tblUserPrivilegeDetails.Add(tblUserPrivilegeDetailObj);
                                    db.SaveChanges();

                                    decimal PrivilegeDetailID = result.PrivilegeDetailID;
                                    if (PrivilegeDetailID > 0)
                                    {
                                        List<tblUserPrivilegeDetail> lstUPD = db.tblUserPrivilegeDetails.Where(w => w.UserID == UserID && w.Plans == PlanName && w.IsValid == false && w.IsOnHold == 1).ToList();
                                        if (lstUPD.Count > 0)
                                        {
                                            foreach (var privdataisonhold1 in lstUPD)
                                            {
                                                tblUserPrivilegeDetail tbluserprivIsOnHold1Obj = db.tblUserPrivilegeDetails.Where(w => w.PrivilegeDetailID == privdataisonhold1.PrivilegeDetailID).FirstOrDefault();
                                                if (tbluserprivIsOnHold1Obj != null)
                                                {
                                                    tbluserprivIsOnHold1Obj.IsOnHold = 0;
                                                    tbluserprivIsOnHold1Obj.LastModifyDateTime = DateTime.Now;
                                                    db.SaveChanges();
                                                }
                                            }
                                        }

                                        BindAddPlanGrid();
                                        clsCommon.ShowMessage("Plan Added Successfully.");
                                    }
                                    else
                                    {
                                        clsCommon.ShowMessage("An Error Occured: Plan Not Added, Please Try Again");
                                    }                                   
                                }
                            }
                        }
                        else
                        {
                            clsCommon.ShowMessage(strMsg);
                        }                      
                    }
                }
                if (btnAddPlantouser.Text == "Update")
                {
                    bool Added = objAdmin.UpdatePlantoUser(Convert.ToInt32(ViewState["PlanId"]), ddlPlan.SelectedItem.Text, ref strMsg);
                    if (Added)
                    {
                        clsCommon.ShowMessage("Plan Added Succseesfully.");
                        btnAddPlantouser.Text = "Add Plan";
                        BindAddPlanGrid();
                    }
                    else
                    {
                        clsCommon.ShowMessage(strMsg);

                    }
                }

            }
            catch (Exception ex)
            {

                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }

        }
        protected void btnViewMappedPlan_Click(object sender, EventArgs e)
        {
            BindAddPlanGrid();

        }
        protected void gvMappedPlan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string UserID = string.Empty;
            
            try
            {
                string UserEmail = Convert.ToString(Session["UsrId"]);
                AspNetUser userdata = db.AspNetUsers.Where(w => w.Email == UserEmail).FirstOrDefault();
                if (userdata != null)
                {
                    UserID = userdata.Id;
                }                

                if (e.CommandName.ToLower() == "sel")
                {
                    GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    int index = row.RowIndex;
                    ddlPlan.SelectedItem.Text = Convert.ToString(gvMappedPlan.DataKeys[index]["Plans"]);
                    btnAddPlantouser.Text = "Update";
                    ViewState["PlanId"]=Convert.ToString(gvMappedPlan.DataKeys[index]["PrivilegeDetailID"]);
                }
                if (e.CommandName.ToLower() == "del")
                {
                    GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    int index = row.RowIndex;
                    int PrivilegeDetailID = Convert.ToInt32(gvMappedPlan.DataKeys[index]["PrivilegeDetailID"]);                              
                    if (objAdmin.DeletePlanFromUser(PrivilegeDetailID, UserID, ref strMsg))
                    {
                        clsCommon.ShowMessage("Plan Deleted Succseesfully.");
                        BindAddPlanGrid();
                    }
                    else
                    {
                        clsCommon.ShowMessage(strMsg);

                    }
                }

            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }
        protected void gvMappedPlan_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                DataRowView drview = e.Row.DataItem as DataRowView;
                LinkButton QC = (e.Row.FindControl("lnkbtnDelete") as LinkButton);
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    QC.OnClientClick = "return ConfirmMsg('Are you sure to delete this plan');";
                }

            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }

     


        void BindAddPlanGrid()
        {
            try
            {
                if (ddlUserId.SelectedIndex > 0)
                {
                    DataTable dtMappedPlan = objAdmin.GetPlanByUserId(ddlUserId.SelectedItem.Value);
                    if (dtMappedPlan != null && dtMappedPlan.Rows.Count > 0)
                    {
                        gvMappedPlan.DataSource = dtMappedPlan;
                        gvMappedPlan.DataBind();
                    }
                    else
                    {
                        gvMappedPlan.DataSource = null;
                        gvMappedPlan.DataBind();
                        clsCommon.ShowMessage("No Data Found");
                    }
                }
            }
            catch (Exception ex)
            {

                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }

        void BindCertificateGrid()
        {
            try
            {
                if (ddlUserId.SelectedIndex > 0)
                {
                    DataTable dtMappedPlan = objAdmin.GetCertificateByUserId(ddlUserId.SelectedItem.Value, txtCertificateNo.Text);
                    if (dtMappedPlan != null && dtMappedPlan.Rows.Count > 0)
                    {
                        gvCerti.DataSource = dtMappedPlan;
                        gvCerti.DataBind();
                    }
                    else
                    {
                        gvCerti.DataSource = null;
                        gvCerti.DataBind();
                        clsCommon.ShowMessage("No Data Found");
                    }
                }
            }
            catch (Exception ex)
            {

                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }

        void BindPaymentGrid()
        {
            try
            {
                if (ddlUserId.SelectedIndex > 0)
                {
                    DataTable dtMappedPlan = objAdmin.GetCertificateforPaymentByUserId(ddlUserId.SelectedItem.Value, txtCerificateNoForPayment.Text, hdn_FromDate.Value, hdn_ToDate.Value);
                    if (dtMappedPlan != null && dtMappedPlan.Rows.Count > 0)
                    {
                        gvStatusUpdate.DataSource = dtMappedPlan;
                        gvStatusUpdate.DataBind();
                    }
                    else
                    {
                        gvStatusUpdate.DataSource = null;
                        gvStatusUpdate.DataBind();
                        clsCommon.ShowMessage("No Data Found");
                    }
                }
            }
            catch (Exception ex)
            {

                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }

        protected void btnSearchCertificate_Click(object sender, EventArgs e)
        {
            BindCertificateGrid();
           
        }

        protected void gvCerti_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToLower() == "cancl")
                {
                    GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    int index = row.RowIndex;
                    int TransId = Convert.ToInt32(gvCerti.DataKeys[index]["TransactionID"]);
                    string  CertiNo = Convert.ToString(gvCerti.DataKeys[index]["CertificateNo"]);
                    if (objAdmin.CancelCertificate(TransId, CertiNo, ref strMsg))
                    {
                        clsCommon.ShowMessage("Certificate Cancelled Succseesfully.");
                        BindCertificateGrid();
                    }
                    else
                    {
                        clsCommon.ShowMessage(strMsg);

                    }
                }

            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }

        protected void gvCerti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                DataRowView drview = e.Row.DataItem as DataRowView;
                LinkButton QC = (e.Row.FindControl("lnkbtnCertDelete") as LinkButton);
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (txtCertificateNo.Text.Trim() == "")
                    {

                        QC.Visible = false;
                    }
                    else
                    {
                        if (Convert.ToInt32(gvCerti.DataKeys[e.Row.RowIndex]["StatusID"]) == 14)
                        {
                            QC.Enabled = false;
                        }
                        else
                        {

                            QC.OnClientClick = "return ConfirmMsg('Are you sure to cancel the certificate');";
                        }
                    }


                }

            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }


        protected void btnSearchCertificateforPayment_Click(object sender, EventArgs e)
        {
            dvUpdateAllButton.Visible = false;
            dvUpdatellTextBox.Visible = false;
            dvUpdateButton.Visible = true;
            txtTotalCount.Text = "";
            txtTotalAmount.Text = "";

            BindPaymentGrid();
        }

        protected void btnUpdatePaymentStatus_Click(object sender, EventArgs e)
        {
            tblUserPrivilegeDetail tblUserPrivilegeDetail = new tblUserPrivilegeDetail();

            try
            {
                decimal TotalFinalAmount = 0;
                decimal FinalAmount = 0;
                int CheckedCount = 0;                             

                foreach (GridViewRow grow in gvStatusUpdate.Rows)
                {
                    CheckBox chkboxPayment = grow.FindControl("chkPayment") as CheckBox;
                    TextBox txtPaymentRemarks = grow.FindControl("txtPayRemarks") as TextBox;

                    if (chkboxPayment.Checked)
                    {
                        CheckedCount += 1;

                        if (txtPaymentRemarks.Text.Trim() == "")
                        {
                            clsCommon.ShowMessage("Payment Remark is Mandatory");
                            return;
                        }
                    }                 
                }

                if (CheckedCount == 0)
                {
                    clsCommon.ShowMessage("Please Check Atleast One row !");
                    return;
                }                

                foreach (GridViewRow grow in gvStatusUpdate.Rows)
                {
                    CheckBox chkboxPayment = grow.FindControl("chkPayment") as CheckBox;
                    TextBox txtPaymentRemarks = grow.FindControl("txtPayRemarks") as TextBox;

                    if (chkboxPayment.Checked)
                    {
                        #region Commission Percentage Calculation For MIS Logic Part

                        decimal TransactionID = Convert.ToDecimal(gvStatusUpdate.DataKeys[grow.RowIndex]["TransactionID"]);

                        var data = (from t in db.tblTransactions.AsNoTracking()
                                    join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id into userbased
                                    from userbasedobj in userbased.DefaultIfEmpty()
                                    join plans in db.tblMstPlans.AsNoTracking() on t.PlanID equals plans.PlanID into plansbased
                                    from plansbasedobj in plansbased.DefaultIfEmpty()
                                    where t.TransactionID == TransactionID
                                    select new
                                    {
                                        UserId = t.UserID,
                                        IsGSTNoFound = (!string.IsNullOrEmpty(userbasedobj.GSTno)) ? true : false,
                                        PlanName = plansbasedobj.Name,
                                        PlanTotalAmount = plansbasedobj.Total,
                                        Amount = plansbasedobj.Amount

                                    }).FirstOrDefault();

                        if (data != null)
                        {
                            decimal GST = Convert.ToDecimal(ConfigurationManager.AppSettings["GSTPer"]);
                            decimal TDS = Convert.ToDecimal(ConfigurationManager.AppSettings["TDSPer"]);
                            decimal TotalPlanAmount = 0;
                            decimal PlanBaseAmount = 0;
                            decimal TotalCommissionAmount = 0;
                            decimal TotalCommissionAmountForMIS = 0;
                            decimal CommissionPercentage = 0;
                            bool? IsCommsn = null;
                            string Mode = string.Empty;
                            tblUserPrivilegeDetail = db.tblUserPrivilegeDetails.Where(w => w.UserID == data.UserId && w.Plans == data.PlanName && (w.IsValid != false || w.IsOnHold == 1)).FirstOrDefault();
                            if (tblUserPrivilegeDetail != null)
                            {
                                // TotalAmount Calculation For Payment START
                                if (tblUserPrivilegeDetail.IsCommision != null)
                                {
                                    bool IsCommision = Convert.ToBoolean(tblUserPrivilegeDetail.IsCommision);
                                    if (IsCommision.Equals(true))
                                    {
                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmount = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmount * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmount * GST) / 100;

                                            TotalCommissionAmount = (TotalCommissionAmount - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmount;
                                        }
                                        else
                                        {
                                            TotalCommissionAmount = TotalCommissionAmount - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }

                                        FinalAmount = TotalPlanAmount;
                                    }
                                }
                                else
                                {
                                    bool IsCommision = false;
                                    if (IsCommision.Equals(false))
                                    {
                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }

                                        FinalAmount = TotalPlanAmount;
                                    }
                                }

                                TotalFinalAmount += FinalAmount;
                                // TotalAmount Calculation For Payment END

                                //---------------------------------------------//

                                Mode = tblUserPrivilegeDetail.Mode;

                                // TotalAmount Calculation For CommisionReportMIS START
                                if (tblUserPrivilegeDetail.IsCommision != null)
                                {
                                    bool IsCommision = Convert.ToBoolean(tblUserPrivilegeDetail.IsCommision);
                                    if (IsCommision.Equals(true))
                                    {
                                        IsCommsn = true;

                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                            TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                        else
                                        {
                                            TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                    }
                                    else
                                    {
                                        IsCommsn = false;

                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                            TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                        else
                                        {
                                            TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                    }
                                }
                                else
                                {
                                    IsCommsn = null;

                                    if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                    {
                                        CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                    }
                                    else
                                    {
                                        CommissionPercentage = 0;
                                    }

                                    if (data.PlanTotalAmount > 0)
                                    {
                                        TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                    }
                                    else
                                    {
                                        TotalPlanAmount = 0;
                                    }


                                    if (data.Amount > 0)
                                    {
                                        PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                    }
                                    else
                                    {
                                        PlanBaseAmount = 0;
                                    }

                                    TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                    decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                    if (data.IsGSTNoFound == true)
                                    {
                                        decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                        TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                        FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                    }
                                    else
                                    {
                                        TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                        FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                    }
                                }

                                tblTransaction transObj = db.tblTransactions.Where(w => w.TransactionID == TransactionID).FirstOrDefault();
                                if (transObj != null)
                                {
                                    transObj.CommisionPercentage = CommissionPercentage;
                                    transObj.CommisionAmount = TotalCommissionAmountForMIS;
                                    transObj.IsCommision = IsCommsn;
                                    transObj.Mode = Mode;
                                    db.SaveChanges();
                                }
                                // TotalAmount Calculation For CommisionReportMIS END
                            }
                        }
                        #endregion

                        #region Payment Status Update Logic Part

                        objAdmin.UpdatePaymentStatus(Convert.ToInt32(gvStatusUpdate.DataKeys[grow.RowIndex]["TransactionID"]),
                           Convert.ToString(gvStatusUpdate.DataKeys[grow.RowIndex]["CertificateNo"]),
                           Convert.ToDouble(gvStatusUpdate.DataKeys[grow.RowIndex]["Total"]),
                           Convert.ToString(txtPaymentRemarks.Text),
                           ddlUserId.SelectedItem.Value,
                           ref strMsg
                           );
                        #endregion
                    }
                }
                clsCommon.ShowMessage("Payment status updated successfully");
                BindPaymentGrid();
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }

        protected void CheckBox_Header_CheckedChanged(object sender, EventArgs e)
        {
            int CheckCount = 0;
            decimal TotalAmount = 0;

            CheckBox ChkBoxHeader = (CheckBox)gvStatusUpdate.HeaderRow.FindControl("CheckBox_Header");
            foreach (GridViewRow row in gvStatusUpdate.Rows)
            {                
                CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkPayment");
                TextBox txtPaymentRemarks = (TextBox)row.FindControl("txtPayRemarks");
                if (ChkBoxHeader.Checked == true)
                {
                    CheckCount += 1;
                    TotalAmount += Convert.ToDecimal(gvStatusUpdate.DataKeys[row.RowIndex]["Total"]);

                    ChkBoxRows.Checked = true;
                    dvUpdateAllButton.Visible = true;
                    dvUpdatellTextBox.Visible = true;
                    dvUpdateButton.Visible = false;
                    txtPaymentRemarks.Attributes.Add("disabled","true");
                    txtPaymentRemarks.Style.Add("cursor","no-drop");
                    txtPaymentRemarks.Text = "";
                    txtUpdateAllRemark.Text = "";
                }
                else
                {
                    ChkBoxRows.Checked = false;
                    dvUpdateAllButton.Visible = false;
                    dvUpdatellTextBox.Visible = false;
                    dvUpdateButton.Visible = true;
                    txtPaymentRemarks.Attributes.Remove("disabled");
                    txtPaymentRemarks.Style.Remove("cursor");
                    txtPaymentRemarks.Text = "";
                    txtUpdateAllRemark.Text = "";
                }
            }

            txtTotalCount.Text = Convert.ToString(CheckCount);
            txtTotalAmount.Text = Convert.ToString(TotalAmount);
        }

        protected void btnUpdateAllPayment_Click(object sender, EventArgs e)
        {
            tblUserPrivilegeDetail tblUserPrivilegeDetail = new tblUserPrivilegeDetail();

            try
            {
                decimal TotalFinalAmount = 0;
                decimal FinalAmount = 0;
                int CheckedCount = 0;

                foreach (GridViewRow grow in gvStatusUpdate.Rows)
                {
                    CheckBox chkboxPayment = grow.FindControl("chkPayment") as CheckBox;

                    if (chkboxPayment.Checked)
                    {
                        CheckedCount += 1;
                    }
                }

                if (CheckedCount > 0)
                {
                    if (txtUpdateAllRemark.Text.Trim() == "")
                    {
                        clsCommon.ShowMessage("Payment Remark is Mandatory");
                        return;
                    }
                }  
                else
                {
                    clsCommon.ShowMessage("Please Check Atleast One row !");
                    return;
                }

                foreach (GridViewRow grow in gvStatusUpdate.Rows)
                {
                    CheckBox chkboxPayment = grow.FindControl("chkPayment") as CheckBox;
                    string txtPaymentRemark = txtUpdateAllRemark.Text;

                    if (chkboxPayment.Checked)
                    {
                        #region Commission Percentage Calculation For MIS Logic Part

                        decimal TransactionID = Convert.ToDecimal(gvStatusUpdate.DataKeys[grow.RowIndex]["TransactionID"]);

                        var data = (from t in db.tblTransactions.AsNoTracking()
                                    join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id into userbased
                                    from userbasedobj in userbased.DefaultIfEmpty()
                                    join plans in db.tblMstPlans.AsNoTracking() on t.PlanID equals plans.PlanID into plansbased
                                    from plansbasedobj in plansbased.DefaultIfEmpty()
                                    where t.TransactionID == TransactionID
                                    select new
                                    {
                                        UserId = t.UserID,
                                        IsGSTNoFound = (!string.IsNullOrEmpty(userbasedobj.GSTno)) ? true : false,
                                        PlanName = plansbasedobj.Name,
                                        PlanTotalAmount = plansbasedobj.Total,
                                        Amount = plansbasedobj.Amount

                                    }).FirstOrDefault();

                        if (data != null)
                        {
                            decimal GST = Convert.ToDecimal(ConfigurationManager.AppSettings["GSTPer"]);
                            decimal TDS = Convert.ToDecimal(ConfigurationManager.AppSettings["TDSPer"]);
                            decimal TotalPlanAmount = 0;
                            decimal PlanBaseAmount = 0;
                            decimal TotalCommissionAmount = 0;
                            decimal TotalCommissionAmountForMIS = 0;
                            decimal CommissionPercentage = 0;
                            bool? IsCommsn = null;
                            string Mode = string.Empty;
                            tblUserPrivilegeDetail = db.tblUserPrivilegeDetails.Where(w => w.UserID == data.UserId && w.Plans == data.PlanName && (w.IsValid != false || w.IsOnHold == 1)).FirstOrDefault();
                            if (tblUserPrivilegeDetail != null)
                            {
                                // TotalAmount Calculation For Payment START
                                if (tblUserPrivilegeDetail.IsCommision != null)
                                {
                                    bool IsCommision = Convert.ToBoolean(tblUserPrivilegeDetail.IsCommision);
                                    if (IsCommision.Equals(true))
                                    {
                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmount = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmount * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmount * GST) / 100;

                                            TotalCommissionAmount = (TotalCommissionAmount - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmount;
                                        }
                                        else
                                        {
                                            TotalCommissionAmount = TotalCommissionAmount - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }

                                        FinalAmount = TotalPlanAmount;
                                    }
                                }
                                else
                                {
                                    bool IsCommision = false;
                                    if (IsCommision.Equals(false))
                                    {
                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }

                                        FinalAmount = TotalPlanAmount;
                                    }
                                }

                                TotalFinalAmount += FinalAmount;
                                // TotalAmount Calculation For Payment END

                                //---------------------------------------------//

                                Mode = tblUserPrivilegeDetail.Mode;

                                // TotalAmount Calculation For CommisionReportMIS START
                                if (tblUserPrivilegeDetail.IsCommision != null)
                                {
                                    bool IsCommision = Convert.ToBoolean(tblUserPrivilegeDetail.IsCommision);
                                    if (IsCommision.Equals(true))
                                    {
                                        IsCommsn = true;

                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                            TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                        else
                                        {
                                            TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                    }
                                    else
                                    {
                                        IsCommsn = false;

                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                            TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                        else
                                        {
                                            TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                    }
                                }
                                else
                                {
                                    IsCommsn = null;

                                    if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                    {
                                        CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                    }
                                    else
                                    {
                                        CommissionPercentage = 0;
                                    }

                                    if (data.PlanTotalAmount > 0)
                                    {
                                        TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                    }
                                    else
                                    {
                                        TotalPlanAmount = 0;
                                    }


                                    if (data.Amount > 0)
                                    {
                                        PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                    }
                                    else
                                    {
                                        PlanBaseAmount = 0;
                                    }

                                    TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                    decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                    if (data.IsGSTNoFound == true)
                                    {
                                        decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                        TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                        FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                    }
                                    else
                                    {
                                        TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                        FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                    }
                                }

                                tblTransaction transObj = db.tblTransactions.Where(w => w.TransactionID == TransactionID).FirstOrDefault();
                                if (transObj != null)
                                {
                                    transObj.CommisionPercentage = CommissionPercentage;
                                    transObj.CommisionAmount = TotalCommissionAmountForMIS;
                                    transObj.IsCommision = IsCommsn;
                                    transObj.Mode = Mode;
                                    db.SaveChanges();
                                }
                                // TotalAmount Calculation For CommisionReportMIS END
                            }
                        }
                        #endregion

                        #region Payment Status Update Logic Part

                        objAdmin.UpdatePaymentStatus(Convert.ToInt32(gvStatusUpdate.DataKeys[grow.RowIndex]["TransactionID"]),
                           Convert.ToString(gvStatusUpdate.DataKeys[grow.RowIndex]["CertificateNo"]),
                           Convert.ToDouble(gvStatusUpdate.DataKeys[grow.RowIndex]["Total"]),
                           Convert.ToString(txtPaymentRemark),
                           ddlUserId.SelectedItem.Value,
                           ref strMsg
                           );
                        #endregion
                    }
                }
                clsCommon.ShowMessage("Payment status updated successfully");
                BindPaymentGrid();
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }

        protected void chkPayment_CheckedChanged(object sender, EventArgs e)
        {
            int CheckCount = 0;
            decimal TotalAmount = 0;

            foreach (GridViewRow row in gvStatusUpdate.Rows)
            {
                CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkPayment");
                if (ChkBoxRows.Checked == true)
                {
                    CheckCount += 1;
                    TotalAmount += Convert.ToDecimal(gvStatusUpdate.DataKeys[row.RowIndex]["Total"]);
                }               
            }

            txtTotalCount.Text = Convert.ToString(CheckCount);
            txtTotalAmount.Text = Convert.ToString(TotalAmount);
        }

        protected void ImageButtonCalendarFromDate_Click(object sender, ImageClickEventArgs e)
        {
            if (CalendarFromDate.Visible == true)
            {
                CalendarFromDate.Visible = false;
            }
            else
            {
                CalendarFromDate.Visible = true;
            }            
        }

        protected void CalendarFromDate_SelectionChanged(object sender, EventArgs e)
        {
            DateTime dtFromDate = CalendarFromDate.SelectedDate;
            txtFromDate.Text = dtFromDate.ToString("dd/MM/yyyy");

            string strFromDate = dtFromDate.ToString("yyyy-MM-dd") + " " + "00:00:00.000";
            hdn_FromDate.Value = strFromDate;

            CalendarFromDate.Visible = false;
        }

        protected void ImageButtonCalendarToDate_Click(object sender, ImageClickEventArgs e)
        {
            if (CalendarToDate.Visible == true)
            {
                CalendarToDate.Visible = false;
            }
            else
            {
                CalendarToDate.Visible = true;
            }
        }

        protected void CalendarToDate_SelectionChanged(object sender, EventArgs e)
        {
            DateTime dtToDate = CalendarToDate.SelectedDate;
            txtToDate.Text = dtToDate.ToString("dd/MM/yyyy");

            string strToDate = dtToDate.ToString("yyyy-MM-dd") + " " + "23:59:59.997";
            hdn_ToDate.Value = strToDate;

            CalendarToDate.Visible = false;
        }
    }
}
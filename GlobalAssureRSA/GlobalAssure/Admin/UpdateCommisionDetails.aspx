﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="UpdateCommisionDetails.aspx.cs" Inherits="GlobalAssure.Admin.UpdateCommisionDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cpMain" runat="server">
    <div class="col-lg-12" style="margin-top: 30px">
        <asp:TextBox runat="server" ID="txtUser" CssClass="form-control" placeholder="Enter Username" />
        <asp:HiddenField runat="server" id="hdnUser"/>   
    </div>
    <div class="col-lg-12" style="margin-top: 20px">
        <asp:Button Text="Search" ID="btnSearch" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnSearch_Click" />
        <asp:Button Text="Refresh" ID="btnRefersh" CssClass="btn btn-sm btn-primary" runat="server" OnClick="btnRefersh_Click" />
    </div>
    <div class="row align-items-center mb-30 pt-30" style="padding-top: 80px;">
        <div class="col-lg-12" id="dvCommision" runat="server">
            <%--<div style="height: 500px; overflow: scroll">--%>
                <div class="bg-white table-responsive rounded shadow-sm mb-30" style="width: 100%">
                    <asp:GridView ID="gvCommision" CssClass="table mb-0 table-striped" CellSpacing="0" Width="100%" runat="server"
                        AutoGenerateColumns="false" OnPageIndexChanging="gvCommision_PageIndexChanging"
                          OnRowDataBound="gvCommision_RowDataBound" OnRowCancelingEdit="gvCommision_RowCancelingEdit" OnRowUpdating="gvCommision_RowUpdating" AllowPaging="true"                                 
                        DataKeyNames="PrivilegeDetailID,Email,Plans,CommisionPercentage,IsCommision,Mode,IsValid" OnRowCommand="gvCommision_RowCommand" AutoGenerateEditButton="True" OnRowEditing="gvCommision_RowEditing">
                        <Columns>                           
                            <asp:TemplateField HeaderText="PrivilageID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrivilageID" runat="server" Text='<%#Eval("PrivilegeDetailID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Plans">
                                <ItemTemplate>
                                    <asp:Label ID="lblPlans" runat="server" Text='<%#Eval("Plans") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Commision Percentage">
                                <ItemTemplate>
                                    <asp:Label ID="lblCommision" runat="server" Text='<%#Eval("CommisionPercentage") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCommisionPercentage" runat="server" Text='<%#Eval("CommisionPercentage") %>' onkeypress="javascript:return isNumber(event)"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>       
                            <asp:TemplateField HeaderText="Is Commision">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsCommision" runat="server" Text='<%#Eval("IsCommision") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtIsCommision" runat="server" Placeholder="Enter 1 or 0"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>   
                           <%-- <asp:BoundField DataField="IsCommision" HeaderText="Is Commision" />--%>
                            <asp:TemplateField HeaderText="Mode">
                                <ItemTemplate>
                                    <asp:Label ID="lblMode" runat="server" Text='<%#Eval("Mode") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMode" runat="server" Text='<%#Eval("Mode") %>' onkeyup="uppercase(this.id);"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>                             
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsValid" runat="server" Text='<%#Eval("IsValid") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            <%--</div>--%>
            <%--<asp:Button Text="Update" ID="btnUpdate" CssClass="btn btn-normal btn-primary" runat="server" OnClick="btnUpdate_Click" />--%>
        </div>
    </div>
    <script src="../Scripts/jquery-1.10.2.js"></script>
    <script>       

        function uppercase(id) {
            var txt = document.getElementById(id);
            txt.value = txt.value.toUpperCase();
        }

        function isNumber(evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
                return false;
            }
            else {
                return true;
            }   
        }
    </script>
</asp:Content>

﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class MakeModelMaster : System.Web.UI.Page
    {
        string Message = "";
        int count = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager currPageScriptManager = ScriptManager.GetCurrent(Page) as ScriptManager;
                if (currPageScriptManager != null)
                {
                    currPageScriptManager.RegisterPostBackControl(btnDownload);
                }
                if (!IsPostBack)
                {
                    try
                    {
                        BindDDlMake();
                        BindDDlModel();
                        BindGrid();
                    }
                    catch (Exception ex)
                    {
                        clsCommon.ShowMessage("Error ! Please Try Again");
                    }
                }

            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }           
            
        }

        public void BindDDlMake()
        {
            clsAdmin objMake = new clsAdmin();
            DataTable dtMake = objMake.GetMakeDetails(ref Message);
            if (dtMake.Rows.Count > 0)
            {
                DDLMake.DataSource = dtMake;
                DDLMake.DataTextField = dtMake.Columns[0].ColumnName;
                DDLMake.DataValueField = dtMake.Columns[1].ColumnName;
                DDLMake.DataBind();
                DDLMake.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                DDLMake.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }
        public void BindDDlModel()
        {
            clsAdmin objMake = new clsAdmin();
            DataTable dtModel = objMake.GetModelDetails(ref Message);
            if (dtModel.Rows.Count > 0)
            {
                DDlModel.DataSource = dtModel;
                DDlModel.DataTextField = dtModel.Columns[0].ColumnName;
                DDlModel.DataValueField = dtModel.Columns[0].ColumnName;
                DDlModel.DataBind();
                DDlModel.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                DDlModel.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        protected void gvMakeModel_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvMakeModel_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvMakeModel_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvMakeModel.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }

        }

        public void BindGrid()
        {
            clsAdmin objMakeModelGrid = new clsAdmin();
            objMakeModelGrid.MakeName = DDLMake.SelectedItem.Text;
            objMakeModelGrid.ModelName = DDlModel.SelectedItem.Text;
            DataTable dtMakeModelGrid = objMakeModelGrid.GetMakeModelGrid(ref Message);
            if (dtMakeModelGrid.Rows.Count > 0)
            {
                gvMakeModel.DataSource = dtMakeModelGrid;
                gvMakeModel.DataBind();
                Session["MakeModel"] = dtMakeModelGrid;
            }
            else
            {
                gvMakeModel.DataSource = null;
                gvMakeModel.DataBind();
                clsCommon.ShowMessage("No Data Found");
                Session["MakeModel"] = null;
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }

        }

        protected void DDLMake_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                clsAdmin objGetModel = new clsAdmin();
                objGetModel.MakeName = DDLMake.SelectedItem.Value;
                DataTable dtModel = objGetModel.GetModelFromMake(ref Message);
                if (dtModel.Rows.Count > 0)
                {
                    DDlModel.DataSource = dtModel;
                    DDlModel.DataTextField = dtModel.Columns[0].ColumnName;
                    DDlModel.DataValueField = dtModel.Columns[0].ColumnName;
                    DDlModel.DataBind();
                    DDlModel.Items.Insert(0, new ListItem("--Select--", "0"));
                }
                else
                {
                    BindDDlModel();
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please try Again.");
            }

        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("MakeModelMaster.aspx");
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string strFileName = "MakeModelMIS";
            DataTable dtMakeModel = (DataTable)(Session["MakeModel"]);

            if (dtMakeModel.Rows.Count > 0)
            {
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dtMakeModel, strFileName);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    //  Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    strFileName += DateTime.Now.ToString("yyyymmddHHmmss") + ".xlsx";
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("content-disposition", "attachment;filename=" + strFileName);
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }

            }

        }
    }
}
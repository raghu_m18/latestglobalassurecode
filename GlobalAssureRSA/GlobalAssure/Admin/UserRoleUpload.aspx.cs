﻿using System.IO;
using System.Data;
using GlobalAssure.Controllers;
using System;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections.Generic;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;

namespace GlobalAssure.Admin
{
    public partial class UserRoleUpload : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRole();
                GetUserID();
            }
        }

        public void BindRole()
        {
            clsAdmin objRole = new clsAdmin();
            DataTable dtRole = objRole.GetRole(ref Message);
            ddlRole.DataSource = dtRole;
            ddlRole.DataTextField = dtRole.Columns[0].ColumnName;
            ddlRole.DataValueField = dtRole.Columns[1].ColumnName;
            ddlRole.DataBind();
            ddlRole.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            clsAdmin ObjUpdateRole = new clsAdmin();
            DataTable dtRoleData = new DataTable();
            string ext = Path.GetExtension(this.FileUpload1.PostedFile.FileName);

            try
            {
                if (ddlRole.SelectedValue != "0")
                {
                    if (ext == ".xlsx" || ext == ".xls")
                    {
                        if (FileUpload1.HasFile)
                        {
                            string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                            string Extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
                            string FilePath = Server.MapPath("~/DownloadExcelSheet/" + FileName);
                            FileUpload1.SaveAs(FilePath);

                            using (ClosedXML.Excel.XLWorkbook workBook = new ClosedXML.Excel.XLWorkbook(FilePath))
                            {
                                //Read the first Sheet from Excel file.
                                IXLWorksheet workSheet = workBook.Worksheet(1);

                                //Create a new DataTable.
                                DataTable dt = new DataTable();

                                //Loop through the Worksheet rows.
                                bool firstRow = true;
                                foreach (IXLRow row in workSheet.Rows())
                                {
                                    //Use the first row to add columns to DataTable.
                                    if (firstRow)
                                    {
                                        foreach (IXLCell cell in row.Cells())
                                        {
                                            dt.Columns.Add(cell.Value.ToString());
                                        }
                                        firstRow = false;
                                    }
                                    else
                                    {
                                        //Add rows to DataTable.
                                        dt.Rows.Add();
                                        int i = 0;
                                        foreach (IXLCell cell in row.Cells())
                                        {
                                            if (cell.Value.ToString() != "")
                                            {
                                                dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                                i++;
                                            }
                                        }
                                    }

                                }

                                dtRoleData = dt;
                            }

                            int RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                            if (dtRoleData.Rows.Count > 0 || dtRoleData != null)
                            {                               
                                for (int i = 0; i < dtRoleData.Rows.Count; i++)
                                {
                                    string EmailID = Convert.ToString(dtRoleData.Rows[i]["EmailID"]);
                                    clsAdmin objCheckUser = new clsAdmin();
                                    objCheckUser.EmailAddress = EmailID;
                                    DataTable dtCheck = objCheckUser.CheckEmail(ref Message);
                                    if (dtCheck.Rows.Count > 0)
                                    {
                                        ObjUpdateRole.EmailAddress = Convert.ToString(dtRoleData.Rows[i]["EmailID"]);
                                        ObjUpdateRole.RoleID = RoleID;
                                        ObjUpdateRole.LastModifiedBy = Convert.ToString(Session["UserID"]);
                                        ObjUpdateRole.UpdateUserRole(ref Message);
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                                clsCommon.ShowMessage("Records updated Successfully !");
                            }
                            else
                            {
                                clsCommon.ShowMessage("No Records Found ! !");
                            }
                        }
                        else
                        {
                            clsCommon.ShowMessage("Please Uplod a File !");
                        }
                    }
                    else
                    {
                        clsCommon.ShowMessage("Please upload file with extension (xlsx|xls)");
                    }
                }
                else
                {
                    clsCommon.ShowMessage("Please select a Role");
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Invalid Format or Data");
            }
        }
        public void GetUserID()
        {
            clsAdmin objUserID = new clsAdmin();
            objUserID.Username = User.Identity.Name;
            DataTable dtUserID = objUserID.GetUserID(ref Message);
            if (dtUserID.Rows.Count > 0)
            {
                Session["UserID"] = dtUserID.Rows[0]["Id"];
            }
            else
            {
                Session["UserID"] = null;
            }
        }
    }
}       
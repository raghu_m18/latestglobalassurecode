﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class StateCityMaster : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager currPageScriptManager = ScriptManager.GetCurrent(Page) as ScriptManager;
                if (currPageScriptManager != null)
                {
                    currPageScriptManager.RegisterPostBackControl(btnDownload);
                }
                if (!IsPostBack)
                {
                    BindDDLState();
                    BindDDLCity();
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("No Data Found !");
            }
            
        }
        public void BindDDLState()
        {
            clsAdmin objState = new clsAdmin();
            DataTable dtState = objState.GetStateDetails(ref Message);
            if(dtState.Rows.Count > 0)
            {
                DDLState.DataSource = dtState;
                DDLState.DataTextField = dtState.Columns[0].ColumnName;
                DDLState.DataValueField = dtState.Columns[1].ColumnName;
                DDLState.DataBind();
                DDLState.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                DDLState.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }
        public void BindDDLCity()
        {
            clsAdmin objCity = new clsAdmin();
            DataTable dtCity = objCity.GetCityDetails(ref Message);
            if (dtCity.Rows.Count > 0)
            {
                DDLCity.DataSource = dtCity;
                DDLCity.DataTextField = dtCity.Columns[0].ColumnName;
                DDLCity.DataValueField = dtCity.Columns[0].ColumnName;
                DDLCity.DataBind();
                DDLCity.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                DDLCity.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        public void BindGrid()
        {
            clsAdmin objBindGrid = new clsAdmin();
            objBindGrid.State = DDLState.SelectedItem.Text;
            objBindGrid.City = DDLCity.SelectedItem.Text;
            DataTable dtStateCity = objBindGrid.GetStateCityDetails(ref Message);
            if(dtStateCity.Rows.Count > 0)
            {
                gvStateCity.DataSource = dtStateCity;
                gvStateCity.DataBind();
                Session["StateCity"] = dtStateCity;
            }
            else
            {
                gvStateCity.DataSource = null;
                gvStateCity.DataBind();
                clsCommon.ShowMessage("Error ! Please Try Again");
                Session["StateCity"] = null;
            }

        }
        protected void DDLState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                clsAdmin objGetCityfromState = new clsAdmin();
                objGetCityfromState.State = DDLState.SelectedItem.Value;
                DataTable dtGetCity = objGetCityfromState.GetCityfromState(ref Message);
                if (dtGetCity.Rows.Count > 0)
                {
                    DDLCity.DataSource = dtGetCity;
                    DDLCity.DataTextField = dtGetCity.Columns[0].ColumnName;
                    DDLCity.DataValueField = dtGetCity.Columns[0].ColumnName;
                    DDLCity.DataBind();
                    DDLCity.Items.Insert(0, new ListItem("--Select--", "0"));
                }
                else
                {
                    BindDDLCity();
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }
            
        }

        protected void gvStateCity_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvStateCity.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }

        }

        protected void gvStateCity_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvStateCity_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("StateCityMaster.aspx");
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }
           
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string strFileName = "StateCityMIS";
            DataTable dtStateCity = (DataTable)(Session["StateCity"]);

            if (dtStateCity.Rows.Count > 0)
            {
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dtStateCity, strFileName);
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    //  Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    strFileName += DateTime.Now.ToString("yyyymmddHHmmss") + ".xlsx";
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("content-disposition", "attachment;filename=" + strFileName);
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }

            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class ParentChildRoleMapping : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BindParentddl();
                    GetUserID();
                    dvGridChid.Visible = false;
                    dvLstChild.Visible = false;                  

                }
                catch (Exception ex)
                {
                    clsCommon.ShowMessage("Error Loading! Please Try Again");
                }
                Session["ChildList"] = null;
            }
            if (Session["ChildList"] == null)
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Name");
                Session["ChildList"] = dataTable;
            }
        }
        void BindParentddl()
        {
            clsAdmin objParentChild = new clsAdmin();
            DataTable dtParent = objParentChild.GetParentDetails(ref Message);
            ddlParent.DataSource = dtParent;
            ddlParent.DataTextField = dtParent.Columns[0].ColumnName;
            ddlParent.DataValueField = dtParent.Columns[1].ColumnName;
            ddlParent.DataBind();
            ddlParent.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        protected void ddlParent_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblParent.Text = ddlParent.SelectedItem.Text;
            hdnParent.Value = ddlParent.SelectedValue;
        }

        void BindGrid()
        {
            clsAdmin objAdmin = new clsAdmin();
            objAdmin.Child = txtChild.Text;
            DataTable dtChild = objAdmin.GetChildDetails(ref Message);
            if (dtChild.Rows.Count > 0)
            {
                gvChild.DataSource = dtChild;
                gvChild.DataBind();
                dvGridChid.Visible = true;
            }
            else
            {
                gvChild.DataSource = null;
                gvChild.DataBind();
                dvGridChid.Visible = false;
                clsCommon.ShowMessage("No Record found");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            clsAdmin objAllData = new clsAdmin();
            try
            {
                if (ddlParent.SelectedValue != "0")
                {
                    if (txtChild.Text != "")
                    {
                        BindGrid();                        
                        //dvLstChild.Visible = false;
                    }
                    else
                    {
                        clsCommon.ShowMessage("Please Enter Child");

                    }
                }
                else
                {
                    clsCommon.ShowMessage("Please Select Parent");
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error !, Please Try Again");
            }
        }

        protected void gvChild_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvChild_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }      

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string str = string.Empty;
            string strname = string.Empty;
            DataTable dt = (DataTable)Session["ChildList"];
            //dt.Columns.Add("Name", typeof(string));
            //dt.Columns.Add("Email", typeof(string));
            DataRow dtrow = null;
            try
            {
                foreach (GridViewRow gvrow in gvChild.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                    if (chk != null & chk.Checked)
                    {
                        dtrow = dt.NewRow();
                        dtrow["Name"] = gvChild.DataKeys[gvrow.RowIndex].Value.ToString();
                        dtrow["Email"] = gvrow.Cells[2].Text;
                        dt.Rows.Add(dtrow);
                    }

                }
                if (dt.Rows.Count > 0)
                {
                    gvlstChilds.DataSource = dt;
                    gvlstChilds.DataBind();
                    dvLstChild.Visible = true;
                    Session["ChildList"] = dt;
                }
                else
                {
                    gvlstChilds.DataSource = null;
                    gvlstChilds.DataBind();
                    dvLstChild.Visible = false;
                    clsCommon.ShowMessage("Please Select User to update");
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error Adding ! Please Try Again");
            }


        }

        protected void gvlstChilds_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        //protected void gvlstChilds_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gvlstChilds.PageIndex = e.NewPageIndex;
        //    DataTable dtlstChilds = (DataTable)Session["ChildList"];
        //    if (dtlstChilds.Rows.Count > 0)
        //    {
        //        gvlstChilds.DataSource = dtlstChilds;
        //        gvlstChilds.DataBind();
        //    }
        //    else
        //    {

        //    }

        //}

        protected void gvlstChilds_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            clsAdmin objUpdateChild = new clsAdmin();
            try
            {
                DataTable dtUpdateChilds = (DataTable)Session["ChildList"];//GetGridData();
                int count = dtUpdateChilds.Rows.Count;
                if (dtUpdateChilds.Rows.Count > 0)
                {
                    for (int i = 0; i < dtUpdateChilds.Rows.Count; i++)
                    {
                        objUpdateChild.ParentUserID = hdnParent.Value;
                        objUpdateChild.Child = Convert.ToString(dtUpdateChilds.Rows[i]["Email"]);
                        objUpdateChild.LastModifiedBy = Convert.ToString(Session["UserID"]);
                        objUpdateChild.UpdateMappingUserID(ref Message);
                    }
                    clsCommon.ShowMessage(count + "  " + " Records Updated Successfully !");
                    Session["ChildList"] = null;
                    dvGridChid.Visible = false;
                    dvLstChild.Visible = false;
                    txtChild.Text = "";
                    ddlParent.SelectedValue = "0";
                    lblParent.Text = "";
                }
                else
                {
                    clsCommon.ShowMessage("No Data to Update");
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error,Please Try again");
            }

        }

        public void GetUserID()
        {
            clsAdmin objUserID = new clsAdmin();
            objUserID.Username = User.Identity.Name;
            DataTable dtUserID = objUserID.GetUserID(ref Message);
            if (dtUserID.Rows.Count > 0)
            {
                Session["UserID"] = dtUserID.Rows[0]["Id"];
            }
            else
            {
                Session["UserID"] = null;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace GlobalAssure.Admin
{
    public class clsAdmin
    {
        #region Properties         
        public string EmailAddress { get; set; }
        public int RoleID { get; set; }
        public string LastModifiedBy { get; set; }
        public string Child { get; set; }       
        public string ParentUserID { get; set; }
        public string Username { get; set; }
        public int PrivilageID { get; set; }
        public decimal?  CommisionPercentage { get; set; }
        public bool IsCommision { get; set; }
        public string  Mode { get; set; }
        public string MakeName { get; set; }
        public string ModelName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string  Plan { get; set; }
        public string NewUser { get; set; }
        public string Olduser { get; set; }
        #endregion

        #region Plan add/update/delete
        public bool AddPlantoUser(string UserId, string PlanName, string CreatedBy, ref string Message)
        {
            bool PlanAdded = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_PlanAddtoUser");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@s_UserId", UserId);
                cmd.Parameters.AddWithValue("@s_Plans", PlanName);
                cmd.Parameters.AddWithValue("@s_CreatedBy", CreatedBy);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                if (Result > 0)
                {
                    PlanAdded = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return PlanAdded;
        }

        public bool UpdatePlantoUser(int PrivilegeDetailID, string PlanName, ref string Message)
        {
            bool Planupdateded = false;
            try
            {
                SqlCommand cmd = new SqlCommand("update tblUserPrivilegeDetail set Plans = @s_Plans where PrivilegeDetailID=@PrivilegeDetailID");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@PrivilegeDetailID", PrivilegeDetailID);
                cmd.Parameters.AddWithValue("@s_Plans", PlanName);
                int Result = DAL.ExecuteNonQuery(cmd);
                if (Result > 0)
                {
                    Planupdateded = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return Planupdateded;
        }

        public bool DeletePlanFromUser(int PlanId, string UserID, ref string Message)
        {
            bool PlanDeleted = false;
            try
            {
                SqlCommand cmd = new SqlCommand("Update tblUserPrivilegeDetail set IsValid = 0,IsOnHold = 1,LastModifyDateTime = GETDATE(),LastModifiedBy = '" + UserID + "' where PrivilegeDetailID = @PrivilegeDetailID");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@PrivilegeDetailID", PlanId);
                int Result = DAL.ExecuteNonQuery(cmd);
                if (Result > 0)
                {
                    PlanDeleted = true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return PlanDeleted;
        }
        public DataTable GetPlanByUserId(string UserId)
        {
            DataTable dtPlan = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetPlanByUserId");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@s_UserId", UserId);
                dtPlan = DAL.ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtPlan;



        }
        public DataTable GetActiveUserId()
        {
            DataTable dtPlan = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("select Email,Id from AspNetUsers where ISNULL(IsActive,1)=1 order by Email asc ");
                cmd.CommandType = CommandType.Text;
                dtPlan = DAL.ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtPlan;



        }
        public DataTable GetActivePlan()
        {
            DataTable dtPlan = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("select [Name], PlanID from tblMstPlan where IsValid=1 and EndDate is null order by [Name] asc ");
                cmd.CommandType = CommandType.Text;
                dtPlan = DAL.ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtPlan;



        }

        #endregion Plan add/update/delete

        #region Certificate
        public DataTable GetCertificateByUserId(string UserId,string CertificateNo)
        {
            DataTable dtPlan = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetCertiByUserId");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@s_UserId", UserId);
                cmd.Parameters.AddWithValue("@s_CertificateNo", CertificateNo);
                dtPlan = DAL.ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtPlan;



        }
        public bool CancelCertificate(int TransactionId, string CertificateNo, ref string Message)
        {
            bool Planupdateded = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_CancelCerti");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@n_TransId", TransactionId);
                cmd.Parameters.AddWithValue("@s_CertificateNo", CertificateNo);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                if (Result > 0)
                {
                    Planupdateded = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Planupdateded;
        }
        #endregion Certificate

        #region Payment Status Update
        public DataTable GetCertificateforPaymentByUserId(string UserId, string CertificateNo, string FromDate, string ToDate)
        {
            DataTable dtPlan = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetCertiforPaymentByUserId");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@s_UserId", UserId);
                cmd.Parameters.AddWithValue("@s_CertificateNo", CertificateNo);
                cmd.Parameters.AddWithValue("@s_FromDate", FromDate);
                cmd.Parameters.AddWithValue("@s_ToDate", ToDate);
                dtPlan = DAL.ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtPlan;



        }
        public bool UpdatePaymentStatus(int TransactionId, string CertificateNo, double TotalAmount, string PayRemarks, string UserId, ref string Message)
        {
            bool Planupdateded = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_UpdatePaymentStatus");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@n_TransId", TransactionId);
                cmd.Parameters.AddWithValue("@s_CertificateNo", CertificateNo);               
                cmd.Parameters.AddWithValue("@n_TotalAmount", TotalAmount);
                cmd.Parameters.AddWithValue("@s_PayRemarks", PayRemarks);
                cmd.Parameters.AddWithValue("@s_UserId", UserId);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                if (Result > 0)
                {
                    Planupdateded = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Planupdateded;
        }
        #endregion Payment Status Update

        #region GetRole
        public DataTable GetRole(ref string Message) //Role Count
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetRoleDetails");
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        #endregion

        public bool UpdateUserRole(ref string Message)
        {
            bool isSaved = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_UpdateRoleDetails");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                cmd.Parameters.AddWithValue("@LastModifiedBy", LastModifiedBy);
                cmd.Parameters.AddWithValue("@Email", EmailAddress);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);              
                if (Result > 0)
                { isSaved = true; }
            }
            catch (Exception ex)
            {                
                Message = ex.Message;
            }

            return isSaved;
        }

        public DataTable CheckEmail(ref string Message) //Role Count
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_CheckEmail");
                cmd.Parameters.AddWithValue("@Email", EmailAddress);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }

        #region GetParent
        public DataTable GetParentDetails(ref string Message) //Role Count
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetParentDetails");
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        #endregion

        #region GetChild
        public DataTable GetChildDetails(ref string Message) //Role Count
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetChildDetails");
                cmd.Parameters.AddWithValue("@Email", Child);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        #endregion

        #region GetAllChilds
        public DataTable GetAllChilds(ref string Message) //Role Count
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetAllChild");
                cmd.Parameters.AddWithValue("@MappingID", ParentUserID);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        #endregion

        public bool UpdateMappingUserID(ref string Message)
        {
            bool isSaved = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_UpdateMappigUserID");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@mappingUserID", ParentUserID);
                cmd.Parameters.AddWithValue("@LastModifiedBy", LastModifiedBy);
                cmd.Parameters.AddWithValue("@Email", Child);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                if (Result > 0)
                { isSaved = true; }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSaved;
        }

        #region ParentChildMIS
        public DataTable GetParentChildMIS(ref string Message) //Role Count
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetParentChildMIS");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;                
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        #endregion

        public bool DeleteChild(ref string Message)
        {
            bool isSaved = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_DeleteChild");
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.AddWithValue("@EmailID", EmailAddress);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                if (Result > 0)
                { isSaved = true; }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSaved;
        }

        #region Commision Update
        public DataTable GetUserDetails(ref string Message) //Role Count
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetUserDetails");
                cmd.Parameters.AddWithValue("@Username", Username);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }

        public bool UpdateCommision(ref string Message)
        {
            bool isSaved = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_UpdateCommision");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PrivilageID", PrivilageID);
                cmd.Parameters.AddWithValue("@CommisionPercentage", CommisionPercentage);
                cmd.Parameters.AddWithValue("@IsCommision", IsCommision);
                cmd.Parameters.AddWithValue("@Mode", Mode);
                cmd.Parameters.AddWithValue("@Username", LastModifiedBy);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                if (Result > 0)
                { isSaved = true; }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSaved;
        }
        #endregion

        #region MakeModelMaster
        public DataTable GetMakeDetails(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetMakeDetails");               
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        public DataTable GetModelDetails(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetModelDetails");
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }

        public DataTable GetModelFromMake(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetModelFromMake");
                cmd.Parameters.AddWithValue("@MakeID", MakeName);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }

        public DataTable GetMakeModelGrid(ref string Message) //Role Count
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetMakeModelDetails");
                cmd.Parameters.AddWithValue("@Make", MakeName);
                cmd.Parameters.AddWithValue("@Model", ModelName);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        #endregion

        #region StateCityMaster
        public DataTable GetStateDetails(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetStateDetails");
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }

        public DataTable GetCityDetails(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetCityDetails");
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        public DataTable GetCityfromState(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetCityFromState");
                cmd.Parameters.AddWithValue("@StateID", State);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }

        public DataTable GetStateCityDetails(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetStateCityDetails");
                cmd.Parameters.AddWithValue("@State", State);
                cmd.Parameters.AddWithValue("@City", City);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }
        #endregion

        #region PlanMaster
        public DataTable GetPlanDetails(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetPlanDetails");
                cmd.Parameters.AddWithValue("@Plan", Plan);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }

        public DataTable GetPlanDDl(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetPlan");
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;

        }


        #endregion

        #region 
        public DataTable GetOldUser(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetOldUser");
                cmd.Parameters.AddWithValue("@OldUser", Olduser);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;
        }


        public bool UpdateNewUser(ref string Message)
        {
            bool isSaved = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_UpdateUser");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@oldUser", Olduser);
                cmd.Parameters.AddWithValue("@newUser", NewUser);
                cmd.Parameters.AddWithValue("@LastModifiedBy", LastModifiedBy);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                if (Result > 0)
                { isSaved = true; }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSaved;
        }
        #endregion

        #region GetUserID
        public DataTable GetUserID(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetUserID");
                cmd.Parameters.AddWithValue("@UserName", Username);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;
        }
        #endregion

        #region UserRoleUnmapping
        public DataTable GetUserRole(ref string Message) //
        {
            DataTable dtPlanName = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("usp_GetUserRole");
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                cmd.CommandType = CommandType.StoredProcedure;
                dtPlanName = DAL.ExecuteDataTable(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
            }
            catch (Exception ex)
            {
                //Logger.WriteLog.Error("clsAdmin-GetHospiCount", ex);
                Message = ex.Message;
            }
            return dtPlanName;
        }


        public bool DeleteRole(ref string Message)
        {
            bool isSaved = false;
            try
            {
                SqlCommand cmd = new SqlCommand("usp_DeleteRole");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmailID", EmailAddress);
                cmd.Parameters.Add("@s_Error_Message", SqlDbType.Char, 500);
                cmd.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                int Result = DAL.ExecuteNonQuery(cmd);
                Message = Convert.ToString(cmd.Parameters["@s_Error_Message"].Value);
                if (Result > 0)
                { isSaved = true; }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return isSaved;
        }
        #endregion

    }

}
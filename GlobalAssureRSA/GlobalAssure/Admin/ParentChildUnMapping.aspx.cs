﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace GlobalAssure.Admin
{
    public partial class ParentChildUnMapping : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    BindParentddl();
                    dvGridChid.Visible = false;
                }
                catch (Exception ex)
                {
                    clsCommon.ShowMessage("Error Loading! Please Try Again");
                }               
            }
        }

        protected void ddlParent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblParent.Text = ddlParent.SelectedItem.Text;
                hdnParent.Value = ddlParent.SelectedValue;
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("Error Loading! Please Try Again");
            }
            
        }

        void BindParentddl()
        {
            try
            {
                clsAdmin objParentChild = new clsAdmin();
                DataTable dtParent = objParentChild.GetParentDetails(ref Message);
                ddlParent.DataSource = dtParent;
                ddlParent.DataTextField = dtParent.Columns[0].ColumnName;
                ddlParent.DataValueField = dtParent.Columns[1].ColumnName;
                ddlParent.DataBind();
                ddlParent.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("Please Try Again");
            }
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            clsAdmin objAllData = new clsAdmin();
            try
            {
                if (ddlParent.SelectedValue != "0")
                {
                    clsAdmin objParentChild = new clsAdmin();
                    objParentChild.ParentUserID = hdnParent.Value;
                    DataTable dtChilds = objParentChild.GetAllChilds(ref Message);
                    if(dtChilds.Rows.Count > 0)
                    {
                        gvChild.DataSource = dtChilds;
                        gvChild.DataBind();
                        dvGridChid.Visible = true;
                    }
                    else
                    {
                        gvChild.DataSource = null;
                        gvChild.DataBind();
                        clsCommon.ShowMessage("No Records Found!");
                        dvGridChid.Visible = false;
                    }
                }
                else
                {
                    clsCommon.ShowMessage("Please Select Parent");
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Error !, Please Try Again");
            }
        }

        protected void gvChild_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvChild_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();            
            dt.Columns.Add("Email", typeof(string));
            DataRow dtrow = null;
            try
            {
                foreach (GridViewRow gvrow in gvChild.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                    if (chk != null & chk.Checked)
                    {
                        dtrow = dt.NewRow();
                        //dtrow["Name"] = gvChild.DataKeys[gvrow.RowIndex].Value.ToString();
                        dtrow["Email"] = gvrow.Cells[2].Text;
                        dt.Rows.Add(dtrow);
                    }

                }
                if (dt.Rows.Count > 0)
                {
                    int Count = dt.Rows.Count;
                    clsAdmin objDeleteChild = new clsAdmin();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objDeleteChild.EmailAddress = Convert.ToString(dt.Rows[i]["Email"]);
                        objDeleteChild.DeleteChild(ref Message);
                    }
                    clsCommon.ShowMessage(Count +" "+ "Records Deleted Successfully !");
                    dvGridChid.Visible = false;
                    ddlParent.SelectedValue = "0";
                    lblParent.Text = "";
                }
                else
                {
                    clsCommon.ShowMessage("No Child Found, Please Select a user!");
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Invalid Data, Please Try Again Later!");
            }
            

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class UserRoleUnmapping : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindDDl();
                    dvUserRole.Visible = false;
                }
               
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please try again");
            }
        }
        public void BindDDl()
        {
            clsAdmin objUserRole = new clsAdmin();
            DataTable dtUserRole = objUserRole.GetRole(ref Message);
            if (dtUserRole.Rows.Count > 0)
            {
                ddlRole.DataSource = dtUserRole;
                ddlRole.DataTextField = dtUserRole.Columns[0].ColumnName;
                ddlRole.DataValueField = dtUserRole.Columns[1].ColumnName;
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlRole.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                clsAdmin objGetUserList = new clsAdmin();
                if (ddlRole.SelectedValue != "0")
                {
                    objGetUserList.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                    DataTable dtUserList = objGetUserList.GetUserRole(ref Message);
                    if(dtUserList.Rows.Count > 0)
                    {
                        gvUserRole.DataSource = dtUserList;
                        gvUserRole.DataBind();
                        dvUserRole.Visible = true;
                    }
                    else
                    {
                        gvUserRole.DataSource = null;
                        gvUserRole.DataBind();
                        clsCommon.ShowMessage("No Records Found !");
                        dvUserRole.Visible = false;
                    }
                }
                else
                {
                    clsCommon.ShowMessage("Please Select a Role");
                    dvUserRole.Visible = false;
                }
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please try again");
                dvUserRole.Visible = false;
            }
            
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("UserRoleUnmapping.aspx");
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please try again");
            }
            
        }

        protected void gvUserRole_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void gvUserRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblParent.Text = ddlRole.SelectedItem.Text;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Email", typeof(string));
            DataRow dtrow = null;
            try
            {
                foreach (GridViewRow gvrow in gvUserRole.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                    if (chk != null & chk.Checked)
                    {
                        dtrow = dt.NewRow();
                        //dtrow["Name"] = gvChild.DataKeys[gvrow.RowIndex].Value.ToString();
                        dtrow["Email"] = gvrow.Cells[2].Text;
                        dt.Rows.Add(dtrow);
                    }

                }
                if (dt.Rows.Count > 0)
                {
                    int Count = dt.Rows.Count;
                    clsAdmin objDeleteChild = new clsAdmin();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objDeleteChild.EmailAddress = Convert.ToString(dt.Rows[i]["Email"]);
                        objDeleteChild.DeleteRole(ref Message);
                    }
                    clsCommon.ShowMessage(Count + " " + "Records Deleted Successfully !");
                    dvUserRole.Visible = false;
                    ddlRole.SelectedValue = "0";
                    lblParent.Text = "";
                }
                else
                {
                    clsCommon.ShowMessage("No Role Found, Please Select a user!");
                }
            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage("Invalid Data, Please Try Again Later!");
            }


        }
    
    }
}
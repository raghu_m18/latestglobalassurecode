﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using GlobalAssure.Entity;
using System.Configuration;
using System.IO;
using System.Globalization;

namespace GlobalAssure.Admin
{

    public partial class _EmailConfigurator : BaseClass
    {
        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();
        clsAdmin objAdmin = new clsAdmin();
        string strMsg = "";
        bool IsDownload = false;
        protected void Page_Load(object sender, EventArgs e)
        {

            
            ScriptManager.GetCurrent(this).RegisterPostBackControl(ddlAction);
            ScriptManager.GetCurrent(this).RegisterPostBackControl(ddlEmailTemplate);
            ScriptManager.GetCurrent(this).RegisterPostBackControl(btnSaveEmailTemplate);
            ScriptManager.GetCurrent(this).RegisterPostBackControl(btnCancelEmailTemplate);
            ScriptManager.GetCurrent(this).RegisterPostBackControl(btnEmailLogDownload);
            txtOrderNo.Attributes.Add("type", "number");
            txtEmailLimitCount.Attributes.Add("type", "number");
            txtSMSTriggerCount.Attributes.Add("type", "number");
            if (ValidateAdmin() == true)
            {
                if (!IsPostBack)
                {
                    
                }
            }
            else
            {
                Response.Redirect("~/Account/Login");

            }
        }

        protected void ddlAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAction.SelectedIndex > 0)
            {
                mvEmail.ActiveViewIndex = Convert.ToInt32(ddlAction.SelectedItem.Value) - 1;
                if (Convert.ToInt32(ddlAction.SelectedItem.Value) == 1)//plan Mapping
                {
                    BindEmailConfigGrid();
                }
                else if (Convert.ToInt32(ddlAction.SelectedItem.Value) == 2)//Email Send Report
                {
                    txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    BindEmailLogGrid(1);
                }
                else if (Convert.ToInt32(ddlAction.SelectedItem.Value) == 3)//plan Mapping
                {
                    BindTemplateName();
                }
            }

        }

        protected void btnSaveEmailConfig_Click(object sender, EventArgs e)
        {


            clsEmailConfig objEmailConf = new clsEmailConfig();
            objEmailConf.SMTPHost = txtSMTPHostName.Value;
            objEmailConf.SMTPPort = txtSMTPPortNo.Value;
            objEmailConf.SMTPUserName = txtSMTPUserName.Value;
            objEmailConf.SMTPPassword = txtSMTPPassword.Value;
            objEmailConf.FromEmail = txtFromEmail.Value;
            objEmailConf.FromName = txtFrom.Value;
            int OrdNo = 0;
            int.TryParse(txtOrderNo.Value, out OrdNo);
            objEmailConf.OrderNo = OrdNo;
            int EMailCount = 0;
            int.TryParse(txtEmailLimitCount.Value, out EMailCount);
            objEmailConf.EmailLimitCount = EMailCount;
            int SMSTriggerCount = 0;
            int.TryParse(txtSMSTriggerCount.Value, out SMSTriggerCount);
            objEmailConf.SMSTriggerCount = SMSTriggerCount;

            objEmailConf.Status = (rdoActive.Checked == true) ? 1 : 0;
            objEmailConf.SecureSMTP = (chkIsSecure.Checked == true) ? 1 : 0;
            objEmailConf.LastModifyBy = Convert.ToString(Session["UsrId"]);

            if (btnSaveEmailConfig.Text == "Save")
            {
                bool isSaved = objEmailConf.SaveEmailConfig(ref strMsg);
                if (isSaved)
                {
                    clsCommon.ShowMessage("Record Saved Successfully.");
                }
            }
            else
            {
                int ConfigId = 0;

                int.TryParse(Convert.ToString(ViewState["EmailConfigId"]), out ConfigId);

                objEmailConf.EmailConfigId = ConfigId;
                bool isUpdate = objEmailConf.UpdateEmailConfig(ref strMsg);
                if (isUpdate)
                {
                    clsCommon.ShowMessage("Record Updated Successfully.");
                }
            }
            btnSaveEmailConfig.Text = "Save";

            ClearTextBox();
            BindEmailConfigGrid();

        }



        void BindEmailConfigGrid()
        {
            try
            {
                clsEmailConfig objEmailConf = new clsEmailConfig();
                DataTable dtEmailConfig = objEmailConf.GetEmailConfigData(ref strMsg);
                if (dtEmailConfig != null && dtEmailConfig.Rows.Count > 0)
                {
                    gvEmailConfig.DataSource = dtEmailConfig;
                    gvEmailConfig.DataBind();
                }
                else
                {
                    gvEmailConfig.DataSource = null;
                    gvEmailConfig.DataBind();
                    clsCommon.ShowMessage("No Data Found");
                }

            }
            catch (Exception ex)
            {

                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }

        protected void gvEmailConfig_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drview = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                TableCell EmailLimitCountCell = e.Row.Cells[9];
                int EmailLimit = Convert.ToInt32(gvEmailConfig.DataKeys[e.Row.RowIndex].Values["n_EmailLimitCount"]);
                if (EmailLimit == 0)
                {
                    EmailLimitCountCell.Text = "";
                }


                TableCell SMSTriggerCountCell = e.Row.Cells[11];
                int SMSTriggerCount = Convert.ToInt32(gvEmailConfig.DataKeys[e.Row.RowIndex].Values["n_SMSTriggerCount"]);
                if (SMSTriggerCount == 0)
                {
                    SMSTriggerCountCell.Text = "";
                }

                TableCell StatusCell = e.Row.Cells[12];
                int status = Convert.ToInt32(gvEmailConfig.DataKeys[e.Row.RowIndex].Values["n_Status"]);
                


                StatusCell.Font.Bold = true;
                if (status == 0)
                {
                    StatusCell.Text = "Inactive";
                    StatusCell.ForeColor = System.Drawing.Color.Brown;
                }
                else if (status == 1)
                {
                    StatusCell.Text = "Active";
                    StatusCell.ForeColor = System.Drawing.Color.Green;

                }
            }
        }

        protected void gvEmailConfig_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToLower() == "edt")
                {
                    btnSaveEmailConfig.Text = "Update";
                    GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    int index = row.RowIndex;
                    txtSMTPHostName.Value = Convert.ToString(gvEmailConfig.DataKeys[index]["s_SMTP_Host"]);
                    txtSMTPPortNo.Value = Convert.ToString(gvEmailConfig.DataKeys[index]["s_SMTP_Port"]);
                    txtSMTPUserName.Value = Convert.ToString(gvEmailConfig.DataKeys[index]["s_SMTP_User_Name"]);
                    txtSMTPPassword.Value = Convert.ToString(gvEmailConfig.DataKeys[index]["s_SMTP_Password"]);
                    txtFromEmail.Value = Convert.ToString(gvEmailConfig.DataKeys[index]["s_From_Email"]);
                    txtFrom.Value = Convert.ToString(gvEmailConfig.DataKeys[index]["s_From_Name"]);
                    txtOrderNo.Value = Convert.ToString(gvEmailConfig.DataKeys[index]["n_OrderNo"]);
                    txtEmailLimitCount.Value = (Convert.ToString(gvEmailConfig.DataKeys[index]["n_EmailLimitCount"]) == "0") ? "" : Convert.ToString(gvEmailConfig.DataKeys[index]["n_EmailLimitCount"]);
                    txtSMSTriggerCount.Value = (Convert.ToString(gvEmailConfig.DataKeys[index]["n_SMSTriggerCount"]) == "0") ? "" : Convert.ToString(gvEmailConfig.DataKeys[index]["n_SMSTriggerCount"]);
                    ViewState["EmailConfigId"] = gvEmailConfig.DataKeys[index]["n_Email_Config_Id"];
                    bool SecureEmail = false;
                    bool.TryParse(Convert.ToString(gvEmailConfig.DataKeys[index]["n_SecureSMTP"]), out SecureEmail);
                    chkIsSecure.Checked = SecureEmail;
                    int Status = 0;
                    int.TryParse(Convert.ToString(gvEmailConfig.DataKeys[index]["n_Status"]), out Status);
                    if (Status == 1)
                    {
                        rdoInActive.Checked = false;
                        rdoActive.Checked = true;
                    }
                    else
                    {
                        rdoActive.Checked = false;
                        rdoInActive.Checked = true; 
                    }
                }

            }
            catch (Exception ex)
            {
                clsCommon.ShowMessage(ex.Message.ToString().Replace("'", " "));
            }
        }


        void ClearTextBox()
        {
            txtSMTPHostName.Value = "";
            txtSMTPPortNo.Value = "";
            txtSMTPUserName.Value = "";
            txtSMTPPassword.Value = "";
            txtFromEmail.Value = "";
            txtFrom.Value = "";
            txtOrderNo.Value = "";
            txtEmailLimitCount.Value = "";
            txtSMSTriggerCount.Value = "";
            rdoInActive.Checked = false;
            rdoActive.Checked = true;
            chkIsSecure.Checked = false;
        }


        void BindTemplateName()
        {
            string[] TemplateFileName = Directory.GetFiles(Server.MapPath("~/Content/EmailTemplates"));
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("--Select--", "0");
            foreach (string item in TemplateFileName)
            {
                string FileName = Path.GetFileNameWithoutExtension(item);
                list.Add(FileName, item);
            }

            ddlEmailTemplate.DataSource = list;
            ddlEmailTemplate.DataTextField = "Key";
            ddlEmailTemplate.DataValueField = "Value";
            ddlEmailTemplate.DataBind();


        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearTextBox();
            btnSaveEmailConfig.Text = "Save";
            ViewState["EmailConfigId"] = "0";
        }

        protected void ddlEmailTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEmailTemplate.SelectedIndex > 0)
            {
                btnSaveEmailTemplate.Enabled = true;
                string emailContain = "";

                using (System.IO.StreamReader reader = new System.IO.StreamReader(ddlEmailTemplate.SelectedItem.Value))
                {
                    emailContain = reader.ReadToEnd();
                }

                txtEmailTemplate.Text = emailContain;
            }
            else
            {
                btnSaveEmailTemplate.Enabled = false;
            }
            
        }

        protected void btnSaveEmailTemplate_Click(object sender, EventArgs e)
        {
            if (txtEmailTemplate.Text.Trim() != "")
            {
                string strFilePath = ddlEmailTemplate.SelectedItem.Value;
                StreamWriter sw = new StreamWriter(strFilePath);
                sw.Write(txtEmailTemplate.Text);
                sw.Dispose();
                clsCommon.ShowMessage("Data Saved Successfully");
                txtEmailTemplate.Text = "";
                ddlEmailTemplate.SelectedIndex = 0;
            }
            else
            {
                clsCommon.ShowMessage("No data availabe for save");
            }

        }

        protected void btnCancelEmailTemplate_Click(object sender, EventArgs e)
        {
            txtEmailTemplate.Text = "";
            ddlEmailTemplate.SelectedIndex = 0;
        }


        protected void PageSize_Changed(object sender, EventArgs e)
        {
            BindEmailLogGrid(1);
        }


        void BindEmailLogGrid(int PageIndex)
        {
            char DownloadFlag = '0';
            if (IsDownload == true)
            {
                DownloadFlag = '1';
            }

            int RecourdCount = 0;
            clsEmailConfig objEmailConfig = new clsEmailConfig();

            DateTime dtFrom = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string strFromDate = dtFrom.ToString("yyyy-MM-dd");
            DateTime dtTo = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string strToDate = dtTo.ToString("yyyy-MM-dd");

            DataTable dtLog = objEmailConfig.GetEmailLogData(PageIndex, int.Parse(ddlPageSize.SelectedValue), strFromDate, strToDate, out RecourdCount, ref strMsg, DownloadFlag);

            if (DownloadFlag == '1')
            {
                if (dtLog != null && dtLog.Rows.Count > 0)
                    ExportToExcel(dtLog);
                else
                    clsCommon.ShowMessage("No Record Found");
            }
            else
            {

                if (dtLog != null && dtLog.Rows.Count > 0)
                {
                    gvEmailSendReport.DataSource = dtLog;
                    gvEmailSendReport.DataBind();

                    PopulatePager(RecourdCount, PageIndex);
                }
                else
                {
                    gvEmailSendReport.DataSource = null;
                    gvEmailSendReport.DataBind();
                    clsCommon.ShowMessage("No Record Found");
                }
            }

        }


        protected void Page_Changed(object sender, EventArgs e)
        {
            int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
            this.BindEmailLogGrid(pageIndex);
        }

        private void PopulatePager(int recordCount, int currentPage)
        {
            double dblPageCount = (double)((decimal)recordCount / decimal.Parse(ddlPageSize.SelectedValue));
            int pageCount = (int)Math.Ceiling(dblPageCount);
            List<ListItem> pages = new List<ListItem>();
            if (pageCount > 0)
            {
                pages.Add(new ListItem("First", "1", currentPage > 1));
                for (int i = 1; i <= pageCount; i++)
                {
                    pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPage));
                }
                pages.Add(new ListItem("Last", pageCount.ToString(), currentPage < pageCount));
            }
            rptPager.DataSource = pages;
            rptPager.DataBind();
        }

        protected void btnSearchEmailLog_Click(object sender, EventArgs e)
        {
            BindEmailLogGrid(1);
        }

        protected void btnEmailLogDownload_Click(object sender, EventArgs e)
        {
            IsDownload = true;
            BindEmailLogGrid(1);
        }


        protected void ExportToExcel(DataTable dtData)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=EmailLogData_" + DateTime.Now.Ticks + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                GridView GridView1 = new GridView();

                GridView1.AllowPaging = false;
                GridView1.ShowHeader = true;
                GridView1.DataSource = dtData;
                GridView1.DataBind();

                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = GridView1.HeaderStyle.BackColor;
                }

                foreach (GridViewRow row in GridView1.Rows)
                {
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView1.RenderControl(hw);

                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

    }


}
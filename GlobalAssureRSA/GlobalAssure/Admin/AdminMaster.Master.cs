﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class _AdminMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UsrId"] != null)
            {
                spnLoginUser.InnerHtml =Convert.ToString(Session["UsrId"]);
            }
        }
    }
}
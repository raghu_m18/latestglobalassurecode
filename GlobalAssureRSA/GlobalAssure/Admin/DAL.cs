﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace GlobalAssure.Admin
{
   public class DAL
    {
      static  SqlConnection SqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        public static int ExecuteNonQuery(SqlCommand SqlCmd)
       {
           int Status = 0;
          
            try
           {
               SqlCmd.Connection = SqlCon;
               if(SqlCon.State!=ConnectionState.Open)
               SqlCon.Open();
               Status = SqlCmd.ExecuteNonQuery();
               SqlCon.Close();
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               SqlCon.Close();
           }

           return Status;
       }
     public static int ExecuteNonQuery(SqlCommand SqlCmd, string CommandText, CommandType CmdType)
       {
           int Status = 0;
           try
           {
               SqlCmd.Connection = SqlCon;
               SqlCmd.CommandText = CommandText;
               SqlCmd.CommandType = CmdType;
               if (SqlCon.State != ConnectionState.Open)
               SqlCon.Open();
               Status = SqlCmd.ExecuteNonQuery();
               SqlCon.Close();
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               SqlCon.Close();
           }

           return Status;
       }
     public static DataSet ExecuteDataset(SqlCommand SqlCmd, string CommandText, CommandType CmdType)
       {
           DataSet dsResult = new DataSet();
           try
           {
               SqlCmd.Connection = SqlCon;
               SqlCmd.CommandText = CommandText;
               SqlCmd.CommandType = CmdType;
               if (SqlCon.State != ConnectionState.Open)
               SqlCon.Open();
                SqlDataAdapter SqlAdapter = new SqlDataAdapter(SqlCmd);
                SqlAdapter.Fill(dsResult);
               SqlCon.Close();
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               SqlCon.Close();
           }

           return dsResult;
       }
     public static DataSet ExecuteDataset(SqlCommand SqlCmd)
     {
         DataSet dsResult = new DataSet();
         try
         {
             SqlCmd.Connection = SqlCon;
             if (SqlCon.State != ConnectionState.Open)
             SqlCon.Open();
             SqlDataAdapter OracleAdapter = new SqlDataAdapter(SqlCmd);
             OracleAdapter.Fill(dsResult);
             SqlCon.Close();
         }
         catch (Exception ex)
         {
             throw ex;
         }
         finally
         {
             SqlCon.Close();
         }

         return dsResult;
     }
     public static DataTable ExecuteDataTable(SqlCommand SqlCmd, string CommandText, CommandType CmdType,bool IsTransaction=false)
     {
         DataTable dtResult = new DataTable();
         try
         {
             if (IsTransaction==false)
             {
                 
                 SqlCmd.Connection = SqlCon;
                 if (SqlCon.State != ConnectionState.Open)
                     SqlCon.Open();
             }
             SqlCmd.CommandText = CommandText;
             SqlCmd.CommandType = CmdType;
             SqlDataAdapter OracleAdapter = new SqlDataAdapter(SqlCmd);
             OracleAdapter.Fill(dtResult);
             
         }
         catch (Exception ex)
         {
             throw ex;
         }
         finally
         {
             if (IsTransaction == false)
             SqlCon.Close();
         }
        
         return dtResult;
     }
     public static DataTable ExecuteDataTable(SqlCommand SqlCmd)
     {
         DataTable dtResult = new DataTable();
         try
         {
             SqlCmd.Connection = SqlCon;
             if (SqlCon.State != ConnectionState.Open)
             SqlCon.Open();
             SqlDataAdapter OracleAdapter = new SqlDataAdapter(SqlCmd);
             OracleAdapter.Fill(dtResult);
             SqlCon.Close();
         }
         catch (Exception ex)
         {
             throw ex;
         }
         finally
         {
             SqlCon.Close();
         }

         return dtResult;
     }

     public static object ExecuteScalar(SqlCommand SqlCmd)
     {
         object Result;
         try
         {
             SqlCmd.Connection = SqlCon;
             if (SqlCon.State != ConnectionState.Open)
                 SqlCon.Open();
             Result = SqlCmd.ExecuteScalar();
             SqlCon.Close();
         }
         catch (Exception ex)
         {
             throw ex;
         }
         finally
         {
             SqlCon.Close();
         }

         return Result;
     }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GlobalAssure.Admin
{
    public partial class UserCorrection : System.Web.UI.Page
    {
        string Message = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    dvNewEmail.Visible = false;
                    dvuser.Visible = false;
                    GetUserID();
                }
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage(""+ ex +"");
            }
                       
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hdnUsername.Value = txtOlduser.Text;
                BindGrid();
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("" + ex + "");
            }
           
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserCorrection.aspx");
        }

        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string OldUser = hdnUsername.Value;
                clsAdmin objNewUser = new clsAdmin();
                objNewUser.NewUser = txtNewuser.Text;
                objNewUser.Olduser = OldUser;
                objNewUser.LastModifiedBy = Convert.ToString(Session["UserID"]);
                if (objNewUser.UpdateNewUser(ref Message))
                {
                    dvNewEmail.Visible = false;
                    txtOlduser.Enabled = true;
                    dvuser.Visible = false;
                    txtNewuser.Text = "";
                    txtOlduser.Text = "";
                    clsCommon.ShowMessage("Update Successfully");
                }
                else
                {
                    dvNewEmail.Visible = true;
                    txtOlduser.Enabled = false;
                    dvuser.Visible = true;
                    clsCommon.ShowMessage("Failed ! Please Try Again");
                }
            }
            catch(Exception ex)
            {
                clsCommon.ShowMessage("Error ! Please Try Again");
            }
           
        }

        public void BindGrid()
        {
            clsAdmin objUser = new clsAdmin();
            objUser.Olduser = txtOlduser.Text;
            DataTable dtuser = objUser.GetOldUser(ref Message);
            if (dtuser.Rows.Count > 0)
            {
                gvUsers.DataSource = dtuser;
                gvUsers.DataBind();
                dvNewEmail.Visible = true;
                txtOlduser.Enabled = false;
                dvuser.Visible = true;
            }
            else
            {
                gvUsers.DataSource = null;
                gvUsers.DataBind();
                txtOlduser.Enabled = true;
                dvNewEmail.Visible = false;
                dvuser.Visible = true;
                clsCommon.ShowMessage("No User Found");
            }
        }

        public void GetUserID()
        {
            clsAdmin objUserID = new clsAdmin();
            objUserID.Username = User.Identity.Name;
            DataTable dtUserID = objUserID.GetUserID(ref Message);
            if(dtUserID.Rows.Count > 0)
            {
                Session["UserID"] = dtUserID.Rows[0]["Id"];
            }
            else
            {
                Session["UserID"] = null;
            }
        }
    }
}
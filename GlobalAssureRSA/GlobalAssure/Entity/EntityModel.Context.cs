﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class GlobalAssureRSAEntities : DbContext
    {
        public GlobalAssureRSAEntities()
            : base("name=GlobalAssureRSAEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<DEMO> DEMOes { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<Membership> Memberships { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<tblAutoMailCommunicationLog> tblAutoMailCommunicationLogs { get; set; }
        public virtual DbSet<tblCDReplenishDetail> tblCDReplenishDetails { get; set; }
        public virtual DbSet<tblChequeAuditLog> tblChequeAuditLogs { get; set; }
        public virtual DbSet<tblCodificationErrorLog> tblCodificationErrorLogs { get; set; }
        public virtual DbSet<tblGeneratedCertificateAuditLog> tblGeneratedCertificateAuditLogs { get; set; }
        public virtual DbSet<tblInternalCodification> tblInternalCodifications { get; set; }
        public virtual DbSet<tblInvoiceDocumentDetail> tblInvoiceDocumentDetails { get; set; }
        public virtual DbSet<tblMstBank> tblMstBanks { get; set; }
        public virtual DbSet<tblMstCity> tblMstCities { get; set; }
        public virtual DbSet<tblMstCommon> tblMstCommons { get; set; }
        public virtual DbSet<tblMstCommonType> tblMstCommonTypes { get; set; }
        public virtual DbSet<tblMstCommunication> tblMstCommunications { get; set; }
        public virtual DbSet<tblMstCommunicationName> tblMstCommunicationNames { get; set; }
        public virtual DbSet<tblMstPayment> tblMstPayments { get; set; }
        public virtual DbSet<tblMstProduct> tblMstProducts { get; set; }
        public virtual DbSet<tblMstState> tblMstStates { get; set; }
        public virtual DbSet<tblMstTieUpCompany> tblMstTieUpCompanies { get; set; }
        public virtual DbSet<tblOnlinePaymentDetail> tblOnlinePaymentDetails { get; set; }
        public virtual DbSet<tblOnlinePaymentTransactionLog> tblOnlinePaymentTransactionLogs { get; set; }
        public virtual DbSet<tblPasswordHistory> tblPasswordHistories { get; set; }
        public virtual DbSet<tblPlanFeatureDetail> tblPlanFeatureDetails { get; set; }
        public virtual DbSet<tblPlanFeatureOfProduct> tblPlanFeatureOfProducts { get; set; }
        public virtual DbSet<tblTransactionAuditLog> tblTransactionAuditLogs { get; set; }
        public virtual DbSet<tblUserAuditLog> tblUserAuditLogs { get; set; }
        public virtual DbSet<tblUserFloatAmount> tblUserFloatAmounts { get; set; }
        public virtual DbSet<tblUserLockingAudit> tblUserLockingAudits { get; set; }
        public virtual DbSet<tblUserPrivilegeDetail> tblUserPrivilegeDetails { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<tblMstWorkFlowStatu> tblMstWorkFlowStatus { get; set; }
        public virtual DbSet<tblMstVendor> tblMstVendors { get; set; }
        public virtual DbSet<tblsPaymentMapping> tblsPaymentMappings { get; set; }
        public virtual DbSet<tblMstMake> tblMstMakes { get; set; }
        public virtual DbSet<tblMstPlan> tblMstPlans { get; set; }
        public virtual DbSet<tblMstIncidentDetail> tblMstIncidentDetails { get; set; }
        public virtual DbSet<tblmstModelSegment> tblmstModelSegments { get; set; }
        public virtual DbSet<tblMstModel> tblMstModels { get; set; }
        public virtual DbSet<tblVendorBranchDetail> tblVendorBranchDetails { get; set; }
        public virtual DbSet<tblMstIncident> tblMstIncidents { get; set; }
        public virtual DbSet<tblAssistDetail> tblAssistDetails { get; set; }
        public virtual DbSet<tblTransaction> tblTransactions { get; set; }
        public virtual DbSet<tblMobileAppUser> tblMobileAppUsers { get; set; }
        public virtual DbSet<tblGstNO> tblGstNO { get; set; }
        public virtual DbSet<tblRequestResponse> tblRequestResponse { get; set; }
        public virtual DbSet<tblSaveDocumentAPI> tblSaveDocumentAPI { get; set; }
        public virtual DbSet<tblStateMasterREL> tblStateMasterREL { get; set; }
        public virtual DbSet<tblPlanMasterTravelREL> tblPlanMasterTravelREL { get; set; }
        public virtual DbSet<tblCountryMasterTravelREL> tblCountryMasterTravelREL { get; set; }
        public virtual DbSet<tblAsstVendorUserLocation> tblAsstVendorUserLocation { get; set; }
        public virtual DbSet<tblPedalPlanMapping> tblPedalPlanMapping { get; set; }
        public virtual DbSet<tblVendorResponseUrl> tblVendorResponseUrl { get; set; }
        public virtual DbSet<tblOTPProcess> tblOTPProcess { get; set; }
        public virtual DbSet<tblPlansMapping> tblPlansMapping { get; set; }
        public virtual DbSet<tblCityProductMapping> tblCityProductMapping { get; set; }
        public virtual DbSet<tblRoleMaster> tblRoleMaster { get; set; }
        public virtual DbSet<tblRoleMenuMappingMaster> tblRoleMenuMappingMaster { get; set; }
        public virtual DbSet<tblMenuMaster> tblMenuMaster { get; set; }
        public virtual DbSet<tblAssistDetails_Audit> tblAssistDetails_Audit { get; set; }
        public virtual DbSet<tblCaseCharges> tblCaseCharges { get; set; }
        public virtual DbSet<tblPedalClaimIntimation> tblPedalClaimIntimation { get; set; }
        public virtual DbSet<tblBhartiAXADetails> tblBhartiAXADetails { get; set; }
        public virtual DbSet<tblIFFCODetails> tblIFFCODetails { get; set; }
        public virtual DbSet<tblTataAIGDetails> tblTataAIGDetails { get; set; }
        public virtual DbSet<tblCardProtectDetails> tblCardProtectDetails { get; set; }
        public virtual DbSet<tblMakeMapping> tblMakeMapping { get; set; }
        public virtual DbSet<tblModelMapping> tblModelMapping { get; set; }
        public virtual DbSet<tblMasterPolicyNo> tblMasterPolicyNo { get; set; }
        public virtual DbSet<tblCertificateMasterPolicyNoMappingLog> tblCertificateMasterPolicyNoMappingLog { get; set; }
        public virtual DbSet<tblUniversalShampoo> tblUniversalShampoo { get; set; }
        public virtual DbSet<tblOEMCompanyMapping> tblOEMCompanyMapping { get; set; }
        public virtual DbSet<tblHospiCashPlanMapping> tblHospiCashPlanMapping { get; set; }
        public virtual DbSet<tblHospiCashDetails> tblHospiCashDetails { get; set; }
        public virtual DbSet<tblPartnerAuthCodeMapping> tblPartnerAuthCodeMapping { get; set; }
        public virtual DbSet<tblAssistEmail> tblAssistEmail { get; set; }
        public virtual DbSet<tblIDFCBank> tblIDFCBank { get; set; }
        public virtual DbSet<tblEMIProtectPremiumDetails> tblEMIProtectPremiumDetails { get; set; }
        public virtual DbSet<tblHDFCErgo> tblHDFCErgo { get; set; }
        public virtual DbSet<tblAPITransactionErrorLog> tblAPITransactionErrorLog { get; set; }
        public virtual DbSet<tblICICILombard> tblICICILombard { get; set; }
        public virtual DbSet<tblPedalCycleMultiProductPlanID> tblPedalCycleMultiProductPlanID { get; set; }
        public virtual DbSet<tblPCCompanyLogo> tblPCCompanyLogo { get; set; }
        public virtual DbSet<tblICICILombardSMSReqRes> tblICICILombardSMSReqRes { get; set; }
        public virtual DbSet<tblIPAddress> tblIPAddress { get; set; }
        public virtual DbSet<tblMstAddOn> tblMstAddOn { get; set; }
        public virtual DbSet<tblDocUploadedPath> tblDocUploadedPath { get; set; }
        public virtual DbSet<tblProductPlanMapping> tblProductPlanMapping { get; set; }
        public virtual DbSet<tblAssistCaseTypeSubCaseTypeMapping> tblAssistCaseTypeSubCaseTypeMapping { get; set; }
        public virtual DbSet<tblEBikeDealerDetails> tblEBikeDealerDetails { get; set; }
        public virtual DbSet<tblElectricBikeDetails> tblElectricBikeDetails { get; set; }
        public virtual DbSet<tblPinnace> tblPinnace { get; set; }
    
        public virtual int Insert_Bank(ObjectParameter returnCode)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_Bank", returnCode);
        }
    
        public virtual int Insert_MakeModel(ObjectParameter returnCode)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_MakeModel", returnCode);
        }
    
        public virtual int Insert_Plan(ObjectParameter returnCode)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_Plan", returnCode);
        }
    
        public virtual int Insert_StateCity(ObjectParameter returnCode)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_StateCity", returnCode);
        }
    
        public virtual int Insert_UserFloatAmount(string createdBy, ObjectParameter returnCode)
        {
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_UserFloatAmount", createdByParameter, returnCode);
        }
    
        public virtual int Insert_UserPermission(string createdBy, ObjectParameter returnCode)
        {
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Insert_UserPermission", createdByParameter, returnCode);
        }
    
        public virtual int Update_ChequeStatus(ObjectParameter returnCode)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Update_ChequeStatus", returnCode);
        }
    
        public virtual int Update_LockUnlockUsers(ObjectParameter returnCode)
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Update_LockUnlockUsers", returnCode);
        }
    
        public virtual int Update_UserPermission(string createdBy, ObjectParameter returnCode)
        {
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Update_UserPermission", createdByParameter, returnCode);
        }
    
        public virtual int usp_UniqeNumberGeneration(string codePrefix, string insuranceCompanyCode, string inwardType, string relationshipCode, string policyNo, string employeeNo, ObjectParameter uniqueCode)
        {
            var codePrefixParameter = codePrefix != null ?
                new ObjectParameter("CodePrefix", codePrefix) :
                new ObjectParameter("CodePrefix", typeof(string));
    
            var insuranceCompanyCodeParameter = insuranceCompanyCode != null ?
                new ObjectParameter("InsuranceCompanyCode", insuranceCompanyCode) :
                new ObjectParameter("InsuranceCompanyCode", typeof(string));
    
            var inwardTypeParameter = inwardType != null ?
                new ObjectParameter("InwardType", inwardType) :
                new ObjectParameter("InwardType", typeof(string));
    
            var relationshipCodeParameter = relationshipCode != null ?
                new ObjectParameter("RelationshipCode", relationshipCode) :
                new ObjectParameter("RelationshipCode", typeof(string));
    
            var policyNoParameter = policyNo != null ?
                new ObjectParameter("PolicyNo", policyNo) :
                new ObjectParameter("PolicyNo", typeof(string));
    
            var employeeNoParameter = employeeNo != null ?
                new ObjectParameter("EmployeeNo", employeeNo) :
                new ObjectParameter("EmployeeNo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UniqeNumberGeneration", codePrefixParameter, insuranceCompanyCodeParameter, inwardTypeParameter, relationshipCodeParameter, policyNoParameter, employeeNoParameter, uniqueCode);
        }
    }
}

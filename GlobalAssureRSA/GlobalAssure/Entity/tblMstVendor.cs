//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblMstVendor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblMstVendor()
        {
            this.tblVendorBranchDetails = new HashSet<tblVendorBranchDetail>();
        }
    
        public decimal VendorID { get; set; }
        public string VendorName { get; set; }
        public Nullable<System.DateTime> VendorDOB { get; set; }
        public string VendorEMail { get; set; }
        public string VendorMobileNo { get; set; }
        public string VendorPANCard { get; set; }
        public string VendorAadhaarCard { get; set; }
        public Nullable<decimal> VendorState { get; set; }
        public Nullable<decimal> VendorCity { get; set; }
        public string VendorPincode { get; set; }
        public string VendorLandMark { get; set; }
        public string VendorAddress1 { get; set; }
        public string VendorAddress2 { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string CompanyPANCard { get; set; }
        public string CompanyGSTIN { get; set; }
        public string CompanyEMail { get; set; }
        public string CompanyPhoneNo { get; set; }
        public string CompanyFAX { get; set; }
        public string CompanyTIN { get; set; }
        public string CompanyTAN { get; set; }
        public string CompanyRegisterdOffice { get; set; }
        public string CompanyHeadOffice { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<bool> IsVlid { get; set; }
        public string AccountNo { get; set; }
        public string IFSC_Code { get; set; }
        public string Beneficiary_Name { get; set; }
        public string BankBranchName { get; set; }
        public string GSTNO { get; set; }
        public Nullable<bool> ISAGREEMENTRECEIVEDWITHKYC { get; set; }
        public Nullable<bool> ISCANCELLEDCHEQUERECIVED { get; set; }
        public string VENDOR_CODE { get; set; }
        public string Vendor_Category { get; set; }
        public string Service_Type { get; set; }
        public Nullable<decimal> Base_Price { get; set; }
        public string KiloMeters { get; set; }
        public Nullable<decimal> Per_km_Price { get; set; }
        public string Service_Time { get; set; }
    
        public virtual tblMstCity tblMstCity { get; set; }
        public virtual tblMstState tblMstState { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblVendorBranchDetail> tblVendorBranchDetails { get; set; }
    }
}

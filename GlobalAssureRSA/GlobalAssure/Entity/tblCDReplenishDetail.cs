//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCDReplenishDetail
    {
        public decimal CDReplenishDetailID { get; set; }
        public Nullable<bool> IsVaild { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CDBelongTo { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<decimal> CDAmount { get; set; }
        public Nullable<decimal> CashAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual tblMstBank tblMstBank { get; set; }
        public virtual tblMstCommonType tblMstCommonType { get; set; }
        public virtual tblMstPayment tblMstPayment { get; set; }
    }
}

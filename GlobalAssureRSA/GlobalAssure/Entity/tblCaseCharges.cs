//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCaseCharges
    {
        public int Id { get; set; }
        public Nullable<decimal> AssistID { get; set; }
        public string userId { get; set; }
        public Nullable<decimal> TollCharges { get; set; }
        public Nullable<decimal> WaitingHoursCharge { get; set; }
        public Nullable<decimal> VehicleCustodyCharge { get; set; }
        public Nullable<decimal> OtherCharges { get; set; }
        public Nullable<System.DateTime> dt_Insert_Date { get; set; }
        public Nullable<System.DateTime> dt_Modify_Date { get; set; }
        public Nullable<decimal> TotalKiloMeters { get; set; }
        public Nullable<decimal> TotalKiloMetersCharge { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> FinalAmount { get; set; }
        public Nullable<decimal> CustomerPaidAmount { get; set; }
        public Nullable<System.DateTime> CustomerPaidDate { get; set; }
        public string ReferenceNo { get; set; }
    }
}

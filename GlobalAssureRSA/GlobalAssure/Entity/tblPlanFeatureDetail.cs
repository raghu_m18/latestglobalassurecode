//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPlanFeatureDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblPlanFeatureDetail()
        {
            this.tblPlanFeatureOfProducts = new HashSet<tblPlanFeatureOfProduct>();
        }
    
        public decimal PlanFeatureDetailID { get; set; }
        public string PlanFeature { get; set; }
        public string Details { get; set; }
        public string Assistance { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblPlanFeatureOfProduct> tblPlanFeatureOfProducts { get; set; }
    }
}

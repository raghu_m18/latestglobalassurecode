//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEMIProtectPremiumDetails
    {
        public int Id { get; set; }
        public Nullable<decimal> TransactionID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ProductTypeID { get; set; }
        public string UserID { get; set; }
        public string CertificateNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string VehicleType { get; set; }
        public string VehicleClass { get; set; }
        public string RegistrationNo { get; set; }
        public string ProductID { get; set; }
        public string MakeID { get; set; }
        public string ModelID { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string UploadedAmount { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public string CoverStartDate { get; set; }
        public string CoverEndDate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Pincode { get; set; }
        public string Landmark { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string InsuredGender { get; set; }
        public string InsuredDOB { get; set; }
        public string InsuredAge { get; set; }
        public string NomineeName { get; set; }
        public string NomineeGender { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeAge { get; set; }
        public string NomineeRelationship { get; set; }
        public string PaymentType { get; set; }
        public string GSTINNumber { get; set; }
        public string RSALoanAmount { get; set; }
        public string EMIAmount { get; set; }
        public string EMILoanAmount { get; set; }
        public string EMITenure { get; set; }
        public string EMIAccountNo { get; set; }
        public string EMIBankName { get; set; }
        public string EMIBankBranch { get; set; }
        public string Tenure { get; set; }
        public string ContractNo { get; set; }
    }
}

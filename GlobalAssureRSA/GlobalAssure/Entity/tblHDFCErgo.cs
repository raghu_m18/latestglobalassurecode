//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblHDFCErgo
    {
        public int Id { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string POLICY_NO { get; set; }
        public string FIRST_NAME { get; set; }
        public string SURNAME { get; set; }
        public string ADDRESS { get; set; }
        public string LOCALITY { get; set; }
        public string STATE { get; set; }
        public string VEHICLE_REG_NO { get; set; }
        public string CHASSIS_NUM { get; set; }
        public string CAR_MAKE { get; set; }
        public string MODEL { get; set; }
        public Nullable<System.DateTime> POLICY_START_DATE { get; set; }
        public Nullable<System.DateTime> POLICY_END_DATE { get; set; }
        public Nullable<System.DateTime> REGISTRATION_DATE { get; set; }
        public Nullable<bool> STATUS { get; set; }
        public Nullable<System.DateTime> dt_InserDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOTPProcess
    {
        public int UniqueID { get; set; }
        public Nullable<bool> Status { get; set; }
        public string Application { get; set; }
        public string MobileNo { get; set; }
        public Nullable<int> OTP { get; set; }
        public Nullable<System.DateTime> InsertDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
    }
}

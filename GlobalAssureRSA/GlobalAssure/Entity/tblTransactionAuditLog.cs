//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblTransactionAuditLog
    {
        public int AuditLogID { get; set; }
        public Nullable<decimal> TransactionID { get; set; }
        public string UserID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string Remark { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public string CertificateNo { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual tblMstWorkFlowStatu tblMstWorkFlowStatu { get; set; }
        public virtual tblTransaction tblTransaction { get; set; }
    }
}

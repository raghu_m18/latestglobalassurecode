//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblUniversalShampoo
    {
        public int ID { get; set; }
        public string CertificateNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> CertificateIssueDate { get; set; }
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        public string CustomerMobileNo { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentState { get; set; }
        public Nullable<bool> PolicyStatus { get; set; }
        public Nullable<System.DateTime> dt_Insert_Date { get; set; }
        public Nullable<System.DateTime> dt_Modify_Date { get; set; }
    }
}

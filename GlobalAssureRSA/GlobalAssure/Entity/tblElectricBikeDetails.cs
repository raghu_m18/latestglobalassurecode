//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GlobalAssure.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblElectricBikeDetails
    {
        public int ID { get; set; }
        public string FrameNo { get; set; }
        public string BatteryNo { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string PlanName { get; set; }
        public string Title { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredEmailID { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }
        public string Color { get; set; }
        public Nullable<bool> PolicyStatus { get; set; }
        public Nullable<System.DateTime> dt_Insert_Date { get; set; }
        public Nullable<System.DateTime> dt_Modify_Date { get; set; }
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GlobalAssure.Startup))]
namespace GlobalAssure
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

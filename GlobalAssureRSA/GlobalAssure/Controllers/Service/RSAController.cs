﻿using GlobalAssure.Controllers.API;
using GlobalAssure.Entity;
using GlobalAssure.Models;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;
using System.Web.Mvc;
using System.Configuration;
using System.Web;
using GlobalAssure.RelianceRSA;
namespace GlobalAssure.Controllers
{
    public class RSAController : ApiController
    {
        /// <summary>
        /// IndexId     Date            Developer        Desc
        /// 1001        06/12/2019      Bikash           Reliance Service Calling added to get data for create Assist Request   
        /// 1002        21-4-20         Arshad           Added Get Dealer Dashboard Method
        /// </summary>

        APILogic _logic = new APILogic();
        private GlobalAssureRSAEntities _db = new GlobalAssureRSAEntities();
        private APICertificateResponse res = new APICertificateResponse();
        private tblRequestResponse reqres = new tblRequestResponse();
        private APIGetVendorStatusResponse GetVendorStatusRes = new APIGetVendorStatusResponse();
        private APIGetVendorAssistResponse GetVendorAssistRes = new APIGetVendorAssistResponse();
        private APIUpdateVendorLatLongResponse UpdateVendorLatLongRes = new APIUpdateVendorLatLongResponse();
        private APISaveDocumentDataResponse SaveDocRes = new APISaveDocumentDataResponse();
        private APIGetDocumentDataResponse GetDocRes = new APIGetDocumentDataResponse();
        private APIGetCertificatePaymentStatusResponse PayStsRes = new APIGetCertificatePaymentStatusResponse();
        //
        private APIGetStoreNameForUserResponse GetStoreNameRes = new APIGetStoreNameForUserResponse();
        private APIPCGetQuoteResponse PCGetQuoteobj = new APIPCGetQuoteResponse();
        private APIPCCreateProposalResponse PCCreateProposalObj = new APIPCCreateProposalResponse();
        private APIPCGetPolicyCopyResponse PCGetPolicyCopyObj = new APIPCGetPolicyCopyResponse();
        private APIPCMakePaymentResponse PCMakePaymentObj = new APIPCMakePaymentResponse();
        private APIPCGetPolicyPendingPaymentResponse PCGetPolicyPendingPaymentObj = new APIPCGetPolicyPendingPaymentResponse();
        private APIPCMakeMultiplePaymentResponse PCMakeMultiplePaymentObj = new APIPCMakeMultiplePaymentResponse();
        private APIPCMISDetailsResponse PCMISDetailsObj = new APIPCMISDetailsResponse();
        private APIAgentProfileResponse PCGetAgentProfileObj = new APIAgentProfileResponse();
        private APIGetPCRelationshipResponse PCRelationshipObj = new APIGetPCRelationshipResponse();
        private APIGetPCCertificateDataResponse PCCertificateDataObj = new APIGetPCCertificateDataResponse();
        private APIGetPCWalletDetailsResponse PCWalletDetailsObj = new APIGetPCWalletDetailsResponse();

        private APIDealerDashboardResponse DealerDashboardobj = new APIDealerDashboardResponse();
        private APIPCUpdateDealerProfileResponse PCGetDealerProfileObj = new APIPCUpdateDealerProfileResponse();
        private APIMakeMultiplePaymentResponse MakeMultiplePaymentObj = new APIMakeMultiplePaymentResponse();
        private APISendOTPResponse SendOTPResponseObj = new APISendOTPResponse();
        private APIValidateOTPResponse ValidateOTPResponseObj = new APIValidateOTPResponse();
        private APIGetStateMasterResponse GetStateMasterResponseObj = new APIGetStateMasterResponse();
        private APIGetCityMasterResponse GetCityMasterResponseObj = new APIGetCityMasterResponse();
        private APIGetReliancePolicyStatusResponse GetReliancePolicyStatusObj = new APIGetReliancePolicyStatusResponse();
        private APIGetVendorLatLongResponse GetVendorLatLongResponseObj = new APIGetVendorLatLongResponse();
        private APIVendorAcceptRejectAssistanceResponse VendorAcceptRejectAssistanceResponseObj = new APIVendorAcceptRejectAssistanceResponse();
        private APIGetPCMakeMasterResponse GetPCMakeMasterResponseObj = new APIGetPCMakeMasterResponse();
        private APIGetPCModelMasterResponse GetPCModelMasterResponseObj = new APIGetPCModelMasterResponse();
        private APIGetMakeMasterResponse GetMakeMasterResponseObj = new APIGetMakeMasterResponse();
        private APIGetModelMasterResponse GetModelMasterResponseObj = new APIGetModelMasterResponse();
        private APIGetDealerPolicyCountResponse GetDealerPolicyCountResponseObj = new APIGetDealerPolicyCountResponse();
        private APIIssueCovidCertificateResponse IssueCovidCertificateResponseObj = new APIIssueCovidCertificateResponse();        
		private APIInitClaimSearchResponse InitClaimSearchobj = new APIInitClaimSearchResponse();
		private APISaveCertificateResponse SaveCertificateResponseObj = new APISaveCertificateResponse();
        private PCGetPolicyDetailsResponse GetPolicyDetailsResponseObj = new PCGetPolicyDetailsResponse();
        private PCClaimIntimationResponse ClaimIntimationResponseObj = new PCClaimIntimationResponse();
        private PCGetClaimDetailsResponse GetClaimDetailsResponseObj = new PCGetClaimDetailsResponse();
        private APICustomerIssueCertificateResponse CustomerIssueCertificateResponseObj = new APICustomerIssueCertificateResponse();
        private APIGetCertificateCopyResponse GetCertificateCopyResponseObj = new APIGetCertificateCopyResponse();
        private APIAssistRequestGenerationResponse AssistRequestGenerationResponseObj = new APIAssistRequestGenerationResponse();
        private APIGenericIssueCertificateResponse GenericIssueCertificateResponseObj = new APIGenericIssueCertificateResponse();
        private APIGenericIssueCertificateWalletResponse GenericIssueCertificateWalletResponseObj = new APIGenericIssueCertificateWalletResponse();
        private APIGenericIssueCertificateV1Response GenericIssueCertificateV1ResponseObj = new APIGenericIssueCertificateV1Response();
        private APIGetCertificatePaymentStatusV1Response PayStsResV1 = new APIGetCertificatePaymentStatusV1Response();
        private APIGetCertificateCopyV1Response GetCertificateCopyV1ResponseObj = new APIGetCertificateCopyV1Response();
        private PartnerAssistRequestResponse PartnerAssistRequestResponseObj = new PartnerAssistRequestResponse();
        private APIPCExternalLinkGenerationResponse PCExternalLinkGenerationObj = new APIPCExternalLinkGenerationResponse();
        private APIGetVendorAcceptedResponse GetVendorAcceptedRes = new APIGetVendorAcceptedResponse();
        private APIGetVendorRejectedResponse GetVendorRejectedRes = new APIGetVendorRejectedResponse();
        private APIGetVendorCompletedResponse GetVendorCompletedRes = new APIGetVendorCompletedResponse();
        private APIAssistRequestCompletionResponse AssistRequestCompletionResponseObj = new APIAssistRequestCompletionResponse();
        private APIAssistLoctionDetailsResponse AssistLoctionDetailsResponseObj = new APIAssistLoctionDetailsResponse();
        private APIGetPCTieUpCompanyMasterResponse GetPCTieUpCompanyMasterResponseObj = new APIGetPCTieUpCompanyMasterResponse();
        private APIGetPCTieUpCompanyAddOnResponse GetPCTieUpCompanyAddOnResponseObj = new APIGetPCTieUpCompanyAddOnResponse();
        private APIPCCreateProposalMultipleProductResponse PCCreateProposalMultipleProductResponseObj = new APIPCCreateProposalMultipleProductResponse();


        #region Login
        [System.Web.Http.HttpPost]
        //public LoginResponse Login(LoginInput input)
        //{
        //    LoginResponse resp = new LoginResponse();
        //    try
        //    {
        //       // bool valid = ValidateUser(input.UserID, input.Password);
        //        if (valid)
        //        {
        //            resp.ErrorCode = "0";
        //            resp.ErrorMessage = "";
        //            resp.UserName = input.UserID;
        //            resp.Token = _logic.GenerateToken(input.UserID);
        //        }
        //        else
        //        {
        //            resp.ErrorCode = "-1";
        //            resp.ErrorMessage = "Invalid User/Password or user not Authorized.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        resp.ErrorCode = "-1";
        //        resp.ErrorMessage = ex.Message;
        //    }
        //    return resp;
        //}
        #endregion

        #region IssueCertificate

        public IssueCertificateResponse IssueCertificate(CertificateDataRequest data)
        {
            IssueCertificateResponse objResponse = new IssueCertificateResponse();
            string ErrorMessage;
            bool valid = true;

            if (data == null)
            {
                objResponse.ErrorCode = "-99";
                objResponse.ErrorMessage = "Invalid Request.";
                return objResponse;
            }

            tblTransaction model = _logic.ValidateCertificateRequest(data, out ErrorMessage, ref valid);
            if (valid)
            {
                IssueCertificateResponse result = _logic.SaveIssueCertificate(model, data);
                objResponse.CertificateNo = result.CertificateNo;
            }
            else
            {

                objResponse.ErrorCode = "-99";
                objResponse.ErrorCode = ErrorMessage;
            }
            return objResponse;
        }
        #endregion

        #region NewAssistRequest
        [System.Web.Http.HttpPost]
        public NewAssistResponse NewAssistRequest(NewAssistRequest data)
        {


            //// NewAssistRequest
            NewAssistResponse objResponse = new NewAssistResponse();
            if (data == null)
            {
                objResponse.ErrorCode = "-99";
                objResponse.ErrorMessage = "Invalid Request.";
                return objResponse;
            }


            string UserID = ConfigurationManager.AppSettings["LandMarkUser"];
            if (!string.IsNullOrEmpty(data.UserId))
            {
                if (UserID != data.UserId)
                {
                    objResponse.ErrorCode = "-99";
                    objResponse.ErrorMessage = "Invalid user.";
                    return objResponse;
                }
            }
            else
            {
                objResponse.ErrorCode = "-99";
                objResponse.ErrorMessage = "Invalid Request.";
                return objResponse;

            }



            string errorMessage;
            bool validFlag = true;
            string userName = data.UserId;
            bool isManual = false;
            decimal branchId = (from u in _db.tblVendorBranchDetails where u.BranchName == data.Vendor select u.BranchID).FirstOrDefault();

            if (branchId == 0)
            {
                data.ManualAllocation = isManual;

            }
            else
            {
                data.ManualAllocation = true;
            }

            tblAssistDetail getResult = _logic.ValidateDataNewAssist(data, branchId, out errorMessage, ref validFlag);

            data.TransactionId = getResult.TransactionId;
            data.IncidentDetailsId = getResult.IncidentDetailid;

            if (validFlag)

            {
                var result = _logic.SaveNewAssistant(data, data.ManualAllocation, branchId, userName);

                if (result.ReturnValue == "SaveSuccess")
                {

                    objResponse.AssistRefNo = result.ReffeNo;

                }
                else if (result.ReturnValue == "AssistExist")
                {
                    objResponse.ErrorCode = "";
                    objResponse.ErrorCode = "Data already exist";
                }

            }
            else

            {

                objResponse.ErrorCode = "";

                objResponse.ErrorCode = errorMessage;

            }

            return objResponse;


            //return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);



        }


        #endregion

        #region GetAssistDetails
        [System.Web.Http.HttpPost]
        public getAssistResponse GetAssistDetails(GetAssistRequest data)
        {

            getAssistResponse getAssistanceResponse = new getAssistResponse();
            IEnumerable<GetAssistDetailsValue> searchResult = new List<GetAssistDetailsValue>();
            string errorMessage;
            bool validFlag = true;

            if (data != null)
            {
                if (string.IsNullOrEmpty(data.UserId))
                {
                    getAssistanceResponse.ErrorCode = "-99";
                    getAssistanceResponse.ErrorMessage = "invalid User";
                    return getAssistanceResponse;
                }
                else
                {
                    string UserID = ConfigurationManager.AppSettings["LandMarkUser"];
                    if (UserID != data.UserId)
                    {
                        getAssistanceResponse.ErrorCode = "-99";
                        getAssistanceResponse.ErrorMessage = "invalid User";
                        return getAssistanceResponse;
                    }
                }

            }
            else
            {
                getAssistanceResponse.ErrorCode = "-99";
                getAssistanceResponse.ErrorMessage = "Inavalid request";
                return getAssistanceResponse;
            }

            //Get assist details data
            searchResult = _logic.GetValidateData(data, out errorMessage, ref validFlag);


            if (validFlag)
            {
                getAssistanceResponse.getAssistResponseList = searchResult;
                getAssistanceResponse.ErrorCode = null;
                getAssistanceResponse.ErrorMessage = null;
                return getAssistanceResponse;

            }
            else
            {

                getAssistanceResponse.getAssistResponseList = searchResult;
                getAssistanceResponse.ErrorCode = "-99";
                getAssistanceResponse.ErrorMessage = errorMessage;
                return getAssistanceResponse;
            }


            //End
        }
        #endregion

        #region GetCertificate
        [System.Web.Http.HttpPost]
        public getCertificateResponse GetCertificateDetails(GetCertificateRequest data)
        {
            getCertificateResponse objResponse = new getCertificateResponse();
            if (data != null)
            {
                if (string.IsNullOrEmpty(data.UserId))
                {
                    objResponse.ErrorCode = "-99";
                    objResponse.ErrorMessage = "invalid User";
                    return objResponse;
                }
                else
                {
                    string UserID = ConfigurationManager.AppSettings["LandMarkUser"];
                    if (UserID != data.UserId)
                    {
                        objResponse.ErrorCode = "-99";
                        objResponse.ErrorMessage = "invalid User";
                        return objResponse;
                    }
                }

            }
            else
            {
                objResponse.ErrorCode = "-99";
                objResponse.ErrorMessage = "Inavalid request";
                return objResponse;
            }

            objResponse = _logic.GetCertificateDetails(data);


            return objResponse;
        }
        #endregion

        #region MobileAPP

        #region UserRegistration
        public MobileUserRegistrationResponse MobilUserRegistration(MobilUserRegistration data)
        {
            MobileUserRegistrationResponse response = new MobileUserRegistrationResponse();
            try
            {
                if (data != null)
                {
                    if (string.IsNullOrEmpty(data.Email) || string.IsNullOrEmpty(data.MobileNo) || string.IsNullOrEmpty(data.UserName)
                        || string.IsNullOrEmpty(data.Password))
                    {
                        response.ErrorCode = "-99";
                        response.ErrorMessage = "Invalid request";
                    }
                    else
                    {
                        bool CertificateExist = _logic.ValidateUserRegistration(data);
                        if (CertificateExist)
                        {
                            bool UserNameExist = _logic.ValidateUserName(data.UserName);
                            if (!UserNameExist)
                            {
                                _logic.SaveUserRegistration(data);
                                response.status = true;
                            }
                            else
                            {
                                response.ErrorCode = "-99";
                                response.ErrorMessage = " UserName already exists";
                            }
                        }
                        else
                        {
                            response.ErrorCode = "";
                            response.ErrorMessage = "Certificate details does not exist";
                        }

                    }
                }
                else
                {
                    response.ErrorCode = "99";
                    response.ErrorMessage = "Invalid request";
                }
            }
            catch (Exception e)
            {
                response.ErrorCode = "-1";
                response.ErrorMessage = "Exception";
                HandleException(e);
            }

            return response;
        }
        #endregion

        #region MobileLogin
        public MobileLoginResponse MobileLogin(LoginInput data)
        {
            MobileLoginResponse objResponse = new MobileLoginResponse();
            try
            {
                if (data != null)
                {
                    if (string.IsNullOrEmpty(data.UserID) || string.IsNullOrEmpty(data.Password))
                    {
                        objResponse.ErrorCode = "-99";
                        objResponse.ErrorMessage = "Invalid request";
                    }
                    else
                    {
                        data.UserID = data.UserID.Trim();
                        data.Password = data.Password.Trim();

                        var result = _logic.ValidateMobileUser(data);
                        if (result)
                        {
                            string Token = _logic.GenerateToken(data.UserID);
                            objResponse.Token = Token;
                            objResponse.userId = data.UserID;
                        }
                        else
                        {
                            objResponse.ErrorCode = "-99";
                            objResponse.ErrorMessage = "Invalid Userid or Password";
                        }

                    }

                }
            }
            catch (Exception e)
            {
                objResponse.ErrorCode = "-1";
                objResponse.ErrorMessage = "Exception";
                HandleException(e);
            }

            return objResponse;
        }
        #endregion

        #region RestPasswordStatus
        [System.Web.Http.HttpPost]
        public ResetPasswordOut ResetPassword(ResetPassword resetPassword)
        {
            ResetPasswordOut resetPasswordObj = new ResetPasswordOut();
            string errorMessage;
            bool validFlag = true;

            ResetPassword resetPasswordObjVal = _logic.ValidateResetPassword(resetPassword, out errorMessage, ref validFlag);

            if (validFlag)
            {

                var updateResult = _logic.UpdatePassword(resetPassword.NewPassword, resetPassword.UserId, resetPassword.MobileNumber, resetPassword.Email);


                if (updateResult.ReturnValueValidation == "Password changed successfully")
                {
                    resetPasswordObj.Status = true;
                    resetPasswordObj.ErrorCode = "";
                    resetPasswordObj.ErrorMessage = "";

                }
                else if (updateResult.ReturnValueValidation == "Change password not updated")
                {
                    resetPasswordObj.Status = false;
                    resetPasswordObj.ErrorCode = "";
                    resetPasswordObj.ErrorMessage = updateResult.ReturnValueValidation;
                }

            }
            else

            {

                resetPasswordObj.ErrorCode = "";
                resetPasswordObj.Status = false;
                resetPasswordObj.ErrorMessage = errorMessage;

            }


            return resetPasswordObj;


        }


        #endregion

        #region NewAssist
        [System.Web.Http.HttpPost]
        public NewAssistResponse NewAssist(NewAssistRequest data)
        {
            int VendorClosedStsId = 10;
            data.RegistrationNo = null;
            data.Vendor = null;
            AspNetUser userdata = new AspNetUser();
            AspNetUser userdatacreatedby = new AspNetUser();
            tblMobileAppUser muserdata = new tblMobileAppUser();

            string Username = ConfigurationManager.AppSettings["GA_AppAssistUser"];
            userdata = _db.AspNetUsers.AsNoTracking().Where(w => w.Email == Username).FirstOrDefault();
            if (userdata != null)
            {
                data.UserId = Username;
            }

            //// NewAssistRequest
            NewAssistResponse objResponse = new NewAssistResponse();
            if (data == null)
            {
                objResponse.ErrorCode = "-99";
                objResponse.ErrorMessage = "Invalid Request.";
                return objResponse;
            }

            if (string.IsNullOrEmpty(data.MUserID))
            {
                objResponse.ErrorMessage = "MUserID is Mandatory !!";
                objResponse.status = "Failed";
                return objResponse;
            }
            else
            {
                string MUserID = data.MUserID.Trim();

                int UserInTableCheck = _db.AspNetUsers.Where(w => w.Email == MUserID && w.IsActive != false).Count();
                if (UserInTableCheck > 0)
                {
                    userdatacreatedby = _db.AspNetUsers.Where(w => w.Email == MUserID && w.IsActive != false).FirstOrDefault();
                    if (userdatacreatedby != null)
                    {
                        if (userdatacreatedby.Id != null)
                        {
                            data.UID = userdatacreatedby.Id;

                            tblAssistDetail tblAD = new tblAssistDetail();
                            int AD = _db.tblAssistDetails.Where(w => w.CreatedBy == data.UID).Count();
                            if (AD > 0)
                            {
                                tblAD = _db.tblAssistDetails.Where(w => w.CreatedBy == data.UID).OrderByDescending(o => o.CreatedDate).FirstOrDefault();
                                if (tblAD != null)
                                {
                                    if (!tblAD.StatusId.Equals(VendorClosedStsId))
                                    {
                                        objResponse.ErrorMessage = "Previous Assist Request Already Open !!";
                                        objResponse.status = "Failed";

                                        return objResponse;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        objResponse.ErrorMessage = "MUserID Not found !!";
                        objResponse.status = "Failed";
                        return objResponse;
                    }
                }
                else
                {
                    muserdata = _db.tblMobileAppUsers.Where(w => w.Email == MUserID && w.isValid != false).FirstOrDefault();
                    if (muserdata != null)
                    {
                        if (muserdata.id > 0)
                        {
                            data.UID = Convert.ToString(muserdata.id);

                            tblAssistDetail tblAD = new tblAssistDetail();
                            int AD = _db.tblAssistDetails.Where(w => w.CreatedBy == data.UID).Count();
                            if (AD > 0)
                            {
                                tblAD = _db.tblAssistDetails.Where(w => w.CreatedBy == data.UID).OrderByDescending(o => o.CreatedDate).FirstOrDefault();
                                if (tblAD != null)
                                {
                                    if (!tblAD.StatusId.Equals(VendorClosedStsId))
                                    {
                                        objResponse.ErrorMessage = "Previous Assist Request Already Open !!";
                                        objResponse.status = "Failed";

                                        return objResponse;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        objResponse.ErrorMessage = "MUserID Not found !!";
                        objResponse.status = "Failed";
                        return objResponse;
                    }
                }
            }


            if (!string.IsNullOrEmpty(data.Token) || !string.IsNullOrEmpty(data.UserId))
            {
                //if (!ValidateToken(data.Token))
                //{
                //    objResponse.ErrorCode = "-99";
                //    objResponse.ErrorMessage = "Invalid token.";
                //    return objResponse;
                //}
            }
            else
            {
                objResponse.ErrorCode = "-99";
                objResponse.ErrorMessage = "Invalid Request.";
                return objResponse;
            }

            string errorMessage;
            bool validFlag = true;
            string userName = data.UserId;
            //bool isManual = false;
            decimal branchId = (from u in _db.tblVendorBranchDetails where u.BranchName == data.Vendor select u.BranchID).FirstOrDefault();

            if (branchId == 0)
            {
                data.ManualAllocation = false;
            }
            else
            {
                data.ManualAllocation = true;
            }

            tblAssistDetail getResult = _logic.ValidateDataNewAssist(data, branchId, out errorMessage, ref validFlag);

            data.TransactionId = getResult.TransactionId;
            data.IncidentDetailsId = getResult.IncidentDetailid;

            if (validFlag)
            {
                var result = _logic.SaveNewAssistant(data, data.ManualAllocation, branchId, userName);

                if (result.ReturnValue == "SaveSuccess")
                {
                    objResponse.AssistRefNo = result.ReffeNo;
                    objResponse.status = "Success";
                }
                else if (result.ReturnValue == "AssistExist")
                {
                    objResponse.ErrorCode = "";
                    objResponse.ErrorCode = "Data already exist";
                }
            }
            else
            {
                objResponse.ErrorCode = "";
                objResponse.ErrorCode = errorMessage;
            }

            return objResponse;
        }


        #endregion

        #region AssistStatus

        [System.Web.Http.HttpPost]
        public AssistStatusData AssistStatus(AssistStatusRequest StatusRequest)
        {

            AssistStatusData assistStatusData = new AssistStatusData();
            //IEnumerable<AssistStatusData> searchResult = new List<AssistStatusData>();
            string errorMessage;
            bool validFlag = true;

            if (StatusRequest == null)
            {
                assistStatusData.ErrorCode = "-99";
                assistStatusData.ErrorMessage = "Invalid Request.";
                return assistStatusData;
            }
            else if (string.IsNullOrEmpty(StatusRequest.Token))
            {
                assistStatusData.ErrorCode = "-99";
                assistStatusData.ErrorMessage = "Invalid Request.";
                return assistStatusData;
                //if (!ValidateToken(Token))
                //{
                //    assistStatusData.ErrorCode = "-99";
                //    assistStatusData.ErrorMessage = "Invalid token.";
                //    return assistStatusData;
                //}
            }
            //else
            //{
            //    assistStatusData.ErrorCode = "-99";
            //    assistStatusData.ErrorMessage = "Invalid Request.";
            //    return assistStatusData;
            //}


            AssistStatusData searchResult = _logic.GetValidateAssistStatus(StatusRequest.UserId, out errorMessage, ref validFlag);

            if (validFlag)
            {
                assistStatusData.LandMark = searchResult.LandMark;
                assistStatusData.VendorName = searchResult.VendorName;
                assistStatusData.VendorBranch = searchResult.VendorBranch;
                assistStatusData.PinCode = searchResult.PinCode;
                assistStatusData.AssistStatus = searchResult.AssistStatus;
                assistStatusData.ErrorCode = "";
                assistStatusData.ErrorMessage = errorMessage;
                return assistStatusData;
            }
            else
            {
                assistStatusData.ErrorCode = "";
                assistStatusData.ErrorMessage = errorMessage;
                return assistStatusData;
            }
        }

        #endregion

        #endregion

        #region SaveReliancePolicy
        public NewAssistResponse SaveReliancePolicy(RelianceAssistRequest data)
        {

            //sa 1001
            #region GetDatafromReliance Service for Saving
            string UserID = System.Configuration.ConfigurationManager.AppSettings["Userid"];
            string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];
            try
            {
                string HTML = string.Empty;
                string message = string.Empty;
                string status = string.Empty;
                ReliancePolicyModel model = new ReliancePolicyModel();
                RGICL_Policy_Service_RSAClient client = new RGICL_Policy_Service_RSAClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 1, 0); // Timeout 1 min

                string Param = string.Empty;

                if (!string.IsNullOrEmpty(data.RegistrationNo))
                {
                    Param = data.RegistrationNo;
                }
                else if (!string.IsNullOrEmpty(data.PolicyNumber))
                {
                    Param = data.PolicyNumber;
                }

                var Response = client.GetPolicyDetailforRSA(UserID, Password, Param.Trim());
                if (Response != null)
                {


                    //  model.ChasisNo = Response.ChassisNumber;
                    data.EngineNo = Response.EngineNumber;
                    data.InsuredName = Response.InsuredName;
                    data.Make = Response.Make;
                    data.Model = Response.Model;
                    if (string.IsNullOrEmpty(data.VehicleNo))
                    {
                        data.VehicleNo = data.RegistrationNo;
                    }

                    // model.PolicyNumber = Response.PolicyNumber;
                    data.PolicyStatus = Response.PolicyStatus;
                    data.RiskStartDate = Response.RiskStartDate;
                    data.RiskEndDate = Response.RiskEndDate;
                    //  model.VehicleNo = Response.VehicleNumber;
                    data.ResponseStatus = Response.ResponseStatus;
                    data.RSAVendorName = Response.InsuredName;
                    data.ProductCode = Response.ProductCode;
                    data.InsuredphoneNumber = Response.InsuredphoneNumber;
                    //data.InsuredMobileNumber = Response.InsuredMobileNumber;
                    data.InsuredEmailID = Response.InsuredEmailID;
                    data.InsuredAddressLine = Response.InsuredAddressLine;
                    data.InsuredCityName = Response.InsuredCityName;
                    data.InsuredDistrictName = Response.InsuredDistrictName;
                }
                else
                {
                    status = CrossCutting_Constant.Failure;
                    message = "Sorry! Something went wrong. Please try again!";
                }
                client.Close();
            }

            catch (Exception e)
            {
                HandleException(e);
            }
            #endregion
            //ea 1001
            NewAssistResponse res = new NewAssistResponse();
            res = _logic.SaveReliancePolicy(data);


            return res;
        }

        #endregion


        #region APIIssueCertificate
        public APICertificateResponse APIIssueCertificate(APICertificateDataRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APICertificateResponse res = new APICertificateResponse();
                    res = _logic.APIIssueCertificate(data);

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(data.TransactionID);
                    reqres.Response = JsonConvert.SerializeObject(res);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //
                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    res.TransId = 0;
                    res.ErrorMessage = "Internal Server Error";
                    res.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    res.status = "Failed";

                    // Request Insert in Database
                    reqres.TransactionId = res.TransId;
                    reqres.Request = JsonConvert.SerializeObject(data);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    // Response Insert in Database
                    reqres.TransactionId = res.TransId;
                    reqres.Response = JsonConvert.SerializeObject(res);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    return res;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                res.TransId = Convert.ToInt64(data.TransactionID);
                res.ErrorMessage = "Internal Server Error";
                res.ErrorMessageDetail = null;
                res.status = "Failed";
                return res;
            }
        }

        #endregion

        #region APICustomerIssueCertificate
        public APICustomerIssueCertificateResponse APICustomerIssueCertificate(APICustomerIssueCertificateRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APICustomerIssueCertificateResponse CustomerIssueCertificateResponseObj = new APICustomerIssueCertificateResponse();
                    CustomerIssueCertificateResponseObj = _logic.APICustomerIssueCertificate(data);

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(data.TransactionID);
                    reqres.Response = JsonConvert.SerializeObject(CustomerIssueCertificateResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //
                    CustomerIssueCertificateResponseObj.ErrorMessageDetail = null;

                    return CustomerIssueCertificateResponseObj;
                }
                else
                {
                    CustomerIssueCertificateResponseObj.TransId = 0;
                    CustomerIssueCertificateResponseObj.ErrorMessage = "Internal Server Error";
                    CustomerIssueCertificateResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    CustomerIssueCertificateResponseObj.status = "Failed";

                    // Request Insert in Database
                    reqres.TransactionId = CustomerIssueCertificateResponseObj.TransId;
                    reqres.Request = JsonConvert.SerializeObject(data);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    // Response Insert in Database
                    reqres.TransactionId = CustomerIssueCertificateResponseObj.TransId;
                    reqres.Response = JsonConvert.SerializeObject(CustomerIssueCertificateResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    return CustomerIssueCertificateResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                
                CustomerIssueCertificateResponseObj.TransId = Convert.ToInt64(data.TransactionID);
                CustomerIssueCertificateResponseObj.ErrorMessage = "Internal Server Error";
                CustomerIssueCertificateResponseObj.ErrorMessageDetail = null;
                CustomerIssueCertificateResponseObj.status = "Failed";

                return CustomerIssueCertificateResponseObj;
            }
        }

        #endregion

        #region APIGetCertificateCopy
        public APIGetCertificateCopyResponse APIGetCertificateCopy(APIGetCertificateCopyRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetCertificateCopyResponse res = new APIGetCertificateCopyResponse();                   

                    res = _logic.APIGetCertificateCopy(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetCertificateCopyResponseObj.TransactionID = null;
                    GetCertificateCopyResponseObj.ErrorMessage = "Internal Server Error";
                    GetCertificateCopyResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetCertificateCopyResponseObj.status = "Failed";                   

                    return GetCertificateCopyResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);

                GetCertificateCopyResponseObj.TransactionID = data.TransactionID;
                GetCertificateCopyResponseObj.ErrorMessage = "Internal Server Error";
                GetCertificateCopyResponseObj.ErrorMessageDetail = null;
                GetCertificateCopyResponseObj.status = "Failed";

                return GetCertificateCopyResponseObj;
            }
        }

        #endregion

        #region APIGenericIssueCertificate
        public APIGenericIssueCertificateResponse APIGenericIssueCertificate(APIGenericIssueCertificateRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGenericIssueCertificateResponse GenericIssueCertificateResponseObj = new APIGenericIssueCertificateResponse();
                    GenericIssueCertificateResponseObj = _logic.APIGenericIssueCertificate(data);

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(data.TransactionID);
                    reqres.Response = JsonConvert.SerializeObject(GenericIssueCertificateResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //
                    GenericIssueCertificateResponseObj.ErrorMessageDetail = null;

                    return GenericIssueCertificateResponseObj;
                }
                else
                {
                    GenericIssueCertificateResponseObj.TransId = 0;
                    GenericIssueCertificateResponseObj.ErrorMessage = "Internal Server Error";
                    GenericIssueCertificateResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    GenericIssueCertificateResponseObj.status = "Failed";

                    // Request Insert in Database
                    reqres.TransactionId = GenericIssueCertificateResponseObj.TransId;
                    reqres.Request = JsonConvert.SerializeObject(data);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    // Response Insert in Database
                    reqres.TransactionId = GenericIssueCertificateResponseObj.TransId;
                    reqres.Response = JsonConvert.SerializeObject(GenericIssueCertificateResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    return GenericIssueCertificateResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);

                GenericIssueCertificateResponseObj.TransId = Convert.ToInt64(data.TransactionID);
                GenericIssueCertificateResponseObj.ErrorMessage = "Internal Server Error";
                GenericIssueCertificateResponseObj.ErrorMessageDetail = null;
                GenericIssueCertificateResponseObj.status = "Failed";

                return GenericIssueCertificateResponseObj;
            }
        }

        #endregion

        #region APIGenericIssueCertificateWallet
        public APIGenericIssueCertificateWalletResponse APIGenericIssueCertificateWallet(APIGenericIssueCertificateWalletRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGenericIssueCertificateWalletResponse GenericIssueCertificateWalletResponseObj = new APIGenericIssueCertificateWalletResponse();
                    GenericIssueCertificateWalletResponseObj = _logic.APIGenericIssueCertificateWallet(data);

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(data.TransactionID);
                    reqres.Response = JsonConvert.SerializeObject(GenericIssueCertificateWalletResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //
                    GenericIssueCertificateWalletResponseObj.ErrorMessageDetail = null;

                    return GenericIssueCertificateWalletResponseObj;
                }
                else
                {
                    GenericIssueCertificateWalletResponseObj.TransId = 0;
                    GenericIssueCertificateWalletResponseObj.ErrorMessage = "Internal Server Error";
                    GenericIssueCertificateWalletResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    GenericIssueCertificateWalletResponseObj.status = "Failed";

                    // Request Insert in Database
                    reqres.TransactionId = GenericIssueCertificateWalletResponseObj.TransId;
                    reqres.Request = JsonConvert.SerializeObject(data);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    // Response Insert in Database
                    reqres.TransactionId = GenericIssueCertificateWalletResponseObj.TransId;
                    reqres.Response = JsonConvert.SerializeObject(GenericIssueCertificateWalletResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    return GenericIssueCertificateWalletResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);

                GenericIssueCertificateWalletResponseObj.TransId = Convert.ToInt64(data.TransactionID);
                GenericIssueCertificateWalletResponseObj.ErrorMessage = "Internal Server Error";
                GenericIssueCertificateWalletResponseObj.ErrorMessageDetail = null;
                GenericIssueCertificateWalletResponseObj.status = "Failed";

                return GenericIssueCertificateWalletResponseObj;
            }
        }

        #endregion

        #region APIGetCertificatePaymentStatus
        public APIGetCertificatePaymentStatusResponse APIGetCertificatePaymentStatus(APIGetCertificatePaymentStatusRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetCertificatePaymentStatusResponse PayStsRes = new APIGetCertificatePaymentStatusResponse();
                    PayStsRes = _logic.APIGetCertificatePaymentStatus(data);

                    PayStsRes.ErrorMessageDetail = null;

                    return PayStsRes;
                }
                else
                {
                    res.ErrorMessage = "Internal Server Error";
                    res.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    res.status = "Failed";

                    return PayStsRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                res.ErrorMessage = "Internal Server Error";
                res.ErrorMessageDetail = null;
                res.status = "Failed";
                return PayStsRes;
            }
        }

        #endregion

        #region APIGenericIssueCertificateV1
        public APIGenericIssueCertificateV1Response APIGenericIssueCertificateV1(APIGenericIssueCertificateV1Request data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGenericIssueCertificateV1Response GenericIssueCertificateV1ResponseObj = new APIGenericIssueCertificateV1Response();
                    GenericIssueCertificateV1ResponseObj = _logic.APIGenericIssueCertificateV1(data);

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(data.TransactionID);
                    reqres.Response = JsonConvert.SerializeObject(GenericIssueCertificateV1ResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //
                    GenericIssueCertificateV1ResponseObj.ErrorMessageDetail = null;

                    //// Error Log Code Start
                    if (GenericIssueCertificateV1ResponseObj.status.ToLower().Equals("failed"))
                    {
                        tblAPITransactionErrorLog tblAPITransactionErrorLog = new tblAPITransactionErrorLog();
                        tblAPITransactionErrorLog.TransactionID = data.TransactionID;
                        tblAPITransactionErrorLog.ErrorMessage = GenericIssueCertificateV1ResponseObj.ErrorMessage;
                        tblAPITransactionErrorLog.Status = "Failed";
                        tblAPITransactionErrorLog.InsertDate = DateTime.Now;
                        _db.tblAPITransactionErrorLog.Add(tblAPITransactionErrorLog);
                        _db.SaveChanges();
                    }
                    //// Error Log Code End

                    return GenericIssueCertificateV1ResponseObj;
                }
                else
                {
                    GenericIssueCertificateV1ResponseObj.TransId = 0;
                    GenericIssueCertificateV1ResponseObj.ErrorMessage = "Internal Server Error";
                    GenericIssueCertificateV1ResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    GenericIssueCertificateV1ResponseObj.status = "Failed";

                    // Request Insert in Database
                    reqres.TransactionId = GenericIssueCertificateV1ResponseObj.TransId;
                    reqres.Request = JsonConvert.SerializeObject(data);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    // Response Insert in Database
                    reqres.TransactionId = GenericIssueCertificateV1ResponseObj.TransId;
                    reqres.Response = JsonConvert.SerializeObject(GenericIssueCertificateV1ResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    return GenericIssueCertificateV1ResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);

                GenericIssueCertificateV1ResponseObj.TransId = Convert.ToInt64(data.TransactionID);
                GenericIssueCertificateV1ResponseObj.ErrorMessage = "Internal Server Error";
                GenericIssueCertificateV1ResponseObj.ErrorMessageDetail = null;
                GenericIssueCertificateV1ResponseObj.status = "Failed";

                //// Error Log Code Start
                if (GenericIssueCertificateV1ResponseObj.status.ToLower().Equals("failed"))
                {
                    tblAPITransactionErrorLog tblAPITransactionErrorLog = new tblAPITransactionErrorLog();
                    tblAPITransactionErrorLog.TransactionID = data.TransactionID;
                    tblAPITransactionErrorLog.ErrorMessage = GenericIssueCertificateV1ResponseObj.ErrorMessage;
                    tblAPITransactionErrorLog.Status = "Failed";
                    tblAPITransactionErrorLog.InsertDate = DateTime.Now;
                    _db.tblAPITransactionErrorLog.Add(tblAPITransactionErrorLog);
                    _db.SaveChanges();
                }
                //// Error Log Code End

                return GenericIssueCertificateV1ResponseObj;
            }
        }

        #endregion

        #region APIGetCertificatePaymentStatusV1
        public APIGetCertificatePaymentStatusV1Response APIGetCertificatePaymentStatusV1(APIGetCertificatePaymentStatusV1Request data)
        {
            try
            {
                if (data != null)
                {
                    APIGetCertificatePaymentStatusV1Response PayStsRes = new APIGetCertificatePaymentStatusV1Response();
                    PayStsRes = _logic.APIGetCertificatePaymentStatusV1(data);

                    PayStsRes.ErrorMessageDetail = null;

                    return PayStsRes;
                }
                else
                {
                    PayStsResV1.ErrorMessage = "Internal Server Error";
                    PayStsResV1.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    PayStsResV1.status = "Failed";
                    return PayStsResV1;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PayStsResV1.ErrorMessage = "Internal Server Error";
                PayStsResV1.ErrorMessageDetail = null;
                PayStsResV1.status = "Failed";
                return PayStsResV1;
            }
        }

        #endregion

        #region APIGetCertificateCopyV1
        public APIGetCertificateCopyV1Response APIGetCertificateCopyV1(APIGetCertificateCopyV1Request data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetCertificateCopyV1Response res = new APIGetCertificateCopyV1Response();

                    res = _logic.APIGetCertificateCopyV1(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetCertificateCopyV1ResponseObj.TransactionID = null;
                    GetCertificateCopyV1ResponseObj.ErrorMessage = "Internal Server Error";
                    GetCertificateCopyV1ResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetCertificateCopyV1ResponseObj.status = "Failed";

                    return GetCertificateCopyV1ResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);

                GetCertificateCopyV1ResponseObj.TransactionID = data.TransactionID;
                GetCertificateCopyV1ResponseObj.ErrorMessage = "Internal Server Error";
                GetCertificateCopyV1ResponseObj.ErrorMessageDetail = null;
                GetCertificateCopyV1ResponseObj.status = "Failed";

                return GetCertificateCopyV1ResponseObj;
            }
        }

        #endregion

        #region APISaveDocumentData
        public APISaveDocumentDataResponse APISaveDocumentData(APISaveDocumentDataRequest data)
        {
            try
            {
                if (data != null)
                {
                    APISaveDocumentDataResponse SaveDocRes = new APISaveDocumentDataResponse();
                    SaveDocRes = _logic.APISaveDocumentData(data);

                    SaveDocRes.ErrorMessageDetail = null;

                    return SaveDocRes;
                }
                else
                {
                    SaveDocRes.ErrorMessage = "Internal Server Error";
                    SaveDocRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    SaveDocRes.status = "Failed";

                    return SaveDocRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                SaveDocRes.ErrorMessage = "Internal Server Error";
                SaveDocRes.ErrorMessageDetail = null;
                SaveDocRes.status = "Failed";
                return SaveDocRes;
            }
        }

        #endregion

        #region APIGetDocumentData
        public APIGetDocumentDataResponse APIGetDocumentData(APIGetDocumentDataRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetDocumentDataResponse GetDocRes = new APIGetDocumentDataResponse();
                    GetDocRes = _logic.APIGetDocumentData(data);

                    GetDocRes.ErrorMessageDetail = null;

                    return GetDocRes;
                }
                else
                {
                    SaveDocRes.ErrorMessage = "Internal Server Error";
                    SaveDocRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Model is NULL";
                    SaveDocRes.status = "Failed";

                    return GetDocRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                SaveDocRes.ErrorMessage = "Internal Server Error";
                SaveDocRes.ErrorMessageDetail = null;
                SaveDocRes.status = "Failed";
                return GetDocRes;
            }
        }

        #endregion
      

        #region APIGetVendorStatus
        public APIGetVendorStatusResponse APIGetVendorStatus(APIGetVendorStatusRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetVendorStatusResponse GetVendorStatusRes = new APIGetVendorStatusResponse();
                    GetVendorStatusRes = _logic.APIGetVendorStatus(data);

                    GetVendorStatusRes.ErrorMessageDetail = null;

                    return GetVendorStatusRes;
                }
                else
                {
                    GetVendorStatusRes.ErrorMessage = "Internal Server Error";
                    GetVendorStatusRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetVendorStatusRes.status = "Failed";

                    return GetVendorStatusRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetVendorStatusRes.ErrorMessage = "Internal Server Error";
                GetVendorStatusRes.ErrorMessageDetail = null;
                GetVendorStatusRes.status = "Failed";
                return GetVendorStatusRes;
            }
        }

        #endregion

        #region APIGetVendorAssistRequest
        public APIGetVendorAssistResponse APIGetVendorAssistRequest(APIGetVendorAssistRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetVendorAssistResponse GetVendorAssistRes = new APIGetVendorAssistResponse();
                    GetVendorAssistRes = _logic.APIGetVendorAssistRequest(data);

                    GetVendorAssistRes.ErrorMessageDetail = null;

                    return GetVendorAssistRes;
                }
                else
                {
                    GetVendorAssistRes.ErrorMessage = "Internal Server Error";
                    GetVendorAssistRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetVendorAssistRes.status = "Failed";

                    return GetVendorAssistRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetVendorAssistRes.ErrorMessage = "Internal Server Error";
                GetVendorAssistRes.ErrorMessageDetail = null;
                GetVendorAssistRes.status = "Failed";
                return GetVendorAssistRes;
            }
        }

        #endregion

        #region APIUpdateVendorLatLong
        public APIUpdateVendorLatLongResponse APIUpdateVendorLatLong(APIUpdateVendorLatLongRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIUpdateVendorLatLongResponse UpdateVendorLatLongRes = new APIUpdateVendorLatLongResponse();
                    UpdateVendorLatLongRes = _logic.APIUpdateVendorLatLong(data);

                    UpdateVendorLatLongRes.ErrorMessageDetail = null;

                    return UpdateVendorLatLongRes;
                }
                else
                {
                    UpdateVendorLatLongRes.ErrorMessage = "Internal Server Error";
                    UpdateVendorLatLongRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    UpdateVendorLatLongRes.status = "Failed";

                    return UpdateVendorLatLongRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                UpdateVendorLatLongRes.ErrorMessage = "Internal Server Error";
                UpdateVendorLatLongRes.ErrorMessageDetail = null;
                UpdateVendorLatLongRes.status = "Failed";
                return UpdateVendorLatLongRes;
            }
        }

        #endregion

        //Pedal Cycle API Start       
        #region ValidatePCUser
        public APIGetStoreNameForUserResponse ValidatePCUser(APIGetStoreNameForUserRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetStoreNameForUserResponse GetStoreNameRes = new APIGetStoreNameForUserResponse();
                    GetStoreNameRes = _logic.ValidatePCUser(data);

                    GetStoreNameRes.ErrorMessageDetail = null;

                    return GetStoreNameRes;
                }
                else
                {
                    GetStoreNameRes.ErrorMessage = "Internal Server Error";
                    GetStoreNameRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetStoreNameRes.status = "Failed";

                    return GetStoreNameRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetStoreNameRes.ErrorMessage = "Internal Server Error";
                GetStoreNameRes.ErrorMessageDetail = null;
                GetStoreNameRes.status = "Failed";
                return GetStoreNameRes;
            }
        }

        #endregion

        #region PCGetQuote
        public APIPCGetQuoteResponse PCGetQuote(APIPCGetQuoteRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIPCGetQuoteResponse PCGetQuoteobj = new APIPCGetQuoteResponse();
                    PCGetQuoteobj = _logic.PCGetQuote(data);

                    PCGetQuoteobj.ErrorMessageDetail = null;

                    return PCGetQuoteobj;
                }
                else
                {
                    PCGetQuoteobj.ErrorMessage = "Internal Server Error";
                    PCGetQuoteobj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCGetQuoteobj.status = "Failed";

                    return PCGetQuoteobj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCGetQuoteobj.ErrorMessage = "Internal Server Error";
                PCGetQuoteobj.ErrorMessageDetail = null;
                PCGetQuoteobj.status = "Failed";
                return PCGetQuoteobj;
            }
        }

        #endregion 

        #region PCCreateProposal
        public APIPCCreateProposalResponse PCCreateProposal(APIPCCreateProposalRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCCreateProposalResponse res = new APIPCCreateProposalResponse();

                    res = _logic.PCCreateProposal(data);

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(data.UniqueTransactionID);
                    reqres.Response = JsonConvert.SerializeObject(res);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //
                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCCreateProposalObj.TransactionID = "";
                    PCCreateProposalObj.ErrorMessage = "Internal Server Error";
                    PCCreateProposalObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCCreateProposalObj.status = "Failed";

                    // Request Insert in Database
                    reqres.TransactionId = Convert.ToInt64(PCCreateProposalObj.TransactionID);
                    reqres.Request = JsonConvert.SerializeObject(data);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(PCCreateProposalObj.TransactionID);
                    reqres.Response = JsonConvert.SerializeObject(PCCreateProposalObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    return PCCreateProposalObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCCreateProposalObj.TransactionID = data.UniqueTransactionID;
                PCCreateProposalObj.ErrorMessage = "Internal Server Error";
                PCCreateProposalObj.ErrorMessageDetail = null;
                PCCreateProposalObj.status = "Failed";
                return PCCreateProposalObj;
            }
        }

        #endregion

        #region PCMakePayment
        public APIPCMakePaymentResponse PCMakePayment(APIPCMakePaymentRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCMakePaymentResponse res = new APIPCMakePaymentResponse();

                    res = _logic.PCMakePayment(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCMakePaymentObj.TransactionID = null;
                    PCMakePaymentObj.ErrorMessage = "Internal Server Error";
                    PCMakePaymentObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCMakePaymentObj.status = "Failed";

                    return PCMakePaymentObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCMakePaymentObj.TransactionID = data.TransactionID;
                PCMakePaymentObj.ErrorMessage = "Internal Server Error";
                PCMakePaymentObj.ErrorMessageDetail = null;
                PCMakePaymentObj.status = "Failed";
                return PCMakePaymentObj;
            }
        }

        #endregion

        #region PCGetPolicyCopy
        public APIPCGetPolicyCopyResponse PCGetPolicyCopy(APIPCGetPolicyCopyRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCGetPolicyCopyResponse res = new APIPCGetPolicyCopyResponse();

                    //// Request Insert in Database
                    //reqres.TransactionId = Convert.ToInt64(data.TransactionID);
                    //reqres.Request = JsonConvert.SerializeObject(data);
                    //reqres.InsertDate = DateTime.Now;
                    //_db.tblRequestResponse.Add(reqres);
                    //_db.SaveChanges();
                    ////

                    res = _logic.PCGetPolicyCopy(data);

                    //// Response Insert in Database
                    //reqres.TransactionId = Convert.ToInt64(data.TransactionID);
                    //reqres.Response = JsonConvert.SerializeObject(res);
                    //reqres.InsertDate = DateTime.Now;
                    //_db.tblRequestResponse.Add(reqres);
                    //_db.SaveChanges();
                    ////

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCGetPolicyCopyObj.TransactionID = null;
                    PCGetPolicyCopyObj.ErrorMessage = "Internal Server Error";
                    PCGetPolicyCopyObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCGetPolicyCopyObj.status = "Failed";

                    //// Request Insert in Database
                    //reqres.TransactionId = Convert.ToInt64(PCGetPolicyCopyObj.TransactionID);
                    //reqres.Request = JsonConvert.SerializeObject(data);
                    //reqres.InsertDate = DateTime.Now;
                    //_db.tblRequestResponse.Add(reqres);
                    //_db.SaveChanges();
                    ////

                    //// Response Insert in Database
                    //reqres.TransactionId = Convert.ToInt64(PCGetPolicyCopyObj.TransactionID);
                    //reqres.Response = JsonConvert.SerializeObject(PCGetPolicyCopyObj);
                    //reqres.InsertDate = DateTime.Now;
                    //_db.tblRequestResponse.Add(reqres);
                    //_db.SaveChanges();
                    ////

                    return PCGetPolicyCopyObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCGetPolicyCopyObj.TransactionID = data.TransactionID;
                PCGetPolicyCopyObj.ErrorMessage = "Internal Server Error";
                PCGetPolicyCopyObj.ErrorMessageDetail = null;
                PCGetPolicyCopyObj.status = "Failed";
                return PCGetPolicyCopyObj;
            }
        }

        #endregion

        #region PCGetPolicyPendingPaymentDetails
        public APIPCGetPolicyPendingPaymentResponse PCGetPolicyPendingPaymentDetails(APIPCGetPolicyPendingPaymentRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCGetPolicyPendingPaymentResponse res = new APIPCGetPolicyPendingPaymentResponse();

                    res = _logic.PCGetPolicyPendingPaymentDetails(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCGetPolicyPendingPaymentObj.TransactionID = null;
                    PCGetPolicyPendingPaymentObj.ErrorMessage = "Internal Server Error";
                    PCGetPolicyPendingPaymentObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCGetPolicyPendingPaymentObj.status = "Failed";
                    return PCGetPolicyPendingPaymentObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCGetPolicyPendingPaymentObj.TransactionID = data.TransactionID;
                PCGetPolicyPendingPaymentObj.ErrorMessage = "Internal Server Error";
                PCGetPolicyPendingPaymentObj.ErrorMessageDetail = null;
                PCGetPolicyPendingPaymentObj.status = "Failed";
                return PCGetPolicyPendingPaymentObj;
            }
        }

        #endregion

        #region PCMakeMultiplePayment
        public APIPCMakeMultiplePaymentResponse PCMakeMultiplePayment(APIPCMakeMultiplePaymentRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCMakeMultiplePaymentResponse res = new APIPCMakeMultiplePaymentResponse();

                    res = _logic.PCMakeMultiplePayment(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCMakeMultiplePaymentObj.TransactionID = null;
                    PCMakeMultiplePaymentObj.ErrorMessage = "Internal Server Error";
                    PCMakeMultiplePaymentObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCMakeMultiplePaymentObj.status = "Failed";

                    return PCMakeMultiplePaymentObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCMakeMultiplePaymentObj.TransactionID = data.TransactionID;
                PCMakeMultiplePaymentObj.ErrorMessage = "Internal Server Error";
                PCMakeMultiplePaymentObj.ErrorMessageDetail = null;
                PCMakeMultiplePaymentObj.status = "Failed";
                return PCMakeMultiplePaymentObj;
            }
        }

        #endregion

        #region PCMISDetails
        public APIPCMISDetailsResponse PCMISDetails(APIPCMISDetailsRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCMISDetailsResponse res = new APIPCMISDetailsResponse();

                    res = _logic.PCMISDetails(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCMISDetailsObj.TransactionID = null;
                    PCMISDetailsObj.ErrorMessage = "Internal Server Error";
                    PCMISDetailsObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCMISDetailsObj.status = "Failed";

                    return PCMISDetailsObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCMISDetailsObj.TransactionID = data.TransactionID;
                PCMISDetailsObj.ErrorMessage = "Internal Server Error";
                PCMISDetailsObj.ErrorMessageDetail = null;
                PCMISDetailsObj.status = "Failed";
                return PCMISDetailsObj;
            }
        }

        #endregion

        #region APIGetPCMakeMaster
        public APIGetPCMakeMasterResponse APIGetPCMakeMaster(APIGetPCMakeMasterRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetPCMakeMasterResponse res = new APIGetPCMakeMasterResponse();

                    res = _logic.APIGetPCMakeMaster(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetPCMakeMasterResponseObj.TransactionID = null;
                    GetPCMakeMasterResponseObj.ErrorMessage = "Internal Server Error";
                    GetPCMakeMasterResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetPCMakeMasterResponseObj.status = "Failed";

                    return GetPCMakeMasterResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetPCMakeMasterResponseObj.TransactionID = data.TransactionID;
                GetPCMakeMasterResponseObj.ErrorMessage = "Internal Server Error";
                GetPCMakeMasterResponseObj.ErrorMessageDetail = null;
                GetPCMakeMasterResponseObj.status = "Failed";
                return GetPCMakeMasterResponseObj;
            }
        }
        
        #endregion

        #region APIGetPCModelMaster
        public APIGetPCModelMasterResponse APIGetPCModelMaster(APIGetPCModelMasterRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetPCModelMasterResponse res = new APIGetPCModelMasterResponse();

                    res = _logic.APIGetPCModelMaster(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetPCModelMasterResponseObj.TransactionID = null;
                    GetPCModelMasterResponseObj.ErrorMessage = "Internal Server Error";
                    GetPCModelMasterResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetPCModelMasterResponseObj.status = "Failed";

                    return GetPCModelMasterResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetPCModelMasterResponseObj.TransactionID = data.TransactionID;
                GetPCModelMasterResponseObj.ErrorMessage = "Internal Server Error";
                GetPCModelMasterResponseObj.ErrorMessageDetail = null;
                GetPCModelMasterResponseObj.status = "Failed";
                return GetPCModelMasterResponseObj;
            }
        }
        
        #endregion

        #region PCGetAgentProfile
        public APIAgentProfileResponse PCGetAgentProfile(APIAgentProfileRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIAgentProfileResponse res = new APIAgentProfileResponse();

                    res = _logic.PCGetAgentProfile(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCGetAgentProfileObj.ErrorMessage = "Internal Server Error";
                    PCGetAgentProfileObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCGetAgentProfileObj.status = "Failed";

                    return PCGetAgentProfileObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCGetAgentProfileObj.ErrorMessage = "Internal Server Error";
                PCGetAgentProfileObj.ErrorMessageDetail = null;
                PCGetAgentProfileObj.status = "Failed";
                return PCGetAgentProfileObj;
            }
        }
        
        #endregion

        #region PCUpdateDealerProfile
        public APIPCUpdateDealerProfileResponse PCUpdateDealerProfile(APIPCDealerProfileUpdateRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCUpdateDealerProfileResponse res = new APIPCUpdateDealerProfileResponse();

                    res = _logic.PCUpdateDealerProfile(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCGetDealerProfileObj.ErrorMessage = "Internal Server Error";
                    PCGetDealerProfileObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCGetDealerProfileObj.status = "Failed";

                    return PCGetDealerProfileObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCGetAgentProfileObj.ErrorMessage = "Internal Server Error";
                PCGetAgentProfileObj.ErrorMessageDetail = null;
                PCGetAgentProfileObj.status = "Failed";
                return PCGetDealerProfileObj;
            }
        }
        
        #endregion

        #region APIGetDealerDashboard
        public APIDealerDashboardResponse PCGetDealerDashboard(APIGetDelaerDashboardRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIDealerDashboardResponse GetDealerDashboardRes = new APIDealerDashboardResponse();
                    GetDealerDashboardRes = _logic.PCGetDealerDashboard(data);

                    GetDealerDashboardRes.ErrorMessageDetail = null;

                    return GetDealerDashboardRes;
                }
                else
                {
                    DealerDashboardobj.ErrorMessage = "Internal Server Error";
                    DealerDashboardobj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    DealerDashboardobj.status = "Failed";

                    return DealerDashboardobj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                DealerDashboardobj.ErrorMessage = "Internal Server Error";
                DealerDashboardobj.ErrorMessageDetail = null;
                DealerDashboardobj.status = "Failed";
                return DealerDashboardobj;
            }
        }

        #endregion

        #region APIGetPCRelationship
        public APIGetPCRelationshipResponse APIGetPCRelationship(APIGetPCRelationshipRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetPCRelationshipResponse GetPCRelationshipRes = new APIGetPCRelationshipResponse();
                    GetPCRelationshipRes = _logic.APIGetPCRelationship(data);

                    GetPCRelationshipRes.ErrorMessageDetail = null;

                    return GetPCRelationshipRes;
                }
                else
                {
                    PCRelationshipObj.ErrorMessage = "Internal Server Error";
                    PCRelationshipObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCRelationshipObj.status = "Failed";

                    return PCRelationshipObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCRelationshipObj.ErrorMessage = "Internal Server Error";
                PCRelationshipObj.ErrorMessageDetail = null;
                PCRelationshipObj.status = "Failed";
                return PCRelationshipObj;
            }
        }

        #endregion

        #region APIGetPCCertificateData
        public APIGetPCCertificateDataResponse APIGetPCCertificateData(APIGetPCCertificateDataRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetPCCertificateDataResponse GetPCCertificateDataRes = new APIGetPCCertificateDataResponse();
                    GetPCCertificateDataRes = _logic.APIGetPCCertificateData(data);

                    GetPCCertificateDataRes.ErrorMessageDetail = null;

                    return GetPCCertificateDataRes;
                }
                else
                {
                    PCCertificateDataObj.ErrorMessage = "Internal Server Error";
                    PCCertificateDataObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCCertificateDataObj.status = "Failed";

                    return PCCertificateDataObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCCertificateDataObj.ErrorMessage = "Internal Server Error";
                PCCertificateDataObj.ErrorMessageDetail = null;
                PCCertificateDataObj.status = "Failed";
                return PCCertificateDataObj;
            }
        }

        #endregion

        #region APIGetPCWalletDetails
        public APIGetPCWalletDetailsResponse APIGetPCWalletDetails(APIGetPCWalletDetailsRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetPCWalletDetailsResponse GetPCWalletDetailsRes = new APIGetPCWalletDetailsResponse();
                    GetPCWalletDetailsRes = _logic.APIGetPCWalletDetails(data);

                    GetPCWalletDetailsRes.ErrorMessageDetail = null;

                    return GetPCWalletDetailsRes;
                }
                else
                {
                    PCWalletDetailsObj.ErrorMessage = "Internal Server Error";
                    PCWalletDetailsObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCWalletDetailsObj.status = "Failed";

                    return PCWalletDetailsObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCWalletDetailsObj.ErrorMessage = "Internal Server Error";
                PCWalletDetailsObj.ErrorMessageDetail = null;
                PCWalletDetailsObj.status = "Failed";

                return PCWalletDetailsObj;
            }
        }

        #endregion

        #region APIPCGetSearchClaim
        public APIInitClaimSearchResponse PCGetSearchClaim(APIInitClaimSearchRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIInitClaimSearchResponse ClaimSearchRes = new APIInitClaimSearchResponse();
                    ClaimSearchRes = _logic.PCInitClaimSearch(data);

                    ClaimSearchRes.ErrorMessageDetail = null;

                    return ClaimSearchRes;
                }
                else
                {
                    InitClaimSearchobj.ErrorMessage = "Internal Server Error";
                    InitClaimSearchobj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    InitClaimSearchobj.status = "Failed";

                    return InitClaimSearchobj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                InitClaimSearchobj.ErrorMessage = "Internal Server Error";
                InitClaimSearchobj.ErrorMessageDetail = null;
                InitClaimSearchobj.status = "Failed";
                return InitClaimSearchobj;
            }
        }

        #endregion

        #region APIPCGetSearchClaim
        public APIInitClaimSearchResponse PCGetRegSearchClaim(APIInitClaimSearchRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIInitClaimSearchResponse ClaimSearchRes = new APIInitClaimSearchResponse();
                    ClaimSearchRes = _logic.PCRegClaimSearch(data);

                    ClaimSearchRes.ErrorMessageDetail = null;

                    return ClaimSearchRes;
                }
                else
                {
                    InitClaimSearchobj.ErrorMessage = "Internal Server Error";
                    InitClaimSearchobj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    InitClaimSearchobj.status = "Failed";

                    return InitClaimSearchobj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                InitClaimSearchobj.ErrorMessage = "Internal Server Error";
                InitClaimSearchobj.ErrorMessageDetail = null;
                InitClaimSearchobj.status = "Failed";
                return InitClaimSearchobj;
            }
        }

        #endregion


        #region PCGetPolicyDetails
        public PCGetPolicyDetailsResponse PCGetPolicyDetails(PCGetPolicyDetailsRequest data)
        {
            try
            {
                if (data != null)
                {
                    PCGetPolicyDetailsResponse GetPolicyDetailsRes = new PCGetPolicyDetailsResponse();
                    GetPolicyDetailsRes = _logic.PCGetPolicyDetails(data);

                    GetPolicyDetailsRes.ErrorMessageDetail = null;

                    return GetPolicyDetailsRes;
                }
                else
                {
                    GetPolicyDetailsResponseObj.ErrorMessage = "Internal Server Error";
                    GetPolicyDetailsResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetPolicyDetailsResponseObj.status = "Failed";

                    return GetPolicyDetailsResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetPolicyDetailsResponseObj.ErrorMessage = "Internal Server Error";
                GetPolicyDetailsResponseObj.ErrorMessageDetail = null;
                GetPolicyDetailsResponseObj.status = "Failed";

                return GetPolicyDetailsResponseObj;
            }
        }

        #endregion

        #region PCClaimIntimation
        public PCClaimIntimationResponse PCClaimIntimation(PCClaimIntimationRequest data)
        {
            try
            {
                if (data != null)
                {
                    PCClaimIntimationResponse ClaimIntimationRes = new PCClaimIntimationResponse();
                    ClaimIntimationRes = _logic.PCClaimIntimation(data);

                    ClaimIntimationRes.ErrorMessageDetail = null;

                    return ClaimIntimationRes;
                }
                else
                {
                    ClaimIntimationResponseObj.ErrorMessage = "Internal Server Error";
                    ClaimIntimationResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    ClaimIntimationResponseObj.status = "Failed";

                    return ClaimIntimationResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                ClaimIntimationResponseObj.ErrorMessage = "Internal Server Error";
                ClaimIntimationResponseObj.ErrorMessageDetail = null;
                ClaimIntimationResponseObj.status = "Failed";

                return ClaimIntimationResponseObj;
            }
        }

        #endregion

        #region PCGetClaimDetails
        public PCGetClaimDetailsResponse PCGetClaimDetails(PCGetClaimDetailsRequest data)
        {
            try
            {
                if (data != null)
                {
                    PCGetClaimDetailsResponse GetClaimDetailsRes = new PCGetClaimDetailsResponse();
                    GetClaimDetailsRes = _logic.PCGetClaimDetails(data);

                    GetClaimDetailsRes.ErrorMessageDetail = null;

                    return GetClaimDetailsRes;
                }
                else
                {
                    GetClaimDetailsResponseObj.ErrorMessage = "Internal Server Error";
                    GetClaimDetailsResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetClaimDetailsResponseObj.status = "Failed";

                    return GetClaimDetailsResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetClaimDetailsResponseObj.ErrorMessage = "Internal Server Error";
                GetClaimDetailsResponseObj.ErrorMessageDetail = null;
                GetClaimDetailsResponseObj.status = "Failed";

                return GetClaimDetailsResponseObj;
            }
        }

        #endregion

        #region Un-Used API Code
        //#region PCExternalLinkGeneration
        //public APIPCExternalLinkGenerationResponse PCExternalLinkGeneration(APIPCExternalLinkGenerationRequest data)
        //{
        //    try
        //    {
        //        if (data != null)
        //        {
        //            tblRequestResponse reqres = new tblRequestResponse();
        //            APIPCExternalLinkGenerationResponse res = new APIPCExternalLinkGenerationResponse();

        //            res = _logic.PCExternalLinkGeneration(data);

        //            // Response Insert in Database
        //            reqres.TransactionId = Convert.ToInt64(data.UniqueTransactionID);
        //            reqres.Response = JsonConvert.SerializeObject(res);
        //            reqres.InsertDate = DateTime.Now;
        //            _db.tblRequestResponse.Add(reqres);
        //            _db.SaveChanges();
        //            //
        //            res.ErrorMessageDetail = null;

        //            return res;
        //        }
        //        else
        //        {
        //            PCExternalLinkGenerationObj.TransactionID = "";
        //            PCExternalLinkGenerationObj.ErrorMessage = "Internal Server Error";
        //            PCExternalLinkGenerationObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
        //            PCExternalLinkGenerationObj.status = "Failed";

        //            // Request Insert in Database
        //            reqres.TransactionId = Convert.ToInt64(PCExternalLinkGenerationObj.TransactionID);
        //            reqres.Request = JsonConvert.SerializeObject(data);
        //            reqres.InsertDate = DateTime.Now;
        //            _db.tblRequestResponse.Add(reqres);
        //            _db.SaveChanges();
        //            //

        //            // Response Insert in Database
        //            reqres.TransactionId = Convert.ToInt64(PCExternalLinkGenerationObj.TransactionID);
        //            reqres.Response = JsonConvert.SerializeObject(PCExternalLinkGenerationObj);
        //            reqres.InsertDate = DateTime.Now;
        //            _db.tblRequestResponse.Add(reqres);
        //            _db.SaveChanges();
        //            //

        //            return PCExternalLinkGenerationObj;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        HandleException(e);
        //        PCExternalLinkGenerationObj.TransactionID = data.UniqueTransactionID;
        //        PCExternalLinkGenerationObj.ErrorMessage = "Internal Server Error";
        //        PCExternalLinkGenerationObj.ErrorMessageDetail = null;
        //        PCExternalLinkGenerationObj.status = "Failed";
        //        return PCExternalLinkGenerationObj;
        //    }
        //}

        //#endregion
        #endregion

        #region PCExternalLinkGeneration
        public APIPCExternalLinkGenerationResponse PCExternalLinkGeneration(APIPCExternalLinkGenerationRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCExternalLinkGenerationResponse res = new APIPCExternalLinkGenerationResponse();

                    res = _logic.PCExternalLinkGeneration(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCExternalLinkGenerationObj.TransactionID = null;
                    PCExternalLinkGenerationObj.ErrorMessage = "Internal Server Error";
                    PCExternalLinkGenerationObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCExternalLinkGenerationObj.status = "Failed";

                    return PCExternalLinkGenerationObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCExternalLinkGenerationObj.TransactionID = data.TransactionID;
                PCExternalLinkGenerationObj.ErrorMessage = "Internal Server Error";
                PCExternalLinkGenerationObj.ErrorMessageDetail = null;
                PCExternalLinkGenerationObj.status = "Failed";

                return PCExternalLinkGenerationObj;
            }
        }

        #endregion

        #region APIGetPCTieUpCompanyMaster
        public APIGetPCTieUpCompanyMasterResponse APIGetPCTieUpCompanyMaster(APIGetPCTieUpCompanyMasterRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetPCTieUpCompanyMasterResponse res = new APIGetPCTieUpCompanyMasterResponse();

                    res = _logic.APIGetPCTieUpCompanyMaster(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetPCTieUpCompanyMasterResponseObj.TransactionID = null;
                    GetPCTieUpCompanyMasterResponseObj.ErrorMessage = "Internal Server Error";
                    GetPCTieUpCompanyMasterResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetPCTieUpCompanyMasterResponseObj.status = "Failed";

                    return GetPCTieUpCompanyMasterResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetPCTieUpCompanyMasterResponseObj.TransactionID = data.TransactionID;
                GetPCTieUpCompanyMasterResponseObj.ErrorMessage = "Internal Server Error";
                GetPCTieUpCompanyMasterResponseObj.ErrorMessageDetail = null;
                GetPCTieUpCompanyMasterResponseObj.status = "Failed";

                return GetPCTieUpCompanyMasterResponseObj;
            }
        }

        #endregion

        #region APIGetPCTieUpCompanyAddOn
        public APIGetPCTieUpCompanyAddOnResponse APIGetPCTieUpCompanyAddOn(APIGetPCTieUpCompanyAddOnRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetPCTieUpCompanyAddOnResponse res = new APIGetPCTieUpCompanyAddOnResponse();

                    res = _logic.APIGetPCTieUpCompanyAddOn(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetPCTieUpCompanyAddOnResponseObj.TransactionID = null;
                    GetPCTieUpCompanyAddOnResponseObj.ErrorMessage = "Internal Server Error";
                    GetPCTieUpCompanyAddOnResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetPCTieUpCompanyAddOnResponseObj.status = "Failed";

                    return GetPCTieUpCompanyAddOnResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetPCTieUpCompanyAddOnResponseObj.TransactionID = data.TransactionID;
                GetPCTieUpCompanyAddOnResponseObj.ErrorMessage = "Internal Server Error";
                GetPCTieUpCompanyAddOnResponseObj.ErrorMessageDetail = null;
                GetPCTieUpCompanyAddOnResponseObj.status = "Failed";

                return GetPCTieUpCompanyAddOnResponseObj;
            }
        }

        #endregion

        #region PCCreateProposalMultipleProduct
        public APIPCCreateProposalMultipleProductResponse PCCreateProposalMultipleProduct(APIPCCreateProposalMultipleProductRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIPCCreateProposalMultipleProductResponse res = new APIPCCreateProposalMultipleProductResponse();

                    res = _logic.PCCreateProposalMultipleProduct(data);

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(data.UniqueTransactionID);
                    reqres.Response = JsonConvert.SerializeObject(res);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //
                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PCCreateProposalMultipleProductResponseObj.TransactionID = "";
                    PCCreateProposalMultipleProductResponseObj.ErrorMessage = "Internal Server Error";
                    PCCreateProposalMultipleProductResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PCCreateProposalMultipleProductResponseObj.status = "Failed";

                    // Request Insert in Database
                    reqres.TransactionId = Convert.ToInt64(PCCreateProposalMultipleProductResponseObj.TransactionID);
                    reqres.Request = JsonConvert.SerializeObject(data);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    // Response Insert in Database
                    reqres.TransactionId = Convert.ToInt64(PCCreateProposalMultipleProductResponseObj.TransactionID);
                    reqres.Response = JsonConvert.SerializeObject(PCCreateProposalMultipleProductResponseObj);
                    reqres.InsertDate = DateTime.Now;
                    _db.tblRequestResponse.Add(reqres);
                    _db.SaveChanges();
                    //

                    return PCCreateProposalMultipleProductResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PCCreateProposalMultipleProductResponseObj.TransactionID = data.UniqueTransactionID;
                PCCreateProposalMultipleProductResponseObj.ErrorMessage = "Internal Server Error";
                PCCreateProposalMultipleProductResponseObj.ErrorMessageDetail = null;
                PCCreateProposalMultipleProductResponseObj.status = "Failed";

                return PCCreateProposalMultipleProductResponseObj;
            }
        }

        #endregion

        //Pedal Cycle API End

        #region APIMakeMultiplePayment
        public APIMakeMultiplePaymentResponse APIMakeMultiplePayment(APIMakeMultiplePaymentRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIMakeMultiplePaymentResponse res = new APIMakeMultiplePaymentResponse();

                    res = _logic.APIMakeMultiplePayment(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    MakeMultiplePaymentObj.TransactionID = null;
                    MakeMultiplePaymentObj.ErrorMessage = "Internal Server Error";
                    MakeMultiplePaymentObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    MakeMultiplePaymentObj.status = "Failed";

                    return MakeMultiplePaymentObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                MakeMultiplePaymentObj.TransactionID = data.TransactionID;
                MakeMultiplePaymentObj.ErrorMessage = "Internal Server Error";
                MakeMultiplePaymentObj.ErrorMessageDetail = null;
                MakeMultiplePaymentObj.status = "Failed";
                return MakeMultiplePaymentObj;
            }
        }

        #endregion

        #region SendOTP
        public APISendOTPResponse SendOTP(APISendOTPRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APISendOTPResponse res = new APISendOTPResponse();

                    res = _logic.SendOTP(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    SendOTPResponseObj.TransactionID = null;
                    SendOTPResponseObj.ErrorMessage = "Internal Server Error";
                    SendOTPResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    SendOTPResponseObj.status = "Failed";

                    return SendOTPResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                SendOTPResponseObj.TransactionID = data.TransactionID;
                SendOTPResponseObj.ErrorMessage = "Internal Server Error";
                SendOTPResponseObj.ErrorMessageDetail = null;
                SendOTPResponseObj.status = "Failed";
                return SendOTPResponseObj;
            }
        }

        #endregion

        #region ValidateOTP
        public APIValidateOTPResponse ValidateOTP(APIValidateOTPRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIValidateOTPResponse res = new APIValidateOTPResponse();

                    res = _logic.ValidateOTP(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    ValidateOTPResponseObj.TransactionID = null;
                    ValidateOTPResponseObj.ErrorMessage = "Internal Server Error";
                    ValidateOTPResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    ValidateOTPResponseObj.status = "Failed";

                    return ValidateOTPResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                ValidateOTPResponseObj.TransactionID = data.TransactionID;
                ValidateOTPResponseObj.ErrorMessage = "Internal Server Error";
                ValidateOTPResponseObj.ErrorMessageDetail = null;
                ValidateOTPResponseObj.status = "Failed";
                return ValidateOTPResponseObj;
            }
        }

        #endregion

        #region APIGetStateMaster
        public APIGetStateMasterResponse APIGetStateMaster(APIGetStateMasterRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetStateMasterResponse res = new APIGetStateMasterResponse();

                    res = _logic.APIGetStateMaster(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetStateMasterResponseObj.TransactionID = null;
                    GetStateMasterResponseObj.ErrorMessage = "Internal Server Error";
                    GetStateMasterResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetStateMasterResponseObj.status = "Failed";

                    return GetStateMasterResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetStateMasterResponseObj.TransactionID = data.TransactionID;
                GetStateMasterResponseObj.ErrorMessage = "Internal Server Error";
                GetStateMasterResponseObj.ErrorMessageDetail = null;
                GetStateMasterResponseObj.status = "Failed";
                return GetStateMasterResponseObj;
            }
        }

        #endregion

        #region APIGetCityMaster
        public APIGetCityMasterResponse APIGetCityMaster(APIGetCityMasterRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetCityMasterResponse res = new APIGetCityMasterResponse();

                    res = _logic.APIGetCityMaster(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetCityMasterResponseObj.TransactionID = null;
                    GetCityMasterResponseObj.ErrorMessage = "Internal Server Error";
                    GetCityMasterResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetCityMasterResponseObj.status = "Failed";

                    return GetCityMasterResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetCityMasterResponseObj.TransactionID = data.TransactionID;
                GetCityMasterResponseObj.ErrorMessage = "Internal Server Error";
                GetCityMasterResponseObj.ErrorMessageDetail = null;
                GetCityMasterResponseObj.status = "Failed";
                return GetCityMasterResponseObj;
            }
        }

        #endregion

        #region APISaveReliancePolicy
        public NewAssistResponse APISaveReliancePolicy(RelianceAssistRequest data)
        {
            string RespStatus = "policy is eligible for rsa";
            NewAssistResponse res = new NewAssistResponse();

            //sa 1001
            #region GetDatafromReliance Service for Saving
            string UserID = System.Configuration.ConfigurationManager.AppSettings["Userid"];
            string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];
            try
            {
                //string HTML = string.Empty;
                //string message = string.Empty;
                //string status = string.Empty;
                ReliancePolicyModel model = new ReliancePolicyModel();
                RGICL_Policy_Service_RSAClient client = new RGICL_Policy_Service_RSAClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 1, 0); // Timeout 1 min

                if (!string.IsNullOrEmpty(data.RegistrationNo) || !string.IsNullOrEmpty(data.PolicyNumber))
                {
                    string Param = string.Empty;

                    if (!string.IsNullOrEmpty(data.PolicyNumber))
                    {
                        Param = data.PolicyNumber;
                    }
                    else if (!string.IsNullOrEmpty(data.RegistrationNo))
                    {
                        Param = data.RegistrationNo;
                    }

                    var Response = client.GetPolicyDetailforRSA(UserID, Password, Param.Trim());
                    if (Response != null)
                    {
                        if (Response.ResponseStatus.ToLower().Equals(RespStatus))
                        {
                            //  model.ChasisNo = Response.ChassisNumber;
                            //
                            data.ChasisNo = Response.ChassisNumber;
                            //
                            data.EngineNo = Response.EngineNumber;
                            //data.InsuredName = Response.InsuredName;
                            data.Make = Response.Make;
                            data.Model = Response.Model;
                            if (string.IsNullOrEmpty(data.VehicleNo))
                            {
                                //data.VehicleNo = data.RegistrationNo;
                                data.VehicleNo = Response.VehicleNumber;
                            }

                            // model.PolicyNumber = Response.PolicyNumber;
                            data.PolicyStatus = Response.PolicyStatus;
                            data.RiskStartDate = Response.RiskStartDate;
                            data.RiskEndDate = Response.RiskEndDate;
                            //  model.VehicleNo = Response.VehicleNumber;
                            data.ResponseStatus = Response.ResponseStatus;
                            data.RSAVendorName = Response.InsuredName;
                            data.ProductCode = Response.ProductCode;
                            //data.InsuredphoneNumber = Response.InsuredphoneNumber;
                            //data.InsuredMobileNumber = Response.InsuredMobileNumber;
                            data.InsuredEmailID = Response.InsuredEmailID;
                            data.InsuredAddressLine = Response.InsuredAddressLine;
                            data.InsuredCityName = Response.InsuredCityName;
                            data.InsuredDistrictName = Response.InsuredDistrictName;
                        }
                        else
                        {
                            res.ErrorCode = "-99";
                            res.ErrorMessage = "Sorry! Something went wrong.Please try again!";
                            res.status = "Failure";

                            return res;
                        }                        
                    }
                    else
                    {
                        res.ErrorCode = "-99";
                        res.ErrorMessage = "Sorry! Something went wrong.Please try again!";
                        res.status = "Failure";

                        return res;
                    }

                    client.Close();

                    res = _logic.APISaveReliancePolicy(data);
                }
                else
                {
                    res.ErrorCode = "-98";
                    res.ErrorMessage = "PolicyNumber Or RegistrationNo Is Mandatory!";
                    res.status = "Failure";

                    return res;
                }
            }

            catch (Exception e)
            {
                HandleException(e);

                res.ErrorCode = "-99";
                res.ErrorMessage = "Sorry! Something went wrong.Please try again!";
                res.status = "Failure";
            }
            #endregion

            return res;
        }

        #endregion

        #region APIGetReliancePolicyStatus
        public APIGetReliancePolicyStatusResponse APIGetReliancePolicyStatus(APIGetReliancePolicyStatusRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetReliancePolicyStatusResponse res = new APIGetReliancePolicyStatusResponse();

                    res = _logic.APIGetReliancePolicyStatus(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetReliancePolicyStatusObj.ErrorMessage = "Internal Server Error";
                    GetReliancePolicyStatusObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetReliancePolicyStatusObj.status = "Failed";

                    return GetReliancePolicyStatusObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetReliancePolicyStatusObj.ErrorMessage = "Internal Server Error";
                GetReliancePolicyStatusObj.ErrorMessageDetail = null;
                GetReliancePolicyStatusObj.status = "Failed";
                return GetReliancePolicyStatusObj;
            }
        }

        #endregion

        #region APIGetVendorLatLong
        public APIGetVendorLatLongResponse APIGetVendorLatLong(APIGetVendorLatLongRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetVendorLatLongResponse res = new APIGetVendorLatLongResponse();

                    res = _logic.APIGetVendorLatLong(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetVendorLatLongResponseObj.ErrorMessage = "Internal Server Error";
                    GetVendorLatLongResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetVendorLatLongResponseObj.status = "Failed";

                    return GetVendorLatLongResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetVendorLatLongResponseObj.ErrorMessage = "Internal Server Error";
                GetVendorLatLongResponseObj.ErrorMessageDetail = null;
                GetVendorLatLongResponseObj.status = "Failed";
                return GetVendorLatLongResponseObj;
            }
        }

        #endregion

        #region APIVendorAcceptRejectAssistance
        public APIVendorAcceptRejectAssistanceResponse APIVendorAcceptRejectAssistance(APIVendorAcceptRejectAssistanceRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIVendorAcceptRejectAssistanceResponse res = new APIVendorAcceptRejectAssistanceResponse();

                    res = _logic.APIVendorAcceptRejectAssistance(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    VendorAcceptRejectAssistanceResponseObj.ErrorMessage = "Internal Server Error";
                    VendorAcceptRejectAssistanceResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    VendorAcceptRejectAssistanceResponseObj.status = "Failed";

                    return VendorAcceptRejectAssistanceResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                VendorAcceptRejectAssistanceResponseObj.ErrorMessage = "Internal Server Error";
                VendorAcceptRejectAssistanceResponseObj.ErrorMessageDetail = null;
                VendorAcceptRejectAssistanceResponseObj.status = "Failed";
                return VendorAcceptRejectAssistanceResponseObj;
            }
        }

        #endregion

        #region APIGetVendorAcceptedRequest
        public APIGetVendorAcceptedResponse APIGetVendorAcceptedRequest(APIGetVendorAcceptedRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetVendorAcceptedResponse GetVendorAcceptedRes = new APIGetVendorAcceptedResponse();
                    GetVendorAcceptedRes = _logic.APIGetVendorAcceptedRequest(data);

                    GetVendorAcceptedRes.ErrorMessageDetail = null;

                    return GetVendorAcceptedRes;
                }
                else
                {
                    GetVendorAcceptedRes.ErrorMessage = "Internal Server Error";
                    GetVendorAcceptedRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetVendorAcceptedRes.status = "Failed";

                    return GetVendorAcceptedRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetVendorAcceptedRes.ErrorMessage = "Internal Server Error";
                GetVendorAcceptedRes.ErrorMessageDetail = null;
                GetVendorAcceptedRes.status = "Failed";

                return GetVendorAcceptedRes;
            }
        }

        #endregion

        #region APIGetVendorRejectedRequest
        public APIGetVendorRejectedResponse APIGetVendorRejectedRequest(APIGetVendorRejectedRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetVendorRejectedResponse GetVendorRejectedRes = new APIGetVendorRejectedResponse();
                    GetVendorRejectedRes = _logic.APIGetVendorRejectedRequest(data);

                    GetVendorRejectedRes.ErrorMessageDetail = null;

                    return GetVendorRejectedRes;
                }
                else
                {
                    GetVendorRejectedRes.ErrorMessage = "Internal Server Error";
                    GetVendorRejectedRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetVendorRejectedRes.status = "Failed";

                    return GetVendorRejectedRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetVendorRejectedRes.ErrorMessage = "Internal Server Error";
                GetVendorRejectedRes.ErrorMessageDetail = null;
                GetVendorRejectedRes.status = "Failed";

                return GetVendorRejectedRes;
            }
        }

        #endregion

        #region APIGetVendorCompletedRequest
        public APIGetVendorCompletedResponse APIGetVendorCompletedRequest(APIGetVendorCompletedRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIGetVendorCompletedResponse GetVendorCompletedRes = new APIGetVendorCompletedResponse();
                    GetVendorCompletedRes = _logic.APIGetVendorCompletedRequest(data);

                    GetVendorCompletedRes.ErrorMessageDetail = null;

                    return GetVendorCompletedRes;
                }
                else
                {
                    GetVendorCompletedRes.ErrorMessage = "Internal Server Error";
                    GetVendorCompletedRes.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetVendorCompletedRes.status = "Failed";

                    return GetVendorCompletedRes;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetVendorCompletedRes.ErrorMessage = "Internal Server Error";
                GetVendorCompletedRes.ErrorMessageDetail = null;
                GetVendorCompletedRes.status = "Failed";

                return GetVendorCompletedRes;
            }
        }

        #endregion

        #region APIAssistRequestCompletion
        public APIAssistRequestCompletionResponse APIAssistRequestCompletion(APIAssistRequestCompletionRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIAssistRequestCompletionResponse res = new APIAssistRequestCompletionResponse();

                    res = _logic.APIAssistRequestCompletion(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    AssistRequestCompletionResponseObj.ErrorMessage = "Internal Server Error";
                    AssistRequestCompletionResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    AssistRequestCompletionResponseObj.status = "Failed";

                    return AssistRequestCompletionResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                AssistRequestCompletionResponseObj.ErrorMessage = "Internal Server Error";
                AssistRequestCompletionResponseObj.ErrorMessageDetail = null;
                AssistRequestCompletionResponseObj.status = "Failed";

                return AssistRequestCompletionResponseObj;
            }
        }

        #endregion

        #region APIAssistLoctionDetails
        public APIAssistLoctionDetailsResponse APIAssistLoctionDetails(APIAssistLoctionDetailsRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIAssistLoctionDetailsResponse res = new APIAssistLoctionDetailsResponse();

                    res = _logic.APIAssistLoctionDetails(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    AssistLoctionDetailsResponseObj.ErrorMessage = "Internal Server Error";
                    AssistLoctionDetailsResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    AssistLoctionDetailsResponseObj.status = "Failed";

                    return AssistLoctionDetailsResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                AssistLoctionDetailsResponseObj.ErrorMessage = "Internal Server Error";
                AssistLoctionDetailsResponseObj.ErrorMessageDetail = null;
                AssistLoctionDetailsResponseObj.status = "Failed";

                return AssistLoctionDetailsResponseObj;
            }
        }

        #endregion

        #region APIGetMakeMaster
        public APIGetMakeMasterResponse APIGetMakeMaster(APIGetMakeMasterRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetMakeMasterResponse res = new APIGetMakeMasterResponse();

                    res = _logic.APIGetMakeMaster(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetMakeMasterResponseObj.TransactionID = null;
                    GetMakeMasterResponseObj.ErrorMessage = "Internal Server Error";
                    GetMakeMasterResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetMakeMasterResponseObj.status = "Failed";

                    return GetMakeMasterResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetMakeMasterResponseObj.TransactionID = data.TransactionID;
                GetMakeMasterResponseObj.ErrorMessage = "Internal Server Error";
                GetMakeMasterResponseObj.ErrorMessageDetail = null;
                GetMakeMasterResponseObj.status = "Failed";
                return GetMakeMasterResponseObj;
            }
        }

        #endregion    

        #region APIGetModelMaster
        public APIGetModelMasterResponse APIGetModelMaster(APIGetModelMasterRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetModelMasterResponse res = new APIGetModelMasterResponse();

                    res = _logic.APIGetModelMaster(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetModelMasterResponseObj.TransactionID = null;
                    GetModelMasterResponseObj.ErrorMessage = "Internal Server Error";
                    GetModelMasterResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetModelMasterResponseObj.status = "Failed";

                    return GetModelMasterResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetModelMasterResponseObj.TransactionID = data.TransactionID;
                GetModelMasterResponseObj.ErrorMessage = "Internal Server Error";
                GetModelMasterResponseObj.ErrorMessageDetail = null;
                GetModelMasterResponseObj.status = "Failed";
                return GetModelMasterResponseObj;
            }
        }

        #endregion

        #region APIGetDealerPolicyCount
        public APIGetDealerPolicyCountResponse APIGetDealerPolicyCount(APIGetDealerPolicyCountRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIGetDealerPolicyCountResponse res = new APIGetDealerPolicyCountResponse();

                    res = _logic.APIGetDealerPolicyCount(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    GetDealerPolicyCountResponseObj.TransactionID = null;
                    GetDealerPolicyCountResponseObj.ErrorMessage = "Internal Server Error";
                    GetDealerPolicyCountResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    GetDealerPolicyCountResponseObj.status = "Failed";

                    return GetDealerPolicyCountResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                GetDealerPolicyCountResponseObj.TransactionID = data.TransactionID;
                GetDealerPolicyCountResponseObj.ErrorMessage = "Internal Server Error";
                GetDealerPolicyCountResponseObj.ErrorMessageDetail = null;
                GetDealerPolicyCountResponseObj.status = "Failed";
                return GetDealerPolicyCountResponseObj;
            }
        }

        #endregion

        #region APIIssueCovidCertificate
        public APIIssueCovidCertificateResponse APIIssueCovidCertificate(APIIssueCovidCertificateRequest data)
        {
            try
            {
                if (data != null)
                {
                    tblRequestResponse reqres = new tblRequestResponse();
                    APIIssueCovidCertificateResponse res = new APIIssueCovidCertificateResponse();

                    res = _logic.APIIssueCovidCertificate(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    IssueCovidCertificateResponseObj.TransactionID = "";
                    IssueCovidCertificateResponseObj.ErrorMessage = "Internal Server Error";
                    IssueCovidCertificateResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    IssueCovidCertificateResponseObj.status = "Failed";                  

                    return IssueCovidCertificateResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                IssueCovidCertificateResponseObj.TransactionID = data.TransactionID;
                IssueCovidCertificateResponseObj.ErrorMessage = "Internal Server Error";
                IssueCovidCertificateResponseObj.ErrorMessageDetail = null;
                IssueCovidCertificateResponseObj.status = "Failed";

                return IssueCovidCertificateResponseObj;
            }
        }
        #endregion

        #region APISaveCertificate
        public APISaveCertificateResponse APISaveCertificate(APISaveCertificateRequest data)
        {
            try
            {
                if (data != null)
                {
                    APISaveCertificateResponse res = new APISaveCertificateResponse();

                    res = _logic.APISaveCertificate(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    SaveCertificateResponseObj.TransactionID = "";
                    SaveCertificateResponseObj.ErrorMessage = "Internal Server Error";
                    SaveCertificateResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    SaveCertificateResponseObj.status = "Failed";

                    return SaveCertificateResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                SaveCertificateResponseObj.TransactionID = data.TransactionID;
                SaveCertificateResponseObj.ErrorMessage = "Internal Server Error";
                SaveCertificateResponseObj.ErrorMessageDetail = null;
                SaveCertificateResponseObj.status = "Failed";

                return SaveCertificateResponseObj;
            }
        }
        #endregion

        #region APIAssistRequestGeneration
        public APIAssistRequestGenerationResponse APIAssistRequestGeneration(APIAssistRequestGenerationRequest data)
        {
            try
            {
                if (data != null)
                {
                    APIAssistRequestGenerationResponse res = new APIAssistRequestGenerationResponse();

                    res = _logic.APIAssistRequestGeneration(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    AssistRequestGenerationResponseObj.TransactionID = null;
                    AssistRequestGenerationResponseObj.ErrorMessage = "Internal Server Error";
                    AssistRequestGenerationResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    AssistRequestGenerationResponseObj.status = "Failed";

                    return AssistRequestGenerationResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                AssistRequestGenerationResponseObj.TransactionID = data.TransactionID;
                AssistRequestGenerationResponseObj.ErrorMessage = "Internal Server Error";
                AssistRequestGenerationResponseObj.ErrorMessageDetail = null;
                AssistRequestGenerationResponseObj.status = "Failed";
                return AssistRequestGenerationResponseObj;
            }
        }

        #endregion

        #region PartnerAssistRequest
        public PartnerAssistRequestResponse PartnerAssistRequest(PartnerAssistRequestRequest data)
        {
            try
            {
                if (data != null)
                {
                    PartnerAssistRequestResponse res = new PartnerAssistRequestResponse();

                    res = _logic.PartnerAssistRequest(data);

                    res.ErrorMessageDetail = null;

                    return res;
                }
                else
                {
                    PartnerAssistRequestResponseObj.TransactionID = null;
                    PartnerAssistRequestResponseObj.ErrorMessage = "Internal Server Error";
                    PartnerAssistRequestResponseObj.ErrorMessageDetail = "Request Data is Not in Correct Format, Bad Request";
                    PartnerAssistRequestResponseObj.status = "Failed";

                    return PartnerAssistRequestResponseObj;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                PartnerAssistRequestResponseObj.TransactionID = data.TransactionID;
                PartnerAssistRequestResponseObj.ErrorMessage = "Internal Server Error";
                PartnerAssistRequestResponseObj.ErrorMessageDetail = null;
                PartnerAssistRequestResponseObj.status = "Failed";
                return PartnerAssistRequestResponseObj;
            }
        }

        #endregion

        private bool ValidateToken(string token)
        {
            string tokenString = _logic.DecriptToken(token);

            if (tokenString.Contains("||"))
            {
                int index = tokenString.IndexOf("||");
                string username = tokenString.Substring(0, index);
                string loginDateTime = tokenString.Substring(index + 2);
                string timeout = "180"; //ConfigurationManager.AppSettings["MobileUserLoginTimeoutInMinutes"].ToString();
                int _timeout = Convert.ToInt32(timeout);
                TimeSpan loginTimeout = new TimeSpan(0, _timeout, 0);
                if (DateTime.UtcNow.Subtract(Convert.ToDateTime(loginDateTime)) > loginTimeout)
                    return false;

                return true;
            }
            else
            {
                return false;
            }
        }

        //private string ValidateUser(string username, string password)
        //{
        //    var membership = System.Web.Security.Membership.Providers["RSAProvider"];

        //    Boolean result = false;

        //    result = membership.ValidateUser(username, password);
        //    // return result;

        //    if (!result)
        //    {
        //        return null;
        //    }

        //    string guid = _logic.getUserGUID(username);
        //    if (guid == null)
        //    {
        //        // Error
        //    }
        //    return guid;

        //}

        #region HandleException
        public static bool HandleException(Exception ex)
        {
            bool rethrow = false;
            rethrow = UIExceptionHandler.HandleException(ref ex);
            if (rethrow)
            {
                throw ex;
            }
            return false;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace GlobalAssure.Controllers.API
{
    public class PedalPlanModel
    {
        public decimal PlanID { get; set; }
        public string PlanName { get; set; }
        public decimal? PlanAmount { get; set; }
        public decimal? DiscountedPlanAmount { get; set; }
    }
}
﻿using CsvHelper;
using GlobalAssure.Entity;
using GlobalAssure.Models;
using LinqToExcel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalAssure.Controllers
{
    public class VendorController : Controller
    {

        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();
        Communication _c = new Communication();

        // GET: Vendor
        public ActionResult Index()
        {
            return View();
        }

        #region SearchAssist
        public JsonResult SearchAssist(string Registration, string RefNo)
        {
            try
            {
                List<AssistVendroDetail> list = new List<AssistVendroDetail>();

                ViewBag.Registration = Registration;
                ViewBag.RefNo = RefNo;

                TempData["Load"] = "FirstTime";
                list = this.GetSearchAssistList(Registration, RefNo);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                var Html = RenderPartialViewToString("~/Views/Vendor/_SearchAssistVendorGrid.cshtml", Grid);
                return Json(new { status = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        private List<AssistVendroDetail> GetSearchAssistList(string registration, string refNo)
        {
            List<AssistVendroDetail> list = new List<AssistVendroDetail>();
            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID }).FirstOrDefault();


            list = (from a in db.tblAssistDetails
                    join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID
                    join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                    join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                    from status in objStatus.DefaultIfEmpty()
                    join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction

                    from Action in objAction.DefaultIfEmpty()
                    where ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo))
                    && (a.StatusId == CrossCutting_Constant.StatusID_Vendor_Assigned)
                    && (branch.userid == userdata.Id)
                    )
                    select new AssistVendroDetail
                    {
                        TransactionID = t.TransactionID,
                        ActionId = (int)a.ActionId,
                        Address = a.Address,
                        CreatedDate = a.CreatedDate,
                        Issue = a.Issue,
                        Landmark = a.Landmark,
                        Lat = a.Lat,
                        Lon = a.Lon,
                        PinCode = a.PinCode,
                        RefNo = a.RefNo,
                        Registration = t.RegistrationNo,
                        Remarks = a.Remarks,
                        Status = status.Status,
                        Action = Action.Status,
                        VendorId = a.VendorId,
                        ContactNo = t.CustomerContact,
                        AssistId = a.Id

                    }).ToList();

            return list;
        }
        #endregion

        #region SearchAssistNextPage
        public ActionResult SearchAssistNextPage(int? Page, string Registration, string RefNo, int SortingOrder = 0, int Paging = 5)
        {
            try
            {
                List<AssistVendroDetail> list = new List<AssistVendroDetail>();
                TempData["Load"] = "FirstTime";
                ViewBag.Registration = Registration;
                ViewBag.RefNo = RefNo;
                list = this.GetSearchAssistList(Registration, RefNo);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                var Html = RenderPartialViewToString("~/Views/Vendor/_SearchAssistVendorGridNextPage.cshtml", grid);
                return Json(new { status = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        #endregion


        public JsonResult GetAssistDetailForVendor(string AssitId)
        {
            string HTML = String.Empty;
            try
            {
                string UserName = HttpContext.User.Identity.Name;
                decimal Assitid = Convert.ToDecimal(AssitId);
                AssistVendroDetail assistDetails = this.GetAssistDetails(Assitid);
                HTML = RenderPartialViewToString("~/Views/Vendor/AssistDetailForVendor.cshtml", assistDetails);
                return Json(new { message = CrossCutting_Constant.Success, HTML = HTML }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        private AssistVendroDetail GetAssistDetails(decimal AssitId)
        {
            AssistVendroDetail data = new AssistVendroDetail();
            tblAssistDetail objTran = db.tblAssistDetails.Where(x => x.Id == AssitId).FirstOrDefault();
            data = (from a in db.tblAssistDetails.AsNoTracking()
                    join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                    join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                    from status in objStatus.DefaultIfEmpty()
                    join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                    from Action in objAction.DefaultIfEmpty()
                    join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                    from Incident in objincident.DefaultIfEmpty()
                    where a.Id == AssitId
                    select new AssistVendroDetail
                    {
                        TransactionID = t.TransactionID,
                        Registration = t.RegistrationNo,
                        ContactNo = t.CustomerContact,
                        ActionId = (int)a.ActionId,
                        Address = a.Address,
                        CreatedDate = a.CreatedDate,
                        Issue = a.Issue,
                        Landmark = a.Landmark,
                        Lat = a.Lat,
                        Lon = a.Lon,
                        PinCode = a.PinCode,
                        RefNo = a.RefNo,
                        Remarks = a.Remarks,
                        StatusId = (int)a.StatusId,
                        VendorId = a.VendorId,
                        Action = Action.Status,
                        Status = status.Status,
                        AssistId = a.Id,
                        VehicleColor = a.VehicleColor,
                        VehicleYear = a.VehicleYear,
                        IncidentDetail = Incident.IncidentDetail,
                        ChasisNo = t.ChassisNo,
                        CustomerName = t.CustomerName,
                        EngineNo = t.EngineNo,
                        Make = t.tblMstMake.Name,
                        Model = t.tblMstModel.Name,
                        RegistrationNo = t.RegistrationNo,
                        CertificateNo = t.CertificateNo,
                        CertificateStartDate = t.CoverStartDate,
                        CertificateEndDate = t.CoverEndDate,
                        StsId = (int)a.StatusId

                    }).FirstOrDefault();

            data.lstAction = db.tblMstWorkFlowStatus.Where(x => x.Module == 2).Select(x =>
                new SelectListItem { Text = x.Status, Value = x.StatusID.ToString() }).ToList();

            data.lstStatus = db.tblMstWorkFlowStatus.Where(x => x.Module == 1 && x.StatusID != 8).Select(x =>
                new SelectListItem { Text = x.Status, Value = x.StatusID.ToString() }).ToList();

            return data;

        }

        public JsonResult SaveAssistVendorData(AssistVendroDetail data)
        {
            string AssistanceCompletionToVendorBody = ConfigurationManager.AppSettings["AssistanceCompletionToVendorBody"];
            string AssistanceCompletionToCustomerBody = ConfigurationManager.AppSettings["AssistanceCompletionToCustomerBody"];
            string Message = string.Empty;
            bool MsgSent = false;

            tblAssistDetail objTbl = db.tblAssistDetails.Where(x => x.Id == data.AssistId).FirstOrDefault();

            var vendata = (from vbd in db.tblVendorBranchDetails.AsNoTracking()
                           join v in db.tblMstVendors.AsNoTracking() on vbd.VendorID equals v.VendorID into objVendorBased
                           from objVendor in objVendorBased.DefaultIfEmpty()
                           where vbd.BranchID == objTbl.VendorId
                           select new
                           {
                               VendorMobileNo = objVendor.VendorMobileNo

                           }).FirstOrDefault();

            switch (data.ActionId)
            {
                case CrossCutting_Constant.ActionID_Vendor_Accepted_Request:
                    data.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                    break;
                case CrossCutting_Constant.ActionID_Vendor_Reject_Request:
                    data.StatusId = CrossCutting_Constant.StatusID_Vendor_Not_Assign;
                    break;
            }

            if (data.StsId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
            {
                objTbl.ActionId = data.ActionId;
                objTbl.StatusId = data.StsId;
            }
            else
            {
                objTbl.ActionId = data.ActionId;
                objTbl.StatusId = data.StatusId;
            }

            objTbl.CreatedDate = DateTime.Now;

            db.SaveChanges();

            if (objTbl.StatusId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
            {
                string CustomerMobileNo = objTbl.Issue;
                if (!string.IsNullOrEmpty(CustomerMobileNo))
                {
                    Message = AssistanceCompletionToCustomerBody;

                    MsgSent = _c.SendingSMSMessage(CustomerMobileNo, Message);

                    if (MsgSent.Equals(true))
                    {
                        // Message Send. :: Success
                    }
                    else
                    {
                        // Message Not Send. :: Failed
                    }
                }

                string VendorMobileNo = vendata.VendorMobileNo;
                if (!string.IsNullOrEmpty(VendorMobileNo))
                {
                    Message = AssistanceCompletionToVendorBody;

                    MsgSent = _c.SendingSMSMessage(VendorMobileNo, Message);

                    if (MsgSent.Equals(true))
                    {
                        // Message Send. :: Success
                    }
                    else
                    {
                        // Message Not Send. :: Failed
                    }
                }
            }

            return Json(new { message = CrossCutting_Constant.Success }, JsonRequestBehavior.AllowGet);
        }

        // GET: Vendor
        public ActionResult NewVendor()
        {
            VendorModel model = new VendorModel();

            model.StateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                            .ToDictionary(d => d.StateID, d => d.State);
            model.IsVlid = true;
            return View(model);
        }

        public JsonResult SaveVendorDetails(VendorModel model)
        {
            tblMstVendor objTblVendor = new tblMstVendor();

            if (model.VendorID > 0)
            {
                objTblVendor = db.tblMstVendors.Where(x => x.VendorID == model.VendorID).FirstOrDefault();
            }

            objTblVendor.VendorName = model.VendorName;
            objTblVendor.VendorDOB = model.VendorDOB;
            objTblVendor.VendorEMail = model.VendorEMail;
            objTblVendor.VendorMobileNo = model.VendorMobileNo;
            objTblVendor.VendorPANCard = model.VendorPANCard;
            objTblVendor.VendorAadhaarCard = model.VendorAadhaarCard;
            objTblVendor.VendorState = model.VendorStateID;
            objTblVendor.VendorCity = model.VendorCityID;
            objTblVendor.VendorPincode = model.VendorPincode;
            objTblVendor.VendorLandMark = model.VendorLandMark;
            objTblVendor.VendorAddress1 = model.VendorAddress1;
            objTblVendor.VendorAddress2 = model.VendorAddress2;
            objTblVendor.CompanyName = model.CompanyName;
            objTblVendor.CompanyRegistrationNo = model.CompanyRegistrationNo;
            objTblVendor.CompanyPANCard = model.CompanyPANCard;
            objTblVendor.CompanyGSTIN = model.CompanyGSTIN;
            objTblVendor.CompanyEMail = model.CompanyEMail;
            objTblVendor.CompanyPhoneNo = model.CompanyPhoneNo;
            objTblVendor.CompanyFAX = model.CompanyFAX;
            objTblVendor.CompanyTIN = model.CompanyTIN;
            objTblVendor.CompanyTAN = model.CompanyTAN;
            objTblVendor.CompanyRegisterdOffice = model.CompanyRegisterdOffice;
            objTblVendor.CompanyHeadOffice = model.CompanyHeadOffice;
            objTblVendor.IsVlid = Convert.ToBoolean(model.isActive);

            if (model.VendorID == 0)
            {
                db.tblMstVendors.Add(objTblVendor);
            }
            db.SaveChanges();

            return Json(new { message = CrossCutting_Constant.Success }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VendorBranchDetails()
        {
            BranchDetails model = new BranchDetails();
            model.isValid = true;
            model.StateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                            .ToDictionary(d => d.StateID, d => d.State);
            return View(model);
        }

        public JsonResult SaveBranchDetails(BranchDetails model)
        {
            tblVendorBranchDetail objTblBranch = new tblVendorBranchDetail();
            tblMstVendor objVendor = new tblMstVendor();
            decimal Vendorid = 0;
            string Latitude = string.Empty, Longitude = string.Empty;
            if (model.Latitudes != null)
            {
                Latitude = model.Latitudes.Length >= 12 ? model.Latitudes.Substring(0, 11) : model.Latitudes;
            }

            if (model.Longitudes != null)
            {
                Longitude = model.Longitudes.Length >= 12 ? model.Longitudes.Substring(0, 11) : model.Longitudes;
            }


            string message = string.Empty;
            if (model.BranchID == 0)
            {
                if (!db.AspNetUsers.Where(x => x.Email == model.VendorUserName).Any())
                {

                    if (!string.IsNullOrEmpty(model.VendorID))
                    {
                        Vendorid = Convert.ToDecimal(model.VendorID);
                        objVendor = db.tblMstVendors.Where(x => x.VendorID == Vendorid).FirstOrDefault();

                        objTblBranch.VendorID = Vendorid;
                        objTblBranch.BranchName = model.BranchName;
                        objTblBranch.State = model.StateID;
                        objTblBranch.City = model.CityID;
                        objTblBranch.Pincode = model.Pincode;
                        objTblBranch.LandMark = model.LandMark;
                        objTblBranch.Address1 = model.Address1;
                        objTblBranch.Address2 = model.Address2;
                        objTblBranch.Latitudes = Latitude;
                        objTblBranch.Longitudes = Longitude;
                        objTblBranch.Status = Convert.ToBoolean(model.isActive);
                        objTblBranch.GeoLocation = model.Geo_autocomplete;
                        db.tblVendorBranchDetails.Add(objTblBranch);
                        Guid userid = CreateBrandchUser(objVendor, model.VendorUserName, model.NewPassword);
                        objTblBranch.userid = userid.ToString();
                        db.SaveChanges();

                        return Json(new { status = CrossCutting_Constant.Success }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = CrossCutting_Constant.Failure, message = "Error" }, JsonRequestBehavior.AllowGet);
                    }


                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Failure, message = "UserName already exists" }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                objTblBranch = db.tblVendorBranchDetails.Where(x => x.BranchID == model.BranchID).FirstOrDefault();

                objTblBranch.BranchName = model.BranchName;
                objTblBranch.State = model.StateID;
                objTblBranch.City = model.CityID;
                objTblBranch.Pincode = model.Pincode;
                objTblBranch.LandMark = model.LandMark;
                objTblBranch.Address1 = model.Address1;
                objTblBranch.Address2 = model.Address2;
                objTblBranch.Latitudes = Latitude;
                objTblBranch.Longitudes = Longitude;
                objTblBranch.GeoLocation = model.Geo_autocomplete;
                objTblBranch.Status = Convert.ToBoolean(model.isActive);
                db.SaveChanges();
                return Json(new { status = CrossCutting_Constant.Success }, JsonRequestBehavior.AllowGet);
            }

        }


        #region SearchVendor
        public ActionResult SearchVendors()
        {
            return View();
        }

        public ActionResult SearchVendor(string VendorName)
        {
            try
            {
                ViewBag.VendorName = VendorName;
                TempData["Load"] = "FirstTime";
                IQueryable<VendorModel> list = this.GetVendorList(VendorName);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                return PartialView("~/Views/Vendor/_VendorSearchGrid.cshtml", Grid);
                //return Json(new { status = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        #region SearchAssistNextPage
        public JsonResult SearchVendorNextPage(int? Page, string VendorName, int SortingOrder = 0, int Paging = 5)
        {
            try
            {

                TempData["Load"] = "FirstTime";
                ViewBag.VendorName = VendorName;
                IQueryable<VendorModel> list = this.GetVendorList(VendorName);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                var Html = RenderPartialViewToString("~/Views/Vendor/_VendorSearchGridNextPage.cshtml", grid);
                // return PartialView("~/Views/Vendor/_VendorSearchGridNextPage.cshtml", grid);              
                return Json(new { status = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        #endregion
        private IQueryable<VendorModel> GetVendorList(string VendorName)
        {

            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID }).FirstOrDefault();


            IQueryable<VendorModel> VendorList = db.tblMstVendors.Where(x => x.VendorName.Contains(VendorName) || string.IsNullOrEmpty(VendorName))
                .Select(x => new VendorModel
                {
                    VendorID = x.VendorID,
                    VendorName = x.VendorName,
                    VendorEMail = x.VendorEMail,
                    VendorDOB = x.VendorDOB,
                    VendorMobileNo = x.VendorMobileNo
                }).OrderByDescending(x => x.VendorName);

            return VendorList;
        }

        public JsonResult GetVendorDetails(decimal VendorID)
        {
            string HTML = String.Empty;
            try
            {
                string UserName = HttpContext.User.Identity.Name;
                decimal Assitid = Convert.ToDecimal(VendorID);
                VendorModel VendorDetails = this.VendorDetails(Assitid);
                VendorDetails.StateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                            .ToDictionary(d => d.StateID, d => d.State);
                VendorDetails.CityListItem = db.tblMstCities.AsNoTracking().Where(w => w.IsValid != false && w.StateID == VendorDetails.VendorStateID).Select(s => new { s.CityID, s.City })
                            .ToDictionary(d => d.CityID, d => d.City);
                HTML = RenderPartialViewToString("~/Views/Vendor/NewVendor.cshtml", VendorDetails);
                return Json(new { message = CrossCutting_Constant.Success, HTML = HTML }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        private VendorModel VendorDetails(decimal VendorID)
        {
            VendorModel data = new VendorModel();
            tblMstVendor objTran = db.tblMstVendors.Where(x => x.VendorID == VendorID).FirstOrDefault();
            data.VendorID = objTran.VendorID;
            data.VendorName = objTran.VendorName;
            data.VendorDOB = objTran.VendorDOB;
            data.VendorEMail = objTran.VendorEMail;
            data.VendorMobileNo = objTran.VendorMobileNo;
            data.VendorPANCard = objTran.VendorPANCard;
            data.VendorAadhaarCard = objTran.VendorAadhaarCard;
            data.VendorStateID = (int)objTran.VendorState;
            data.VendorCityID = (int)objTran.VendorCity;
            data.VendorPincode = objTran.VendorPincode;
            data.VendorLandMark = objTran.VendorLandMark;
            data.VendorAddress1 = objTran.VendorAddress1;
            data.VendorAddress2 = objTran.VendorAddress2;
            data.CompanyName = objTran.CompanyName;
            data.CompanyRegistrationNo = objTran.CompanyRegistrationNo;
            data.CompanyPhoneNo = objTran.CompanyPhoneNo;
            data.CompanyFAX = objTran.CompanyFAX;
            data.CompanyPANCard = objTran.CompanyPANCard;
            data.CompanyGSTIN = objTran.CompanyGSTIN;
            data.CompanyEMail = objTran.CompanyEMail;
            data.CompanyTAN = objTran.CompanyTAN;
            data.CompanyTIN = objTran.CompanyTIN;
            data.CompanyHeadOffice = objTran.CompanyHeadOffice;
            data.CompanyRegisterdOffice = objTran.CompanyRegisterdOffice;
            data.isActive = objTran.IsVlid == true ? 1 : 0;
            return data;

        }
        #endregion

        #region SearchBranch
        public ActionResult SearchBranchs()
        {
            return View();
        }

        public ActionResult SearchBranch(string BranchName)
        {
            try
            {
                ViewBag.Registration = BranchName;
                TempData["Load"] = "FirstTime";
                IQueryable<BranchDetails> list = this.GetBranchList(BranchName);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                return PartialView("~/Views/Vendor/_BranchSearchGrid.cshtml", Grid);
                //return Json(new { status = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        #region SearchAssistNextPage
        public JsonResult SearchBranchNextPage(int? Page, string VendorName, int SortingOrder = 0, int Paging = 5)
        {
            try
            {

                TempData["Load"] = "FirstTime";
                ViewBag.Registration = VendorName;
                IQueryable<BranchDetails> list = this.GetBranchList(VendorName);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                var Html = RenderPartialViewToString("~/Views/Vendor/_BranchSearchGridNextPage.cshtml", grid);
                //return PartialView("~/Views/Vendor/_VendorSearchGridNextPage.cshtml", grid);
                return Json(new { status = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);

            }

            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        #endregion
        private IQueryable<BranchDetails> GetBranchList(string BranchName)
        {

            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID }).FirstOrDefault();


            IQueryable<BranchDetails> BranchList = db.tblVendorBranchDetails.Where(x => x.BranchName.Contains(BranchName) || string.IsNullOrEmpty(BranchName))
                .Select(x => new BranchDetails
                {
                    //VendorID = x.VendorID,
                    //VendorName = x.VendorName,
                    //VendorEMail = x.VendorEMail,
                    //VendorDOB = x.VendorDOB,
                    //VendorMobileNo = x.VendorMobileNo

                    BranchID = x.BranchID,
                    VendorID = x.VendorID.ToString(),
                    BranchName = x.BranchName,
                    Address1 = x.Address1,
                    VendorName = x.tblMstVendor.VendorName,

                }).OrderByDescending(x => x.VendorName);

            return BranchList;
        }

        public JsonResult GetBranchDetails(decimal BranchId)
        {
            string HTML = String.Empty;
            try
            {
                string UserName = HttpContext.User.Identity.Name;
                decimal branchid = Convert.ToDecimal(BranchId);
                BranchDetails BranchDetails = this.BranchDetails(branchid);
                BranchDetails.StateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                            .ToDictionary(d => d.StateID, d => d.State);
                BranchDetails.CityListItem = db.tblMstCities.AsNoTracking().Where(w => w.IsValid != false && w.StateID == BranchDetails.StateID).Select(s => new { s.CityID, s.City })
                            .ToDictionary(d => d.CityID, d => d.City);
                HTML = RenderPartialViewToString("~/Views/Vendor/VendorBranchDetails.cshtml", BranchDetails);
                return Json(new { message = CrossCutting_Constant.Success, HTML = HTML }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        private BranchDetails BranchDetails(decimal BranchID)
        {
            BranchDetails data = new BranchDetails();
            tblVendorBranchDetail objTran = db.tblVendorBranchDetails.Where(x => x.BranchID == BranchID).FirstOrDefault();
            data.BranchID = objTran.BranchID;
            data.CompanyName = objTran.tblMstVendor.CompanyName;
            data.VendorName = objTran.tblMstVendor.VendorName + " - " + data.CompanyName;
            data.BranchName = objTran.BranchName;
            data.StateID = objTran.State;
            data.CityID = objTran.City;
            data.Pincode = objTran.Pincode;
            data.Address1 = objTran.Address1;
            data.Address2 = objTran.Address2;
            data.LandMark = objTran.LandMark;
            data.Latitudes = objTran.Latitudes;
            data.Longitudes = objTran.Longitudes;
            data.Geo_autocomplete = objTran.GeoLocation;
            data.isActive = objTran.Status == true ? 1 : 0;
            return data;

        }
        #endregion

        private Guid CreateBrandchUser(tblMstVendor vendor, string username, string password)
        {


            ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));
            var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.VendorReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);

            Guid userId = Guid.NewGuid();
            AspNetUser user = new AspNetUser();
            tblPasswordHistory tblpassword = new tblPasswordHistory();
            user.Id = userId.ToString();
            user.Email = username;
            user.PasswordHash = "ACW4yfCCHVBM+KsZeCqq/9JEc60+fOZ8jolwe1PkcVzuauisevfodbJMsqOpUCUJLA==";
            user.SecurityStamp = Guid.NewGuid().ToString();
            user.PhoneNumber = vendor.VendorMobileNo;
            user.UserName = username;
            user.DOB = vendor.VendorDOB;
            user.IsAdmin = false;
            user.Name = vendor.VendorName;
            user.IsActive = true;
            user.IsFunctionalityLocked = false;
            user.TieUpCompanyID = 4;
            db.AspNetUsers.Add(user);

            tblpassword.UserId = userId.ToString();
            tblpassword.Password = password;

            db.tblPasswordHistories.Add(tblpassword);
            db.SaveChanges();

            _c.SendingMail(user.UserName, user.Email, CrossCutting_Constant.VendorPassword, "NewUser");


            return userId;
        }

        public JsonResult GetVendorName(string UserKey)
        {
            try
            {
                List<SelectListItem> _lstVendorName = db.tblMstVendors.Where(a => a.VendorName.Contains(UserKey) && a.IsVlid == true).Select(a => new SelectListItem { Text = a.VendorName + " - " + a.CompanyName, Value = a.VendorID + "" }).ToList();
                return Json(_lstVendorName, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        public JsonResult GetBranchName(string UserKey)
        {
            try
            {
                List<SelectListItem> _lstVendorName = db.tblVendorBranchDetails.Where(a => a.BranchName.Contains(UserKey) && a.Status == true).Select(a => new SelectListItem { Text = a.BranchName, Value = a.BranchID + "" }).ToList();
                return Json(_lstVendorName, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }



        #region AssistMIS


        public ActionResult AssistMIS()
        {
            return View();
        }

        public void DownloadAssistMISReport()
        {
            try
            {
                string UserName = HttpContext.User.Identity.Name;
                var userid = db.AspNetUsers.Where(w => w.UserName == UserName).Select(x => new { x.Id, x.IsAdmin, x.TieUpCompanyID }).FirstOrDefault();
                decimal BranchID;

                if (userid != null)
                {
                    if (userid.IsAdmin == true)
                    {
                        List<AssistMISReport> _report = new List<AssistMISReport>();

                        _report = (from a in db.tblAssistDetails
                                   join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                                   from status in objStatus.DefaultIfEmpty()
                                   join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                                   from Action in objAction.DefaultIfEmpty()
                                   join venbr in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals venbr.BranchID into objVendorBranch
                                   from VendorBranch in objVendorBranch.DefaultIfEmpty()
                                   select new AssistMISReport
                                   {
                                       Address = a.Address,
                                       Lon = a.Lon,
                                       Lat = a.Lat,
                                       Remarks = a.Remarks,
                                       Registration = a.tblTransaction.RegistrationNo,
                                       RefNo = a.RefNo,
                                       CustomerName = a.tblTransaction.CustomerName,
                                       Plan = a.tblTransaction.tblMstPlan.Name,
                                       Make = a.tblTransaction.tblMstMake.Name,
                                       Model = a.tblTransaction.tblMstModel.Name,
                                       Certificateno = a.tblTransaction.CertificateNo,
                                       CreatedDate = a.CreatedDate,
                                       Customer_Contact = a.tblTransaction.CustomerContact,
                                       Customer_Email = a.tblTransaction.CustomerEmail,
                                       Issue = a.Issue,
                                       Landmark = a.Landmark,
                                       PinCode = a.PinCode,
                                       Action = Action.Status,
                                       Status = status.Status,
                                       VendorBranchName = VendorBranch.BranchName,
                                       AssistanceSummary = a.SummaryDetails,

                                   }).Distinct().ToList();

                        if (_report.Any())
                        {
                            foreach (var reportdata in _report)
                            {
                                if (!string.IsNullOrEmpty(reportdata.VendorBranchName))
                                {
                                    var vendorBranchData = db.tblVendorBranchDetails.Where(w => w.BranchName == reportdata.VendorBranchName).FirstOrDefault();
                                    if (vendorBranchData != null)
                                    {
                                        var vendorData = db.tblMstVendors.Where(w => w.VendorID == vendorBranchData.VendorID).FirstOrDefault();
                                        if (vendorData != null)
                                        {
                                            reportdata.VendorName = vendorData.VendorName;
                                        }
                                    }
                                }
                            }
                        }

                        MemoryStream _MemoryStream = new MemoryStream();
                        string[] HeaderArray = new string[] { "Registration", "Customer Name", "Customer Email", "Customer Contact", "Plan",
                    "Certificateno", "Make", "Model", "RefNo","Issue" ,"Remarks" ,"Address" ,"PinCode" ,"Landmark" ,"Latitude","longitude","Action","Status","CreatedDate","VendorName","VendorBranchName","AssistanceSummary" };


                        if (_report.Any())
                        {
                            using (StreamWriter write = new StreamWriter(_MemoryStream))
                            {
                                using (CsvWriter csw = new CsvWriter(write))
                                {

                                    foreach (var header in HeaderArray)
                                    {
                                        csw.WriteField(header);
                                    }
                                    csw.NextRecord();

                                    foreach (var row in _report)
                                    {
                                        csw.WriteRecord<dynamic>(row);
                                        csw.NextRecord();
                                    }
                                }
                            }

                        }


                        Response.Clear();
                        Response.ClearHeaders();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=Branch_AssistDetails" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + ".csv");
                        Response.Charset = "";
                        Response.ContentType = "application/x-msexcel";
                        Response.BinaryWrite(_MemoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                    else
                    {
                        BranchID = db.tblVendorBranchDetails.Where(x => x.userid == userid.Id.ToString()).Select(x => x.BranchID).FirstOrDefault();

                        List<AssistMISReport> _report = new List<AssistMISReport>();

                        _report = (from a in db.tblAssistDetails
                                   join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                                   from status in objStatus.DefaultIfEmpty()
                                   join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                                   from Action in objAction.DefaultIfEmpty()
                                   join venbr in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals venbr.BranchID into objVendorBranch
                                   from VendorBranch in objVendorBranch.DefaultIfEmpty()
                                   where (a.VendorId == BranchID)
                                   select new AssistMISReport
                                   {
                                       Address = a.Address,
                                       Lon = a.Lon,
                                       Lat = a.Lat,
                                       Remarks = a.Remarks,
                                       Registration = a.tblTransaction.RegistrationNo,
                                       RefNo = a.RefNo,
                                       CustomerName = a.tblTransaction.CustomerName,
                                       Plan = a.tblTransaction.tblMstPlan.Name,
                                       Make = a.tblTransaction.tblMstMake.Name,
                                       Model = a.tblTransaction.tblMstModel.Name,
                                       Certificateno = a.tblTransaction.CertificateNo,
                                       CreatedDate = a.CreatedDate,
                                       Customer_Contact = a.tblTransaction.CustomerContact,
                                       Customer_Email = a.tblTransaction.CustomerEmail,
                                       Issue = a.Issue,
                                       Landmark = a.Landmark,
                                       PinCode = a.PinCode,
                                       Action = Action.Status,
                                       Status = status.Status,
                                       VendorBranchName = VendorBranch.BranchName,
                                       AssistanceSummary = a.SummaryDetails,

                                   }).Distinct().ToList();

                        if (_report.Any())
                        {
                            foreach (var reportdata in _report)
                            {
                                if (!string.IsNullOrEmpty(reportdata.VendorBranchName))
                                {
                                    var vendorBranchData = db.tblVendorBranchDetails.Where(w => w.BranchName == reportdata.VendorBranchName).FirstOrDefault();
                                    if (vendorBranchData != null)
                                    {
                                        var vendorData = db.tblMstVendors.Where(w => w.VendorID == vendorBranchData.VendorID).FirstOrDefault();
                                        if (vendorData != null)
                                        {
                                            reportdata.VendorName = vendorData.VendorName;
                                        }
                                    }
                                }
                            }
                        }

                        MemoryStream _MemoryStream = new MemoryStream();
                        string[] HeaderArray = new string[] { "Registration", "Customer Name", "Customer Email", "Customer Contact", "Plan",
                    "Certificateno", "Make", "Model", "RefNo","Issue" ,"Remarks" ,"Address" ,"PinCode" ,"Landmark" ,"Latitude","longitude", "Action","Status","CreatedDate","VendorName","VendorBranchName","AssistanceSummary" };


                        if (_report.Any())
                        {

                            using (StreamWriter write = new StreamWriter(_MemoryStream))
                            {
                                using (CsvWriter csw = new CsvWriter(write))
                                {

                                    foreach (var header in HeaderArray)
                                    {
                                        csw.WriteField(header);
                                    }
                                    csw.NextRecord();

                                    foreach (var row in _report)
                                    {
                                        csw.WriteRecord<dynamic>(row);
                                        csw.NextRecord();
                                    }
                                }
                            }

                        }


                        Response.Clear();
                        Response.ClearHeaders();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=Branch_AssistDetails" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + ".csv");
                        Response.Charset = "";
                        Response.ContentType = "application/x-msexcel";
                        Response.BinaryWrite(_MemoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
                else
                {
                    BranchID = 0;

                    List<AssistMISReport> _report = new List<AssistMISReport>();

                    _report = (from a in db.tblAssistDetails
                               join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                               from status in objStatus.DefaultIfEmpty()
                               join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                               from Action in objAction.DefaultIfEmpty()
                               join venbr in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals venbr.BranchID into objVendorBranch
                               from VendorBranch in objVendorBranch.DefaultIfEmpty()
                               where (a.VendorId == BranchID)
                               select new AssistMISReport
                               {
                                   Address = a.Address,
                                   Lon = a.Lon,
                                   Lat = a.Lat,
                                   Remarks = a.Remarks,
                                   Registration = a.tblTransaction.RegistrationNo,
                                   RefNo = a.RefNo,
                                   CustomerName = a.tblTransaction.CustomerName,
                                   Plan = a.tblTransaction.tblMstPlan.Name,
                                   Make = a.tblTransaction.tblMstMake.Name,
                                   Model = a.tblTransaction.tblMstModel.Name,
                                   Certificateno = a.tblTransaction.CertificateNo,
                                   CreatedDate = a.CreatedDate,
                                   Customer_Contact = a.tblTransaction.CustomerContact,
                                   Customer_Email = a.tblTransaction.CustomerEmail,
                                   Issue = a.Issue,
                                   Landmark = a.Landmark,
                                   PinCode = a.PinCode,
                                   Action = Action.Status,
                                   Status = status.Status,
                                   VendorBranchName = VendorBranch.BranchName,
                                   AssistanceSummary = a.SummaryDetails,

                               }).Distinct().ToList();

                    if (_report.Any())
                    {
                        foreach (var reportdata in _report)
                        {
                            if (!string.IsNullOrEmpty(reportdata.VendorBranchName))
                            {
                                var vendorBranchData = db.tblVendorBranchDetails.Where(w => w.BranchName == reportdata.VendorBranchName).FirstOrDefault();
                                if (vendorBranchData != null)
                                {
                                    var vendorData = db.tblMstVendors.Where(w => w.VendorID == vendorBranchData.VendorID).FirstOrDefault();
                                    if (vendorData != null)
                                    {
                                        reportdata.VendorName = vendorData.VendorName;
                                    }
                                }
                            }
                        }
                    }

                    MemoryStream _MemoryStream = new MemoryStream();
                    string[] HeaderArray = new string[] { "Registration", "Customer Name", "Customer Email", "Customer Contact", "Plan",
                    "Certificateno", "Make", "Model", "RefNo","Issue" ,"Remarks" ,"Address" ,"PinCode" ,"Landmark" ,"Latitude","longitude", "Action","Status","CreatedDate","VendorName","VendorBranchName","AssistanceSummary" };

                    if (_report.Any())
                    {

                        using (StreamWriter write = new StreamWriter(_MemoryStream))
                        {
                            using (CsvWriter csw = new CsvWriter(write))
                            {

                                foreach (var header in HeaderArray)
                                {
                                    csw.WriteField(header);
                                }
                                csw.NextRecord();

                                foreach (var row in _report)
                                {
                                    csw.WriteRecord<dynamic>(row);
                                    csw.NextRecord();
                                }
                            }
                        }

                    }


                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=Branch_AssistDetails" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + ".csv");
                    Response.Charset = "";
                    Response.ContentType = "application/x-msexcel";
                    Response.BinaryWrite(_MemoryStream.ToArray());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }
        }

        #endregion

        #region RenderPartialViewToString
        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        #region HandleException
        public static bool HandleException(Exception ex)
        {
            bool rethrow = false;
            rethrow = UIExceptionHandler.HandleException(ref ex);
            if (rethrow)
            {
                throw ex;
            }
            return false;
        }
        #endregion

        #region CompanyVendorBulkUpload

        public ActionResult VendorBranchBulkUpload()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMesage = TempData["ErrorMessage"].ToString();
            }
            return View();
        }

        [HttpPost]
        public ActionResult SaveVendorBranchBulkUpload()
        {
            HttpFileCollectionBase FileUpload = Request.Files;
            List<VendorBranchModel> VendorBranch = new List<VendorBranchModel>();
            List<VendorBranchModel> OutputExcel = new List<VendorBranchModel>();
            if (FileUpload != null)
            {
                try
                {
                    if (FileUpload[0].ContentType == CrossCutting_Constant.ContentType1 || FileUpload[0].ContentType == CrossCutting_Constant.ContentType2 || FileUpload[0].ContentType == CrossCutting_Constant.ContentType3)
                    {
                        string filename = FileUpload[0].FileName;
                        string targetpath = Server.MapPath(CrossCutting_Constant.DocLocation);
                        FileUpload[0].SaveAs(targetpath + filename);
                        string pathToExcelFile = targetpath + filename;
                        var excelFile = new ExcelQueryFactory(pathToExcelFile);
                        string SheetName = "Sheet1";
                        var VendorBranchDetails = (from a in excelFile.Worksheet<VendorBranchModel>(SheetName) select a).ToList();                        

                        foreach (var item in VendorBranchDetails)
                        {
                            try
                            {
                                string Message = ValidateVendorDetails(item);
                                if (Message == "")
                                {
                                    if (item.VendorID > 0)//Update Vendor and Branch Details
                                    {
                                        tblMstVendor objTblVendor = new tblMstVendor();
                                        objTblVendor = db.tblMstVendors.Where(x => x.VendorID == item.VendorID).FirstOrDefault();
                                        objTblVendor.VendorName = item.VendorName;
                                        objTblVendor.VendorDOB = item.VendorDOB;
                                        objTblVendor.VendorEMail = item.VendorEMail;
                                        objTblVendor.VendorMobileNo = item.VendorMobileNo;
                                        objTblVendor.VendorPANCard = item.VendorPANCard;
                                        objTblVendor.VendorAadhaarCard = item.VendorAadhaarCard;
                                        var State = db.tblMstStates.Where(x => x.State == item.VendorState).Select(s => s.StateID).FirstOrDefault();
                                        objTblVendor.VendorState = State;
                                        var City = db.tblMstCities.Where(x => x.City == item.VendorCity).Select(s => s.CityID).FirstOrDefault();
                                        objTblVendor.VendorCity = City;
                                        objTblVendor.VendorPincode = item.VendorPincode;
                                        objTblVendor.VendorLandMark = item.VendorLandMark;
                                        objTblVendor.VendorAddress1 = item.VendorAddress1;
                                        objTblVendor.VendorAddress2 = item.VendorAddress2;
                                        objTblVendor.CompanyName = item.CompanyName;
                                        objTblVendor.CompanyRegistrationNo = item.CompanyRegistrationNo;
                                        objTblVendor.CompanyPANCard = item.CompanyPANCard;
                                        objTblVendor.CompanyGSTIN = item.CompanyGSTIN;
                                        objTblVendor.CompanyEMail = item.CompanyEMail;
                                        objTblVendor.CompanyPhoneNo = item.CompanyPhoneNo;
                                        objTblVendor.CompanyFAX = item.CompanyFAX;
                                        objTblVendor.CompanyTIN = item.CompanyTIN;
                                        objTblVendor.CompanyTAN = item.CompanyTAN;
                                        objTblVendor.CompanyRegisterdOffice = item.CompanyRegisterdOffice;
                                        objTblVendor.CompanyHeadOffice = item.CompanyHeadOffice;
                                        objTblVendor.Status = item.Status;
                                        objTblVendor.IsVlid = Convert.ToBoolean(item.isActive);
                                        objTblVendor.AccountNo = item.ACCOUNTNO;
                                        objTblVendor.IFSC_Code = item.IFSC_CODE;
                                        objTblVendor.Beneficiary_Name = item.BENEFICIARY_NAME;
                                        objTblVendor.BankBranchName = item.BANKBRANCHNAME;
                                        objTblVendor.GSTNO = item.GSTNO;
                                        if (item.ISAGREEMENTRECEIVEDWITHKYC != null)
                                        {
                                            string AREEMENTRECIEVED = item.ISAGREEMENTRECEIVEDWITHKYC.ToLower();
                                            if (AREEMENTRECIEVED == "yes")
                                            {
                                                objTblVendor.ISAGREEMENTRECEIVEDWITHKYC = Convert.ToBoolean(1);
                                            }
                                            else if (AREEMENTRECIEVED == "" || AREEMENTRECIEVED == "no")
                                            {
                                                objTblVendor.ISAGREEMENTRECEIVEDWITHKYC = Convert.ToBoolean(0);
                                            }
                                        }
                                        if (item.ISAGREEMENTRECEIVEDWITHKYC != null)
                                        {
                                            string ISCANCELLEDCHEQUE = item.CANCELLEDCHEQUERECIVED.ToLower();
                                            if (ISCANCELLEDCHEQUE == "yes")
                                            {
                                                objTblVendor.ISCANCELLEDCHEQUERECIVED = Convert.ToBoolean(1);
                                            }
                                            else if (ISCANCELLEDCHEQUE == "" || ISCANCELLEDCHEQUE == "no")
                                            {
                                                objTblVendor.ISCANCELLEDCHEQUERECIVED = Convert.ToBoolean(0);
                                            }
                                        }
                                        objTblVendor.VENDOR_CODE = item.VENDOR_CODE;
                                        objTblVendor.Vendor_Category = item.Vendor_Category;
                                        objTblVendor.Service_Type = item.Service_Type;
                                        objTblVendor.Base_Price = item.Base_Price;
                                        objTblVendor.KiloMeters = item.KiloMeters;
                                        objTblVendor.Per_km_Price = item.Per_km_Price;
                                        objTblVendor.Service_Time = item.Service_Time;                                       
                                        db.SaveChanges();
                                        
                                        tblVendorBranchDetail objTblBranch = new tblVendorBranchDetail();
                                        string Latitude = string.Empty, Longitude = string.Empty;
                                        if (item.Latitudes != null)
                                        {
                                            Latitude = item.Latitudes.Length >= 12 ? item.Latitudes.Substring(0, 11) : item.Latitudes;
                                        }

                                        if (item.Longitudes != null)
                                        {
                                            Longitude = item.Longitudes.Length >= 12 ? item.Longitudes.Substring(0, 11) : item.Longitudes;
                                        }
                                        string message = string.Empty;

                                        objTblBranch = db.tblVendorBranchDetails.Where(x => x.VendorID == item.VendorID).FirstOrDefault();
                                        objTblBranch.BranchName = item.BranchName;
                                        var BranchState = db.tblMstStates.Where(x => x.State == item.BranchState).Select(s => s.StateID).FirstOrDefault();
                                        var BranchCity = db.tblMstCities.Where(x => x.City == item.BranchCity).Select(s => s.CityID).FirstOrDefault();
                                        objTblBranch.State = BranchState;
                                        objTblBranch.City = BranchCity;
                                        objTblBranch.Pincode = item.Pincode;
                                        objTblBranch.LandMark = item.LandMark;
                                        objTblBranch.Address1 = item.Address1;
                                        objTblBranch.Address2 = item.Address2;
                                        objTblBranch.Latitudes = Latitude;
                                        objTblBranch.Longitudes = Longitude;
                                        objTblBranch.GeoLocation = item.GeoLocation;
                                        objTblBranch.Status = Convert.ToBoolean(item.isActive);
                                        db.SaveChanges();

                                        item.RESPONSE = CrossCutting_Constant.Success;
                                        item.RESPONSE_REMARK = "Vendor and Branch Details Updated Successfully.";
                                        OutputExcel.Add(item);
                                    }
                                    else                            //Add Vendor and Branch Details
                                    {
                                        tblMstVendor objTblVendor = new tblMstVendor();
                                        string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                                        string sql = "usp_GetVendorCount";
                                        var State = db.tblMstStates.Where(x => x.State == item.BranchState).Select(s => s.StateID).FirstOrDefault();                                        
                                        var City = db.tblMstCities.Where(x => x.City == item.BranchCity).Select(s => s.CityID).FirstOrDefault();
                                        DataSet ds = new DataSet();
                                        using (SqlConnection conn = new SqlConnection(connString))
                                        {
                                            using (SqlDataAdapter da = new SqlDataAdapter())
                                            {
                                                da.SelectCommand = new SqlCommand(sql, conn);
                                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                                da.SelectCommand.Parameters.AddWithValue("@VendorEmail", item.VendorEMail);
                                                da.SelectCommand.Parameters.AddWithValue("@ServiceType", item.Service_Type);
                                                da.SelectCommand.Parameters.AddWithValue("@City", City);
                                                da.SelectCommand.Parameters.AddWithValue("@State", State);
                                                da.SelectCommand.Parameters.AddWithValue("@s_Error_Message", Message);
                                                da.SelectCommand.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;
                                                da.Fill(ds, "data");
                                                Message = Convert.ToString(da.SelectCommand.Parameters["@s_Error_Message"].Value);
                                            }
                                        }

                                        DataTable dt = ds.Tables["data"];
                                        int VendorCount = Convert.ToInt32(dt.Rows[0]["VendorCount"]);
                                        if (VendorCount == 0)
                                        {
                                            objTblVendor.VendorName = item.VendorName;
                                            objTblVendor.VendorDOB = item.VendorDOB;
                                            objTblVendor.VendorEMail = item.VendorEMail;
                                            objTblVendor.VendorMobileNo = item.VendorMobileNo;
                                            objTblVendor.VendorPANCard = item.VendorPANCard;
                                            objTblVendor.VendorAadhaarCard = item.VendorAadhaarCard;
                                            //var State = db.tblMstStates.Where(x => x.State == item.VendorState).Select(s => s.StateID).FirstOrDefault();
                                            objTblVendor.VendorState = State;
                                           // var City = db.tblMstCities.Where(x => x.City == item.VendorCity).Select(s => s.CityID).FirstOrDefault();
                                            objTblVendor.VendorCity = City;
                                            objTblVendor.VendorPincode = item.VendorPincode;
                                            objTblVendor.VendorLandMark = item.VendorLandMark;
                                            objTblVendor.VendorAddress1 = item.VendorAddress1;
                                            objTblVendor.VendorAddress2 = item.VendorAddress2;
                                            objTblVendor.CompanyName = item.CompanyName;
                                            objTblVendor.CompanyRegistrationNo = item.CompanyRegistrationNo;
                                            objTblVendor.CompanyPANCard = item.CompanyPANCard;
                                            objTblVendor.CompanyGSTIN = item.CompanyGSTIN;
                                            objTblVendor.CompanyEMail = item.CompanyEMail;
                                            objTblVendor.CompanyPhoneNo = item.CompanyPhoneNo;
                                            objTblVendor.CompanyFAX = item.CompanyFAX;
                                            objTblVendor.CompanyTIN = item.CompanyTIN;
                                            objTblVendor.CompanyTAN = item.CompanyTAN;
                                            objTblVendor.CompanyRegisterdOffice = item.CompanyRegisterdOffice;
                                            objTblVendor.CompanyHeadOffice = item.CompanyHeadOffice;
                                            objTblVendor.Status = item.Status;
                                            objTblVendor.IsVlid = Convert.ToBoolean(item.isActive);
                                            objTblVendor.AccountNo = item.ACCOUNTNO;
                                            objTblVendor.IFSC_Code = item.IFSC_CODE;
                                            objTblVendor.Beneficiary_Name = item.BENEFICIARY_NAME;
                                            objTblVendor.BankBranchName = item.BANKBRANCHNAME;
                                            objTblVendor.GSTNO = item.GSTNO;
                                            if (item.ISAGREEMENTRECEIVEDWITHKYC != null)
                                            {
                                                string AREEMENTRECIEVED = item.ISAGREEMENTRECEIVEDWITHKYC.ToLower();
                                                if (AREEMENTRECIEVED == "yes")
                                                {
                                                    objTblVendor.ISAGREEMENTRECEIVEDWITHKYC = Convert.ToBoolean(1);
                                                }
                                                else if (AREEMENTRECIEVED == "" || AREEMENTRECIEVED == "no")
                                                {
                                                    objTblVendor.ISAGREEMENTRECEIVEDWITHKYC = Convert.ToBoolean(0);
                                                }
                                            }
                                            if (item.ISAGREEMENTRECEIVEDWITHKYC != null)
                                            {
                                                string ISCANCELLEDCHEQUE = item.CANCELLEDCHEQUERECIVED.ToLower();
                                                if (ISCANCELLEDCHEQUE == "yes")
                                                {
                                                    objTblVendor.ISCANCELLEDCHEQUERECIVED = Convert.ToBoolean(1);
                                                }
                                                else if (ISCANCELLEDCHEQUE == "" || ISCANCELLEDCHEQUE == "no")
                                                {
                                                    objTblVendor.ISCANCELLEDCHEQUERECIVED = Convert.ToBoolean(0);
                                                }
                                            }
                                            objTblVendor.VENDOR_CODE = item.VENDOR_CODE;
                                            objTblVendor.Vendor_Category = item.Vendor_Category;
                                            objTblVendor.Service_Type = item.Service_Type;
                                            objTblVendor.Base_Price = item.Base_Price;
                                            objTblVendor.KiloMeters = item.KiloMeters;
                                            objTblVendor.Per_km_Price = item.Per_km_Price;
                                            objTblVendor.Service_Time = item.Service_Time;                                            

                                            if (item.VendorID == 0)
                                            {
                                                db.tblMstVendors.Add(objTblVendor);
                                            }
                                            db.SaveChanges();


                                            decimal VendorID = objTblVendor.VendorID;
                                            tblVendorBranchDetail objTblBranch = new tblVendorBranchDetail();
                                            tblMstVendor objVendor = new tblMstVendor();
                                            decimal Vendorid = 0;
                                            string Latitude = string.Empty, Longitude = string.Empty;
                                            if (item.Latitudes != null)
                                            {
                                                Latitude = item.Latitudes.Length >= 12 ? item.Latitudes.Substring(0, 11) : item.Latitudes;
                                            }

                                            if (item.Longitudes != null)
                                            {
                                                Longitude = item.Longitudes.Length >= 12 ? item.Longitudes.Substring(0, 11) : item.Longitudes;
                                            }
                                            string message = string.Empty;
                                            if (item.BranchID == 0)
                                            {                                               
                                                if (!db.AspNetUsers.Where(x => x.Email == item.VendorEMail).Any())
                                                {
                                                    Vendorid = Convert.ToDecimal(VendorID);
                                                    objVendor = db.tblMstVendors.Where(x => x.VendorID == Vendorid).FirstOrDefault();
                                                    objTblBranch.VendorID = Vendorid;
                                                    objTblBranch.BranchName = item.BranchName;
                                                    var BranchState = db.tblMstStates.Where(x => x.State == item.BranchState).Select(s => s.StateID).FirstOrDefault();
                                                    var BranchCity = db.tblMstCities.Where(x => x.City == item.BranchCity).Select(s => s.CityID).FirstOrDefault();
                                                    objTblBranch.State = BranchState;
                                                    objTblBranch.City = BranchCity;
                                                    objTblBranch.Pincode = item.Pincode;
                                                    objTblBranch.LandMark = item.LandMark;
                                                    objTblBranch.Address1 = item.Address1;
                                                    objTblBranch.Address2 = item.Address2;
                                                    objTblBranch.Latitudes = Latitude;
                                                    objTblBranch.Longitudes = Longitude;
                                                    objTblBranch.Status = Convert.ToBoolean(item.BranchStatus);
                                                    objTblBranch.GeoLocation = item.GeoLocation;
                                                    db.tblVendorBranchDetails.Add(objTblBranch);
                                                    string Password = ConfigurationManager.AppSettings["GlobalAssurePass"];
                                                    Guid userid = CreateBrandchUser(objVendor, item.VendorEMail, Password);
                                                    objTblBranch.userid = userid.ToString();
                                                    db.SaveChanges();
                                                    //}                                      

                                                    item.RESPONSE = CrossCutting_Constant.Success;
                                                    item.RESPONSE_REMARK = "Vendor Bulk Details Uploded Successfully.";
                                                    OutputExcel.Add(item);
                                                }
                                                else
                                                {

                                                    var VendorUserID = db.AspNetUsers.Where(x => x.Email == item.VendorEMail).FirstOrDefault();
                                                    Vendorid = Convert.ToDecimal(VendorID);
                                                    objVendor = db.tblMstVendors.Where(x => x.VendorID == Vendorid).FirstOrDefault();
                                                    objTblBranch.VendorID = Vendorid;
                                                    objTblBranch.userid = VendorUserID.Id;
                                                    objTblBranch.BranchName = item.BranchName;
                                                    var BranchState = db.tblMstStates.Where(x => x.State == item.BranchState).Select(s => s.StateID).FirstOrDefault();
                                                    var BranchCity = db.tblMstCities.Where(x => x.City == item.BranchCity).Select(s => s.CityID).FirstOrDefault();
                                                    objTblBranch.State = BranchState;
                                                    objTblBranch.City = BranchCity;
                                                    objTblBranch.Pincode = item.Pincode;
                                                    objTblBranch.LandMark = item.LandMark;
                                                    objTblBranch.Address1 = item.Address1;
                                                    objTblBranch.Address2 = item.Address2;
                                                    objTblBranch.Latitudes = Latitude;
                                                    objTblBranch.Longitudes = Longitude;
                                                    objTblBranch.Status = Convert.ToBoolean(item.BranchStatus);
                                                    objTblBranch.GeoLocation = item.GeoLocation;
                                                    db.tblVendorBranchDetails.Add(objTblBranch);
                                                    db.SaveChanges();

                                                    item.RESPONSE = CrossCutting_Constant.Success;
                                                    item.RESPONSE_REMARK = "Vendor Bulk Details Uploaded Successfully.";
                                                    OutputExcel.Add(item);
                                                }
                                            }
                                            else
                                            {
                                                //Update Code for vendor Branch details if Branch ID Exist
                                                objTblBranch = db.tblVendorBranchDetails.Where(x => x.BranchID == item.BranchID).FirstOrDefault();

                                                objTblBranch.BranchName = item.BranchName;
                                                var BranchState = db.tblMstStates.Where(x => x.State == item.BranchState).Select(s => s.StateID).FirstOrDefault();
                                                var BranchCity = db.tblMstCities.Where(x => x.City == item.BranchCity).Select(s => s.CityID).FirstOrDefault();
                                                objTblBranch.State = BranchState;
                                                objTblBranch.City = BranchCity;
                                                objTblBranch.Pincode = item.Pincode;
                                                objTblBranch.LandMark = item.LandMark;
                                                objTblBranch.Address1 = item.Address1;
                                                objTblBranch.Address2 = item.Address2;
                                                objTblBranch.Latitudes = Latitude;
                                                objTblBranch.Longitudes = Longitude;
                                                objTblBranch.GeoLocation = item.GeoLocation;
                                                objTblBranch.Status = Convert.ToBoolean(item.isActive);
                                                db.SaveChanges();

                                                item.RESPONSE = CrossCutting_Constant.Success;
                                                item.RESPONSE_REMARK = "Vendor Bulk Details Uploaded Successfully.";
                                                OutputExcel.Add(item);
                                            }
                                        }
                                        else
                                        {
                                            item.RESPONSE = CrossCutting_Constant.Failure;
                                            item.RESPONSE_REMARK = "Vendor Already Exist with Same Email,Service Type,State and City ";
                                            OutputExcel.Add(item);
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    item.RESPONSE = CrossCutting_Constant.Failure;
                                    item.RESPONSE_REMARK = Message;
                                    OutputExcel.Add(item);
                                }
                            }
                            catch (Exception ex)
                            {
                                HandleException(ex);
                                item.RESPONSE = CrossCutting_Constant.Failure;
                                item.RESPONSE_REMARK = "Error : " + " " + "(" + ex + ")";
                                OutputExcel.Add(item);
                            }


                        }
                        if ((System.IO.File.Exists(pathToExcelFile)))
                        {
                            System.IO.File.Delete(pathToExcelFile);
                        }

                        if (OutputExcel.Any())
                        {
                            MemoryStream _MemoryStream = new MemoryStream();
                            string[] HeaderArray =
                                new string[]
                                {
                                    "VendorName",
                                    "VendorDOB",
                                    "VendorEMail",
                                    "VendorMobileNo",
                                    "VendorPANCard",
                                    "VendorAadhaarCard",
                                    "VendorState",
                                    "VendorCity",
                                    "VendorPincode",
                                    "VendorLandMark",
                                    "VendorAddress1",
                                    "VendorAddress2",
                                    "CompanyName",
                                    "CompanyRegistrationNo",
                                    "CompanyPhoneNo",
                                    "CompanyFAX",
                                    "CompanyPANCard",
                                    "CompanyGSTIN",
                                    "CompanyEMail",
                                    "CompanyTIN",
                                    "CompanyTAN",
                                    "CompanyRegisterdOffice",
                                    "CompanyHeadOffice",
                                    "Status",
                                    "BranchID",
                                    "BranchVendorName",
                                    "BranchName",
                                    "BranchStateID",
                                    "BranchCityID",
                                    "GeoLocation",
                                    "Address1",
                                    "Address2",
                                    "PinCode",
                                    "Latitudes",
                                    "Longitudes",
                                    "BranchStatus",
                                    "isActive",
                                    "ACCOUNTNO",
                                    "IFSC_CODE",
                                    "BENEFICIARY_NAME",
                                    "BANKBRANCHNAME",
                                    "GSTNO",
                                    "ISAGREEMENTRECEIVEDWITHKYC",
                                    "CANCELLEDCHEQUERECIVED_Y_N",
                                    "VENDOR_CODE",
                                    "Vendor_Category",
                                    "Service_Type",
                                    "Base_Price",
                                    "KiloMeters",
                                    "Per_km_Price",
                                    "Service_Time",
                                    "RESPONSE",
                                    "RESPONSE_REMARK"
                                };

                            var listobj = OutputExcel.Select(x => new
                            {
                                x.VendorName,
                                x.VendorDOB,
                                x.VendorEMail,
                                x.VendorMobileNo,
                                x.VendorPANCard,
                                x.VendorAadhaarCard,
                                x.VendorState,
                                x.VendorCity,
                                x.VendorPincode,
                                x.VendorLandMark,
                                x.VendorAddress1,
                                x.VendorAddress2,
                                x.CompanyName,
                                x.CompanyRegistrationNo,
                                x.CompanyPhoneNo,
                                x.CompanyFAX,
                                x.CompanyPANCard,
                                x.CompanyGSTIN,
                                x.CompanyEMail,
                                x.CompanyTIN,
                                x.CompanyTAN,
                                x.CompanyRegisterdOffice,
                                x.CompanyHeadOffice,
                                x.Status,
                                x.BranchID,
                                x.BranchVendorName,
                                x.BranchName,
                                x.BranchState,
                                x.BranchCity,
                                x.GeoLocation,
                                x.Address1,
                                x.Address2,
                                x.Pincode,
                                x.Latitudes,
                                x.Longitudes,
                                x.BranchStatus,
                                x.isActive,
                                x.ACCOUNTNO,
                                x.IFSC_CODE,
                                x.BENEFICIARY_NAME,
                                x.BANKBRANCHNAME,
                                x.GSTNO,
                                x.ISAGREEMENTRECEIVEDWITHKYC,
                                x.CANCELLEDCHEQUERECIVED,
                                x.VENDOR_CODE,
                                x.Vendor_Category,
                                x.Service_Type,
                                x.Base_Price,
                                x.KiloMeters,
                                x.Per_km_Price,
                                x.Service_Time,
                                x.RESPONSE,
                                x.RESPONSE_REMARK
                            }).ToList();

                            using (StreamWriter write = new StreamWriter(_MemoryStream))
                            {
                                using (CsvWriter csw = new CsvWriter(write))
                                {
                                    foreach (var header in HeaderArray)
                                    {
                                        csw.WriteField(header);
                                    }
                                    csw.NextRecord();

                                    foreach (var row in listobj)
                                    {
                                        csw.WriteRecord<dynamic>(row);
                                        csw.NextRecord();
                                    }
                                }
                            }

                            Response.Clear();
                            Response.ClearHeaders();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.AddHeader("content-disposition", "attachment;filename=VendorBulkUploadOutputReport_" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + ".csv");
                            Response.Charset = "";
                            Response.ContentType = "application/x-msexcel";
                            Response.BinaryWrite(_MemoryStream.ToArray());
                            Response.Flush();
                            Response.End();
                        }
                    }
                }
                catch (Exception e)
                {
                    TempData["ErrorMessage"] = "Invalid Data";
                    HandleException(e);
                }

            }

            return RedirectToAction("VendorBranchBulkUpload", "Vendor");
        }

        public string ValidateVendorDetails(VendorBranchModel objVendorDetails)
        {
            string Message = "";
            if (objVendorDetails.VendorName == "" || objVendorDetails.VendorName == null)
            {
                Message = Message + "Vendor name is Mandatory" + ",";
            }
            //if (Convert.ToString(objVendorDetails.VendorDOB) == "" || objVendorDetails.VendorDOB == null)
            //{
            //    Message = Message + "Vendor DOB is Mandatory" + ",";
            //}
            if (Convert.ToString(objVendorDetails.VendorEMail) == "" || objVendorDetails.VendorEMail == null)
            {
                Message = Message + "Vendor EmailID is Mandatory" + ",";
            }
            if (objVendorDetails.VendorMobileNo != null)
            {
                if (objVendorDetails.VendorMobileNo.Length > 14)
                {
                    Message = Message + "Invalid Mobile No" + ",";
                }
                else
                {

                }

            }

            if (Convert.ToString(objVendorDetails.VendorMobileNo) == "" || objVendorDetails.VendorMobileNo == null)
            {
                Message = Message + "Vendor MobileNo is Mandatory" + ",";
            }
            //if (Convert.ToString(objVendorDetails.VendorPANCard) == "" || objVendorDetails.VendorPANCard == null)
            //{
            //    Message = Message + "Vendor PanCard is Mandatory" + ",";
            //}
            if (Convert.ToString(objVendorDetails.VendorState) == "" || objVendorDetails.VendorState == null)
            {
                Message = Message + "Vendor State is Mandatory" + ",";
            }
            if (Convert.ToString(objVendorDetails.VendorCity) == "" || objVendorDetails.VendorCity == null)
            {
                Message = Message + "Vendor City is Mandatory" + ",";
            }
            if (Convert.ToString(objVendorDetails.VendorPincode) == "" || objVendorDetails.VendorPincode == null)
            {
                Message = Message + "Vendor Pincode is Mandatory" + ",";
            }
            if (Convert.ToString(objVendorDetails.VendorLandMark) == "" || objVendorDetails.VendorLandMark == null)
            {
                Message = Message + "Vendor LandMark is Mandatory" + ",";
            }
            if (Convert.ToString(objVendorDetails.VendorAddress1) == "" || objVendorDetails.VendorAddress1 == null)
            {
                Message = Message + "Vendor Address1 is Mandatory" + ",";

            }
            //if (Convert.ToString(objVendorDetails.VendorAddress2) == "" || objVendorDetails.VendorAddress2 == null)
            //{
            //    Message = Message + "Vendor Address2 is Mandatory" + ",";
            //}
            if (Convert.ToString(objVendorDetails.CompanyName) == "" || objVendorDetails.CompanyName == null)
            {
                Message = Message + "Company Name is Mandatory" + ",";
            }
            //if (Convert.ToString(objVendorDetails.CompanyRegistrationNo) == "" || objVendorDetails.CompanyRegistrationNo == null)
            //{
            //    Message += "Company RegistrationNo is Mandatory";
            //    return Message;
            //}
            if (Convert.ToString(objVendorDetails.CompanyRegisterdOffice) == "" || objVendorDetails.CompanyRegisterdOffice == null)
            {
                Message = Message + "Company Registerd Office is Mandatory" + ",";
            }
            //if (Convert.ToString(objVendorDetails.CompanyPhoneNo) == "" || objVendorDetails.CompanyPhoneNo == null)
            //{
            //    Message = Message + "Company PhoneNo is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.CompanyFAX) == "" || objVendorDetails.CompanyFAX == null)
            //{
            //    Message += "Company FAX is Mandatory";
            //    return Message;
            //}
            //if (Convert.ToString(objVendorDetails.CompanyPANCard) == "" || objVendorDetails.CompanyPANCard == null)
            //{
            //    Message += "Company PANCard is Mandatory";
            //    return Message;
            //}
            //if (Convert.ToString(objVendorDetails.CompanyGSTIN) == "" || objVendorDetails.CompanyGSTIN == null)
            //{
            //    Message += "Company GSTIN is Mandatory";
            //    return Message;
            //}
            //if (Convert.ToString(objVendorDetails.CompanyEMail) == "" || objVendorDetails.CompanyEMail == null)
            //{
            //    Message = Message + "Company EMail is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.CompanyTIN) == "" || objVendorDetails.CompanyTIN == null)
            //{
            //    Message += "Company TIN is Mandatory";
            //    return Message;
            //}
            //if (Convert.ToString(objVendorDetails.CompanyTAN) == "" || objVendorDetails.CompanyTAN == null)
            //{
            //    Message += "Company TAN is Mandatory";
            //    return Message;
            //}
            if (Convert.ToString(objVendorDetails.CompanyHeadOffice) == "" || objVendorDetails.CompanyHeadOffice == null)
            {
                Message = Message + "Company HeadOffice is Mandatory" + ",";
            }
            //if (Convert.ToString(objVendorDetails.Status) == "" || objVendorDetails.Status == null)
            //{
            //    Message = Message + "Status is Mandatory" + ",";
            //}
            if (Convert.ToString(objVendorDetails.BranchID) == "" || objVendorDetails.BranchID == null)
            {
                Message = Message + "BranchID is Mandatory" + ",";

            }
            if (Convert.ToString(objVendorDetails.BranchVendorName) == "" || objVendorDetails.BranchVendorName == null)
            {
                Message = Message + "Branch Vendor Name is Mandatory" + ",";

            }
            if (Convert.ToString(objVendorDetails.BranchName) == "" || objVendorDetails.BranchName == null)
            {
                Message = Message + "Branch Name is Mandatory" + ",";

            }
            if (Convert.ToString(objVendorDetails.BranchState) == "" || objVendorDetails.BranchState == null)
            {
                Message = Message + "Branch StateID is Mandatory" + ",";
            }
            if (Convert.ToString(objVendorDetails.BranchCity) == "" || objVendorDetails.BranchCity == null)
            {
                Message = Message + "Branch CityID is Mandatory" + ",";
            }
            //if (Convert.ToString(objVendorDetails.VendorUserName) == "" || objVendorDetails.VendorUserName == null)
            //{
            //    Message = Message + "Vendor UserName is Mandatory";
            //}
            //if (Convert.ToString(objVendorDetails.Password) == "" || objVendorDetails.Password == null)
            //{
            //    Message = Message + "Password is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.ConfirmPassword) == "" || objVendorDetails.ConfirmPassword == null)
            //{
            //    Message = Message + "Confirm Password is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.GeoLocation) == "" || objVendorDetails.GeoLocation == null)
            //{
            //    Message = Message + "GeoLocation is Mandatory" + ",";
            //}
            if (Convert.ToString(objVendorDetails.Address1) == "" || objVendorDetails.Address1 == null)
            {
                Message = Message + "Address1 is Mandatory" + ",";
            }
            //if (Convert.ToString(objVendorDetails.Address2) == "" || objVendorDetails.Address2 == null)
            //{
            //    Message = Message + "Address2 is Mandatory" + ",";
            //}
            if (Convert.ToString(objVendorDetails.Pincode) == "" || objVendorDetails.Pincode == null)
            {
                Message = Message + "Pincode is Mandatory" + ",";
            }
            if (Convert.ToString(objVendorDetails.LandMark) == "" || objVendorDetails.LandMark == null)
            {
                Message = Message + "LandMark is Mandatory" + ",";
            }
            //if (Convert.ToString(objVendorDetails.Latitudes) == "" || objVendorDetails.Latitudes == null)
            //{
            //    Message = Message + "Latitudes is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.Longitudes) == "" || objVendorDetails.Longitudes == null)
            //{
            //    Message = Message + "Longitudes is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.BranchStatus) == "" || objVendorDetails.BranchStatus == null)
            //{
            //    Message = Message + "BranchStatus is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.Password) != Convert.ToString(objVendorDetails.ConfirmPassword))
            //{
            //    Message = Message + "Password Mismatched" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.ACCOUNTNO) == "" || objVendorDetails.ACCOUNTNO == null)
            //{
            //    Message = Message + "ACCOUNT NO is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.IFSC_CODE) == "" || objVendorDetails.IFSC_CODE == null)
            //{
            //    Message = Message + "IFSC_CODE is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.BENEFICIARY_NAME) == "" || objVendorDetails.BENEFICIARY_NAME == null)
            //{
            //    Message = Message + "BENEFICIARY NAME is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.BANKBRANCHNAME) == "" || objVendorDetails.BANKBRANCHNAME == null)
            //{
            //    Message = Message + "BANK BRANCH NAME is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.GSTNO) == "" || objVendorDetails.GSTNO == null)
            //{
            //    Message = Message + "GSTNO is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.ISAGREEMENTRECEIVEDWITHKYC) == "" || objVendorDetails.ISAGREEMENTRECEIVEDWITHKYC == null)
            //{
            //    Message = Message + "IS AGREEMENT RECEIVED WITH KYC is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.CANCELLEDCHEQUERECIVED) == "" || objVendorDetails.CANCELLEDCHEQUERECIVED == null)
            //{
            //    Message = Message + "CANCELLED CHEQUE RECIVED is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.VENDOR_CODE) == "" || objVendorDetails.VENDOR_CODE == null)
            //{
            //    Message = Message + "VENDOR CODE is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.Vendor_Category) == "" || objVendorDetails.Vendor_Category == null)
            //{
            //    Message = Message + "Vendor Category is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.Service_Type) == "" || objVendorDetails.Service_Type == null)
            //{
            //    Message = Message + "Service Type is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.Base_Price) == "" || objVendorDetails.Base_Price == null)
            //{
            //    Message = Message + "Base Price is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.KiloMeters) == "" || objVendorDetails.KiloMeters == null)
            //{
            //    Message = Message + "KiloMeters is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.Per_km_Price) == "" || objVendorDetails.Per_km_Price == null)
            //{
            //    Message = Message + "Per km Price is Mandatory" + ",";
            //}
            //if (Convert.ToString(objVendorDetails.Service_Time) == "" || objVendorDetails.Service_Time == null)
            //{
            //    Message = Message + "Service Time is Mandatory" + ",";
            //}

            var State = db.tblMstStates.Where(x => x.State == objVendorDetails.VendorState).Select(s => s.StateID).FirstOrDefault();
            var City = db.tblMstCities.Where(x => x.City == objVendorDetails.VendorCity).Select(s => s.CityID).FirstOrDefault();
            if (State == 0 || City == 0)
            {
                Message = Message + "Invalid Vendor State/City as per MasterData" + ",";
            }
            var BranchState = db.tblMstStates.Where(x => x.State == objVendorDetails.BranchState).Select(s => s.StateID).FirstOrDefault();
            var BranchCity = db.tblMstCities.Where(x => x.City == objVendorDetails.BranchCity).Select(s => s.CityID).FirstOrDefault();
            if (BranchState == 0 || BranchCity == 0)
            {
                Message = Message + "Invalid Branch State/City as per MasterData" + ",";
            }


            return Message;
        }

        #endregion
    }
}
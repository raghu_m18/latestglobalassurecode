﻿using GlobalAssure.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalAssure.Controllers
{
    public class CertificateController : Controller
    {
        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();

        // GET: Certificate
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GenerateCertificate(string UserName, string ProductName)
        {
            string ResponseCode = string.Empty;
            string ResponseMessage = string.Empty;
            string CertificateNo = string.Empty;
            string Prifix = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(UserName))
                {
                    ResponseCode = "-1";
                    ResponseMessage = "UserName is required.";
                    return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(ProductName))
                {
                    ResponseCode = "-1";
                    ResponseMessage = "ProductName is required.";
                    return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
                }

                var userdata = db.AspNetUsers.Where(w => w.UserName == UserName && w.IsAdmin != true).Select(s => new { s.Id, s.UserName,s.Prefix, s.IsActive }).FirstOrDefault();

                if (userdata == null)
                {
                    ResponseCode = "-1";
                    ResponseMessage = "UserName '" + UserName + "' is not available in the system.";
                    return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
                }

                if(string.IsNullOrEmpty(userdata.Prefix))
                {
                    ResponseCode = "-1";
                    ResponseMessage = "Prefix is not available for User '" + UserName + "' .";
                    return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
                }

                Prifix = userdata.Prefix.Trim();

                var productdata = db.tblMstProducts.Where(w => w.IsValid != false && w.ProductName == ProductName).FirstOrDefault();
                if(productdata == null)
                {
                    ResponseCode = "-1";
                    ResponseMessage = "ProductName '" + ProductName + "' is not available in the system.";
                    return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
                }

                List<string> ProductforUser = db.tblUserPrivilegeDetails.Where(w => w.IsValid != false && w.UserID == userdata.Id && w.Product != null && w.Product != "")
                                            .Select(s => s.Product.ToLower()).Distinct().ToList();

                if (ProductforUser.Any())
                {
                    int productForUserhaveALL = ProductforUser.Where(w => w.ToLower() == "all").Count();
                    if (productForUserhaveALL == 0)
                    {
                        if(ProductforUser.Where(w=> w == ProductName.ToLower()).FirstOrDefault() == null)
                        {
                            ResponseCode = "-1";
                            ResponseMessage = "User '" + UserName + "' do not have privilege for product '" + ProductName + "'";
                            return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    ResponseCode = "-1";
                    ResponseMessage = "User '" + UserName + "' do not have privilege for product '" + ProductName + "'";
                    return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
                }


                ObjectParameter Output = new ObjectParameter("UniqueCode", typeof(string));
                var UniqueCodeNo = db.usp_UniqeNumberGeneration(CrossCutting_Constant.BulkCertificatePrefix, "", "", "", "", "", Output);                
                CertificateNo = Prifix + Convert.ToString(ProductName) + Convert.ToString(Output.Value);

                db.tblGeneratedCertificateAuditLogs.Add(new tblGeneratedCertificateAuditLog {
                    IsValid = true,
                    CreatedDate = DateTime.Now,
                    UserID = userdata.Id,
                    ProductID = productdata.ProductID,
                    ProductName = productdata.ProductName,
                    CertificateNo = CertificateNo
                });

                db.SaveChanges();
                ResponseCode = "0";
                ResponseMessage = "Successfully Generated CertificateNo : " + CertificateNo;
                return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ResponseCode = "-1";
                ResponseMessage = "something went wrong. please try again.";
                HandleException(ex);
                return Json(new { ResponseCode = ResponseCode, ResponseMessage = ResponseMessage, CertificateNo = CertificateNo }, JsonRequestBehavior.AllowGet);
            }
        }

        public static bool HandleException(Exception ex)
        {
            bool rethrow = false;
            rethrow = UIExceptionHandler.HandleException(ref ex);
            if (rethrow)
            {
                throw ex;
            }
            return false;
        }

    }
}
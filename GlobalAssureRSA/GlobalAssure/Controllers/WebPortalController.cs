﻿using CsvHelper;
using Fonet;
using GlobalAssure.Entity;
using GlobalAssure.Models;
using LinqToExcel;
using log4net;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace GlobalAssure.Controllers
{
    public class WebPortalController : Controller
    {
        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();
        ILog logger1 = LogManager.GetLogger("OnlinePayment");
        Communication _c = new Communication();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        HomeController objHome = new HomeController();
        AssistanceController objAssist = new AssistanceController();

        // GET: PolicyBazaar
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error(string Error)
        {
            ViewBag.Error = Error;
            return View();
        }

        #region issueCertificate
        public async System.Threading.Tasks.Task<ActionResult> issueCertificate(string data)
        {
            TransactionModel model = new TransactionModel();
            GlobalAssureEncryption enc = new GlobalAssureEncryption();
            CertificateRequest request = new CertificateRequest();
            // Request Parameter Samples
            // string encriptMessage = enc.Encrypt("FName=Virat&LName=Kohli&Email=sharafudheen@inubesolutions.com&Mobile=9999999999&Make=KTM&Model=DUKE&Reg=KA01KH1234&User=policyBazaar&Pass=Assist@123");

            string DecriptMessage = enc.Decrypt(data);
            string[] SplitMessage = DecriptMessage.Split('&');
            string[] para = new string[10];

            for (int i = 0; i < SplitMessage.Length; i++)
            {
                string[] str = SplitMessage[i].Split('=');
                para[i] = str[1];
            }

            request.FName = para[0] == "" ? null : para[0];
            request.LName = para[1] == "" ? null : para[1];
            request.Email = para[2] == "" ? null : para[2];
            request.Mobile = para[3] == "" ? null : para[3];
            request.Make = para[4] == "" ? null : para[4];
            request.Model = para[5] == "" ? null : para[5];
            request.Reg = para[6] == "" ? null : para[6];
            request.User = para[7] == "" ? null : para[7];
            request.Password = para[8] == "" ? null : para[8].TrimStart('\0');
            request.MName = para[9] == "" ? null : para[9].TrimStart('\0');
            try
            {
                if (string.IsNullOrEmpty(request.User) || string.IsNullOrEmpty(request.Password))
                {

                    ViewBag.Error = "Invalid User";
                    return View("~/Views/WebPortal/Error.cshtml");
                }
                else
                {
                    bool validUser = await isValidUser(request.User, request.Password);

                    if (validUser)
                    {
                        //    string.IsNullOrEmpty(request.Mobile) || string.IsNullOrEmpty(request.Make) || string.IsNullOrEmpty(request.Model)
                        //|| string.IsNullOrEmpty(request.Reg) ||

                        string UserName = request.User;
                        string ValidRequest = string.Empty;

                        model = GetIssueCertificate(UserName, request.Make, request.Model, ref ValidRequest);
                        model.CustomerName = request.FName;
                        model.CustomerMiddleName = request.MName;
                        model.CustomerLastName = request.LName;
                        model.CustomerContact = request.Mobile;
                        model.CustomerEmail = request.Email;
                        model.RegistrationNo = request.Reg;
                        model.IssueByID = CrossCutting_Constant.IssueBy_Application_ID;
                        return View(model);
                    }
                    else
                    {

                        ViewBag.Error = "Invalid User";
                        return View("~/Views/WebPortal/Error.cshtml");


                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }


        }
        #endregion

        #region SaveIssueCertificate
        [HttpPost]
        public ActionResult SaveIssueCertificate(TransactionModel transmodel)
        {
            bool IsPayNow = false;
            bool onlinePay = false;
            string CertificateNo = "";
            string PaymentRefNo = "";
            string Path = "";



            vPayOnlinePaymentResponseModel vPayResponse = new vPayOnlinePaymentResponseModel();
            try
            {
                if (ModelState.IsValid)
                {

                    DateTime CurrentDate = DateTime.Now;
                    tblTransaction model = new tblTransaction();
                    tblTransactionAuditLog audit = new tblTransactionAuditLog();
                    tblChequeAuditLog chequeaudit = new tblChequeAuditLog();
                    var productname = db.tblMstProducts.AsNoTracking().Where(w => w.ProductID == transmodel.ProductID).Select(s => s.ProductName).FirstOrDefault();

                    string UserName = User.Identity.Name;
                    AspNetUser userdata = db.AspNetUsers.Where(w => w.UserName == UserName).FirstOrDefault();

                    ObjectParameter Output = new ObjectParameter("UniqueCode", typeof(string));
                    var UniqueCodeNo = db.usp_UniqeNumberGeneration(CrossCutting_Constant.CertificatePrefix, "", "", "", "", "", Output);
                    model.CertificateNo = Convert.ToString(productname) + Convert.ToString(Output.Value);

                    model.IssueByID = transmodel.IssueByID;
                    model.UserID = userdata.Id;
                    model.CreatedDate = CurrentDate;
                    model.IsValid = true;
                    model.ProductID = transmodel.ProductID;
                    model.PlanID = transmodel.PlanID;
                    model.MakeID = transmodel.MakeID;
                    model.ModelID = transmodel.ModelID;
                    model.EngineNo = transmodel.EngineNo;
                    model.ChassisNo = transmodel.ChassisNo;
                    model.RegistrationNo = transmodel.RegistrationNo;
                    model.RegistrationDate = transmodel.RegistrationDate;
                    model.CustomerName = transmodel.CustomerName;
                    model.CustomerMiddleName = transmodel.CustomerMiddleName;
                    model.CustomerLastName = transmodel.CustomerLastName;
                    model.CustomerEmail = transmodel.CustomerEmail;
                    model.CustomerContact = transmodel.CustomerContact;
                    // Start date
                    if (userdata.RiskStartCount > 0)
                    {
                        int RiskStartCount = userdata.RiskStartCount;
                        string RiskDate = Convert.ToString(transmodel.CoverStartDate);
                        DateTime RiskStartDate = Convert.ToDateTime(RiskDate);
                        model.CoverStartDate = RiskStartDate.AddDays(RiskStartCount);
                    }
                    else
                    {
                        model.CoverStartDate = transmodel.CoverStartDate;
                    }
                    
                   //End Date
                    if (userdata.RiskStartCount > 0)
                    {
                        int RiskStartCount = userdata.RiskStartCount;
                        string RiskDate = Convert.ToString(transmodel.CoverEndDate);
                        DateTime RiskEndDate = Convert.ToDateTime(RiskDate);
                        model.CoverEndDate = RiskEndDate.AddDays(RiskStartCount);
                    }
                    else
                    {
                        model.CoverEndDate = transmodel.CoverEndDate;
                    }


                    model.IssueDate = CurrentDate;

                    if (transmodel.ChkIsSameAsPermanentAddress)
                    {
                        model.PermanentCityID = transmodel.PermanentCityID;
                        model.PermanentAddress = transmodel.PermanentAddress;
                        model.PermanentAddress2 = transmodel.PermanentAddress2;
                        model.PermanentAddress3 = transmodel.PermanentAddress3;
                        model.PermanentLandmark = transmodel.PermanentLandmark;
                        model.CommunicationCityID = transmodel.PermanentCityID;
                        model.CommunicationAddress = transmodel.PermanentAddress;
                        model.CommunicationAddress2 = transmodel.PermanentAddress2;
                        model.CommunicationAddress3 = transmodel.PermanentAddress3;
                        model.CommunicationLandmark = transmodel.PermanentLandmark;
                        model.IsSameAsPermanentAddress = true;
                    }
                    else
                    {
                        model.PermanentCityID = transmodel.PermanentCityID;
                        model.PermanentAddress = transmodel.PermanentAddress;
                        model.PermanentAddress2 = transmodel.PermanentAddress2;
                        model.PermanentAddress3 = transmodel.PermanentAddress3;
                        model.PermanentLandmark = transmodel.PermanentLandmark;
                        model.CommunicationCityID = transmodel.CommunicationCityID;
                        model.CommunicationAddress = transmodel.CommunicationAddress;
                        model.CommunicationAddress2 = transmodel.CommunicationAddress2;
                        model.CommunicationAddress3 = transmodel.CommunicationAddress3;
                        model.CommunicationLandmark = transmodel.CommunicationLandmark;
                        model.IsSameAsPermanentAddress = false;
                    }

                    model.TransactionTypeID = transmodel.TransactionTypeID;
                    model.PaymentID = CrossCutting_Constant.Payment_Online_ID;



                    decimal? CurrentPlanAmount = db.tblMstPlans.Where(w => w.PlanID == transmodel.PlanID).Select(s => s.Total).FirstOrDefault();


                    audit.UserID = model.UserID;
                    audit.StatusID = model.StatusID;
                    audit.Remark = model.Remark;
                    audit.ChequeNo = model.ChequeNo;
                    audit.ChequeDate = model.ChequeDate;
                    audit.ChequeAmount = model.ChequeAmount;
                    audit.IsValid = true;
                    audit.CreatedDateTime = CurrentDate;
                    audit.CertificateNo = model.CertificateNo;
                    model.tblTransactionAuditLogs.Add(audit);
                    db.tblTransactions.Add(model);
                    db.SaveChanges();

                    CertificateNo = model.CertificateNo;
                    PaymentRefNo = model.PaymentReferenceNo;

                    // Vpay Online Payment

                    TempData["Transactionid"] = model.TransactionID;
                    if (!string.IsNullOrEmpty(CertificateNo))
                    {
                        TempData["CertificateNo"] = model.CertificateNo;
                    }



                    onlinePay = true;
                    ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));
                    var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.PaymentReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);

                    OnlinePaymentModel onlinepaymentmodel = new OnlinePaymentModel();
                    onlinepaymentmodel.PayerType = CrossCutting_Constant.PayerType_Agent;
                    onlinepaymentmodel.Amount = Convert.ToDecimal(CurrentPlanAmount);
                    onlinepaymentmodel.PaymentReferenceNo = Convert.ToString(OutputOfPaymentReference.Value);
                    onlinepaymentmodel.CreatedBy = userdata.Id;
                    onlinepaymentmodel.TransactionIDs = new List<decimal> { model.TransactionID };
                    onlinepaymentmodel.CustomerName = transmodel.CustomerName;
                    onlinepaymentmodel.ContactNo = Convert.ToString(transmodel.CustomerContact);
                    onlinepaymentmodel.Email = transmodel.CustomerEmail;

                    vPayResponse = this.MakeVPayOnlinePayment(onlinepaymentmodel);
                    model.StatusID = CrossCutting_Constant.StatusID_Pending_Payment;

                    if (!string.IsNullOrEmpty(CertificateNo))
                    {
                        Path = objHome.GetCertificatePath(model.TransactionID);
                    }

                    //if (vPayResponse.vPayResponseCode == "200" && vPayResponse.vPayResponseCodeDescription == "Success")
                    //{

                    //    string url = vPayResponse.vPayTransactionURL;
                    //    _c.SendingPaymentLinkMail(transmodel.CustomerEmail, url);
                    //}
                    db.SaveChanges();

                    return Json(new { status = CrossCutting_Constant.Success, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path, IsPayNow = true, vPayResponse = vPayResponse }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Failure, certificateno = "", paymentrefno = "", path = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region MakeVPayOnlinePayment
        public vPayOnlinePaymentResponseModel MakeVPayOnlinePayment(OnlinePaymentModel model)
        {
            vPayOnlinePaymentResponseModel _vPayOnlinePaymentResponseModel = new vPayOnlinePaymentResponseModel();
            vPayOnlinePaymentModel _vPayOnlinePaymentModel = new vPayOnlinePaymentModel();

            tblUserPrivilegeDetail tblUserPrivilegeDetail = new tblUserPrivilegeDetail();

            try
            {
                decimal TotalFinalAmount = 0;

                DateTime CurrentDate = DateTime.Now;
                string ApplicationURL = System.Configuration.ConfigurationManager.AppSettings["ApplicationURL"];
                //model.ResponseURL = ApplicationURL + "WebPortal/OnlinePaymentResponse/";
                model.ResponseURL = ApplicationURL + "WebPortal/OnlineRPPaymentResponse/";

                bool IsProductionEnvironment = true;//Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsProductionEnvironment"]);
                string finalPremiumToCollect = Convert.ToDecimal(2).ToString("##.00");
                if (IsProductionEnvironment)
                {
                    //finalPremiumToCollect = Convert.ToDecimal(model.Amount).ToString("##.00");

                    decimal FinalAmount = 0;
                    foreach (var item in model.TransactionIDs)
                    {
                        var data = (from t in db.tblTransactions.AsNoTracking()
                                    join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id into userbased
                                    from userbasedobj in userbased.DefaultIfEmpty()
                                    join plans in db.tblMstPlans.AsNoTracking() on t.PlanID equals plans.PlanID into plansbased
                                    from plansbasedobj in plansbased.DefaultIfEmpty()
                                    where t.TransactionID == item
                                    select new
                                    {
                                        UserId = t.UserID,
                                        IsGSTNoFound = (!string.IsNullOrEmpty(userbasedobj.GSTno)) ? true : false,
                                        PlanName = plansbasedobj.Name,
                                        PlanTotalAmount = plansbasedobj.Total,
                                        Amount = plansbasedobj.Amount

                                    }).FirstOrDefault();

                        if (data != null)
                        {
                            decimal GST = Convert.ToDecimal(ConfigurationManager.AppSettings["GSTPer"]);
                            decimal TDS = Convert.ToDecimal(ConfigurationManager.AppSettings["TDSPer"]);
                            decimal TotalPlanAmount = 0;
                            decimal PlanBaseAmount = 0;
                            decimal TotalCommissionAmount = 0;
                            decimal TotalCommissionAmountForMIS = 0;
                            decimal CommissionPercentage = 0;
                            bool? IsCommsn = null;
                            string Mode = string.Empty;
                            tblUserPrivilegeDetail = db.tblUserPrivilegeDetails.Where(w => w.UserID == data.UserId && w.Plans == data.PlanName && (w.IsValid != false || w.IsOnHold == 1)).FirstOrDefault();
                            if (tblUserPrivilegeDetail != null)
                            {
                                // TotalAmount Calculation For Payment START
                                if (tblUserPrivilegeDetail.IsCommision != null)
                                {
                                    bool IsCommision = Convert.ToBoolean(tblUserPrivilegeDetail.IsCommision);
                                    if (IsCommision.Equals(true))
                                    {
                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmount = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmount * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmount * GST) / 100;

                                            TotalCommissionAmount = (TotalCommissionAmount - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmount;
                                        }
                                        else
                                        {
                                            TotalCommissionAmount = TotalCommissionAmount - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }

                                        FinalAmount = TotalPlanAmount;
                                    }
                                }
                                else
                                {
                                    bool IsCommision = false;
                                    if (IsCommision.Equals(false))
                                    {
                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }

                                        FinalAmount = TotalPlanAmount;
                                    }
                                }

                                TotalFinalAmount += FinalAmount;
                                // TotalAmount Calculation For Payment END

                                //---------------------------------------------//

                                Mode = tblUserPrivilegeDetail.Mode;

                                // TotalAmount Calculation For CommisionReportMIS START
                                if (tblUserPrivilegeDetail.IsCommision != null)
                                {
                                    bool IsCommision = Convert.ToBoolean(tblUserPrivilegeDetail.IsCommision);
                                    if (IsCommision.Equals(true))
                                    {
                                        IsCommsn = true;

                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                            TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                        else
                                        {
                                            TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                    }
                                    else
                                    {
                                        IsCommsn = false;

                                        if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                        {
                                            CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                        }
                                        else
                                        {
                                            CommissionPercentage = 0;
                                        }

                                        if (data.PlanTotalAmount > 0)
                                        {
                                            TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                        }
                                        else
                                        {
                                            TotalPlanAmount = 0;
                                        }


                                        if (data.Amount > 0)
                                        {
                                            PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                        }
                                        else
                                        {
                                            PlanBaseAmount = 0;
                                        }

                                        TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                        decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                        if (data.IsGSTNoFound == true)
                                        {
                                            decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                            TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                        else
                                        {
                                            TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                            FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                        }
                                    }
                                }
                                else
                                {
                                    IsCommsn = null;

                                    if (tblUserPrivilegeDetail.CommisionPercentage != null)
                                    {
                                        CommissionPercentage = Convert.ToDecimal(tblUserPrivilegeDetail.CommisionPercentage);
                                    }
                                    else
                                    {
                                        CommissionPercentage = 0;
                                    }

                                    if (data.PlanTotalAmount > 0)
                                    {
                                        TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                    }
                                    else
                                    {
                                        TotalPlanAmount = 0;
                                    }


                                    if (data.Amount > 0)
                                    {
                                        PlanBaseAmount = Convert.ToDecimal(data.Amount);
                                    }
                                    else
                                    {
                                        PlanBaseAmount = 0;
                                    }

                                    TotalCommissionAmountForMIS = (PlanBaseAmount * CommissionPercentage) / 100;
                                    decimal TDSAmount = (TotalCommissionAmountForMIS * TDS) / 100;
                                    if (data.IsGSTNoFound == true)
                                    {
                                        decimal GstPercentage = (TotalCommissionAmountForMIS * GST) / 100;

                                        TotalCommissionAmountForMIS = (TotalCommissionAmountForMIS - TDSAmount) + GstPercentage;
                                        FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                    }
                                    else
                                    {
                                        TotalCommissionAmountForMIS = TotalCommissionAmountForMIS - TDSAmount;
                                        FinalAmount = TotalPlanAmount - TotalCommissionAmountForMIS;
                                    }
                                }

                                tblTransaction transObj = db.tblTransactions.Where(w => w.TransactionID == item).FirstOrDefault();
                                if (transObj != null)
                                {
                                    transObj.CommisionPercentage = CommissionPercentage;
                                    transObj.CommisionAmount = TotalCommissionAmountForMIS;
                                    transObj.IsCommision = IsCommsn;
                                    transObj.Mode = Mode;
                                    db.SaveChanges();
                                }
                                // TotalAmount Calculation For CommisionReportMIS END
                            }
                            else
                            {
                                // TotalAmount Calculation For Payment START
                                if (data.PlanTotalAmount > 0)
                                {
                                    TotalPlanAmount = Convert.ToDecimal(data.PlanTotalAmount);
                                }
                                else
                                {
                                    TotalPlanAmount = 0;
                                }

                                FinalAmount = TotalPlanAmount;

                                TotalFinalAmount += FinalAmount;
                                // TotalAmount Calculation For Payment END

                                // TotalAmount Calculation For CommisionReportMIS START
                                tblTransaction transObj = db.tblTransactions.Where(w => w.TransactionID == item).FirstOrDefault();
                                if (transObj != null)
                                {
                                    transObj.CommisionPercentage = 0;
                                    transObj.CommisionAmount = 0;
                                    transObj.IsCommision = false;
                                    transObj.Mode = null;
                                    db.SaveChanges();
                                }
                                // TotalAmount Calculation For CommisionReportMIS END
                            }
                        }
                    }

                    finalPremiumToCollect = Convert.ToDecimal(TotalFinalAmount).ToString("##.00");
                }

                _vPayOnlinePaymentModel.datetime = CurrentDate.ToString("yyyy-MM-dd HH:mm:ss"); //// 2014-12-05 12:35:07
                _vPayOnlinePaymentModel.partnerid = System.Configuration.ConfigurationManager.AppSettings["vPayPartnerID"];
                _vPayOnlinePaymentModel.billno = model.PaymentReferenceNo;
                _vPayOnlinePaymentModel.phone = model.ContactNo;
                _vPayOnlinePaymentModel.email = model.Email;
                _vPayOnlinePaymentModel.name = model.CustomerName;
                _vPayOnlinePaymentModel.amount = finalPremiumToCollect;
                _vPayOnlinePaymentModel.paymentmode = "1";
                _vPayOnlinePaymentModel.udf1 = model.IMDCode;
                _vPayOnlinePaymentModel.udf2 = model.ProductCode;
                _vPayOnlinePaymentModel.response_url = model.ResponseURL;

                model._vPayOnlinePaymentModel = _vPayOnlinePaymentModel;
                List<tblTransaction> transList = db.tblTransactions.Where(w => model.TransactionIDs.Contains(w.TransactionID)).ToList();

                tblOnlinePaymentDetail _tblOnlinePaymentDetail = new tblOnlinePaymentDetail();
                //_tblOnlinePaymentDetail.Amount = model.Amount;
                _tblOnlinePaymentDetail.Amount = TotalFinalAmount;
                _tblOnlinePaymentDetail.CreatedBy = model.CreatedBy;
                _tblOnlinePaymentDetail.CreatedDate = CurrentDate;
                _tblOnlinePaymentDetail.CustomerName = model.CustomerName;
                _tblOnlinePaymentDetail.IMDCode = model.IMDCode;
                _tblOnlinePaymentDetail.IsValid = true;
                _tblOnlinePaymentDetail.ProductCode = model.ProductCode;
                _tblOnlinePaymentDetail.PaymentReferenceNo = model.PaymentReferenceNo;


                foreach (var item in model.TransactionIDs)
                {
                    tblTransaction trans = db.tblTransactions.Where(w => w.TransactionID == item).FirstOrDefault();
                    tblMstPlan plan = null;
                    if (trans != null)
                    {
                        plan = db.tblMstPlans.Where(x => x.PlanID == trans.PlanID).FirstOrDefault();
                    }
                    _tblOnlinePaymentDetail.tblOnlinePaymentTransactionLogs.Add(new tblOnlinePaymentTransactionLog
                    {
                        CreatedBy = model.CreatedBy,
                        CreatedDate = CurrentDate,
                        IsValid = true,
                        TotalAmount = TotalFinalAmount,
                        TransactionID = item,
                        PlanAmount = plan.Total != null ? plan.Total : 0
                    });
                }

                db.tblOnlinePaymentDetails.Add(_tblOnlinePaymentDetail);
                db.SaveChanges();

                transList.ForEach(x =>
                {
                    x.OnlinePaymentID = _tblOnlinePaymentDetail.OnlinePaymentID;

                });

                db.SaveChanges();

                model.OnlinePaymentID = _tblOnlinePaymentDetail.OnlinePaymentID;
                _vPayOnlinePaymentResponseModel = RedirectToVPay(_vPayOnlinePaymentModel, _tblOnlinePaymentDetail);
                _vPayOnlinePaymentResponseModel.PaymentReferenceNo = model.PaymentReferenceNo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                _vPayOnlinePaymentResponseModel.Response = "Exception occure before calling web initiate transaction api.";
            }

            return _vPayOnlinePaymentResponseModel;
        }

        #region Vpay Redirect Model
        private vPayOnlinePaymentResponseModel RedirectToVPayold(vPayOnlinePaymentModel model, tblOnlinePaymentDetail _tblOnlinePaymentDetail)
        {

            vPayOnlinePaymentResponseModel _vPayOnlinePaymentResponseModel = new vPayOnlinePaymentResponseModel();

            try
            {
                string vPaySecretKey = System.Configuration.ConfigurationManager.AppSettings["vPaySecretKey"].ToString();
                string vPayToken = System.Configuration.ConfigurationManager.AppSettings["vPayToken"].ToString();
                string vPayURL = System.Configuration.ConfigurationManager.AppSettings["vPayURL"].ToString();
                string vPayMethod_Web_Initiate_Transaction = System.Configuration.ConfigurationManager.AppSettings["vPayMethod_Web_Initiate_Transaction"].ToString();
                string pipeRequest = "'" + model.amount + "''" + model.phone + "''" + model.partnerid + "''" + model.billno + "'";
                string checksum = GetHMACSHA256(pipeRequest, vPaySecretKey);
                model.checksum = checksum;

                string URL = vPayURL + vPayMethod_Web_Initiate_Transaction;

                HttpClient client = new HttpClient();
                var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                Uri bs = new Uri(URL);
                client.BaseAddress = bs;
                client.DefaultRequestHeaders.Add("X-App-Token", vPayToken);
                var response = client.PostAsync(URL, content).Result;

                logger1.Error("Policy Bazar :: RedirectToVPay() >Request > #URL :- " + vPayURL + "#Token:-" + vPayToken + "#Phone:-" + model.phone
                    + "#email:-" + model.email + "#Amount:-" + model.amount);
                logger1.Error("Policy Bazar :: RedirectToVPay() >response > #Status:-" + response.StatusCode + " #Respone.Is Success :- " + response.IsSuccessStatusCode
                    + "#Request Message:-" + response.RequestMessage + "Response :-" + response.Headers);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse = JsonConvert.DeserializeObject<vPayWebInitiateTransactionResponse>(result);
                        _tblOnlinePaymentDetail.WebInitiateTransactionResponseStatusCode = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.request.code;

                        switch (_vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.request.code)
                        {
                            case CrossCutting_Constant.vPayResponseCode_200:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_200;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_200_Description;

                                string responsepipeRequest = "'" + model.amount + "''" + model.phone + "''" + model.partnerid + "''" + model.billno + "''" + _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_id + "'";
                                logger1.Error("after service call ()> Response Request #" + responsepipeRequest + "#vPaySecretKey:-" + vPaySecretKey);
                                string responsechecksum = GetHMACSHA256(responsepipeRequest, vPaySecretKey);

                                if (_vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.checksum == responsechecksum)
                                {
                                    _vPayOnlinePaymentResponseModel.Flag = true;
                                    _vPayOnlinePaymentResponseModel.vPayTransactionURL = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_url;
                                    _tblOnlinePaymentDetail.transaction_id = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_id;
                                    _tblOnlinePaymentDetail.transaction_url = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_url;
                                }
                                else
                                {
                                    _vPayOnlinePaymentResponseModel.Response = "Web_Initiate_Transaction Api's response checksum not matched.";
                                }
                                break;
                            case CrossCutting_Constant.vPayResponseCode_400:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_400;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_400_Description;
                                _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_400_Description;
                                break;
                            case CrossCutting_Constant.vPayResponseCode_502:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_502;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_502_Description;
                                _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_502_Description;
                                break;
                            case CrossCutting_Constant.vPayResponseCode_503:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_503;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_503_Description;
                                _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_503_Description;
                                break;
                            case CrossCutting_Constant.vPayResponseCode_506:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_506;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_506_Description;
                                _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_506_Description;
                                break;
                        }

                        _tblOnlinePaymentDetail.Remark = _vPayOnlinePaymentResponseModel.Response;
                    }
                    else
                    {
                        _tblOnlinePaymentDetail.Remark = "Got webserivce IsSuccessStatusCode is 'false'.";
                    }
                }
                else
                {
                    _tblOnlinePaymentDetail.Remark = "Got webserivce status code '" + response.StatusCode + "'.";
                }
            }
            catch (Exception ex)
            {
                _vPayOnlinePaymentResponseModel.Response = "Exception during web initiate transaction api call.";
                HandleException(ex);
            }

            db.SaveChanges();
            return _vPayOnlinePaymentResponseModel;
        }

        #endregion


        private vPayOnlinePaymentResponseModel RedirectToVPay(vPayOnlinePaymentModel model, tblOnlinePaymentDetail _tblOnlinePaymentDetail)
        {

            vPayOnlinePaymentResponseModel _vPayOnlinePaymentResponseModel = new vPayOnlinePaymentResponseModel();


            try
            {

                string RazorPaySecretKey = Convert.ToString(ConfigurationManager.AppSettings["RazorPaySecretKey"]);
                string RazorPayToken = Convert.ToString(ConfigurationManager.AppSettings["RazorPayToken"]);
                string RazorPayURL = Convert.ToString(ConfigurationManager.AppSettings["RazorPayURL"]);
                string RazorPayMethod_Web_Initiate_Transaction = Convert.ToString(ConfigurationManager.AppSettings["RazorPayMethod_Web_Initiate_Transaction"]);
                string RazorPayEmail = Convert.ToString(ConfigurationManager.AppSettings["RazorPayEmail"]);
                string RazorPaySMS = Convert.ToString(ConfigurationManager.AppSettings["RazorPaySMS"]);
                string URL = RazorPayURL + RazorPayMethod_Web_Initiate_Transaction;
                string LinkExpiryinDays = Convert.ToString(ConfigurationManager.AppSettings["LinkExpiryinDays"]);

                int EmailSendByRazorpay = 0;
                int SMSSendByRazorpay = 0;
                int.TryParse(RazorPayEmail, out EmailSendByRazorpay);
                int.TryParse(RazorPaySMS, out SMSSendByRazorpay);
                int Defaultlinkexpiry = 1;
                int.TryParse(LinkExpiryinDays, out Defaultlinkexpiry);
                TimeSpan next3days = TimeSpan.FromTicks(DateTime.Now.AddDays(Defaultlinkexpiry).Ticks);
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.AddDays(Defaultlinkexpiry).Subtract(new DateTime(1970, 1, 1)).TotalSeconds);


                RazorPayCustomer RPCust = new RazorPayCustomer();
                RPCust.contact = model.phone;
                RPCust.name = model.name.Replace(".","").Replace("@","");
                RPCust.email = model.email;
                RazorPayRequestModel RazorPayReq = new RazorPayRequestModel();
                RazorPayReq.customer = RPCust;
                RazorPayReq.type = "link";
                RazorPayReq.view_less = 1;
                RazorPayReq.amount = Convert.ToInt32(model.amount.Replace(".", ""));
                RazorPayReq.currency = "INR";
                RazorPayReq.description = "Global Assure Payment";
                RazorPayReq.receipt = model.billno;
                RazorPayReq.sms_notify = SMSSendByRazorpay;
                RazorPayReq.email_notify = EmailSendByRazorpay;
                RazorPayReq.expire_by = (int)unixTimestamp;
                RazorPayReq.callback_url = model.response_url;
                RazorPayReq.callback_method = "get";


                HttpClient client = new HttpClient();
                var content = new StringContent(JsonConvert.SerializeObject(RazorPayReq), Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                Uri bs = new Uri(URL);
                client.BaseAddress = bs;

                //Set Basic Auth
                var user = RazorPayToken;
                var password = RazorPaySecretKey;
                var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var response = client.PostAsync(URL, content).Result;

                logger1.Error(" RedirectToVPay() >Request > #URL :- " + RazorPayURL + "#Token:-" + RazorPayToken + "#Phone:-" + model.phone
                 + "#email:-" + model.email + "#Amount:-" + model.amount);
                logger1.Error(" RedirectToVPay() >response > #Status:-" + response.StatusCode + " #Respone.Is Success :- " + response.IsSuccessStatusCode
                 + "#Request Message:-" + response.RequestMessage + "Response :-" + response.Headers);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        //_vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse = JsonConvert.DeserializeObject<vPayWebInitiateTransactionResponse>(result);

                        RazorWebInitiateTransactionResponse RazorPayResp = JsonConvert.DeserializeObject<RazorWebInitiateTransactionResponse>(result);


                        request vpReq = new request();
                        vpReq.status = "true";
                        vpReq.code = CrossCutting_Constant.vPayResponseCode_200;
                        vpReq.timestamp =HomeController.UnixTimeStampToDateTime(RazorPayResp.created_at).ToString("yyyy-dd-MM HH:mm:ss");
                        vpReq.message = "Transaction Created";
                        payment vpPmt = new payment();
                        vpPmt.billno = RazorPayResp.receipt;
                        string amt = Convert.ToString(RazorPayResp.amount);
                        vpPmt.amount = amt.Insert(amt.Length - 2, ".");
                        vpPmt.transaction_id = RazorPayResp.id;
                        vpPmt.transaction_url = RazorPayResp.short_url;
                        vPayWebInitiateTransactionResponse vPWTResp = new vPayWebInitiateTransactionResponse();
                        vPWTResp.request = vpReq;
                        vPWTResp.payment = vpPmt;
                        vPWTResp.version = "1";
                        vPWTResp.checksum = "";

                        _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse = vPWTResp;

                        _tblOnlinePaymentDetail.WebInitiateTransactionResponseStatusCode = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.request.code;

                        switch (_vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.request.code)
                        {
                            case CrossCutting_Constant.vPayResponseCode_200:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_200;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_200_Description;

                                string responsepipeRequest = "'" + model.amount + "''" + model.phone + "''" + model.partnerid + "''" + model.billno + "''" + _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_id + "'";
                                logger1.Error("after service call ()> Response Request #" + responsepipeRequest + "#RazorPaySecretKey:-" + RazorPaySecretKey);
                                //string responsechecksum = "";// GetHMACSHA256(responsepipeRequest, vPaySecretKey);

                                if (_vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_url != "")
                                {
                                    _vPayOnlinePaymentResponseModel.Flag = true;
                                    _vPayOnlinePaymentResponseModel.vPayTransactionURL = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_url;
                                    _tblOnlinePaymentDetail.transaction_id = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_id;
                                    _tblOnlinePaymentDetail.transaction_url = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_url;
                                }
                                else
                                {
                                    _vPayOnlinePaymentResponseModel.Response = "Web_Initiate_Transaction Api's response checksum not matched.";
                                }
                                break;
                            case CrossCutting_Constant.vPayResponseCode_400:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_400;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_400_Description;
                                _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_400_Description;
                                break;
                            case CrossCutting_Constant.vPayResponseCode_502:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_502;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_502_Description;
                                _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_502_Description;
                                break;
                            case CrossCutting_Constant.vPayResponseCode_503:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_503;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_503_Description;
                                _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_503_Description;
                                break;
                            case CrossCutting_Constant.vPayResponseCode_506:
                                _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_506;
                                _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_506_Description;
                                _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_506_Description;
                                break;
                        }

                        _tblOnlinePaymentDetail.Remark = _vPayOnlinePaymentResponseModel.Response;
                    }
                    else
                    {
                        _tblOnlinePaymentDetail.Remark = "Got webserivce IsSuccessStatusCode is 'false'.";
                    }
                }
                else
                {
                    _tblOnlinePaymentDetail.Remark = "Got webserivce status code '" + response.StatusCode + "'.";
                }
            }
            catch (Exception ex)
            {
                _vPayOnlinePaymentResponseModel.Response = "Exception during web initiate transaction api call.";
                HandleException(ex);
            }

            #region Vpay Code start
            //try
            //{


            //    string vPaySecretKey = System.Configuration.ConfigurationManager.AppSettings["vPaySecretKey"].ToString();
            //    string vPayToken = System.Configuration.ConfigurationManager.AppSettings["vPayToken"].ToString();
            //    string vPayURL = System.Configuration.ConfigurationManager.AppSettings["vPayURL"].ToString();
            //    string vPayMethod_Web_Initiate_Transaction = System.Configuration.ConfigurationManager.AppSettings["vPayMethod_Web_Initiate_Transaction"].ToString();
            //    string pipeRequest = "'" + model.amount + "''" + model.phone + "''" + model.partnerid + "''" + model.billno + "'";
            //    string checksum = GetHMACSHA256(pipeRequest, vPaySecretKey);
            //    model.checksum = checksum;

            //    string URL = vPayURL + vPayMethod_Web_Initiate_Transaction;

            //    HttpClient client = new HttpClient();
            //    var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            //    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //    Uri bs = new Uri(URL);
            //    client.BaseAddress = bs;
            //    client.DefaultRequestHeaders.Add("X-App-Token", vPayToken);
            //    var response = client.PostAsync(URL, content).Result;

            //    logger2.Error(" RedirectToVPay() >Request > #URL :- " + vPayURL + "#Token:-" + vPayToken + "#Phone:-" + model.phone
            //     + "#email:-" + model.email + "#Amount:-" + model.amount);
            //    logger2.Error(" RedirectToVPay() >response > #Status:-" + response.StatusCode + " #Respone.Is Success :- " + response.IsSuccessStatusCode
            //     + "#Request Message:-" + response.RequestMessage + "Response :-" + response.Headers);
            //    if (response.StatusCode == System.Net.HttpStatusCode.OK)
            //    {
            //        if (response.IsSuccessStatusCode)
            //        {
            //            var result = response.Content.ReadAsStringAsync().Result;
            //            _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse = JsonConvert.DeserializeObject<vPayWebInitiateTransactionResponse>(result);
            //            _tblOnlinePaymentDetail.WebInitiateTransactionResponseStatusCode = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.request.code;

            //            switch (_vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.request.code)
            //            {
            //                case CrossCutting_Constant.vPayResponseCode_200:
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_200;
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_200_Description;

            //                    string responsepipeRequest = "'" + model.amount + "''" + model.phone + "''" + model.partnerid + "''" + model.billno + "''" + _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_id + "'";
            //                    logger2.Error("after service call ()> Response Request #" + responsepipeRequest + "#vPaySecretKey:-" + vPaySecretKey);
            //                    string responsechecksum = GetHMACSHA256(responsepipeRequest, vPaySecretKey);

            //                    if (_vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.checksum == responsechecksum)
            //                    {
            //                        _vPayOnlinePaymentResponseModel.Flag = true;
            //                        _vPayOnlinePaymentResponseModel.vPayTransactionURL = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_url;
            //                        _tblOnlinePaymentDetail.transaction_id = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_id;
            //                        _tblOnlinePaymentDetail.transaction_url = _vPayOnlinePaymentResponseModel._vPayWebInitiateTransactionResponse.payment.transaction_url;
            //                    }
            //                    else
            //                    {
            //                        _vPayOnlinePaymentResponseModel.Response = "Web_Initiate_Transaction Api's response checksum not matched.";
            //                    }
            //                    break;
            //                case CrossCutting_Constant.vPayResponseCode_400:
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_400;
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_400_Description;
            //                    _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_400_Description;
            //                    break;
            //                case CrossCutting_Constant.vPayResponseCode_502:
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_502;
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_502_Description;
            //                    _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_502_Description;
            //                    break;
            //                case CrossCutting_Constant.vPayResponseCode_503:
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_503;
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_503_Description;
            //                    _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_503_Description;
            //                    break;
            //                case CrossCutting_Constant.vPayResponseCode_506:
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCode = CrossCutting_Constant.vPayResponseCode_506;
            //                    _vPayOnlinePaymentResponseModel.vPayResponseCodeDescription = CrossCutting_Constant.vPayResponseCode_506_Description;
            //                    _vPayOnlinePaymentResponseModel.Response = CrossCutting_Constant.vPayResponseCode_506_Description;
            //                    break;
            //            }

            //            _tblOnlinePaymentDetail.Remark = _vPayOnlinePaymentResponseModel.Response;
            //        }
            //        else
            //        {
            //            _tblOnlinePaymentDetail.Remark = "Got webserivce IsSuccessStatusCode is 'false'.";
            //        }
            //    }
            //    else
            //    {
            //        _tblOnlinePaymentDetail.Remark = "Got webserivce status code '" + response.StatusCode + "'.";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _vPayOnlinePaymentResponseModel.Response = "Exception during web initiate transaction api call.";
            //    HandleException(ex);
            //}
            #endregion Vpay Code End

            db.SaveChanges();
            return _vPayOnlinePaymentResponseModel;
        }

        public void vPayCallback(vPayCallBackModel vPayCallBackModel)
        {
            string BccEmailID = Convert.ToString(Session["UserName"]);
            string Path = string.Empty;
            string landmarkServiceUser = ConfigurationManager.AppSettings["LandMarkUser"];

            tblOnlinePaymentDetail _tblOnlinePaymentDetail = db.tblOnlinePaymentDetails.Where(w => w.transaction_id == vPayCallBackModel.transaction_id).FirstOrDefault();
            //AspNetUser userdata = new AspNetUser();
            //if (_tblOnlinePaymentDetail != null)
            //{
            //    userdata = db.AspNetUsers.Where(x => x.Id == _tblOnlinePaymentDetail.CreatedBy).FirstOrDefault();
            //}

            //else
            //{
            //    var user = db.tblTransactions.Where(x => x.OnlinePaymentID == _tblOnlinePaymentDetail.OnlinePaymentID).Select(x => x.UserID).FirstOrDefault();
            //    userdata = db.AspNetUsers.Where(x => x.Id == user).FirstOrDefault();
            //}

            logger1.Error("Policy Bazar :: Hit Call back from payment API > Response -- #Status :-" + vPayCallBackModel.Status + "-- #Amount:- " + vPayCallBackModel.Amount
                    + "Payment Mode" + vPayCallBackModel.paymentmode);
            if (vPayCallBackModel.Status == CrossCutting_Constant.vPayCallback_Status_Success)
            {
                logger1.Error("Policy Bazar :: ---Call back Status Success---");
                //decimal ReceivedAmount = 0;
                List<tblTransaction> translist = db.tblTransactions.Where(w => w.OnlinePaymentID == _tblOnlinePaymentDetail.OnlinePaymentID).ToList();
                translist.ForEach(x =>
                {
                    var data = (from t in db.tblTransactions.AsNoTracking()
                                join plans in db.tblMstPlans.AsNoTracking() on t.PlanID equals plans.PlanID into plansbased
                                from plansbasedobj in plansbased.DefaultIfEmpty()
                                join updpname in db.tblUserPrivilegeDetails.AsNoTracking() on plansbasedobj.Name equals updpname.Plans into updpnamebased
                                from updpnamebasedobj in updpnamebased.DefaultIfEmpty()
                                where t.TransactionID == x.TransactionID && updpnamebasedobj.UserID == x.UserID && (updpnamebasedobj.IsValid != false || updpnamebasedobj.IsOnHold == 1)
                                select new
                                {
                                    PlanTotalAmount = plansbasedobj.Total,
                                    CommissionPercentage = updpnamebasedobj.CommisionPercentage

                                }).FirstOrDefault();

                    if (data != null)
                    {
                        decimal CommissionPercentage = Convert.ToDecimal(data.CommissionPercentage);
                        decimal PlanTotalAmount = Convert.ToDecimal(data.PlanTotalAmount);

                        AspNetUser userdata = db.AspNetUsers.Where(w => w.Id == x.UserID).FirstOrDefault();
                        if (userdata != null)
                        {
                            if (CommissionPercentage > 0)
                            {
                                userdata.PaymentReceivedAmount = Convert.ToDecimal(userdata.PaymentReceivedAmount) + Convert.ToDecimal(PlanTotalAmount);
                            }
                            else
                            {
                                userdata.PaymentReceivedAmount = Convert.ToDecimal(userdata.PaymentReceivedAmount) + Convert.ToDecimal(PlanTotalAmount);
                            }
                        }
                        db.SaveChanges();

                        //if (CommissionPercentage > 0)
                        //{
                        //    ReceivedAmount = ReceivedAmount + PlanTotalAmount;
                        //}
                        //else
                        //{
                        //    ReceivedAmount = ReceivedAmount + PlanTotalAmount;
                        //}
                    }

                    x.StatusID = CrossCutting_Constant.StatusID_Payment_Entry;
                    x.PaymentReceivedDate = DateTime.Now;
                    x.IsPaymentMade = true;
                    x.PaymentReferenceNo = _tblOnlinePaymentDetail.PaymentReferenceNo;
                    x.PaymentReferenceCreatedDate = _tblOnlinePaymentDetail.CreatedDate;

                    Path = string.Empty;
                    Path = objHome.GetCertificatePath(x.TransactionID);
                    if (landmarkServiceUser == x.UserID)
                    {
                        _c.SendingCertificateMail(x.CustomerEmail, Path, x.CertificateNo, null, true);
                    }
                    else
                    {
                        _c.SendingCertificateMail(x.CustomerEmail, Path, x.CertificateNo, null, false, BccEmailID);
                    }

                    tblTransactionAuditLog audit = new tblTransactionAuditLog();
                    audit.UserID = _tblOnlinePaymentDetail.CreatedBy;
                    audit.IsValid = true;
                    audit.CreatedDateTime = _tblOnlinePaymentDetail.CreatedDate;
                    audit.StatusID = CrossCutting_Constant.StatusID_Payment_Entry;
                    audit.CertificateNo = x.CertificateNo;
                    x.tblTransactionAuditLogs.Add(audit);
                });

                //if (userdata != null)
                //{
                //    if (ReceivedAmount > 0)
                //    {
                //        userdata.PaymentReceivedAmount = Convert.ToDecimal(userdata.PaymentReceivedAmount) + Convert.ToDecimal(ReceivedAmount);
                //    }
                //    else
                //    {
                //        userdata.PaymentReceivedAmount = Convert.ToDecimal(userdata.PaymentReceivedAmount) + Convert.ToDecimal(_tblOnlinePaymentDetail.Amount);
                //    }
                //}

                _tblOnlinePaymentDetail.Remark = "";
                logger1.Error(" Policy Bazar :: --Transaction Updated--");
            }

            else
            {
                _tblOnlinePaymentDetail.Remark = "Payment is in '" + vPayCallBackModel.Status + "' Status.";
                logger1.Error(" Policy Bazar :: --Callback response error--  #vPayCallBackModel.Status :- " + vPayCallBackModel.Status);
            }

            _tblOnlinePaymentDetail.Status = vPayCallBackModel.Status;
            _tblOnlinePaymentDetail.PayerType = vPayCallBackModel.paymentmode;

            db.SaveChanges();
        }

        public string GetHMACSHA256(string text, string key)
        {
            UTF8Encoding encoder = new UTF8Encoding();

            byte[] hashValue;
            byte[] keybyt = encoder.GetBytes(key);
            byte[] message = encoder.GetBytes(text);

            HMACSHA256 hashString = new HMACSHA256(keybyt);
            string hex = "";

            hashValue = hashString.ComputeHash(message);

            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }

        #region OnlinePaymentResponse
        public ActionResult OnlinePaymentResponse(vPayCallBackModel model)
        {
            //
            tblOnlinePaymentDetail _tblOnlinePaymentDetailnew = db.tblOnlinePaymentDetails.Where(w => w.transaction_id == model.transaction_id).FirstOrDefault();
            _tblOnlinePaymentDetailnew.Checksum = model.Checksum;
            db.SaveChanges();
            //

            OnlinePaymentResponseModel responseModel = new OnlinePaymentResponseModel();

            string CertificatePath = string.Empty;
            string CertificateNo = string.Empty;
            try
            {
                CertificateNo = Convert.ToString(TempData["CertificateNo"]);
                CertificatePath = Convert.ToString(TempData["CertificatePath"]);

                logger1.Error("Policy Bazar :: OnlinePaymentResponse()");
                if (model != null)
                {
                    this.vPayCallback(model);
                    if (model.Status == CrossCutting_Constant.vPayCallback_Status_Success)
                    {
                        responseModel.Status = CrossCutting_Constant.Success;
                        logger1.Error("Policy Bazar :: --Success--");
                        if (!string.IsNullOrEmpty(model.transaction_id))
                        {
                            tblOnlinePaymentDetail _tblOnlinePaymentDetail = db.tblOnlinePaymentDetails.Where(w => w.transaction_id == model.transaction_id).FirstOrDefault();
                            responseModel.PaymentReferenceNo = _tblOnlinePaymentDetail.PaymentReferenceNo;
                        }
                    }
                    else
                    {
                        responseModel.Status = CrossCutting_Constant.Failure;
                        responseModel.Remark = "Payment is in '" + model.Status + "' Status.";
                        logger1.Error("Policy Bazar :: Payment is in " + model.Status);
                    }
                }
                else
                {
                    responseModel.Status = CrossCutting_Constant.Failure;
                    responseModel.Remark = "Policy Bazar :: Response is not found.";
                    logger1.Error(" Policy Bazar :: Response is not found.");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                responseModel.Status = CrossCutting_Constant.Failure;
                responseModel.Remark = "Something went wrong. please try again.";
                logger1.Error("Policy Bazar :: Something went wrong. please try again.");
            }

            //return View(responseModel);

            string PayTransactionId = model.transaction_id;
            var GetUserByPayTransactionId = db.tblOnlinePaymentDetails.Where(w => w.transaction_id == PayTransactionId).FirstOrDefault();
            if (GetUserByPayTransactionId != null)
            {
                string PaymentReferenceNo = GetUserByPayTransactionId.PaymentReferenceNo;
                decimal tblOnlinePayID = GetUserByPayTransactionId.OnlinePaymentID;
                var GettblTransDatabytblOnlinePayID = db.tblTransactions.Where(w => w.OnlinePaymentID == tblOnlinePayID).FirstOrDefault();
                if (GettblTransDatabytblOnlinePayID != null)
                {
                    string tblCertificateNo = GettblTransDatabytblOnlinePayID.CertificateNo;
                    string UserID = GettblTransDatabytblOnlinePayID.UserID;

                    var CertificateTrnsCheckByUser = db.tblTransactions.Where(w => w.CertificateNo == tblCertificateNo && w.UserID == UserID).FirstOrDefault();
                    if (CertificateTrnsCheckByUser != null)
                    {
                        decimal Transactionid = CertificateTrnsCheckByUser.TransactionID;
                        string ResponseStatus = model.Status;
                        if (ResponseStatus == CrossCutting_Constant.vPayCallback_Status_Success)
                        {
                            if (!string.IsNullOrEmpty(CertificateNo))
                            {
                                responseModel.CertificateNo = CertificateNo;
                            }

                            if (!string.IsNullOrEmpty(CertificatePath))
                            {
                                responseModel.CertificatePath = CertificatePath;
                            }

                            // Success URL
                            var VendorResponseUrl = db.tblVendorResponseUrl.Where(w => w.UserId == UserID && w.IsValid != false).FirstOrDefault();
                            if (VendorResponseUrl != null)
                            {
                                string ResUrl = VendorResponseUrl.ResponseUrl + "?Param=";
                                string Query = "PaymentRefNo=" + responseModel.PaymentReferenceNo + "&CertificateNo=" + tblCertificateNo + "&Remark=" + model.Status + "";
                                string EnQuery = Encode(Query);
                                string FullResUrl = ResUrl + EnQuery;
                                return Redirect(FullResUrl);
                            }
                            else
                            {
                                return View(responseModel);
                            }
                        }
                        else
                        {
                            // Failed URL
                            var VendorResponseUrl = db.tblVendorResponseUrl.Where(w => w.UserId == UserID && w.IsValid != false).FirstOrDefault();
                            if (VendorResponseUrl != null)
                            {
                                string ResUrl = VendorResponseUrl.ResponseFailedUrl + "?Param=";
                                string Query = "PaymentRefNo=" + null + "&CertificateNo=" + null + "&Remark=" + model.Status + "";
                                string EnQuery = Encode(Query);
                                string FullResUrl = ResUrl + EnQuery;
                                return Redirect(FullResUrl);
                            }
                            else
                            {
                                return View(responseModel);
                            }
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View();
            }
        }
        #endregion
        #endregion

        #region Assistance
        public async System.Threading.Tasks.Task<ActionResult> Assistance(string data)
        {
            AssistModel model = new AssistModel();
            GlobalAssureEncryption enc = new GlobalAssureEncryption();
            AssistRequest request = new AssistRequest();

            // Request Parameter Samples
            // string encriptMessage = enc.Encrypt("CertificateNo=PC2903190000000006&User=policyBazaar&Pass=Inube@123");

            string DecriptMessage = enc.Decrypt(data);
            string[] SplitMessage = DecriptMessage.Split('&');
            string[] para = new string[10];

            for (int i = 0; i < SplitMessage.Length; i++)
            {
                string[] str = SplitMessage[i].Split('=');
                para[i] = str[1];
            }

            request.CertificateNo = para[0] == "" ? null : para[0];
            request.User = para[1] == "" ? null : para[1];
            request.Password = para[2] == "" ? null : para[2].TrimStart('\0');

            try
            {
                if (string.IsNullOrEmpty(request.User) || string.IsNullOrEmpty(request.Password))
                {

                    ViewBag.Error = "Invalid User";
                    return View("~/Views/WebPortal/Error.cshtml");
                }
                else
                {
                    bool validUser = await isValidUser(request.User, request.Password);
                    string HTML = String.Empty;
                    if (validUser)
                    {
                        //    string.IsNullOrEmpty(request.Mobile) || string.IsNullOrEmpty(request.Make) || string.IsNullOrEmpty(request.Model)
                        //|| string.IsNullOrEmpty(request.Reg) ||

                        string UserName = request.User;
                        string ValidRequest = string.Empty;
                        string CertificateNo = request.CertificateNo;
                        decimal Transactionid = db.tblTransactions.Where(x => x.CertificateNo == CertificateNo).Select(x => x.TransactionID).FirstOrDefault();


                        AssistModel assistDetails = new AssistModel();
                        assistDetails = objAssist.GetAssistDetail(Transactionid);
                        assistDetails.ActionID = CrossCutting_Constant.ActionID_Vendor_No_Action;
                        //HTML = objAssist.RenderPartialViewToString("~/Views/WebPortal/GetAssistDetails.cshtml", assistDetails);
                        return View(assistDetails);

                    }
                    else
                    {

                        ViewBag.Error = "Invalid User";
                        return View("~/Views/WebPortal/Error.cshtml");


                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }


        }

        public async System.Threading.Tasks.Task<ActionResult> RelianceAssistance(string data)

        {
            AssistModel model = new AssistModel();
            GlobalAssureEncryption enc = new GlobalAssureEncryption();
            AssistRequest request = new AssistRequest();

            //Request Parameter Samples

            //data = enc.Encrypt("User=assist@inube.com&Pass=Assist@123");


            string DecriptMessage = enc.Decrypt(data);
            string[] SplitMessage = DecriptMessage.Split('&');
            string[] para = new string[10];

            for (int i = 0; i < SplitMessage.Length; i++)
            {
                string[] str = SplitMessage[i].Split('=');
                para[i] = str[1];
            }


            request.User = para[0] == "" ? null : para[0];
            request.Password = para[1] == "" ? null : para[1].TrimStart('\0');

            try
            {
                if (string.IsNullOrEmpty(request.User) || string.IsNullOrEmpty(request.Password))
                {

                    ViewBag.Error = "Invalid User";
                    return View("~/Views/WebPortal/Error.cshtml");
                }
                else
                {
                    bool validUser = await isValidUser(request.User, request.Password);
                    string HTML = String.Empty;
                    if (validUser)
                    {
                        //    string.IsNullOrEmpty(request.Mobile) || string.IsNullOrEmpty(request.Make) || string.IsNullOrEmpty(request.Model)
                        //|| string.IsNullOrEmpty(request.Reg) ||

                        string UserName = request.User;
                        string ValidRequest = string.Empty;


                        return View("~/Views/Assistance/RelianceRSA.cshtml");

                    }
                    else
                    {

                        ViewBag.Error = "Invalid User";
                        return View("~/Views/WebPortal/Error.cshtml");


                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }


        }
        #endregion

        public ActionResult AssistanceResponse()
        {
            return View();
        }

        public JsonResult SaveAssistDetails(AssistModel objAssist)
        {

            tblAssistDetail tblAssist = new tblAssistDetail();
            bool isManual = objAssist.Manual_Allocation;
            decimal branchid = objAssist.Vendor_BranchID;
            string UserName = HttpContext.User.Identity.Name;
            var user = db.AspNetUsers.Where(x => x.UserName == UserName).FirstOrDefault();
            try
            {
                if (objAssist != null)
                {

                    if (objAssist.TransactionID > 0)
                    {
                        ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));
                        var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.AssistReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);
                        tblAssist.RefNo = Convert.ToString(OutputOfPaymentReference.Value);
                        tblAssist.TransactionId = objAssist.TransactionID;
                        tblAssist.Remarks = objAssist.Remarks;
                        tblAssist.PinCode = objAssist.PinCode;
                        tblAssist.Landmark = objAssist.Landmark;
                        tblAssist.Issue = objAssist.Issue;
                        tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                        tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                        tblAssist.IsValid = true;
                        tblAssist.Address = objAssist.Address;
                        tblAssist.StreetAddress = objAssist.StreetAddress;
                        tblAssist.CreatedDate = DateTime.Now;
                        tblAssist.City = objAssist.City;
                        tblAssist.Location = objAssist.location;
                        tblAssist.State = objAssist.State;
                        tblAssist.Address = objAssist.Address;
                        tblAssist.IncidentDetailid = objAssist.IncidentDetailId;
                        tblAssist.VehicleYear = objAssist.VehicleYear;
                        tblAssist.VehicleColor = objAssist.VehicleColor;
                        tblVendorBranchDetail selectedVendor = this.objAssist.getAllocatedVendor(isManual, objAssist.Latitude, objAssist.Longitude, branchid);
                        tblAssist.VendorId = selectedVendor.BranchID;
                        tblAssist.UserID = user == null ? null : user.Id;

                        db.tblAssistDetails.Add(tblAssist);
                        db.SaveChanges();

                        return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = tblAssist.RefNo, Vendor = selectedVendor.tblMstVendor.VendorName, Branch = selectedVendor.BranchName }, JsonRequestBehavior.AllowGet);
                    }


                }
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, AssistRefNo = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        #region GetIssueCertificate
        private TransactionModel GetIssueCertificate(string UserName, string make, string vehicleModel, ref string Valid)
        {

            TransactionModel model = new TransactionModel();
            var objMake = db.tblMstMakes.Where(x => x.Name == make.Trim()).Select(s => new { s.MakeID, s.Name, s.tblMstProduct.ProductName, s.ProductID }).FirstOrDefault();

            if (objMake != null)
            {
                var objModel = db.tblMstModels.Where(x => x.Name == vehicleModel && x.MakeID == objMake.MakeID).Select(
                    s => new { s.ModelID, s.Name }).FirstOrDefault();
                if (objModel != null)
                {
                    model.ProductID = objMake.ProductID;

                    model.PlanListItem = db.tblMstPlans.Where(a => a.ProductID == model.ProductID && a.IsValid != false).Select(x => new { x.PlanID, x.Name }).
                        OrderBy(o => o.Name).ToDictionary(d => d.PlanID, d => d.Name);

                    model.MakeListItem = db.tblMstMakes.AsNoTracking().Where(w => w.IsValid != false && w.ProductID == model.ProductID)
                                                        .Select(s => new { s.MakeID, s.Name }).OrderBy(o => o.Name).ToDictionary(d => d.MakeID, d => d.Name);
                    model.MakeID = objMake.MakeID;
                    model.ModelListItem = db.tblMstModels.AsNoTracking().Where(w => w.IsValid != false && w.MakeID == model.MakeID)
                                                                  .Select(s => new { s.ModelID, s.Name, s.Variant }).OrderBy(o => o.Name)
                                                                  .ToDictionary(d => d.ModelID, d => d.Name + " - " + d.Variant);


                    model.Make = objMake.Name;
                    model.Model = objModel.Name;
                    model.ModelID = objModel.ModelID;

                }
            }
            else
            {

            }


            model.PaymentID = CrossCutting_Constant.Payment_External_ID;
            model.IssueDate = DateTime.Now;
            model.CoverStartDate = DateTime.Now;
            model.IsSingleChequeSinglePolicy = true;
            model.PermanentStateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                              .ToDictionary(d => d.StateID, d => d.State);
            model.BankListItem = db.tblMstBanks.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.BankID, s.BankCode, s.BankName })
                                  .ToDictionary(d => d.BankID, d => d.BankName);
            model.TransactionTypeListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.TransactionType)
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            var commontypedata = db.tblMstCommonTypes.Where(w => w.MasterType == CrossCutting_Constant.ChequeType || w.MasterType == CrossCutting_Constant.ChequeForPolicy)
                                   .Select(s => new { s.CommonTypeID, s.Description, s.MasterType }).ToList();

            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.DealerName, s.TotalUsedPlanAmount, s.PaymentReceivedAmount, s.TieUpCompanyID, s.TotalCDBalanceAmount }).FirstOrDefault();

            model.DealerName = userdata.DealerName;
            model.UserID = userdata.Id;

            var UserPrivilegeDetails = (from u in db.AspNetUsers
                                        join up in db.tblUserPrivilegeDetails on u.Id equals up.UserID
                                        where u.UserName == UserName
                                        select new { up.Payment, up.Product, up.Make, up.Model, up.Plans }).ToList();

            List<string> ProductforUser = new List<string>();
            List<string> PaymentforUser = new List<string>();

            if (UserPrivilegeDetails.Any())
            {
                ProductforUser = UserPrivilegeDetails.Where(w => w.Product != null && w.Product != "").Select(s => s.Product.ToLower()).Distinct().ToList();
                PaymentforUser = UserPrivilegeDetails.Where(w => w.Payment != null && w.Payment != "").Select(s => s.Payment.ToLower()).Distinct().ToList();

                if (ProductforUser.Any())
                {
                    int productForUserhaveALL = ProductforUser.Where(w => w.ToLower() == "all").Count();
                    if (productForUserhaveALL > 0)
                    {
                        model.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false)
                                      .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);
                    }
                    else
                    {
                        model.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false && ProductforUser.Contains(w.ProductName.ToLower()))
                                      .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);
                    }
                }
            }
            return model;
        }
        #endregion

        //#region GetCertificatePath
        //public string GetCertificatePath(decimal TransactionID)
        //{
        //    DateTime CurrentDate = DateTime.Now;
        //    string Path = String.Empty;
        //    try
        //    {
        //        #region  GenerateCertificate
        //        FonetDriver driver = FonetDriver.Make();
        //        GenerateCertificateModel _data = new GenerateCertificateModel();

        //        var certificatedata = (from transaction in db.tblTransactions.AsNoTracking()
        //                               join product in db.tblMstProducts.AsNoTracking() on transaction.ProductID equals product.ProductID
        //                               join make in db.tblMstMakes.AsNoTracking() on transaction.MakeID equals make.MakeID
        //                               join mo in db.tblMstModels.AsNoTracking() on transaction.ModelID equals mo.ModelID
        //                               join plan in db.tblMstPlans.AsNoTracking() on transaction.PlanID equals plan.PlanID
        //                               join cityleft in db.tblMstCities.AsNoTracking() on transaction.PermanentCityID equals cityleft.CityID into cityleftinto
        //                               from city in cityleftinto.DefaultIfEmpty()
        //                               join user in db.AspNetUsers.AsNoTracking() on transaction.UserID equals user.Id
        //                               where transaction.TransactionID == TransactionID
        //                               select new
        //                               {
        //                                   CertificateNo = transaction.CertificateNo,
        //                                   CoverStartDate = transaction.CoverStartDate,
        //                                   CoverEndDate = transaction.CoverEndDate,
        //                                   RegistrationNo = transaction.RegistrationNo,
        //                                   ChassisNo = transaction.ChassisNo,
        //                                   EngineNo = transaction.EngineNo,
        //                                   CustomerName = transaction.CustomerName,
        //                                   CustomerLastName = transaction.CustomerLastName,
        //                                   CustomerContact = transaction.CustomerContact,
        //                                   CustomerEmail = transaction.CustomerEmail,
        //                                   PermanentAddress = transaction.PermanentAddress,
        //                                   PermanentAddress2 = transaction.PermanentAddress2,
        //                                   PermanentAddress3 = transaction.PermanentAddress3,
        //                                   City = (city != null) ? city.City : transaction.CityName,
        //                                   State = (city != null) ? city.tblMstState.State : transaction.StateName,
        //                                   Product = product.ProductName,
        //                                   Make = make.Name,
        //                                   Model = mo.Name,
        //                                   Variant = mo.Variant,
        //                                   Plan = plan.Name,
        //                                   Amount = plan.Amount,
        //                                   GSTAmount = plan.GST,
        //                                   TotalAmount = plan.Total,
        //                                   Dealer = user.DealerName
        //                               }).FirstOrDefault();

        //        string CertificateNo = certificatedata.CertificateNo;

        //        _data.DealerName = certificatedata.Dealer;
        //        _data.CurrentDateTime = Convert.ToDateTime(CurrentDate).ToString("yyyy-MM-dd HH:mm:ss");
        //        _data.CertificateNo = certificatedata.CertificateNo;
        //        _data.CoverStartDate = ((certificatedata.CoverStartDate != null) ? Convert.ToDateTime(certificatedata.CoverStartDate).ToString("dd-MM-yyyy") : "");
        //        _data.CoverEndDate = ((certificatedata.CoverEndDate != null) ? Convert.ToDateTime(certificatedata.CoverEndDate).ToString("dd-MM-yyyy") : "");
        //        _data.RegistrationNo = certificatedata.RegistrationNo;
        //        _data.ChassisNo = certificatedata.ChassisNo;
        //        _data.EngineNo = certificatedata.EngineNo;
        //        _data.FirstName = certificatedata.CustomerName;
        //        _data.LastName = certificatedata.CustomerLastName;
        //        _data.ContactNo = certificatedata.CustomerContact;
        //        _data.Email = certificatedata.CustomerEmail;

        //        var Address1 = certificatedata.PermanentAddress;
        //        var Address2 = certificatedata.PermanentAddress2;
        //        var Address3 = certificatedata.PermanentAddress3;

        //        if (!string.IsNullOrEmpty(Address1))
        //        {
        //            if (Address1.Length > 90)
        //            {
        //                _data.Address1 = Address1.Substring(0, 45) + "   " + Address1.Substring(45, 45) + "   " + Address1.Substring(90, Address1.Length - 90);
        //            }
        //            else if (Address1.Length > 45)
        //            {
        //                _data.Address1 = Address1.Substring(0, 45) + "   " + Address1.Substring(45, Address1.Length - 45);
        //            }
        //            else
        //            {
        //                _data.Address1 = Address1;
        //            }
        //        }


        //        if (!string.IsNullOrEmpty(Address2))
        //        {
        //            if (Address2.Length > 90)
        //            {
        //                _data.Address2 = Address2.Substring(0, 45) + "   " + Address2.Substring(45, 45) + "   " + Address2.Substring(90, Address2.Length - 90);
        //            }
        //            else if (Address2.Length > 45)
        //            {
        //                _data.Address2 = Address2.Substring(0, 45) + "   " + Address2.Substring(45, Address2.Length - 45);
        //            }
        //            else
        //            {
        //                _data.Address2 = Address2;
        //            }
        //        }


        //        if (!string.IsNullOrEmpty(Address3))
        //        {
        //            if (Address3.Length > 90)
        //            {
        //                _data.Address3 = Address3.Substring(0, 45) + "   " + Address3.Substring(45, 45) + "   " + Address3.Substring(90, Address3.Length - 90);
        //            }
        //            else if (Address3.Length > 45)
        //            {
        //                _data.Address3 = Address3.Substring(0, 45) + "   " + Address3.Substring(45, Address3.Length - 45);
        //            }
        //            else
        //            {
        //                _data.Address3 = Address3;
        //            }
        //        }

        //        _data.City = certificatedata.City;
        //        _data.State = certificatedata.State;
        //        _data.Product = certificatedata.Product;
        //        _data.Make = certificatedata.Make;
        //        _data.Model = certificatedata.Model;
        //        _data.Variant = certificatedata.Variant;
        //        _data.Plan = certificatedata.Plan;
        //        _data.Amount = Convert.ToString(certificatedata.Amount);
        //        _data.GSTAmount = Convert.ToString(certificatedata.GSTAmount);
        //        _data.IGST = Convert.ToString(certificatedata.GSTAmount);
        //        _data.TotalAmount = Convert.ToString(certificatedata.TotalAmount);
        //        _data.GSTNoOfTheServiceRecipient = "NA";
        //        //_data.DealershipStatmAndSign = Comma;
        //        _data.IsStateUP = "FALSE";
        //        _data.SACCode = ConfigurationManager.AppSettings["SACCode"];
        //        _data.PlaceOfSupply = certificatedata.State;

        //        decimal? HalfGSTAmount = Math.Round((Convert.ToDecimal(certificatedata.GSTAmount) / 2), 2);
        //        _data.CGST = Convert.ToString(HalfGSTAmount);
        //        _data.SGST = Convert.ToString(HalfGSTAmount);

        //        if (!string.IsNullOrEmpty(certificatedata.State))
        //        {
        //            if (certificatedata.State.ToLower() == CrossCutting_Constant.State_UP_Name_Lower)
        //            {
        //                _data.IsStateUP = "TRUE";
        //            }
        //        }

        //        // _data.AmountInWords = Comma;
        //        if (certificatedata.TotalAmount != null)
        //        {
        //            string TotalInWords = string.Empty;
        //            string isNegative = "";
        //            string number = Convert.ToString(certificatedata.TotalAmount);
        //            number = Convert.ToDouble(number).ToString();
        //            if (number.Contains("-"))
        //            {
        //                isNegative = "Minus ";
        //                number = number.Substring(1, number.Length - 1);
        //            }
        //            if (number == "0")
        //            {
        //                TotalInWords = "Zero Only";
        //            }
        //            else
        //            {
        //                TotalInWords = isNegative + ConvertToWords(number);
        //            }

        //            _data.AmountInWords = TotalInWords;
        //        }



        //        var planfeatureDetailListDate = (from pd in db.tblPlanFeatureDetails
        //                                         join pdf in db.tblPlanFeatureOfProducts on pd.PlanFeatureDetailID equals pdf.PlanFeatureDetailID
        //                                         where pdf.ProductName == certificatedata.Product && pdf.Plan == certificatedata.Plan
        //                                         select new
        //                                         {
        //                                             feature = pd.PlanFeature,
        //                                             detail = pd.Details,
        //                                             assistance = pd.Assistance,
        //                                             isfeature = pdf.IsFeature,
        //                                             isAssistance = pdf.IsAssistance,
        //                                             isAmount = pdf.IsAmount,
        //                                             Amount = pdf.Amount
        //                                         }).ToList();

        //        if (planfeatureDetailListDate.Any())
        //        {
        //            _data.IsPlanFeature = "TRUE";

        //            foreach (var item in planfeatureDetailListDate)
        //            {
        //                if (item.isfeature == true)
        //                {
        //                    _data.PlanFeatureDetailModelList.Add(new PlanFeatureDetailModel { Feature = item.feature, Detail = item.detail });
        //                }
        //                else if (item.isAssistance == true)
        //                {
        //                    _data.PlanFeatureDetailModelList.Add(new PlanFeatureDetailModel { Feature = item.feature, Detail = item.assistance });
        //                }
        //                else if (item.isAmount == true)
        //                {
        //                    _data.PlanFeatureDetailModelList.Add(new PlanFeatureDetailModel { Feature = item.feature, Detail = item.detail.Replace("$$Amount$$", item.Amount) });
        //                }
        //            }
        //        }
        //        else
        //        {
        //            _data.IsPlanFeature = "FALSE";
        //        }


        //        XmlSerializer _ser = new XmlSerializer(_data.GetType());
        //        MemoryStream data_stream = new MemoryStream();
        //        _ser.Serialize(data_stream, _data);

        //        var xslt = new XslCompiledTransform();
        //        xslt.Load(System.Web.HttpContext.Current.Server.MapPath(@"~/Content/XSL_Templates/GenerateCertificateNo.xsl"));
        //        data_stream.Seek(0, SeekOrigin.Begin);
        //        var fo_stream = new MemoryStream();
        //        using (var reader1 = XmlReader.Create(data_stream))
        //        {
        //            xslt.Transform(reader1, null, fo_stream);
        //        }
        //        data_stream.Close();
        //        fo_stream.Seek(0, SeekOrigin.Begin);
        //        string tempPath = ConfigurationManager.AppSettings["TempPath"];
        //        tempPath += "\\" + CertificateNo + ".pdf";
        //        driver.BaseDirectory = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(@"~/Content/XSL_Templates"));
        //        driver.Options = new Fonet.Render.Pdf.PdfRendererOptions();
        //        driver.Options.AddPrivateFont(new FileInfo(System.Web.HttpContext.Current.Server.MapPath(@"~/Content/XSL_Templates/Fonts/Rupee_Foradian.ttf")));
        //        driver.Options.AddPrivateFont(new FileInfo(System.Web.HttpContext.Current.Server.MapPath(@"~/Content/XSL_Templates/Fonts/ufonts.com_gillsans-light.ttf")));
        //        driver.Options.AddPrivateFont(new FileInfo(System.Web.HttpContext.Current.Server.MapPath(@"~/Content/XSL_Templates/Fonts/Franklin Gothic Book Regular.ttf")));
        //        driver.Options.AddPrivateFont(new FileInfo(System.Web.HttpContext.Current.Server.MapPath(@"~/Content/XSL_Templates/Fonts/Franklin Gothic Demi Regular.ttf")));
        //        driver.Options.FontType = Fonet.Render.Pdf.FontType.Embed;
        //        driver.Render(fo_stream, new FileStream(System.Web.HttpContext.Current.Server.MapPath(tempPath), FileMode.Create));
        //        fo_stream.Close();
        //        Path = tempPath;
        //        // Path = this.UploadFileOnAzureStorage(TransactionID, tempPath, CertificateNo);
        //        #endregion
        //        return Path;
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleException(ex);
        //        return Path;
        //    }
        //}
        //#endregion

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        #region isValidUser
        public async System.Threading.Tasks.Task<bool> isValidUser(string usernam, string password)
        {
            bool isValid = false;
            AspNetUser _AspNetUser = db.AspNetUsers.Where(a => a.UserName == usernam && (a.TieUpCompanyID == 5 || a.TieUpCompanyID == 7)).FirstOrDefault();

            if (_AspNetUser != null)
            {
                SignInStatus result = await SignInManager.PasswordSignInAsync(usernam, password, false, shouldLockout: false);
                if (result == 0)
                {
                    isValid = true;
                }
                else
                {
                    isValid = false;
                }
            }
            return isValid;
        }
        #endregion

        #region ConvertToWords
        private static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }

        private static String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }

        private static String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX  
                bool isDone = false;//test if already translated  
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))  
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric  
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping  
                    String place = "";//digit grouping name:hundres,thousand,etc...  
                    switch (numDigits)
                    {
                        case 1://ones' range  

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range  
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range  
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range  
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range  
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range  
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...  
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)  
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros  
                        //if (beginsZero) word = " and " + word.Trim();  
                    }
                    //ignore digit grouping names  
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }

        private static String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Only";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents  
                        endStr = "Paisa " + endStr;//Cents  
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }


        private static String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }
        #endregion

        #region HandleException
        public static bool HandleException(Exception ex)
        {
            bool rethrow = false;
            rethrow = UIExceptionHandler.HandleException(ref ex);
            if (rethrow)
            {
                throw ex;
            }
            return false;
        }
        #endregion

        #region RenderPartialViewToString
        public string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        #region Encode
        public static string Encode(string clearText)
        {
            string base64Decoded = clearText;
            string base64Encoded;
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(base64Decoded);
            base64Encoded = System.Convert.ToBase64String(data);

            return base64Encoded;
        }

        #endregion

        #region Decode
        public static string Decode(string cipherText)
        {
            byte[] b = Convert.FromBase64String(cipherText);
            string strOriginal = System.Text.Encoding.UTF8.GetString(b);

            return strOriginal;
        }

        #endregion

        #region IssueCovidCertificate
        public async System.Threading.Tasks.Task<ActionResult> IssueCovidCertificate(string pid)
        {
            //string EncodedSample = Encode(pid);

            // Link Closed Emails Start
            string LinkClosedEmails = ConfigurationManager.AppSettings["LinkClosedEmails"];
            string[] lstLinkClosedEmails = LinkClosedEmails.Split(',');
            // Link Closed Emails End

            // UserNames Start
            string CovidExternalUserID1 = ConfigurationManager.AppSettings["CovidExternalUserID1"];
            string CovidExternalUserID2 = ConfigurationManager.AppSettings["CovidExternalUserID2"];
            // UserNames End

            // PlanIDs Start
            // Silver PlanIDs
            int SilverPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Silver" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (SilverPlanTypeID > 0)
            {
                string strSilverPlanTypeID = Convert.ToString(SilverPlanTypeID);
                decimal?[] strSilverPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strSilverPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strSilverPlansID.Any())
                {
                    ViewData["SilverPlansID"] = strSilverPlansID;
                }
                else
                {
                    ViewData["SilverPlansID"] = null;
                }
            }

            // Gold PlanIDs
            int GoldPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Gold" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (GoldPlanTypeID > 0)
            {
                string strGoldPlanTypeID = Convert.ToString(GoldPlanTypeID);
                decimal?[] strGoldPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strGoldPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strGoldPlansID.Any())
                {
                    ViewData["GoldPlansID"] = strGoldPlansID;
                }
                else
                {
                    ViewData["GoldPlansID"] = null;
                }
            }

            // Platinum PlanIDs
            int PlatinumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Platinum" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (PlatinumPlanTypeID > 0)
            {
                string strPlatinumPlanTypeID = Convert.ToString(PlatinumPlanTypeID);
                decimal?[] strPlatinumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strPlatinumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strPlatinumPlansID.Any())
                {
                    ViewData["PlatinumPlansID"] = strPlatinumPlansID;
                }
                else
                {
                    ViewData["PlatinumPlansID"] = null;
                }
            }

            // Palladium PlanIDs
            int PalladiumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Palladium" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (PalladiumPlanTypeID > 0)
            {
                string strPalladiumPlanTypeID = Convert.ToString(PalladiumPlanTypeID);
                decimal?[] strPalladiumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strPalladiumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strPalladiumPlansID.Any())
                {
                    ViewData["PalladiumPlansID"] = strPalladiumPlansID;
                }
                else
                {
                    ViewData["PalladiumPlansID"] = null;
                }
            }

            // Titanium PlanIDs
            int TitaniumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Titanium" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (TitaniumPlanTypeID > 0)
            {
                string strTitaniumPlanTypeID = Convert.ToString(TitaniumPlanTypeID);
                decimal?[] strTitaniumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strTitaniumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strTitaniumPlansID.Any())
                {
                    ViewData["TitaniumPlansID"] = strTitaniumPlansID;
                }
                else
                {
                    ViewData["TitaniumPlansID"] = null;
                }
            }
            // PlanIDs End

            ViewBag.ErrMsg = "Here";

            TransactionModel model = new TransactionModel();
            try
            {
                // Request Parameter Samples
                // string encryptMessage = enc.Encrypt("pid=12345&email=gaurav.arora@globalassure.com");

                string decryptPid = Decode(pid);
                int temppid = 0;
                int.TryParse(decryptPid, out temppid);
             //   if (!string.IsNullOrEmpty(decryptPid) && temppid>0)
             //   {
             //       ViewBag.Error = "under maintenance. try again";
             //       return View("~/Views/WebPortal/Error.cshtml");

             //   }

             //else 
             if (!string.IsNullOrEmpty(decryptPid))
                {                   
                    if (decryptPid.ToLower().Contains("&email"))
                    {
                        string[] arrDecPid = decryptPid.Split('&');
                        if (arrDecPid.Any())
                        {
                            string param = arrDecPid[0];
                            model.PId = param;
                            TempData["PId"] = param;

                            string[] arrEmail = arrDecPid[1].Split('=');
                            if (arrEmail.Any())
                            {
                                string email = arrEmail[1];

                                if (!lstLinkClosedEmails.Contains(email.ToLower()))
                                {
                                    var emailCheckInTbl = db.AspNetUsers.Where(w => w.Email == email && w.IsActive != false).ToList();
                                    if (emailCheckInTbl.Any())
                                    {
                                        string UserName = emailCheckInTbl[0].Email;
                                        TempData["UserName"] = UserName;
                                        Session["UserName"] = UserName;
                                    }
                                    else
                                    {
                                        TempData["UserName"] = CovidExternalUserID2;
                                        Session["UserName"] = CovidExternalUserID2;
                                    }
                                }
                                else
                                {
                                    ViewBag.Error = "This Link Has Been Closed.";
                                    return View("~/Views/WebPortal/Error.cshtml");
                                }                             
                            }
                            else
                            {
                                ViewBag.Error = "//3:: Parameter Not Found or Decryption Error";
                                return View("~/Views/WebPortal/Error.cshtml");
                            }                            
                        }
                        else
                        {
                            ViewBag.Error = "//2:: Parameter Not Found or Decryption Error";
                            return View("~/Views/WebPortal/Error.cshtml");
                        }                                                                   
                    }
                    else
                    {
                        string param = decryptPid;
                        model.PId = param;
                        TempData["PId"] = param;

                        TempData["UserName"] = CovidExternalUserID1;
                        Session["UserName"] = CovidExternalUserID1;
                    }

                    model = GetIssueCovidCertificate(Convert.ToString(TempData["UserName"]));
                    model.IssueByID = CrossCutting_Constant.IssueBy_Application_ID;

                    model.UserName = Convert.ToString(TempData["UserName"]);
                }
                else
                {
                    ViewBag.Error = "//1:: Parameter Not Found or Decryption Error";
                    return View("~/Views/WebPortal/Error.cshtml");
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }

            return View(model);
        }

        public TransactionModel GetIssueCovidCertificate(string UserName)
        {

            TransactionModel model = new TransactionModel();

            model.PaymentID = CrossCutting_Constant.Payment_Cheque_ID;
            model.IssueDate = DateTime.Now;
            model.CoverStartDate = DateTime.Now;
            //  model.CoverEndDate = DateTime.Now.AddDays(364);

            model.IsChequePaymentTypeIntermediary = true;
            model.IsSingleChequeSinglePolicy = false;
            model.IsChequeTypeLocal = true;

            model.PermanentStateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                              .ToDictionary(d => d.StateID, d => d.State);

            model.BankListItem = db.tblMstBanks.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.BankID, s.BankCode, s.BankName })
                                  .ToDictionary(d => d.BankID, d => d.BankName);

            model.TransactionTypeListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.TransactionType)
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            model.InsuredGenderListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.InsuredGender)
                                           .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            model.NomineeGenderListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.NomineeGender)
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            model.NomineeRelationshipListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.NomineeRelationship)
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            //Covid
            model.SumInsuredListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "Covid")
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);
            //

            var commontypedata = db.tblMstCommonTypes.Where(w => w.MasterType == CrossCutting_Constant.ChequeType || w.MasterType == CrossCutting_Constant.ChequeForPolicy)
                                   .Select(s => new { s.CommonTypeID, s.Description, s.MasterType }).ToList();


            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.DealerName, s.TotalUsedPlanAmount, s.PaymentReceivedAmount, s.TieUpCompanyID, s.TotalCDBalanceAmount }).FirstOrDefault();

            model.DealerName = userdata.DealerName;
            model.TotalUsedPlanAmount = Convert.ToDecimal(userdata.TotalUsedPlanAmount);
            model.TotalPaymentReceivedAmount = Convert.ToDecimal(userdata.PaymentReceivedAmount);
            var TotalFloatPlanAmount = db.tblUserFloatAmounts.Where(w => w.UserID == userdata.Id && w.Amount != null && w.IsValid != false).Sum(s => s.Amount);
            model.TotalFloatPlanAmount = Convert.ToDecimal(TotalFloatPlanAmount);

            var TotalAmounts = (model.TotalFloatPlanAmount + model.TotalPaymentReceivedAmount);

            var TotalBalanceAmount = (TotalAmounts - model.TotalUsedPlanAmount);

            model.TotalBalanceAmount = (TotalBalanceAmount < 0) ? 0 : TotalBalanceAmount;

            model.Total_CD_Balance = userdata.TotalCDBalanceAmount;

            var UserPrivilegeDetails = (from u in db.AspNetUsers
                                        join up in db.tblUserPrivilegeDetails on u.Id equals up.UserID
                                        where u.UserName == UserName
                                        select new { up.Payment, up.Product, up.Make, up.Model, up.Plans }).ToList();

            List<string> ProductforUser = new List<string>();
            List<string> PaymentforUser = new List<string>();

            if (UserPrivilegeDetails.Any())
            {
                ProductforUser = UserPrivilegeDetails.Where(w => w.Product != null && w.Product != "").Select(s => s.Product.ToLower()).Distinct().ToList();
                PaymentforUser = UserPrivilegeDetails.Where(w => w.Payment != null && w.Payment != "").Select(s => s.Payment.ToLower()).Distinct().ToList();

                if (ProductforUser.Any())
                {
                    int productForUserhaveALL = ProductforUser.Where(w => w.ToLower() == "all").Count();
                    if (productForUserhaveALL > 0)
                    {
                        model.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false)
                                      .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);
                    }
                    else
                    {
                        model.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false && ProductforUser.Contains(w.ProductName.ToLower()))
                                      .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);
                    }
                }               
            }

            model.PaymentListItem = db.tblMstPayments.AsNoTracking().Where(w => w.IsValid != false)
                                         .Select(s => new { s.PaymentID, s.Payment }).ToDictionary(d => d.PaymentID, d => d.Payment);
            model.PaymentListItem.OrderBy(x => x.Key);

            return model;
        }
        #endregion

        #region GetPlanByProductIDAndSumInsIDForUser
        public ActionResult GetPlanByProductIDAndSumInsIDForUser(int ProductID, int SumInsID, int StateID, int CityID)
        {
            try
            {
                //string UserName = ConfigurationManager.AppSettings["CovidExternalUserID1"];
                string UserName = Convert.ToString(Session["UserName"]);               

                // SumInsuredIDs
                int SumInsID25000 = db.tblMstCommonTypes.Where(w => w.Description == "25000" && w.MasterType == "Covid" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
                int SumInsID50000 = db.tblMstCommonTypes.Where(w => w.Description == "50000" && w.MasterType == "Covid" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
                int SumInsID100000 = db.tblMstCommonTypes.Where(w => w.Description == "100000" && w.MasterType == "Covid" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
                int SumInsID200000 = db.tblMstCommonTypes.Where(w => w.Description == "200000" && w.MasterType == "Covid" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();

                // PlanIDs
                // 25000
                decimal?[] SumIns25000PlansID = new decimal?[100];
                decimal?[] decSumIns25000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID25000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns25000PlansID.Any())
                {
                    SumIns25000PlansID = decSumIns25000PlansID;
                }
                else
                {
                    SumIns25000PlansID = null;
                }

                // 50000
                decimal?[] SumIns50000PlansID = new decimal?[100];
                decimal?[] decSumIns50000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID50000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns50000PlansID.Any())
                {
                    SumIns50000PlansID = decSumIns50000PlansID;
                }
                else
                {
                    SumIns50000PlansID = null;
                }

                // 100000
                decimal?[] SumIns100000PlansID = new decimal?[100];
                decimal?[] decSumIns100000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID100000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns100000PlansID.Any())
                {
                    SumIns100000PlansID = decSumIns100000PlansID;
                }
                else
                {
                    SumIns100000PlansID = null;
                }

                // 200000
                decimal?[] SumIns200000PlansID = new decimal?[100];
                decimal?[] decSumIns200000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID200000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns200000PlansID.Any())
                {
                    SumIns200000PlansID = decSumIns200000PlansID;
                }
                else
                {
                    SumIns200000PlansID = null;
                }

                List<SelectListItem> planListItem = new List<SelectListItem>();
                List<string> planforUser = new List<string>();

                //string UseName = Convert.ToString(User.Identity.Name);
                var UserData = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.TieUpCompanyID }).FirstOrDefault();

                bool IsTVSTieUp = false;
                if (UserData.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID)
                {
                    IsTVSTieUp = true;
                }

                var UserPrivilegeDetails = (from u in db.AspNetUsers
                                            join up in db.tblUserPrivilegeDetails on u.Id equals up.UserID
                                            where u.UserName == UserName
                                            select new { up.Plans }).ToList();

                //
                var ProductCityPlanMappingPlanID = db.tblCityProductMapping.Where(w => w.ProductID == ProductID && w.StateID == StateID && w.CityID == CityID && w.IsValid != false).Select(s => s.PlanID).Distinct().ToList();
                //

                if (UserPrivilegeDetails.Any())
                {
                    planforUser = UserPrivilegeDetails.Where(w => w.Plans != null).Select(s => s.Plans.ToLower()).Distinct().ToList();
                    if (planforUser.Any())
                    {
                        int panForUserhaveALL = planforUser.Where(w => w.ToLower() == "all").Count();

                        if (IsTVSTieUp)
                        {
                            if (panForUserhaveALL > 0)
                            {
                                if (SumInsID == SumInsID25000)
                                {
                                    //var lstData = db.tblMstPlans.Where( a => a.ProductID == ProductID && a.EndDate==null && SumIns25000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();                                   

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns25000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID50000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID100000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID200000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (SumInsID == SumInsID25000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns25000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns25000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID50000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID100000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID200000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (panForUserhaveALL > 0)
                            {
                                if (SumInsID == SumInsID25000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns25000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns25000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID50000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID100000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID200000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (SumInsID == SumInsID25000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns25000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns25000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID50000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID100000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID200000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                var jsonResult = Json(new
                {
                    planlist = planListItem
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetPlanAmountByPlanID
        public ActionResult GetPlanAmountByPlanID(int PlanID)
        {
            try
            {
                var amountdata = db.tblMstPlans.Where(w => w.PlanID == PlanID).Select(s => new { s.Amount, s.GST, s.Total, s.ValidityYear, }).FirstOrDefault();
                if (amountdata != null)
                {
                    return Json(new { message = CrossCutting_Constant.Success, Amount = amountdata.Amount, GST = amountdata.GST, Total = amountdata.Total, Year = amountdata.ValidityYear }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { message = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { message = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region GetCityByStateID
        public ActionResult GetCityByStateID(int StateID)
        {
            try
            {
                List<SelectListItem> data = new List<SelectListItem>();
                var lstData = db.tblMstCities.Where(a => a.StateID == StateID && a.IsValid != false).Select(x => new { x.CityID, x.City }).OrderBy(o => o.City).ToList();
                foreach (var _item in lstData)
                {
                    data.Add(new SelectListItem { Value = _item.CityID.ToString(), Text = _item.City });
                }
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region SaveIssueCovidCertificate
        [HttpPost]
        public ActionResult SaveIssueCovidCertificate(TransactionModel transmodel)
        {
            string UserName = transmodel.UserName;

            // PlanIDs Start
            // Silver PlanIDs
            decimal?[] SilverPlansID = new decimal?[100];
            int SilverPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Silver" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (SilverPlanTypeID > 0)
            {
                string strSilverPlanTypeID = Convert.ToString(SilverPlanTypeID);
                decimal?[] strSilverPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strSilverPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strSilverPlansID.Any())
                {
                    SilverPlansID = strSilverPlansID;
                }
                else
                {
                    SilverPlansID = null;
                }
            }

            // Gold PlanIDs
            decimal?[] GoldPlansID = new decimal?[100];
            int GoldPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Gold" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (GoldPlanTypeID > 0)
            {
                string strGoldPlanTypeID = Convert.ToString(GoldPlanTypeID);
                decimal?[] strGoldPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strGoldPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strGoldPlansID.Any())
                {
                    GoldPlansID = strGoldPlansID;
                }
                else
                {
                    GoldPlansID = null;
                }
            }

            // Platinum PlanIDs
            decimal?[] PlatinumPlansID = new decimal?[100];
            int PlatinumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Platinum" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (PlatinumPlanTypeID > 0)
            {
                string strPlatinumPlanTypeID = Convert.ToString(PlatinumPlanTypeID);
                decimal?[] strPlatinumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strPlatinumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strPlatinumPlansID.Any())
                {
                    PlatinumPlansID = strPlatinumPlansID;
                }
                else
                {
                    PlatinumPlansID = null;
                }
            }

            // Palladium PlanIDs
            decimal?[] PalladiumPlansID = new decimal?[100];
            int PalladiumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Palladium" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (PalladiumPlanTypeID > 0)
            {
                string strPalladiumPlanTypeID = Convert.ToString(PalladiumPlanTypeID);
                decimal?[] strPalladiumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strPalladiumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strPalladiumPlansID.Any())
                {
                    PalladiumPlansID = strPalladiumPlansID;
                }
                else
                {
                    PalladiumPlansID = null;
                }
            }

            // Titanium PlanIDs
            decimal?[] TitaniumPlansID = new decimal?[100];
            int TitaniumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Titanium" && w.MasterType == "CovidPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (TitaniumPlanTypeID > 0)
            {
                string strTitaniumPlanTypeID = Convert.ToString(TitaniumPlanTypeID);
                decimal?[] strTitaniumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strTitaniumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strTitaniumPlansID.Any())
                {
                    TitaniumPlansID = strTitaniumPlansID;
                }
                else
                {
                    TitaniumPlansID = null;
                }
            }
            // PlanIDs End

            if (transmodel.PlanID > 0)
            {
                decimal PlanID = Convert.ToDecimal(transmodel.PlanID);

                if (SilverPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "No";
                    transmodel.SecondaryCover = "No";
                }
                else if (GoldPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "Yes";
                    transmodel.SecondaryCover = "No";
                }
                else if (PlatinumPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "Yes";
                    transmodel.SecondaryCover = "Yes";
                }
                else if (PalladiumPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "Yes";
                    transmodel.SecondaryCover = "Yes";
                }
                else if (TitaniumPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "Yes";
                    transmodel.SecondaryCover = "No";
                }
            }

            bool IsPayNow = true;
            bool onlinePay = true;
            string CertificateNo = "";
            string PaymentRefNo = "";
            string Path = "";

            vPayOnlinePaymentResponseModel vPayResponse = new vPayOnlinePaymentResponseModel();
            try
            {
                if (transmodel.InsuredDOB == null)
                {
                    ViewBag.ErrMsg = "Insured DOB Is Mandatory Field";
                    return Json(new { lblerrmsg = ViewBag.ErrMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    DateTime dtInsuredDOB = Convert.ToDateTime(transmodel.InsuredDOB);
                    DateTime now = DateTime.Now;

                    int MinAgeMonth = 0;
                    int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["CovidMinAgeLimitInMonths"]), out MinAgeMonth);

                    int MaxAgeMonth = 0;
                    int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["CovidMaxAgeLimitInMonths"]), out MaxAgeMonth);

                    int InsuredMonth = ((now.Year - dtInsuredDOB.Year) * 12) + now.Month - dtInsuredDOB.Month;

                    if (InsuredMonth < MinAgeMonth || InsuredMonth > MaxAgeMonth)
                    {
                        ViewBag.ErrMsg = "Insured DOB is Not Eligible for this Policy";
                        return Json(new { lblerrmsg = ViewBag.ErrMsg }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            DateTime CurrentDate = DateTime.Now;
                            tblTransaction model = new tblTransaction();
                            tblTransactionAuditLog audit = new tblTransactionAuditLog();
                            tblChequeAuditLog chequeaudit = new tblChequeAuditLog();
                            var productname = db.tblMstProducts.AsNoTracking().Where(w => w.ProductID == 11).Select(s => s.ProductName).FirstOrDefault();

                            AspNetUser userdata = db.AspNetUsers.Where(w => w.UserName == UserName).FirstOrDefault();

                            ObjectParameter Output = new ObjectParameter("UniqueCode", typeof(string));
                            var UniqueCodeNo = db.usp_UniqeNumberGeneration(CrossCutting_Constant.CertificatePrefix, "", "", "", "", "", Output);
                            model.CertificateNo = Convert.ToString(productname) + Convert.ToString(Output.Value);

                            model.Remark2 = Convert.ToString(TempData["PId"]);
                            model.Remark = transmodel.PrimaryCover + "/" + transmodel.SecondaryCover;
                            model.Remark1 = transmodel.DiseaseName;
                            model.SumInsuredID = transmodel.SumInsuredID;
                            model.IssueByID = CrossCutting_Constant.IssueBy_Application_ID;
                            model.UserID = userdata.Id;
                            model.CreatedDate = CurrentDate;
                            model.IsValid = true;
                            model.ProductID = 11;
                            model.PlanID = transmodel.PlanID;
                            model.CustomerName = transmodel.CustomerName;
                            model.CustomerMiddleName = transmodel.CustomerMiddleName;
                            model.CustomerLastName = transmodel.CustomerLastName;
                            model.CustomerContact = transmodel.CustomerContact;
                            model.GSTINNumber = transmodel.GSTINNumber;
                            //
                            model.CustomerEmail = transmodel.CustomerEmail;
                            model.InsuredGenderId = transmodel.InsuredGenderId;
                            model.InsuredDOB = transmodel.InsuredDOB;
                            model.NomineeName = transmodel.NomineeName;
                            model.NomineeGenderId = transmodel.NomineeGenderId;
                            model.NomineeDOB = transmodel.NomineeDOB;
                            model.NomineeRelationshipId = transmodel.NomineeRelationshipId;
                            //
                            if (userdata.RiskStartCount > 0)
                            {
                                int RiskStartCount = userdata.RiskStartCount;
                                string RiskDate = Convert.ToString(transmodel.CoverStartDate);
                                DateTime RiskStartDate = Convert.ToDateTime(RiskDate);
                                model.CoverStartDate = RiskStartDate.AddDays(RiskStartCount);
                            }
                            else
                            {
                                model.CoverStartDate = transmodel.CoverStartDate;
                            }

                            DateTime CovStartDate = Convert.ToDateTime(model.CoverStartDate);
                            if (transmodel.CoverEndDate == null)
                            {
                                var validYear = db.tblMstPlans.Where(w => w.PlanID == transmodel.PlanID).Select(s => s.ValidityYear).FirstOrDefault();
                                int validityYear = Convert.ToInt32(validYear);
                                model.CoverEndDate = CovStartDate.AddYears(validityYear).AddDays(-1);
                            }
                            else
                            {
                                if (userdata.RiskStartCount > 0)
                                {
                                    int RiskStartCount = userdata.RiskStartCount;
                                    string RiskDate = Convert.ToString(transmodel.CoverEndDate);
                                    DateTime RiskEndDate = Convert.ToDateTime(RiskDate);
                                    model.CoverEndDate = RiskEndDate.AddDays(RiskStartCount);
                                }
                                else
                                {
                                    model.CoverEndDate = transmodel.CoverEndDate;
                                }
                            }

                            model.IssueDate = CurrentDate;

                            if (transmodel.ChkIsSameAsPermanentAddress)
                            {
                                model.PermanentCityID = transmodel.PermanentCityID;
                                model.PermanentAddress = transmodel.PermanentAddress;
                                model.PermanentAddress2 = transmodel.PermanentAddress2;
                                //model.PermanentAddress3 = transmodel.PermanentAddress3;
                                //model.PermanentLandmark = transmodel.PermanentLandmark;
                                model.CommunicationCityID = transmodel.PermanentCityID;
                                model.CommunicationAddress = transmodel.PermanentAddress;
                                model.CommunicationAddress2 = transmodel.PermanentAddress2;
                                //model.CommunicationAddress3 = transmodel.PermanentAddress3;
                                //model.CommunicationLandmark = transmodel.PermanentLandmark;
                                model.IsSameAsPermanentAddress = true;
                            }
                            else
                            {
                                model.PermanentCityID = transmodel.PermanentCityID;
                                model.PermanentAddress = transmodel.PermanentAddress;
                                model.PermanentAddress2 = transmodel.PermanentAddress2;
                                //model.PermanentAddress3 = transmodel.PermanentAddress3;
                                //model.PermanentLandmark = transmodel.PermanentLandmark;
                                model.CommunicationCityID = transmodel.CommunicationCityID;
                                model.CommunicationAddress = transmodel.CommunicationAddress;
                                model.CommunicationAddress2 = transmodel.CommunicationAddress2;
                                //model.CommunicationAddress3 = transmodel.CommunicationAddress3;
                                //model.CommunicationLandmark = transmodel.CommunicationLandmark;
                                model.IsSameAsPermanentAddress = false;
                            }

                            model.StatusID = CrossCutting_Constant.StatusID_Pending_Payment;
                            model.PaymentID = CrossCutting_Constant.Payment_Online_ID;

                            if (model.PaymentID == CrossCutting_Constant.Payment_Online_ID)
                            {
                                model.StatusID = CrossCutting_Constant.StatusID_Pending_Payment;
                                IsPayNow = true;
                            }

                            decimal? CurrentPlanAmount = db.tblMstPlans.Where(w => w.PlanID == transmodel.PlanID).Select(s => s.Total).FirstOrDefault();

                            audit.UserID = model.UserID;
                            audit.StatusID = model.StatusID;
                            audit.Remark = model.Remark;
                            audit.ChequeNo = model.ChequeNo;
                            audit.ChequeDate = model.ChequeDate;
                            audit.ChequeAmount = model.ChequeAmount;
                            audit.IsValid = true;
                            audit.CreatedDateTime = CurrentDate;
                            audit.CertificateNo = model.CertificateNo;
                            model.tblTransactionAuditLogs.Add(audit);
                            db.tblTransactions.Add(model);
                            db.SaveChanges();

                            CertificateNo = model.CertificateNo;
                            PaymentRefNo = model.PaymentReferenceNo;

                            // Vpay Online Payment

                            TempData["Transactionid"] = model.TransactionID;
                            if (!string.IsNullOrEmpty(CertificateNo))
                            {
                                TempData["CertificateNo"] = model.CertificateNo;
                            }
                            TempData["payLater"] = false;

                            if ((IsPayNow && model.PaymentID == CrossCutting_Constant.Payment_Online_ID))
                            {
                                onlinePay = true;
                                ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));
                                var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.PaymentReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);

                                OnlinePaymentModel onlinepaymentmodel = new OnlinePaymentModel();
                                onlinepaymentmodel.IMDCode = userdata.IMDCode;
                                onlinepaymentmodel.PayerType = CrossCutting_Constant.PayerType_Agent;
                                onlinepaymentmodel.Amount = Convert.ToDecimal(CurrentPlanAmount);
                                onlinepaymentmodel.PaymentReferenceNo = Convert.ToString(OutputOfPaymentReference.Value);
                                onlinepaymentmodel.CreatedBy = userdata.Id;
                                onlinepaymentmodel.TransactionIDs = new List<decimal> { model.TransactionID };
                                onlinepaymentmodel.CustomerName = transmodel.CustomerName;
                                onlinepaymentmodel.ContactNo = Convert.ToString(transmodel.CustomerContact);
                                onlinepaymentmodel.Email = transmodel.CustomerEmail;

                                vPayResponse = this.MakeVPayOnlinePayment(onlinepaymentmodel);
                            }

                            if (!string.IsNullOrEmpty(CertificateNo))
                            {
                                Path = objHome.GetCertificatePath(model.TransactionID);
                                TempData["CertificatePath"] = Path;
                            }

                            db.SaveChanges();

                            return Json(new { status = CrossCutting_Constant.Success, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path, IsPayNow = onlinePay, vPayResponse = vPayResponse }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { status = CrossCutting_Constant.Failure, certificateno = "", paymentrefno = "", path = "" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region MisReportDecryptionPage
        public async System.Threading.Tasks.Task<ActionResult> MisReportDecryptionPage()
        {
            try
            {
                string MisReportDecryptionPageUserID = ConfigurationManager.AppSettings["MisReportDecryptionPageUserID"];
                string MisReportDecryptionPagePassword = ConfigurationManager.AppSettings["MisReportDecryptionPagePassword"];
                ViewData["MisReportDecryptionPageUserID"] = MisReportDecryptionPageUserID;
                ViewData["MisReportDecryptionPagePassword"] = MisReportDecryptionPagePassword;

                return View();
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }
        }

        #region MisEncryptedReportUpload
        [HttpPost]
        public ActionResult MisEncryptedReportUpload()
        {
            string filename = string.Empty;
            string sheetname = string.Empty;

            List<MISReportDecrypt> _EncReport = new List<MISReportDecrypt>();
            List<MISReportDecrypt> OutputExcel = new List<MISReportDecrypt>();

            try
            {
                HttpFileCollectionBase FileUpload = Request.Files;
                List<string> data = new List<string>();
                if (FileUpload != null)
                {
                    if (FileUpload[0].ContentType == CrossCutting_Constant.ContentType1 || FileUpload[0].ContentType == CrossCutting_Constant.ContentType2 || FileUpload[0].ContentType == CrossCutting_Constant.ContentType3)
                    {
                        filename = FileUpload[0].FileName;
                        string targetpath = Server.MapPath(CrossCutting_Constant.DocLocation);
                        FileUpload[0].SaveAs(targetpath + filename);
                        string pathToExcelFile = targetpath + filename;
                        var excelFile = new ExcelQueryFactory(pathToExcelFile);
                        sheetname = Path.GetFileNameWithoutExtension(FileUpload[0].FileName);
                        string SheetName = sheetname;
                        var BulkCertificationSheet = (from a in excelFile.Worksheet<MISReportDecrypt>(SheetName) select a).ToList();
                        _EncReport = BulkCertificationSheet;

                        foreach (var item in _EncReport)
                        {
                            item.CustomerMobileNo = (!string.IsNullOrEmpty(item.CustomerMobileNo)) ? Decode(item.CustomerMobileNo) : item.CustomerMobileNo;

                            OutputExcel.Add(item);
                        }

                        if ((System.IO.File.Exists(pathToExcelFile)))
                        {
                            System.IO.File.Delete(pathToExcelFile);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);

                if (!OutputExcel.Any())
                {
                    return View("~/Views/Error/NotFound.cshtml");
                }
            }

            if (OutputExcel.Any())
            {
                MemoryStream _MemoryStream = new MemoryStream();

                string[] HeaderArray = new string[] {
                            "LANAccountNo",
                            "CertificateNo",
                            "PaymentReferenceNo",
                            "ChallanNo",
                            "EngineNo",
                            "ChassisNo",
                            "RegistrationNo",
                            "Product",
                            "Make",
                            "Model",
                            "Variant",
                            "Plan",
                            "PlanAmount",
                            "GST (18%)",
                            "TotalPlanAmount",
                            "FirstName",
                            "MiddleName",
                            "LastName",
                            "CertificateIssueDate",
                            "CoverStartDate",
                            "CoverEndDate",
                            "CustomerMobileNo",
                            "EmailID",
                            "PermanentAddress1",
                            "PermanentAddress2",
                            "PermanentAddress3",
                            "PermanentLandmark",
                            "PermanentCity",
                            "PermanentState",
                            "PaymentMode",
                            "ChequeNo",
                            "ChequeDate",
                            "ChequeAmount",
                            "ChequeType",
                            "ChequePaymentType",
                            "Bank",
                            "Branch",
                            "PaymentStatus",
                            "PayoutStatus",
                            "LoginID",
                            "LoginName",
                            "DealerShare",
                            "GSTPayment",
                            "TDS",
                            "NetPaid",
                            "PayoutPercentage",
                            "DealerContact",
                            "PaymentMade_Y_N",
                            "PaymentDate",
                            "InvoiceReceived_Y_N",
                            "IMDCODE",
                            "TieUpCompany",
                            "TransactionID",
                            "TransactionDateTime",
                            "SubpaymentMode",
                            "PaymentStatus",
                            "CertificateStatus",
                            "InsuredGender",
                            "InsuredDOB",
                            "NomineeName",
                            "NomineeGender",
                            "NomineeDOB",
                            "NomineeRelationship",
                            "GSTINNumbar",
                            "BranchCode",
                            "AgentEmail",
                            "AgentName",
                            "PId"

                            };

                var listobj = OutputExcel.Select(x => new
                {
                    x.LANAccountNo,
                    x.CertificateNo,
                    x.PaymentReferenceNo,
                    x.ChallanNo,
                    x.EngineNo, 
                    x.ChassisNo, 
                    x.RegistrationNo,
                    x.Product,
                    x.Make,
                    x.Model,
                    x.Variant,
                    x.Plan,
                    x.PlanAmount,
                    x.GSTAmount,
                    x.TotalPlanAmount,
                    x.FirstName,
                    x.MiddleName,
                    x.LastName,
                    x.CertificateIssueDate,
                    x.CoverStartDate,
                    x.CoverEndDate,
                    x.CustomerMobileNo,
                    x.EmailID,
                    x.PermanentAddress1,
                    x.PermanentAddress2,
                    x.PermanentAddress3,
                    x.PermanentLandmark,
                    x.PermanentCity,
                    x.PermanentState,
                    x.PaymentMode,
                    x.ChequeNo,
                    x.ChequeDate,
                    x.ChequeAmount,
                    x.ChequeType,
                    x.ChequePaymentType,
                    x.Bank,
                    x.Branch,
                    x.PaymentStatus,
                    x.PayoutStatus,
                    x.LoginID,
                    x.LoginName,
                    x.DealerShare,
                    x.GSTPayment,
                    x.TDS,
                    x.NetPaid,
                    x.PayoutPercentage,
                    x.DealerContact,
                    x.PaymentMade_Y_N,
                    x.PaymentDateString,
                    x.InvoiceReceived_Y_N,
                    x.IMDCode,
                    x.TieUpCompany,
                    x.TransactionID,
                    x.TransactionDateTime,
                    x.SubpaymentMode,
                    x.OnlinePaymentStatus,
                    x.CertificateStatus,
                    x.InsuredGender,
                    x.InsuredDOB,
                    x.NomineeName,
                    x.NomineeGender,
                    x.NomineeDOB,
                    x.NomineeRelationship,
                    x.GSTINNumbar,
                    x.BranchCode,
                    x.AgentEmail,
                    x.AgentName,
                    x.PId

                }).ToList();


                using (StreamWriter write = new StreamWriter(_MemoryStream))
                {
                    using (CsvWriter csw = new CsvWriter(write))
                    {
                        foreach (var header in HeaderArray)
                        {
                            csw.WriteField(header);
                        }
                        csw.NextRecord();

                        foreach (var row in listobj)
                        {
                            csw.WriteRecord<dynamic>(row);
                            csw.NextRecord();
                        }
                    }
                }

                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + sheetname + "_" + DateTime.Now.ToString("ddMMyyyy-hhmmss") + ".csv");
                Response.Charset = "";
                Response.ContentType = "application/x-msexcel";
                Response.BinaryWrite(_MemoryStream.ToArray());
                Response.Flush();
                Response.End();
            }
            else
            {
                MemoryStream _MemoryStream = new MemoryStream();
                string[] HeaderArray = new string[] { "RESPONSE_REMARK" };
                string[] DataArray = new string[] { "Something Went Wrong in Decryption, Please Try Again" };

                using (StreamWriter write = new StreamWriter(_MemoryStream))
                {
                    using (CsvWriter csw = new CsvWriter(write))
                    {
                        foreach (var header in HeaderArray)
                        {
                            csw.WriteField(header);
                        }
                        csw.NextRecord();

                        foreach (var row in DataArray)
                        {
                            csw.WriteField(row);
                            csw.NextRecord();
                        }
                    }
                }

                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + sheetname + "_" + DateTime.Now.ToString("ddMMyyyy-hhmmss") + ".csv");
                Response.Charset = "";
                Response.ContentType = "application/x-msexcel";
                Response.BinaryWrite(_MemoryStream.ToArray());
                Response.Flush();
                Response.End();
            }

            return null;
        }
        #endregion

        #endregion

        [AllowAnonymous]
        public async System.Threading.Tasks.Task<ActionResult> OnlineRPPaymentResponse(string razorpay_payment_id, string razorpay_invoice_id, string razorpay_invoice_status, string razorpay_invoice_receipt, string razorpay_signature)
        {
            string RazorPaySecretKey = Convert.ToString(ConfigurationManager.AppSettings["RazorPaySecretKey"]);
            string expected_signature = razorpay_invoice_id + '|' +
                     razorpay_invoice_receipt + '|' +
                     razorpay_invoice_status + '|' +
                     razorpay_payment_id;

            string generated_signature = GetHMACSHA256(expected_signature, RazorPaySecretKey);
            if (generated_signature == razorpay_signature​)
            {
                vPayCallBackModel model = new vPayCallBackModel();
                model.Checksum = razorpay_signature​;
                model.transaction_id = razorpay_invoice_id;
                model.Status = (razorpay_invoice_status == "paid") ? CrossCutting_Constant.vPayCallback_Status_Success : CrossCutting_Constant.vPayCallback_Status_Failed;
                tblOnlinePaymentDetail _tblOnlinePaymentDetailnew = db.tblOnlinePaymentDetails.Where(w => w.transaction_id == model.transaction_id).FirstOrDefault();
                _tblOnlinePaymentDetailnew.Checksum = model.Checksum;
                db.SaveChanges();
               

                OnlinePaymentResponseModel responseModel = new OnlinePaymentResponseModel();

                string CertificatePath = string.Empty;
                string CertificateNo = string.Empty;
                try
                {
                    CertificateNo = Convert.ToString(TempData["CertificateNo"]);
                    CertificatePath = Convert.ToString(TempData["CertificatePath"]);

                    logger1.Error("Policy Bazar :: OnlinePaymentResponse()");
                    if (model != null)
                    {
                        this.vPayCallback(model);
                        if (model.Status == CrossCutting_Constant.vPayCallback_Status_Success)
                        {
                            responseModel.Status = CrossCutting_Constant.Success;
                            logger1.Error("Policy Bazar :: --Success--");
                            if (!string.IsNullOrEmpty(model.transaction_id))
                            {
                                tblOnlinePaymentDetail _tblOnlinePaymentDetail = db.tblOnlinePaymentDetails.Where(w => w.transaction_id == model.transaction_id).FirstOrDefault();
                                responseModel.PaymentReferenceNo = _tblOnlinePaymentDetail.PaymentReferenceNo;
                            }
                        }
                        else
                        {
                            responseModel.Status = CrossCutting_Constant.Failure;
                            responseModel.Remark = "Payment is in '" + model.Status + "' Status.";
                            logger1.Error("Policy Bazar :: Payment is in " + model.Status);
                        }
                    }
                    else
                    {
                        responseModel.Status = CrossCutting_Constant.Failure;
                        responseModel.Remark = "Policy Bazar :: Response is not found.";
                        logger1.Error(" Policy Bazar :: Response is not found.");
                    }
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                    responseModel.Status = CrossCutting_Constant.Failure;
                    responseModel.Remark = "Something went wrong. please try again.";
                    logger1.Error("Policy Bazar :: Something went wrong. please try again.");
                }

                //return View(responseModel);

                string PayTransactionId = model.transaction_id;
                var GetUserByPayTransactionId = db.tblOnlinePaymentDetails.Where(w => w.transaction_id == PayTransactionId).FirstOrDefault();
                if (GetUserByPayTransactionId != null)
                {
                    string PaymentReferenceNo = GetUserByPayTransactionId.PaymentReferenceNo;
                    decimal tblOnlinePayID = GetUserByPayTransactionId.OnlinePaymentID;
                    var GettblTransDatabytblOnlinePayID = db.tblTransactions.Where(w => w.OnlinePaymentID == tblOnlinePayID).FirstOrDefault();
                    if (GettblTransDatabytblOnlinePayID != null)
                    {
                        string tblCertificateNo = GettblTransDatabytblOnlinePayID.CertificateNo;
                        string UserID = GettblTransDatabytblOnlinePayID.UserID;

                        var CertificateTrnsCheckByUser = db.tblTransactions.Where(w => w.CertificateNo == tblCertificateNo && w.UserID == UserID).FirstOrDefault();
                        if (CertificateTrnsCheckByUser != null)
                        {
                            decimal Transactionid = CertificateTrnsCheckByUser.TransactionID;
                            string ResponseStatus = model.Status;
                            if (ResponseStatus == CrossCutting_Constant.vPayCallback_Status_Success)
                            {
                                CertificateNo = CertificateTrnsCheckByUser.CertificateNo;
                                decimal ProductID = Convert.ToDecimal(CertificateTrnsCheckByUser.ProductID);
                                int IssueByID = Convert.ToInt32(CertificateTrnsCheckByUser.IssueByID);

                                if (string.IsNullOrEmpty(CertificatePath))
                                {
                                    HomeController hm = new HomeController();

                                    if (ProductID == 19)
                                    {
                                        if (IssueByID == CrossCutting_Constant.IssueBy_API_ID)
                                        {
                                            CertificatePath = hm.GetCertificatePath(Transactionid);
                                        }
                                        else
                                        {
                                            // Product : Hospital Cash (HC)
                                            string PathGA = string.Empty;
                                            string RiskCovryMergePath = string.Empty;

                                            PathGA = hm.GetCertificatePath(Transactionid);

                                            tblHospiCashDetails tblHospiCashDetailsObj = db.tblHospiCashDetails.Where(w => w.TransactionID == Transactionid).FirstOrDefault();
                                            if (tblHospiCashDetailsObj != null)
                                            {
                                                string DownloadURLCOI = tblHospiCashDetailsObj.COI;
                                                string RiskCovryPolicyNumber = tblHospiCashDetailsObj.PolicyNumber;
                                                string RiskCovryPath = this.GetDataToAPIForCOI(DownloadURLCOI, RiskCovryPolicyNumber);

                                                // PDF's Merge Code Start
                                                string GAPolicyNumber = tblHospiCashDetailsObj.CustomerRefNo;
                                                string OutFileName = tblHospiCashDetailsObj.CertificateFileName;

                                                RiskCovryMergePath = this.MergePDF(PathGA, RiskCovryPath, OutFileName);
                                                // PDF's Merge Code End
                                            }

                                            CertificatePath = RiskCovryMergePath;
                                        }                                       
                                    }
                                    else
                                    {
                                        CertificatePath = hm.GetCertificatePath(Transactionid);
                                    }                                  
                                }

                                if (!string.IsNullOrEmpty(CertificateNo))
                                {
                                    responseModel.CertificateNo = CertificateNo;
                                }

                                if (!string.IsNullOrEmpty(CertificatePath))
                                {
                                    responseModel.CertificatePath = CertificatePath;
                                }

                                // Success URL
                                var VendorResponseUrl = db.tblVendorResponseUrl.Where(w => w.UserId == UserID && w.IsValid != false).FirstOrDefault();

                                if (CertificateTrnsCheckByUser.Remark.ToLower().Equals("isbyapi_pcexternallinkgeneration"))
                                {
                                    VendorResponseUrl = null;
                                }

                                if (VendorResponseUrl != null && VendorResponseUrl.ResponseUrl.ToLower().Contains("riskcovry.com"))
                                {
                                    string ResUrl = VendorResponseUrl.ResponseUrl;// + "?carrier="+ Encode("Carriers::GlobalAssure::Payment");
                                    string fstqry = "Carriers::GlobalAssure::Payment";

                                    string PaymetStatus = (razorpay_invoice_status == "paid") ? "SUCCESS" : "FAIL";
                                    string secqry = "{\"Status\": \""+ PaymetStatus + "\",\"CertificateNo\": \""+ CertificateNo + "\",\"PolicyNo\": \""+ CertificateNo + "\",\"TransactionNo\": \""+ razorpay_invoice_id + "\"}";

                                    string FinalQry = "?carrier=" + fstqry + "&enc=" + Encode(secqry);
                                  
                                    string FullResUrl = ResUrl + FinalQry;
                                    return Redirect(FullResUrl);
                                }

                                if (VendorResponseUrl != null)
                                {
                                    string ResUrl = VendorResponseUrl.ResponseUrl + "?Param=";
                                    string Query = "PaymentRefNo=" + responseModel.PaymentReferenceNo + "&CertificateNo=" + tblCertificateNo + "&Remark=" + model.Status + "";
                                    string EnQuery = Encode(Query);
                                    string FullResUrl = ResUrl + EnQuery;
                                    return Redirect(FullResUrl);
                                }
                                else
                                {
                                    return View(responseModel);
                                }
                            }
                            else
                            {
                                // Failed URL
                                var VendorResponseUrl = db.tblVendorResponseUrl.Where(w => w.UserId == UserID && w.IsValid != false).FirstOrDefault();

                                if (CertificateTrnsCheckByUser.Remark.ToLower().Equals("isbyapi_pcexternallinkgeneration"))
                                {
                                    VendorResponseUrl = null;
                                }

                                if (VendorResponseUrl != null)
                                {
                                    string ResUrl = VendorResponseUrl.ResponseFailedUrl + "?Param=";
                                    string Query = "PaymentRefNo=" + null + "&CertificateNo=" + null + "&Remark=" + model.Status + "";
                                    string EnQuery = Encode(Query);
                                    string FullResUrl = ResUrl + EnQuery;
                                    return Redirect(FullResUrl);
                                }
                                else
                                {
                                    return View(responseModel);
                                }
                            }
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }

            return View();
        }


        #region IssueCovidIndemnityCertificate
        public async System.Threading.Tasks.Task<ActionResult> IssueCovidIndemnityCertificate(string pid)
        {
            //string EncodedSample = Encode(pid);

            // Link Closed Emails Start
            string LinkClosedEmailsCI = ConfigurationManager.AppSettings["LinkClosedEmailsCI"];
            string[] lstLinkClosedEmailsCI = LinkClosedEmailsCI.Split(',');
            // Link Closed Emails End

            // UserNames Start
            string CovidExternalUserCIID1 = ConfigurationManager.AppSettings["CovidExternalUserCIID1"];
            string CovidExternalUserCIID2 = ConfigurationManager.AppSettings["CovidExternalUserCIID2"];
            // UserNames End

            // PlanIDs Start
            // Silver PlanIDs
            int SilverPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Silver" && w.MasterType == "CovidIndemnityPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (SilverPlanTypeID > 0)
            {
                string strSilverPlanTypeID = Convert.ToString(SilverPlanTypeID);
                decimal?[] strSilverPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strSilverPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strSilverPlansID.Any())
                {
                    ViewData["SilverPlansID"] = strSilverPlansID;
                }
                else
                {
                    ViewData["SilverPlansID"] = null;
                }
            }

            // Gold PlanIDs
            int GoldPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Gold" && w.MasterType == "CovidIndemnityPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (GoldPlanTypeID > 0)
            {
                string strGoldPlanTypeID = Convert.ToString(GoldPlanTypeID);
                decimal?[] strGoldPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strGoldPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strGoldPlansID.Any())
                {
                    ViewData["GoldPlansID"] = strGoldPlansID;
                }
                else
                {
                    ViewData["GoldPlansID"] = null;
                }
            }

            // Platinum PlanIDs
            int PlatinumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Platinum" && w.MasterType == "CovidIndemnityPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (PlatinumPlanTypeID > 0)
            {
                string strPlatinumPlanTypeID = Convert.ToString(PlatinumPlanTypeID);
                decimal?[] strPlatinumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strPlatinumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strPlatinumPlansID.Any())
                {
                    ViewData["PlatinumPlansID"] = strPlatinumPlansID;
                }
                else
                {
                    ViewData["PlatinumPlansID"] = null;
                }
            }

            // Palladium PlanIDs
            int PalladiumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Palladium" && w.MasterType == "CovidIndemnityPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (PalladiumPlanTypeID > 0)
            {
                string strPalladiumPlanTypeID = Convert.ToString(PalladiumPlanTypeID);
                decimal?[] strPalladiumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strPalladiumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strPalladiumPlansID.Any())
                {
                    ViewData["PalladiumPlansID"] = strPalladiumPlansID;
                }
                else
                {
                    ViewData["PalladiumPlansID"] = null;
                }
            }
            // PlanIDs End

            ViewBag.ErrMsg = "Here";

            TransactionModel model = new TransactionModel();
            try
            {
                // Request Parameter Samples
                // string encryptMessage = enc.Encrypt("pid=12345&email=gaurav.arora@globalassure.com");

                string decryptPid = Decode(pid);
                int temppid = 0;
                int.TryParse(decryptPid, out temppid);
                //   if (!string.IsNullOrEmpty(decryptPid) && temppid>0)
                //   {
                //       ViewBag.Error = "under maintenance. try again";
                //       return View("~/Views/WebPortal/Error.cshtml");

                //   }

                //else 
                if (!string.IsNullOrEmpty(decryptPid))
                {
                    if (decryptPid.ToLower().Contains("&email"))
                    {
                        string[] arrDecPid = decryptPid.Split('&');
                        if (arrDecPid.Any())
                        {
                            string param = arrDecPid[0];
                            model.PId = param;
                            TempData["PIdCI"] = param;

                            string[] arrEmail = arrDecPid[1].Split('=');
                            if (arrEmail.Any())
                            {
                                string email = arrEmail[1];

                                if (!lstLinkClosedEmailsCI.Contains(email.ToLower()))
                                {
                                    var emailCheckInTbl = db.AspNetUsers.Where(w => w.Email == email && w.IsActive != false).ToList();
                                    if (emailCheckInTbl.Any())
                                    {
                                        string UserName = emailCheckInTbl[0].Email;
                                        TempData["UserName"] = UserName;
                                        Session["UserName"] = UserName;
                                    }
                                    else
                                    {
                                        TempData["UserName"] = CovidExternalUserCIID2;
                                        Session["UserName"] = CovidExternalUserCIID2;
                                    }
                                }
                                else
                                {
                                    ViewBag.Error = "This Link Has Been Closed.";
                                    return View("~/Views/WebPortal/Error.cshtml");
                                }
                            }
                            else
                            {
                                ViewBag.Error = "//3:: Parameter Not Found or Decryption Error";
                                return View("~/Views/WebPortal/Error.cshtml");
                            }
                        }
                        else
                        {
                            ViewBag.Error = "//2:: Parameter Not Found or Decryption Error";
                            return View("~/Views/WebPortal/Error.cshtml");
                        }
                    }
                    else
                    {
                        string param = decryptPid;
                        model.PId = param;
                        TempData["PIdCI"] = param;

                        TempData["UserName"] = CovidExternalUserCIID1;
                        Session["UserName"] = CovidExternalUserCIID1;
                    }

                    model = GetIssueCovidIndemnityCertificate(Convert.ToString(TempData["UserName"]));
                    model.IssueByID = CrossCutting_Constant.IssueBy_Application_ID;

                    model.UserName = Convert.ToString(TempData["UserName"]);
                }
                else
                {
                    ViewBag.Error = "//1:: Parameter Not Found or Decryption Error";
                    return View("~/Views/WebPortal/Error.cshtml");
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }

            return View(model);
        }

        public TransactionModel GetIssueCovidIndemnityCertificate(string UserName)
        {

            TransactionModel model = new TransactionModel();

            model.PaymentID = CrossCutting_Constant.Payment_Cheque_ID;
            model.IssueDate = DateTime.Now;
            model.CoverStartDate = DateTime.Now;
            //  model.CoverEndDate = DateTime.Now.AddDays(364);

            model.IsChequePaymentTypeIntermediary = true;
            model.IsSingleChequeSinglePolicy = false;
            model.IsChequeTypeLocal = true;

            model.PermanentStateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                              .ToDictionary(d => d.StateID, d => d.State);

            model.BankListItem = db.tblMstBanks.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.BankID, s.BankCode, s.BankName })
                                  .ToDictionary(d => d.BankID, d => d.BankName);

            model.TransactionTypeListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.TransactionType)
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            model.InsuredGenderListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.InsuredGender)
                                           .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            model.NomineeGenderListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.NomineeGender)
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            //CovidIndemnity
            model.SumInsuredListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "CovidIndemnity")
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            model.ProposerRelationshipListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "CovidIndemnityProposerRelationship")
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            model.NomineeRelationshipListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "CovidIndemnityNomineeRelationship")
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            model.ProposerGenderListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "ProposerGender")
                                            .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);
            //

            var commontypedata = db.tblMstCommonTypes.Where(w => w.MasterType == CrossCutting_Constant.ChequeType || w.MasterType == CrossCutting_Constant.ChequeForPolicy)
                                   .Select(s => new { s.CommonTypeID, s.Description, s.MasterType }).ToList();


            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.DealerName, s.TotalUsedPlanAmount, s.PaymentReceivedAmount, s.TieUpCompanyID, s.TotalCDBalanceAmount }).FirstOrDefault();

            model.DealerName = userdata.DealerName;
            model.TotalUsedPlanAmount = Convert.ToDecimal(userdata.TotalUsedPlanAmount);
            model.TotalPaymentReceivedAmount = Convert.ToDecimal(userdata.PaymentReceivedAmount);
            var TotalFloatPlanAmount = db.tblUserFloatAmounts.Where(w => w.UserID == userdata.Id && w.Amount != null && w.IsValid != false).Sum(s => s.Amount);
            model.TotalFloatPlanAmount = Convert.ToDecimal(TotalFloatPlanAmount);

            var TotalAmounts = (model.TotalFloatPlanAmount + model.TotalPaymentReceivedAmount);

            var TotalBalanceAmount = (TotalAmounts - model.TotalUsedPlanAmount);

            model.TotalBalanceAmount = (TotalBalanceAmount < 0) ? 0 : TotalBalanceAmount;

            model.Total_CD_Balance = userdata.TotalCDBalanceAmount;

            var UserPrivilegeDetails = (from u in db.AspNetUsers
                                        join up in db.tblUserPrivilegeDetails on u.Id equals up.UserID
                                        where u.UserName == UserName
                                        select new { up.Payment, up.Product, up.Make, up.Model, up.Plans }).ToList();

            List<string> ProductforUser = new List<string>();
            List<string> PaymentforUser = new List<string>();

            if (UserPrivilegeDetails.Any())
            {
                ProductforUser = UserPrivilegeDetails.Where(w => w.Product != null && w.Product != "").Select(s => s.Product.ToLower()).Distinct().ToList();
                PaymentforUser = UserPrivilegeDetails.Where(w => w.Payment != null && w.Payment != "").Select(s => s.Payment.ToLower()).Distinct().ToList();

                if (ProductforUser.Any())
                {
                    int productForUserhaveALL = ProductforUser.Where(w => w.ToLower() == "all").Count();
                    if (productForUserhaveALL > 0)
                    {
                        model.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false)
                                      .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);
                    }
                    else
                    {
                        model.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false && ProductforUser.Contains(w.ProductName.ToLower()))
                                      .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);
                    }
                }



                model.PaymentListItem = db.tblMstPayments.AsNoTracking().Where(w => w.IsValid != false)
                                         .Select(s => new { s.PaymentID, s.Payment }).ToDictionary(d => d.PaymentID, d => d.Payment);
                model.PaymentListItem.OrderBy(x => x.Key);

            }

            return model;
        }
        #endregion

        #region GetCIPlanByProductIDAndSumInsIDForUser
        public ActionResult GetCIPlanByProductIDAndSumInsIDForUser(int ProductID, int SumInsID, int StateID, int CityID)
        {
            try
            {
                //string UserName = ConfigurationManager.AppSettings["CovidExternalUserCIID1"];
                string UserName = Convert.ToString(Session["UserName"]);

                // SumInsuredIDs
                int SumInsID50000 = db.tblMstCommonTypes.Where(w => w.Description == "50000" && w.MasterType == "CovidIndemnity" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
                int SumInsID100000 = db.tblMstCommonTypes.Where(w => w.Description == "100000" && w.MasterType == "CovidIndemnity" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
                int SumInsID200000 = db.tblMstCommonTypes.Where(w => w.Description == "200000" && w.MasterType == "CovidIndemnity" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
                int SumInsID300000 = db.tblMstCommonTypes.Where(w => w.Description == "300000" && w.MasterType == "CovidIndemnity" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
                int SumInsID500000 = db.tblMstCommonTypes.Where(w => w.Description == "500000" && w.MasterType == "CovidIndemnity" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();

                // PlanIDs                
                // 50000
                decimal?[] SumIns50000PlansID = new decimal?[100];
                decimal?[] decSumIns50000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID50000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns50000PlansID.Any())
                {
                    SumIns50000PlansID = decSumIns50000PlansID;
                }
                else
                {
                    SumIns50000PlansID = null;
                }

                // 100000
                decimal?[] SumIns100000PlansID = new decimal?[100];
                decimal?[] decSumIns100000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID100000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns100000PlansID.Any())
                {
                    SumIns100000PlansID = decSumIns100000PlansID;
                }
                else
                {
                    SumIns100000PlansID = null;
                }

                // 200000
                decimal?[] SumIns200000PlansID = new decimal?[100];
                decimal?[] decSumIns200000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID200000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns200000PlansID.Any())
                {
                    SumIns200000PlansID = decSumIns200000PlansID;
                }
                else
                {
                    SumIns200000PlansID = null;
                }

                // 300000
                decimal?[] SumIns300000PlansID = new decimal?[100];
                decimal?[] decSumIns300000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID300000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns300000PlansID.Any())
                {
                    SumIns300000PlansID = decSumIns300000PlansID;
                }
                else
                {
                    SumIns300000PlansID = null;
                }

                // 500000
                decimal?[] SumIns500000PlansID = new decimal?[100];
                decimal?[] decSumIns500000PlansID = db.tblPlansMapping.Where(w => w.SumInsuredID == SumInsID500000 && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (decSumIns500000PlansID.Any())
                {
                    SumIns500000PlansID = decSumIns500000PlansID;
                }
                else
                {
                    SumIns500000PlansID = null;
                }

                List<SelectListItem> planListItem = new List<SelectListItem>();
                List<string> planforUser = new List<string>();

                //string UseName = Convert.ToString(User.Identity.Name);
                var UserData = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.TieUpCompanyID }).FirstOrDefault();

                bool IsTVSTieUp = false;
                if (UserData.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID)
                {
                    IsTVSTieUp = true;
                }

                var UserPrivilegeDetails = (from u in db.AspNetUsers
                                            join up in db.tblUserPrivilegeDetails on u.Id equals up.UserID
                                            where u.UserName == UserName
                                            select new { up.Plans }).ToList();

                //
                var ProductCityPlanMappingPlanID = db.tblCityProductMapping.Where(w => w.ProductID == ProductID && w.StateID == StateID && w.CityID == CityID && w.IsValid != false).Select(s => s.PlanID).Distinct().ToList();
                //

                if (UserPrivilegeDetails.Any())
                {
                    planforUser = UserPrivilegeDetails.Where(w => w.Plans != null).Select(s => s.Plans.ToLower()).Distinct().ToList();
                    if (planforUser.Any())
                    {
                        int panForUserhaveALL = planforUser.Where(w => w.ToLower() == "all").Count();

                        if (IsTVSTieUp)
                        {
                            if (panForUserhaveALL > 0)
                            {
                                if (SumInsID == SumInsID50000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID100000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID200000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID300000)
                                {
                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns300000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID500000)
                                {
                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns500000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (SumInsID == SumInsID50000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID100000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID200000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID300000)
                                {
                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns300000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID500000)
                                {
                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns500000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (panForUserhaveALL > 0)
                            {
                                if (SumInsID == SumInsID50000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID100000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID200000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID300000)
                                {
                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns300000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID500000)
                                {
                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && SumIns500000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (SumInsID == SumInsID50000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns50000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID100000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns100000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID200000)
                                {
                                    //var lstData = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                    //foreach (var _item in lstData)
                                    //{
                                    //    planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                    //}

                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns200000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID300000)
                                {
                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns300000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                                else if (SumInsID == SumInsID500000)
                                {
                                    var lst = db.tblMstPlans.Where(a => a.ProductID == ProductID && planforUser.Contains(a.Name.ToLower()) && SumIns500000PlansID.Contains(a.PlanID) && a.IsValid != false && a.TieUpCompanyID == null).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();
                                    if (lst.Any())
                                    {
                                        var lstData = lst.Where(w => !ProductCityPlanMappingPlanID.Contains(w.PlanID)).Select(x => new { x.PlanID, x.Name }).OrderBy(o => o.PlanID).ToList();

                                        foreach (var _item in lstData)
                                        {
                                            planListItem.Add(new SelectListItem { Value = _item.PlanID.ToString(), Text = _item.Name });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                var jsonResult = Json(new
                {
                    planlist = planListItem
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region SaveIssueCovidIndemnityCertificate
        [HttpPost]
        public ActionResult SaveIssueCovidIndemnityCertificate(TransactionModel transmodel)
        {
            string UserName = transmodel.UserName;

            // PlanIDs Start
            // Silver PlanIDs
            decimal?[] SilverPlansID = new decimal?[100];
            int SilverPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Silver" && w.MasterType == "CovidIndemnityPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (SilverPlanTypeID > 0)
            {
                string strSilverPlanTypeID = Convert.ToString(SilverPlanTypeID);
                decimal?[] strSilverPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strSilverPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strSilverPlansID.Any())
                {
                    SilverPlansID = strSilverPlansID;
                }
                else
                {
                    SilverPlansID = null;
                }
            }

            // Gold PlanIDs
            decimal?[] GoldPlansID = new decimal?[100];
            int GoldPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Gold" && w.MasterType == "CovidIndemnityPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (GoldPlanTypeID > 0)
            {
                string strGoldPlanTypeID = Convert.ToString(GoldPlanTypeID);
                decimal?[] strGoldPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strGoldPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strGoldPlansID.Any())
                {
                    GoldPlansID = strGoldPlansID;
                }
                else
                {
                    GoldPlansID = null;
                }
            }

            // Platinum PlanIDs
            decimal?[] PlatinumPlansID = new decimal?[100];
            int PlatinumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Platinum" && w.MasterType == "CovidIndemnityPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (PlatinumPlanTypeID > 0)
            {
                string strPlatinumPlanTypeID = Convert.ToString(PlatinumPlanTypeID);
                decimal?[] strPlatinumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strPlatinumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strPlatinumPlansID.Any())
                {
                    PlatinumPlansID = strPlatinumPlansID;
                }
                else
                {
                    PlatinumPlansID = null;
                }
            }

            // Palladium PlanIDs
            decimal?[] PalladiumPlansID = new decimal?[100];
            int PalladiumPlanTypeID = db.tblMstCommonTypes.Where(w => w.Description == "Palladium" && w.MasterType == "CovidIndemnityPlansType" && w.IsValid != false).Select(s => s.CommonTypeID).FirstOrDefault();
            if (PalladiumPlanTypeID > 0)
            {
                string strPalladiumPlanTypeID = Convert.ToString(PalladiumPlanTypeID);
                decimal?[] strPalladiumPlansID = db.tblPlansMapping.Where(w => w.Remark1 == strPalladiumPlanTypeID && w.IsValid != false).Select(s => s.PlanID).ToArray();
                if (strPalladiumPlansID.Any())
                {
                    PalladiumPlansID = strPalladiumPlansID;
                }
                else
                {
                    PalladiumPlansID = null;
                }
            }
            // PlanIDs End

            if (transmodel.PlanID > 0)
            {
                decimal PlanID = Convert.ToDecimal(transmodel.PlanID);

                if (SilverPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "No";
                    transmodel.SecondaryCover = "No";
                }
                else if (GoldPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "Yes";
                    transmodel.SecondaryCover = "No";
                }
                else if (PlatinumPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "Yes";
                    transmodel.SecondaryCover = "Yes";
                }
                else if (PalladiumPlansID.Contains(PlanID))
                {
                    transmodel.PrimaryCover = "Yes";
                    transmodel.SecondaryCover = "Yes";
                }
            }

            bool IsPayNow = false;
            bool onlinePay = false;
            string CertificateNo = "";
            string PaymentRefNo = "";
            string Path = "";

            vPayOnlinePaymentResponseModel vPayResponse = new vPayOnlinePaymentResponseModel();
            try
            {
                if (transmodel.InsuredDOB == null)
                {
                    ViewBag.ErrMsg = "Insured DOB Is Mandatory Field";
                    return Json(new { lblerrmsg = ViewBag.ErrMsg }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    DateTime dtInsuredDOB = Convert.ToDateTime(transmodel.InsuredDOB);
                    DateTime now = DateTime.Now;

                    int MinAgeMonth = 0;
                    int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["CovidIndemnityMinAgeLimitInMonths"]), out MinAgeMonth);

                    int MaxAgeMonth = 0;
                    int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["CovidIndemnityMaxAgeLimitInMonths"]), out MaxAgeMonth);

                    int InsuredMonth = ((now.Year - dtInsuredDOB.Year) * 12) + now.Month - dtInsuredDOB.Month;

                    bool IsDOBBelow55 = ValidateInsuredDOBBelow55(dtInsuredDOB);

                    //if (InsuredMonth < MinAgeMonth || InsuredMonth > MaxAgeMonth)
                    if (InsuredMonth < MinAgeMonth || IsDOBBelow55 == false)
                    {
                        ViewBag.ErrMsg = "Insured DOB is Not Eligible for this Policy";
                        return Json(new { lblerrmsg = ViewBag.ErrMsg }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string strNomineeDOB = Convert.ToDateTime(transmodel.NomineeDOB).ToString("dd/MM/yyyy");

                        bool IsDOBBelow18Years = ValidationForAgeBelow18Years(strNomineeDOB);
                        if (IsDOBBelow18Years == true)
                        {
                            ViewBag.ErrMsg = "Nominee DOB is Not Eligible for this Policy";
                            return Json(new { lblerrmsg = ViewBag.ErrMsg }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (transmodel.ProposerDOB != null)
                            {
                                string strProposerDOB = Convert.ToDateTime(transmodel.ProposerDOB).ToString("dd/MM/yyyy");

                                bool IsProposerDOBBelow18Years = ValidationForAgeBelow18Years(strProposerDOB);
                                if (IsProposerDOBBelow18Years == true)
                                {
                                    ViewBag.ErrMsg = "Proposer DOB is Not Eligible for this Policy";
                                    return Json(new { lblerrmsg = ViewBag.ErrMsg }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {

                                }
                            }
                            else
                            {

                            }

                            if (ModelState.IsValid)
                            {
                                DateTime CurrentDate = DateTime.Now;
                                tblTransaction model = new tblTransaction();
                                tblTransactionAuditLog audit = new tblTransactionAuditLog();
                                tblChequeAuditLog chequeaudit = new tblChequeAuditLog();
                                var productname = db.tblMstProducts.AsNoTracking().Where(w => w.ProductID == 12).Select(s => s.ProductName).FirstOrDefault();

                                AspNetUser userdata = db.AspNetUsers.Where(w => w.UserName == UserName).FirstOrDefault();

                                ObjectParameter Output = new ObjectParameter("UniqueCode", typeof(string));
                                var UniqueCodeNo = db.usp_UniqeNumberGeneration(CrossCutting_Constant.CertificatePrefix, "", "", "", "", "", Output);

                                model.CertificateNo = Convert.ToString(productname) + Convert.ToString(Output.Value);

                                model.Remark2 = Convert.ToString(TempData["PIdCI"]);
                                model.Remark = transmodel.PrimaryCover + "/" + transmodel.SecondaryCover;
                                model.Remark1 = transmodel.DiseaseName;
                                model.SumInsuredID = transmodel.SumInsuredID;
                                model.IssueByID = CrossCutting_Constant.IssueBy_Application_ID;
                                model.UserID = userdata.Id;
                                model.CreatedDate = CurrentDate;
                                model.IsValid = true;
                                model.ProductID = 12;
                                model.PlanID = transmodel.PlanID;
                                model.CustomerName = transmodel.CustomerName;
                                model.CustomerMiddleName = transmodel.CustomerMiddleName;
                                model.CustomerLastName = transmodel.CustomerLastName;
                                model.CustomerContact = transmodel.CustomerContact;
                                model.GSTINNumber = transmodel.GSTINNumber;
                                //
                                model.CustomerEmail = transmodel.CustomerEmail;
                                model.InsuredGenderId = transmodel.InsuredGenderId;
                                model.InsuredDOB = transmodel.InsuredDOB;
                                model.NomineeName = transmodel.NomineeName;
                                model.NomineeGenderId = transmodel.NomineeGenderId;
                                model.NomineeDOB = transmodel.NomineeDOB;
                                model.NomineeRelationshipId = transmodel.NomineeRelationshipId;
                                //
                                //
                                model.ProposerName = transmodel.ProposerName;
                                model.ProposerGenderId = transmodel.ProposerGenderId;
                                model.ProposerDOB = transmodel.ProposerDOB;
                                model.ProposerRelationshipId = transmodel.ProposerRelationshipId;
                                //

                                if (userdata.RiskStartCount > 0)
                                {
                                    int RiskStartCount = userdata.RiskStartCount;
                                    string RiskDate = Convert.ToString(transmodel.CoverStartDate);
                                    DateTime RiskStartDate = Convert.ToDateTime(RiskDate);
                                    model.CoverStartDate = RiskStartDate.AddDays(RiskStartCount);
                                }
                                else
                                {
                                    model.CoverStartDate = transmodel.CoverStartDate;
                                }

                                DateTime CovStartDate = Convert.ToDateTime(model.CoverStartDate);
                                if (transmodel.CoverEndDate == null)
                                {
                                    var validYear = db.tblMstPlans.Where(w => w.PlanID == transmodel.PlanID).Select(s => s.ValidityYear).FirstOrDefault();
                                    int validityYear = Convert.ToInt32(validYear);

                                    model.CoverEndDate = CovStartDate.AddYears(validityYear).AddDays(-1);
                                }
                                else
                                {
                                    if (userdata.RiskStartCount > 0)
                                    {
                                        int RiskStartCount = userdata.RiskStartCount;
                                        string RiskDate = Convert.ToString(transmodel.CoverEndDate);
                                        DateTime RiskEndDate = Convert.ToDateTime(RiskDate);
                                        model.CoverEndDate = RiskEndDate.AddDays(RiskStartCount);
                                    }
                                    else
                                    {
                                        model.CoverEndDate = transmodel.CoverEndDate;
                                    }

                                }

                                model.IssueDate = CurrentDate;

                                if (transmodel.ChkIsSameAsPermanentAddress)
                                {
                                    model.PermanentCityID = transmodel.PermanentCityID;
                                    model.PermanentAddress = transmodel.PermanentAddress;
                                    model.PermanentAddress2 = transmodel.PermanentAddress2;
                                    //model.PermanentAddress3 = transmodel.PermanentAddress3;
                                    //model.PermanentLandmark = transmodel.PermanentLandmark;
                                    model.CommunicationCityID = transmodel.PermanentCityID;
                                    model.CommunicationAddress = transmodel.PermanentAddress;
                                    model.CommunicationAddress2 = transmodel.PermanentAddress2;
                                    //model.CommunicationAddress3 = transmodel.PermanentAddress3;
                                    //model.CommunicationLandmark = transmodel.PermanentLandmark;
                                    model.IsSameAsPermanentAddress = true;
                                }
                                else
                                {
                                    model.PermanentCityID = transmodel.PermanentCityID;
                                    model.PermanentAddress = transmodel.PermanentAddress;
                                    model.PermanentAddress2 = transmodel.PermanentAddress2;
                                    //model.PermanentAddress3 = transmodel.PermanentAddress3;
                                    //model.PermanentLandmark = transmodel.PermanentLandmark;
                                    model.CommunicationCityID = transmodel.CommunicationCityID;
                                    model.CommunicationAddress = transmodel.CommunicationAddress;
                                    model.CommunicationAddress2 = transmodel.CommunicationAddress2;
                                    //model.CommunicationAddress3 = transmodel.CommunicationAddress3;
                                    //model.CommunicationLandmark = transmodel.CommunicationLandmark;
                                    model.IsSameAsPermanentAddress = false;
                                }

                                model.StatusID = CrossCutting_Constant.StatusID_Pending_Payment;
                                model.PaymentID = CrossCutting_Constant.Payment_Online_ID;

                                if (model.PaymentID == CrossCutting_Constant.Payment_Online_ID)
                                {
                                    model.StatusID = CrossCutting_Constant.StatusID_Pending_Payment;
                                    IsPayNow = true;
                                }

                                decimal? CurrentPlanAmount = db.tblMstPlans.Where(w => w.PlanID == transmodel.PlanID).Select(s => s.Total).FirstOrDefault();                                                                

                                audit.UserID = model.UserID;
                                audit.StatusID = model.StatusID;
                                audit.Remark = model.Remark;
                                audit.ChequeNo = model.ChequeNo;
                                audit.ChequeDate = model.ChequeDate;
                                audit.ChequeAmount = model.ChequeAmount;
                                audit.IsValid = true;
                                audit.CreatedDateTime = CurrentDate;
                                audit.CertificateNo = model.CertificateNo;
                                model.tblTransactionAuditLogs.Add(audit);
                                db.tblTransactions.Add(model);
                                db.SaveChanges();

                                CertificateNo = model.CertificateNo;
                                PaymentRefNo = model.PaymentReferenceNo;

                                // Vpay Online Payment

                                TempData["Transactionid"] = model.TransactionID;
                                if (!string.IsNullOrEmpty(CertificateNo))
                                {
                                    TempData["CertificateNo"] = model.CertificateNo;
                                }
                                TempData["payLater"] = false;

                                if ((IsPayNow && model.PaymentID == CrossCutting_Constant.Payment_Online_ID))
                                {
                                    onlinePay = true;
                                    ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));
                                    var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.PaymentReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);

                                    OnlinePaymentModel onlinepaymentmodel = new OnlinePaymentModel();
                                    onlinepaymentmodel.IMDCode = userdata.IMDCode;
                                    onlinepaymentmodel.PayerType = CrossCutting_Constant.PayerType_Agent;
                                    onlinepaymentmodel.Amount = Convert.ToDecimal(CurrentPlanAmount);
                                    onlinepaymentmodel.PaymentReferenceNo = Convert.ToString(OutputOfPaymentReference.Value);
                                    onlinepaymentmodel.CreatedBy = userdata.Id;
                                    onlinepaymentmodel.TransactionIDs = new List<decimal> { model.TransactionID };
                                    onlinepaymentmodel.CustomerName = transmodel.CustomerName;
                                    onlinepaymentmodel.ContactNo = Convert.ToString(transmodel.CustomerContact);
                                    onlinepaymentmodel.Email = transmodel.CustomerEmail;

                                    vPayResponse = this.MakeVPayOnlinePayment(onlinepaymentmodel);
                                }

                                if (!string.IsNullOrEmpty(CertificateNo))
                                {
                                    Path = objHome.GetCertificatePath(model.TransactionID);
                                    TempData["CertificatePath"] = Path;
                                }

                                db.SaveChanges();

                                return Json(new { status = CrossCutting_Constant.Success, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path, IsPayNow = onlinePay, vPayResponse = vPayResponse }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { status = CrossCutting_Constant.Failure, certificateno = "", paymentrefno = "", path = "" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        private static bool ValidateInsuredDOBBelow55(DateTime DOB)
        {
            int MaxAgeYears = 0;
            int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["CovidIndemnityMaxAgeLimitInYears"]), out MaxAgeYears);

            DateTime dtToday = DateTime.Today;
            DateTime dtAddDOB = DOB.AddYears(MaxAgeYears);

            return (dtAddDOB >= dtToday);
        }

        #region ValidationForAgeBelow18Years
        public bool ValidationForAgeBelow18Years(string DOB)
        {
            bool IsDOBBelow18Years = false;

            try
            {
                DateTime? DateDOB = null;

                if (!string.IsNullOrEmpty(DOB))
                    DateDOB = DateTime.ParseExact(DOB, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                DateTime dtDOB = Convert.ToDateTime(DateDOB);
                DateTime now = DateTime.Now;

                int Adult18YearsAgeMonth = 0;
                int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["CovidIndemnityYearsAgeLimitInMonths"]), out Adult18YearsAgeMonth);

                int DOBMonth = ((now.Year - dtDOB.Year) * 12) + now.Month - dtDOB.Month;

                if (DOBMonth < Adult18YearsAgeMonth)
                {
                    IsDOBBelow18Years = true;
                }
                else
                {
                    IsDOBBelow18Years = false;
                }

                return IsDOBBelow18Years;
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }
        }
        #endregion

        #region ValidationForInsuredAgeBelow18Years
        public ActionResult ValidationForInsuredAgeBelow18Years(string InsDOB)
        {
            bool IsInsuredBelow18Years = false;

            try
            {
                DateTime? InsuredDOB = null;

                if (!string.IsNullOrEmpty(InsDOB))
                    InsuredDOB = DateTime.ParseExact(InsDOB, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                DateTime dtInsuredDOB = Convert.ToDateTime(InsuredDOB);
                DateTime now = DateTime.Now;

                int Adult18YearsAgeMonth = 0;
                int.TryParse(Convert.ToString(ConfigurationManager.AppSettings["CovidIndemnityYearsAgeLimitInMonths"]), out Adult18YearsAgeMonth);

                int InsuredMonth = ((now.Year - dtInsuredDOB.Year) * 12) + now.Month - dtInsuredDOB.Month;

                if (InsuredMonth < Adult18YearsAgeMonth)
                {
                    IsInsuredBelow18Years = true;
                    return Json(new { status = CrossCutting_Constant.Success, isinsuredbelow18years = IsInsuredBelow18Years }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IsInsuredBelow18Years = false;
                    return Json(new { status = CrossCutting_Constant.Success, isinsuredbelow18years = IsInsuredBelow18Years }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, isinsuredbelow18years = IsInsuredBelow18Years }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region IssueCertificateGAWebPortal        
        public async System.Threading.Tasks.Task<ActionResult> IssueCertificateGAWebPortal(string data)
        {            
            IssueCertificateGAWebPortal request = new IssueCertificateGAWebPortal();
            GlobalAssureEncryption enc = new GlobalAssureEncryption();

            //data = "";
            //string EncryptedMessage = enc.Encrypt(data);

            try
            {               
                if (string.IsNullOrEmpty(data))
                {
                    ViewBag.Error = "//Error:: Parameter Not Found";
                    return View("~/Views/WebPortal/Error.cshtml");
                }
                else
                {
                    string DecriptMessage = enc.Decrypt(data);
                    string[] SplitMessage = DecriptMessage.Split('&');
                    string[] para = new string[10];

                    for (int i = 0; i < SplitMessage.Length; i++)
                    {
                        string[] str = SplitMessage[i].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        para[i] = str[1];
                    }

                    request.UserID = para[0] == "" ? "" : para[0];
                    request.Name = para[1] == "" ? "" : para[1];
                    request.Email = para[2] == "" ? "" : para[2];
                    request.MobileNo = para[3] == "" ? "" : para[3];
                    request.RegistrationNo = para[4] == "" ? "" : para[4];
                    request.ProductId = para[5] == "" ? "" : para[5];
                    request.PlanId = para[6] == "" ? "" : para[6].Replace("\0", string.Empty);

                    if (string.IsNullOrEmpty(request.UserID))
                    {
                        ViewBag.Error = "//Error:: Parameter 'UserID' Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                    else if (string.IsNullOrEmpty(request.Name))
                    {
                        ViewBag.Error = "//Error:: Parameter 'Name' Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                    else if (string.IsNullOrEmpty(request.Email))
                    {
                        ViewBag.Error = "//Error:: Parameter 'Email' Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                    else if (string.IsNullOrEmpty(request.MobileNo))
                    {
                        ViewBag.Error = "//Error:: Parameter 'MobileNo' Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                    else if (string.IsNullOrEmpty(request.RegistrationNo))
                    {
                        ViewBag.Error = "//Error:: Parameter 'RegistrationNo' Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                    else if (string.IsNullOrEmpty(request.ProductId))
                    {
                        ViewBag.Error = "//Error:: Parameter 'ProductId' Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                    else if (string.IsNullOrEmpty(request.PlanId))
                    {
                        ViewBag.Error = "//Error:: Parameter 'PlanId' Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }

            return View(request);
        }
        #endregion

        #region SaveIssueCertificateGAWebPortal
        [HttpPost]
        public ActionResult SaveIssueCertificateGAWebPortal(IssueCertificateGAWebPortal data)
        {            
            bool IsPayNow = true;
            bool onlinePay = true;
            string CertificateNo = "";
            string PaymentRefNo = "";
            string Path = "";
         
            DateTime CurrentDate = DateTime.Now;
            tblTransaction model = new tblTransaction();
            tblTransactionAuditLog audit = new tblTransactionAuditLog();
            tblChequeAuditLog chequeaudit = new tblChequeAuditLog();
            vPayOnlinePaymentResponseModel vPayResponse = new vPayOnlinePaymentResponseModel();

            try
            {
                string Message = ValidateSaveIssueCertificateGAWebPortalData(data);

                if (string.IsNullOrEmpty(Message))
                {
                    string Username = data.UserID.Trim();
                    AspNetUser userdata = db.AspNetUsers.Where(w => w.Email == Username).FirstOrDefault();
                    if (userdata != null)
                    {
                        decimal PID = Convert.ToDecimal(data.ProductId);
                        var productname = db.tblMstProducts.AsNoTracking().Where(w => w.ProductID == PID).Select(s => s.ProductName).FirstOrDefault();

                        ObjectParameter Output = new ObjectParameter("UniqueCode", typeof(string));
                        var UniqueCodeNo = db.usp_UniqeNumberGeneration(CrossCutting_Constant.CertificatePrefix, "", "", "", "", "", Output);
                        model.CertificateNo = Convert.ToString(productname) + Convert.ToString(Output.Value);

                        model.Remark = "GA-WEBPORTAL";
                        model.IssueByID = CrossCutting_Constant.IssueBy_WebService_ID;
                        model.CreatedDate = CurrentDate;
                        model.IssueDate = CurrentDate;
                        model.IsValid = true;

                        model.UserID = userdata.Id;
                        model.ProductID = Convert.ToDecimal(data.ProductId);
                        model.PlanID = Convert.ToDecimal(data.PlanId);
                        model.CustomerName = data.Name;
                        model.CustomerContact = data.MobileNo;
                        model.CustomerEmail = data.Email;
                        model.RegistrationNo = data.RegistrationNo;

                        data.CoverStartDate = DateTime.Now.Date;                        
                        if (data.CoverStartDate != null)
                        {
                            decimal PrdID = Convert.ToDecimal(data.PlanId);
                            var PlanData = db.tblMstPlans.Where(w => w.PlanID == PrdID && w.IsValid != false).Select(s => new { s.PlanID, s.Name, s.Amount, s.GST, s.Total, s.ValidityYear }).FirstOrDefault();
                            if (PlanData != null)
                            {
                                int PlanValidityYear = Convert.ToInt32(PlanData.ValidityYear);

                                DateTime startDate = Convert.ToDateTime(data.CoverStartDate);                               
                                if (userdata.RiskStartCount > 0)
                                {
                                    int RiskStartCount = userdata.RiskStartCount;
                                    string RiskDate = Convert.ToString(startDate);
                                    DateTime RiskStartDate = Convert.ToDateTime(RiskDate);
                                    model.CoverStartDate = RiskStartDate.AddDays(RiskStartCount);
                                }
                                else
                                {
                                    model.CoverStartDate = startDate;
                                }
                                DateTime CovertStartdate = Convert.ToDateTime(model.CoverStartDate);
                                DateTime EndDate = CovertStartdate.AddYears(PlanValidityYear).AddDays(-1);
                                model.CoverEndDate = EndDate;
                            }
                        }

                        model.IsSameAsPermanentAddress = true;
                        //model.PermanentCityID = ;

                        model.StatusID = CrossCutting_Constant.StatusID_Pending_Payment;
                        model.PaymentID = CrossCutting_Constant.Payment_Online_ID;

                        if (model.PaymentID == CrossCutting_Constant.Payment_Online_ID)
                        {
                            model.StatusID = CrossCutting_Constant.StatusID_Pending_Payment;
                            IsPayNow = true;
                        }

                        decimal ProdID = Convert.ToDecimal(data.PlanId);
                        decimal? CurrentPlanAmount = db.tblMstPlans.Where(w => w.PlanID == ProdID).Select(s => s.Total).FirstOrDefault();

                        audit.UserID = model.UserID;
                        audit.StatusID = model.StatusID;
                        audit.Remark = model.Remark;
                        audit.IsValid = true;
                        audit.CreatedDateTime = CurrentDate;
                        audit.CertificateNo = model.CertificateNo;
                        model.tblTransactionAuditLogs.Add(audit);
                        db.tblTransactions.Add(model);
                        db.SaveChanges();

                        CertificateNo = model.CertificateNo;
                        PaymentRefNo = model.PaymentReferenceNo;

                        // Vpay Online Payment

                        TempData["Transactionid"] = model.TransactionID;
                        if (!string.IsNullOrEmpty(CertificateNo))
                        {
                            TempData["CertificateNo"] = model.CertificateNo;
                        }
                        TempData["payLater"] = false;

                        if ((IsPayNow && model.PaymentID == CrossCutting_Constant.Payment_Online_ID))
                        {
                            onlinePay = true;
                            ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));
                            var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.PaymentReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);

                            OnlinePaymentModel onlinepaymentmodel = new OnlinePaymentModel();
                            onlinepaymentmodel.IMDCode = userdata.IMDCode;
                            onlinepaymentmodel.PayerType = CrossCutting_Constant.PayerType_Agent;
                            onlinepaymentmodel.Amount = Convert.ToDecimal(CurrentPlanAmount);
                            onlinepaymentmodel.PaymentReferenceNo = Convert.ToString(OutputOfPaymentReference.Value);
                            onlinepaymentmodel.CreatedBy = userdata.Id;
                            onlinepaymentmodel.TransactionIDs = new List<decimal> { model.TransactionID };
                            onlinepaymentmodel.CustomerName = model.CustomerName;
                            onlinepaymentmodel.ContactNo = model.CustomerContact;
                            onlinepaymentmodel.Email = model.CustomerEmail;

                            vPayResponse = this.MakeVPayOnlinePayment(onlinepaymentmodel);
                        }

                        if (!string.IsNullOrEmpty(CertificateNo))
                        {
                            Path = objHome.GetCertificatePath(model.TransactionID);
                            TempData["CertificatePath"] = Path;
                        }

                        db.SaveChanges();

                        return Json(new { status = CrossCutting_Constant.Success, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path, IsPayNow = onlinePay, vPayResponse = vPayResponse }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = CrossCutting_Constant.Failure, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path, ErrMsg = "UserID Not Found" }, JsonRequestBehavior.AllowGet);
                    } 
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Failure, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path, ErrMsg = Message }, JsonRequestBehavior.AllowGet);
                }                
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, certificateno = CertificateNo, paymentrefno = PaymentRefNo, path = Path, ErrMsg = "Internal Server Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        internal string ValidateSaveIssueCertificateGAWebPortalData(IssueCertificateGAWebPortal data)
        {
            string Message = string.Empty;
            AspNetUser userdata = new AspNetUser();

            try
            {                
                // UserID Check
                if (string.IsNullOrEmpty(data.UserID))
                {
                    string ErrMsg = "UserID is Mandatory";
                    Message = Message + ErrMsg;
                }
                else
                {
                    string Username = data.UserID.Trim();

                    var UserInTableCheck = db.AspNetUsers.Where(w => w.Email == Username).FirstOrDefault();
                    if (UserInTableCheck != null)
                    {
                        decimal PID = Convert.ToDecimal(data.PlanId);
                        var PlanData = db.tblMstPlans.Where(w => w.PlanID == PID && w.IsValid != false).Select(s => new { s.PlanID, s.Name, s.Amount, s.GST, s.Total, s.ValidityYear }).FirstOrDefault();
                        if (PlanData != null)
                        {
                            data.PlanId = Convert.ToString(PlanData.PlanID);
                        }
                        else
                        {
                            string ErrMsg = "PlanId Not Found";
                            Message = Message + ErrMsg;
                        }
                    }
                    else
                    {
                        string ErrMsg = "UserID Not Found";
                        Message = Message + ErrMsg;
                    }
                }


                // CustomerName Check
                if (string.IsNullOrEmpty(data.Name))
                {
                    string ErrMsg = "Name is Mandatory";
                    Message = Message + ErrMsg;
                }
                else
                {
                    string FirstName = data.Name.Trim();
                    if (FirstName.Length > 50)
                    {
                        string ErrMsg = "Name MaxLength is 25";
                        Message = Message + ErrMsg;
                    }

                    var regex = new Regex("^[a-zA-Z ]*$");
                    if (regex.IsMatch(FirstName))
                    {

                    }
                    else
                    {
                        string ErrMsg = "Name Should be Alphabets Only";
                        Message = Message + ErrMsg;
                    }
                }


                // EmailId Check
                if (string.IsNullOrEmpty(data.Email))
                {
                    string ErrMsg = "Email is Mandatory";
                    Message = Message + ErrMsg;
                }
                else
                {
                    string EmailId = data.Email.Trim();
                    if (EmailId.Length > 50)
                    {
                        string ErrMsg = "Email MaxLength is 50";
                        Message = Message + ErrMsg;
                    }

                    var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    if (regex.IsMatch(EmailId))
                    {

                    }
                    else
                    {
                        string ErrMsg = "Email is Invalid";
                        Message = Message + ErrMsg;
                    }
                }


                // MobileNo Check
                if (string.IsNullOrEmpty(data.MobileNo))
                {
                    string ErrMsg = "MobileNo is Mandatory";
                    Message = Message + ErrMsg;
                }
                else
                {
                    string MobileNo = data.MobileNo.Trim();
                    if (MobileNo.Length != 10)
                    {
                        string ErrMsg = "MobileNo Should be 10 Digits Only";
                        Message = Message + ErrMsg;
                    }

                    var regex = new Regex("^[0-9]{10}$");
                    if (regex.IsMatch(MobileNo))
                    {

                    }
                    else
                    {
                        string ErrMsg = "MobileNo is Invalid";
                        Message = Message + ErrMsg;
                    }
                }


                // RegistrationNo Check
                if (string.IsNullOrEmpty(data.RegistrationNo))
                {
                    string ErrMsg = "RegistrationNo is Mandatory";
                    Message = Message + ErrMsg;
                }
                else
                {
                    string RegistrationNo = data.RegistrationNo.Trim();
                    if (RegistrationNo.Length < 5)
                    {
                        string ErrMsg = "RegistrationNo Minimum 5 digits Required";
                        Message = Message + ErrMsg;
                    }
                }


                // ProductId Validate
                if (string.IsNullOrEmpty(data.ProductId))
                {
                    string ErrMsg = "ProductId is Mandatory";
                    Message = Message + ErrMsg;
                }
                else
                {
                    string ProdID = data.ProductId.Trim();
                    var regex = new Regex("^[0-9]*$");
                    if (regex.IsMatch(ProdID))
                    {

                    }
                    else
                    {
                        string ErrMsg = "ProductId is Invalid";
                        Message = Message + ErrMsg;
                    }

                    decimal PrdID = Convert.ToDecimal(data.ProductId);
                    var ProdIDCheck = db.tblMstProducts.Where(w => w.ProductID == PrdID && w.IsValid != false).FirstOrDefault();
                    if (ProdIDCheck != null)
                    {
                        data.ProductId = Convert.ToString(ProdIDCheck.ProductID);
                    }
                    else
                    {
                        string ErrMsg = "Invalid ProductId";
                        Message = Message + ErrMsg;
                    }
                }


                // PlanId Validate
                if (string.IsNullOrEmpty(data.PlanId))
                {
                    string ErrMsg = "PlanId is Mandatory";
                    Message = Message + ErrMsg;
                }
                else
                {
                    string PlID = data.PlanId.Trim();
                    var regex = new Regex("^[0-9]*$");
                    if (regex.IsMatch(PlID))
                    {

                    }
                    else
                    {
                        string ErrMsg = "PlanId is Invalid";
                        Message = Message + ErrMsg;
                    }

                    decimal PID = Convert.ToDecimal(data.PlanId);
                    decimal ProdID = Convert.ToDecimal(data.ProductId);
                    int PlanIDCheck = db.tblMstPlans.Where(w => w.PlanID == PID && w.IsValid != false).Count();
                    if (PlanIDCheck > 0)
                    {
                        int PlanByProductIdCheck = db.tblMstPlans.Where(w => w.PlanID == PID && w.ProductID == ProdID && w.IsValid != false).Count();
                        if (PlanByProductIdCheck > 0)
                        {

                        }
                        else
                        {
                            string ErrMsg = "Invalid PlanId and ProductId Combination";
                            Message = Message + ErrMsg;
                        }
                    }
                    else
                    {
                        string ErrMsg = "Invalid PlanId";
                        Message = Message + ErrMsg;
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = "Internal Server Error";
                Message = Message + ErrMsg;
            }

            return Message;
        }
        #endregion

        #region PedalClaimIntimationGAWebPortal        
        public async System.Threading.Tasks.Task<ActionResult> PedalClaimIntimationGAWebPortal(string data)
        {
            PedalClaimIntimationGAWebPortal request = new PedalClaimIntimationGAWebPortal();
            GlobalAssureEncryption enc = new GlobalAssureEncryption();
            tblTransaction tblTransaction = new tblTransaction();

            //data = "";
            //string EncryptedMessage = enc.Encrypt(data);

            try
            {              
                request = GetPedalClaimIntimationGAWebPortal();

                if (string.IsNullOrEmpty(data))
                {
                    ViewBag.Error = "//Error:: Parameter Not Found";
                    return View("~/Views/WebPortal/Error.cshtml");
                }
                else
                {
                    string DecriptMessage = enc.Decrypt(data);
                    string[] SplitMessage = DecriptMessage.Split('&');
                    string[] para = new string[10];

                    for (int i = 0; i < SplitMessage.Length; i++)
                    {
                        string[] str = SplitMessage[i].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        para[i] = str[1];
                    }

                    request.CertificateNo = para[0] == "" ? "" : para[0];                   

                    if (string.IsNullOrEmpty(request.CertificateNo))
                    {
                        ViewBag.Error = "//Error:: Parameter 'CertificateNo' Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                    else
                    {
                        string CertificateNo = request.CertificateNo;
                        tblTransaction = db.tblTransactions.Where(w => w.IsValid != false && w.CertificateNo == CertificateNo && w.ProductID == 10).FirstOrDefault();
                        if (tblTransaction != null)
                        {                           
                            int StatusID = 0;
                            int.TryParse(Convert.ToString(db.tblTransactions.Where(w => w.IsValid != false && w.CertificateNo == CertificateNo && w.ProductID == 10).Select(s => s.StatusID).FirstOrDefault()), out StatusID);
                            
                            if (StatusID > 0)
                            {
                                if (StatusID != 2 && StatusID != 14)
                                {                                   
                                    decimal TransactionId = tblTransaction.TransactionID;

                                    request.TransactionId = TransactionId;
                                    request.CertificateNo = CertificateNo;
                                }
                                else
                                {
                                    ViewBag.Error = "//Error:: Certificate Status is not Eligible For Claim";
                                    return View("~/Views/WebPortal/Error.cshtml");
                                }
                            }
                            else
                            {
                                ViewBag.Error = "//Error:: Certificate Status Not Found";
                                return View("~/Views/WebPortal/Error.cshtml");
                            }
                        }
                        else
                        {
                            ViewBag.Error = "//Error:: Certificate Not Found";
                            return View("~/Views/WebPortal/Error.cshtml");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = e.Message;
                ViewBag.Error = "//Error:: " + ErrMsg + "";
                return View("~/Views/WebPortal/Error.cshtml");
            }
            
            return View(request);
        }

        public PedalClaimIntimationGAWebPortal GetPedalClaimIntimationGAWebPortal()
        {
            PedalClaimIntimationGAWebPortal model = new PedalClaimIntimationGAWebPortal();

            model.ClaimTypeListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "PedalClaimType").Select(s => new { s.CommonTypeID, s.Description }).OrderBy(o => o.Description)
                              .ToDictionary(d => d.CommonTypeID, d => d.Description);              

            return model;
        }
        #endregion

        #region SavePedalClaimIntimation
        [HttpPost]
        public ActionResult SavePedalClaimIntimation(PedalClaimIntimationGAWebPortal model)
        {
            tblPedalClaimIntimation tblPedalClaimIntimation = new tblPedalClaimIntimation();
            string FIRCopyPath = Convert.ToString(ConfigurationManager.AppSettings["FIRCopyPath"]);
            string CChequeCopyPath = Convert.ToString(ConfigurationManager.AppSettings["CChequeCopyPath"]);
            string FIRCopyTempPath = Convert.ToString(ConfigurationManager.AppSettings["FIRCopyTempPath"]);
            string CChequeCopyTempPath = Convert.ToString(ConfigurationManager.AppSettings["CChequeCopyTempPath"]);
            string UserName = Convert.ToString(ConfigurationManager.AppSettings["PedalClaimUserID"]);
            string PrefixClaimRefNo = "CRN";
            string ImgExt = ".jpg";
            string UserId = string.Empty;
            DateTime dtCurrentDateTime = DateTime.Now;
            string FIRCopyBase64String = string.Empty;
            string CChequeCopyBase64String = string.Empty;
            string ClaimReferenceNo = string.Empty;          

            try
            {
                HttpFileCollectionBase FileUpload = Request.Files;
                if (FileUpload != null)
                {
                    var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).FirstOrDefault();
                    if (userdata != null)
                    {
                        UserId = userdata.Id;

                        decimal TransactionId = model.TransactionId;
                        if (TransactionId > 0)
                        {
                            // ClaimRefNo Generation Start
                            string RandomRefNo = DateTime.Now.ToString("yyyyMMddHHmmss");
                            string ClaimRefNo = PrefixClaimRefNo + RandomRefNo;
                            // ClaimRefNo Generation End

                            string CurrentDateTime = dtCurrentDateTime.ToString("yyyyMMddHHmmss");

                            string FIRCopyCertiNo = "FIRCopy" + model.CertificateNo;
                            //string FIRImgName = FileUpload[0].FileName;
                            string FIRImgName = FIRCopyCertiNo + CurrentDateTime + ImgExt;
                            string FIRImgpath = FIRCopyPath;
                            FileUpload[0].SaveAs(FIRImgpath + FIRImgName);
                            string PathToFIRImgFile = FIRImgpath + FIRImgName;

                            FIRCopyTempPath += "\\" + FIRImgName;

                            //----------------------------------------------------//

                            string CChequeCopyCertiNo = "CChequeCopy" + model.CertificateNo;
                            //string CChequeImgName = FileUpload[1].FileName;
                            string CChequeImgName = CChequeCopyCertiNo + CurrentDateTime + ImgExt;
                            string CChequeImgpath = CChequeCopyPath;
                            FileUpload[1].SaveAs(CChequeImgpath + CChequeImgName);
                            string PathToCChequeImgFile = CChequeImgpath + CChequeImgName;

                            CChequeCopyTempPath += "\\" + CChequeImgName;

                            Base64Convertion obj = new Base64Convertion();
                            FIRCopyBase64String = obj.ConvertFileToBase64(FIRCopyTempPath);
                            if (string.IsNullOrEmpty(FIRCopyBase64String))
                            {
                                ViewBag.Error = "//Error:: File Convertion Failed";
                                return View("~/Views/WebPortal/Error.cshtml");
                            }

                            CChequeCopyBase64String = obj.ConvertFileToBase64(CChequeCopyTempPath);
                            if (string.IsNullOrEmpty(CChequeCopyBase64String))
                            {
                                ViewBag.Error = "//Error:: File Convertion Failed";
                                return View("~/Views/WebPortal/Error.cshtml");
                            }

                            tblPedalClaimIntimation.UserId = UserId;
                            tblPedalClaimIntimation.TransactionId = TransactionId;
                            tblPedalClaimIntimation.RefNo = ClaimRefNo;
                            tblPedalClaimIntimation.IsValid = true;
                            tblPedalClaimIntimation.CreatedDate = dtCurrentDateTime;
                            tblPedalClaimIntimation.ClaimType = model.hdnClaimType;
                            tblPedalClaimIntimation.ClaimIntimationDate = model.ClaimIntimationDate;
                            tblPedalClaimIntimation.ClaimDetails = model.ClaimDetails;
                            tblPedalClaimIntimation.FIRImage = FIRCopyBase64String;
                            tblPedalClaimIntimation.FIRImageName = FIRImgName;
                            tblPedalClaimIntimation.Name = model.Name;
                            tblPedalClaimIntimation.BankName = model.BankName;
                            tblPedalClaimIntimation.IfscCode = model.IFSCCode;
                            tblPedalClaimIntimation.BankBranch = model.BankBranch;
                            tblPedalClaimIntimation.AccountNumber = model.AccountNo;
                            tblPedalClaimIntimation.CancelledChequeCopy = CChequeCopyBase64String;

                            db.tblPedalClaimIntimation.Add(tblPedalClaimIntimation);
                            db.SaveChanges();

                            ClaimReferenceNo = tblPedalClaimIntimation.RefNo;
                            if (!string.IsNullOrEmpty(ClaimReferenceNo))
                            {
                                model.ClaimReferenceNo = ClaimReferenceNo;
                                model.Status = CrossCutting_Constant.Success;
                            }
                            else
                            {
                                ViewBag.Error = "//Error:: Claim Details Not Saved :: ClaimReferenceNo Not Found";
                                return View("~/Views/WebPortal/Error.cshtml");
                            }
                        }
                        else
                        {
                            ViewBag.Error = "//Error:: Certificate Data Not Found";
                            return View("~/Views/WebPortal/Error.cshtml");
                        }
                    }
                    else
                    {
                        ViewBag.Error = "//Error:: User Not Found";
                        return View("~/Views/WebPortal/Error.cshtml");
                    }
                }
                else
                {
                    ViewBag.Error = "//Error:: Uploaded Files Not Found";
                    return View("~/Views/WebPortal/Error.cshtml");
                }              
            }
            catch (Exception e)
            {
                HandleException(e);
                string Errmsg = e.Message;
                model.ErrorMsg = Errmsg;
                model.Status = CrossCutting_Constant.Failure;
            }

            return View(model);
        }
        #endregion

        #region MergePDF File Code
        public string GetDataToAPIForCOI(string DownloadURL, string RiskCovryPolicyNo)
        {
            string responseString = string.Empty;
            try
            {
                string tempPath = ConfigurationManager.AppSettings["TempPath"];
                if (!string.IsNullOrEmpty(RiskCovryPolicyNo))
                {
                    tempPath += "\\" + RiskCovryPolicyNo + ".pdf";
                }

                if (!string.IsNullOrEmpty(DownloadURL))
                {
                    WebClient myWebClient = new WebClient();
                    myWebClient.DownloadFile(DownloadURL, System.Web.HttpContext.Current.Server.MapPath(tempPath));
                    responseString = tempPath;
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return responseString;
        }

        private string MergePDF(string File1, string File2, string OutFileName)
        {
            string outputPdfPath = "";
            string Path = ConfigurationManager.AppSettings["TempPath"];
            try
            {
                // File 1 Start
                string[] GeneratepathFile1 = File1.Split('\\');
                string FinalPathFile1 = System.Web.HttpContext.Current.Server.MapPath(Path) + GeneratepathFile1[1];
                // File 1 End

                // File 2 Start
                string[] GeneratepathFile2 = File2.Split('\\');
                string FinalPathFile2 = System.Web.HttpContext.Current.Server.MapPath(Path) + GeneratepathFile2[1];
                // File 2 End

                string[] fileArray = new string[3];
                fileArray[0] = FinalPathFile1;
                fileArray[1] = FinalPathFile2;

                PdfReader reader = null;
                Document sourceDocument = null;
                PdfCopy pdfCopyProvider = null;
                PdfImportedPage importedPage;
                string OutputFileName = OutFileName + ".pdf";
                string OutputFilePath = Path;
                outputPdfPath = OutputFilePath + OutputFileName;

                sourceDocument = new Document();
                pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(System.Web.HttpContext.Current.Server.MapPath(outputPdfPath), System.IO.FileMode.Create));

                //output file Open  
                sourceDocument.Open();


                //files list wise Loop  
                for (int f = 0; f < fileArray.Length - 1; f++)
                {
                    int pages = TotalPageCount(fileArray[f]);

                    reader = new PdfReader(fileArray[f]);
                    //Add pages in new file  
                    for (int i = 1; i <= pages; i++)
                    {
                        importedPage = pdfCopyProvider.GetImportedPage(reader, i);
                        pdfCopyProvider.AddPage(importedPage);
                    }

                    reader.Close();
                }
                //save the output file  
                sourceDocument.Close();
            }
            catch (Exception ex)
            {
                HandleException(ex);

                outputPdfPath = "";
                return outputPdfPath;
            }

            return outputPdfPath;
        }

        private static int TotalPageCount(string file)
        {
            using (StreamReader sr = new StreamReader(System.IO.File.OpenRead(file)))
            {
                Regex regex = new Regex(@"/Type\s*/Page[^s]");
                MatchCollection matches = regex.Matches(sr.ReadToEnd());

                return matches.Count;
            }
        }
        #endregion

        #region Electric Bike
        #region EBikeOTPVerification        
        public async System.Threading.Tasks.Task<ActionResult> EBikeOTPVerification()
        {
            return View();
        }

        #region EBikeSendOTP and EBikeValidateOTP
        [HttpPost]
        public ActionResult EBikeSendOTP(string strMobileNo)
        {
            string strApplication = "WebPortalEBike";
            int RandomNo = 0;
            DateTime CurrDateTime = DateTime.Now;
            tblOTPProcess tblOTPProcess = new tblOTPProcess();

            try
            {                
                tblEBikeDealerDetails tblEBikeDealerDetails = db.tblEBikeDealerDetails.Where(w => w.MobileNo == strMobileNo && w.IsValid == true).FirstOrDefault();
                if (tblEBikeDealerDetails == null)
                {
                    string ErrorMsg = "This Mobile No not Registered with us Please enter Registered Mobile No or Contact Administrator.";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                }                

                string Application = strApplication;
                RandomNo = GenerateRandomNo();

                if (RandomNo > 0)
                {
                    string SendOTPUrl = ConfigurationManager.AppSettings["SendOTPUrl"];
                    string OTPusername = ConfigurationManager.AppSettings["OTPusername"];
                    string OTPpassword = ConfigurationManager.AppSettings["OTPpassword"];
                    string MobileNo = strMobileNo;
                    string OTPudh = "";
                    string OTPfrom = ConfigurationManager.AppSettings["OTPfrom"];

                    string OTPText = string.Format("Your OTP is : {0} .Please Don't share with any one", RandomNo.ToString());

                    string OTPdlrurl = ConfigurationManager.AppSettings["OTPdlr-url"];

                    string FullSendOtpUrl = SendOTPUrl + "username=" + OTPusername + "&password=" + OTPpassword + "&to=" + MobileNo + "&udh=" + OTPudh + "&from=" + OTPfrom
                        + "&text=" + HttpUtility.UrlEncode(OTPText) + "&dlr-url=" + OTPdlrurl;

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    HttpWebRequest myReq = (System.Net.HttpWebRequest)WebRequest.Create(FullSendOtpUrl);
                    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                    string responseString = respStreamReader.ReadToEnd();
                    respStreamReader.Close();
                    myResp.Close();

                    if (responseString.ToLower() == "sent.")
                    {
                        // Update OTP Status = '0' In Database Table Start 
                        List<tblOTPProcess> listOTPProcess = db.tblOTPProcess.AsNoTracking().Where(w => w.Status == true && w.Application == Application && w.MobileNo == MobileNo).ToList();
                        if (listOTPProcess.Any())
                        {
                            listOTPProcess.ForEach(x => 
                            {
                                int UniqueID = x.UniqueID;

                                tblOTPProcess UpdatetblOTPProcess = db.tblOTPProcess.Where(w => w.UniqueID == UniqueID).FirstOrDefault();
                                if (UpdatetblOTPProcess != null)
                                {
                                    UpdatetblOTPProcess.Status = false;
                                    db.SaveChanges();
                                }                               
                            });
                        }
                        // Update OTP Status = '0' In Database Table End 

                        // Saving OTP In Database Table Start 
                        tblOTPProcess.Status = true;
                        tblOTPProcess.Application = Application;
                        tblOTPProcess.MobileNo = MobileNo;
                        tblOTPProcess.OTP = RandomNo;
                        tblOTPProcess.InsertDate = CurrDateTime;
                        db.tblOTPProcess.Add(tblOTPProcess);
                        db.SaveChanges();
                        // Saving OTP In Database Table End

                        return Json(new { status = CrossCutting_Constant.Success }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMsg = "OTP Sent Failed, Please Try Again..";
                        return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrorMsg = "OTP Generation Failure, Please Try Again..";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                }               
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrorMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EBikeValidateOTP(string strMobileNo, string strOTP)
        {
            string strApplication = "WebPortalEBike";
            DateTime CurrDateTime = DateTime.Now;
            tblOTPProcess tblOTPProcess = new tblOTPProcess();

            try
            {
                string Application = strApplication;
                string MobileNo = strMobileNo;
                int OTP = 0;
                int.TryParse(strOTP, out OTP);

                if (OTP > 0)
                {
                    tblOTPProcess = db.tblOTPProcess.Where(w => w.Status == true && w.Application == Application && w.MobileNo == MobileNo && w.OTP == OTP).FirstOrDefault();
                    if (tblOTPProcess != null)
                    {
                        // Modified OTP Status as 0 In Database Table Start 
                        tblOTPProcess.Status = false;
                        tblOTPProcess.ModifyDate = CurrDateTime;
                        db.SaveChanges();
                        // Modified OTP Status as 0 In Database Table End

                        return Json(new { status = CrossCutting_Constant.Success }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string ErrorMsg = "OTP Mismatched, Please Enter Valid OTP or Send OTP Again..";
                        return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                    }                     
                }
                else
                {
                    string ErrorMsg = "Invalid OTP, Please Send OTP Again..";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrorMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        #region EBikeDataUpload
        public ActionResult EBikeDataUpload()
        {
            EDataUploadModel model = new EDataUploadModel();

            Dictionary<int, string> dicVehicleTypeListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "EBikeVehicleType")
                                                             .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            for (int iLoop = 0; iLoop < 10; iLoop++)
            {
                model.EDataUploadModelList.Add(new EDataUploadListModel
                {
                    FrameNo = "",
                    BatteryNo = "",
                    VehicleTypeListItem = dicVehicleTypeListItem,
                    Make = "",
                    Model = "",
                    Plan = "",
                    Title = "",
                    InsuredFirstName = "",
                    InsuredLastName = "",
                    InsuredMobileNo = "",
                    InsuredEmailID = "",
                    InsuredAddress = "",
                    InsuredState = "",
                    InsuredCity = "",
                    Color = ""
                });
            }

            return View(model);
        }
        #endregion

        #region SaveEBikeData
        [HttpPost]
        public ActionResult SaveEBikeData(EDataUploadModel model)
        {           
            DateTime CurrDateTime = DateTime.Now;            

            try
            {
                if (model == null)
                {
                    string ErrorMsg = "DataModel is NULL :: An Error Occurred, Please Submit Again.";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                }

                List<EDataUploadListModel> listEDataUploadModel = model.EDataUploadModelList.Where(w => w.IsChecked == true).ToList();

                if (listEDataUploadModel.Any())
                {
                    listEDataUploadModel.ForEach(x => 
                    {
                        tblElectricBikeDetails obj = new tblElectricBikeDetails();

                        int intVehicleTypeID = 0;
                        int.TryParse(Convert.ToString(x.VehicleTypeID), out intVehicleTypeID);

                        string strVehicleType = db.tblMstCommonTypes.AsNoTracking().Where(w => w.CommonTypeID == intVehicleTypeID).Select(s => s.Description).FirstOrDefault();

                        obj.FrameNo = x.FrameNo;
                        obj.BatteryNo = x.BatteryNo;
                        obj.VehicleType = (!string.IsNullOrEmpty(strVehicleType)) ? strVehicleType : null;
                        obj.Make = x.Make;
                        obj.Model = x.Model;
                        obj.PlanName = x.Plan;
                        obj.Title = x.Title;
                        obj.InsuredFirstName = x.InsuredFirstName;
                        obj.InsuredLastName = x.InsuredLastName;
                        obj.InsuredMobileNo = x.InsuredMobileNo;
                        obj.InsuredEmailID = x.InsuredEmailID;
                        obj.InsuredAddress = x.InsuredAddress;
                        obj.InsuredState = x.InsuredState;
                        obj.InsuredCity = x.InsuredCity;
                        obj.Color = x.Color;
                        obj.PolicyStatus = true;
                        obj.dt_Insert_Date = CurrDateTime;

                        db.tblElectricBikeDetails.Add(obj);
                        db.SaveChanges();
                    });

                    return Json(new { status = CrossCutting_Constant.Success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrorMsg = "DataList is NULL :: An Error Occurred, Please Submit Again.";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrorMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        #region GenerateRandomNoForOTP
        public int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();

            return _rdm.Next(_min, _max);
        }
        #endregion
    }

    public class Base64Convertion
    {
        #region ConvertFileToBase64
        public string ConvertFileToBase64(string Path)
        {
            string FilePath = System.Web.HttpContext.Current.Server.MapPath(Path);
            Byte[] bytes = File.ReadAllBytes(FilePath);
            string strBase64 = Convert.ToBase64String(bytes);

            return strBase64;
        }
        #endregion

        #region ConvertBase64ToFile
        public string ConvertBase64ToFile(string Base64Str, string Path)
        {
            string FilePath = System.Web.HttpContext.Current.Server.MapPath(Path);
            Byte[] bytes = Convert.FromBase64String(Base64Str);
            File.WriteAllBytes(FilePath, bytes);

            return FilePath;
        }
        #endregion
    }
}
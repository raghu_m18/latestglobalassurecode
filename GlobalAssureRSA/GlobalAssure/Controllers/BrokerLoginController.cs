﻿using GlobalAssure.Entity;
using GlobalAssure.Models;
using iNube.ClaimsLive.CrossCutting.Security;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GlobalAssure.Controllers
{
    public class BrokerLoginController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();


        public BrokerLoginController()
        {
        }

        public BrokerLoginController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        // GET: BrokerLogin
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Login(string queryString, bool isTestingStub = false)
        {
            string UserName = string.Empty;
            string Id = string.Empty;
            bool IsLocked = false;
            bool IsFunctionalityLocked = false;
            bool IsAdmin = false;
            bool IsBulkCertificationMenu = false;
            bool IsTVSTieUp = false;
            string Password = string.Empty;
            string LogoPath = string.Empty;

            string ResponseCode = string.Empty;
            string ResponseMessage = string.Empty;
            string ResponsePageURL = string.Empty;
            bool isUserValid = true;
            bool isApplicationIDValid = true;

            try
            {
                BrokerURLModel _BrokerURLModel = JsonConvert.DeserializeObject<BrokerURLModel>(EncryptDecrypt.DecryptText(queryString, ConfigurationManager.AppSettings["EncryptDecryptPassword"]));

                if (!string.IsNullOrEmpty(_BrokerURLModel.ApplicationID))
                {
                    if (_BrokerURLModel.ApplicationID != ConfigurationManager.AppSettings["BrokerApplicationID"])
                    {
                        isApplicationIDValid = false;
                        ResponseCode = "-1";
                        ResponseMessage = "ApplicationID is incorrect. please enter valid ApplicationID or contact <a>support@globalassure.com</a>";
                    }
                }
                else
                {
                    isApplicationIDValid = false;
                    ResponseCode = "-1";
                    ResponseMessage = "ApplicationID is mandatory. please enter valid ApplicationID.";
                }

                if (isApplicationIDValid)
                {
                    if (!string.IsNullOrEmpty(_BrokerURLModel.UserName))
                    {
                        var userData = (from u in db.AspNetUsers
                                        join ph in db.tblPasswordHistories on u.Id equals ph.UserId
                                        where u.UserName == _BrokerURLModel.UserName && u.IsAdmin!=true
                                        select new { u.Id, u.UserName, u.IsAdmin, u.IsLocked, u.IsFunctionalityLocked, ph.Date, ph.Password,
                                                     u.LogoPath, u.IsBulkCertificationMenu,u.TieUpCompanyID }).OrderByDescending(o => o.Date).FirstOrDefault();

                        if (userData == null)
                        {
                            isUserValid = false;
                            ResponseCode = "-1";
                            ResponseMessage = "UserName '" + _BrokerURLModel.UserName + "' is not available in the system. please contact <a>support@globalassure.com</a>";
                        }
                        else
                        {
                            UserName = userData.UserName;
                            Id = userData.Id;
                            IsAdmin = Convert.ToBoolean(userData.IsAdmin);
                            if(userData.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID)
                            {
                                IsTVSTieUp = true;
                            }
                            IsFunctionalityLocked = Convert.ToBoolean(userData.IsFunctionalityLocked);
                            IsLocked = Convert.ToBoolean(userData.IsLocked);
                            Password = userData.Password;
                            LogoPath = userData.LogoPath;
                            IsBulkCertificationMenu = Convert.ToBoolean(userData.IsBulkCertificationMenu);

                            if (IsFunctionalityLocked)
                            {
                                isUserValid = false;
                                ResponseCode = "-1";
                                ResponseMessage = "Your account has been locked due to pending payments. Please clear your payments or contact <a>support@globalassure.com</a>";
                            }

                        }
                    }
                    else
                    {
                        isUserValid = false;
                        ResponseCode = "-1";
                        ResponseMessage = "UserName is mandatory. please enter valid UserName.";
                    }


                    if (isUserValid)
                    {
                        if (IsLocked)
                        {
                            return View("Lockout");
                        }

                        var result = await SignInManager.PasswordSignInAsync(UserName, Password, false, shouldLockout: false);
                        switch (result)
                        {
                            case SignInStatus.Success:
                                string LogoFilePathAppSetting = ConfigurationManager.AppSettings["TempLOGO"];
                                string CookieName = "UserType";
                                string FunctionalityLockCookieName = "FunctionalityLock";
                                string LogoPathCookie = "LogoPathCookie";
                                string BulkCertificationMenuCookieName = "BulkCertificationMenu";
                                string IsTVSTieUpCookie = "IsTVSTieUp";
                                bool IsAdminUser = IsAdmin;
                                HttpCookie myCookie = Request.Cookies[CookieName] ?? new HttpCookie(CookieName);
                                HttpCookie myFunctionalityLockCookie = Request.Cookies[FunctionalityLockCookieName] ?? new HttpCookie(FunctionalityLockCookieName);
                                HttpCookie myLogoPathCookie = Request.Cookies[LogoPathCookie] ?? new HttpCookie(LogoPathCookie);
                                HttpCookie myBulkCertificationMenuCookie = Request.Cookies[BulkCertificationMenuCookieName] ?? new HttpCookie(BulkCertificationMenuCookieName);
                                HttpCookie myIsTVSTieUpCookie = Request.Cookies[IsTVSTieUpCookie] ?? new HttpCookie(IsTVSTieUpCookie);

                                myFunctionalityLockCookie.Values["IsFunctionalityLocked"] = IsFunctionalityLocked == true ? "True" : "False";

                                myLogoPathCookie.Values["LogoPath"] = "";
                                if (!string.IsNullOrEmpty(LogoPath))
                                {
                                    string FileName = LogoPath.Substring(LogoPath.LastIndexOf('\\') + 1);
                                    string FilePath = LogoFilePathAppSetting + "/" + FileName;
                                    FilePath = FilePath.Replace("\\", "/");
                                    FilePath = FilePath.Replace("~", "");
                                    myLogoPathCookie.Values["LogoPath"] = FilePath;
                                }

                                if (IsAdminUser)
                                {
                                    myCookie.Values["User"] = "Admin";
                                }
                                else
                                {
                                    myCookie.Values["User"] = "Normal";
                                }

                                if (IsTVSTieUp)
                                {
                                    myIsTVSTieUpCookie.Values["IsTVSTieUp"] = "True";
                                }
                                else
                                {
                                    myIsTVSTieUpCookie.Values["IsTVSTieUp"] = "False";
                                }


                                myBulkCertificationMenuCookie.Values["IsBulkCertificationMenu"] = "False";

                                if (IsBulkCertificationMenu)
                                {
                                    myBulkCertificationMenuCookie.Values["IsBulkCertificationMenu"] = "True";
                                }


                                myCookie.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(myCookie);

                                myFunctionalityLockCookie.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(myFunctionalityLockCookie);

                                myLogoPathCookie.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(myLogoPathCookie);

                                myBulkCertificationMenuCookie.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(myBulkCertificationMenuCookie);

                                myIsTVSTieUpCookie.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(myIsTVSTieUpCookie);

                                List<string> MakeforUser = new List<string>();
                                List<string> ModelforUser = new List<string>();
                                List<string> PlanforUser = new List<string>();

                                TransactionModel model = new GlobalAssure.Controllers.HomeController().GetIssueCertificate(UserName);
                                model.IssueByID = CrossCutting_Constant.IssueBy_BrokerPortal_ID;

                                var UserPrivilegeDetails = (from u in db.AspNetUsers
                                                            join up in db.tblUserPrivilegeDetails on u.Id equals up.UserID
                                                            where u.UserName == UserName
                                                            select new { up.Make, up.Model, up.Plans }).ToList();

                                if (UserPrivilegeDetails.Any())
                                {
                                    MakeforUser = UserPrivilegeDetails.Where(w => w.Make != null && w.Make != "").Select(s => s.Make.ToLower()).Distinct().ToList();
                                    ModelforUser = UserPrivilegeDetails.Where(w => w.Model != null && w.Model != "").Select(s => s.Model.ToLower()).Distinct().ToList();
                                    PlanforUser = UserPrivilegeDetails.Where(w => w.Plans != null && w.Plans != "").Select(s => s.Plans.ToLower()).Distinct().ToList();
                                }

                                model.CustomerName = _BrokerURLModel.FirstName;
                                model.CustomerLastName = _BrokerURLModel.LastName;
                                model.CustomerMiddleName = _BrokerURLModel.LastName;
                                model.CustomerContact = _BrokerURLModel.Mobile;
                                model.ChassisNo = _BrokerURLModel.ChassisNo;
                                model.EngineNo = _BrokerURLModel.EngineNo;
                                model.Product = _BrokerURLModel.Product;
                                model.Make = _BrokerURLModel.Make;
                                model.Model = _BrokerURLModel.Model;
                                model.VehicleType = _BrokerURLModel.VehicleType;
                                model.State = _BrokerURLModel.State;
                                model.City = _BrokerURLModel.City;
                                model.CertStartDate = _BrokerURLModel.CertStartDate;
                                model.CertEndDate = _BrokerURLModel.CertEndDate;


                                ///// Set Values Coming from Broker URL /////

                                if (!string.IsNullOrEmpty(model.CertStartDate))
                                {
                                    model._CoverStartDate = Convert.ToDateTime(model.CertStartDate);
                                    model._CoverEndDate = Convert.ToDateTime(model._CoverStartDate).AddYears(1);
                                }


                                if (!string.IsNullOrEmpty(model.Product))
                                {
                                    if (model.ProductListItem.Any())
                                    {
                                        model.ProductID = model.ProductListItem.Where(w => w.Value == model.Product.Trim()).Select(s => s.Key).FirstOrDefault();
                                    }


                                    if (model.ProductID != 0)
                                    {
                                        if (MakeforUser.Any())
                                        {
                                            int makeForUserhaveALL = MakeforUser.Where(w => w.ToLower() == "all").Count();
                                            if (makeForUserhaveALL > 0)
                                            {
                                                model.MakeListItem = db.tblMstMakes.AsNoTracking().Where(w => w.IsValid != false && w.ProductID == model.ProductID)
                                                              .Select(s => new { s.MakeID, s.Name }).OrderBy(o => o.Name).ToDictionary(d => d.MakeID, d => d.Name);
                                            }
                                            else
                                            {
                                                model.MakeListItem = db.tblMstMakes.AsNoTracking().Where(w => w.IsValid != false && w.ProductID == model.ProductID && MakeforUser.Contains(w.Name.ToLower()))
                                                             .Select(s => new { s.MakeID, s.Name }).OrderBy(o => o.Name).ToDictionary(d => d.MakeID, d => d.Name);
                                            }

                                            if (model.MakeListItem.Any())
                                            {
                                                model.MakeID = model.MakeListItem.Where(w => w.Value == model.Make.Trim()).Select(s => s.Key).FirstOrDefault();
                                            }

                                            if (model.MakeID != 0)
                                            {
                                                if (ModelforUser.Any())
                                                {
                                                    int modelForUserhaveALL = ModelforUser.Where(w => w.ToLower() == "all").Count();
                                                    if (modelForUserhaveALL > 0)
                                                    {
                                                        model.ModelListItem = db.tblMstModels.AsNoTracking().Where(w => w.IsValid != false && w.MakeID == model.MakeID)
                                                                      .Select(s => new { s.ModelID, s.Name, s.Variant }).OrderBy(o => o.Name)
                                                                      .ToDictionary(d => d.ModelID, d => d.Name + " - " + d.Variant);
                                                    }
                                                    else
                                                    {
                                                        model.ModelListItem = db.tblMstModels.AsNoTracking().Where(w => w.IsValid != false && w.MakeID == model.MakeID && ModelforUser.Contains(w.Name.ToLower()))
                                                                     .Select(s => new { s.ModelID, s.Name, s.Variant }).OrderBy(o => o.Name)
                                                                     .ToDictionary(d => d.ModelID, d => d.Name + " - " + d.Variant);
                                                    }

                                                    if (model.ModelListItem.Any())
                                                    {
                                                        model.ModelID = db.tblMstModels.Where(w => w.Name == model.Model.Trim()).Select(s => s.ModelID).FirstOrDefault();
                                                    }
                                                }
                                            }

                                        }


                                        if (PlanforUser.Any())
                                        {
                                            int planForUserhaveALL = PlanforUser.Where(w => w.ToLower() == "all").Count();
                                            if (planForUserhaveALL > 0)
                                            {
                                                model.PlanListItem = db.tblMstPlans.AsNoTracking().Where(w => w.IsValid != false && w.ProductID == model.ProductID)
                                                              .Select(s => new { s.PlanID, s.Name }).OrderBy(o => o.Name).ToDictionary(d => d.PlanID, d => d.Name);
                                            }
                                            else
                                            {
                                                model.PlanListItem = db.tblMstPlans.AsNoTracking().Where(w => w.IsValid != false && w.ProductID == model.ProductID && PlanforUser.Contains(w.Name.ToLower()))
                                                             .Select(s => new { s.PlanID, s.Name }).OrderBy(o => o.Name).ToDictionary(d => d.PlanID, d => d.Name);
                                            }
                                        }

                                    }

                                }


                                if (!string.IsNullOrEmpty(model.VehicleType))
                                {
                                    model.TransactionTypeID = model.TransactionTypeListItem.Where(w => w.Value == model.VehicleType.Trim()).Select(s => s.Key).FirstOrDefault();
                                }

                                if (!string.IsNullOrEmpty(model.State))
                                {
                                    var StateID = model.PermanentStateListItem.Where(w => w.Value == model.State.Trim()).Select(s => s.Key).FirstOrDefault();
                                    if (StateID != 0)
                                    {
                                        model.PermanentStateID = Convert.ToInt32(StateID);
                                    }

                                    if (model.PermanentStateID != null && model.PermanentStateID != 0)
                                    {
                                        model.PermanentCityListItem = db.tblMstCities.AsNoTracking().Where(w => w.IsValid != false && w.StateID == model.PermanentStateID)
                                                                        .Select(s => new { s.CityID, s.City }).ToDictionary(d => d.CityID, d => d.City);

                                        var CityID = model.PermanentCityListItem.Where(w => w.Value == model.City.Trim()).Select(s => s.Key).FirstOrDefault();
                                        if (CityID != 0)
                                        {
                                            model.PermanentCityID = Convert.ToInt32(CityID);
                                        }
                                    }

                                }

                                ///////////////////////////////////////////////
                                ViewBag.IsBrokerURL = true;
                                ViewBag.UserName = UserName;
                                return View("~/Views/Home/IssueCertificate.cshtml", model);



                            default:
                                ResponseCode = "-1";
                                ResponseMessage = "Invalid login attempt.";

                                ViewBag.ResponseCode = ResponseCode;
                                ViewBag.ResponseMessage = ResponseMessage;
                                ViewBag.ResponsePageURL = ResponsePageURL;

                                return View();
                        }

                    }
                }



            }
            catch (Exception ex)
            {
                ResponseCode = "-1";
                ResponseMessage = "something went wrong. please try again.";
                HandleException(ex);
            }


            ViewBag.ResponseCode = ResponseCode;
            ViewBag.ResponseMessage = ResponseMessage;
            ViewBag.ResponsePageURL = ResponsePageURL;

            return View();

        }

        //[HttpPost]
        public ActionResult GetEncrypt(string UserName, string ApplicationID, string Product, string CertStartDate, string CertEndDate, string VehicleType, string Make,
            string Model, string EngineNo, string ChassisNo, string FirstName, string LastName, string Mobile, string State, string City)
        {
            string ResponseCode = string.Empty;
            string ResponseMessage = string.Empty;
            string ResponsePageURL = string.Empty;

            BrokerURLModel model = new BrokerURLModel();
            model.UserName = UserName;
            model.ApplicationID = ApplicationID;
            model.Product = Product;
            model.CertStartDate = CertStartDate;
            model.CertEndDate = CertEndDate;
            model.VehicleType = VehicleType;
            model.Make = Make;
            model.Model = Model;
            model.EngineNo = EngineNo;
            model.ChassisNo = ChassisNo;
            model.FirstName = FirstName;
            model.LastName = LastName;
            model.Mobile = Mobile;
            model.State = State;
            model.City = City;


            try
            {
                string encPassword = ConfigurationManager.AppSettings["EncryptDecryptPassword"];
                string applicationURL = ConfigurationManager.AppSettings["ApplicationURL"];
                string _brokerURLobjStr = JsonConvert.SerializeObject(model);
                string encryptedbrokerURLString = HttpContext.Server.UrlEncode(EncryptDecrypt.EncryptText(_brokerURLobjStr, encPassword));

                ResponsePageURL = applicationURL + "BrokerLogin/Login?queryString=" + encryptedbrokerURLString + "&isTestingStub=True";
                ResponseMessage = "Successfully URL got generated.";
                ResponseCode = "0";
            }
            catch (Exception ex)
            {
                ResponseCode = "-1";
                ResponseMessage = "Please try again.";
                HandleException(ex);
            }


            var jsonResult = Json(new
            {
                ResponseCode = ResponseCode,
                ResponseMessage = ResponseMessage,
                ResponsePageURL = ResponsePageURL
            }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        public static bool HandleException(Exception ex)
        {
            bool rethrow = false;
            rethrow = UIExceptionHandler.HandleException(ref ex);
            if (rethrow)
            {
                throw ex;
            }
            return false;
        }

    }


    public class BrokerURLModel
    {
        public string UserName { get; set; }
        public string ApplicationID { get; set; }
        public string Product { get; set; }
        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Mobile { get; set; }
        public string State { get; set; }
        public string City { get; set; }
    }
}
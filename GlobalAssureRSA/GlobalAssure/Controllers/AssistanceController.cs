﻿using CsvHelper;
using GlobalAssure.Entity;
using GlobalAssure.Models;
using GlobalAssure.RelianceRSA;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Device.Location;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.Xml;

namespace GlobalAssure.Controllers
{
    /// <summary>
    /// IndexId     Date            Developer        Desc
    /// 1001        26/11/2019      Bikash           Reliance Service URL Change   
    /// 1002        04/12/2019      Arshad           Added RILAssit Controller
    /// 1003        10/12/2019      Hassan           Changes in Assistance Data Entry Page
    /// 1004        07/01/2020      Arshad           Added Travel Controller
    /// ---------------------------- New Changes -------------------------------------------
    /// 1005        07/03/2020      Hassan           Search Assistance Data by CertificateNo
    /// 1006        12/03/2020      Arshad           Added MIS Controller and View ,  Added Assist Summary in view and binding code in Controller
    /// 1007        16/07/2020      Arshad           Added new Method to show the Assitdetails on New Page and also added method for 3 letter autofill
    ///                                             
    /// </summary>
    public class AssistanceController : Controller
    {

        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();
        ILog logger = LogManager.GetLogger("OnlinePayment");
        Communication _c = new Communication();
        GlobalAssureEncryption enc = new GlobalAssureEncryption();

        // GET: Assist
        public ActionResult Index()
        {
            return View();
        }

        //sa 1004
        public ActionResult Travel()
        {
            TravelModel TravelModel = new TravelModel();
            try
            {
                string UserName = Convert.ToString(User.Identity.Name);
                TravelModel = GetAllListItems();
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }

            return View(TravelModel);
        }

        public TravelModel GetAllListItems()
        {
            TravelModel TravelModel = new TravelModel();

            TravelModel.CountryVisitingListItem = db.tblCountryMasterTravelREL.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.CountryId, s.CountryName })
                              .ToDictionary(d => d.CountryId, d => d.CountryName);
            TravelModel.StateListItem = db.tblStateMasterREL.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateId, s.StateName })
                          .ToDictionary(d => d.StateId, d => d.StateName);
            TravelModel.PlanListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.MasterType == CrossCutting_Constant.RILTravelPlan && w.IsValid != false).Select(s => new { s.CommonTypeID, s.Description })
                          .ToDictionary(d => d.CommonTypeID, d => d.Description);
            TravelModel.NomRelListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.MasterType == CrossCutting_Constant.NomineeRelationship && w.IsValid != false).Select(s => new { s.CommonTypeID, s.Description })
                          .ToDictionary(d => d.CommonTypeID, d => d.Description);
            TravelModel.AgeGroupListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.MasterType == CrossCutting_Constant.RILTravelAgeGroup && w.IsValid != false).Select(s => new { s.CommonTypeID, s.Description })
                          .ToDictionary(d => d.CommonTypeID, d => d.Description);
            TravelModel.GenderListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.MasterType == CrossCutting_Constant.RILTravelGender && w.IsValid != false).Select(s => new { s.CommonTypeID, s.Description })
                          .ToDictionary(d => d.CommonTypeID, d => d.Description);

            return TravelModel;
        }

        //se 1004
        public ActionResult AssistDataEntry()
        {
            AssistanceDataEntryModel AsstDataEntrymodel = new AssistanceDataEntryModel();
            try
            {
                string UserName = Convert.ToString(User.Identity.Name);
                AsstDataEntrymodel = GetProducts(UserName);
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }

            return View(AsstDataEntrymodel);
        }

        public AssistanceDataEntryModel GetProducts(string UserName)
        {
            int CompanyId = 0;
            string[] ProductsList = ConfigurationManager.AppSettings["AssistanceDataEntryProducts"].ToLower().Split(',');

            AssistanceDataEntryModel AsstDataEntrymodel = new AssistanceDataEntryModel();

            var userdata = db.AspNetUsers.AsNoTracking().Where(w => w.Email == UserName).FirstOrDefault();
            if (userdata != null)
            {
                if (userdata.CompanyID == null)
                {
                    CompanyId = 0;        
                }
                else
                {
                    CompanyId = Convert.ToInt32(userdata.CompanyID);
                }
            }

            AsstDataEntrymodel.PermanentStateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                              .ToDictionary(d => d.StateID, d => d.State);

            //AsstDataEntrymodel.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false)
            //              .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);

            AsstDataEntrymodel.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false && ProductsList.Contains(w.ProductName.ToLower()))
                          .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);            

            if (CompanyId > 0)
            {
                AsstDataEntrymodel.CompanyListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.Company_Name && w.CommonTypeID == CompanyId)
                                             .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

                AsstDataEntrymodel.CompID = CompanyId;
            }
            else
            {
                AsstDataEntrymodel.CompanyListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.Company_Name)
                                             .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

                AsstDataEntrymodel.CompID = CompanyId;
            }          

            return AsstDataEntrymodel;
        }

        public ActionResult AssistDataEntryWeb()
        {
            AssistanceDataEntryModel AsstDataEntrymodel = new AssistanceDataEntryModel();
            try
            {
                string UserName = Convert.ToString(User.Identity.Name);
                AsstDataEntrymodel = GetProductsWeb(UserName);
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }

            return View(AsstDataEntrymodel);
        }

        public AssistanceDataEntryModel GetProductsWeb(string UserName)
        {
            int CompanyId = 0;
            string[] ProductsList = ConfigurationManager.AppSettings["AssistanceDataEntryProducts"].ToLower().Split(',');

            AssistanceDataEntryModel AsstDataEntrymodel = new AssistanceDataEntryModel();

            var userdata = db.AspNetUsers.AsNoTracking().Where(w => w.Email == UserName).FirstOrDefault();
            if (userdata != null)
            {
                if (userdata.CompanyID == null)
                {
                    CompanyId = 0;
                }
                else
                {
                    CompanyId = Convert.ToInt32(userdata.CompanyID);
                }
            }

            AsstDataEntrymodel.PermanentStateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                              .ToDictionary(d => d.StateID, d => d.State);

            //AsstDataEntrymodel.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false)
            //              .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);

            AsstDataEntrymodel.ProductListItem = db.tblMstProducts.AsNoTracking().Where(w => w.IsValid != false && ProductsList.Contains(w.ProductName.ToLower()))
                          .Select(s => new { s.ProductID, s.ProductName }).ToDictionary(d => d.ProductID, d => d.ProductName);

            if (CompanyId > 0)
            {
                AsstDataEntrymodel.CompanyListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.Company_Name && w.CommonTypeID == CompanyId)
                                             .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

                AsstDataEntrymodel.CompID = CompanyId;
            }
            else
            {
                AsstDataEntrymodel.CompanyListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.Company_Name)
                                             .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

                AsstDataEntrymodel.CompID = CompanyId;
            }

            AsstDataEntrymodel.OEMListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "OEMName")
                                             .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            return AsstDataEntrymodel;
        }

        public ActionResult AssistDataEntryNew()
        {
            AssistanceDataEntryNewModel AsstDataEntryNewmodel = new AssistanceDataEntryNewModel();
            try
            {
                string UserName = Convert.ToString(User.Identity.Name);
                AsstDataEntryNewmodel = GetProductsNew(UserName);
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }

            return View(AsstDataEntryNewmodel);
        }

        public AssistanceDataEntryNewModel GetProductsNew(string UserName)
        {

            AssistanceDataEntryNewModel AsstDataEntryNewmodel = new AssistanceDataEntryNewModel();

            AsstDataEntryNewmodel.PermanentStateListItem = db.tblMstStates.AsNoTracking().Where(w => w.IsValid != false).Select(s => new { s.StateID, s.State })
                              .ToDictionary(d => d.StateID, d => d.State);

            return AsstDataEntryNewmodel;
        }

        // sa 1003

        #region GetCityByStateID
        public ActionResult GetCityByStateID(int StateID)
        {
            try
            {
                List<SelectListItem> data = new List<SelectListItem>();
                var lstData = db.tblMstCities.Where(a => a.StateID == StateID && a.IsValid != false).Select(x => new { x.CityID, x.City }).OrderBy(o => o.City).ToList();
                foreach (var _item in lstData)
                {
                    data.Add(new SelectListItem { Value = _item.CityID.ToString(), Text = _item.City });
                }
                return Json(new { CityMasterData = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetCityByStateIDForFetch
        public List<SelectListItem> GetCityByStateIDForFetch(int StateID)
        {
            try
            {
                List<SelectListItem> data = new List<SelectListItem>();
                var lstData = db.tblMstCities.Where(a => a.StateID == StateID && a.IsValid != false).Select(x => new { x.CityID, x.City }).OrderBy(o => o.City).ToList();
                foreach (var _item in lstData)
                {
                    data.Add(new SelectListItem { Value = _item.CityID.ToString(), Text = _item.City });
                }

                return data;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion


        #region GetCityByStateIDForAssist
        public ActionResult GetCityByStateIDForAssist(int StateID)
        {
            try
            {
                List<SelectListItem> data = new List<SelectListItem>();
                var lstData = (from vbd in db.tblVendorBranchDetails.AsNoTracking()
                               join c in db.tblMstCities.AsNoTracking() on vbd.City equals c.CityID into Citybased
                               from Citybasedobj in Citybased.DefaultIfEmpty()
                               where vbd.State == StateID
                               select new
                               {
                                   CityID = Citybasedobj.CityID,
                                   City = Citybasedobj.City

                               }).Distinct().OrderBy(o => o.City).ToList();

                foreach (var _item in lstData)
                {
                    data.Add(new SelectListItem { Value = _item.CityID.ToString(), Text = _item.City });
                }
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion


        #region GetAreaByCityID
        public ActionResult GetAreaByCityID(int CityID)
        {
            try
            {
                List<SelectListItem> data = new List<SelectListItem>();
                var lstData = db.tblVendorBranchDetails.Where(a => a.City == CityID && a.Status != false).Select(x => new { x.BranchID, x.BranchName }).OrderBy(o => o.BranchID).ToList();

                foreach (var _item in lstData)
                {
                    data.Add(new SelectListItem { Value = _item.BranchID.ToString(), Text = _item.BranchName });
                }
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetMakeByProductIDForUser
        public ActionResult GetMakeByProductIDForUser(int ProductID)
        {
            try
            {
                List<SelectListItem> makeListItem = new List<SelectListItem>();
                List<string> makeforUser = new List<string>();

                string UseName = Convert.ToString(User.Identity.Name);

                var lstData = db.tblMstMakes.Where(a => a.ProductID == ProductID && a.IsValid != false).Select(x => new { x.MakeID, x.Name }).OrderBy(o => o.Name).ToList();
                foreach (var _item in lstData)
                {
                    makeListItem.Add(new SelectListItem { Value = _item.MakeID.ToString(), Text = _item.Name });
                }

                var jsonResult = Json(new
                {
                    makelist = makeListItem
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetCompanyNameByOEMIdForWeb
        public ActionResult GetCompanyNameByOEMIdForWeb(int OEMId)
        {
            try
            {
                List<SelectListItem> companyListItem = new List<SelectListItem>();

                string UseName = Convert.ToString(User.Identity.Name);

                var lstData = (from OCM in db.tblOEMCompanyMapping.AsNoTracking()
                               join MCT in db.tblMstCommonTypes.AsNoTracking() on OCM.CompanyId equals MCT.CommonTypeID into Companybased
                               from CompanybasedObj in Companybased.DefaultIfEmpty()
                               where OCM.OEMId == OEMId && OCM.IsValid != false && CompanybasedObj.MasterType == CrossCutting_Constant.Company_Name
                               select new
                               {
                                   CompanyID = CompanybasedObj.CommonTypeID,
                                   CompanyName = CompanybasedObj.Description

                               }).OrderBy(o => o.CompanyName).ToList();

                foreach (var _item in lstData)
                {
                    companyListItem.Add(new SelectListItem { Value = _item.CompanyID.ToString(), Text = _item.CompanyName });
                }

                var jsonResult = Json(new
                {
                    companylist = companyListItem
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetAllCompanyNameForWeb
        public ActionResult GetAllCompanyNameForWeb()
        {
            try
            {
                List<SelectListItem> companyListItem = new List<SelectListItem>();

                string UseName = Convert.ToString(User.Identity.Name);

                var lstData = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.Company_Name)
                                             .Select(s => new { s.CommonTypeID, s.Description }).ToList();

                foreach (var _item in lstData)
                {
                    companyListItem.Add(new SelectListItem { Value = _item.CommonTypeID.ToString(), Text = _item.Description });
                }

                var jsonResult = Json(new
                {
                    companylist = companyListItem
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetMakeByProductIDForFetch
        public List<SelectListItem> GetMakeByProductIDForFetch(int ProductID)
        {
            try
            {
                List<SelectListItem> makeListItem = new List<SelectListItem>();
                List<string> makeforUser = new List<string>();

                string UseName = Convert.ToString(User.Identity.Name);

                var lstData = db.tblMstMakes.Where(a => a.ProductID == ProductID && a.IsValid != false).Select(x => new { x.MakeID, x.Name }).OrderBy(o => o.Name).ToList();
                foreach (var _item in lstData)
                {
                    makeListItem.Add(new SelectListItem { Value = _item.MakeID.ToString(), Text = _item.Name });
                }
               
                return makeListItem;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetModelByMakeIDForUser
        public ActionResult GetModelByMakeIDForUser(int MakeID)
        {
            try
            {
                List<SelectListItem> modelListItem = new List<SelectListItem>();
                List<string> modelforUser = new List<string>();

                string UseName = Convert.ToString(User.Identity.Name);

                var lstData = db.tblMstModels.Where(a => a.MakeID == MakeID && a.IsValid != false).Select(x => new { x.ModelID, x.Name, x.Variant }).OrderBy(o => o.Name).ToList();
                foreach (var _item in lstData)
                {
                    modelListItem.Add(new SelectListItem { Value = _item.ModelID.ToString(), Text = _item.Name + " - " + _item.Variant });
                }

                var jsonResult = Json(new
                {
                    modellist = modelListItem,
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetModelByMakeIDForFetch
        public List<SelectListItem> GetModelByMakeIDForFetch(int MakeID)
        {
            try
            {
                List<SelectListItem> modelListItem = new List<SelectListItem>();
                List<string> modelforUser = new List<string>();

                string UseName = Convert.ToString(User.Identity.Name);

                var lstData = db.tblMstModels.Where(a => a.MakeID == MakeID && a.IsValid != false).Select(x => new { x.ModelID, x.Name, x.Variant }).OrderBy(o => o.Name).ToList();
                foreach (var _item in lstData)
                {
                    modelListItem.Add(new SelectListItem { Value = _item.ModelID.ToString(), Text = _item.Name + " - " + _item.Variant });
                }
               
                return modelListItem;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion
        // ea 1003

        #region SaveAssistDataEntry
        [HttpPost]
        public ActionResult SaveAssistDataEntry(AssistanceDataEntryModel AsstDataEntrymodel)
        {
            string AssistRefNo = "";

            try
            {
                if (AsstDataEntrymodel != null)
                {
                    DateTime CurrentDate = DateTime.Now;
                    tblTransaction model = new tblTransaction();
                    tblTransactionAuditLog audit = new tblTransactionAuditLog();

                    string UserName = User.Identity.Name;
                    AspNetUser userdata = db.AspNetUsers.Where(w => w.UserName == UserName).FirstOrDefault();

                    ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));

                    var assist = db.tblTransactions.Where(x => x.CustomerContact == AsstDataEntrymodel.CustomerContact &&
                            x.CreatedDate >= DateTime.Today).Any();

                    //if (assist == false)  // no record exists
                    //{
                    var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.AssistReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);
                    model.CertificateNo = Convert.ToString(OutputOfPaymentReference.Value);

                    model.IssueByID = 10;
                    model.UserID = userdata.Id;
                    model.CreatedDate = CurrentDate;
                    model.IsValid = true;

                    model.ProductID = AsstDataEntrymodel.ProductID;
                    model.MakeID = AsstDataEntrymodel.MakeID;
                    model.ModelID = AsstDataEntrymodel.ModelID;

                    model.EngineNo = AsstDataEntrymodel.EngineNo;
                    model.ChassisNo = AsstDataEntrymodel.ChassisNo;
                    model.RegistrationNo = AsstDataEntrymodel.RegistrationNo;

                    model.CustomerName = AsstDataEntrymodel.CustomerName;
                    model.CustomerMiddleName = AsstDataEntrymodel.CustomerMiddleName;
                    model.CustomerLastName = AsstDataEntrymodel.CustomerLastName;
                    model.CustomerContact = AsstDataEntrymodel.CustomerContact;
                    model.CustomerEmail = AsstDataEntrymodel.CustomerEmail;

                    model.PermanentCityID = AsstDataEntrymodel.PermanentCityID;

                    model.IssueDate = CurrentDate;

                    model.StatusID = CrossCutting_Constant.StatusID_Payment_Entry;
                    model.Remark = CrossCutting_Constant.RemarkAssistanceDataEntry;

                    //
                    //model.AssistCompanyID = Convert.ToInt32(AsstDataEntrymodel.CompanyID);
                    model.AssistCompanyID = Convert.ToInt32(AsstDataEntrymodel.CompanyDetails);
                    //

                    if (!string.IsNullOrEmpty(AsstDataEntrymodel.OEMDetails))
                    {
                        model.AssistOEMID = Convert.ToInt32(AsstDataEntrymodel.OEMDetails);
                    }
                    else
                    {
                        model.AssistOEMID = null;
                    }

                    model.CardLast4Digit = AsstDataEntrymodel.CardLast4DigitFetched;
                    model.MobileNoLast4Digit = AsstDataEntrymodel.MobileNoLast4DigitFetched;

                    audit.UserID = model.UserID;
                    audit.StatusID = model.StatusID;
                    audit.Remark = model.Remark;
                    audit.IsValid = true;
                    audit.CreatedDateTime = CurrentDate;
                    audit.CertificateNo = model.CertificateNo;
                    model.tblTransactionAuditLogs.Add(audit);
                    db.tblTransactions.Add(model);
                    db.SaveChanges();

                    AssistRefNo = model.CertificateNo;

                    return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = AssistRefNo }, JsonRequestBehavior.AllowGet);
                    //}
                    //else // Request already submitted
                    //{
                    //    return Json(new { status = "AssistExist", AssistRefNo = AssistRefNo }, JsonRequestBehavior.AllowGet);
                    //}
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, AssistRefNo = "" }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region TravelDataPost
        [HttpPost]
        public ActionResult TravelDataPost(TravelModel TravelModel)
        {
            try
            {
                TravelModelRequest TravelModelRequest = new TravelModelRequest();
                MemberDetail MemberDetail = new MemberDetail();

                string CurrentTime = DateTime.Now.ToString("HH:mm:ss");
                string Time = "00:00:00";
                List<MemberDetail> lstMemberDetail = new List<MemberDetail>();

                string PSONumberUnique = "PSO" + DateTime.Now.Ticks.ToString();
                string PaxId = "PAX" + DateTime.Now.Ticks.ToString();
                string Product_Code = ConfigurationManager.AppSettings["Product_Code"];
                string AgentCode_BASCode = ConfigurationManager.AppSettings["AgentCode_BASCode"];
                string UserName = ConfigurationManager.AppSettings["UserName"];
                string EncryptedPassword = ConfigurationManager.AppSettings["EncryptedPassword"];
                string IntermediatoryBranchCode = ConfigurationManager.AppSettings["IntermediatoryBranchCode"];
                string IntermediatoryDepartmentName = ConfigurationManager.AppSettings["IntermediatoryDepartmentName"];
                string BusinessType = ConfigurationManager.AppSettings["BusinessType"];

                if (TravelModel != null)
                {
                    TravelModelRequest.Product_Code = Product_Code;
                    TravelModelRequest.AgentCode_BASCode = AgentCode_BASCode;
                    TravelModelRequest.UserName = UserName;
                    TravelModelRequest.EncryptedPassword = EncryptedPassword;
                    TravelModelRequest.IntermediatoryBranchCode = IntermediatoryBranchCode;
                    TravelModelRequest.IntermediatoryDepartmentName = IntermediatoryDepartmentName;

                    if (TravelModel.ComFlatNo != null || TravelModel.ComBuilding != null)
                    {
                        TravelModelRequest.AddressLine1 = TravelModel.ComFlatNo + "," + TravelModel.ComBuilding;
                    }
                    else
                    {
                        TravelModelRequest.AddressLine1 = null;
                    }

                    if (TravelModel.ComStreet != null || TravelModel.ComLandMark != null)
                    {
                        TravelModelRequest.AddressLine2 = TravelModel.ComStreet + "," + TravelModel.ComLandMark;
                    }
                    else
                    {
                        TravelModelRequest.AddressLine2 = null;
                    }

                    TravelModelRequest.CityName = TravelModel.ComTownCity;
                    TravelModelRequest.PinCode = TravelModel.ComPincode;
                    TravelModelRequest.State = TravelModel.ComState;
                    TravelModelRequest.EmailID = TravelModel.Email;
                    TravelModelRequest.MobileNumber = TravelModel.MobileNo;
                    TravelModelRequest.LandLineNumber = TravelModel.ComLandLine;
                    TravelModelRequest.NameofPlan = TravelModel.PlanName;
                    TravelModelRequest.PolicyStartDate = Convert.ToDateTime(TravelModel.StartDate).ToString("yyyy-MM-dd") + " " + CurrentTime;
                    TravelModelRequest.PolicyEndDate = Convert.ToDateTime(TravelModel.EndDate).ToString("yyyy-MM-dd") + " " + CurrentTime;
                    TravelModelRequest.BusinessType = BusinessType;
                    TravelModelRequest.PSONumber = PSONumberUnique;
                    TravelModelRequest.SenderName = TravelModel.FirstName + " " + TravelModel.MiddleName + " " + TravelModel.LastName;
                    TravelModelRequest.CountryVisiting = TravelModel.CountryVisitingName;
                    TravelModelRequest.StateVisit = TravelModel.StateVisiting;
                    TravelModelRequest.City_Of_visit = TravelModel.CityOfVisit;
                    TravelModelRequest.IsRegGST = TravelModel.RegGST;
                    TravelModelRequest.Cust_GSTINNO = TravelModel.GSTINNO;

                    MemberDetail.PaxId = PaxId;
                    MemberDetail.InsuredFirstName = TravelModel.InsFirstName;
                    MemberDetail.InsuredMiddleName = TravelModel.InsMiddleName;
                    MemberDetail.InsuredLastName = TravelModel.InsLastName;
                    MemberDetail.InsuredGender = TravelModel.InsGender;
                    MemberDetail.PassportNo = TravelModel.InsPassportNo;
                    MemberDetail.IdentificationNo = MemberDetail.PassportNo;
                    MemberDetail.NomineeName = TravelModel.NomName;
                    MemberDetail.RelationshipOfTheNomineeWithInsured = TravelModel.NomRel;
                    MemberDetail.DateOfBirth = Convert.ToDateTime(TravelModel.InsDOB).ToString("yyyy-MM-dd") + " " + Time;
                    MemberDetail.AgeGroup = TravelModel.AgeGroup;

                    if (TravelModel.MedicalCondition != null)
                    {
                        if (TravelModel.MedicalCondition == "No")
                        {
                            MemberDetail.SufferingFromAnyPreExistingDisease = false;
                        }
                        else if (TravelModel.MedicalCondition == "Yes")
                        {
                            MemberDetail.SufferingFromAnyPreExistingDisease = true;
                        }
                    }

                    MemberDetail.NameOfDiseases = TravelModel.NameOfDiseaseDetails;
                    MemberDetail.AddressOfTheHomeToBeCoveredUnderHomeBurglaryPlan = TravelModel.AddOfHomeCoveredUnderBurgPlan;

                    lstMemberDetail.Add(MemberDetail);

                    TravelModelRequest.MemberDetails = lstMemberDetail;

                    var serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = int.MaxValue;

                    string SerializedRequest = serializer.Serialize(TravelModelRequest);

                    string ServiceURL = ConfigurationManager.AppSettings["Service_URL_RILTravel"];

                    string ResponseResult = CallService(ServiceURL, SerializedRequest);
                    //string DeserializedResponse = 

                    return View();
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        public string CallService(string ServiceURL, string RequestData)
        {
            string ResponseResult = "";
            try
            {
                int httpTimeout = 300;
                if (int.TryParse(ConfigurationManager.AppSettings["httptimeoutinsec"], out httpTimeout) == false)
                {
                    httpTimeout = 300;
                }

                byte[] byteArray = Encoding.UTF8.GetBytes(RequestData);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ServiceURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = httpTimeout * 1000;
                httpWebRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = httpWebRequest.GetRequestStream())
                {
                    // Write the data to the request stream.  
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    // Close the Stream object.  
                    dataStream.Flush();
                    dataStream.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    ResponseResult = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }

            return ResponseResult;
        }
        #endregion

        #region SearchCertificate
        public JsonResult SearchCertificate(string CertificateNo, string EngineNo, string ChassisNo, string Registration, string MobileNo,string EmailID)
        {
            try
            {
                List<AssistModel> list = new List<AssistModel>();
                ViewBag.CertificateNo = CertificateNo;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.Registration = Registration;
                ViewBag.MobileNo = MobileNo;
                ViewBag.Email = EmailID;

                TempData["Load"] = "FirstTime";
                list = this.GetSearchCertificateList(CertificateNo, EngineNo, ChassisNo, Registration, MobileNo,EmailID);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SearchCertificateGrid.cshtml", Grid);
                return Json(new { status = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region SearchCertificateGridNextpage
        public ActionResult SearchCertificateNextpage(int? Page, string CertificateNo, string EngineNo, string ChassisNo, string Registration, string MobileNo, string EmailID,int SortingOrder = 0, int Paging = 5)
        {
            try
            {
                TempData["Load"] = "FirstTime";
                ViewBag.CertificateNo = CertificateNo;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.Registration = Registration;
                ViewBag.MobileNo = MobileNo;
                ViewBag.Email = EmailID;
                List<AssistModel> list = new List<AssistModel>();
                list = this.GetSearchCertificateList(CertificateNo, EngineNo, ChassisNo, Registration, MobileNo,EmailID);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/SearchCertificateGridNextpage.cshtml", grid);
                return Json(new { status = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        #endregion

        #region GetSearchCertificateList
        public List<AssistModel> GetSearchCertificateList(string CertificateNo, string EngineNo, string ChassisNo, string Registration, string MobileNo,string EmailID)
        {
            //string[] AllDataAssistUser = WebConfigurationManager.AppSettings["AllDataAssistUser"].ToLower().Split(',');

            int CompanyId = 0;
            List<AssistModel> list = new List<AssistModel>();

            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID, s.CompanyID }).FirstOrDefault();
            if (userdata != null)
            {
                if (userdata.CompanyID == null)
                {
                    CompanyId = 0;
                }
                else
                {
                    CompanyId = Convert.ToInt32(userdata.CompanyID);
                }
            }

            //if (AllDataAssistUser.Contains(UserName))
            if (CompanyId > 0)
            {
                list = (from t in db.tblTransactions.AsNoTracking()
                        join p in db.tblMstMakes.AsNoTracking() on t.MakeID equals p.MakeID into makebased
                        from makebasedobj in makebased.DefaultIfEmpty()
                        join model in db.tblMstModels on t.ModelID equals model.ModelID
                        into modelbased
                        from modelbasedobj in modelbased.DefaultIfEmpty()
                        join pstats in db.tblMstWorkFlowStatus on t.StatusID equals pstats.StatusID into statusbased
                        from statusbasedobj in statusbased.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes on t.AssistCompanyID equals cmntype.CommonTypeID into cmntypebased
                        from cmntypebasedobj in cmntypebased.DefaultIfEmpty()
                        where (t.CertificateNo.Contains(CertificateNo) || string.IsNullOrEmpty(CertificateNo))
                        && (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo))
                        && (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo))
                        && (t.RegistrationNo.Contains(Registration) || string.IsNullOrEmpty(Registration))
                        && (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo))
                        && (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID))
                        && (t.StatusID == CrossCutting_Constant.StatusID_Payment_Entry)
                        && (t.UserID == userdata.Id)
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            CertificateNo = t.CertificateNo,
                            Make = makebasedobj.Name,
                            Model = modelbasedobj.Name,
                            Status = statusbasedobj.Status,
                            FirstName = t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            RegistrationNo = t.RegistrationNo,
                            CompanyName = cmntypebasedobj.Description

                        }).ToList();
            }
            else
            {
                list = (from t in db.tblTransactions.AsNoTracking()
                        join p in db.tblMstMakes.AsNoTracking() on t.MakeID equals p.MakeID into makebased
                        from makebasedobj in makebased.DefaultIfEmpty()
                        join model in db.tblMstModels on t.ModelID equals model.ModelID
                        into modelbased
                        from modelbasedobj in modelbased.DefaultIfEmpty()
                        join pstats in db.tblMstWorkFlowStatus on t.StatusID equals pstats.StatusID into statusbased
                        from statusbasedobj in statusbased.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes on t.AssistCompanyID equals cmntype.CommonTypeID into cmntypebased
                        from cmntypebasedobj in cmntypebased.DefaultIfEmpty()
                        where (t.CertificateNo.Contains(CertificateNo) || string.IsNullOrEmpty(CertificateNo))
                        && (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo))
                        && (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo))
                        && (t.RegistrationNo.Contains(Registration) || string.IsNullOrEmpty(Registration))
                        && (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo))
                        && (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID))
                        && (t.StatusID == CrossCutting_Constant.StatusID_Payment_Entry || t.StatusID == CrossCutting_Constant.StatusID_Pending_Payment)
                        //&& (t.UserID == userdata.Id)
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            CertificateNo = t.CertificateNo,
                            Make = makebasedobj.Name,
                            Model = modelbasedobj.Name,
                            Status = statusbasedobj.Status,
                            FirstName = t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            RegistrationNo = t.RegistrationNo,
                            CompanyName = cmntypebasedobj.Description

                        }).ToList();                
            }

            return list;
        }


        #endregion
        [HttpGet]
        public JsonResult GetAssistDetails(string TransactionID)
        {
            string HTML = String.Empty;


            try
            {
                string UserName = HttpContext.User.Identity.Name;
                var user = db.AspNetUsers.Where(x => x.UserName == UserName).FirstOrDefault();
                ViewData["TieUpCompany"] = user.TieUpCompanyID;

                decimal Transactionid = Convert.ToDecimal(TransactionID);
                AssistModel assistDetails = new AssistModel();
                assistDetails = this.GetAssistDetail(Transactionid);
                assistDetails.ActionID = CrossCutting_Constant.ActionID_Vendor_No_Action;

                HTML = RenderPartialViewToString("~/Views/Assistance/GetAssistDetails.cshtml", assistDetails);
                return Json(new { message = CrossCutting_Constant.Success, HTML = HTML, isRecentAssistOpen = assistDetails.isRecentAssistOpen }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }


        }
        //sa 1007
        [HttpGet]
        public ActionResult GetAssistDetailToEdit(string TransactionID)
        {
            string UserName = HttpContext.User.Identity.Name;
            var user = db.AspNetUsers.Where(x => x.UserName == UserName).FirstOrDefault();
            ViewData["TieUpCompany"] = user.TieUpCompanyID;

            decimal Transactionid = Convert.ToDecimal(TransactionID);
            AssistModel assistDetails = new AssistModel();
            assistDetails = this.GetAssistDetail(Transactionid);
            assistDetails.ActionID = CrossCutting_Constant.ActionID_Vendor_No_Action;
            return View(assistDetails);
        }
        //ea 1007

        public AssistModel GetAssistDetail(decimal transactionid)
        {
            AssistModel data = new AssistModel();

            tblTransaction objTran = db.tblTransactions.Where(x => x.TransactionID == transactionid).FirstOrDefault();
            data = (from t in db.tblTransactions.AsNoTracking()
                    join p in db.tblMstMakes.AsNoTracking() on t.MakeID equals p.MakeID into makebased
                    from makebasedobj in makebased.DefaultIfEmpty()
                    join model in db.tblMstModels on t.ModelID equals model.ModelID into modelbased
                    from modelbasedobj in modelbased.DefaultIfEmpty()
                    where (t.TransactionID == transactionid)
                    select new AssistModel
                    {
                        TransactionID = t.TransactionID,
                        CertificateNo = t.CertificateNo,
                        Make = makebasedobj.Name,
                        Model = modelbasedobj.Name,
                        FirstName = t.CustomerName,
                        MiddleName = t.CustomerMiddleName,
                        LastName = t.CustomerLastName,
                        Email = t.CustomerEmail,
                        Mobile = t.CustomerContact,
                        RegistrationNo = t.RegistrationNo,
                        EngineNo = t.EngineNo,
                        ChassisNo = t.ChassisNo,
                        CertificateStartDate = t.CoverStartDate,
                        CertificateEndDate = t.CoverEndDate,
                        IncidentTypeId = 0,
                        IncidentDetailId = 0,
                        Issue = t.CustomerContact,
                        AssistCompanyID = t.AssistCompanyID,
                        hdnAssistCompanyID = t.AssistCompanyID,
                        AssistOEMID = t.AssistOEMID,
                        hdnAssistOEMID = t.AssistOEMID

                    }).FirstOrDefault();

            data.CaseTypeListItem = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistanceCaseType")
                                             .Select(s => new { s.CommonTypeID, s.Description }).ToDictionary(d => d.CommonTypeID, d => d.Description);

            data.RequestTypeListItem = (from mct in db.tblMstCommonTypes.AsNoTracking()
                                        where mct.IsValid != false && mct.MasterType == "AssistanceRequestType"
                                        select new
                                        {
                                            RequestTypeID = mct.CommonTypeID,
                                            RequestType = mct.Description

                                        }).ToDictionary(d => d.RequestTypeID, d => d.RequestType);

            // RequestPlannedDateTime Time Start
            data.RequestPlannedDateTimeHoursListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"}
                    };

            data.RequestPlannedDateTimeMinsListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"},
                        {"24", "24"},{"25", "25"},{"26", "26"},{"27", "27"},{"28", "28"},{"29", "29"},{"30", "30"},{"31", "31"},{"32", "32"},{"33", "33"},{"34", "34"},{"35", "35"},
                        {"36", "36"},{"37", "37"},{"38", "38"},{"39", "39"},{"40", "40"},{"41", "41"},{"42", "42"},{"43", "43"},{"44", "44"},{"45", "45"},{"46", "46"},{"47", "47"},
                        {"48", "48"},{"49", "49"},{"50", "50"},{"51", "51"},{"52", "52"},{"53", "53"},{"54", "54"},{"55", "55"},{"56", "56"},{"57", "57"},{"58", "58"},{"59", "59"}
                    };

            data.RequestPlannedDateTimeSecsListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"},
                        {"24", "24"},{"25", "25"},{"26", "26"},{"27", "27"},{"28", "28"},{"29", "29"},{"30", "30"},{"31", "31"},{"32", "32"},{"33", "33"},{"34", "34"},{"35", "35"},
                        {"36", "36"},{"37", "37"},{"38", "38"},{"39", "39"},{"40", "40"},{"41", "41"},{"42", "42"},{"43", "43"},{"44", "44"},{"45", "45"},{"46", "46"},{"47", "47"},
                        {"48", "48"},{"49", "49"},{"50", "50"},{"51", "51"},{"52", "52"},{"53", "53"},{"54", "54"},{"55", "55"},{"56", "56"},{"57", "57"},{"58", "58"},{"59", "59"}
                    };
            // RequestPlannedDateTime Time End

            data.lstIncidentType = db.tblMstIncidents.Where(x => x.IsValid == true).OrderBy(x => x.OrderBy)
                .Select(x => new SelectListItem { Text = x.IncidentType, Value = x.Incidentid.ToString() }).ToList();

            var AssistDetails = db.tblAssistDetails.Where(x => x.TransactionId == transactionid).FirstOrDefault();
            data.isRecentAssistOpen = false;
            if (AssistDetails != null)
            {
                if (AssistDetails.CreatedDate.Value.Date >= DateTime.Today.AddDays(-1) && AssistDetails.StatusId != CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
                {
                    data.isRecentAssistOpen = true;
                }
            }

            data.lstStatus = db.tblMstWorkFlowStatus.Where(x => x.StatusID == 10).Select(x =>
                new SelectListItem { Text = x.Status, Value = x.StatusID.ToString() }).ToList();

            data.lstVendorName = db.tblMstVendors.Where(x => x.IsVlid != false).Select(x =>
                new SelectListItem { Text = x.VendorName, Value = x.VendorID.ToString() }).OrderBy(o => o.Text).ToList();

            data.lstStateItem = (from vbd in db.tblVendorBranchDetails.AsNoTracking()
                                 join s in db.tblMstStates.AsNoTracking() on vbd.State equals s.StateID into Statebased
                                 from Statebasedobj in Statebased.DefaultIfEmpty()
                                 select new
                                 {
                                     StateID = Statebasedobj.StateID,
                                     State = Statebasedobj.State

                                 }).Distinct().OrderBy(o => o.State).ToDictionary(d => d.StateID, d => d.State);

            data.OEMListItem = (from mct in db.tblMstCommonTypes.AsNoTracking()
                                where mct.IsValid != false && mct.MasterType == "OEMName"
                                select new
                                {
                                    OEMID = mct.CommonTypeID,
                                    OEMName = mct.Description

                                }).ToDictionary(d => d.OEMID, d => d.OEMName);

            data.CompanyListItem = (from mct in db.tblMstCommonTypes.AsNoTracking()
                                    where mct.IsValid != false && mct.MasterType == "CompanyName"
                                    select new
                                    {
                                        CompanyID = mct.CommonTypeID,
                                        Company = mct.Description

                                    }).ToDictionary(d => d.CompanyID, d => d.Company);

            return data;

        }
        //sa 1002 


        public ActionResult RILAssist()
        {
            AssistModel data = new AssistModel();

            ViewBag.IncidentType = db.tblMstIncidents.Where(x => x.IsValid == true).OrderBy(x => x.OrderBy)
                .Select(x => new SelectListItem { Text = x.IncidentType, Value = x.Incidentid.ToString() }).ToList();


            var incidentDetails = db.tblMstIncidentDetails.Where(x => x.Detailid == data.IncidentDetailId).ToList();
            ViewBag.IncidentDetails = incidentDetails
               .Select(x => new SelectListItem { Text = x.IncidentDetail, Value = x.Detailid.ToString() }).ToList();

            return View();
        }



        //se 1002
        //se 1006

        public JsonResult SaveAssistDetails(AssistModel objAssist)
        {
            string strICICILombard = Convert.ToString(ConfigurationManager.AppSettings["ICICILombard"]);
            int ICICILombard = 0;
            int.TryParse(strICICILombard, out ICICILombard);

            tblAsstVendorUserLocation tblAsstVendorUserLocation = new tblAsstVendorUserLocation();
            tblAssistDetail tblAssist = new tblAssistDetail();
            bool isManual = objAssist.Manual_Allocation;
            decimal branchid = objAssist.Vendor_BranchID;
            //bool isRelianceAssist = false;

            string AssistanceCaseCreatedToCustomerBody = ConfigurationManager.AppSettings["AssistanceCaseCreatedToCustomerBody"];
            string AssistanceCompletionToVendorBody = ConfigurationManager.AppSettings["AssistanceCompletionToVendorBody"];
            string AssistanceCompletionToCustomerBody = ConfigurationManager.AppSettings["AssistanceCompletionToCustomerBody"];
            string[] AssistanceSendMsgMobileNos = ConfigurationManager.AppSettings["AssistanceSendMsgMobileNos"].Split(',');
            string AssistanceCompletionAssistanceTeamBody = ConfigurationManager.AppSettings["AssistanceCompletionAssistanceTeamBody"];
            string Message = string.Empty;
            bool MsgSent = false;

            // CheckIsRelianceAssistance Logic Start
            string CompanyId = "0";
            string[] IsEditable = Convert.ToString(ConfigurationManager.AppSettings["IsEditable"]).Split(',');

            bool boolIsRelianceAssistance = false;
            string RILUserID = Convert.ToString(ConfigurationManager.AppSettings["RILUserID"]);
            string RelianceUserID = string.Empty;
            string AssistUserID = string.Empty;

            var tblAssistD = db.tblAssistDetails.Where(w => w.Id == objAssist.AssistID).Select(s => new { s.AssistCreatedBy }).FirstOrDefault();
            if (tblAssistD != null)
            {
                string AssistCreatedBy = tblAssistD.AssistCreatedBy;
                var usrdata = db.AspNetUsers.AsNoTracking().Where(w => w.Id == AssistCreatedBy).Select(s => new { s.CompanyID }).FirstOrDefault();
                if (usrdata != null)
                {
                    if (usrdata.CompanyID == null)
                    {
                        CompanyId = "0";
                    }
                    else
                    {
                        CompanyId = Convert.ToString(usrdata.CompanyID);
                    }
                }
            }

            var userdata = db.AspNetUsers.Where(w => w.Email == RILUserID).FirstOrDefault();
            if (userdata != null)
            {
                RelianceUserID = userdata.Id;
            }

            var tblAssistDetails = db.tblAssistDetails.Where(w => w.Id == objAssist.AssistID).Select(s => new { s.TransactionId }).FirstOrDefault();
            if (tblAssistDetails != null)
            {
                decimal TransactionId = Convert.ToDecimal(tblAssistDetails.TransactionId);
                var tblTransaction = db.tblTransactions.Where(w => w.TransactionID == TransactionId).Select(s => new { s.UserID }).FirstOrDefault();
                if (!string.IsNullOrEmpty(tblTransaction.UserID))
                {
                    AssistUserID = tblTransaction.UserID;
                    if (AssistUserID.Equals(RelianceUserID))
                    {
                        boolIsRelianceAssistance = true;
                    }
                    else
                    {
                        if (IsEditable.Contains(CompanyId))
                        {
                            boolIsRelianceAssistance = true;
                        }
                        else
                        {
                            boolIsRelianceAssistance = false;
                        }
                    }
                }                              
            }
            // CheckIsRelianceAssistance Logic End

            string UserName = HttpContext.User.Identity.Name;
            var user = db.AspNetUsers.Where(x => x.UserName == UserName).FirstOrDefault();
            //if (user.TieUpCompanyID == CrossCutting_Constant.TieUpRelianceAssist)
            //{
            //    isRelianceAssist = true;
            //}
            //else
            //{
            //    isRelianceAssist = false;
            //}

            try
            {
                if (objAssist != null)
                {
                    if (objAssist.AssistID > 0) // Update Vendor Data
                    {
                        var data = (from vbd in db.tblVendorBranchDetails.AsNoTracking()
                                    join v in db.tblMstVendors.AsNoTracking() on vbd.VendorID equals v.VendorID into objVendorBased
                                    from objVendor in objVendorBased.DefaultIfEmpty()
                                    where vbd.BranchID == objAssist.Vendor_BranchID
                                    select new
                                    {
                                        VendorMobileNo = objVendor.VendorMobileNo

                                    }).FirstOrDefault();

                        tblAssist = db.tblAssistDetails.Where(x => x.Id == objAssist.AssistID).FirstOrDefault();                        

                        if (boolIsRelianceAssistance == true)
                        {
                            //
                            tblAssist.UserID = user.Id;
                            //
                            tblAssist.PinCode = objAssist.PinCode;
                            tblAssist.Landmark = objAssist.Landmark;
                            //tblAssist.Issue = tblAssist.Issue;
                            tblAssist.Address = objAssist.Address;
                            //tblAssist.StreetAddress = objAssist.StreetAddress;
                            tblAssist.CreatedDate = DateTime.Now;
                            //tblAssist.City = objAssist.City;
                            //tblAssist.Location = objAssist.location;
                            //tblAssist.State = objAssist.State;
                            //tblAssist.Address = objAssist.Address;
                            tblAssist.IncidentDetailid = objAssist.IncidentDetailId;
                            tblAssist.Lat = Convert.ToString(objAssist.LatitudeLoc);
                            tblAssist.Lon = Convert.ToString(objAssist.LongitudeLoc);
                            //
                            tblAssist.Remark1 = objAssist.RemarkId;
                            //
                            if (string.IsNullOrEmpty(objAssist.AssistSummary))
                            {
                                string CurrDateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                tblAssist.SummaryDetails = tblAssist.SummaryDetails + " ( " + CurrDateTime + " ) " + "N/A";
                            }
                            else
                            {
                                string CurrDateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                tblAssist.SummaryDetails = tblAssist.SummaryDetails + " ( " + CurrDateTime + " ) " + objAssist.AssistSummary;
                            }
                            //

                            //decimal VendorID = Convert.ToDecimal(tblAssist.VendorId);
                            //tblAssist.VendorId = VendorID;       

                            DateTime? VendorReachedIncidentLocationDateTime = null;
                            if (!string.IsNullOrEmpty(objAssist.strVendorReachedIncidentLocationDateTime))
                            {
                                VendorReachedIncidentLocationDateTime = DateTime.ParseExact(objAssist.strVendorReachedIncidentLocationDateTime, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                string IncDate = Convert.ToDateTime(VendorReachedIncidentLocationDateTime).ToString("yyyy-MM-dd");
                                //string IncTime = DateTime.Now.ToString("HH:mm:ss");
                                string IncHours = objAssist.VendorReachedIncidentLocationHours;
                                string IncMins = objAssist.VendorReachedIncidentLocationMins;
                                string IncSecs = objAssist.VendorReachedIncidentLocationSecs;
                                string IncTime = IncHours + ":" + IncMins + ":" + IncSecs;
                                string strIncDateTime = IncDate + " " + IncTime;
                                DateTime dtIncDateTime = Convert.ToDateTime(strIncDateTime);

                                tblAssist.VenReachedIncLocDateTime = dtIncDateTime;
                            }
                            else
                            {
                                VendorReachedIncidentLocationDateTime = tblAssist.VenReachedIncLocDateTime;
                                tblAssist.VenReachedIncLocDateTime = VendorReachedIncidentLocationDateTime;
                            }

                            DateTime? VendorReachedDropLocationDateTime = null;
                            if (!string.IsNullOrEmpty(objAssist.strVendorReachedDropLocationDateTime))
                            {
                                VendorReachedDropLocationDateTime = DateTime.ParseExact(objAssist.strVendorReachedDropLocationDateTime, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                string DropDate = Convert.ToDateTime(VendorReachedDropLocationDateTime).ToString("yyyy-MM-dd");
                                //string DropTime = DateTime.Now.ToString("HH:mm:ss");
                                string DropHours = objAssist.VendorReachedDropLocationHours;
                                string DropMins = objAssist.VendorReachedDropLocationMins;
                                string DropSecs = objAssist.VendorReachedDropLocationSecs;
                                string DropTime = DropHours + ":" + DropMins + ":" + DropSecs;
                                string strDropDateTime = DropDate + " " + DropTime;
                                DateTime dtDropDateTime = Convert.ToDateTime(strDropDateTime);

                                tblAssist.VenReachedDropLocDateTime = dtDropDateTime;
                            }
                            else
                            {
                                VendorReachedDropLocationDateTime = tblAssist.VenReachedDropLocDateTime;
                                tblAssist.VenReachedDropLocDateTime = VendorReachedDropLocationDateTime;
                            }

                            //
                            if (objAssist.StsId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue && tblAssist.StatusId != CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                            {
                                tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Closed_Issue;
                            }
                            else if (objAssist.StsId == 0 && tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                            {
                                tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                                //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            }
                            else if (objAssist.StsId == 0 && tblAssist.StatusId != CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                            {

                            }
                            else if (objAssist.StsId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue && tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                            {
                                tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Closed_Issue;
                                //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            }
                            else
                            {
                                tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                                //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            }

                            if (objAssist.StsId != 0 && tblAssist.StatusId != CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
                            {
                                //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            }

                            db.SaveChanges();

                            var branchDetails = db.tblVendorBranchDetails.Where(w => w.BranchID == tblAssist.VendorId).FirstOrDefault();
                            if (branchDetails != null)
                            {
                                var vendorDetails = db.tblMstVendors.Where(w => w.VendorID == branchDetails.VendorID).FirstOrDefault();
                                if (vendorDetails != null)
                                {
                                    var vendorUserDetails = db.AspNetUsers.Where(w => w.Email == vendorDetails.VendorEMail).FirstOrDefault();
                                    if (vendorUserDetails != null)
                                    {
                                        string VendorUserId = vendorUserDetails.Id;

                                        tblAsstVendorUserLocation = db.tblAsstVendorUserLocation.Where(w => w.ReferenceNo == tblAssist.RefNo).FirstOrDefault();
                                        if (tblAsstVendorUserLocation != null)
                                        {
                                            tblAsstVendorUserLocation.VendorId = VendorUserId;
                                        }
                                        db.SaveChanges();
                                    }
                                }
                            }

                            if (objAssist.StsId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue && tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
                            {
                                int dbAssistCompanyID = Convert.ToInt32(tblAssist.AssistCompanyID);
                                if (dbAssistCompanyID == ICICILombard)
                                {
                                    #region Send ICICILombard SMS Logic Part :: Case Completion

                                    string strIsSendSMS_CaseCompletion = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCompletionSMS_ICICILombard").Select(s => s.Description).FirstOrDefault();
                                    int IsSendSMS_CaseCompletion = (!string.IsNullOrEmpty(strIsSendSMS_CaseCompletion)) ? (strIsSendSMS_CaseCompletion == "1") ? 1 : 0 : 0;
                                    if (IsSendSMS_CaseCompletion == 1)
                                    {
                                        int RemarkId = 0;
                                        int.TryParse(tblAssist.Remark1, out RemarkId);

                                        string dbRemark = db.tblMstCommonTypes.AsNoTracking().Where(w => w.CommonTypeID == RemarkId).Select(s => s.Description.ToUpper()).FirstOrDefault();
                                        if (!string.IsNullOrEmpty(dbRemark))
                                        {
                                            if (dbRemark.Equals("CASE COMPLETED"))
                                            {
                                                string CustomerMobileNo = tblAssist.Issue;
                                                if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                {
                                                    string AssistanceReferenceNo = tblAssist.RefNo;
                                                    Message = AssistanceCompletionToCustomerBody + AssistanceReferenceNo;

                                                    MsgSent = _c.SendingICICILSMSMessage(tblAssist, CustomerMobileNo, Message);

                                                    if (MsgSent.Equals(true))
                                                    {
                                                        // Message Send. :: Success
                                                    }
                                                    else
                                                    {
                                                        // Message Not Send. :: Failed
                                                    }
                                                }
                                            }
                                        }

                                        string VendorMobileNo = data.VendorMobileNo;
                                        if (!string.IsNullOrEmpty(VendorMobileNo))
                                        {
                                            string AssistanceReferenceNo = tblAssist.RefNo;
                                            Message = AssistanceCompletionToVendorBody + AssistanceReferenceNo;

                                            MsgSent = _c.SendingICICILSMSMessage(tblAssist, VendorMobileNo, Message);

                                            if (MsgSent.Equals(true))
                                            {
                                                // Message Send. :: Success
                                            }
                                            else
                                            {
                                                // Message Not Send. :: Failed
                                            }
                                        }

                                        foreach (var AssistTeamMobileNo in AssistanceSendMsgMobileNos)
                                        {
                                            string AssistanceTeamMobileNo = AssistTeamMobileNo;
                                            if (!string.IsNullOrEmpty(AssistanceTeamMobileNo))
                                            {
                                                string AssistanceReferenceNo = tblAssist.RefNo;
                                                Message = AssistanceCompletionAssistanceTeamBody + AssistanceReferenceNo;

                                                MsgSent = _c.SendingICICILSMSMessage(tblAssist, AssistanceTeamMobileNo, Message);

                                                if (MsgSent.Equals(true))
                                                {
                                                    // Message Send. :: Success
                                                }
                                                else
                                                {
                                                    // Message Not Send. :: Failed
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Send SMS Logic Part :: Case Completion

                                    string strIsSendSMS_CaseCompletion = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCompletionSMS").Select(s => s.Description).FirstOrDefault();
                                    int IsSendSMS_CaseCompletion = (!string.IsNullOrEmpty(strIsSendSMS_CaseCompletion)) ? (strIsSendSMS_CaseCompletion == "1") ? 1 : 0 : 0;
                                    if (IsSendSMS_CaseCompletion == 1)
                                    {
                                        int RemarkId = 0;
                                        int.TryParse(tblAssist.Remark1, out RemarkId);

                                        string dbRemark = db.tblMstCommonTypes.AsNoTracking().Where(w => w.CommonTypeID == RemarkId).Select(s => s.Description.ToUpper()).FirstOrDefault();
                                        if (!string.IsNullOrEmpty(dbRemark))
                                        {
                                            if (dbRemark.Equals("CASE COMPLETED"))
                                            {
                                                string CustomerMobileNo = tblAssist.Issue;
                                                if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                {
                                                    string AssistanceReferenceNo = tblAssist.RefNo;
                                                    Message = AssistanceCompletionToCustomerBody + AssistanceReferenceNo;

                                                    MsgSent = _c.SendingSMSMessage(CustomerMobileNo, Message);

                                                    if (MsgSent.Equals(true))
                                                    {
                                                        // Message Send. :: Success
                                                    }
                                                    else
                                                    {
                                                        // Message Not Send. :: Failed
                                                    }
                                                }
                                            }
                                        }

                                        string VendorMobileNo = data.VendorMobileNo;
                                        if (!string.IsNullOrEmpty(VendorMobileNo))
                                        {
                                            string AssistanceReferenceNo = tblAssist.RefNo;
                                            Message = AssistanceCompletionToVendorBody + AssistanceReferenceNo;

                                            MsgSent = _c.SendingSMSMessage(VendorMobileNo, Message);

                                            if (MsgSent.Equals(true))
                                            {
                                                // Message Send. :: Success
                                            }
                                            else
                                            {
                                                // Message Not Send. :: Failed
                                            }
                                        }

                                        foreach (var AssistTeamMobileNo in AssistanceSendMsgMobileNos)
                                        {
                                            string AssistanceTeamMobileNo = AssistTeamMobileNo;
                                            if (!string.IsNullOrEmpty(AssistanceTeamMobileNo))
                                            {
                                                string AssistanceReferenceNo = tblAssist.RefNo;
                                                Message = AssistanceCompletionAssistanceTeamBody + AssistanceReferenceNo;

                                                MsgSent = _c.SendingSMSMessage(AssistanceTeamMobileNo, Message);

                                                if (MsgSent.Equals(true))
                                                {
                                                    // Message Send. :: Success
                                                }
                                                else
                                                {
                                                    // Message Not Send. :: Failed
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }

                            //return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = tblAssist.RefNo, Vendor = selectedVendor.tblMstVendor.VendorName, Branch = selectedVendor.BranchName }, JsonRequestBehavior.AllowGet);
                            return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = tblAssist.RefNo }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            //
                            tblAssist.UserID = user.Id;
                            //
                            //tblAssist.PinCode = objAssist.PinCode;
                            //tblAssist.Landmark = objAssist.Landmark;
                            //tblAssist.Issue = tblAssist.Issue;
                            //tblAssist.Address = objAssist.Address;
                            //tblAssist.StreetAddress = objAssist.StreetAddress;
                            tblAssist.CreatedDate = DateTime.Now;
                            //tblAssist.City = objAssist.City;
                            //tblAssist.Location = objAssist.location;
                            //tblAssist.State = objAssist.State;
                            //tblAssist.Address = objAssist.Address;
                            // tblAssist.IncidentDetailid = objAssist.IncidentDetailId;                        
                            //
                            tblAssist.Remark1 = objAssist.RemarkId;
                            //
                            if (string.IsNullOrEmpty(objAssist.AssistSummary))
                            {
                                string CurrDateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                tblAssist.SummaryDetails = tblAssist.SummaryDetails + " ( " + CurrDateTime + " ) " + "N/A";
                            }
                            else
                            {
                                string CurrDateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                tblAssist.SummaryDetails = tblAssist.SummaryDetails + " ( " + CurrDateTime + " ) " + objAssist.AssistSummary;
                            }
                            //

                            //decimal VendorID = Convert.ToDecimal(tblAssist.VendorId);
                            //tblAssist.VendorId = VendorID;       

                            DateTime? VendorReachedIncidentLocationDateTime = null;
                            if (!string.IsNullOrEmpty(objAssist.strVendorReachedIncidentLocationDateTime))
                            {
                                VendorReachedIncidentLocationDateTime = DateTime.ParseExact(objAssist.strVendorReachedIncidentLocationDateTime, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                string IncDate = Convert.ToDateTime(VendorReachedIncidentLocationDateTime).ToString("yyyy-MM-dd");
                                //string IncTime = DateTime.Now.ToString("HH:mm:ss");
                                string IncHours = objAssist.VendorReachedIncidentLocationHours;
                                string IncMins = objAssist.VendorReachedIncidentLocationMins;
                                string IncSecs = objAssist.VendorReachedIncidentLocationSecs;
                                string IncTime = IncHours + ":" + IncMins + ":" + IncSecs;
                                string strIncDateTime = IncDate + " " + IncTime;
                                DateTime dtIncDateTime = Convert.ToDateTime(strIncDateTime);

                                tblAssist.VenReachedIncLocDateTime = dtIncDateTime;
                            }
                            else
                            {
                                VendorReachedIncidentLocationDateTime = tblAssist.VenReachedIncLocDateTime;
                                tblAssist.VenReachedIncLocDateTime = VendorReachedIncidentLocationDateTime;
                            }

                            DateTime? VendorReachedDropLocationDateTime = null;
                            if (!string.IsNullOrEmpty(objAssist.strVendorReachedDropLocationDateTime))
                            {
                                VendorReachedDropLocationDateTime = DateTime.ParseExact(objAssist.strVendorReachedDropLocationDateTime, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                string DropDate = Convert.ToDateTime(VendorReachedDropLocationDateTime).ToString("yyyy-MM-dd");
                                //string DropTime = DateTime.Now.ToString("HH:mm:ss");
                                string DropHours = objAssist.VendorReachedDropLocationHours;
                                string DropMins = objAssist.VendorReachedDropLocationMins;
                                string DropSecs = objAssist.VendorReachedDropLocationSecs;
                                string DropTime = DropHours + ":" + DropMins + ":" + DropSecs;
                                string strDropDateTime = DropDate + " " + DropTime;
                                DateTime dtDropDateTime = Convert.ToDateTime(strDropDateTime);

                                tblAssist.VenReachedDropLocDateTime = dtDropDateTime;
                            }
                            else
                            {
                                VendorReachedDropLocationDateTime = tblAssist.VenReachedDropLocDateTime;
                                tblAssist.VenReachedDropLocDateTime = VendorReachedDropLocationDateTime;
                            }

                            //
                            if (objAssist.StsId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue && tblAssist.StatusId != CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                            {
                                tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Closed_Issue;
                            }
                            else if (objAssist.StsId == 0 && tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                            {
                                tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                                //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            }
                            else if (objAssist.StsId == 0 && tblAssist.StatusId != CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                            {

                            }
                            else if (objAssist.StsId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue && tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                            {
                                tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Closed_Issue;
                                //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            }
                            else
                            {
                                tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                                //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            }

                            if (objAssist.StsId != 0 && tblAssist.StatusId != CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
                            {
                                //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            }

                            db.SaveChanges();

                            var branchDetails = db.tblVendorBranchDetails.Where(w => w.BranchID == tblAssist.VendorId).FirstOrDefault();
                            if (branchDetails != null)
                            {
                                var vendorDetails = db.tblMstVendors.Where(w => w.VendorID == branchDetails.VendorID).FirstOrDefault();
                                if (vendorDetails != null)
                                {
                                    var vendorUserDetails = db.AspNetUsers.Where(w => w.Email == vendorDetails.VendorEMail).FirstOrDefault();
                                    if (vendorUserDetails != null)
                                    {
                                        string VendorUserId = vendorUserDetails.Id;

                                        tblAsstVendorUserLocation = db.tblAsstVendorUserLocation.Where(w => w.ReferenceNo == tblAssist.RefNo).FirstOrDefault();
                                        if (tblAsstVendorUserLocation != null)
                                        {
                                            tblAsstVendorUserLocation.VendorId = VendorUserId;
                                        }
                                        db.SaveChanges();
                                    }
                                }
                            }

                            if (objAssist.StsId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue && tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
                            {
                                int dbAssistCompanyID = Convert.ToInt32(tblAssist.AssistCompanyID);
                                if (dbAssistCompanyID == ICICILombard)
                                {
                                    #region Send ICICILombard SMS Logic Part :: Case Completion

                                    string strIsSendSMS_CaseCompletion = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCompletionSMS_ICICILombard").Select(s => s.Description).FirstOrDefault();
                                    int IsSendSMS_CaseCompletion = (!string.IsNullOrEmpty(strIsSendSMS_CaseCompletion)) ? (strIsSendSMS_CaseCompletion == "1") ? 1 : 0 : 0;
                                    if (IsSendSMS_CaseCompletion == 1)
                                    {
                                        int RemarkId = 0;
                                        int.TryParse(tblAssist.Remark1, out RemarkId);

                                        string dbRemark = db.tblMstCommonTypes.AsNoTracking().Where(w => w.CommonTypeID == RemarkId).Select(s => s.Description.ToUpper()).FirstOrDefault();
                                        if (!string.IsNullOrEmpty(dbRemark))
                                        {
                                            if (dbRemark.Equals("CASE COMPLETED"))
                                            {
                                                string CustomerMobileNo = tblAssist.Issue;
                                                if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                {
                                                    string AssistanceReferenceNo = tblAssist.RefNo;
                                                    Message = AssistanceCompletionToCustomerBody + AssistanceReferenceNo;

                                                    MsgSent = _c.SendingICICILSMSMessage(tblAssist, CustomerMobileNo, Message);

                                                    if (MsgSent.Equals(true))
                                                    {
                                                        // Message Send. :: Success
                                                    }
                                                    else
                                                    {
                                                        // Message Not Send. :: Failed
                                                    }
                                                }
                                            }
                                        }

                                        string VendorMobileNo = data.VendorMobileNo;
                                        if (!string.IsNullOrEmpty(VendorMobileNo))
                                        {
                                            string AssistanceReferenceNo = tblAssist.RefNo;
                                            Message = AssistanceCompletionToVendorBody + AssistanceReferenceNo;

                                            MsgSent = _c.SendingICICILSMSMessage(tblAssist, VendorMobileNo, Message);

                                            if (MsgSent.Equals(true))
                                            {
                                                // Message Send. :: Success
                                            }
                                            else
                                            {
                                                // Message Not Send. :: Failed
                                            }
                                        }

                                        foreach (var AssistTeamMobileNo in AssistanceSendMsgMobileNos)
                                        {
                                            string AssistanceTeamMobileNo = AssistTeamMobileNo;
                                            if (!string.IsNullOrEmpty(AssistanceTeamMobileNo))
                                            {
                                                string AssistanceReferenceNo = tblAssist.RefNo;
                                                Message = AssistanceCompletionAssistanceTeamBody + AssistanceReferenceNo;

                                                MsgSent = _c.SendingICICILSMSMessage(tblAssist, AssistanceTeamMobileNo, Message);

                                                if (MsgSent.Equals(true))
                                                {
                                                    // Message Send. :: Success
                                                }
                                                else
                                                {
                                                    // Message Not Send. :: Failed
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Send SMS Logic Part :: Case Completion

                                    string strIsSendSMS_CaseCompletion = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCompletionSMS").Select(s => s.Description).FirstOrDefault();
                                    int IsSendSMS_CaseCompletion = (!string.IsNullOrEmpty(strIsSendSMS_CaseCompletion)) ? (strIsSendSMS_CaseCompletion == "1") ? 1 : 0 : 0;
                                    if (IsSendSMS_CaseCompletion == 1)
                                    {
                                        int RemarkId = 0;
                                        int.TryParse(tblAssist.Remark1, out RemarkId);

                                        string dbRemark = db.tblMstCommonTypes.AsNoTracking().Where(w => w.CommonTypeID == RemarkId).Select(s => s.Description.ToUpper()).FirstOrDefault();
                                        if (!string.IsNullOrEmpty(dbRemark))
                                        {
                                            if (dbRemark.Equals("CASE COMPLETED"))
                                            {
                                                string CustomerMobileNo = tblAssist.Issue;
                                                if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                {
                                                    string AssistanceReferenceNo = tblAssist.RefNo;
                                                    Message = AssistanceCompletionToCustomerBody + AssistanceReferenceNo;

                                                    MsgSent = _c.SendingSMSMessage(CustomerMobileNo, Message);

                                                    if (MsgSent.Equals(true))
                                                    {
                                                        // Message Send. :: Success
                                                    }
                                                    else
                                                    {
                                                        // Message Not Send. :: Failed
                                                    }
                                                }
                                            }
                                        }

                                        string VendorMobileNo = data.VendorMobileNo;
                                        if (!string.IsNullOrEmpty(VendorMobileNo))
                                        {
                                            string AssistanceReferenceNo = tblAssist.RefNo;
                                            Message = AssistanceCompletionToVendorBody + AssistanceReferenceNo;

                                            MsgSent = _c.SendingSMSMessage(VendorMobileNo, Message);

                                            if (MsgSent.Equals(true))
                                            {
                                                // Message Send. :: Success
                                            }
                                            else
                                            {
                                                // Message Not Send. :: Failed
                                            }
                                        }

                                        foreach (var AssistTeamMobileNo in AssistanceSendMsgMobileNos)
                                        {
                                            string AssistanceTeamMobileNo = AssistTeamMobileNo;
                                            if (!string.IsNullOrEmpty(AssistanceTeamMobileNo))
                                            {
                                                string AssistanceReferenceNo = tblAssist.RefNo;
                                                Message = AssistanceCompletionAssistanceTeamBody + AssistanceReferenceNo;

                                                MsgSent = _c.SendingSMSMessage(AssistanceTeamMobileNo, Message);

                                                if (MsgSent.Equals(true))
                                                {
                                                    // Message Send. :: Success
                                                }
                                                else
                                                {
                                                    // Message Not Send. :: Failed
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }

                            //return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = tblAssist.RefNo, Vendor = selectedVendor.tblMstVendor.VendorName, Branch = selectedVendor.BranchName }, JsonRequestBehavior.AllowGet);
                            return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = tblAssist.RefNo }, JsonRequestBehavior.AllowGet);
                        }                       
                    }
                    else // New assist
                    {
                        if (objAssist.TransactionID > 0)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(objAssist.CaseTypeID)) && !string.IsNullOrEmpty(Convert.ToString(objAssist.SubCategoryID)))
                            {
                                ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));

                                var assit = false;
                                if (assit == false)  // no record exists
                                {
                                    var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.AssistReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);

                                    var tbltrans = db.tblTransactions.Where(w => w.TransactionID == objAssist.TransactionID).FirstOrDefault();
                                    if (tbltrans != null)
                                    {
                                        if (tbltrans.Remark == CrossCutting_Constant.RemarkAssistanceDataEntry)
                                        {
                                            tblAssist.RefNo = objAssist.CertificateNo;
                                        }
                                        else
                                        {
                                            tblAssist.RefNo = Convert.ToString(OutputOfPaymentReference.Value);
                                        }
                                    }

                                    tblAssist.TransactionId = objAssist.TransactionID;
                                    tblAssist.Remarks = objAssist.Remarks;
                                    tblAssist.PinCode = objAssist.PinCode;
                                    tblAssist.Landmark = objAssist.Landmark;
                                    tblAssist.Issue = objAssist.Issue;
                                    //
                                    //tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                                    //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                                    tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Closed_Issue;
                                    //
                                    tblAssist.IsValid = true;
                                    tblAssist.Address = objAssist.Address;
                                    tblAssist.StreetAddress = objAssist.StreetAddress;
                                    tblAssist.CreatedDate = DateTime.Now;
                                    tblAssist.AssistCreatedDate = DateTime.Now;
                                    tblAssist.City = objAssist.City;
                                    tblAssist.Location = objAssist.location;
                                    tblAssist.State = objAssist.State;
                                    tblAssist.Address = objAssist.Address;
                                    tblAssist.IncidentDetailid = objAssist.IncidentDetailId;
                                    tblAssist.VehicleYear = objAssist.VehicleYear;
                                    tblAssist.VehicleColor = objAssist.VehicleColor;
                                    //
                                    tblAssist.Lat = Convert.ToString(objAssist.Latitude);
                                    tblAssist.Lon = Convert.ToString(objAssist.Longitude);
                                    //

                                    //tblVendorBranchDetail selectedVendor = getAllocatedVendor(isManual, objAssist.Latitude, objAssist.Longitude, branchid, isRelianceAssist);
                                    //tblAssist.VendorId = selectedVendor.BranchID;

                                    tblAssist.UserID = user == null ? null : user.Id;
                                    tblAssist.AssistCreatedBy = user == null ? null : user.Id;
                                    //
                                    tblAssist.SummaryDetails = objAssist.AssistSummary;
                                    //

                                    //
                                    tblAssist.AssistCompanyID = objAssist.hdnAssistCompanyID;
                                    //
                                    tblAssist.AssistOEMID = objAssist.hdnAssistOEMID;

                                    DateTime? RequestPlannedDateTime = null;
                                    if (objAssist.IsPlanned == 1 && !string.IsNullOrEmpty(objAssist.strRequestPlannedDateTime))
                                    {
                                        RequestPlannedDateTime = DateTime.ParseExact(objAssist.strRequestPlannedDateTime, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        string RequestTypeDate = Convert.ToDateTime(RequestPlannedDateTime).ToString("yyyy-MM-dd");
                                        //string RequestTypeTime = DateTime.Now.ToString("HH:mm:ss");
                                        string RequestTypeHours = objAssist.RequestPlannedDateTimeHours;
                                        string RequestTypeMins = objAssist.RequestPlannedDateTimeMins;
                                        string RequestTypeSecs = objAssist.RequestPlannedDateTimeSecs;
                                        string RequestTypeTime = RequestTypeHours + ":" + RequestTypeMins + ":" + RequestTypeSecs;
                                        string strRequestTypeDateTime = RequestTypeDate + " " + RequestTypeTime;
                                        DateTime dtRequestTypeDateTime = Convert.ToDateTime(strRequestTypeDateTime);

                                        tblAssist.IsPlanned = true;
                                        tblAssist.PlannedDateTime = dtRequestTypeDateTime;
                                    }
                                    else
                                    {
                                        tblAssist.IsPlanned = false;
                                        tblAssist.PlannedDateTime = null;
                                    }

                                    tblAssist.AssistCaseTypeID = objAssist.CaseTypeID;
                                    tblAssist.AssistSubCategoryID = objAssist.SubCategoryID;

                                    var result = db.tblAssistDetails.Add(tblAssist);
                                    db.SaveChanges();

                                    decimal AssistId = result.Id;
                                    if (AssistId > 0)
                                    {
                                        
                                    }

                                    if (tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
                                    {
                                        
                                    }

                                    return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = tblAssist.RefNo }, JsonRequestBehavior.AllowGet);
                                }
                                else // Request already submitted
                                {
                                    return Json(new { status = "AssistExist", AssistRefNo = tblAssist.RefNo }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                ObjectParameter OutputOfPaymentReference = new ObjectParameter("UniqueCode", typeof(string));

                                var assit = db.tblAssistDetails.Where(x => x.TransactionId == objAssist.TransactionID &&
                                x.CreatedDate >= DateTime.Today && x.IncidentDetailid == objAssist.IncidentDetailId && x.StatusId != CrossCutting_Constant.StatusID_Vendor_Closed_Issue).Any();

                                if (assit == false)  // no record exists
                                {
                                    var UniqueCodeNoOfPaymentRef = db.usp_UniqeNumberGeneration(CrossCutting_Constant.AssistReferencePrefix, "", "", "", "", "", OutputOfPaymentReference);

                                    var tbltrans = db.tblTransactions.Where(w => w.TransactionID == objAssist.TransactionID).FirstOrDefault();
                                    if (tbltrans != null)
                                    {
                                        if (tbltrans.Remark == CrossCutting_Constant.RemarkAssistanceDataEntry)
                                        {
                                            tblAssist.RefNo = objAssist.CertificateNo;
                                        }
                                        else
                                        {
                                            tblAssist.RefNo = Convert.ToString(OutputOfPaymentReference.Value);
                                        }
                                    }

                                    tblAssist.TransactionId = objAssist.TransactionID;
                                    tblAssist.Remarks = objAssist.Remarks;
                                    tblAssist.PinCode = objAssist.PinCode;
                                    tblAssist.Landmark = objAssist.Landmark;
                                    tblAssist.Issue = objAssist.Issue;
                                    //
                                    //tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                                    //tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                                    tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Not_Assign;
                                    //
                                    tblAssist.IsValid = true;
                                    tblAssist.Address = objAssist.Address;
                                    tblAssist.StreetAddress = objAssist.StreetAddress;
                                    tblAssist.CreatedDate = DateTime.Now;
                                    tblAssist.AssistCreatedDate = DateTime.Now;
                                    tblAssist.City = objAssist.City;
                                    tblAssist.Location = objAssist.location;
                                    tblAssist.State = objAssist.State;
                                    tblAssist.Address = objAssist.Address;
                                    tblAssist.IncidentDetailid = objAssist.IncidentDetailId;
                                    tblAssist.VehicleYear = objAssist.VehicleYear;
                                    tblAssist.VehicleColor = objAssist.VehicleColor;
                                    //
                                    tblAssist.Lat = Convert.ToString(objAssist.Latitude);
                                    tblAssist.Lon = Convert.ToString(objAssist.Longitude);
                                    //

                                    //tblVendorBranchDetail selectedVendor = getAllocatedVendor(isManual, objAssist.Latitude, objAssist.Longitude, branchid, isRelianceAssist);
                                    //tblAssist.VendorId = selectedVendor.BranchID;

                                    tblAssist.UserID = user == null ? null : user.Id;
                                    tblAssist.AssistCreatedBy = user == null ? null : user.Id;
                                    //
                                    tblAssist.SummaryDetails = objAssist.AssistSummary;
                                    //

                                    //
                                    tblAssist.AssistCompanyID = objAssist.hdnAssistCompanyID;
                                    //
                                    tblAssist.AssistOEMID = objAssist.hdnAssistOEMID;

                                    DateTime? RequestPlannedDateTime = null;
                                    if (objAssist.IsPlanned == 1 && !string.IsNullOrEmpty(objAssist.strRequestPlannedDateTime))
                                    {
                                        RequestPlannedDateTime = DateTime.ParseExact(objAssist.strRequestPlannedDateTime, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        string RequestTypeDate = Convert.ToDateTime(RequestPlannedDateTime).ToString("yyyy-MM-dd");
                                        //string RequestTypeTime = DateTime.Now.ToString("HH:mm:ss");
                                        string RequestTypeHours = objAssist.RequestPlannedDateTimeHours;
                                        string RequestTypeMins = objAssist.RequestPlannedDateTimeMins;
                                        string RequestTypeSecs = objAssist.RequestPlannedDateTimeSecs;
                                        string RequestTypeTime = RequestTypeHours + ":" + RequestTypeMins + ":" + RequestTypeSecs;
                                        string strRequestTypeDateTime = RequestTypeDate + " " + RequestTypeTime;
                                        DateTime dtRequestTypeDateTime = Convert.ToDateTime(strRequestTypeDateTime);

                                        tblAssist.IsPlanned = true;
                                        tblAssist.PlannedDateTime = dtRequestTypeDateTime;
                                    }
                                    else
                                    {
                                        tblAssist.IsPlanned = false;
                                        tblAssist.PlannedDateTime = null;
                                    }

                                    var result = db.tblAssistDetails.Add(tblAssist);
                                    db.SaveChanges();

                                    decimal AssistId = result.Id;
                                    if (AssistId > 0)
                                    {
                                        int dbAssistCompanyID = Convert.ToInt32(tblAssist.AssistCompanyID);
                                        if (dbAssistCompanyID == ICICILombard)
                                        {
                                            #region Send ICICILombard SMS Logic Part :: Case Creation

                                            string strIsSendSMS_CaseCreation = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCreationSMS_ICICILombard").Select(s => s.Description).FirstOrDefault();
                                            int IsSendSMS_CaseCreation = (!string.IsNullOrEmpty(strIsSendSMS_CaseCreation)) ? (strIsSendSMS_CaseCreation == "1") ? 1 : 0 : 0;
                                            if (IsSendSMS_CaseCreation == 1)
                                            {
                                                _c.SendingAssistanceCreatedICICILSMSMessage(tblAssist);
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Send SMS Logic Part :: Case Creation

                                            string strIsSendSMS_CaseCreation = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCreationSMS").Select(s => s.Description).FirstOrDefault();
                                            int IsSendSMS_CaseCreation = (!string.IsNullOrEmpty(strIsSendSMS_CaseCreation)) ? (strIsSendSMS_CaseCreation == "1") ? 1 : 0 : 0;
                                            if (IsSendSMS_CaseCreation == 1)
                                            {
                                                _c.SendingAssistanceCreatedSMSMessage(AssistId);
                                            }
                                            #endregion
                                        }

                                        _c.SendingAssistanceCreatedMail(AssistId);
                                    }

                                    if (tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
                                    {
                                        string CustomerMobileNo = tblAssist.Issue;
                                        string AssistanceReferenceNo = tblAssist.RefNo;

                                        int dbAssistCompanyID = Convert.ToInt32(tblAssist.AssistCompanyID);
                                        if (dbAssistCompanyID == ICICILombard)
                                        {
                                            #region Send ICICILombard SMS Logic Part :: Case Creation

                                            string strIsSendSMS_CaseCreation = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCreationSMS_ICICILombard").Select(s => s.Description).FirstOrDefault();
                                            int IsSendSMS_CaseCreation = (!string.IsNullOrEmpty(strIsSendSMS_CaseCreation)) ? (strIsSendSMS_CaseCreation == "1") ? 1 : 0 : 0;
                                            if (IsSendSMS_CaseCreation == 1)
                                            {
                                                if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                {
                                                    Message = AssistanceCaseCreatedToCustomerBody + AssistanceReferenceNo;

                                                    MsgSent = _c.SendingICICILSMSMessage(tblAssist, CustomerMobileNo, Message);

                                                    if (MsgSent.Equals(true))
                                                    {
                                                        // Message Send. :: Success
                                                    }
                                                    else
                                                    {
                                                        // Message Not Send. :: Failed
                                                    }
                                                }
                                            }
                                            #endregion

                                            #region Send ICICILombard SMS Logic Part :: Customer Incident Location

                                            string strIsSendSMS_CustomerIncidentLocation = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCustIncidentLocSMS_ICICILombard").Select(s => s.Description).FirstOrDefault();
                                            int IsSendSMS_CustomerIncidentLocation = (!string.IsNullOrEmpty(strIsSendSMS_CustomerIncidentLocation)) ? (strIsSendSMS_CustomerIncidentLocation == "1") ? 1 : 0 : 0;
                                            if (IsSendSMS_CustomerIncidentLocation == 1)
                                            {
                                                #region Customer Location Details Send on ICICILombard SMS Logic Part

                                                string AssistIncidentLocationURLToCustomerBody = ConfigurationManager.AppSettings["AssistIncidentLocationURLToCustomerBody"];
                                                string GlobalAssureHelpURL = ConfigurationManager.AppSettings["GlobalAssureHelpURL"];
                                                string EncKey = ConfigurationManager.AppSettings["HelpEncKey"];
                                                string TokenNo = GlobalAssureEncryption.EncryptText(AssistanceReferenceNo, EncKey);
                                                if (!string.IsNullOrEmpty(TokenNo))
                                                {
                                                    string Long_URL = GlobalAssureHelpURL + HttpUtility.UrlEncode(TokenNo);

                                                    string mesage = string.Empty;
                                                    string Short_URL = BitlyShorten(ConfigurationManager.AppSettings["Bitly_Service_URL"], ConfigurationManager.AppSettings["Bitly_Access_Token"], Long_URL, ref mesage);
                                                    if (!string.IsNullOrEmpty(Short_URL) && string.IsNullOrEmpty(mesage))
                                                    {
                                                        if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                        {
                                                            Message = AssistIncidentLocationURLToCustomerBody + Short_URL;

                                                            MsgSent = _c.SendingICICILSMSMessage(tblAssist, CustomerMobileNo, Message);
                                                            if (MsgSent.Equals(true))
                                                            {
                                                                // Message Send. :: Success
                                                            }
                                                            else
                                                            {
                                                                // Message Not Send. :: Failed
                                                            }
                                                        }
                                                    }
                                                }

                                                #endregion
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Send SMS Logic Part :: Case Creation

                                            string strIsSendSMS_CaseCreation = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCreationSMS").Select(s => s.Description).FirstOrDefault();
                                            int IsSendSMS_CaseCreation = (!string.IsNullOrEmpty(strIsSendSMS_CaseCreation)) ? (strIsSendSMS_CaseCreation == "1") ? 1 : 0 : 0;
                                            if (IsSendSMS_CaseCreation == 1)
                                            {
                                                if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                {
                                                    Message = AssistanceCaseCreatedToCustomerBody + AssistanceReferenceNo;

                                                    MsgSent = _c.SendingSMSMessage(CustomerMobileNo, Message);

                                                    if (MsgSent.Equals(true))
                                                    {
                                                        // Message Send. :: Success
                                                    }
                                                    else
                                                    {
                                                        // Message Not Send. :: Failed
                                                    }
                                                }
                                            }
                                            #endregion

                                            #region Send SMS Logic Part :: Customer Incident Location

                                            string strIsSendSMS_CustomerIncidentLocation = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistCustIncidentLocSMS").Select(s => s.Description).FirstOrDefault();
                                            int IsSendSMS_CustomerIncidentLocation = (!string.IsNullOrEmpty(strIsSendSMS_CustomerIncidentLocation)) ? (strIsSendSMS_CustomerIncidentLocation == "1") ? 1 : 0 : 0;
                                            if (IsSendSMS_CustomerIncidentLocation == 1)
                                            {
                                                #region Customer Location Details Send on SMS Logic Part

                                                string AssistIncidentLocationURLToCustomerBody = ConfigurationManager.AppSettings["AssistIncidentLocationURLToCustomerBody"];
                                                string GlobalAssureHelpURL = ConfigurationManager.AppSettings["GlobalAssureHelpURL"];
                                                string EncKey = ConfigurationManager.AppSettings["HelpEncKey"];
                                                string TokenNo = GlobalAssureEncryption.EncryptText(AssistanceReferenceNo, EncKey);
                                                if (!string.IsNullOrEmpty(TokenNo))
                                                {
                                                    string Long_URL = GlobalAssureHelpURL + HttpUtility.UrlEncode(TokenNo);

                                                    string mesage = string.Empty;
                                                    string Short_URL = BitlyShorten(ConfigurationManager.AppSettings["Bitly_Service_URL"], ConfigurationManager.AppSettings["Bitly_Access_Token"], Long_URL, ref mesage);
                                                    if (!string.IsNullOrEmpty(Short_URL) && string.IsNullOrEmpty(mesage))
                                                    {
                                                        if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                        {
                                                            Message = AssistIncidentLocationURLToCustomerBody + Short_URL;

                                                            MsgSent = _c.SendingSMSMessage(CustomerMobileNo, Message);
                                                            if (MsgSent.Equals(true))
                                                            {
                                                                // Message Send. :: Success
                                                            }
                                                            else
                                                            {
                                                                // Message Not Send. :: Failed
                                                            }
                                                        }
                                                    }
                                                }

                                                #endregion
                                            }
                                            #endregion
                                        }
                                    }

                                    return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = tblAssist.RefNo }, JsonRequestBehavior.AllowGet);
                                }
                                else // Request already submitted
                                {
                                    return Json(new { status = "AssistExist", AssistRefNo = tblAssist.RefNo }, JsonRequestBehavior.AllowGet);
                                }
                            }                            
                        }
                    }
                }

                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, AssistRefNo = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        //ee 1006

        #region SaveAssignedVendorDetails
        public JsonResult SaveAssignedVendorDetails(VendorForAssistModel VendorForAssistModelObj)
        {
            string strICICILombard = Convert.ToString(ConfigurationManager.AppSettings["ICICILombard"]);
            int ICICILombard = 0;
            int.TryParse(strICICILombard, out ICICILombard);

            tblAssistDetail tblAssist = new tblAssistDetail();
            string VendorName = string.Empty;
            string BranchName = string.Empty;

            string AssistanceAssignToVendorBody = ConfigurationManager.AppSettings["AssistanceAssignToVendorBody"];
            string AssistanceAssignToCustomerBody = ConfigurationManager.AppSettings["AssistanceAssignToCustomerBody"];
            string[] AssistanceSendMsgMobileNos = ConfigurationManager.AppSettings["AssistanceSendMsgMobileNos"].Split(',');
            string AssistanceAssignToVendorAssistanceTeamBody = ConfigurationManager.AppSettings["AssistanceAssignToVendorAssistanceTeamBody"];
            string Message = string.Empty;
            bool MsgSent = false;

            try
            {
                string UserName = HttpContext.User.Identity.Name;
                var user = db.AspNetUsers.Where(x => x.Email == UserName).FirstOrDefault();

                if (VendorForAssistModelObj != null)
                {
                    decimal VendorBranchId = VendorForAssistModelObj.VendorForAssistModelList.Where(w => w.IsChecked == true)
                                             .Select(s => s.VendorBranchID).FirstOrDefault();

                    var data = (from vbd in db.tblVendorBranchDetails.AsNoTracking()
                                join v in db.tblMstVendors.AsNoTracking() on vbd.VendorID equals v.VendorID into objVendorBased
                                from objVendor in objVendorBased.DefaultIfEmpty()
                                where vbd.BranchID == VendorBranchId
                                select new 
                                {
                                    VendorName = objVendor.VendorName,
                                    BranchName = vbd.BranchName,
                                    VendorMobileNo = objVendor.VendorMobileNo

                                }).FirstOrDefault();

                    if (VendorForAssistModelObj.AssistID > 0) // Vendor Assign
                    {
                        tblAssist = db.tblAssistDetails.Where(x => x.Id == VendorForAssistModelObj.AssistID).FirstOrDefault();
                        if (tblAssist != null)
                        {
                            tblAssist.UserID = user.Id;
                            tblAssist.CreatedDate = DateTime.Now;

                            if (string.IsNullOrEmpty(VendorForAssistModelObj.AssistSummary))
                            {
                                string CurrDateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                tblAssist.SummaryDetails = tblAssist.SummaryDetails + " ( " + CurrDateTime + " ) " + "N/A";
                            }
                            else
                            {
                                string CurrDateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                                tblAssist.SummaryDetails = tblAssist.SummaryDetails + " ( " + CurrDateTime + " ) " + VendorForAssistModelObj.AssistSummary;
                            }
                            
                            tblAssist.Remark1 = VendorForAssistModelObj.RemarkID;
                            tblAssist.StatusId = CrossCutting_Constant.StatusID_Vendor_Assigned;
                            tblAssist.ActionId = CrossCutting_Constant.ActionID_Vendor_No_Action;
                            tblAssist.VendorId = VendorBranchId;
                            tblAssist.VendorAssignedDateTime = DateTime.Now;

                            db.SaveChanges();

                            VendorName = data.VendorName;
                            BranchName = data.BranchName;

                            if (tblAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Assigned)
                            {
                                string CustomerMobileNo = tblAssist.Issue;
                                string AssistanceReferenceNo = tblAssist.RefNo;

                                int dbAssistCompanyID = Convert.ToInt32(tblAssist.AssistCompanyID);
                                if (dbAssistCompanyID == ICICILombard)
                                {
                                    #region Send ICICILombard SMS Logic Part :: Case Vendor Assignment

                                    string strIsSendSMS_CaseVendorAssignment = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistAssignmentSMS_ICICILombard").Select(s => s.Description).FirstOrDefault();
                                    int IsSendSMS_CaseVendorAssignment = (!string.IsNullOrEmpty(strIsSendSMS_CaseVendorAssignment)) ? (strIsSendSMS_CaseVendorAssignment == "1") ? 1 : 0 : 0;
                                    if (IsSendSMS_CaseVendorAssignment == 1)
                                    {
                                        if (!string.IsNullOrEmpty(CustomerMobileNo))
                                        {
                                            AssistanceReferenceNo = tblAssist.RefNo;
                                            Message = AssistanceAssignToCustomerBody + AssistanceReferenceNo;

                                            MsgSent = _c.SendingICICILSMSMessage(tblAssist, CustomerMobileNo, Message);

                                            if (MsgSent.Equals(true))
                                            {
                                                // Message Send. :: Success
                                            }
                                            else
                                            {
                                                // Message Not Send. :: Failed
                                            }
                                        }

                                        string VendorMobileNo = data.VendorMobileNo;
                                        if (!string.IsNullOrEmpty(VendorMobileNo))
                                        {
                                            AssistanceReferenceNo = tblAssist.RefNo;
                                            Message = AssistanceAssignToVendorBody + AssistanceReferenceNo;

                                            MsgSent = _c.SendingICICILSMSMessage(tblAssist, VendorMobileNo, Message);

                                            if (MsgSent.Equals(true))
                                            {
                                                // Message Send. :: Success
                                            }
                                            else
                                            {
                                                // Message Not Send. :: Failed
                                            }
                                        }

                                        foreach (var AssistTeamMobileNo in AssistanceSendMsgMobileNos)
                                        {
                                            string AssistanceTeamMobileNo = AssistTeamMobileNo;
                                            if (!string.IsNullOrEmpty(AssistanceTeamMobileNo))
                                            {
                                                AssistanceReferenceNo = tblAssist.RefNo;
                                                Message = AssistanceAssignToVendorAssistanceTeamBody + AssistanceReferenceNo;

                                                MsgSent = _c.SendingICICILSMSMessage(tblAssist, AssistanceTeamMobileNo, Message);

                                                if (MsgSent.Equals(true))
                                                {
                                                    // Message Send. :: Success
                                                }
                                                else
                                                {
                                                    // Message Not Send. :: Failed
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Send ICICILombard SMS Logic Part :: Live Location Tracking

                                    string strIsSendSMS_LiveLocationTracking = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistLiveLocTrackingSMS_ICICILombard").Select(s => s.Description).FirstOrDefault();
                                    int IsSendSMS_LiveLocationTracking = (!string.IsNullOrEmpty(strIsSendSMS_LiveLocationTracking)) ? (strIsSendSMS_LiveLocationTracking == "1") ? 1 : 0 : 0;
                                    if (IsSendSMS_LiveLocationTracking == 1)
                                    {
                                        #region Vendor Location Send To Customer and Customer Location Send To Vendor on ICICILombard SMS Logic Part

                                        string AssistVendorLocationLinkToCustomerBody = ConfigurationManager.AppSettings["AssistVendorLocationLinkToCustomerBody"];
                                        string AssistCustomerLocationLinkToVendorBody = ConfigurationManager.AppSettings["AssistCustomerLocationLinkToVendorBody"];
                                        string GlobalAssureHelpURL = ConfigurationManager.AppSettings["GlobalAssureHelpURL"];
                                        string EncKey = ConfigurationManager.AppSettings["HelpEncKey"];

                                        #region Vendor Location Send To Customer on ICICILombard SMS Logic Part
                                        if (!string.IsNullOrEmpty(CustomerMobileNo))
                                        {
                                            string AssistanceReferenceNo_Plus_Type = AssistanceReferenceNo + "&Customer";
                                            string TokenNo = GlobalAssureEncryption.EncryptText(AssistanceReferenceNo_Plus_Type, EncKey);
                                            if (!string.IsNullOrEmpty(TokenNo))
                                            {
                                                string Long_URL = GlobalAssureHelpURL + HttpUtility.UrlEncode(TokenNo);

                                                string mesage = string.Empty;
                                                string Short_URL = BitlyShorten(ConfigurationManager.AppSettings["Bitly_Service_URL"], ConfigurationManager.AppSettings["Bitly_Access_Token"], Long_URL, ref mesage);
                                                if (!string.IsNullOrEmpty(Short_URL) && string.IsNullOrEmpty(mesage))
                                                {
                                                    if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                    {
                                                        Message = AssistVendorLocationLinkToCustomerBody + Short_URL;

                                                        MsgSent = _c.SendingICICILSMSMessage(tblAssist, CustomerMobileNo, Message);
                                                        if (MsgSent.Equals(true))
                                                        {
                                                            // Message Send. :: Success
                                                        }
                                                        else
                                                        {
                                                            // Message Not Send. :: Failed
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Customer Location Send To Vendor on ICICILombard SMS Logic Part
                                        string VendorMobNo = VendorForAssistModelObj.VendorMobileNo;
                                        if (!string.IsNullOrEmpty(VendorMobNo))
                                        {
                                            string AssistanceReferenceNo_Plus_Type = AssistanceReferenceNo + "&Vendor";
                                            string TokenNo = GlobalAssureEncryption.EncryptText(AssistanceReferenceNo_Plus_Type, EncKey);
                                            if (!string.IsNullOrEmpty(TokenNo))
                                            {
                                                string Long_URL = GlobalAssureHelpURL + HttpUtility.UrlEncode(TokenNo);

                                                string mesage = string.Empty;
                                                string Short_URL = BitlyShorten(ConfigurationManager.AppSettings["Bitly_Service_URL"], ConfigurationManager.AppSettings["Bitly_Access_Token"], Long_URL, ref mesage);
                                                if (!string.IsNullOrEmpty(Short_URL) && string.IsNullOrEmpty(mesage))
                                                {
                                                    if (!string.IsNullOrEmpty(VendorMobNo))
                                                    {
                                                        Message = AssistCustomerLocationLinkToVendorBody + Short_URL;

                                                        MsgSent = _c.SendingICICILSMSMessage(tblAssist, VendorMobNo, Message);
                                                        if (MsgSent.Equals(true))
                                                        {
                                                            // Message Send. :: Success
                                                        }
                                                        else
                                                        {
                                                            // Message Not Send. :: Failed
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #endregion
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Send SMS Logic Part :: Case Vendor Assignment

                                    string strIsSendSMS_CaseVendorAssignment = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistAssignmentSMS").Select(s => s.Description).FirstOrDefault();
                                    int IsSendSMS_CaseVendorAssignment = (!string.IsNullOrEmpty(strIsSendSMS_CaseVendorAssignment)) ? (strIsSendSMS_CaseVendorAssignment == "1") ? 1 : 0 : 0;
                                    if (IsSendSMS_CaseVendorAssignment == 1)
                                    {
                                        if (!string.IsNullOrEmpty(CustomerMobileNo))
                                        {
                                            AssistanceReferenceNo = tblAssist.RefNo;
                                            Message = AssistanceAssignToCustomerBody + AssistanceReferenceNo;

                                            MsgSent = _c.SendingSMSMessage(CustomerMobileNo, Message);

                                            if (MsgSent.Equals(true))
                                            {
                                                // Message Send. :: Success
                                            }
                                            else
                                            {
                                                // Message Not Send. :: Failed
                                            }
                                        }

                                        string VendorMobileNo = data.VendorMobileNo;
                                        if (!string.IsNullOrEmpty(VendorMobileNo))
                                        {
                                            AssistanceReferenceNo = tblAssist.RefNo;
                                            Message = AssistanceAssignToVendorBody + AssistanceReferenceNo;

                                            MsgSent = _c.SendingSMSMessage(VendorMobileNo, Message);

                                            if (MsgSent.Equals(true))
                                            {
                                                // Message Send. :: Success
                                            }
                                            else
                                            {
                                                // Message Not Send. :: Failed
                                            }
                                        }

                                        foreach (var AssistTeamMobileNo in AssistanceSendMsgMobileNos)
                                        {
                                            string AssistanceTeamMobileNo = AssistTeamMobileNo;
                                            if (!string.IsNullOrEmpty(AssistanceTeamMobileNo))
                                            {
                                                AssistanceReferenceNo = tblAssist.RefNo;
                                                Message = AssistanceAssignToVendorAssistanceTeamBody + AssistanceReferenceNo;

                                                MsgSent = _c.SendingSMSMessage(AssistanceTeamMobileNo, Message);

                                                if (MsgSent.Equals(true))
                                                {
                                                    // Message Send. :: Success
                                                }
                                                else
                                                {
                                                    // Message Not Send. :: Failed
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Send SMS Logic Part :: Live Location Tracking

                                    string strIsSendSMS_LiveLocationTracking = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "AssistLiveLocTrackingSMS").Select(s => s.Description).FirstOrDefault();
                                    int IsSendSMS_LiveLocationTracking = (!string.IsNullOrEmpty(strIsSendSMS_LiveLocationTracking)) ? (strIsSendSMS_LiveLocationTracking == "1") ? 1 : 0 : 0;
                                    if (IsSendSMS_LiveLocationTracking == 1)
                                    {
                                        #region Vendor Location Send To Customer and Customer Location Send To Vendor on SMS Logic Part

                                        string AssistVendorLocationLinkToCustomerBody = ConfigurationManager.AppSettings["AssistVendorLocationLinkToCustomerBody"];
                                        string AssistCustomerLocationLinkToVendorBody = ConfigurationManager.AppSettings["AssistCustomerLocationLinkToVendorBody"];
                                        string GlobalAssureHelpURL = ConfigurationManager.AppSettings["GlobalAssureHelpURL"];
                                        string EncKey = ConfigurationManager.AppSettings["HelpEncKey"];

                                        #region Vendor Location Send To Customer on SMS Logic Part
                                        if (!string.IsNullOrEmpty(CustomerMobileNo))
                                        {
                                            string AssistanceReferenceNo_Plus_Type = AssistanceReferenceNo + "&Customer";
                                            string TokenNo = GlobalAssureEncryption.EncryptText(AssistanceReferenceNo_Plus_Type, EncKey);
                                            if (!string.IsNullOrEmpty(TokenNo))
                                            {
                                                string Long_URL = GlobalAssureHelpURL + HttpUtility.UrlEncode(TokenNo);

                                                string mesage = string.Empty;
                                                string Short_URL = BitlyShorten(ConfigurationManager.AppSettings["Bitly_Service_URL"], ConfigurationManager.AppSettings["Bitly_Access_Token"], Long_URL, ref mesage);
                                                if (!string.IsNullOrEmpty(Short_URL) && string.IsNullOrEmpty(mesage))
                                                {
                                                    if (!string.IsNullOrEmpty(CustomerMobileNo))
                                                    {
                                                        Message = AssistVendorLocationLinkToCustomerBody + Short_URL;

                                                        MsgSent = _c.SendingSMSMessage(CustomerMobileNo, Message);
                                                        if (MsgSent.Equals(true))
                                                        {
                                                            // Message Send. :: Success
                                                        }
                                                        else
                                                        {
                                                            // Message Not Send. :: Failed
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Customer Location Send To Vendor on SMS Logic Part
                                        string VendorMobNo = VendorForAssistModelObj.VendorMobileNo;
                                        if (!string.IsNullOrEmpty(VendorMobNo))
                                        {
                                            string AssistanceReferenceNo_Plus_Type = AssistanceReferenceNo + "&Vendor";
                                            string TokenNo = GlobalAssureEncryption.EncryptText(AssistanceReferenceNo_Plus_Type, EncKey);
                                            if (!string.IsNullOrEmpty(TokenNo))
                                            {
                                                string Long_URL = GlobalAssureHelpURL + HttpUtility.UrlEncode(TokenNo);

                                                string mesage = string.Empty;
                                                string Short_URL = BitlyShorten(ConfigurationManager.AppSettings["Bitly_Service_URL"], ConfigurationManager.AppSettings["Bitly_Access_Token"], Long_URL, ref mesage);
                                                if (!string.IsNullOrEmpty(Short_URL) && string.IsNullOrEmpty(mesage))
                                                {
                                                    if (!string.IsNullOrEmpty(VendorMobNo))
                                                    {
                                                        Message = AssistCustomerLocationLinkToVendorBody + Short_URL;

                                                        MsgSent = _c.SendingSMSMessage(VendorMobNo, Message);
                                                        if (MsgSent.Equals(true))
                                                        {
                                                            // Message Send. :: Success
                                                        }
                                                        else
                                                        {
                                                            // Message Not Send. :: Failed
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #endregion
                                    }
                                    #endregion
                                }
                            }

                            return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = tblAssist.RefNo, Vendor = VendorName, Branch = BranchName }, JsonRequestBehavior.AllowGet);
                        }

                        return Json(new { status = CrossCutting_Constant.Failure, errormessage = "AssistID Not Found" }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = "AssistID In Model NULL" }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = CrossCutting_Constant.Failure, errormessage = "Vendor Model NULL" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrorMessage = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region GetIndentByIncidentType
        public ActionResult GetIndentByIncidentType(int typeId)
        {
            try
            {
                List<SelectListItem> lstIncident = new List<SelectListItem>();

                lstIncident = db.tblMstIncidentDetails.Where(x => x.IncidentDetailid == typeId && x.IsValid == true).
                     Select(x => new SelectListItem { Text = x.IncidentDetail, Value = x.Detailid.ToString() }).ToList();

                var jsonResult = Json(new
                {
                    Incidentlist = lstIncident,
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetBranchByVendorName
        public ActionResult GetBranchByVendorName(int VendorID)
        {
            try
            {
                List<SelectListItem> lstBranchName = new List<SelectListItem>();

                lstBranchName = db.tblVendorBranchDetails.Where(x => x.VendorID == VendorID && x.Status == true).
                     Select(x => new SelectListItem { Text = x.BranchName, Value = x.BranchID.ToString() }).ToList();

                var jsonResult = Json(new
                {
                    BranchNameList = lstBranchName,
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        #region GetRemarkByStatus
        public ActionResult GetRemarkByStatus(int StatusID)
        {
            try
            {
                List<SelectListItem> lstRemark = new List<SelectListItem>();

                lstRemark = db.tblMstCommonTypes.Where(w => w.MasterType == "CaseClosureRemark" && w.IsValid != false).
                     Select(s => new SelectListItem { Text = s.Description, Value = s.CommonTypeID.ToString() }).ToList();

                var jsonResult = Json(new
                {
                    RemarkList = lstRemark,
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion

        public ActionResult AssistList()
        {

            ViewBag.StatusLst = db.tblMstWorkFlowStatus.Where(x => x.Module == 1 && x.IsValid == true).Select(x => new SelectListItem { Value = x.StatusID + "", Text = x.Status }).ToList();  //Module-  1.status  2. Action
            return View();
        }

        #region SearchAssist
        public JsonResult SearchAssist(string Registration, string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string CertificateIssueDateFrom, string CertificateIssueDateTo , int statusId,string EmailID)
        {
            try
            {
                DateTime? FromDate = null;
                DateTime? ToDate = null;

                if (!string.IsNullOrEmpty(CertificateIssueDateFrom))
                    FromDate = DateTime.ParseExact(CertificateIssueDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(CertificateIssueDateTo))
                    ToDate = DateTime.ParseExact(CertificateIssueDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                List<AssistVendroDetail> list = new List<AssistVendroDetail>();

                ViewBag.Registration = Registration;
                ViewBag.RefNo = RefNo;
                ViewBag.FirstName = FirstName;
                ViewBag.LastName = LastName;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.MobileNo = MobileNo;
                ViewBag.CertificateIssueDateFrom = CertificateIssueDateFrom;
                ViewBag.CertificateIssueDateTo = CertificateIssueDateTo;
                ViewBag.Status = statusId;
                ViewBag.Email = EmailID;

                TempData["Load"] = "FirstTime";
                list = this.GetSearchAssistList(Registration, RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, CertificateIssueDateFrom, CertificateIssueDateTo, statusId, EmailID);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SearchAssistGrid.cshtml", Grid);
                return Json(new { status = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        #region SearchAssistNextPage
        public ActionResult SearchAssistNextPage(int? Page,string Registration, string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo,string EmailID, string CertificateIssueDateFrom, string CertificateIssueDateTo, int statusId, int SortingOrder = 0, int Paging = 5)
        {
            try
            {
                DateTime? FromDate = null;
                DateTime? ToDate = null;

                if (!string.IsNullOrEmpty(CertificateIssueDateFrom))
                    FromDate = DateTime.ParseExact(CertificateIssueDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(CertificateIssueDateTo))
                    ToDate = DateTime.ParseExact(CertificateIssueDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                List<AssistVendroDetail> list = new List<AssistVendroDetail>();
                TempData["Load"] = "FirstTime";
                ViewBag.Registration = Registration;
                ViewBag.RefNo = RefNo;
                ViewBag.FirstName = FirstName;
                ViewBag.LastName = LastName;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.MobileNo = MobileNo;
                ViewBag.CertificateIssueDateFrom = CertificateIssueDateFrom;
                ViewBag.CertificateIssueDateTo = CertificateIssueDateTo;
                ViewBag.Status = statusId;
                ViewBag.Email = EmailID;

                list = this.GetSearchAssistList(Registration, RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, CertificateIssueDateFrom, CertificateIssueDateTo, statusId, EmailID);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SearchAssistGridNextPage.cshtml", grid);
                return Json(new { status = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        #endregion

        private List<AssistVendroDetail> GetSearchAssistList(string registration, string refNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string CertificateIssueDateFrom, string CertificateIssueDateTo, int? statusid,string EmailID)
        {
            DateTime? FromDate = null;
            DateTime? ToDate = null;

            if (!string.IsNullOrEmpty(CertificateIssueDateFrom))
                FromDate = DateTime.ParseExact(CertificateIssueDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(CertificateIssueDateTo))
                ToDate = DateTime.ParseExact(CertificateIssueDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            List<AssistVendroDetail> list = new List<AssistVendroDetail>();
            List<AssistVendroDetail> lstNotAssigned = new List<AssistVendroDetail>();
            List<AssistVendroDetail> lstAssigned = new List<AssistVendroDetail>();
            List<AssistVendroDetail> lstClosed = new List<AssistVendroDetail>();

            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID }).FirstOrDefault();

            if (statusid == 0)

            {
                statusid = 8;
                lstNotAssigned = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid)
                        select new AssistVendroDetail
                        {
                            TransactionID = t.TransactionID,
                            ActionId = (int)a.ActionId,
                            Address = a.Address,
                            CreatedDate1 = t.CreatedDate,
                            CreatedDate2 = a.AssistCreatedDate,
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            Lat = a.Lat,
                            Lon = a.Lon,
                            PinCode = a.PinCode,
                            RefNo = a.RefNo,
                            Registration = t.RegistrationNo,
                            Remarks = a.Remarks,
                            Status = status.Status,
                            Action = "",
                            VendorId = a.VendorId,
                            ContactNo = t.CustomerContact,
                            AssistId = a.Id,
                            CompanyName = cmnType.Description,
                            VendorName = "",
                            BranchName = "",
                            VendorAssignedDateTime = null,
                            VendorReachedIncidentLocationDateTime = null,
                            VendorReachedDropLocationDateTime = null

                        }).ToList();



                statusid = 9;
              lstAssigned   = (from a in db.tblAssistDetails
                        join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                        from Action in objAction.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                        from mstvendor in objvendor.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid)
                        select new AssistVendroDetail
                        {
                            TransactionID = t.TransactionID,
                            ActionId = (int)a.ActionId,
                            Address = a.Address,
                            CreatedDate1 = t.CreatedDate,
                            CreatedDate2 = a.AssistCreatedDate,
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            Lat = a.Lat,
                            Lon = a.Lon,
                            PinCode = a.PinCode,
                            RefNo = a.RefNo,
                            Registration = t.RegistrationNo,
                            Remarks = a.Remarks,
                            Status = status.Status,
                            Action = Action.Status,
                            VendorId = a.VendorId,
                            ContactNo = t.CustomerContact,
                            AssistId = a.Id,
                            CompanyName = cmnType.Description,
                            VendorName = mstvendor.VendorName,
                            BranchName = branch.BranchName,
                            VendorAssignedDateTime = a.VendorAssignedDateTime,
                            VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                            VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime

                        }).ToList();



                statusid = 10;
                lstClosed = (from a in db.tblAssistDetails                               
                               join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                               join branch in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals branch.BranchID into objbranch
                               from branch in objbranch.DefaultIfEmpty()
                               join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                               from status in objStatus.DefaultIfEmpty()
                               join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                               from Action in objAction.DefaultIfEmpty()
                               join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                               from cmnType in objcmntype.DefaultIfEmpty()
                               join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                               from mstvendor in objvendor.DefaultIfEmpty()
                               where
                               ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                               (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                               (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                               (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                               (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                               (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                               (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                               (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                               (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                               a.StatusId == statusid)
                               select new AssistVendroDetail
                               {
                                   TransactionID = t.TransactionID,
                                   ActionId = (int)a.ActionId,
                                   Address = a.Address,
                                   CreatedDate1 = t.CreatedDate,
                                   CreatedDate2 = a.AssistCreatedDate,
                                   CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                                   Issue = a.Issue,
                                   Landmark = a.Landmark,
                                   Lat = a.Lat,
                                   Lon = a.Lon,
                                   PinCode = a.PinCode,
                                   RefNo = a.RefNo,
                                   Registration = t.RegistrationNo,
                                   Remarks = a.Remarks,
                                   Status = status.Status,
                                   Action = Action.Status,
                //                   VendorId = a.VendorId,
                                   ContactNo = t.CustomerContact,
                                   AssistId = a.Id,
                                   CompanyName = cmnType.Description,
                                   VendorName = mstvendor.VendorName,
                                   BranchName = branch.BranchName,
                                   VendorAssignedDateTime = a.VendorAssignedDateTime,
                                   VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                                   VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime

                               }).ToList();

                list.AddRange(lstNotAssigned);
                list.AddRange(lstAssigned);
                list.AddRange(lstClosed);
            }

          else  if (statusid == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                list = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        where 
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid)
                        select new AssistVendroDetail
                        {
                            TransactionID = t.TransactionID,
                            ActionId = (int)a.ActionId,
                            Address = a.Address,
                            CreatedDate1 = t.CreatedDate,
                            CreatedDate2 = a.AssistCreatedDate,
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            Lat = a.Lat,
                            Lon = a.Lon,
                            PinCode = a.PinCode,
                            RefNo = a.RefNo,
                            Registration = t.RegistrationNo,
                            Remarks = a.Remarks,
                            Status = status.Status,
                            Action = "",
                            VendorId = a.VendorId,
                            ContactNo = t.CustomerContact,
                            AssistId = a.Id,
                            CompanyName = cmnType.Description,
                            VendorName = "",
                            BranchName= "",
                            VendorAssignedDateTime = null,
                            VendorReachedIncidentLocationDateTime = null,
                            VendorReachedDropLocationDateTime = null

                        }).ToList();
            }
            else
            {
                list = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join branch in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals branch.BranchID into objbranch
                        from branch in objbranch.DefaultIfEmpty()
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                        from Action in objAction.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                        from mstvendor in objvendor.DefaultIfEmpty()
                        where 
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid)
                        select new AssistVendroDetail
                        {
                            TransactionID = t.TransactionID,
                            ActionId = (int)a.ActionId,
                            Address = a.Address,
                            CreatedDate1 = t.CreatedDate,
                            CreatedDate2 = a.AssistCreatedDate,
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            Lat = a.Lat,
                            Lon = a.Lon,
                            PinCode = a.PinCode,
                            RefNo = a.RefNo,
                            Registration = t.RegistrationNo,
                            Remarks = a.Remarks,
                            Status = status.Status,
                            Action = Action.Status,
                            VendorId = a.VendorId,
                            ContactNo = t.CustomerContact,
                            AssistId = a.Id,
                            CompanyName = cmnType.Description,
                            VendorName = mstvendor.VendorName,
                            BranchName = branch.BranchName,
                            VendorAssignedDateTime = a.VendorAssignedDateTime,
                            VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                            VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime

                        }).ToList();
            }

            list.ForEach(x =>
            {
                if (x.CertificateNo.ToUpper().Contains("AT"))
                {
                    x.CreatedDate = x.CreatedDate1;
                }
                else
                {
                    x.CreatedDate = x.CreatedDate2;
                }

            });

            return list;
        }
        
        public ActionResult DownloadAssistList(string Registration, string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string CertificateIssueDateFrom, string CertificateIssueDateTo, int statusId,string EmailID)
        {
            try
            {
                DateTime? FromDate = null;
                DateTime? ToDate = null;

                if (!string.IsNullOrEmpty(CertificateIssueDateFrom))
                    FromDate = DateTime.ParseExact(CertificateIssueDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(CertificateIssueDateTo))
                    ToDate = DateTime.ParseExact(CertificateIssueDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                List<AssistVendroDetail> list = new List<AssistVendroDetail>();
                ViewBag.Registration = Registration;
                ViewBag.RefNo = RefNo;
                ViewBag.FirstName = FirstName;
                ViewBag.LastName = LastName;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.MobileNo = MobileNo;
                ViewBag.CertificateIssueDateFrom = CertificateIssueDateFrom;
                ViewBag.CertificateIssueDateTo = CertificateIssueDateTo;
                ViewBag.Status = statusId;
                TempData["Load"] = "FirstTime";
                list = this.GetSearchAssistList(Registration, RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, CertificateIssueDateFrom, CertificateIssueDateTo, statusId, EmailID);

                if (list.Count > 0)
                {
                    if (list.Any())
                    {
                        MemoryStream _MemoryStream = new MemoryStream();
                        string[] HeaderArray =
                            new string[]
                            {
                             "Vehicle Registration",
                             "Reference No",
                             "Contact No",
                             "Reported Date",
                             "Action",
                             "Status",
                             "Company Name",
                             "Vendor Name",
                             "Branch Name",
                             "Vendor Assign Date/Time",
                             "Vendor Reached in Incident Location Date/Time",
                             "Vendor Reached in Drop Location Date/Time"                                     
                            };

                        var listobj = list.Select(x => new
                        {
                            x.Registration,
                            x.RefNo,
                            x.ContactNo,
                            x.CreatedDate,
                            x.Action,
                            x.Status,
                            x.CompanyName,
                            x.VendorName,
                            x.BranchName,
                            x.VendorAssignedDateTime,
                            x.VendorReachedIncidentLocationDateTime,
                            x.VendorReachedDropLocationDateTime                            
                        }).ToList();

                        using (StreamWriter write = new StreamWriter(_MemoryStream))
                        {
                            using (CsvWriter csw = new CsvWriter(write))
                            {
                                foreach (var header in HeaderArray)
                                {
                                    csw.WriteField(header);
                                }
                                csw.NextRecord();

                                foreach (var row in listobj)
                                {
                                    csw.WriteRecord<dynamic>(row);
                                    csw.NextRecord();
                                }
                            }
                        }

                        Response.Clear();
                        Response.ClearHeaders();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=AssistListDetails" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + ".csv");
                        Response.Charset = "";
                        Response.ContentType = "application/x-msexcel";
                        Response.BinaryWrite(_MemoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);                
            }
            return View();
        }
        #endregion

        public ActionResult AssistView()
        {
            return View();
        }

        #region SearchAssistView
        public JsonResult SearchAssistView(string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string FromDate, string ToDate)
        {
            try
            {
                List<AssistVendroDetail> list = new List<AssistVendroDetail>();

                ViewBag.RefNo = RefNo;
                ViewBag.FirstName = FirstName;
                ViewBag.LastName = LastName;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.MobileNo = MobileNo;
                ViewBag.FromDate = FromDate;
                ViewBag.ToDate = ToDate;

                TempData["Load"] = "FirstTime";

                list = this.GetSearchAssistViewList(RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, FromDate, ToDate);

                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SearchAssistViewGrid.cshtml", Grid);

                return Json(new { status = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        #region SearchAssistViewNextPage
        public ActionResult SearchAssistViewNextPage(int? Page, string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string FromDate, string ToDate, int SortingOrder = 0, int Paging = 5)
        {
            try
            {
                List<AssistVendroDetail> list = new List<AssistVendroDetail>();                

                ViewBag.RefNo = RefNo;
                ViewBag.FirstName = FirstName;
                ViewBag.LastName = LastName;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.MobileNo = MobileNo;
                ViewBag.FromDate = FromDate;
                ViewBag.ToDate = ToDate;

                TempData["Load"] = "FirstTime";

                list = this.GetSearchAssistViewList(RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, FromDate, ToDate);

                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SearchAssistViewGridNextPage.cshtml", grid);

                return Json(new { status = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        #endregion

        private List<AssistVendroDetail> GetSearchAssistViewList(string refNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string FromDate, string ToDate)
        {
            int CompanyId = 0;
            List<AssistVendroDetail> list = new List<AssistVendroDetail>();

            DateTime? FDate = null;
            DateTime? TDate = null;

            if (!string.IsNullOrEmpty(FromDate))
                FDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(ToDate))
                TDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID, s.CompanyID }).FirstOrDefault();
            if (userdata != null)
            {
                if (userdata.CompanyID == null)
                {
                    CompanyId = 0;
                }
                else
                {
                    CompanyId = Convert.ToInt32(userdata.CompanyID);
                }
            }

            if (CompanyId > 0)
            {
                list = (from a in db.tblAssistDetails
                            //join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                        from Action in objAction.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                            //join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                            //from mstvendor in objvendor.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FDate) || FDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(TDate) || TDate == null) &&
                        (a.AssistCreatedBy == userdata.Id))
                        select new AssistVendroDetail
                        {
                            TransactionID = t.TransactionID,
                            ActionId = (int)a.ActionId,
                            Address = a.Address,
                            CreatedDate = a.AssistCreatedDate,
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            Lat = a.Lat,
                            Lon = a.Lon,
                            PinCode = a.PinCode,
                            RefNo = a.RefNo,
                            Registration = t.RegistrationNo,
                            Remarks = a.Remarks,
                            Status = status.Status,
                            Action = Action.Status,
                            //VendorId = a.VendorId,
                            ContactNo = t.CustomerContact,
                            AssistId = a.Id,
                            CompanyName = cmnType.Description,
                            //VendorName = mstvendor.VendorName,
                            //BranchName = branch.BranchName,
                            VendorAssignedDateTime = a.VendorAssignedDateTime,
                            VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                            VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime,
                            AssistCreatedDate = a.AssistCreatedDate

                        }).OrderByDescending(o => o.AssistCreatedDate).ToList();
            }
            else
            {
                list = (from a in db.tblAssistDetails
                            //join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                        from Action in objAction.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                            //join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                            //from mstvendor in objvendor.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FDate) || FDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(TDate) || TDate == null)
                        //&& (a.AssistCreatedBy == userdata.Id)
                        )
                        select new AssistVendroDetail
                        {
                            TransactionID = t.TransactionID,
                            ActionId = (int)a.ActionId,
                            Address = a.Address,
                            CreatedDate = a.AssistCreatedDate,
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            Lat = a.Lat,
                            Lon = a.Lon,
                            PinCode = a.PinCode,
                            RefNo = a.RefNo,
                            Registration = t.RegistrationNo,
                            Remarks = a.Remarks,
                            Status = status.Status,
                            Action = Action.Status,
                            //VendorId = a.VendorId,
                            ContactNo = t.CustomerContact,
                            AssistId = a.Id,
                            CompanyName = cmnType.Description,
                            //VendorName = mstvendor.VendorName,
                            //BranchName = branch.BranchName,
                            VendorAssignedDateTime = a.VendorAssignedDateTime,
                            VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                            VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime,
                            AssistCreatedDate = a.AssistCreatedDate

                        }).OrderByDescending(o => o.AssistCreatedDate).ToList();
            }

            return list;
        }
        #endregion

        #region AssistViewOEM
        public ActionResult AssistViewOEM()
        {
            AssistanceDataEntryModel AsstDataEntrymodel = new AssistanceDataEntryModel();
            try
            {
                string UserName = Convert.ToString(User.Identity.Name);
                AsstDataEntrymodel = GetAssistViewOEM(UserName);
            }
            catch (Exception e)
            {
                HandleException(e);
                throw;
            }

            return View(AsstDataEntrymodel);
        }

        public AssistanceDataEntryModel GetAssistViewOEM(string UserName)
        {
            int OEMId = 0;
            int CompId = 0;
            AssistanceDataEntryModel AsstDataEntrymodel = new AssistanceDataEntryModel();

            var userdata = db.AspNetUsers.AsNoTracking().Where(w => w.Email == UserName).FirstOrDefault();
            if (userdata != null)
            {
                if (userdata.OEMID == null)
                {
                    OEMId = 0;
                }
                else
                {
                    OEMId = Convert.ToInt32(userdata.OEMID);
                }

                if (userdata.CompanyID == null)
                {
                    CompId = 0;
                }
                else
                {
                    CompId = Convert.ToInt32(userdata.CompanyID);
                }
            }          

            if (CompId > 0)
            {
                AsstDataEntrymodel.CompanyListItem = (from OCM in db.tblOEMCompanyMapping.AsNoTracking()
                                                      join MCT in db.tblMstCommonTypes.AsNoTracking() on OCM.CompanyId equals MCT.CommonTypeID into Companybased
                                                      from CompanybasedObj in Companybased.DefaultIfEmpty()
                                                      where OCM.OEMId == OEMId && OCM.IsValid != false && CompanybasedObj.MasterType == CrossCutting_Constant.Company_Name
                                                      select new
                                                      {
                                                          CompanyID = CompanybasedObj.CommonTypeID,
                                                          CompanyName = CompanybasedObj.Description

                                                      }).OrderBy(o => o.CompanyName).ToDictionary(d => d.CompanyID, d => d.CompanyName);

                AsstDataEntrymodel.CompID = CompId;
            }
            else
            {
                AsstDataEntrymodel.CompanyListItem = (from OCM in db.tblOEMCompanyMapping.AsNoTracking()
                                                      join MCT in db.tblMstCommonTypes.AsNoTracking() on OCM.CompanyId equals MCT.CommonTypeID into Companybased
                                                      from CompanybasedObj in Companybased.DefaultIfEmpty()
                                                      where OCM.OEMId == OEMId && OCM.IsValid != false && CompanybasedObj.MasterType == CrossCutting_Constant.Company_Name
                                                      select new
                                                      {
                                                          CompanyID = CompanybasedObj.CommonTypeID,
                                                          CompanyName = CompanybasedObj.Description

                                                      }).OrderBy(o => o.CompanyName).ToDictionary(d => d.CompanyID, d => d.CompanyName);

                AsstDataEntrymodel.CompID = CompId;
            }                    

            if  (OEMId > 0)
            {
                AsstDataEntrymodel.hdnOEMID = Convert.ToString(OEMId);
            }
            else
            {
                AsstDataEntrymodel.hdnOEMID = "";
            }            

            return AsstDataEntrymodel;
        }
        #endregion

        #region SearchAssistViewOEM
        public JsonResult SearchAssistViewOEM(string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string FromDate, string ToDate, string OEMId, string CompanyId)
        {
            try
            {
                List<AssistVendroDetail> list = new List<AssistVendroDetail>();

                ViewBag.RefNo = RefNo;
                ViewBag.FirstName = FirstName;
                ViewBag.LastName = LastName;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.MobileNo = MobileNo;
                ViewBag.FromDate = FromDate;
                ViewBag.ToDate = ToDate;
                ViewBag.OEMId = OEMId;
                ViewBag.CompanyId = CompanyId;

                TempData["Load"] = "FirstTime";

                list = this.GetSearchAssistViewOEMList(RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, FromDate, ToDate, OEMId, CompanyId);

                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SearchAssistViewOEMGrid.cshtml", Grid);

                return Json(new { status = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        #region SearchAssistViewOEMNextPage
        public ActionResult SearchAssistViewOEMNextPage(int? Page, string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string FromDate, string ToDate, string OEMId, string CompanyId, int SortingOrder = 0, int Paging = 5)
        {
            try
            {
                List<AssistVendroDetail> list = new List<AssistVendroDetail>();

                ViewBag.RefNo = RefNo;
                ViewBag.FirstName = FirstName;
                ViewBag.LastName = LastName;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.MobileNo = MobileNo;
                ViewBag.FromDate = FromDate;
                ViewBag.ToDate = ToDate;
                ViewBag.OEMId = OEMId;
                ViewBag.CompanyId = CompanyId;

                TempData["Load"] = "FirstTime";

                list = this.GetSearchAssistViewOEMList(RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, FromDate, ToDate, OEMId, CompanyId);

                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SearchAssistViewOEMGridNextPage.cshtml", grid);

                return Json(new { status = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }
        #endregion

        private List<AssistVendroDetail> GetSearchAssistViewOEMList(string refNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string FromDate, string ToDate, string OEMId, string CompanyId)
        {
            List<AssistVendroDetail> list = new List<AssistVendroDetail>();

            int intOEMId = 0;
            int intCompanyId = 0;
            int.TryParse(OEMId, out intOEMId);
            int.TryParse(CompanyId, out intCompanyId);

            if (intOEMId == 0 || intCompanyId == 0)
            {
                return list;
            }

            DateTime? FDate = null;
            DateTime? TDate = null;

            if (!string.IsNullOrEmpty(FromDate))
                FDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(ToDate))
                TDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID, s.CompanyID }).FirstOrDefault();

            list = (from a in db.tblAssistDetails
                        //join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID
                    join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                    join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                    from status in objStatus.DefaultIfEmpty()
                    join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                    from Action in objAction.DefaultIfEmpty()
                    join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                    from cmnType in objcmntype.DefaultIfEmpty()
                        //join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                        //from mstvendor in objvendor.DefaultIfEmpty()
                    where
                    ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                    (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                    (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                    (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                    (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                    (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                    (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FDate) || FDate == null) &&
                    (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(TDate) || TDate == null) &&
                    a.AssistOEMID == intOEMId &&
                    a.AssistCompanyID == intCompanyId
                    //&& (a.AssistCreatedBy == userdata.Id)
                    )
                    select new AssistVendroDetail
                    {
                        TransactionID = t.TransactionID,
                        ActionId = (int)a.ActionId,
                        Address = a.Address,
                        CreatedDate = a.AssistCreatedDate,
                        Issue = a.Issue,
                        Landmark = a.Landmark,
                        Lat = a.Lat,
                        Lon = a.Lon,
                        PinCode = a.PinCode,
                        RefNo = a.RefNo,
                        Registration = t.RegistrationNo,
                        Remarks = a.Remarks,
                        Status = status.Status,
                        Action = Action.Status,
                        //VendorId = a.VendorId,
                        ContactNo = t.CustomerContact,
                        AssistId = a.Id,
                        CompanyName = cmnType.Description,
                        //VendorName = mstvendor.VendorName,
                        //BranchName = branch.BranchName,
                        VendorAssignedDateTime = a.VendorAssignedDateTime,
                        VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                        VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime,
                        AssistCreatedDate = a.AssistCreatedDate

                    }).OrderByDescending(o => o.AssistCreatedDate).ToList();

            return list;
        }
        #endregion

        #region DownloadAssistViewOEM
        public ActionResult DownloadhAssistViewOEM(string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string FromDate, string ToDate, string OEMId, string CompanyId)
        {
            try
            {
                List<AssistVendroDetail> list = new List<AssistVendroDetail>();

                ViewBag.RefNo = RefNo;
                ViewBag.FirstName = FirstName;
                ViewBag.LastName = LastName;
                ViewBag.EngineNo = EngineNo;
                ViewBag.ChassisNo = ChassisNo;
                ViewBag.MobileNo = MobileNo;
                ViewBag.FromDate = FromDate;
                ViewBag.ToDate = ToDate;
                ViewBag.OEMId = OEMId;
                ViewBag.CompanyId = CompanyId;
                TempData["Load"] = "FirstTime";

                list = this.GetSearchAssistViewOEMList(RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, FromDate, ToDate, OEMId, CompanyId);

                if (list.Count > 0)
                {
                    if (list.Any())
                    {
                        MemoryStream _MemoryStream = new MemoryStream();
                        string[] HeaderArray =
                            new string[]
                            {
                             "Vehicle Registration",
                             "Reference No",
                             "Contact No",
                             "Reported Date",
                             "Action",
                             "Status",
                             "Company Name",                            
                             "Vendor Assign Date/Time",
                             "Vendor Reached in Incident Location Date/Time",
                             "Vendor Reached in Drop Location Date/Time"
                            };

                        var listobj = list.Select(x => new
                        {
                            x.Registration,
                            x.RefNo,
                            x.ContactNo,
                            x.CreatedDate,
                            x.Action,
                            x.Status,
                            x.CompanyName,                            
                            x.VendorAssignedDateTime,
                            x.VendorReachedIncidentLocationDateTime,
                            x.VendorReachedDropLocationDateTime
                        }).ToList();

                        using (StreamWriter write = new StreamWriter(_MemoryStream))
                        {
                            using (CsvWriter csw = new CsvWriter(write))
                            {
                                foreach (var header in HeaderArray)
                                {
                                    csw.WriteField(header);
                                }
                                csw.NextRecord();

                                foreach (var row in listobj)
                                {
                                    csw.WriteRecord<dynamic>(row);
                                    csw.NextRecord();
                                }
                            }
                        }

                        Response.Clear();
                        Response.ClearHeaders();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=AssistListDetailsOEM" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + ".csv");
                        Response.Charset = "";
                        Response.ContentType = "application/x-msexcel";
                        Response.BinaryWrite(_MemoryStream.ToArray());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);               
            }

            return View();
        }
        #endregion

        //public JsonResult GetAssistDetailForVendor(string AssitId)
        //{
        //    string HTML = String.Empty;
        //    try
        //    {
        //        string UserName = HttpContext.User.Identity.Name;
        //        decimal Assitid = Convert.ToDecimal(AssitId);
        //        AssistModel assistDetails = GetAssistDetailForUpdate(Assitid);
        //        HTML = RenderPartialViewToString("~/Views/Assistance/GetAssistDetails.cshtml", assistDetails);
        //        return Json(new { message = CrossCutting_Constant.Success, HTML = HTML }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleException(ex);
        //        throw;
        //    }
        //}

        [HttpGet]
        public ActionResult GetAssistDetailForVendor(string AssitId)
        {
            try
            {
                string UserName = HttpContext.User.Identity.Name;
                decimal Assitid = Convert.ToDecimal(AssitId);
                AssistModel assistDetails = GetAssistDetailForUpdate(Assitid);

                return View(assistDetails);                           
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        private AssistModel GetAssistDetailForUpdate(decimal AssitId)
        {
            string CompanyId = "0";
            string[] IsEditable = Convert.ToString(ConfigurationManager.AppSettings["IsEditable"]).Split(',');

            bool boolIsRelianceAssistance = false;
            string RILUserID = Convert.ToString(ConfigurationManager.AppSettings["RILUserID"]);
            string RelianceUserID = string.Empty;
            string AssistUserID = string.Empty;

            var tblAssistD = db.tblAssistDetails.Where(w => w.Id == AssitId).Select(s => new { s.AssistCreatedBy }).FirstOrDefault();
            if (tblAssistD != null)
            {
                string AssistCreatedBy = tblAssistD.AssistCreatedBy;
                var usrdata = db.AspNetUsers.AsNoTracking().Where(w => w.Id == AssistCreatedBy).Select(s => new { s.CompanyID }).FirstOrDefault();
                if (usrdata != null)
                {
                    if (usrdata.CompanyID == null)
                    {
                        CompanyId = "0";
                    }
                    else
                    {
                        CompanyId = Convert.ToString(usrdata.CompanyID);
                    }
                }
            }

            var userdata = db.AspNetUsers.Where(w => w.Email == RILUserID).FirstOrDefault();
            if (userdata != null)
            {
                RelianceUserID = userdata.Id;
            }

            var tblAssistDetails = db.tblAssistDetails.Where(w => w.Id == AssitId).Select(s => new { s.TransactionId }).FirstOrDefault();
            if (tblAssistDetails != null)
            {
                decimal TransactionId = Convert.ToDecimal(tblAssistDetails.TransactionId);
                var tblTransaction = db.tblTransactions.Where(w => w.TransactionID == TransactionId).Select(s => new { s.UserID }).FirstOrDefault();
                if (!string.IsNullOrEmpty(tblTransaction.UserID))
                {
                    AssistUserID = tblTransaction.UserID;
                    if (AssistUserID.Equals(RelianceUserID))
                    {
                        boolIsRelianceAssistance = true;
                    }
                    else
                    {
                        if (IsEditable.Contains(CompanyId))
                        {
                            boolIsRelianceAssistance = true;
                        }
                        else
                        {
                            boolIsRelianceAssistance = false;
                        }
                    }
                }               
            }            

            AssistModel data = new AssistModel();

            tblAssistDetail objTran = db.tblAssistDetails.Where(x => x.Id == AssitId).FirstOrDefault();

            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                data = (from a in db.tblAssistDetails.AsNoTracking()
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                        from Incident in objincident.DefaultIfEmpty()
                        where a.Id == AssitId
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            Address = a.Address,
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            PinCode = a.PinCode,
                            Remarks = a.Remarks,
                            Action = "",
                            Status = status.Status,
                            ActionID = 0,
                            Make = t.tblMstMake.Name,
                            Model = t.tblMstModel.Name,
                            EngineNo = t.EngineNo,
                            ChassisNo = t.ChassisNo,
                            RegistrationNo = t.RegistrationNo,
                            FirstName = (!string.IsNullOrEmpty(a.CustomerName)) ? a.CustomerName : t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            AssistID = a.Id,
                            BranchName = "",
                            Vendor_BranchID = 0,
                            State = a.State,
                            City = a.City,
                            location = a.Location,
                            StreetAddress = a.StreetAddress,
                            IncidentDetail = Incident.IncidentDetail,
                            VehicleColor = a.VehicleColor,
                            VehicleYear = a.VehicleYear,
                            CertificateStartDate = t.CoverStartDate,
                            CertificateEndDate = t.CoverEndDate,
                            CertificateNo = t.CertificateNo,
                            IncidentDetailId = (int)a.IncidentDetailid,
                            //AssistSummary = a.SummaryDetails,
                            AssistSummaryHistory = a.SummaryDetails,
                            DropLocation = a.DropLocationAddress,
                            DropLocationKM = a.DropLocationKM,
                            AssistCompanyID = a.AssistCompanyID,
                            hdnAssistCompanyID = a.AssistCompanyID,
                            VendorReachedIncidentLocationDateTime = null,
                            VendorReachedDropLocationDateTime = null,
                            AssistOEMID = a.AssistOEMID,
                            hdnAssistOEMID = a.AssistOEMID

                        }).FirstOrDefault();
            }
            else
            {
                if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Assigned)
                {
                    data = (from a in db.tblAssistDetails.AsNoTracking()
                            join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                            join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                            from status in objStatus.DefaultIfEmpty()
                            join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                            from Action in objAction.DefaultIfEmpty()
                            join v in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals v.BranchID into objVendor
                            from Vendor in objVendor.DefaultIfEmpty()
                            join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                            from Incident in objincident.DefaultIfEmpty()
                            where a.Id == AssitId
                            select new AssistModel
                            {
                                TransactionID = t.TransactionID,
                                Address = a.Address,
                                Issue = a.Issue,
                                Landmark = a.Landmark,
                                PinCode = a.PinCode,
                                Remarks = a.Remarks,
                                Action = Action.Status,
                                Status = status.Status,
                                ActionID = (int)a.ActionId,
                                Make = t.tblMstMake.Name,
                                Model = t.tblMstModel.Name,
                                EngineNo = t.EngineNo,
                                ChassisNo = t.ChassisNo,
                                RegistrationNo = t.RegistrationNo,
                                FirstName = (!string.IsNullOrEmpty(a.CustomerName)) ? a.CustomerName : t.CustomerName,
                                MiddleName = t.CustomerMiddleName,
                                LastName = t.CustomerLastName,
                                Email = t.CustomerEmail,
                                Mobile = t.CustomerContact,
                                AssistID = a.Id,
                                BranchName = Vendor.BranchName,
                                Vendor_BranchID = Vendor.BranchID,
                                State = a.State,
                                City = a.City,
                                location = a.Location,
                                StreetAddress = a.StreetAddress,
                                IncidentDetail = Incident.IncidentDetail,
                                VehicleColor = a.VehicleColor,
                                VehicleYear = a.VehicleYear,
                                CertificateStartDate = t.CoverStartDate,
                                CertificateEndDate = t.CoverEndDate,
                                CertificateNo = t.CertificateNo,
                                IncidentDetailId = (int)a.IncidentDetailid,
                                //AssistSummary = a.SummaryDetails,
                                AssistSummaryHistory = a.SummaryDetails,
                                RemarkId = a.Remark1,
                                DropLocation = a.DropLocationAddress,
                                DropLocationKM = a.DropLocationKM,
                                AssistCompanyID = a.AssistCompanyID,
                                hdnAssistCompanyID = a.AssistCompanyID,
                                VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                                VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime,
                                strLatitudeLoc = a.Lat,
                                strLongitudeLoc = a.Lon,
                                AssistOEMID = a.AssistOEMID,
                                hdnAssistOEMID = a.AssistOEMID

                            }).FirstOrDefault();

                    // VendorReachedIncidentLocation Time Start
                    data.VendorReachedIncidentLocationHoursListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"}
                    };

                    data.VendorReachedIncidentLocationMinsListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"},
                        {"24", "24"},{"25", "25"},{"26", "26"},{"27", "27"},{"28", "28"},{"29", "29"},{"30", "30"},{"31", "31"},{"32", "32"},{"33", "33"},{"34", "34"},{"35", "35"},
                        {"36", "36"},{"37", "37"},{"38", "38"},{"39", "39"},{"40", "40"},{"41", "41"},{"42", "42"},{"43", "43"},{"44", "44"},{"45", "45"},{"46", "46"},{"47", "47"},
                        {"48", "48"},{"49", "49"},{"50", "50"},{"51", "51"},{"52", "52"},{"53", "53"},{"54", "54"},{"55", "55"},{"56", "56"},{"57", "57"},{"58", "58"},{"59", "59"}
                    };

                    data.VendorReachedIncidentLocationSecsListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"},
                        {"24", "24"},{"25", "25"},{"26", "26"},{"27", "27"},{"28", "28"},{"29", "29"},{"30", "30"},{"31", "31"},{"32", "32"},{"33", "33"},{"34", "34"},{"35", "35"},
                        {"36", "36"},{"37", "37"},{"38", "38"},{"39", "39"},{"40", "40"},{"41", "41"},{"42", "42"},{"43", "43"},{"44", "44"},{"45", "45"},{"46", "46"},{"47", "47"},
                        {"48", "48"},{"49", "49"},{"50", "50"},{"51", "51"},{"52", "52"},{"53", "53"},{"54", "54"},{"55", "55"},{"56", "56"},{"57", "57"},{"58", "58"},{"59", "59"}
                    };
                    // VendorReachedIncidentLocation Time End

                    // VendorReachedDropLocation Time Start
                    data.VendorReachedDropLocationHoursListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"}
                    };

                    data.VendorReachedDropLocationMinsListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"},
                        {"24", "24"},{"25", "25"},{"26", "26"},{"27", "27"},{"28", "28"},{"29", "29"},{"30", "30"},{"31", "31"},{"32", "32"},{"33", "33"},{"34", "34"},{"35", "35"},
                        {"36", "36"},{"37", "37"},{"38", "38"},{"39", "39"},{"40", "40"},{"41", "41"},{"42", "42"},{"43", "43"},{"44", "44"},{"45", "45"},{"46", "46"},{"47", "47"},
                        {"48", "48"},{"49", "49"},{"50", "50"},{"51", "51"},{"52", "52"},{"53", "53"},{"54", "54"},{"55", "55"},{"56", "56"},{"57", "57"},{"58", "58"},{"59", "59"}
                    };

                    data.VendorReachedDropLocationSecsListItem = new Dictionary<string, string>() {

                        {"00", "00"},{"01", "01"},{"02", "02"},{"03", "03"},{"04", "04"},{"05", "05"},{"06", "06"},{"07", "07"},{"08", "08"},{"09", "09"},{"10", "10"},{"11", "11"},
                        {"12", "12"},{"13", "13"},{"14", "14"},{"15", "15"},{"16", "16"},{"17", "17"},{"18", "18"},{"19", "19"},{"20", "20"},{"21", "21"},{"22", "22"},{"23", "23"},
                        {"24", "24"},{"25", "25"},{"26", "26"},{"27", "27"},{"28", "28"},{"29", "29"},{"30", "30"},{"31", "31"},{"32", "32"},{"33", "33"},{"34", "34"},{"35", "35"},
                        {"36", "36"},{"37", "37"},{"38", "38"},{"39", "39"},{"40", "40"},{"41", "41"},{"42", "42"},{"43", "43"},{"44", "44"},{"45", "45"},{"46", "46"},{"47", "47"},
                        {"48", "48"},{"49", "49"},{"50", "50"},{"51", "51"},{"52", "52"},{"53", "53"},{"54", "54"},{"55", "55"},{"56", "56"},{"57", "57"},{"58", "58"},{"59", "59"}
                    };
                    // VendorReachedDropLocation Time End

                    if (data != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(data.VendorReachedIncidentLocationDateTime)))
                        {
                            data.strVendorReachedIncidentLocationDateTime = Convert.ToDateTime(data.VendorReachedIncidentLocationDateTime).ToString("MM/dd/yyyy");

                            data.VendorReachedIncidentLocationHours = Convert.ToDateTime(data.VendorReachedIncidentLocationDateTime).ToString("HH");
                            data.VendorReachedIncidentLocationMins = Convert.ToDateTime(data.VendorReachedIncidentLocationDateTime).ToString("mm");
                            data.VendorReachedIncidentLocationSecs = Convert.ToDateTime(data.VendorReachedIncidentLocationDateTime).ToString("ss");
                        }
                        else
                        {
                            data.strVendorReachedIncidentLocationDateTime = null;
                        }


                        if (!string.IsNullOrEmpty(Convert.ToString(data.VendorReachedDropLocationDateTime)))
                        {
                            data.strVendorReachedDropLocationDateTime = Convert.ToDateTime(data.VendorReachedDropLocationDateTime).ToString("MM/dd/yyyy");

                            data.VendorReachedDropLocationHours = Convert.ToDateTime(data.VendorReachedDropLocationDateTime).ToString("HH");
                            data.VendorReachedDropLocationMins = Convert.ToDateTime(data.VendorReachedDropLocationDateTime).ToString("mm");
                            data.VendorReachedDropLocationSecs = Convert.ToDateTime(data.VendorReachedDropLocationDateTime).ToString("ss");
                        }
                        else
                        {
                            data.strVendorReachedDropLocationDateTime = null;
                        }
                    }                    
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(objTran.AssistCaseTypeID)) && !string.IsNullOrEmpty(Convert.ToString(objTran.AssistSubCategoryID)))
                    {
                        data = (from a in db.tblAssistDetails.AsNoTracking()
                                join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                                join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                                from status in objStatus.DefaultIfEmpty()
                                join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                                from Action in objAction.DefaultIfEmpty()
                                join v in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals v.BranchID into objVendor
                                from Vendor in objVendor.DefaultIfEmpty()
                                join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                                from Incident in objincident.DefaultIfEmpty()
                                where a.Id == AssitId
                                select new AssistModel
                                {
                                    TransactionID = t.TransactionID,
                                    Address = a.Address,
                                    Issue = a.Issue,
                                    Landmark = a.Landmark,
                                    PinCode = a.PinCode,
                                    Remarks = a.Remarks,
                                    Action = Action.Status,
                                    Status = status.Status,
                                    //ActionID = (int)a.ActionId,
                                    ActionID = 0,
                                    Make = t.tblMstMake.Name,
                                    Model = t.tblMstModel.Name,
                                    EngineNo = t.EngineNo,
                                    ChassisNo = t.ChassisNo,
                                    RegistrationNo = t.RegistrationNo,
                                    FirstName = (!string.IsNullOrEmpty(a.CustomerName)) ? a.CustomerName : t.CustomerName,
                                    MiddleName = t.CustomerMiddleName,
                                    LastName = t.CustomerLastName,
                                    Email = t.CustomerEmail,
                                    Mobile = t.CustomerContact,
                                    AssistID = a.Id,
                                    BranchName = Vendor.BranchName,
                                    //Vendor_BranchID = Vendor.BranchID,
                                    Vendor_BranchID = 0,
                                    State = a.State,
                                    City = a.City,
                                    location = a.Location,
                                    StreetAddress = a.StreetAddress,
                                    IncidentDetail = Incident.IncidentDetail,
                                    VehicleColor = a.VehicleColor,
                                    VehicleYear = a.VehicleYear,
                                    CertificateStartDate = t.CoverStartDate,
                                    CertificateEndDate = t.CoverEndDate,
                                    CertificateNo = t.CertificateNo,
                                    IncidentDetailId = (int)a.IncidentDetailid,
                                    //AssistSummary = a.SummaryDetails,
                                    AssistSummaryHistory = a.SummaryDetails,
                                    RemarkId = a.Remark1,
                                    DropLocation = a.DropLocationAddress,
                                    DropLocationKM = a.DropLocationKM,
                                    AssistCompanyID = a.AssistCompanyID,
                                    hdnAssistCompanyID = a.AssistCompanyID,
                                    VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                                    VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime,
                                    strLatitudeLoc = a.Lat,
                                    strLongitudeLoc = a.Lon,
                                    AssistOEMID = a.AssistOEMID,
                                    hdnAssistOEMID = a.AssistOEMID

                                }).FirstOrDefault();
                    }
                    else
                    {
                        data = (from a in db.tblAssistDetails.AsNoTracking()
                                join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                                join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                                from status in objStatus.DefaultIfEmpty()
                                join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                                from Action in objAction.DefaultIfEmpty()
                                join v in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals v.BranchID into objVendor
                                from Vendor in objVendor.DefaultIfEmpty()
                                join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                                from Incident in objincident.DefaultIfEmpty()
                                where a.Id == AssitId
                                select new AssistModel
                                {
                                    TransactionID = t.TransactionID,
                                    Address = a.Address,
                                    Issue = a.Issue,
                                    Landmark = a.Landmark,
                                    PinCode = a.PinCode,
                                    Remarks = a.Remarks,
                                    Action = Action.Status,
                                    Status = status.Status,
                                    ActionID = (int)a.ActionId,
                                    Make = t.tblMstMake.Name,
                                    Model = t.tblMstModel.Name,
                                    EngineNo = t.EngineNo,
                                    ChassisNo = t.ChassisNo,
                                    RegistrationNo = t.RegistrationNo,
                                    FirstName = (!string.IsNullOrEmpty(a.CustomerName)) ? a.CustomerName : t.CustomerName,
                                    MiddleName = t.CustomerMiddleName,
                                    LastName = t.CustomerLastName,
                                    Email = t.CustomerEmail,
                                    Mobile = t.CustomerContact,
                                    AssistID = a.Id,
                                    BranchName = Vendor.BranchName,
                                    Vendor_BranchID = Vendor.BranchID,
                                    State = a.State,
                                    City = a.City,
                                    location = a.Location,
                                    StreetAddress = a.StreetAddress,
                                    IncidentDetail = Incident.IncidentDetail,
                                    VehicleColor = a.VehicleColor,
                                    VehicleYear = a.VehicleYear,
                                    CertificateStartDate = t.CoverStartDate,
                                    CertificateEndDate = t.CoverEndDate,
                                    CertificateNo = t.CertificateNo,
                                    IncidentDetailId = (int)a.IncidentDetailid,
                                    //AssistSummary = a.SummaryDetails,
                                    AssistSummaryHistory = a.SummaryDetails,
                                    RemarkId = a.Remark1,
                                    DropLocation = a.DropLocationAddress,
                                    DropLocationKM = a.DropLocationKM,
                                    AssistCompanyID = a.AssistCompanyID,
                                    hdnAssistCompanyID = a.AssistCompanyID,
                                    VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                                    VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime,
                                    strLatitudeLoc = a.Lat,
                                    strLongitudeLoc = a.Lon,
                                    AssistOEMID = a.AssistOEMID,
                                    hdnAssistOEMID = a.AssistOEMID

                                }).FirstOrDefault();
                    }                    
                }                    
            }

            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Assigned)
            {
                if (boolIsRelianceAssistance == true)
                {
                    int IncidentDetailid = Convert.ToInt32(db.tblMstIncidentDetails.Where(x => x.Detailid == data.IncidentDetailId).Select(s => s.IncidentDetailid).FirstOrDefault());

                    var incidentDetails = db.tblMstIncidentDetails.Where(x => x.IncidentDetailid == IncidentDetailid).ToList();

                    data.lstIncidentDetails = incidentDetails
                        .Select(x => new SelectListItem { Text = x.IncidentDetail, Value = x.Detailid.ToString() }).ToList();

                    data.IncidentTypeId = IncidentDetailid;

                    data.lstIncidentType = db.tblMstIncidents.Where(x => x.IsValid == true)
                          .Select(x => new SelectListItem { Text = x.IncidentType, Value = x.Incidentid.ToString() }).OrderBy(o => o.Value).ToList();
                }
                else
                {
                    var incidentDetails = db.tblMstIncidentDetails.Where(x => x.Detailid == data.IncidentDetailId).ToList();

                    data.lstIncidentDetails = incidentDetails
                        .Select(x => new SelectListItem { Text = x.IncidentDetail, Value = x.Detailid.ToString() }).ToList();

                    var type = incidentDetails.FirstOrDefault();

                    if (type != null)
                    {
                        data.lstIncidentType = db.tblMstIncidents.Where(x => x.IsValid == true && x.Incidentid == type.IncidentDetailid)
                          .Select(x => new SelectListItem { Text = x.IncidentType, Value = x.Incidentid.ToString() }).ToList();

                        data.IncidentTypeId = (int)type.IncidentDetailid;
                    }
                }
            }
            else
            {
                var incidentDetails = db.tblMstIncidentDetails.Where(x => x.Detailid == data.IncidentDetailId).ToList();

                data.lstIncidentDetails = incidentDetails
                    .Select(x => new SelectListItem { Text = x.IncidentDetail, Value = x.Detailid.ToString() }).ToList();

                var type = incidentDetails.FirstOrDefault();

                if (type != null)
                {
                    data.lstIncidentType = db.tblMstIncidents.Where(x => x.IsValid == true && x.Incidentid == type.IncidentDetailid)
                      .Select(x => new SelectListItem { Text = x.IncidentType, Value = x.Incidentid.ToString() }).ToList();

                    data.IncidentTypeId = (int)type.IncidentDetailid;
                }
            }

            //
            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                data.lstVendorName = db.tblMstVendors.Where(x => x.IsVlid != false).Select(x =>
                new SelectListItem { Text = x.VendorName, Value = x.VendorID.ToString() }).OrderBy(o => o.Text).ToList();
            }
            else
            {
                var BranchDetails = db.tblVendorBranchDetails.Where(x => x.BranchID == data.Vendor_BranchID).ToList();

                data.lstBranchName = BranchDetails
                    .Select(x => new SelectListItem { Text = x.BranchName, Value = x.BranchID.ToString() }).ToList();

                var BName = BranchDetails.FirstOrDefault();

                if (BName != null)
                {
                    data.lstVendorName = db.tblMstVendors.Where(x => x.IsVlid == true && x.VendorID == BName.VendorID)
                      .Select(x => new SelectListItem { Text = x.VendorName, Value = x.VendorID.ToString() }).ToList();

                    data.Vendor_NameId = (int)BName.VendorID;
                }
            }
            //

            data.lstStatus = db.tblMstWorkFlowStatus.Where(x => x.StatusID == 10).Select(x =>
                new SelectListItem { Text = x.Status, Value = x.StatusID.ToString() }).ToList();

            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
            {
                data.RemarkListItem = db.tblMstCommonTypes.Where(w => w.MasterType == "CaseClosureRemark" && w.IsValid != false)
                     .Select(x => new SelectListItem { Text = x.Description, Value = x.CommonTypeID.ToString() }).ToList();

                data.RemId = Convert.ToInt32(data.RemarkId);
            }

            data.lstStateItem = (from vbd in db.tblVendorBranchDetails.AsNoTracking()
                                 join s in db.tblMstStates.AsNoTracking() on vbd.State equals s.StateID into Statebased
                                 from Statebasedobj in Statebased.DefaultIfEmpty()
                                 select new
                                 {
                                     StateID = Statebasedobj.StateID,
                                     State = Statebasedobj.State

                                 }).Distinct().OrderBy(o => o.State).ToDictionary(d => d.StateID, d => d.State);

            data.OEMListItem = (from mct in db.tblMstCommonTypes.AsNoTracking()
                                where mct.IsValid != false && mct.MasterType == "OEMName"
                                select new
                                {
                                    OEMID = mct.CommonTypeID,
                                    OEMName = mct.Description

                                }).ToDictionary(d => d.OEMID, d => d.OEMName);

            data.CompanyListItem = (from mct in db.tblMstCommonTypes.AsNoTracking()
                                    where mct.IsValid != false && mct.MasterType == "CompanyName"
                                    select new
                                    {
                                        CompanyID = mct.CommonTypeID,
                                        Company = mct.Description

                                    }).ToDictionary(d => d.CompanyID, d => d.Company);

            return data;
        }

        [HttpGet]
        public ActionResult GetAssistViewDetailForVendor(string AssitId)
        {
            try
            {
                string UserName = HttpContext.User.Identity.Name;
                decimal Assitid = Convert.ToDecimal(AssitId);
                AssistModel assistDetails = GetAssistViewDetailForUpdate(Assitid);

                return View(assistDetails);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        private AssistModel GetAssistViewDetailForUpdate(decimal AssitId)
        {
            AssistModel data = new AssistModel();

            tblAssistDetail objTran = db.tblAssistDetails.Where(x => x.Id == AssitId).FirstOrDefault();

            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                data = (from a in db.tblAssistDetails.AsNoTracking()
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                        from Incident in objincident.DefaultIfEmpty()
                        where a.Id == AssitId
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            Address = a.Address,
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            PinCode = a.PinCode,
                            Remarks = a.Remarks,
                            Action = "",
                            Status = status.Status,
                            ActionID = 0,
                            Make = t.tblMstMake.Name,
                            Model = t.tblMstModel.Name,
                            EngineNo = t.EngineNo,
                            ChassisNo = t.ChassisNo,
                            RegistrationNo = t.RegistrationNo,
                            FirstName = (!string.IsNullOrEmpty(a.CustomerName)) ? a.CustomerName : t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            AssistID = a.Id,
                            BranchName = "",
                            Vendor_BranchID = 0,
                            State = a.State,
                            City = a.City,
                            location = a.Location,
                            StreetAddress = a.StreetAddress,
                            IncidentDetail = Incident.IncidentDetail,
                            VehicleColor = a.VehicleColor,
                            VehicleYear = a.VehicleYear,
                            CertificateStartDate = t.CoverStartDate,
                            CertificateEndDate = t.CoverEndDate,
                            CertificateNo = t.CertificateNo,
                            IncidentDetailId = (int)a.IncidentDetailid,
                            //AssistSummary = a.SummaryDetails,
                            AssistSummaryHistory = a.SummaryDetails,
                            DropLocation = a.DropLocationAddress,
                            DropLocationKM = a.DropLocationKM,
                            AssistCompanyID = a.AssistCompanyID,
                            hdnAssistCompanyID = a.AssistCompanyID,
                            VendorReachedIncidentLocationDateTime = null,
                            VendorReachedDropLocationDateTime = null

                        }).FirstOrDefault();
            }
            else
            {
                data = (from a in db.tblAssistDetails.AsNoTracking()
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                        from Action in objAction.DefaultIfEmpty()
                        join v in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals v.BranchID into objVendor
                        from Vendor in objVendor.DefaultIfEmpty()
                        join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                        from Incident in objincident.DefaultIfEmpty()
                        where a.Id == AssitId
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            Address = a.Address,
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            PinCode = a.PinCode,
                            Remarks = a.Remarks,
                            Action = Action.Status,
                            Status = status.Status,
                            ActionID = (int)a.ActionId,
                            Make = t.tblMstMake.Name,
                            Model = t.tblMstModel.Name,
                            EngineNo = t.EngineNo,
                            ChassisNo = t.ChassisNo,
                            RegistrationNo = t.RegistrationNo,
                            FirstName = (!string.IsNullOrEmpty(a.CustomerName)) ? a.CustomerName : t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            AssistID = a.Id,
                            BranchName = Vendor.BranchName,
                            Vendor_BranchID = Vendor.BranchID,
                            State = a.State,
                            City = a.City,
                            location = a.Location,
                            StreetAddress = a.StreetAddress,
                            IncidentDetail = Incident.IncidentDetail,
                            VehicleColor = a.VehicleColor,
                            VehicleYear = a.VehicleYear,
                            CertificateStartDate = t.CoverStartDate,
                            CertificateEndDate = t.CoverEndDate,
                            CertificateNo = t.CertificateNo,
                            IncidentDetailId = (int)a.IncidentDetailid,
                            //AssistSummary = a.SummaryDetails,
                            AssistSummaryHistory = a.SummaryDetails,
                            RemarkId = a.Remark1,
                            DropLocation = a.DropLocationAddress,
                            DropLocationKM = a.DropLocationKM,
                            AssistCompanyID = a.AssistCompanyID,
                            hdnAssistCompanyID = a.AssistCompanyID,
                            VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                            VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime,
                            strLatitudeLoc = a.Lat,
                            strLongitudeLoc = a.Lon

                        }).FirstOrDefault();
            }

            var incidentDetails = db.tblMstIncidentDetails.Where(x => x.Detailid == data.IncidentDetailId).ToList();

            data.lstIncidentDetails = incidentDetails
                .Select(x => new SelectListItem { Text = x.IncidentDetail, Value = x.Detailid.ToString() }).ToList();

            var type = incidentDetails.FirstOrDefault();

            if (type != null)
            {
                data.lstIncidentType = db.tblMstIncidents.Where(x => x.IsValid == true && x.Incidentid == type.IncidentDetailid)
                  .Select(x => new SelectListItem { Text = x.IncidentType, Value = x.Incidentid.ToString() }).ToList();

                data.IncidentTypeId = (int)type.IncidentDetailid;
            }

            //
            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                data.lstVendorName = db.tblMstVendors.Where(x => x.IsVlid != false).Select(x =>
                new SelectListItem { Text = x.VendorName, Value = x.VendorID.ToString() }).OrderBy(o => o.Text).ToList();
            }
            else
            {
                var BranchDetails = db.tblVendorBranchDetails.Where(x => x.BranchID == data.Vendor_BranchID).ToList();

                data.lstBranchName = BranchDetails
                    .Select(x => new SelectListItem { Text = x.BranchName, Value = x.BranchID.ToString() }).ToList();

                var BName = BranchDetails.FirstOrDefault();

                if (BName != null)
                {
                    data.lstVendorName = db.tblMstVendors.Where(x => x.IsVlid == true && x.VendorID == BName.VendorID)
                      .Select(x => new SelectListItem { Text = x.VendorName, Value = x.VendorID.ToString() }).ToList();

                    data.Vendor_NameId = (int)BName.VendorID;
                }
            }
            //

            data.lstStatus = db.tblMstWorkFlowStatus.Where(x => x.StatusID == 10).Select(x =>
                new SelectListItem { Text = x.Status, Value = x.StatusID.ToString() }).ToList();

            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
            {
                data.RemarkListItem = db.tblMstCommonTypes.Where(w => w.MasterType == "CaseClosureRemark" && w.IsValid != false)
                     .Select(x => new SelectListItem { Text = x.Description, Value = x.CommonTypeID.ToString() }).ToList();

                data.RemId = Convert.ToInt32(data.RemarkId);
            }

            data.lstStateItem = (from vbd in db.tblVendorBranchDetails.AsNoTracking()
                                 join s in db.tblMstStates.AsNoTracking() on vbd.State equals s.StateID into Statebased
                                 from Statebasedobj in Statebased.DefaultIfEmpty()
                                 select new
                                 {
                                     StateID = Statebasedobj.StateID,
                                     State = Statebasedobj.State

                                 }).Distinct().OrderBy(o => o.State).ToDictionary(d => d.StateID, d => d.State);

            data.CompanyListItem = (from mct in db.tblMstCommonTypes.AsNoTracking()
                                    where mct.IsValid != false && mct.MasterType == "CompanyName"
                                    select new
                                    {
                                        CompanyID = mct.CommonTypeID,
                                        Company = mct.Description

                                    }).ToDictionary(d => d.CompanyID, d => d.Company);

            return data;
        }

        [HttpGet]
        public ActionResult GetAssistViewDetailForVendorOEM(string AssitId)
        {
            try
            {
                string UserName = HttpContext.User.Identity.Name;
                decimal Assitid = Convert.ToDecimal(AssitId);
                AssistModel assistDetails = GetAssistViewDetailForUpdateOEM(Assitid);

                return View(assistDetails);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        private AssistModel GetAssistViewDetailForUpdateOEM(decimal AssitId)
        {
            AssistModel data = new AssistModel();

            tblAssistDetail objTran = db.tblAssistDetails.Where(x => x.Id == AssitId).FirstOrDefault();

            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                data = (from a in db.tblAssistDetails.AsNoTracking()
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                        from Incident in objincident.DefaultIfEmpty()
                        where a.Id == AssitId
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            Address = a.Address,
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            PinCode = a.PinCode,
                            Remarks = a.Remarks,
                            Action = "",
                            Status = status.Status,
                            ActionID = 0,
                            Make = t.tblMstMake.Name,
                            Model = t.tblMstModel.Name,
                            EngineNo = t.EngineNo,
                            ChassisNo = t.ChassisNo,
                            RegistrationNo = t.RegistrationNo,
                            FirstName = (!string.IsNullOrEmpty(a.CustomerName)) ? a.CustomerName : t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            AssistID = a.Id,
                            BranchName = "",
                            Vendor_BranchID = 0,
                            State = a.State,
                            City = a.City,
                            location = a.Location,
                            StreetAddress = a.StreetAddress,
                            IncidentDetail = Incident.IncidentDetail,
                            VehicleColor = a.VehicleColor,
                            VehicleYear = a.VehicleYear,
                            CertificateStartDate = t.CoverStartDate,
                            CertificateEndDate = t.CoverEndDate,
                            CertificateNo = t.CertificateNo,
                            IncidentDetailId = (int)a.IncidentDetailid,
                            //AssistSummary = a.SummaryDetails,
                            AssistSummaryHistory = a.SummaryDetails,
                            DropLocation = a.DropLocationAddress,
                            DropLocationKM = a.DropLocationKM,
                            AssistCompanyID = a.AssistCompanyID,
                            hdnAssistCompanyID = a.AssistCompanyID,
                            VendorReachedIncidentLocationDateTime = null,
                            VendorReachedDropLocationDateTime = null

                        }).FirstOrDefault();
            }
            else
            {
                data = (from a in db.tblAssistDetails.AsNoTracking()
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join at in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals at.StatusID into objAction
                        from Action in objAction.DefaultIfEmpty()
                        join v in db.tblVendorBranchDetails.AsNoTracking() on a.VendorId equals v.BranchID into objVendor
                        from Vendor in objVendor.DefaultIfEmpty()
                        join inc in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals inc.IncidentDetailid into objincident
                        from Incident in objincident.DefaultIfEmpty()
                        where a.Id == AssitId
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            Address = a.Address,
                            Issue = a.Issue,
                            Landmark = a.Landmark,
                            PinCode = a.PinCode,
                            Remarks = a.Remarks,
                            Action = Action.Status,
                            Status = status.Status,
                            ActionID = (int)a.ActionId,
                            Make = t.tblMstMake.Name,
                            Model = t.tblMstModel.Name,
                            EngineNo = t.EngineNo,
                            ChassisNo = t.ChassisNo,
                            RegistrationNo = t.RegistrationNo,
                            FirstName = (!string.IsNullOrEmpty(a.CustomerName)) ? a.CustomerName : t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            AssistID = a.Id,
                            BranchName = Vendor.BranchName,
                            Vendor_BranchID = Vendor.BranchID,
                            State = a.State,
                            City = a.City,
                            location = a.Location,
                            StreetAddress = a.StreetAddress,
                            IncidentDetail = Incident.IncidentDetail,
                            VehicleColor = a.VehicleColor,
                            VehicleYear = a.VehicleYear,
                            CertificateStartDate = t.CoverStartDate,
                            CertificateEndDate = t.CoverEndDate,
                            CertificateNo = t.CertificateNo,
                            IncidentDetailId = (int)a.IncidentDetailid,
                            //AssistSummary = a.SummaryDetails,
                            AssistSummaryHistory = a.SummaryDetails,
                            RemarkId = a.Remark1,
                            DropLocation = a.DropLocationAddress,
                            DropLocationKM = a.DropLocationKM,
                            AssistCompanyID = a.AssistCompanyID,
                            hdnAssistCompanyID = a.AssistCompanyID,
                            VendorReachedIncidentLocationDateTime = a.VenReachedIncLocDateTime,
                            VendorReachedDropLocationDateTime = a.VenReachedDropLocDateTime,
                            strLatitudeLoc = a.Lat,
                            strLongitudeLoc = a.Lon

                        }).FirstOrDefault();
            }

            var incidentDetails = db.tblMstIncidentDetails.Where(x => x.Detailid == data.IncidentDetailId).ToList();

            data.lstIncidentDetails = incidentDetails
                .Select(x => new SelectListItem { Text = x.IncidentDetail, Value = x.Detailid.ToString() }).ToList();

            var type = incidentDetails.FirstOrDefault();

            if (type != null)
            {
                data.lstIncidentType = db.tblMstIncidents.Where(x => x.IsValid == true && x.Incidentid == type.IncidentDetailid)
                  .Select(x => new SelectListItem { Text = x.IncidentType, Value = x.Incidentid.ToString() }).ToList();

                data.IncidentTypeId = (int)type.IncidentDetailid;
            }

            //
            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                data.lstVendorName = db.tblMstVendors.Where(x => x.IsVlid != false).Select(x =>
                new SelectListItem { Text = x.VendorName, Value = x.VendorID.ToString() }).OrderBy(o => o.Text).ToList();
            }
            else
            {
                var BranchDetails = db.tblVendorBranchDetails.Where(x => x.BranchID == data.Vendor_BranchID).ToList();

                data.lstBranchName = BranchDetails
                    .Select(x => new SelectListItem { Text = x.BranchName, Value = x.BranchID.ToString() }).ToList();

                var BName = BranchDetails.FirstOrDefault();

                if (BName != null)
                {
                    data.lstVendorName = db.tblMstVendors.Where(x => x.IsVlid == true && x.VendorID == BName.VendorID)
                      .Select(x => new SelectListItem { Text = x.VendorName, Value = x.VendorID.ToString() }).ToList();

                    data.Vendor_NameId = (int)BName.VendorID;
                }
            }
            //

            data.lstStatus = db.tblMstWorkFlowStatus.Where(x => x.StatusID == 10).Select(x =>
                new SelectListItem { Text = x.Status, Value = x.StatusID.ToString() }).ToList();

            if (objTran.StatusId == CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
            {
                data.RemarkListItem = db.tblMstCommonTypes.Where(w => w.MasterType == "CaseClosureRemark" && w.IsValid != false)
                     .Select(x => new SelectListItem { Text = x.Description, Value = x.CommonTypeID.ToString() }).ToList();

                data.RemId = Convert.ToInt32(data.RemarkId);
            }

            data.lstStateItem = (from vbd in db.tblVendorBranchDetails.AsNoTracking()
                                 join s in db.tblMstStates.AsNoTracking() on vbd.State equals s.StateID into Statebased
                                 from Statebasedobj in Statebased.DefaultIfEmpty()
                                 select new
                                 {
                                     StateID = Statebasedobj.StateID,
                                     State = Statebasedobj.State

                                 }).Distinct().OrderBy(o => o.State).ToDictionary(d => d.StateID, d => d.State);

            data.CompanyListItem = (from mct in db.tblMstCommonTypes.AsNoTracking()
                                    where mct.IsValid != false && mct.MasterType == "CompanyName"
                                    select new
                                    {
                                        CompanyID = mct.CommonTypeID,
                                        Company = mct.Description

                                    }).ToDictionary(d => d.CompanyID, d => d.Company);

            return data;
        }

        public tblVendorBranchDetail getAllocatedVendor(bool isManual, double AssistLat, double AssistLon, decimal branchid = 0, bool isRelianceAssist = false)
        {
            Random rnd = new Random();
            if (isManual)
            {
                tblVendorBranchDetail selected = db.tblVendorBranchDetails.Where(x => x.BranchID == branchid).FirstOrDefault();
                return selected;
            }
            if (isRelianceAssist)
            {
                tblVendorBranchDetail branch = db.tblVendorBranchDetails.Where(x => x.BranchName == "Global Assure").FirstOrDefault();
                return branch;
            }
            else  // Auto allocation 
            {
                List<BranchDetails> lstBranch = db.tblVendorBranchDetails.Select(x => new BranchDetails
                { BranchID = x.BranchID, Latitudes = x.Latitudes, Longitudes = x.Longitudes }).ToList();
                double Distance = 0;
                Decimal BranchId = 0;

                if (lstBranch.Any())
                {
                    foreach (var item in lstBranch)
                    {
                        double newDistance = DistanceInMeter(Convert.ToDouble(item.Latitudes), Convert.ToDouble(item.Longitudes), AssistLat, AssistLon);
                        if (Distance == 0 || newDistance < Distance)
                        {
                            Distance = newDistance;
                            BranchId = item.BranchID;
                        }
                    }
                }

                tblVendorBranchDetail selected = db.tblVendorBranchDetails.Where(x => x.BranchID == BranchId).FirstOrDefault();
                return selected;
            }

        }

        private double DistanceInMeter(double lat1, double lon1, double lat2, double lon2)
        {
            var sCoord = new GeoCoordinate(lat1, lon1);
            var eCoord = new GeoCoordinate(lat2, lon2);
            double distance = sCoord.GetDistanceTo(eCoord);
            return distance;
        }

        public ActionResult Vendor()
        {
            return View();
        }

        #region ReliancePolicy

        #region RelianceRSA
        //public async System.Threading.Tasks.Task<ActionResult> RelianceRSA(string data)
        //{
        //    GlobalAssureEncryption enc = new GlobalAssureEncryption();
        //    AssistRequest request = new AssistRequest();

        //    string encriptMessage = enc.Encrypt("User=RelianceAssist@gmail.com&Pass=Assist@123");
        //    data = encriptMessage;
        //    string DecriptMessage = enc.Decrypt(data);
        //    string[] SplitMessage = DecriptMessage.Split('&');
        //    string[] para = new string[10];




        //    for (int i = 0; i < SplitMessage.Length; i++)
        //    {
        //        string[] str = SplitMessage[i].Split('=');
        //        para[i] = str[1];
        //    }

        //    request.User = para[0] == "" ? null : para[1];
        //    request.Password = para[1] == "" ? null : para[2].TrimStart('\0');

        //    try
        //    {
        //        if (string.IsNullOrEmpty(request.User) || string.IsNullOrEmpty(request.Password))
        //        {

        //            ViewBag.Error = "Invalid User";
        //            return View("~/Views/WebPortal/Error.cshtml");
        //        }
        //        else
        //        {
        //            bool validUser = await objWeb.isValidUser(request.User, request.Password);
        //            string HTML = String.Empty;
        //            if (validUser)
        //            {
        //                return View();
        //            }
        //            else
        //            {
        //                ViewBag.Error = "Invalid User";
        //                return View("~/Views/WebPortal/Error.cshtml");
        //            }

        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        HandleException(e);
        //        throw;
        //    }
        //}
        #endregion

        #region GetReliancePolicyDetails
        public JsonResult GetReliancePolicyDetails(string RegNo, string PolicyNo)
        {
            string UserID = System.Configuration.ConfigurationManager.AppSettings["Userid"];
            string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];
            try
            {
                string HTML = string.Empty;
                string message = string.Empty;
                string status = string.Empty;
                ReliancePolicyModel model = new ReliancePolicyModel();
                RGICL_Policy_Service_RSAClient client = new RGICL_Policy_Service_RSAClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 1, 0); // Timeout 1 min

                string Param = string.Empty;


                if (!string.IsNullOrEmpty(RegNo))
                {
                    Param = RegNo;
                }
                else if (!string.IsNullOrEmpty(PolicyNo))
                {
                    Param = PolicyNo;
                }
                else
                {
                    return Json(new { message = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
                }


                logger.Error("Reliance Service Befor Request:: Userid => " + UserID + "  Password =>" + Password + "  Param =>" + Param);

                var Response = client.GetPolicyDetailforRSA(UserID, Password, Param.Trim());


                logger.Error("Reliance Service :: Response => " + Response);

                if (Response != null)
                {


                    model.ChasisNo = Response.ChassisNumber;
                    model.EngineNo = Response.EngineNumber;
                    model.InsuredName = Response.InsuredName;
                    model.Make = Response.Make;
                    model.Model = Response.Model;
                    model.PolicyNumber = Response.PolicyNumber;
                    model.PolicyStatus = Response.PolicyStatus;
                    model.RiskStartDate = Response.RiskStartDate;
                    model.RiskEndDate = Response.RiskEndDate;
                    model.VehicleNo = Response.VehicleNumber;
                    model.ResponseStatus = Response.ResponseStatus;
                    model.RSAVendorName = Response.InsuredName;
                    model.ProductCode = Response.ProductCode;
                    model.InsuredphoneNumber = Response.InsuredphoneNumber;
                    model.InsuredMobileNumber = Response.InsuredMobileNumber;
                    model.InsuredEmailID = Response.InsuredEmailID;
                    model.InsuredAddressLine = Response.InsuredAddressLine;
                    model.InsuredCityName = Response.InsuredCityName;
                    model.InsuredDistrictName = Response.InsuredDistrictName;

                    DateTime GlobalStartDate = DateTime.Parse("18MAY2019");   // This date shoul next release date.

                    if (Convert.ToDateTime(model.RiskStartDate) < GlobalStartDate)
                    {
                        ViewData["RILVendor"] = "Roadzen";
                    }
                    else
                    {
                        StateVendorMapping objVendorMapping = getRILVendorMappingInfo(Response.PolicyNumber).Result;
                        if (objVendorMapping != null)
                        {
                            ViewData["RILVendor"] = objVendorMapping.Vendor;
                        }
                        else
                        {
                            ViewData["RILVendor"] = "Vendor Not Found";
                        }
                    }

                    ViewData["Param"] = Param;
                    status = CrossCutting_Constant.Success;
                    HTML = RenderPartialViewToString("~/Views/Assistance/GetReliancePolicyDetails.cshtml", model);
                }
                else
                {
                    status = CrossCutting_Constant.Failure;
                    message = "Sorry! Something went wrong. Please try again!";
                }
                client.Close();

                return Json(new { message = status, HTML = HTML, ErroMessage = message }, JsonRequestBehavior.AllowGet);

            }

            catch (Exception e)
            {
                logger.Error("Reliance Service :: Response Error=>  Userid => " + UserID + "  Password =>" + Password + "  Param =>" + PolicyNo + "||" + RegNo);
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure, AssistRefNo = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<StateVendorMapping> getRILVendorMappingInfo(string policyNumber)
        {
            //SU1001
            // string URL = "https://xpas.reliancegeneral.co.in/StateVendorMappingInfo/Service.svc/GetStateMasterVendorMapping";
            string URL = Convert.ToString(ConfigurationManager.AppSettings["RelianceVenderMappingURL"]);
            //EC1001

            string urlParameters = "{ \"PolicyNumber\": \"" + policyNumber + "\"}";


            StateVendorMapping result = new StateVendorMapping();
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.PostAsync(URL, new StringContent(urlParameters, Encoding.UTF8, "application/json"));
                // List data response.

                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(response.Content.ReadAsStringAsync().Result);

                    XmlNamespaceManager ns = new XmlNamespaceManager(xmldoc.NameTable);
                    ns.AddNamespace("ns", "http://schemas.datacontract.org/2004/07/");

                    XmlNode extNode = xmldoc.SelectSingleNode("//ns:Exception", ns);

                    if (extNode != null && extNode.InnerText.ToLower().Contains("success"))
                    {
                        XmlNode _node = xmldoc.SelectSingleNode("//ns:StateName", ns);
                        result.StateName = _node.InnerText;

                        _node = xmldoc.SelectSingleNode("//ns:State_Code", ns);
                        result.State_Code = _node.InnerText;

                        _node = xmldoc.SelectSingleNode("//ns:Vendor", ns);
                        result.Vendor = _node.InnerText;

                    }

                }

            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return result;
        }
        #endregion

        [HttpPost]
        public ActionResult SaveReliancePolicy(string Param)
        {
            tblTransaction objTran = new tblTransaction();
            RGICL_Policy_Service_RSAClient client = new RGICL_Policy_Service_RSAClient();

            try
            {

                string UserID = System.Configuration.ConfigurationManager.AppSettings["Userid"];
                string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];
                string UserName = User.Identity.Name;
                AspNetUser userdata = db.AspNetUsers.Where(w => w.UserName == UserName).FirstOrDefault();
                ViewData["TieUpCompany"] = userdata.TieUpCompanyID;
                var Response = client.GetPolicyDetailforRSA(UserID, Password, Param.Trim());
                decimal Transactionid;


                if (Response != null)
                {
                    DateTime CoverStartDate, CoverEndDate;

                    decimal tran = db.tblTransactions.Where(x => x.RegistrationNo == Response.VehicleNumber || x.EngineNo == Response.EngineNumber)
                        .Select(x => x.TransactionID).FirstOrDefault();
                    if (tran > 0)
                    {
                        Transactionid = tran;

                    }
                    else
                    {

                        DateTime.TryParseExact(Response.RiskStartDate, "dd MMM yyyy",
                        System.Globalization.CultureInfo.InvariantCulture,
                        System.Globalization.DateTimeStyles.None, out CoverStartDate);

                        DateTime.TryParseExact(Response.RiskEndDate, "dd MMM yyyy",
                        System.Globalization.CultureInfo.InvariantCulture,
                        System.Globalization.DateTimeStyles.None, out CoverEndDate);


                        var Model = db.tblMstModels.Where(x => x.Name.Equals(Response.Model)).
                           FirstOrDefault();

                        if (Model == null)
                        {
                            return Json(new { status = CrossCutting_Constant.Failure, Message = "Vehicle Model not found" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {


                            objTran.CertificateNo = Response.PolicyNumber;
                            objTran.ChassisNo = Response.ChassisNumber;
                            objTran.CustomerEmail = Response.InsuredEmailID;
                            objTran.CustomerName = Response.InsuredName;
                            objTran.PermanentAddress = Response.InsuredAddressLine;
                            objTran.EngineNo = Response.EngineNumber;
                            objTran.IsPaymentMade = true;
                            objTran.IsPaymentReceived = true;
                            objTran.RegistrationNo = Response.VehicleNumber;
                            objTran.StateName = Response.InsuredStateName;
                            objTran.StatusID = CrossCutting_Constant.StatusID_Payment_Entry;
                            objTran.UserID = userdata.Id;
                            objTran.VendorDataId = CrossCutting_Constant.TieUpRelianceAssist;
                            objTran.IsSameAsPermanentAddress = true;
                            objTran.CoverStartDate = CoverStartDate;
                            objTran.CoverEndDate = CoverEndDate;
                            objTran.MakeID = Model.MakeID;
                            objTran.ModelID = Model.ModelID;
                            objTran.IsValid = true;
                            objTran.CreatedDate = DateTime.Now;

                            var result = db.tblTransactions.Add(objTran);
                            db.SaveChanges();
                            Transactionid = result.TransactionID;
                        }
                    }
                    AssistModel assistDetails = new AssistModel();
                    assistDetails = this.GetAssistDetail(Transactionid);
                    assistDetails.ActionID = CrossCutting_Constant.ActionID_Vendor_No_Action;

                    string HTML = RenderPartialViewToString("~/Views/Assistance/GetAssistDetails.cshtml", assistDetails);
                    return Json(new { status = CrossCutting_Constant.Success, HTML = HTML }, JsonRequestBehavior.AllowGet);


                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Failure, Message = "Service Error" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                logger.Error(e.InnerException);
                return Json(new { status = CrossCutting_Constant.Failure, Message = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region RenderPartialViewToString
        public string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        #region HandleException
        public static bool HandleException(Exception ex)
        {
            bool rethrow = false;
            rethrow = UIExceptionHandler.HandleException(ref ex);
            if (rethrow)
            {
                throw ex;
            }
            return false;
        }
        #endregion

        public ActionResult SearchAssistancebyCertificateNo(string CertificateNo)
        {
            return View();
        }

        public ActionResult SearchAssistbyCertiNo(string CertificateNo)
        {
            try
            {
                List<AssistModel> list = new List<AssistModel>();
                ViewBag.CertificateNo = CertificateNo;

                TempData["Load"] = "FirstTime";
                list = this.SearchAssistancebyCertificateList(CertificateNo);

                if (!string.IsNullOrEmpty(list[0].AssistanceRefNo))
                {
                    var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                    var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                    var Html = RenderPartialViewToString("~/Views/Assistance/_SearchAssistancebyCertificateGrid.cshtml", Grid);
                    return Json(new { status = CrossCutting_Constant.Success, message = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Success, message = CrossCutting_Constant.NoAssistanceFound }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchAssistancebyCertificateGridNextpage(int? Page, string CertificateNo, int SortingOrder = 0, int Paging = 5)
        {
            try
            {
                TempData["Load"] = "FirstTime";
                ViewBag.CertificateNo = CertificateNo;

                List<AssistModel> list = new List<AssistModel>();
                list = this.SearchAssistancebyCertificateList(CertificateNo);

                if (!string.IsNullOrEmpty(list[0].AssistanceRefNo))
                {
                    var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                    var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                    var Html = RenderPartialViewToString("~/Views/Assistance/SearchCertificateGridNextpage.cshtml", grid);
                    return Json(new { status = CrossCutting_Constant.Success, message = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Success, message = CrossCutting_Constant.NoAssistanceFound }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        public List<AssistModel> SearchAssistancebyCertificateList(string CertificateNo)
        {
            List<AssistModel> list = new List<AssistModel>();
            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID }).FirstOrDefault();

            list = (from t in db.tblTransactions.AsNoTracking()
                    join p in db.tblMstMakes.AsNoTracking() on t.MakeID equals p.MakeID into makebased
                    from makebasedobj in makebased.DefaultIfEmpty()
                    join model in db.tblMstModels on t.ModelID equals model.ModelID into modelbased
                    from modelbasedobj in modelbased.DefaultIfEmpty()
                    join pstats in db.tblMstWorkFlowStatus on t.StatusID equals pstats.StatusID into statusbased
                    from statusbasedobj in statusbased.DefaultIfEmpty()
                    join asstdetails in db.tblAssistDetails on t.TransactionID equals asstdetails.TransactionId into assistbased
                    from assistbasedobj in assistbased.DefaultIfEmpty()
                    where (t.CertificateNo.Contains(CertificateNo))
                    select new AssistModel
                    {
                        TransactionID = t.TransactionID,
                        CertificateNo = t.CertificateNo,
                        AssistanceRefNo = assistbasedobj.RefNo,
                        Make = makebasedobj.Name,
                        Model = modelbasedobj.Name,
                        Status = statusbasedobj.Status,
                        FirstName = t.CustomerName,
                        MiddleName = t.CustomerMiddleName,
                        LastName = t.CustomerLastName,
                        Email = t.CustomerEmail,
                        Mobile = t.CustomerContact,
                        RegistrationNo = t.RegistrationNo
                    }).ToList();

            return list;
        }

        public JsonResult GetAssistDetailsbyCertificateNo(string TransactionID, string AssistanceRefNo)
        {
            string HTML = String.Empty;

            try
            {
                string UserName = HttpContext.User.Identity.Name;
                var user = db.AspNetUsers.Where(x => x.UserName == UserName).FirstOrDefault();
                ViewData["TieUpCompany"] = user.TieUpCompanyID;

                decimal Transactionid = Convert.ToDecimal(TransactionID);
                AssistModel assistDetails = new AssistModel();
                assistDetails = this.GetAssistDetailbyCertiNo(Transactionid, AssistanceRefNo);
                assistDetails.ActionID = CrossCutting_Constant.ActionID_Vendor_No_Action;

                HTML = RenderPartialViewToString("~/Views/Assistance/GetAssistDetailsbyCertificateNo.cshtml", assistDetails);
                return Json(new { message = CrossCutting_Constant.Success, HTML = HTML, isRecentAssistOpen = assistDetails.isRecentAssistOpen }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        public AssistModel GetAssistDetailbyCertiNo(decimal transactionid, string AssistanceRefNo)
        {
            AssistModel data = new AssistModel();

            tblTransaction objTran = db.tblTransactions.Where(x => x.TransactionID == transactionid).FirstOrDefault();
            tblAssistDetail objAssist = db.tblAssistDetails.Where(a => a.RefNo == AssistanceRefNo).FirstOrDefault();

            if (objAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                data = (from t in db.tblTransactions.AsNoTracking()
                        join p in db.tblMstMakes.AsNoTracking() on t.MakeID equals p.MakeID into makebased
                        from makebasedobj in makebased.DefaultIfEmpty()
                        join model in db.tblMstModels on t.ModelID equals model.ModelID into modelbased
                        from modelbasedobj in modelbased.DefaultIfEmpty()
                        join asstdetail in db.tblAssistDetails on t.TransactionID equals asstdetail.TransactionId into asstbased
                        from asstbasedobj in asstbased.DefaultIfEmpty()
                        join id in db.tblMstIncidentDetails on asstbasedobj.IncidentDetailid equals id.Detailid into idbased
                        from idbasedobj in idbased.DefaultIfEmpty()
                        join vbd in db.tblVendorBranchDetails on asstbasedobj.VendorId equals vbd.BranchID into vbdbased
                        from vbdbasedobj in vbdbased.DefaultIfEmpty()
                        where (t.TransactionID == transactionid)
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            CertificateNo = t.CertificateNo,
                            AssistanceRefNo = asstbasedobj.RefNo,
                            Make = makebasedobj.Name,
                            Model = modelbasedobj.Name,
                            FirstName = t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            RegistrationNo = t.RegistrationNo,
                            EngineNo = t.EngineNo,
                            ChassisNo = t.ChassisNo,
                            CertificateStartDate = t.CoverStartDate,
                            CertificateEndDate = t.CoverEndDate,
                            IncidentTypeId = (int)idbasedobj.IncidentDetailid,
                            IncidentDetailId = (int)asstbasedobj.IncidentDetailid,
                            VehicleYear = asstbasedobj.VehicleYear,
                            VehicleColor = asstbasedobj.VehicleColor,
                            Issue = asstbasedobj.Issue,
                            Landmark = asstbasedobj.Landmark,
                            PinCode = asstbasedobj.PinCode,
                            Address = asstbasedobj.Address,
                            BranchName = vbdbasedobj.BranchName,
                            AssistSummary = asstbasedobj.SummaryDetails,
                            Vendor_BranchID = 0

                        }).FirstOrDefault();
            }
            else
            {
                data = (from t in db.tblTransactions.AsNoTracking()
                        join p in db.tblMstMakes.AsNoTracking() on t.MakeID equals p.MakeID into makebased
                        from makebasedobj in makebased.DefaultIfEmpty()
                        join model in db.tblMstModels on t.ModelID equals model.ModelID into modelbased
                        from modelbasedobj in modelbased.DefaultIfEmpty()
                        join asstdetail in db.tblAssistDetails on t.TransactionID equals asstdetail.TransactionId into asstbased
                        from asstbasedobj in asstbased.DefaultIfEmpty()
                        join id in db.tblMstIncidentDetails on asstbasedobj.IncidentDetailid equals id.Detailid into idbased
                        from idbasedobj in idbased.DefaultIfEmpty()
                        join vbd in db.tblVendorBranchDetails on asstbasedobj.VendorId equals vbd.BranchID into vbdbased
                        from vbdbasedobj in vbdbased.DefaultIfEmpty()
                        where (t.TransactionID == transactionid)
                        select new AssistModel
                        {
                            TransactionID = t.TransactionID,
                            CertificateNo = t.CertificateNo,
                            AssistanceRefNo = asstbasedobj.RefNo,
                            Make = makebasedobj.Name,
                            Model = modelbasedobj.Name,
                            FirstName = t.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            Email = t.CustomerEmail,
                            Mobile = t.CustomerContact,
                            RegistrationNo = t.RegistrationNo,
                            EngineNo = t.EngineNo,
                            ChassisNo = t.ChassisNo,
                            CertificateStartDate = t.CoverStartDate,
                            CertificateEndDate = t.CoverEndDate,
                            //IncidentTypeId = (int)idbasedobj.IncidentDetailid,
                            //IncidentDetailId = (int)asstbasedobj.IncidentDetailid,
                            IncidentTypeId = (idbasedobj != null) ? (int)idbasedobj.IncidentDetailid : 0,
                            IncidentDetailId = (asstbasedobj != null) ? (int)asstbasedobj.IncidentDetailid : 0,
                            VehicleYear = asstbasedobj.VehicleYear,
                            VehicleColor = asstbasedobj.VehicleColor,
                            Issue = asstbasedobj.Issue,
                            Landmark = asstbasedobj.Landmark,
                            PinCode = asstbasedobj.PinCode,
                            Address = asstbasedobj.Address,
                            BranchName = vbdbasedobj.BranchName,
                            AssistSummary = asstbasedobj.SummaryDetails,
                            //Vendor_BranchID = vbdbasedobj.BranchID
                            Vendor_BranchID = (vbdbasedobj != null) ? vbdbasedobj.BranchID : 0                        

                        }).FirstOrDefault();
            }

            data.lstIncidentType = db.tblMstIncidents.Where(x => x.IsValid == true).OrderBy(x => x.OrderBy)
                .Select(x => new SelectListItem { Text = x.IncidentType, Value = x.Incidentid.ToString() }).ToList();

            data.lstIncidentDetails = db.tblMstIncidentDetails.Where(x => x.IsValid == true)
                .Select(x => new SelectListItem { Text = x.IncidentDetail, Value = x.Detailid.ToString() }).ToList();

            //
            if (objAssist.StatusId == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                data.Vendor_NameId = 0;
                data.Vendor_BranchID = 0;
            }
            else
            {
                var BranchDetails = db.tblVendorBranchDetails.Where(x => x.BranchID == data.Vendor_BranchID).ToList();

                data.lstBranchName = BranchDetails
                    .Select(x => new SelectListItem { Text = x.BranchName, Value = x.BranchID.ToString() }).ToList();

                var BName = BranchDetails.FirstOrDefault();

                if (BName != null)
                {
                    data.lstVendorName = db.tblMstVendors.Where(x => x.IsVlid == true && x.VendorID == BName.VendorID)
                      .Select(x => new SelectListItem { Text = x.VendorName, Value = x.VendorID.ToString() }).ToList();

                    data.Vendor_NameId = (int)BName.VendorID;
                }
            }
            //

            var AssistDetails = db.tblAssistDetails.Where(x => x.TransactionId == transactionid).FirstOrDefault();
            data.isRecentAssistOpen = false;
            if (AssistDetails != null)
            {
                if (AssistDetails.CreatedDate.Value.Date >= DateTime.Today.AddDays(-1) && AssistDetails.StatusId != CrossCutting_Constant.StatusID_Vendor_Closed_Issue)
                {
                    data.isRecentAssistOpen = true;
                }
            }


            return data;

        }

        //sa 1006
        #region MISDownload

        //public List<AssistModel> GetMISreport()
        //{
        //    List<AssistModel> lstMIS = new List<AssistModel>();
        //    lstMIS = (from a in db.tblAssistDetails.AsNoTracking()
        //              join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
        //              select new AssistModel
        //              {
        //                  TransactionID = t.TransactionID,
        //                  Address = a.Address,
        //                  Issue = a.Issue,
        //                  Landmark = a.Landmark,
        //                  PinCode = a.PinCode,
        //                  Remarks = a.Remarks
        //              }).ToList();

        //    return lstMIS;
        //}
        //public ActionResult MISReports()
        //{
        //    return View(this.GetMISreport());
        //}

        //[HttpPost]
        //public ActionResult DownloadMIS()
        //{
        //    var gv = new GridView();
        //    gv.DataSource = this.GetMISreport();
        //    gv.DataBind();

        //    Response.ClearContent();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
        //    Response.ContentType = "application/ms-excel";

        //    Response.Charset = "";
        //    StringWriter objStringWriter = new StringWriter();
        //    HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);

        //    gv.RenderControl(objHtmlTextWriter);
        //    Response.Output.Write(objStringWriter.ToString());
        //    Response.Flush();
        //    Response.End();


        //    return View("Index");
        //}
        #endregion
        //ea 1006

        //sa 1007
        public JsonResult GetVendorName(string UserKey)
        {
            try
            {
                List<SelectListItem> _lstVendorName = db.tblMstVendors.Where(a => a.VendorName.Contains(UserKey) && a.IsVlid == true).Select(a => new SelectListItem { Text = a.VendorName, Value = a.VendorID + "" }).ToList();
                return Json(_lstVendorName, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }
        //ea 1007

        public JsonResult GetVendorByArea(string UserKey)
        {
            try
            {
                List<SelectListItem> _lstVendorName = db.tblVendorBranchDetails.Where(a => (a.Address1.ToLower().Contains(UserKey.ToLower()) || a.Address2.ToLower().Contains(UserKey.ToLower())) && a.Status == true).Select(a => new SelectListItem { Text = a.Address1 + " - " + a.Address2, Value = a.BranchID + "" }).ToList();
                return Json(_lstVendorName, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }


        [HttpGet]
        public JsonResult GetAuditDetails(decimal AssitId)
        {
            string HTML = String.Empty;
            
            try
            {

                string UserName = HttpContext.User.Identity.Name;
                List<AssistAuditDetails> list = new List<AssistAuditDetails>();
                ViewBag.AssistID = AssitId;
                TempData["Load"] = "FirstTime";
                list = this.GetAuditDetailList(AssitId);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SearchAuditdetailsGrid.cshtml", Grid);
                return Json(new { status = CrossCutting_Constant.Success, HTML = Html }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }


        }

        public ActionResult GetAuditDetailsNextPage(int? Page,decimal AssitId, int SortingOrder = 0, int Paging = 5)
        {
            string HTML = String.Empty;

            try
            {
                ViewBag.AssistID = AssitId;
                List<AssistAuditDetails> list = new List<AssistAuditDetails>();                
                list = this.GetAuditDetailList(AssitId);
                var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                var Html = RenderPartialViewToString("~/Views/Assistance/_SeachAuditdetailGridNextPage.cshtml", grid);
                return Json(new { status = CrossCutting_Constant.Success, HTML = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        private List<AssistAuditDetails> GetAuditDetailList(decimal AssitId)
        {
            string ErrorMessage = string.Empty;
            List<AssistAuditDetails> list = new List<AssistAuditDetails>();
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string sql = "usp_GetAssistDetails";
            try
            {
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = new SqlCommand(sql, conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.AddWithValue("@AssistID", AssitId);
                        da.SelectCommand.Parameters.AddWithValue("@s_Error_Message", ErrorMessage);
                        da.SelectCommand.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;

                        da.Fill(ds, "data");

                        ErrorMessage = Convert.ToString(da.SelectCommand.Parameters["@s_Error_Message"].Value);
                    }
                }

                DataTable dt = ds.Tables["data"];

                if (dt.Rows.Count > 0 && dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        list.Add(new AssistAuditDetails
                        {
                            UserName = Convert.ToString(row["LoginUserName"]),
                            RefNo = Convert.ToString(row["RefNo"]),
                            IncidentDetail = Convert.ToString(row["IncidentDetail"]),
                            Issue = Convert.ToString(row["CustomerMobileNo"]),
                            VendorName = Convert.ToString(row["VendorName"]),
                            BranchName = Convert.ToString(row["BranchName"]),
                            Summary = Convert.ToString(row["Summary"]),
                            Status = Convert.ToString(row["Status"]),
                            Remarks = Convert.ToString(row["Action"]),
                            CreatedDate = Convert.ToString(row["CreatedDateTime"]),
                            location = Convert.ToString(row["Location"]),
                            Landmark = Convert.ToString(row["Landmark"]),
                            PinCode = Convert.ToString(row["PinCode"])
                        });
                    }
                }
            }
            catch(Exception ex)
            {
                HandleException(ex);
            }      


            return list;
        }

        #region SearchVendorForAssistance
        public JsonResult SearchVendorForAssistance(AssistModel AssistModelObj)
        {
            List<VendorsListModel> list = new List<VendorsListModel>();
            VendorForAssistModel model = new VendorForAssistModel();
            var isDataFound = false;
            var isAreaSearched = false;

            string strIncDetailId = Convert.ToString(AssistModelObj.IncidentDetailId);
            string[] lstIncDetailIds = ConfigurationManager.AppSettings["IncDetailIds"].Split(',');

            try
            {
                decimal AssistID = Convert.ToDecimal(AssistModelObj.AssistID);

                string IncidentLocation = db.tblAssistDetails.Where(w => w.Id == AssistID).Select(s => s.Address).FirstOrDefault();
                if (string.IsNullOrEmpty(IncidentLocation))
                {
                    string ErrMsg = "Incident Location Not Found !, Update Incident Location First";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                }

                var tblAssistDetail = db.tblAssistDetails.Where(w => w.Id == AssistID).FirstOrDefault();
                if (tblAssistDetail != null)
                {
                    string DropLocationAddress = (!string.IsNullOrEmpty(tblAssistDetail.DropLocationAddress)) ? tblAssistDetail.DropLocationAddress : "";
                    string DropLocationKM = (!string.IsNullOrEmpty(tblAssistDetail.DropLocationKM)) ? tblAssistDetail.DropLocationKM : "";
                    if (!string.IsNullOrEmpty(DropLocationAddress) && !string.IsNullOrEmpty(DropLocationKM))
                    {
                        var AssistLatLong = db.tblAssistDetails.Where(w => w.Id == AssistID).Select(s => new { s.Lat, s.Lon }).FirstOrDefault();
                        double AssistLat = Convert.ToDouble(AssistLatLong.Lat);
                        double AssistLon = Convert.ToDouble(AssistLatLong.Lon);

                        double AreaLat = 0;
                        double AreaLon = 0;

                        if (!string.IsNullOrEmpty(AssistModelObj.Area))
                        {
                            AreaLat = Convert.ToDouble(AssistModelObj.Latitude);
                            AreaLon = Convert.ToDouble(AssistModelObj.Longitude);
                            AssistModelObj.Vendor_ID = 0;
                        }
                        else
                        {
                            AssistModelObj.Latitude = 0;
                            AssistModelObj.Longitude = 0;
                        }

                        if (AssistModelObj.Vendor_ID > 0)
                        {
                            AssistModelObj.StateID = 0;
                            AssistModelObj.CityID = 0;
                            AssistModelObj.Latitude = 0;
                            AssistModelObj.Longitude = 0;
                        }

                        DataSet dsVendorData = new DataSet();
                        DataView dvVendorData = new DataView();

                        var ServiceType = db.tblMstIncidentDetails.Where(x => x.Detailid == AssistModelObj.IncidentDetailId).Select(s => s.IncidentDetail).FirstOrDefault();

                        SqlConnection objsqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                        string qry = "select vbd.BranchID,vbd.BranchName,v.VendorID,v.VendorName,v.VendorMobileNo,s.StateID,s.State,c.CityID,c.City,vbd.Latitudes,vbd.Longitudes,v.Base_Price,v.VendorLandMark,v.Vendor_Category,v.Service_Type,v.KiloMeters,v.Per_km_Price,v.Service_Time from tblVendorBranchDetails vbd " +
                            "inner join tblMstVendor v on vbd.VendorID = v.VendorID " +
                            "inner join tblMstState s on vbd.State = s.StateID " +
                            "inner join tblMstCity c on vbd.City = c.CityID " +
                            "where vbd.Status = 1 and v.Service_Type = '" + ServiceType + "'";

                        SqlCommand cmd = new SqlCommand(qry, objsqlCon);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dsVendorData, "data");

                        DataTable dtVendorData = dsVendorData.Tables["data"];
                        dtVendorData.Columns.Add("KM", typeof(string));

                        if (dtVendorData.Rows.Count > 0 && dtVendorData != null)
                        {
                            dvVendorData = dtVendorData.DefaultView;

                            if (AssistModelObj.StateID > 0 && AssistModelObj.CityID == 0 && AssistModelObj.Latitude == 0 && AssistModelObj.Longitude == 0 && AssistModelObj.Vendor_ID == 0)
                            {
                                dvVendorData.RowFilter = "StateID = " + AssistModelObj.StateID + "";
                            }
                            else if (AssistModelObj.StateID > 0 && AssistModelObj.CityID > 0 && AssistModelObj.Latitude == 0 && AssistModelObj.Longitude == 0 && AssistModelObj.Vendor_ID == 0)
                            {
                                dvVendorData.RowFilter = "StateID = " + AssistModelObj.StateID + " and CityID = " + AssistModelObj.CityID + "";
                            }
                            else if (AssistModelObj.StateID > 0 && AssistModelObj.CityID > 0 && AssistModelObj.Latitude > 0 && AssistModelObj.Longitude > 0 && AssistModelObj.Vendor_ID == 0)
                            {
                                isAreaSearched = true;

                                dvVendorData.RowFilter = "StateID = " + AssistModelObj.StateID + " and CityID = " + AssistModelObj.CityID + "";
                            }
                            else if (AssistModelObj.StateID > 0 && AssistModelObj.CityID == 0 && AssistModelObj.Latitude > 0 && AssistModelObj.Longitude > 0 && AssistModelObj.Vendor_ID == 0)
                            {
                                isAreaSearched = true;

                                dvVendorData.RowFilter = "StateID = " + AssistModelObj.StateID + "";
                            }
                            else if (AssistModelObj.StateID == 0 && AssistModelObj.CityID == 0 && AssistModelObj.Latitude > 0 && AssistModelObj.Longitude > 0 && AssistModelObj.Vendor_ID == 0)
                            {
                                isAreaSearched = true;
                            }
                            else if (AssistModelObj.StateID == 0 && AssistModelObj.CityID == 0 && AssistModelObj.Latitude == 0 && AssistModelObj.Longitude == 0 && AssistModelObj.Vendor_ID > 0)
                            {
                                dvVendorData.RowFilter = "VendorID = " + AssistModelObj.Vendor_ID + "";
                            }

                            if (isAreaSearched == true)
                            {
                                foreach (DataRowView VendorData in dvVendorData)
                                {
                                    DataRow drVendor = VendorData.Row;

                                    double VenLat = 0;
                                    double VenLon = 0;
                                    var regex = new Regex("^[0-9.-]*$");

                                    string strVenLat = Convert.ToString(drVendor["Latitudes"]);
                                    string strVenLon = Convert.ToString(drVendor["Longitudes"]);

                                    if (string.IsNullOrEmpty(strVenLat))
                                    {
                                        VenLat = 0;
                                    }
                                    else if (!regex.IsMatch(strVenLat))
                                    {
                                        VenLat = 0;
                                    }
                                    else
                                    {
                                        VenLat = Convert.ToDouble(drVendor["Latitudes"]);
                                    }

                                    if (string.IsNullOrEmpty(strVenLon))
                                    {
                                        VenLon = 0;
                                    }
                                    else if (!regex.IsMatch(strVenLon))
                                    {
                                        VenLon = 0;
                                    }
                                    else
                                    {
                                        VenLon = Convert.ToDouble(drVendor["Longitudes"]);
                                    }

                                    double Distance = DistanceInMeter(VenLat, VenLon, AreaLat, AreaLon);
                                    double DistanceInKM = Distance / 1000;

                                    drVendor["KM"] = DistanceInKM;
                                }
                            }
                            else
                            {
                                foreach (DataRowView VendorData in dvVendorData)
                                {
                                    DataRow drVendor = VendorData.Row;

                                    double VenLat = 0;
                                    double VenLon = 0;
                                    var regex = new Regex("^[0-9.-]*$");

                                    string strVenLat = Convert.ToString(drVendor["Latitudes"]);
                                    string strVenLon = Convert.ToString(drVendor["Longitudes"]);

                                    if (string.IsNullOrEmpty(strVenLat))
                                    {
                                        VenLat = 0;
                                    }
                                    else if (!regex.IsMatch(strVenLat))
                                    {
                                        VenLat = 0;
                                    }
                                    else
                                    {
                                        VenLat = Convert.ToDouble(drVendor["Latitudes"]);
                                    }

                                    if (string.IsNullOrEmpty(strVenLon))
                                    {
                                        VenLon = 0;
                                    }
                                    else if (!regex.IsMatch(strVenLon))
                                    {
                                        VenLon = 0;
                                    }
                                    else
                                    {
                                        VenLon = Convert.ToDouble(drVendor["Longitudes"]);
                                    }

                                    double Distance = DistanceInMeter(VenLat, VenLon, AssistLat, AssistLon);
                                    double DistanceInKM = Distance / 1000;

                                    drVendor["KM"] = DistanceInKM;
                                }
                            }

                            if (dvVendorData.Count > 0 && dvVendorData != null)
                            {
                                dvVendorData.Sort = "Base_Price ASC, KM ASC";

                                foreach (DataRowView VendorData in dvVendorData)
                                {
                                    DataRow drVendor = VendorData.Row;

                                    model.VendorForAssistModelList.Add(new VendorsListModel
                                    {
                                        VendorBranchID = Convert.ToDecimal(drVendor["BranchID"]),
                                        VendorName = Convert.ToString(drVendor["VendorName"]),
                                        BranchName = Convert.ToString(drVendor["BranchName"]),
                                        VendorContactNo = Convert.ToString(drVendor["VendorMobileNo"]),
                                        State = Convert.ToString(drVendor["State"]),
                                        City = Convert.ToString(drVendor["City"]),
                                        Price = Convert.ToString(drVendor["Base_Price"]),
                                        KM = Convert.ToString(drVendor["KM"]),
                                        TruckParkLocation = Convert.ToString(drVendor["VendorLandMark"]),
                                        VendorCategory = Convert.ToString(drVendor["Vendor_Category"]),
                                        ServiceType = Convert.ToString(drVendor["Service_Type"]),
                                        UptoKMs = Convert.ToString(drVendor["KiloMeters"]),
                                        PerKMPrice = Convert.ToString(drVendor["Per_km_Price"]),
                                        ServiceTime = Convert.ToString(drVendor["Service_Time"])

                                    });
                                }
                            }
                            else
                            {

                            }

                            if (model.VendorForAssistModelList.Any())
                            {
                                isDataFound = true;
                            }
                            else
                            {
                                isDataFound = false;
                            }
                        }
                        else
                        {

                        }

                        model.AssistID = Convert.ToDecimal(AssistModelObj.AssistID);

                        var tblAssistDetails = db.tblAssistDetails.Where(w => w.Id == AssistID).FirstOrDefault();
                        if (string.IsNullOrEmpty(AssistModelObj.AssistSummary))
                        {
                            model.AssistSummary = tblAssistDetails.SummaryDetails;
                        }
                        else
                        {
                            model.AssistSummary = AssistModelObj.AssistSummary;
                        }

                        model.StatusID = Convert.ToInt32(AssistModelObj.StatusID);
                        model.RemarkID = AssistModelObj.RemarkId;

                        var Html = RenderPartialViewToString("~/Views/Assistance/_ViewVendorsForAssistance.cshtml", model);

                        var jsonResult = Json(new { status = CrossCutting_Constant.Success, Html = Html, isDataFound = isDataFound }, JsonRequestBehavior.AllowGet);
                        jsonResult.MaxJsonLength = int.MaxValue;
                        return jsonResult;
                    }
                    else
                    {
                        if (lstIncDetailIds.Contains(strIncDetailId))
                        {
                            string ErrMsg = "Please Save Drop Location and Kilometer First";
                            return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var AssistLatLong = db.tblAssistDetails.Where(w => w.Id == AssistID).Select(s => new { s.Lat, s.Lon }).FirstOrDefault();
                            double AssistLat = Convert.ToDouble(AssistLatLong.Lat);
                            double AssistLon = Convert.ToDouble(AssistLatLong.Lon);

                            double AreaLat = 0;
                            double AreaLon = 0;

                            if (!string.IsNullOrEmpty(AssistModelObj.Area))
                            {
                                AreaLat = Convert.ToDouble(AssistModelObj.Latitude);
                                AreaLon = Convert.ToDouble(AssistModelObj.Longitude);
                                AssistModelObj.Vendor_ID = 0;
                            }
                            else
                            {
                                AssistModelObj.Latitude = 0;
                                AssistModelObj.Longitude = 0;
                            }

                            if (AssistModelObj.Vendor_ID > 0)
                            {
                                AssistModelObj.StateID = 0;
                                AssistModelObj.CityID = 0;
                                AssistModelObj.Latitude = 0;
                                AssistModelObj.Longitude = 0;
                            }

                            DataSet dsVendorData = new DataSet();
                            DataView dvVendorData = new DataView();

                            var ServiceType = db.tblMstIncidentDetails.Where(x => x.Detailid == AssistModelObj.IncidentDetailId).Select(s => s.IncidentDetail).FirstOrDefault();


                            SqlConnection objsqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                            string qry = "select vbd.BranchID,vbd.BranchName,v.VendorID,v.VendorName,v.VendorMobileNo,s.StateID,s.State,c.CityID,c.City,vbd.Latitudes,vbd.Longitudes,v.Base_Price,v.VendorLandMark,v.Vendor_Category,v.Service_Type,v.KiloMeters,v.Per_km_Price,v.Service_Time from tblVendorBranchDetails vbd " +
                                "inner join tblMstVendor v on vbd.VendorID = v.VendorID " +
                                "inner join tblMstState s on vbd.State = s.StateID " +
                                "inner join tblMstCity c on vbd.City = c.CityID " +
                                "where vbd.Status = 1 and v.Service_Type = '" + ServiceType + "'";

                            SqlCommand cmd = new SqlCommand(qry, objsqlCon);
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(dsVendorData, "data");

                            DataTable dtVendorData = dsVendorData.Tables["data"];
                            dtVendorData.Columns.Add("KM", typeof(string));

                            if (dtVendorData.Rows.Count > 0 && dtVendorData != null)
                            {
                                dvVendorData = dtVendorData.DefaultView;

                                if (AssistModelObj.StateID > 0 && AssistModelObj.CityID == 0 && AssistModelObj.Latitude == 0 && AssistModelObj.Longitude == 0 && AssistModelObj.Vendor_ID == 0)
                                {
                                    dvVendorData.RowFilter = "StateID = " + AssistModelObj.StateID + "";
                                }
                                else if (AssistModelObj.StateID > 0 && AssistModelObj.CityID > 0 && AssistModelObj.Latitude == 0 && AssistModelObj.Longitude == 0 && AssistModelObj.Vendor_ID == 0)
                                {
                                    dvVendorData.RowFilter = "StateID = " + AssistModelObj.StateID + " and CityID = " + AssistModelObj.CityID + "";
                                }
                                else if (AssistModelObj.StateID > 0 && AssistModelObj.CityID > 0 && AssistModelObj.Latitude > 0 && AssistModelObj.Longitude > 0 && AssistModelObj.Vendor_ID == 0)
                                {
                                    isAreaSearched = true;

                                    dvVendorData.RowFilter = "StateID = " + AssistModelObj.StateID + " and CityID = " + AssistModelObj.CityID + "";
                                }
                                else if (AssistModelObj.StateID > 0 && AssistModelObj.CityID == 0 && AssistModelObj.Latitude > 0 && AssistModelObj.Longitude > 0 && AssistModelObj.Vendor_ID == 0)
                                {
                                    isAreaSearched = true;

                                    dvVendorData.RowFilter = "StateID = " + AssistModelObj.StateID + "";
                                }
                                else if (AssistModelObj.StateID == 0 && AssistModelObj.CityID == 0 && AssistModelObj.Latitude > 0 && AssistModelObj.Longitude > 0 && AssistModelObj.Vendor_ID == 0)
                                {
                                    isAreaSearched = true;
                                }
                                else if (AssistModelObj.StateID == 0 && AssistModelObj.CityID == 0 && AssistModelObj.Latitude == 0 && AssistModelObj.Longitude == 0 && AssistModelObj.Vendor_ID > 0)
                                {
                                    dvVendorData.RowFilter = "VendorID = " + AssistModelObj.Vendor_ID + "";
                                }

                                if (isAreaSearched == true)
                                {
                                    foreach (DataRowView VendorData in dvVendorData)
                                    {
                                        DataRow drVendor = VendorData.Row;

                                        double VenLat = 0;
                                        double VenLon = 0;
                                        var regex = new Regex("^[0-9.-]*$");

                                        string strVenLat = Convert.ToString(drVendor["Latitudes"]);
                                        string strVenLon = Convert.ToString(drVendor["Longitudes"]);

                                        if (string.IsNullOrEmpty(strVenLat))
                                        {
                                            VenLat = 0;
                                        }
                                        else if (!regex.IsMatch(strVenLat))
                                        {
                                            VenLat = 0;
                                        }
                                        else
                                        {
                                            VenLat = Convert.ToDouble(drVendor["Latitudes"]);
                                        }

                                        if (string.IsNullOrEmpty(strVenLon))
                                        {
                                            VenLon = 0;
                                        }
                                        else if (!regex.IsMatch(strVenLon))
                                        {
                                            VenLon = 0;
                                        }
                                        else
                                        {
                                            VenLon = Convert.ToDouble(drVendor["Longitudes"]);
                                        }

                                        double Distance = DistanceInMeter(VenLat, VenLon, AreaLat, AreaLon);
                                        double DistanceInKM = Distance / 1000;

                                        drVendor["KM"] = DistanceInKM;
                                    }
                                }
                                else
                                {
                                    foreach (DataRowView VendorData in dvVendorData)
                                    {
                                        DataRow drVendor = VendorData.Row;

                                        double VenLat = 0;
                                        double VenLon = 0;
                                        var regex = new Regex("^[0-9.-]*$");

                                        string strVenLat = Convert.ToString(drVendor["Latitudes"]);
                                        string strVenLon = Convert.ToString(drVendor["Longitudes"]);

                                        if (string.IsNullOrEmpty(strVenLat))
                                        {
                                            VenLat = 0;
                                        }
                                        else if (!regex.IsMatch(strVenLat))
                                        {
                                            VenLat = 0;
                                        }
                                        else
                                        {
                                            VenLat = Convert.ToDouble(drVendor["Latitudes"]);
                                        }

                                        if (string.IsNullOrEmpty(strVenLon))
                                        {
                                            VenLon = 0;
                                        }
                                        else if (!regex.IsMatch(strVenLon))
                                        {
                                            VenLon = 0;
                                        }
                                        else
                                        {
                                            VenLon = Convert.ToDouble(drVendor["Longitudes"]);
                                        }

                                        double Distance = DistanceInMeter(VenLat, VenLon, AssistLat, AssistLon);
                                        double DistanceInKM = Distance / 1000;

                                        drVendor["KM"] = DistanceInKM;
                                    }
                                }

                                if (dvVendorData.Count > 0 && dvVendorData != null)
                                {
                                    dvVendorData.Sort = "Base_Price ASC, KM ASC";

                                    foreach (DataRowView VendorData in dvVendorData)
                                    {
                                        DataRow drVendor = VendorData.Row;

                                        model.VendorForAssistModelList.Add(new VendorsListModel
                                        {
                                            VendorBranchID = Convert.ToDecimal(drVendor["BranchID"]),
                                            VendorName = Convert.ToString(drVendor["VendorName"]),
                                            BranchName = Convert.ToString(drVendor["BranchName"]),
                                            VendorContactNo = Convert.ToString(drVendor["VendorMobileNo"]),
                                            State = Convert.ToString(drVendor["State"]),
                                            City = Convert.ToString(drVendor["City"]),
                                            Price = Convert.ToString(drVendor["Base_Price"]),
                                            KM = Convert.ToString(drVendor["KM"]),
                                            TruckParkLocation = Convert.ToString(drVendor["VendorLandMark"]),
                                            VendorCategory = Convert.ToString(drVendor["Vendor_Category"]),
                                            ServiceType = Convert.ToString(drVendor["Service_Type"]),
                                            UptoKMs = Convert.ToString(drVendor["KiloMeters"]),
                                            PerKMPrice = Convert.ToString(drVendor["Per_km_Price"]),
                                            ServiceTime = Convert.ToString(drVendor["Service_Time"])

                                        });
                                    }
                                }
                                else
                                {

                                }

                                if (model.VendorForAssistModelList.Any())
                                {
                                    isDataFound = true;
                                }
                                else
                                {
                                    isDataFound = false;
                                }
                            }
                            else
                            {

                            }

                            model.AssistID = Convert.ToDecimal(AssistModelObj.AssistID);

                            var tblAssistDetails = db.tblAssistDetails.Where(w => w.Id == AssistID).FirstOrDefault();
                            if (string.IsNullOrEmpty(AssistModelObj.AssistSummary))
                            {
                                model.AssistSummary = tblAssistDetails.SummaryDetails;
                            }
                            else
                            {
                                model.AssistSummary = AssistModelObj.AssistSummary;
                            }

                            model.StatusID = Convert.ToInt32(AssistModelObj.StatusID);
                            model.RemarkID = AssistModelObj.RemarkId;

                            var Html = RenderPartialViewToString("~/Views/Assistance/_ViewVendorsForAssistance.cshtml", model);

                            var jsonResult = Json(new { status = CrossCutting_Constant.Success, Html = Html, isDataFound = isDataFound }, JsonRequestBehavior.AllowGet);
                            jsonResult.MaxJsonLength = int.MaxValue;
                            return jsonResult;
                        }                        
                    }                   
                }
                else
                {
                    string ErrMsg = "Assist Details Not Found";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                }               
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CaseCharges
        public ActionResult CaseCharges()
        {
            ViewBag.CompanyList = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.Company_Name)
                                             .Select(x => new SelectListItem { Value = x.CommonTypeID + "", Text = x.Description }).ToList();

            return View();
        }

        public ActionResult SearchCaseCharges(string CompanyID, string RefNo, string FromDate, string ToDate)
        {
            try
            {
                List<CaseChargeModel> list = new List<CaseChargeModel>();
                ViewBag.CompanyID = CompanyID;
                ViewBag.RefNo = RefNo;
                ViewBag.FromDate = FromDate;
                ViewBag.ToDate = ToDate;
                string FromDate1 = FromDate;
                DateTime dtFromDate = DateTime.ParseExact(FromDate1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string Fromdate = dtFromDate.ToString("yyyy-MM-dd");

                string ToDate1 = ToDate;
                DateTime dtToDate = DateTime.ParseExact(ToDate1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string Todate = dtToDate.ToString("yyyy-MM-dd");
                TempData["Load"] = "FirstTime";
                list = this.SearchCaseChargesList(CompanyID, RefNo, Fromdate, Todate);

                if (!string.IsNullOrEmpty(list[0].ReferenceNo))
                {
                    var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                    var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                    var Html = RenderPartialViewToString("~/Views/Assistance/_SearchCaseChargesGrid.cshtml", Grid);
                    return Json(new { status = CrossCutting_Constant.Success, message = CrossCutting_Constant.Success, Html = Html }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Success, message = CrossCutting_Constant.NoAssistanceFound }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                return Json(new { status = CrossCutting_Constant.Failure }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchCaseChargesNextPage(int? Page, string CompanyID, string RefNo, string FromDate, string ToDate, int SortingOrder = 0, int Paging = 5)
        {
            try
            {
                TempData["Load"] = "FirstTime";
                ViewBag.CompanyID = CompanyID;
                ViewBag.RefNo = RefNo;
                ViewBag.FromDate = FromDate;
                ViewBag.ToDate = ToDate;
                string FromDate1 = FromDate;
                DateTime dtFromDate = DateTime.ParseExact(FromDate1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string Fromdate = dtFromDate.ToString("yyyy-MM-dd");

                string ToDate1 = ToDate;
                DateTime dtToDate = DateTime.ParseExact(ToDate1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string Todate = dtToDate.ToString("yyyy-MM-dd");

                List<CaseChargeModel> list = new List<CaseChargeModel>();
                list = this.SearchCaseChargesList(CompanyID, RefNo, Fromdate, Todate);

                if (!string.IsNullOrEmpty(list[0].ReferenceNo))
                {
                    var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                    var grid = aj.CreateAjaxGrid(list.AsQueryable(), Page.HasValue ? Page.Value : 1, Page.HasValue, 2);
                    var Html = RenderPartialViewToString("~/Views/Assistance/_SearchCaseChargesGridNextPage.cshtml", grid);
                    return Json(new { status = CrossCutting_Constant.Success, message = CrossCutting_Constant.Success, Html = Html, HasItems = grid.HasItems }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Success, message = CrossCutting_Constant.NoAssistanceFound }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        public List<CaseChargeModel> SearchCaseChargesList(string CompanyID, string RefNo, string FromDate, string ToDate)
        {
            List<CaseChargeModel> list = new List<CaseChargeModel>();
            string ErrorMessage = string.Empty;
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string sql = "usp_GetCaseChargesDetails";
            try
            {
                int? intCompanyID = null;
                if (!string.IsNullOrEmpty(CompanyID))
                    intCompanyID = Convert.ToInt32(CompanyID);

                if (!string.IsNullOrEmpty(RefNo))
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            da.SelectCommand = new SqlCommand(sql, conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.AddWithValue("@CompanyID", intCompanyID);
                            da.SelectCommand.Parameters.AddWithValue("@RefNo", RefNo);
                            da.SelectCommand.Parameters.AddWithValue("@FromDate", FromDate);
                            da.SelectCommand.Parameters.AddWithValue("@ToDate", ToDate);
                            da.SelectCommand.Parameters.AddWithValue("@s_Error_Message", ErrorMessage);
                            da.SelectCommand.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;

                            da.Fill(ds, "data");

                            ErrorMessage = Convert.ToString(da.SelectCommand.Parameters["@s_Error_Message"].Value);
                        }
                    }

                    DataTable dt = ds.Tables["data"];

                    if (dt.Rows.Count > 0 && dt != null)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            list.Add(new CaseChargeModel
                            {
                                AssistID = Convert.ToString(row["AssistID"]),
                                ReferenceNo = Convert.ToString(row["RefNo"]),
                                CloseTime = Convert.ToString(row["CloseTime"]),
                                Status = Convert.ToString(row["Status"]),
                                Remark = Convert.ToString(row["Remark"]),
                                CompanyName = Convert.ToString(row["CompanyName"])

                            });
                        }
                    }
                }
                else
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            da.SelectCommand = new SqlCommand(sql, conn);
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.AddWithValue("@CompanyID", intCompanyID);
                            da.SelectCommand.Parameters.AddWithValue("@FromDate", FromDate);
                            da.SelectCommand.Parameters.AddWithValue("@ToDate", ToDate);
                            da.SelectCommand.Parameters.AddWithValue("@s_Error_Message", ErrorMessage);
                            da.SelectCommand.Parameters["@s_Error_Message"].Direction = ParameterDirection.Output;

                            da.Fill(ds, "data");

                            ErrorMessage = Convert.ToString(da.SelectCommand.Parameters["@s_Error_Message"].Value);
                        }
                    }

                    DataTable dt = ds.Tables["data"];

                    if (dt.Rows.Count > 0 && dt != null)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            list.Add(new CaseChargeModel
                            {
                                AssistID = Convert.ToString(row["AssistID"]),
                                ReferenceNo = Convert.ToString(row["RefNo"]),
                                CloseTime = Convert.ToString(row["CloseTime"]),
                                Status = Convert.ToString(row["Status"]),
                                Remark = Convert.ToString(row["Remark"]),
                                CompanyName = Convert.ToString(row["CompanyName"])

                            });
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            return list;
        }

        public JsonResult GetCaseChargesbyAssistID(string AssistID)
        {
            tblCaseCharges objCaseCharge = new tblCaseCharges();
            tblAssistDetail tblAssistDetail = new tblAssistDetail();
            string HTML = String.Empty;

            try
            {
                string UserName = HttpContext.User.Identity.Name;
                var user = db.AspNetUsers.Where(x => x.UserName == UserName).FirstOrDefault();
                ViewData["TieUpCompany"] = user.TieUpCompanyID;

                decimal Transactionid = Convert.ToDecimal(AssistID);
                CloseCaseChargeModel ChargeDetails = new CloseCaseChargeModel();
                ChargeDetails.AssistID = Convert.ToDecimal(AssistID);

                objCaseCharge = db.tblCaseCharges.Where(x => x.AssistID == ChargeDetails.AssistID).FirstOrDefault();
                if (objCaseCharge != null)
                {
                    ChargeDetails.TollCharges = objCaseCharge.TollCharges;
                    ChargeDetails.WaitingHoursCharges = objCaseCharge.WaitingHoursCharge;
                    ChargeDetails.VehicalCustodyHoursCharges = objCaseCharge.VehicleCustodyCharge;
                    ChargeDetails.OtherCharges = objCaseCharge.OtherCharges;
                    ChargeDetails.TotalKiloMeters = objCaseCharge.TotalKiloMeters;
                    ChargeDetails.TotalKiloMetersCharge = objCaseCharge.TotalKiloMetersCharge;
                    ChargeDetails.TotalAmount = objCaseCharge.TotalAmount;
                    ChargeDetails.FinalAmount = objCaseCharge.FinalAmount;
                    ChargeDetails.CustomerPaidAmount = objCaseCharge.CustomerPaidAmount;
                    ChargeDetails.CustomerPaidDate = objCaseCharge.CustomerPaidDate;

                    if (!string.IsNullOrEmpty(Convert.ToString(ChargeDetails.CustomerPaidDate)))
                    {
                        ChargeDetails.strCustomerPaidDate = Convert.ToDateTime(ChargeDetails.CustomerPaidDate).ToString("MM/dd/yyyy");                       
                    }
                    else
                    {
                        ChargeDetails.strCustomerPaidDate = null;
                    }
                    
                    ChargeDetails.ReferenceNo = objCaseCharge.ReferenceNo;
                }

                tblAssistDetail = db.tblAssistDetails.Where(x => x.Id == ChargeDetails.AssistID).FirstOrDefault();
                if (tblAssistDetail != null)
                {
                    ChargeDetails.Ven_ReachedLocation = tblAssistDetail.Ven_ReachedLocation;
                    ChargeDetails.Ven_ReachedTime = tblAssistDetail.Ven_ReachedTime;
                    ChargeDetails.Ven_DropLocation = tblAssistDetail.Ven_DropLocation;
                    ChargeDetails.Ven_DropTime = tblAssistDetail.Ven_DropTime;
                    ChargeDetails.Ven_TotalRunningKMs = tblAssistDetail.Ven_TotalRunningKMs;
                }

                HTML = RenderPartialViewToString("~/Views/Assistance/UpdateCaseChargeDetails.cshtml", ChargeDetails);
                return Json(new { message = CrossCutting_Constant.Success, HTML = HTML }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        public JsonResult SearchCase(string RefNo)
        {
            List<CaseChargeModel> list = new List<CaseChargeModel>();
            string FromDate1 = string.Empty;
            string ToDate1 = string.Empty;

            try
            {
                FromDate1 = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime dtFromDate = DateTime.ParseExact(FromDate1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string Fromdate = dtFromDate.ToString("yyyy-MM-dd");

                ToDate1 = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime dtToDate = DateTime.ParseExact(ToDate1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string Todate = dtToDate.ToString("yyyy-MM-dd");
                TempData["Load"] = "FirstTime";
                list = this.SearchCaseChargesList("", RefNo, Fromdate, Todate);

                if (list.Any())
                {
                    if (!string.IsNullOrEmpty(list[0].ReferenceNo))
                    {
                        var aj = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
                        var Grid = aj.CreateAjaxGrid(list.AsQueryable(), 1, false, 2);
                        var Html = RenderPartialViewToString("~/Views/Assistance/_SearchCaseChargesGrid.cshtml", Grid);
                        return Json(new { status = CrossCutting_Constant.Success, Html = Html, FromDate = FromDate1, ToDate = ToDate1 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = CrossCutting_Constant.Success, FromDate = FromDate1, ToDate = ToDate1 }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = "No Data Found", FromDate = FromDate1, ToDate = ToDate1 }, JsonRequestBehavior.AllowGet);
                }                
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg, FromDate = FromDate1, ToDate = ToDate1 }, JsonRequestBehavior.AllowGet);
            }

        }

        #region SaveClosedCaseChargeDetails
        public JsonResult SaveClosedCaseChargeDetails(CloseCaseChargeModel CloseCaseChargeModelObj)
        {
            tblCaseCharges tblCaseCharges = new tblCaseCharges();
            tblAssistDetail tblAssistDetail = new tblAssistDetail();
            string AssistRefNo = string.Empty;           

            try
            {
                string UserName = HttpContext.User.Identity.Name;
                var user = db.AspNetUsers.Where(x => x.Email == UserName).FirstOrDefault();

                if (CloseCaseChargeModelObj != null)
                {
                    if (CloseCaseChargeModelObj.AssistID > 0) // Case Charge
                    {
                        tblCaseCharges = db.tblCaseCharges.Where(x => x.AssistID == CloseCaseChargeModelObj.AssistID).FirstOrDefault();
                        if (tblCaseCharges != null)
                        {
                            tblCaseCharges.userId = user.Id;
                            tblCaseCharges.dt_Modify_Date = DateTime.Now;
                            tblCaseCharges.TollCharges = CloseCaseChargeModelObj.TollCharges;
                            tblCaseCharges.WaitingHoursCharge = CloseCaseChargeModelObj.WaitingHoursCharges;
                            tblCaseCharges.VehicleCustodyCharge = CloseCaseChargeModelObj.VehicalCustodyHoursCharges;
                            tblCaseCharges.OtherCharges = CloseCaseChargeModelObj.OtherCharges;
                            tblCaseCharges.TotalKiloMeters = CloseCaseChargeModelObj.TotalKiloMeters;
                            tblCaseCharges.TotalKiloMetersCharge = CloseCaseChargeModelObj.TotalKiloMetersCharge;
                            tblCaseCharges.CustomerPaidAmount = CloseCaseChargeModelObj.CustomerPaidAmount;

                            DateTime? CustomerPaidDate = null;
                            if (!string.IsNullOrEmpty(CloseCaseChargeModelObj.strCustomerPaidDate))
                            {
                                CustomerPaidDate = DateTime.ParseExact(CloseCaseChargeModelObj.strCustomerPaidDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                                tblCaseCharges.CustomerPaidDate = CustomerPaidDate;
                            }                            
                            
                            tblCaseCharges.ReferenceNo = CloseCaseChargeModelObj.ReferenceNo;

                            db.SaveChanges();

                            // Amount Calculation Start
                            tblCaseCharges = db.tblCaseCharges.Where(x => x.Id == tblCaseCharges.Id).FirstOrDefault();
                            if (tblCaseCharges != null)
                            {
                                decimal TollCharges = Convert.ToDecimal(tblCaseCharges.TollCharges);
                                decimal WaitingHoursCharge = Convert.ToDecimal(tblCaseCharges.WaitingHoursCharge);
                                decimal VehicleCustodyCharge = Convert.ToDecimal(tblCaseCharges.VehicleCustodyCharge);
                                decimal OtherCharges = Convert.ToDecimal(tblCaseCharges.OtherCharges);
                                decimal TotalKiloMetersCharge = Convert.ToDecimal(tblCaseCharges.TotalKiloMetersCharge);
                                CloseCaseChargeModelObj.TotalAmount = TollCharges + WaitingHoursCharge + VehicleCustodyCharge + OtherCharges + TotalKiloMetersCharge;

                                decimal TotalAmount = Convert.ToDecimal(CloseCaseChargeModelObj.TotalAmount);
                                decimal GSTPer = Convert.ToDecimal(ConfigurationManager.AppSettings["GSTPer"]);
                                decimal GSTAmount = (TotalAmount * GSTPer) / 100;
                                decimal TotalAmountWithGSTAmount = TotalAmount + GSTAmount;
                                CloseCaseChargeModelObj.FinalAmount = TotalAmountWithGSTAmount;

                                tblCaseCharges.TotalAmount = TotalAmount;
                                tblCaseCharges.FinalAmount = TotalAmountWithGSTAmount;

                                db.SaveChanges();
                            }
                            // Amount Calculation End

                            tblAssistDetail = db.tblAssistDetails.Where(x => x.Id == CloseCaseChargeModelObj.AssistID).FirstOrDefault();
                            if (tblAssistDetail != null)
                            {
                                AssistRefNo = tblAssistDetail.RefNo;
                            }

                            return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = AssistRefNo }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            tblCaseCharges tblCaseChargesObj = new tblCaseCharges();

                            tblCaseChargesObj.AssistID = CloseCaseChargeModelObj.AssistID;
                            tblCaseChargesObj.userId = user.Id;
                            tblCaseChargesObj.dt_Insert_Date = DateTime.Now;
                            tblCaseChargesObj.dt_Modify_Date = DateTime.Now;
                            tblCaseChargesObj.TollCharges = CloseCaseChargeModelObj.TollCharges;
                            tblCaseChargesObj.WaitingHoursCharge = CloseCaseChargeModelObj.WaitingHoursCharges;
                            tblCaseChargesObj.VehicleCustodyCharge = CloseCaseChargeModelObj.VehicalCustodyHoursCharges;
                            tblCaseChargesObj.OtherCharges = CloseCaseChargeModelObj.OtherCharges;
                            tblCaseChargesObj.TotalKiloMeters = CloseCaseChargeModelObj.TotalKiloMeters;
                            tblCaseChargesObj.TotalKiloMetersCharge = CloseCaseChargeModelObj.TotalKiloMetersCharge;
                            tblCaseChargesObj.CustomerPaidAmount = CloseCaseChargeModelObj.CustomerPaidAmount;

                            DateTime? CustomerPaidDate = null;
                            if (!string.IsNullOrEmpty(CloseCaseChargeModelObj.strCustomerPaidDate))
                            {
                                CustomerPaidDate = DateTime.ParseExact(CloseCaseChargeModelObj.strCustomerPaidDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                                tblCaseChargesObj.CustomerPaidDate = CustomerPaidDate;
                            }                            

                            tblCaseChargesObj.ReferenceNo = CloseCaseChargeModelObj.ReferenceNo;

                            db.tblCaseCharges.Add(tblCaseChargesObj);
                            db.SaveChanges();

                            // Amount Calculation Start
                            tblCaseCharges = db.tblCaseCharges.Where(x => x.Id == tblCaseChargesObj.Id).FirstOrDefault();
                            if (tblCaseCharges != null)
                            {
                                decimal TollCharges = Convert.ToDecimal(tblCaseChargesObj.TollCharges);
                                decimal WaitingHoursCharge = Convert.ToDecimal(tblCaseChargesObj.WaitingHoursCharge);
                                decimal VehicleCustodyCharge = Convert.ToDecimal(tblCaseChargesObj.VehicleCustodyCharge);
                                decimal OtherCharges = Convert.ToDecimal(tblCaseChargesObj.OtherCharges);
                                decimal TotalKiloMetersCharge = Convert.ToDecimal(tblCaseChargesObj.TotalKiloMetersCharge);
                                CloseCaseChargeModelObj.TotalAmount = TollCharges + WaitingHoursCharge + VehicleCustodyCharge + OtherCharges + TotalKiloMetersCharge;

                                decimal TotalAmount = Convert.ToDecimal(CloseCaseChargeModelObj.TotalAmount);
                                decimal GSTPer = Convert.ToDecimal(ConfigurationManager.AppSettings["GSTPer"]);
                                decimal GSTAmount = (TotalAmount * GSTPer) / 100;
                                decimal TotalAmountWithGSTAmount = TotalAmount + GSTAmount;
                                CloseCaseChargeModelObj.FinalAmount = TotalAmountWithGSTAmount;

                                tblCaseCharges.TotalAmount = TotalAmount;
                                tblCaseCharges.FinalAmount = TotalAmountWithGSTAmount;

                                db.SaveChanges();
                            }
                            // Amount Calculation End

                            tblAssistDetail = db.tblAssistDetails.Where(x => x.Id == CloseCaseChargeModelObj.AssistID).FirstOrDefault();
                            if (tblAssistDetail != null)
                            {
                                AssistRefNo = tblAssistDetail.RefNo;
                            }                           

                            return Json(new { status = CrossCutting_Constant.Success, AssistRefNo = AssistRefNo }, JsonRequestBehavior.AllowGet);
                        }

                        //return Json(new { status = CrossCutting_Constant.Success, errormessage = "AssistID Not Found" }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { status = CrossCutting_Constant.Success, errormessage = "AssistID In Model NULL" }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = CrossCutting_Constant.Failure, errormessage = "Model NULL" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrorMessage = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #endregion

        #region SearchCompanyDataForAssistance
        public ActionResult SearchCompanyDataForAssistance(string CertificateNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string CardLast4Digit, string MobileNoLast4Digit, string CompanyName)
        {
            SearchCompanyDataForAssistanceModel model = new SearchCompanyDataForAssistanceModel();
            List<SelectListItem> Citydata = new List<SelectListItem>();
            List<SelectListItem> Makedata = new List<SelectListItem>();
            List<SelectListItem> Modeldata = new List<SelectListItem>();

            string Company = CompanyName;
            string TataAig = ConfigurationManager.AppSettings["TataAig"];
            string IFFCO = ConfigurationManager.AppSettings["IFFCO"];
            string BhartiAssist = ConfigurationManager.AppSettings["Bharti"];
            string GlobalAssure = ConfigurationManager.AppSettings["GlobalAssure"];
            string UniversalSompo = ConfigurationManager.AppSettings["UniversalSompo"];
            string Reliance = ConfigurationManager.AppSettings["Reliance"];
            string IDFCBank = ConfigurationManager.AppSettings["IDFCBank"];
            string HDFCErgo = ConfigurationManager.AppSettings["HDFCErgo"];
            string ICICILombard = ConfigurationManager.AppSettings["ICICILombard"];
            string RGICL_Hinduja = ConfigurationManager.AppSettings["RGICL_Hinduja"];
            string ElectricOne = ConfigurationManager.AppSettings["ElectricOne"];
            string Pinnace = ConfigurationManager.AppSettings["Pinnace"];//Added By Parag on 17Apr2021

            try
            {
                model = GetCompanyDataForAssistance(CertificateNo, FirstName, LastName, EngineNo, ChassisNo, CardLast4Digit, MobileNoLast4Digit, CompanyName);
                
                if (model != null)
                {
                    if (model.PolicyStatus.ToLower().Equals("active"))
                    {
                        if (Company == GlobalAssure)
                        {
                            // Product and Make and Model Start
                            if (!string.IsNullOrEmpty(model.Product))
                            {
                                decimal PID = Convert.ToDecimal(model.Product);
                                var tblMstProducts = db.tblMstProducts.Where(w => w.ProductID == PID && w.IsValid != false).FirstOrDefault();
                                if (tblMstProducts != null)
                                {
                                    model.Product = Convert.ToString(tblMstProducts.ProductID);
                                    decimal ProductID = tblMstProducts.ProductID;
                                    if (!string.IsNullOrEmpty(model.Make))
                                    {
                                        decimal MKID = Convert.ToDecimal(model.Make);
                                        var tblMstMakes = db.tblMstMakes.Where(w => w.ProductID == ProductID && w.MakeID == MKID && w.IsValid != false).FirstOrDefault();
                                        if (tblMstMakes != null)
                                        {
                                            model.Make = Convert.ToString(tblMstMakes.MakeID);

                                            Makedata = GetMakeByProductIDForFetch(Convert.ToInt32(ProductID));
                                            decimal MakeID = tblMstMakes.MakeID;
                                            if (!string.IsNullOrEmpty(model.Model))
                                            {
                                                decimal MDID = Convert.ToDecimal(model.Model);
                                                var tblMstModels = db.tblMstModels.Where(w => w.MakeID == MakeID && w.ModelID == MDID && w.IsValid != false).FirstOrDefault();
                                                if (tblMstModels != null)
                                                {
                                                    model.Model = Convert.ToString(tblMstModels.ModelID);

                                                    Modeldata = GetModelByMakeIDForFetch(Convert.ToInt32(MakeID));
                                                }
                                                else
                                                {
                                                    model.Model = "";

                                                    Modeldata = GetModelByMakeIDForFetch(Convert.ToInt32(MakeID));
                                                }
                                            }
                                            else
                                            {
                                                model.Model = "";

                                                Modeldata = GetModelByMakeIDForFetch(Convert.ToInt32(MakeID));
                                            }
                                        }
                                        else
                                        {
                                            model.Make = "";
                                            model.Model = "";

                                            Makedata = GetMakeByProductIDForFetch(Convert.ToInt32(ProductID));
                                        }
                                    }
                                    else
                                    {
                                        model.Make = "";
                                        model.Model = "";

                                        Makedata = GetMakeByProductIDForFetch(Convert.ToInt32(ProductID));
                                    }
                                }
                                else
                                {
                                    model.Product = "";
                                    model.Make = "";
                                    model.Model = "";
                                }
                            }
                            else
                            {
                                model.Product = "";
                                model.Make = "";
                                model.Model = "";
                            }
                            // Product and Make and Model End

                            // State and City Start
                            if (!string.IsNullOrEmpty(model.State))
                            {
                                decimal SID = Convert.ToDecimal(model.State);
                                var tblMstStates = db.tblMstStates.Where(w => w.StateID == SID && w.IsValid != false).FirstOrDefault();
                                if (tblMstStates != null)
                                {
                                    model.State = Convert.ToString(tblMstStates.StateID);
                                    decimal StateID = tblMstStates.StateID;
                                    if (!string.IsNullOrEmpty(model.City))
                                    {                                
                                        decimal CID = Convert.ToDecimal(model.City);
                                        var tblMstCity = db.tblMstCities.Where(w => w.StateID == StateID && w.CityID == CID && w.IsValid != false).FirstOrDefault();
                                        if (tblMstCity != null)
                                        {
                                            model.City = Convert.ToString(tblMstCity.CityID);
                                            
                                            Citydata = GetCityByStateIDForFetch(Convert.ToInt32(StateID));
                                        }
                                        else
                                        {
                                            model.City = "";

                                            Citydata = GetCityByStateIDForFetch(Convert.ToInt32(StateID));
                                        }
                                    }
                                    else
                                    {
                                        model.City = "";

                                        Citydata = GetCityByStateIDForFetch(Convert.ToInt32(StateID));
                                    }
                                }
                                else
                                {
                                    model.State = "";
                                    model.City = "";
                                }
                            }
                            else
                            {
                                model.State = "";
                                model.City = "";
                            }
                            // State and City End
                        }
                        else
                        {
                            // Product and Make and Model and Variant Start
                            if (!string.IsNullOrEmpty(model.Product))
                            {
                                var tblMstProducts = db.tblMstProducts.Where(w => w.ProductName.ToLower().Equals(model.Product.ToLower()) && w.IsValid != false).FirstOrDefault();
                                if (tblMstProducts != null)
                                {
                                    model.Product = Convert.ToString(tblMstProducts.ProductID);
                                    decimal ProductID = tblMstProducts.ProductID;
                                    if (!string.IsNullOrEmpty(model.Make))
                                    {
                                        var tblMstMakes = db.tblMstMakes.Where(w => w.ProductID == ProductID && w.Name.ToLower().Equals(model.Make.ToLower()) && w.IsValid != false).FirstOrDefault();
                                        if (tblMstMakes != null)
                                        {
                                            model.Make = Convert.ToString(tblMstMakes.MakeID);

                                            Makedata = GetMakeByProductIDForFetch(Convert.ToInt32(ProductID));
                                            decimal MakeID = tblMstMakes.MakeID;
                                            if (!string.IsNullOrEmpty(model.Model) && !string.IsNullOrEmpty(model.Variant))
                                            {
                                                var tblMstModels = db.tblMstModels.Where(w => w.MakeID == MakeID && w.Name.ToLower().Equals(model.Model.ToLower()) && w.Variant.ToLower().Equals(model.Variant.ToLower()) && w.IsValid != false).FirstOrDefault();
                                                if (tblMstModels != null)
                                                {
                                                    model.Model = Convert.ToString(tblMstModels.ModelID);

                                                    Modeldata = GetModelByMakeIDForFetch(Convert.ToInt32(MakeID));
                                                }
                                                else
                                                {
                                                    model.Model = "";

                                                    Modeldata = GetModelByMakeIDForFetch(Convert.ToInt32(MakeID));
                                                }
                                            }
                                            else
                                            {
                                                model.Model = "";

                                                Modeldata = GetModelByMakeIDForFetch(Convert.ToInt32(MakeID));
                                            }
                                        }
                                        else
                                        {
                                            model.Make = "";
                                            model.Model = "";

                                            Makedata = GetMakeByProductIDForFetch(Convert.ToInt32(ProductID));
                                        }
                                    }
                                    else
                                    {
                                        model.Make = "";
                                        model.Model = "";

                                        Makedata = GetMakeByProductIDForFetch(Convert.ToInt32(ProductID));
                                    }
                                }
                                else
                                {
                                    model.Product = "";
                                    model.Make = "";
                                    model.Model = "";
                                }
                            }
                            else
                            {
                                model.Product = "";
                                model.Make = "";
                                model.Model = "";
                            }
                            // Product and Make and Model and Variant End

                            // State and City Start
                            if (!string.IsNullOrEmpty(model.State))
                            {
                                var tblMstStates = db.tblMstStates.Where(w => w.State.ToLower().Equals(model.State.ToLower()) && w.IsValid != false).FirstOrDefault();
                                if (tblMstStates != null)
                                {
                                    model.State = Convert.ToString(tblMstStates.StateID);
                                    decimal StateID = tblMstStates.StateID;
                                    if (!string.IsNullOrEmpty(model.City))
                                    {
                                        var tblMstCity = db.tblMstCities.Where(w => w.StateID == StateID && w.City.ToLower().Equals(model.City.ToLower()) && w.IsValid != false).FirstOrDefault();
                                        if (tblMstCity != null)
                                        {
                                            model.City = Convert.ToString(tblMstCity.CityID);

                                            Citydata = GetCityByStateIDForFetch(Convert.ToInt32(StateID));
                                        }
                                        else
                                        {
                                            model.City = "";

                                            Citydata = GetCityByStateIDForFetch(Convert.ToInt32(StateID));
                                        }
                                    }
                                    else
                                    {
                                        model.City = "";

                                        Citydata = GetCityByStateIDForFetch(Convert.ToInt32(StateID));
                                    }
                                }
                                else
                                {
                                    model.State = "";
                                    model.City = "";
                                }
                            }
                            else
                            {
                                model.State = "";
                                model.City = "";
                            }
                            // State and City End
                        }

                        var jsonResult = Json(new
                        { 
                            status = CrossCutting_Constant.Success, 
                            PolicyStatus = "Active", 
                            Product = model.Product.Trim(), 
                            Make = model.Make.Trim(), 
                            Model = model.Model.Trim(), 
                            FirstName = model.FirstName.Trim(), 
                            MiddleName = model.MiddleName.Trim(), 
                            LastName = model.LastName.Trim(), 
                            MobileNo = model.MobileNo.Trim(), 
                            EmailId = model.EmailId.Trim(), 
                            State = model.State.Trim(), 
                            City = model.City.Trim(), 
                            VehicleNo = model.VehicleNo.Trim(), 
                            EngineNo = model.EngineNo.Trim(), 
                            ChassisNo = model.ChassisNo.Trim(),
                            CardLast4Digit = model.CardLast4Digit.Trim(),
                            MobileNoLast4Digit = model.MobileNoLast4Digit.Trim(),
                            CityMasterData = Citydata, 
                            makelist = Makedata, 
                            modellist = Modeldata 

                        }, JsonRequestBehavior.AllowGet);
                        jsonResult.MaxJsonLength = int.MaxValue;

                        return jsonResult;
                    }
                    else if (model.PolicyStatus.ToLower().Equals("inactive"))
                    {
                        return Json(new { status = CrossCutting_Constant.Failure, errormsg = "Policy Status Cancelled" }, JsonRequestBehavior.AllowGet);
                    }  
                    else
                    {
                        return Json(new { status = CrossCutting_Constant.Failure, errormsg = "Policy Status Not Found" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string ErrMsg = "No Data Found";
                    return Json(new { status = CrossCutting_Constant.Failure, errormsg = ErrMsg }, JsonRequestBehavior.AllowGet);
                }               
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormsg = ErrMsg }, JsonRequestBehavior.AllowGet);
            }
        }

        public SearchCompanyDataForAssistanceModel GetCompanyDataForAssistance(string CertificateNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string CardLast4Digit, string MobileNoLast4Digit, string CompanyName)
        {
            SearchCompanyDataForAssistanceModel model = new SearchCompanyDataForAssistanceModel();

            string Company = CompanyName;
            string TataAig = ConfigurationManager.AppSettings["TataAig"];
            string IFFCO = ConfigurationManager.AppSettings["IFFCO"];
            string BhartiAssist = ConfigurationManager.AppSettings["Bharti"];
            string GlobalAssure = ConfigurationManager.AppSettings["GlobalAssure"];
            string UniversalSompo = ConfigurationManager.AppSettings["UniversalSompo"];
            string Reliance = ConfigurationManager.AppSettings["Reliance"];
            string IDFCBank = ConfigurationManager.AppSettings["IDFCBank"];
            string HDFCErgo = ConfigurationManager.AppSettings["HDFCErgo"];
            string ICICILombard = ConfigurationManager.AppSettings["ICICILombard"];
            string RGICL_Hinduja = ConfigurationManager.AppSettings["RGICL_Hinduja"];
            string ElectricOne = ConfigurationManager.AppSettings["ElectricOne"];
            string Pinnace = ConfigurationManager.AppSettings["Pinnace"];//Added By Parag on 17Apr2021

            if (Company == TataAig)
            {
                var data = (from t in db.tblTataAIGDetails.AsNoTracking() 
                            where
                            (t.PolicyCertificate.Contains(CertificateNo) || CertificateNo == "") &&
                            (t.CustomerName.Contains(FirstName) || FirstName == "") &&
                            (t.Surname.Contains(LastName) || LastName == "")
                             select new
                             {
                                 Product = t.ProductName,
                                 Make = t.Make,
                                 Model = t.Model,
                                 Variant = "",
                                 FirstName = t.CustomerName,
                                 MiddleName = "",
                                 LastName = t.Surname,
                                 MobileNo = "",
                                 EmailId = "",
                                 State = t.state,
                                 City = t.City,
                                 VehicleNo = t.RegistrationNo,
                                 EngineNo = "",
                                 ChassisNo = "",
                                 CardLast4Digit = "",
                                 MobileNoLast4Digit = "",
                                 PolicyStatus = t.PolicyStatus

                             }).FirstOrDefault();
            
                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == IFFCO)
            {
                var data = (from t in db.tblIFFCODetails.AsNoTracking()
                            where
                            (t.Policy.Contains(CertificateNo) || CertificateNo == "") && 
                            (t.FirstName.Contains(FirstName) || FirstName == "") && 
                            (t.LastName.Contains(LastName) || LastName == "") && 
                            (t.EngineNumber.Contains(EngineNo) || EngineNo == "") && 
                            (t.ChassisNumber.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = t.Product,
                                Make = t.Make,
                                Model = t.Model,
                                Variant = "",
                                FirstName = t.FirstName,
                                MiddleName = "",
                                LastName = t.LastName,
                                MobileNo = "",
                                EmailId = "",
                                State = "",
                                City = "",
                                VehicleNo = t.RegistrationNo,
                                EngineNo = t.EngineNumber,
                                ChassisNo = t.ChassisNumber,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.PolicyStatus
            
                            }).FirstOrDefault();
            
                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == BhartiAssist)
            {
                var data = (from t in db.tblBhartiAXADetails.AsNoTracking()
                            where
                            (t.CertificateNo.Contains(CertificateNo) || CertificateNo == "") && 
                            (t.FirstName.Contains(FirstName) || FirstName == "") &&
                            (t.EngineNo.Contains(EngineNo) || EngineNo == "") && 
                            (t.ChassisNo.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = "",
                                Make = t.Make,
                                Model = t.Model,
                                Variant = t.Variant,
                                FirstName = t.FirstName,
                                MiddleName = "",
                                LastName = "",
                                MobileNo = t.CustomerMobileNo,
                                EmailId = "",
                                State = t.PermanentState,
                                City = t.PermanentCity,
                                VehicleNo = t.RegistrationNo,
                                EngineNo = t.EngineNo,
                                ChassisNo = t.ChassisNo,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.PolicyStatus 
            
                            }).FirstOrDefault();
            
                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == GlobalAssure)
            {
                var data = (from t in db.tblTransactions.AsNoTracking()
                            join city in db.tblMstCities.AsNoTracking() on t.PermanentCityID equals city.CityID into cmnleftintocity
                            from c in cmnleftintocity.DefaultIfEmpty()
                            where
                            (t.CertificateNo.Contains(CertificateNo) || CertificateNo == "") &&
                            (t.CustomerName.Contains(FirstName) || FirstName == "") &&
                            (t.CustomerLastName.Contains(LastName) || LastName == "") &&
                            (t.EngineNo.Contains(EngineNo) || EngineNo == "") &&
                            (t.ChassisNo.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = t.ProductID,
                                Make = t.MakeID,
                                Model = t.ModelID,
                                FirstName = t.CustomerName,
                                MiddleName = t.CustomerMiddleName,
                                LastName = t.CustomerLastName,
                                MobileNo = t.CustomerContact,
                                EmailId = t.CustomerEmail,
                                State = c.StateID,
                                City = c.CityID,
                                VehicleNo = t.RegistrationNo,
                                EngineNo = t.EngineNo,
                                ChassisNo = t.ChassisNo,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.StatusID

                            }).FirstOrDefault();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(Convert.ToString(data.Product))) ? Convert.ToString(data.Product).Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(Convert.ToString(data.Make))) ? Convert.ToString(data.Make).Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(Convert.ToString(data.Model))) ? Convert.ToString(data.Model).Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(Convert.ToString(data.State))) ? Convert.ToString(data.State).Trim() : "";
                    model.City = (!string.IsNullOrEmpty(Convert.ToString(data.City))) ? Convert.ToString(data.City).Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (!data.PolicyStatus.Equals(14)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == UniversalSompo)
            {
                var data = (from t in db.tblUniversalShampoo.AsNoTracking()
                            where
                            (t.CertificateNo.Contains(CertificateNo) || CertificateNo == "") &&
                            (t.FirstName.Contains(FirstName) || FirstName == "") &&
                            (t.LastName.Contains(LastName) || LastName == "") &&
                            (t.EngineNo.Contains(EngineNo) || EngineNo == "") &&
                            (t.ChassisNo.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = "",
                                Make = t.Make,
                                Model = t.Model,
                                Variant = t.Variant,
                                FirstName = t.FirstName,
                                MiddleName = t.MiddleName,
                                LastName = t.LastName,
                                MobileNo = t.CustomerMobileNo,
                                EmailId = "",
                                State = t.PermanentState,
                                City = t.PermanentCity,
                                VehicleNo = t.RegistrationNo,
                                EngineNo = t.EngineNo,
                                ChassisNo = t.ChassisNo,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.PolicyStatus

                            }).FirstOrDefault();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == Reliance)
            {
                string Param = string.Empty;
                string RespStatus = "policy is eligible for rsa";
                string UserID = System.Configuration.ConfigurationManager.AppSettings["Userid"];
                string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];

                RGICL_Policy_Service_RSAClient client = new RGICL_Policy_Service_RSAClient();
                client.InnerChannel.OperationTimeout = new TimeSpan(0, 1, 0); // Timeout 1 min

                if (!string.IsNullOrEmpty(CertificateNo))
                {
                    Param = CertificateNo;
                }
                else if (!string.IsNullOrEmpty(FirstName))
                {
                    Param = FirstName;
                }
                else if (!string.IsNullOrEmpty(LastName))
                {
                    Param = LastName;
                }
                else if (!string.IsNullOrEmpty(EngineNo))
                {
                    Param = EngineNo;
                }
                else if (!string.IsNullOrEmpty(ChassisNo))
                {
                    Param = ChassisNo;
                }

                clsReliance data = new clsReliance();

                var Response = client.GetPolicyDetailforRSA(UserID, Password, Param.Trim());
                if (Response != null)
                {
                    if (Response.ResponseStatus.ToLower().Equals(RespStatus))
                    {
                        data.Product = "PC";
                        data.Make = Response.Make;
                        data.Model = Response.Model;
                        data.Variant = "";
                        data.FirstName = Response.InsuredName;
                        data.MiddleName = "";
                        data.LastName = "";
                        data.MobileNo = Response.InsuredMobileNumber;
                        data.EmailId = Response.InsuredEmailID;
                        data.State = Response.InsuredStateName;
                        data.City = Response.InsuredCityName;
                        data.VehicleNo = Response.VehicleNumber;
                        data.EngineNo = Response.EngineNumber;
                        data.ChassisNo = Response.ChassisNumber;
                        data.PolicyStatus = (Response.PolicyStatus.ToLower().Equals("active")) ? true : false;                        
                    }
                    else
                    {
                        data = null;
                    }
                }
                else
                {
                    data = null;
                }

                client.Close();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = "";
                    model.MobileNoLast4Digit = "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == IDFCBank)
            {
                var data = (from t in db.tblIDFCBank.AsNoTracking()
                            where
                            (t.FirstName.Contains(FirstName) || FirstName == "") &&
                            (t.LastName.Contains(LastName) || LastName == "") &&
                            (t.CardLast4Digit.Contains(CardLast4Digit) || CardLast4Digit == "") &&
                            (t.MobileNoLast4Digit.Contains(MobileNoLast4Digit) || MobileNoLast4Digit == "")
                            select new
                            {
                                Product = "",
                                Make = "",
                                Model = "",
                                Variant = "",
                                FirstName = t.FirstName,
                                MiddleName = "",
                                LastName = t.LastName,
                                MobileNo = "",
                                EmailId = "",
                                State = "",
                                City = "",
                                VehicleNo = "",
                                EngineNo = "",
                                ChassisNo = "",
                                CardLast4Digit = t.CardLast4Digit,
                                MobileNoLast4Digit = t.MobileNoLast4Digit,
                                PolicyStatus = t.PolicyStatus

                            }).FirstOrDefault();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == HDFCErgo)
            {
                var data = (from t in db.tblHDFCErgo.AsNoTracking()
                            where
                            (t.POLICY_NO.Contains(CertificateNo) || CertificateNo == "") &&
                            (t.FIRST_NAME.Contains(FirstName) || FirstName == "") &&
                            (t.SURNAME.Contains(LastName) || LastName == "") &&
                            (t.CHASSIS_NUM.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = "PC",
                                Make = t.CAR_MAKE,
                                Model = t.MODEL,
                                Variant = "",
                                FirstName = t.FIRST_NAME,
                                MiddleName = "",
                                LastName = t.SURNAME,
                                MobileNo = "",
                                EmailId = "",
                                State = t.STATE,
                                City = t.LOCALITY,
                                VehicleNo = t.VEHICLE_REG_NO,
                                EngineNo = "",
                                ChassisNo = t.CHASSIS_NUM,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.STATUS

                            }).FirstOrDefault();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == ICICILombard)
            {
                var data = (from t in db.tblICICILombard.AsNoTracking()                            
                            where
                            (t.POLICYNO.Contains(CertificateNo) || CertificateNo == "") &&
                            (t.InsuredName.Contains(FirstName) || FirstName == "") &&                            
                            (t.EngineNo.Contains(EngineNo) || EngineNo == "") &&
                            (t.ChassisNo.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = "",
                                Make = "",
                                Model = "",
                                Variant = "",
                                FirstName = t.InsuredName,
                                MiddleName = "",
                                LastName = "",
                                MobileNo = "",
                                EmailId = "",
                                State = "",
                                City = "",
                                VehicleNo = t.RegistrationNo,
                                EngineNo = t.EngineNo,
                                ChassisNo = t.ChassisNo,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.PolicyStatus

                            }).FirstOrDefault();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == RGICL_Hinduja)
            {
                var data = (from t in db.tblTransactions.AsNoTracking()
                            join product in db.tblMstProducts.AsNoTracking() on t.ProductID equals product.ProductID into cmnleftintoproduct
                            from product in cmnleftintoproduct.DefaultIfEmpty()
                            where
                            (t.CertificateNo.Contains(CertificateNo) || CertificateNo == "") &&
                            (t.CustomerName.Contains(FirstName) || FirstName == "") &&
                            (t.CustomerLastName.Contains(LastName) || LastName == "") &&
                            (t.EngineNo.Contains(EngineNo) || EngineNo == "") &&
                            (t.ChassisNo.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = product.ProductName,
                                Make = t.MakeName,
                                Model = t.ModelName,
                                Variant = t.VariantName,
                                FirstName = t.CustomerName,
                                MiddleName = t.CustomerMiddleName,
                                LastName = t.CustomerLastName,
                                MobileNo = t.CustomerContact,
                                EmailId = t.CustomerEmail,
                                State = t.State,
                                City = t.City,
                                VehicleNo = t.RegistrationNo,
                                EngineNo = t.EngineNo,
                                ChassisNo = t.ChassisNo,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.IsValid

                            }).FirstOrDefault();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == ElectricOne)
            {
                var data = (from t in db.tblElectricBikeDetails.AsNoTracking()
                            where                            
                            (t.InsuredFirstName.Contains(FirstName) || FirstName == "") &&
                            (t.InsuredLastName.Contains(LastName) || LastName == "") &&
                            (t.BatteryNo.Contains(EngineNo) || EngineNo == "") &&
                            (t.FrameNo.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = t.VehicleType,
                                Make = t.Make,
                                Model = t.Model,
                                Variant = "",
                                FirstName = t.InsuredFirstName,
                                MiddleName = "",
                                LastName = t.InsuredLastName,
                                MobileNo = t.InsuredMobileNo,
                                EmailId = t.InsuredEmailID,
                                State = t.InsuredState,
                                City = t.InsuredCity,
                                VehicleNo = "",
                                EngineNo = t.BatteryNo,
                                ChassisNo = t.FrameNo,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.PolicyStatus

                            }).FirstOrDefault();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }
            else if (Company == Pinnace)//Added By Parag on 17Apr2021
            {
                var data = (from t in db.tblPinnace.AsNoTracking()
                            where
                            (t.InsuredFirstName.Contains(FirstName) || FirstName == "") &&
                            (t.InsuredLastName.Contains(LastName) || LastName == "") &&
                            (t.ChassisNo.Contains(ChassisNo) || ChassisNo == "")
                            select new
                            {
                                Product = "",
                                Make = t.Make,
                                Model = t.Model,
                                Variant = "",
                                FirstName = t.InsuredFirstName,
                                MiddleName = "",
                                LastName = t.InsuredLastName,
                                MobileNo = t.InsuredMobileNo,
                                EmailId = t.InsuredEmailID,
                                State = t.InsuredState,
                                City = t.InsuredCity,
                                VehicleNo = "",
                                EngineNo = "",
                                ChassisNo = t.ChassisNo,
                                CardLast4Digit = "",
                                MobileNoLast4Digit = "",
                                PolicyStatus = t.PolicyStatus

                            }).FirstOrDefault();

                if (data != null)
                {
                    model.Product = (!string.IsNullOrEmpty(data.Product)) ? data.Product.Trim() : "";
                    model.Make = (!string.IsNullOrEmpty(data.Make)) ? data.Make.Trim() : "";
                    model.Model = (!string.IsNullOrEmpty(data.Model)) ? data.Model.Trim() : "";
                    model.Variant = (!string.IsNullOrEmpty(data.Variant)) ? data.Variant.Trim() : "";
                    model.FirstName = (!string.IsNullOrEmpty(data.FirstName)) ? data.FirstName.Trim() : "";
                    model.MiddleName = (!string.IsNullOrEmpty(data.MiddleName)) ? data.MiddleName.Trim() : "";
                    model.LastName = (!string.IsNullOrEmpty(data.LastName)) ? data.LastName.Trim() : "";
                    model.MobileNo = (!string.IsNullOrEmpty(data.MobileNo)) ? data.MobileNo.Trim() : "";
                    model.EmailId = (!string.IsNullOrEmpty(data.EmailId)) ? data.EmailId.Trim() : "";
                    model.State = (!string.IsNullOrEmpty(data.State)) ? data.State.Trim() : "";
                    model.City = (!string.IsNullOrEmpty(data.City)) ? data.City.Trim() : "";
                    model.VehicleNo = (!string.IsNullOrEmpty(data.VehicleNo)) ? data.VehicleNo.Trim() : "";
                    model.EngineNo = (!string.IsNullOrEmpty(data.EngineNo)) ? data.EngineNo.Trim() : "";
                    model.ChassisNo = (!string.IsNullOrEmpty(data.ChassisNo)) ? data.ChassisNo.Trim() : "";
                    model.CardLast4Digit = (!string.IsNullOrEmpty(data.CardLast4Digit)) ? data.CardLast4Digit.Trim() : "";
                    model.MobileNoLast4Digit = (!string.IsNullOrEmpty(data.MobileNoLast4Digit)) ? data.MobileNoLast4Digit.Trim() : "";
                    model.PolicyStatus = (data.PolicyStatus.Equals(true)) ? "Active" : "InActive";
                }
                else
                {
                    model = null;
                }
            }

            return model;
        }
        #endregion

        #region UpdateIncidentLocationForAssist
        public ActionResult UpdateIncidentLocationForAssist(AssistModel AssistModelObj)
        {
            tblAssistDetail tblAssistDetail = new tblAssistDetail();

            try
            {
                decimal AssistID = Convert.ToDecimal(AssistModelObj.AssistID);

                tblAssistDetail = db.tblAssistDetails.Where(w => w.Id == AssistID).FirstOrDefault();
                if (tblAssistDetail != null)
                {
                    tblAssistDetail.Address = AssistModelObj.Address;
                    tblAssistDetail.Lat = Convert.ToString(AssistModelObj.LatitudeLoc);
                    tblAssistDetail.Lon = Convert.ToString(AssistModelObj.LongitudeLoc);
                    tblAssistDetail.CreatedDate = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    string ErrMsg = "Assistance Not Found !, Please Try Again..";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = CrossCutting_Constant.Success, assistid = AssistID }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region SearchDLKMForAssistance
        public ActionResult SearchDLKMForAssistance(AssistModel AssistModelObj)
        {
            string strDropLocationKM = Convert.ToDecimal(2).ToString("##.00");

            try
            {
                decimal decAssistID = Convert.ToDecimal(AssistModelObj.AssistID);
                string IncidentLocation = db.tblAssistDetails.Where(w => w.Id == decAssistID).Select(s => s.Address).FirstOrDefault();
                if (string.IsNullOrEmpty(IncidentLocation))
                {
                    string ErrMsg = "Incident Location Not Found !, Update Incident Location First";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                }

                decimal AssistID = Convert.ToDecimal(AssistModelObj.AssistID);
                var AssistLatLong = db.tblAssistDetails.Where(w => w.Id == AssistID).Select(s => new { s.Lat, s.Lon }).FirstOrDefault();
                double AssistLat = Convert.ToDouble(AssistLatLong.Lat);
                double AssistLon = Convert.ToDouble(AssistLatLong.Lon);

                double IncidentLat = AssistModelObj.LatitudeDL;
                double IncidentLon = AssistModelObj.LongitudeDL;

                double Distance = DistanceInMeter(AssistLat, AssistLon, IncidentLat, IncidentLon);
                double DistanceInKM = Distance / 1000;

                strDropLocationKM = Convert.ToDecimal(DistanceInKM).ToString("##.00");

                return Json(new { status = CrossCutting_Constant.Success, DropLocationKM = strDropLocationKM }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region SaveDLKMForAssistance
        public ActionResult SaveDLKMForAssistance(AssistModel AssistModelObj)
        {
            tblAssistDetail tblAssistDetail = new tblAssistDetail();

            try
            {
                decimal AssistID = Convert.ToDecimal(AssistModelObj.AssistID);
                tblAssistDetail = db.tblAssistDetails.Where(w => w.Id == AssistID).FirstOrDefault();
                if (tblAssistDetail != null)
                {
                    tblAssistDetail.DropLocationAddress = AssistModelObj.DropLocation;
                    tblAssistDetail.DropLocationKM = AssistModelObj.DropLocationKM;
                    db.SaveChanges();

                    return Json(new { status = CrossCutting_Constant.Success }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrMsg = "Assistance Data Not Found";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                }             
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CheckIsRelianceAssistance
        public ActionResult CheckIsRelianceAssistance(decimal AssistID)
        {
            string CompanyId = "0";
            string[] IsEditable = Convert.ToString(ConfigurationManager.AppSettings["IsEditable"]).Split(',');

            bool boolIsRelianceAssistance = false;
            string RILUserID = Convert.ToString(ConfigurationManager.AppSettings["RILUserID"]);
            string RelianceUserID = string.Empty;
            string AssistUserID = string.Empty;

            try
            {               
                var tblAssistD = db.tblAssistDetails.Where(w => w.Id == AssistID).Select(s => new { s.AssistCreatedBy }).FirstOrDefault();
                if (tblAssistD != null)
                {
                    string AssistCreatedBy = tblAssistD.AssistCreatedBy;
                    var usrdata = db.AspNetUsers.AsNoTracking().Where(w => w.Id == AssistCreatedBy).Select(s => new { s.CompanyID }).FirstOrDefault();
                    if (usrdata != null)
                    {
                        if (usrdata.CompanyID == null)
                        {
                            CompanyId = "0";
                        }
                        else
                        {
                            CompanyId = Convert.ToString(usrdata.CompanyID);
                        }
                    }
                }          

                var userdata = db.AspNetUsers.Where(w => w.Email == RILUserID).FirstOrDefault();
                if (userdata != null)
                {
                    RelianceUserID = userdata.Id;
                }

                var tblAssistDetails = db.tblAssistDetails.Where(w => w.Id == AssistID).Select(s => new { s.TransactionId }).FirstOrDefault();
                if (tblAssistDetails != null)
                {
                    decimal TransactionId = Convert.ToDecimal(tblAssistDetails.TransactionId);
                    var tblTransaction = db.tblTransactions.Where(w => w.TransactionID == TransactionId).Select(s => new { s.UserID }).FirstOrDefault();
                    if (!string.IsNullOrEmpty(tblTransaction.UserID))
                    {
                        AssistUserID = tblTransaction.UserID;
                        if (AssistUserID.Equals(RelianceUserID))
                        {
                            boolIsRelianceAssistance = true;
                        }
                        else
                        {
                            if (IsEditable.Contains(CompanyId))
                            {
                                boolIsRelianceAssistance = true;
                            }
                            else
                            {
                                boolIsRelianceAssistance = false;
                            }                        
                        }                       
                    }

                    return Json(new { status = CrossCutting_Constant.Success, IsRelianceAssistance = boolIsRelianceAssistance }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string ErrMsg = "Assistance Not Found";
                    return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
                }                            
            }
            catch (Exception e)
            {
                HandleException(e);
                string ErrMsg = e.Message;
                return Json(new { status = CrossCutting_Constant.Failure, errormessage = ErrMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Asssistance MIS Report
        public ActionResult AsssistanceMISReport()
        {
            ViewBag.StatusLst = db.tblMstWorkFlowStatus.Where(x => x.Module == 1 && x.IsValid == true)
                                             .Select(x => new SelectListItem { Value = x.StatusID + "", Text = x.Status }).ToList();  //Module-  1.status  2. Action

            ViewBag.OEMList = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "OEMName")
                                             .Select(x => new SelectListItem { Value = x.CommonTypeID + "", Text = x.Description }).ToList();

            ViewBag.CompanyList = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == CrossCutting_Constant.Company_Name)
                                             .Select(x => new SelectListItem { Value = x.CommonTypeID + "", Text = x.Description }).ToList();

            ViewBag.RemarkList = db.tblMstCommonTypes.AsNoTracking().Where(w => w.IsValid != false && w.MasterType == "CaseClosureRemark")
                                             .Select(x => new SelectListItem { Value = x.CommonTypeID + "", Text = x.Description }).ToList();

            return View();
        }

        private List<AssistanceMISReport> GetSearchAssistMISReport(string registration, string refNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string CertificateIssueDateFrom, string CertificateIssueDateTo, int statusid, string EmailID, string OEMId, string CompanyId, string RemarkId)
        {
            DateTime? FromDate = null;
            DateTime? ToDate = null;

            if (!string.IsNullOrEmpty(CertificateIssueDateFrom))
                FromDate = DateTime.ParseExact(CertificateIssueDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(CertificateIssueDateTo))
                ToDate = DateTime.ParseExact(CertificateIssueDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            int? intOEMId = null;
            int? intCompanyId = null;

            if (!string.IsNullOrEmpty(OEMId))
                intOEMId = Convert.ToInt32(OEMId);
            if (!string.IsNullOrEmpty(CompanyId))
                intCompanyId = Convert.ToInt32(CompanyId);

            List<AssistanceMISReport> list = new List<AssistanceMISReport>();
            List<AssistanceMISReport> listNotAssigned = new List<AssistanceMISReport>();
            List<AssistanceMISReport> listAssigned = new List<AssistanceMISReport>();
            List<AssistanceMISReport> listClosed = new List<AssistanceMISReport>();

            string UserName = User.Identity.Name;
            var userdata = db.AspNetUsers.Where(w => w.UserName == UserName).Select(s => new { s.Id, s.IsAdmin, s.TieUpCompanyID }).FirstOrDefault();

            if (statusid == 0)
            {
                #region statusid = 8
                statusid = 8;
                listNotAssigned = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        join makebased in db.tblMstMakes.AsNoTracking() on t.MakeID equals makebased.MakeID into objMake
                        from make in objMake.DefaultIfEmpty()
                        join modelbased in db.tblMstModels.AsNoTracking() on t.ModelID equals modelbased.ModelID into objModel
                        from model in objModel.DefaultIfEmpty()
                        join city in db.tblMstCities.AsNoTracking() on t.PermanentCityID equals city.CityID into objcity
                        from city in objcity.DefaultIfEmpty()
                        join state in db.tblMstStates.AsNoTracking() on city.StateID equals state.StateID into objstate
                        from state in objstate.DefaultIfEmpty()
                        join incdetail in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals incdetail.Detailid into objincdetail
                        from incdetail in objincdetail.DefaultIfEmpty()
                        join inc in db.tblMstIncidents.AsNoTracking() on incdetail.IncidentDetailid equals inc.Incidentid into objinc
                        from inc in objinc.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid &&
                        (a.AssistOEMID == intOEMId || intOEMId == null) &&
                        (a.AssistCompanyID == intCompanyId || intCompanyId == null) &&
                        (a.Remark1 == RemarkId || string.IsNullOrEmpty(RemarkId)))
                        select new AssistanceMISReport
                        {
                            AgentName = user.Name,
                            RefNo = a.RefNo,
                            PolicyNo = t.CertificateNo,
                            dtRiskStartDateTime = t.CoverStartDate,
                            dtRiskEndDateTime = t.CoverEndDate,
                            RSACover = "Yes",
                            MakeAndModel = make.Name + "  " + model.Name + "  " + model.Variant,
                            State = state.State,
                            City = city.City,
                            RegistrationNo = t.RegistrationNo,
                            ContactNo = "",
                            dtReportedDateTime1 = t.CreatedDate,
                            dtReportedDateTime2 = a.AssistCreatedDate,
                            dtPlannedDateTime = a.PlannedDateTime,
                            ClosureDate = "",
                            ClosureTime = "",
                            VendorAssignedDate = "",
                            VendorAssignedTime = "",
                            VendorReachedIncidentLocDate = "",
                            VendorReachedIncidentLocTime = "",
                            VendorReachedDropLocDate = "",
                            VendorReachedDropLocTime = "",
                            ServiceTime = "",
                            Action = "",
                            Status = status.Status,
                            CompanyName = cmnType.Description,
                            VendorName = "",
                            BranchName = "",
                            IncidentLocation = a.Address,
                            DropLocation = (a.DropLocationAddress != null) ? a.DropLocationAddress : (a.Ven_DropLocation != null) ? a.Ven_DropLocation : "",
                            VendorToIncident = "",
                            IToD = (a.DropLocationKM != null) ? a.DropLocationKM : (a.Ven_TotalRunningKMs != null) ? a.Ven_TotalRunningKMs : "",
                            TotalKM = "",
                            TotalCost = "",
                            TollCharges = "",
                            WaitingHoursCharge = "",
                            VehicleCustodyHoursCharge = "",
                            OthersCharge = "",
                            CustomerPaidAmount = "",
                            SelectIncident = inc.IncidentType + " - " + incdetail.IncidentDetail,
                            CaseSummary = a.SummaryDetails,
                            TAT1 = "",
                            TAT2 = "",
                            TAT3 = "",
                            Remark = "",
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            IsPlanned = a.IsPlanned,
                            FirstName = (t.CustomerName != null) ? t.CustomerName : a.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,

                            AssistID = a.Id

                        }).OrderBy(o => o.AssistID).ToList();


                listNotAssigned.ForEach(x =>
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskStartDateTime)))
                    {
                        x.RiskStartDate = Convert.ToDateTime(x.dtRiskStartDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskStartDate = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskEndDateTime)))
                    {
                        x.RiskEndDate = Convert.ToDateTime(x.dtRiskEndDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskEndDate = "";
                    }

                    if (x.IsPlanned == true)
                    {
                        x.dtReportedDateTime = x.dtPlannedDateTime;
                    }
                    else
                    {
                        if (x.CertificateNo.ToUpper().Contains("AT"))
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime1;
                        }
                        else
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime2;
                        }
                    }                    

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)))
                    {
                        x.ReportedDate = Convert.ToDateTime(x.dtReportedDateTime).ToString("yyyy-MM-dd");
                        x.ReportedTime = Convert.ToDateTime(x.dtReportedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.ReportedDate = "";
                        x.ReportedTime = "";
                    }

                });
                #endregion

                #region statusid = 9
                statusid = 9;
                listAssigned = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join action in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals action.StatusID into objaction
                        from action in objaction.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        join makebased in db.tblMstMakes.AsNoTracking() on t.MakeID equals makebased.MakeID into objMake
                        from make in objMake.DefaultIfEmpty()
                        join modelbased in db.tblMstModels.AsNoTracking() on t.ModelID equals modelbased.ModelID into objModel
                        from model in objModel.DefaultIfEmpty()
                        join city in db.tblMstCities.AsNoTracking() on t.PermanentCityID equals city.CityID into objcity
                        from city in objcity.DefaultIfEmpty()
                        join state in db.tblMstStates.AsNoTracking() on city.StateID equals state.StateID into objstate
                        from state in objstate.DefaultIfEmpty()
                        join incdetail in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals incdetail.Detailid into objincdetail
                        from incdetail in objincdetail.DefaultIfEmpty()
                        join inc in db.tblMstIncidents.AsNoTracking() on incdetail.IncidentDetailid equals inc.Incidentid into objinc
                        from inc in objinc.DefaultIfEmpty()
                        join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID into objbranch
                        from branch in objbranch.DefaultIfEmpty()
                        join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                        from vendor in objvendor.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid &&
                        (a.AssistOEMID == intOEMId || intOEMId == null) &&
                        (a.AssistCompanyID == intCompanyId || intCompanyId == null) &&
                        (a.Remark1 == RemarkId || string.IsNullOrEmpty(RemarkId)))
                        select new AssistanceMISReport
                        {
                            AgentName = user.Name,
                            RefNo = a.RefNo,
                            PolicyNo = t.CertificateNo,
                            dtRiskStartDateTime = t.CoverStartDate,
                            dtRiskEndDateTime = t.CoverEndDate,
                            RSACover = "Yes",
                            MakeAndModel = make.Name + "  " + model.Name + "  " + model.Variant,
                            State = state.State,
                            City = city.City,
                            RegistrationNo = t.RegistrationNo,
                            ContactNo = (vendor.VendorMobileNo != null) ? vendor.VendorMobileNo : (vendor.CompanyPhoneNo != null) ? vendor.CompanyPhoneNo : "",
                            dtReportedDateTime1 = t.CreatedDate,
                            dtReportedDateTime2 = a.AssistCreatedDate,
                            dtPlannedDateTime = a.PlannedDateTime,
                            ClosureDate = "",
                            ClosureTime = "",
                            dtVendorAssignedDateTime = a.VendorAssignedDateTime,
                            dtVendorReachedIncidentLocDateTime1 = a.VenReachedIncLocDateTime,
                            dtVendorReachedIncidentLocDateTime2 = a.Ven_ReachedTime,
                            dtVendorReachedDropLocDateTime1 = a.VenReachedDropLocDateTime,
                            dtVendorReachedDropLocDateTime2 = a.Ven_DropTime,
                            ServiceTime = "",
                            Action = action.Status,
                            Status = status.Status,
                            CompanyName = cmnType.Description,
                            VendorName = vendor.VendorName,
                            BranchName = branch.BranchName,
                            IncidentLocation = a.Address,
                            DropLocation = (a.DropLocationAddress != null) ? a.DropLocationAddress : (a.Ven_DropLocation != null) ? a.Ven_DropLocation : "",
                            VendorToIncident = "",
                            IToD = (a.DropLocationKM != null) ? a.DropLocationKM : (a.Ven_TotalRunningKMs != null) ? a.Ven_TotalRunningKMs : "",
                            TotalKM = "",
                            TotalCost = "",
                            TollCharges = "",
                            WaitingHoursCharge = "",
                            VehicleCustodyHoursCharge = "",
                            OthersCharge = "",
                            CustomerPaidAmount = "",
                            SelectIncident = inc.IncidentType + " - " + incdetail.IncidentDetail,
                            CaseSummary = a.SummaryDetails,
                            TAT1 = "",
                            TAT2 = "",
                            TAT3 = "",
                            Remark = "",
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            IsPlanned = a.IsPlanned,
                            FirstName = (t.CustomerName != null) ? t.CustomerName : a.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,

                            AssistID = a.Id,
                            AssistLatitude = a.Lat,
                            AssistLongitude = a.Lon,
                            VendorLatitude = branch.Latitudes,
                            VendorLongitude = branch.Longitudes

                        }).OrderBy(o => o.AssistID).ToList();


                listAssigned.ForEach(x =>
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskStartDateTime)))
                    {
                        x.RiskStartDate = Convert.ToDateTime(x.dtRiskStartDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskStartDate = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskEndDateTime)))
                    {
                        x.RiskEndDate = Convert.ToDateTime(x.dtRiskEndDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskEndDate = "";
                    }

                    if (x.IsPlanned == true)
                    {
                        x.dtReportedDateTime = x.dtPlannedDateTime;
                    }
                    else
                    {
                        if (x.CertificateNo.ToUpper().Contains("AT"))
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime1;
                        }
                        else
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime2;
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)))
                    {
                        x.ReportedDate = Convert.ToDateTime(x.dtReportedDateTime).ToString("yyyy-MM-dd");
                        x.ReportedTime = Convert.ToDateTime(x.dtReportedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.ReportedDate = "";
                        x.ReportedTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)))
                    {
                        x.VendorAssignedDate = Convert.ToDateTime(x.dtVendorAssignedDateTime).ToString("yyyy-MM-dd");
                        x.VendorAssignedTime = Convert.ToDateTime(x.dtVendorAssignedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.VendorAssignedDate = "";
                        x.VendorAssignedTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        x.VendorReachedIncidentLocDate = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1).ToString("yyyy-MM-dd");
                        x.VendorReachedIncidentLocTime = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1).ToString("HH:mm:ss");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            x.VendorReachedIncidentLocDate = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2).ToString("yyyy-MM-dd");
                            x.VendorReachedIncidentLocTime = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2).ToString("HH:mm:ss");
                        }
                        else
                        {
                            x.VendorReachedIncidentLocDate = "";
                            x.VendorReachedIncidentLocTime = "";
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedDropLocDateTime1)))
                    {
                        x.VendorReachedDropLocDate = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime1).ToString("yyyy-MM-dd");
                        x.VendorReachedDropLocTime = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime1).ToString("HH:mm:ss");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(x.dtVendorReachedDropLocDateTime2))
                        {
                            x.VendorReachedDropLocDate = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime2).ToString("yyyy-MM-dd");
                            x.VendorReachedDropLocTime = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime2).ToString("HH:mm:ss");
                        }
                        else
                        {
                            x.VendorReachedDropLocDate = "";
                            x.VendorReachedDropLocTime = "";
                        }
                    }

                    #region Calculate Distance Between Vendor Location To Incident Location in KM Logic Part

                    // Vendor Latitude and Longitude
                    double VenLat = 0;
                    double VenLon = 0;
                    var regex = new Regex("^[0-9.-]*$");

                    string strVenLat = x.VendorLatitude;
                    string strVenLon = x.VendorLongitude;

                    if (string.IsNullOrEmpty(strVenLat))
                    {
                        VenLat = 0;
                    }
                    else if (!regex.IsMatch(strVenLat))
                    {
                        VenLat = 0;
                    }
                    else
                    {
                        VenLat = Convert.ToDouble(x.VendorLatitude);
                    }

                    if (string.IsNullOrEmpty(strVenLon))
                    {
                        VenLon = 0;
                    }
                    else if (!regex.IsMatch(strVenLon))
                    {
                        VenLon = 0;
                    }
                    else
                    {
                        VenLon = Convert.ToDouble(x.VendorLongitude);
                    }

                    // Assist Latitude and Longitude
                    double AssistLat = 0;
                    double AssistLon = 0;
                    var regexAssist = new Regex("^[0-9.-]*$");

                    string strAssistLat = x.AssistLatitude;
                    string strAssistLon = x.AssistLongitude;

                    if (string.IsNullOrEmpty(strAssistLat))
                    {
                        AssistLat = 0;
                    }
                    else if (!regexAssist.IsMatch(strAssistLat))
                    {
                        AssistLat = 0;
                    }
                    else
                    {
                        AssistLat = Convert.ToDouble(x.AssistLatitude);
                    }

                    if (string.IsNullOrEmpty(strAssistLon))
                    {
                        AssistLon = 0;
                    }
                    else if (!regexAssist.IsMatch(strAssistLon))
                    {
                        AssistLon = 0;
                    }
                    else
                    {
                        AssistLon = Convert.ToDouble(x.AssistLongitude);
                    }

                    double Distance = DistanceInMeter(VenLat, VenLon, AssistLat, AssistLon);
                    double DistanceInKM = Distance / 1000;

                    x.VendorToIncident = Convert.ToString(DistanceInKM);

                    #endregion

                    #region Calculate Time Difference Between Two Dates Logic Part

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorAssignedDateTime);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT2 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        x.TAT2 = "";
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtVendorAssignedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT3 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)) && !string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            DateTime dtDate1 = Convert.ToDateTime(x.dtVendorAssignedDateTime);
                            DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2);

                            TimeSpan TSpanDifference = dtDate2 - dtDate1;
                            decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                            int DiffInMinutes = TSpanDifference.Minutes;
                            int DiffInSeconds = TSpanDifference.Seconds;

                            x.TAT3 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                        }
                        else
                        {
                            x.TAT3 = "";
                        }
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.ServiceTime = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                            DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2);

                            TimeSpan TSpanDifference = dtDate2 - dtDate1;
                            decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                            int DiffInMinutes = TSpanDifference.Minutes;
                            int DiffInSeconds = TSpanDifference.Seconds;

                            x.ServiceTime = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                        }
                        else
                        {
                            x.ServiceTime = "";
                        }
                    }

                    #endregion

                });
                #endregion

                #region statusid = 10
                statusid = 10;
                listClosed = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join action in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals action.StatusID into objaction
                        from action in objaction.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        join makebased in db.tblMstMakes.AsNoTracking() on t.MakeID equals makebased.MakeID into objMake
                        from make in objMake.DefaultIfEmpty()
                        join modelbased in db.tblMstModels.AsNoTracking() on t.ModelID equals modelbased.ModelID into objModel
                        from model in objModel.DefaultIfEmpty()
                        join city in db.tblMstCities.AsNoTracking() on t.PermanentCityID equals city.CityID into objcity
                        from city in objcity.DefaultIfEmpty()
                        join state in db.tblMstStates.AsNoTracking() on city.StateID equals state.StateID into objstate
                        from state in objstate.DefaultIfEmpty()
                        join incdetail in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals incdetail.Detailid into objincdetail
                        from incdetail in objincdetail.DefaultIfEmpty()
                        join inc in db.tblMstIncidents.AsNoTracking() on incdetail.IncidentDetailid equals inc.Incidentid into objinc
                        from inc in objinc.DefaultIfEmpty()
                        join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID into objbranch
                        from branch in objbranch.DefaultIfEmpty()
                        join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                        from vendor in objvendor.DefaultIfEmpty()
                        join casecharges in db.tblCaseCharges.AsNoTracking() on a.Id equals casecharges.AssistID into objcasecharges
                        from casecharges in objcasecharges.DefaultIfEmpty()
                        join mctcasetype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCaseTypeID equals mctcasetype.CommonTypeID into objmctcasetype
                        from mctcasetype in objmctcasetype.DefaultIfEmpty()
                        join mctsubcategory in db.tblMstCommonTypes.AsNoTracking() on a.AssistSubCategoryID equals mctsubcategory.CommonTypeID into objmctsubcategory
                        from mctsubcategory in objmctsubcategory.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid &&
                        (a.AssistOEMID == intOEMId || intOEMId == null) &&
                        (a.AssistCompanyID == intCompanyId || intCompanyId == null) &&
                        (a.Remark1 == RemarkId || string.IsNullOrEmpty(RemarkId)))
                        select new AssistanceMISReport
                        {
                            AgentName = user.Name,
                            RefNo = a.RefNo,
                            PolicyNo = t.CertificateNo,
                            dtRiskStartDateTime = t.CoverStartDate,
                            dtRiskEndDateTime = t.CoverEndDate,
                            RSACover = "Yes",
                            MakeAndModel = make.Name + "  " + model.Name + "  " + model.Variant,
                            State = state.State,
                            City = city.City,
                            RegistrationNo = t.RegistrationNo,
                            ContactNo = (vendor.VendorMobileNo != null) ? vendor.VendorMobileNo : (vendor.CompanyPhoneNo != null) ? vendor.CompanyPhoneNo : "",
                            dtReportedDateTime1 = t.CreatedDate,
                            dtReportedDateTime2 = a.AssistCreatedDate,
                            dtPlannedDateTime = a.PlannedDateTime,
                            dtClosureDateTime = a.CreatedDate,
                            dtVendorAssignedDateTime = a.VendorAssignedDateTime,
                            dtVendorReachedIncidentLocDateTime1 = a.VenReachedIncLocDateTime,
                            dtVendorReachedIncidentLocDateTime2 = a.Ven_ReachedTime,
                            dtVendorReachedDropLocDateTime1 = a.VenReachedDropLocDateTime,
                            dtVendorReachedDropLocDateTime2 = a.Ven_DropTime,
                            ServiceTime = "",
                            Action = action.Status,
                            Status = status.Status,
                            CompanyName = cmnType.Description,
                            VendorName = vendor.VendorName,
                            BranchName = branch.BranchName,
                            IncidentLocation = a.Address,
                            DropLocation = (a.DropLocationAddress != null) ? a.DropLocationAddress : (a.Ven_DropLocation != null) ? a.Ven_DropLocation : "",
                            VendorToIncident = "",
                            IToD = (a.DropLocationKM != null) ? a.DropLocationKM : (a.Ven_TotalRunningKMs != null) ? a.Ven_TotalRunningKMs : "",
                            decTotalKM = casecharges.TotalKiloMeters,
                            decTotalCost = casecharges.FinalAmount,
                            decTollCharges = casecharges.TollCharges,
                            decWaitingHoursCharge = casecharges.WaitingHoursCharge,
                            decVehicleCustodyHoursCharge = casecharges.VehicleCustodyCharge,
                            decOthersCharge = casecharges.OtherCharges,
                            decCustomerPaidAmount = casecharges.FinalAmount,
                            decCustomerAmountPaid = casecharges.CustomerPaidAmount,
                            dtCustomerPaidDate = casecharges.CustomerPaidDate,
                            ReferenceNo = casecharges.ReferenceNo,
                            SelectIncident = inc.IncidentType + " - " + incdetail.IncidentDetail,
                            CaseSummary = a.SummaryDetails,
                            TAT1 = "",
                            TAT2 = "",
                            TAT3 = "",
                            Remark = (a.Remark1 != null) ? a.Remark1 : "",
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            IsPlanned = a.IsPlanned,
                            FirstName = (t.CustomerName != null) ? t.CustomerName : a.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            CaseType = mctcasetype.Description,
                            SubCategory = mctsubcategory.Description,

                            AssistID = a.Id,
                            AssistLatitude = a.Lat,
                            AssistLongitude = a.Lon,
                            VendorLatitude = branch.Latitudes,
                            VendorLongitude = branch.Longitudes

                        }).OrderBy(o => o.AssistID).ToList();


                listClosed.ForEach(x =>
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskStartDateTime)))
                    {
                        x.RiskStartDate = Convert.ToDateTime(x.dtRiskStartDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskStartDate = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskEndDateTime)))
                    {
                        x.RiskEndDate = Convert.ToDateTime(x.dtRiskEndDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskEndDate = "";
                    }

                    if (x.IsPlanned == true)
                    {
                        x.dtReportedDateTime = x.dtPlannedDateTime;
                    }
                    else
                    {
                        if (x.CertificateNo.ToUpper().Contains("AT"))
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime1;
                        }
                        else
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime2;
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)))
                    {
                        x.ReportedDate = Convert.ToDateTime(x.dtReportedDateTime).ToString("yyyy-MM-dd");
                        x.ReportedTime = Convert.ToDateTime(x.dtReportedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.ReportedDate = "";
                        x.ReportedTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)))
                    {
                        x.VendorAssignedDate = Convert.ToDateTime(x.dtVendorAssignedDateTime).ToString("yyyy-MM-dd");
                        x.VendorAssignedTime = Convert.ToDateTime(x.dtVendorAssignedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.VendorAssignedDate = "";
                        x.VendorAssignedTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        x.VendorReachedIncidentLocDate = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1).ToString("yyyy-MM-dd");
                        x.VendorReachedIncidentLocTime = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1).ToString("HH:mm:ss");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            x.VendorReachedIncidentLocDate = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2).ToString("yyyy-MM-dd");
                            x.VendorReachedIncidentLocTime = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2).ToString("HH:mm:ss");
                        }
                        else
                        {
                            x.VendorReachedIncidentLocDate = "";
                            x.VendorReachedIncidentLocTime = "";
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedDropLocDateTime1)))
                    {
                        x.VendorReachedDropLocDate = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime1).ToString("yyyy-MM-dd");
                        x.VendorReachedDropLocTime = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime1).ToString("HH:mm:ss");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(x.dtVendorReachedDropLocDateTime2))
                        {
                            x.VendorReachedDropLocDate = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime2).ToString("yyyy-MM-dd");
                            x.VendorReachedDropLocTime = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime2).ToString("HH:mm:ss");
                        }
                        else
                        {
                            x.VendorReachedDropLocDate = "";
                            x.VendorReachedDropLocTime = "";
                        }
                    }

                    #region Calculate Distance Between Vendor Location To Incident Location in KM Logic Part

                    // Vendor Latitude and Longitude
                    double VenLat = 0;
                    double VenLon = 0;
                    var regex = new Regex("^[0-9.-]*$");

                    string strVenLat = x.VendorLatitude;
                    string strVenLon = x.VendorLongitude;

                    if (string.IsNullOrEmpty(strVenLat))
                    {
                        VenLat = 0;
                    }
                    else if (!regex.IsMatch(strVenLat))
                    {
                        VenLat = 0;
                    }
                    else
                    {
                        VenLat = Convert.ToDouble(x.VendorLatitude);
                    }

                    if (string.IsNullOrEmpty(strVenLon))
                    {
                        VenLon = 0;
                    }
                    else if (!regex.IsMatch(strVenLon))
                    {
                        VenLon = 0;
                    }
                    else
                    {
                        VenLon = Convert.ToDouble(x.VendorLongitude);
                    }

                    // Assist Latitude and Longitude
                    double AssistLat = 0;
                    double AssistLon = 0;
                    var regexAssist = new Regex("^[0-9.-]*$");

                    string strAssistLat = x.AssistLatitude;
                    string strAssistLon = x.AssistLongitude;

                    if (string.IsNullOrEmpty(strAssistLat))
                    {
                        AssistLat = 0;
                    }
                    else if (!regexAssist.IsMatch(strAssistLat))
                    {
                        AssistLat = 0;
                    }
                    else
                    {
                        AssistLat = Convert.ToDouble(x.AssistLatitude);
                    }

                    if (string.IsNullOrEmpty(strAssistLon))
                    {
                        AssistLon = 0;
                    }
                    else if (!regexAssist.IsMatch(strAssistLon))
                    {
                        AssistLon = 0;
                    }
                    else
                    {
                        AssistLon = Convert.ToDouble(x.AssistLongitude);
                    }

                    double Distance = DistanceInMeter(VenLat, VenLon, AssistLat, AssistLon);
                    double DistanceInKM = Distance / 1000;

                    x.VendorToIncident = Convert.ToString(DistanceInKM);

                    #endregion

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtClosureDateTime)))
                    {
                        x.ClosureDate = Convert.ToDateTime(x.dtClosureDateTime).ToString("yyyy-MM-dd");
                        x.ClosureTime = Convert.ToDateTime(x.dtClosureDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.ClosureDate = "";
                        x.ClosureTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decTotalKM)))
                    {
                        x.TotalKM = Convert.ToString(x.decTotalKM);
                    }
                    else
                    {
                        x.TotalKM = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decTotalCost)))
                    {
                        x.TotalCost = Convert.ToString(x.decTotalCost);
                    }
                    else
                    {
                        x.TotalCost = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decTollCharges)))
                    {
                        x.TollCharges = Convert.ToString(x.decTollCharges);
                    }
                    else
                    {
                        x.TollCharges = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decWaitingHoursCharge)))
                    {
                        x.WaitingHoursCharge = Convert.ToString(x.decWaitingHoursCharge);
                    }
                    else
                    {
                        x.WaitingHoursCharge = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decVehicleCustodyHoursCharge)))
                    {
                        x.VehicleCustodyHoursCharge = Convert.ToString(x.decVehicleCustodyHoursCharge);
                    }
                    else
                    {
                        x.VehicleCustodyHoursCharge = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decOthersCharge)))
                    {
                        x.OthersCharge = Convert.ToString(x.decOthersCharge);
                    }
                    else
                    {
                        x.OthersCharge = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decCustomerPaidAmount)))
                    {
                        x.CustomerPaidAmount = Convert.ToString(x.decCustomerPaidAmount);
                    }
                    else
                    {
                        x.CustomerPaidAmount = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decCustomerAmountPaid)))
                    {
                        x.CustomerAmountPaid = Convert.ToString(x.decCustomerAmountPaid);
                    }
                    else
                    {
                        x.CustomerAmountPaid = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtCustomerPaidDate)))
                    {
                        x.CustomerPaidDate = Convert.ToDateTime(x.dtCustomerPaidDate).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.CustomerPaidDate = "";
                    }

                    #region Calculate Time Difference Between Two Dates Logic Part

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtClosureDateTime)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtClosureDateTime);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT1 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        x.TAT1 = "";
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorAssignedDateTime);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT2 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        x.TAT2 = "";
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtVendorAssignedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT3 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)) && !string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            DateTime dtDate1 = Convert.ToDateTime(x.dtVendorAssignedDateTime);
                            DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2);

                            TimeSpan TSpanDifference = dtDate2 - dtDate1;
                            decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                            int DiffInMinutes = TSpanDifference.Minutes;
                            int DiffInSeconds = TSpanDifference.Seconds;

                            x.TAT3 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                        }
                        else
                        {
                            x.TAT3 = "";
                        }
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.ServiceTime = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                            DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2);

                            TimeSpan TSpanDifference = dtDate2 - dtDate1;
                            decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                            int DiffInMinutes = TSpanDifference.Minutes;
                            int DiffInSeconds = TSpanDifference.Seconds;

                            x.ServiceTime = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                        }
                        else
                        {
                            x.ServiceTime = "";
                        }
                    }

                    #endregion


                    if (!string.IsNullOrEmpty(x.Remark))
                    {
                        int intRemark = 0;
                        int.TryParse(x.Remark, out intRemark);

                        string dbRemark = db.tblMstCommonTypes.AsNoTracking().Where(w => w.CommonTypeID == intRemark).Select(s => s.Description).FirstOrDefault();
                        if (!string.IsNullOrEmpty(dbRemark))
                        {
                            x.Remark = dbRemark;
                        }
                        else
                        {
                            x.Remark = "";
                        }
                    }
                    else
                    {
                        x.Remark = "";
                    }

                });
                #endregion

                list.AddRange(listNotAssigned);
                list.AddRange(listAssigned);
                list.AddRange(listClosed);

                list.OrderBy(o => o.AssistID);
            }
            else if(statusid == CrossCutting_Constant.StatusID_Vendor_Not_Assign)
            {
                list = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        join makebased in db.tblMstMakes.AsNoTracking() on t.MakeID equals makebased.MakeID into objMake
                        from make in objMake.DefaultIfEmpty()
                        join modelbased in db.tblMstModels.AsNoTracking() on t.ModelID equals modelbased.ModelID into objModel
                        from model in objModel.DefaultIfEmpty()
                        join city in db.tblMstCities.AsNoTracking() on t.PermanentCityID equals city.CityID into objcity
                        from city in objcity.DefaultIfEmpty()
                        join state in db.tblMstStates.AsNoTracking() on city.StateID equals state.StateID into objstate
                        from state in objstate.DefaultIfEmpty()
                        join incdetail in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals incdetail.Detailid into objincdetail
                        from incdetail in objincdetail.DefaultIfEmpty()
                        join inc in db.tblMstIncidents.AsNoTracking() on incdetail.IncidentDetailid equals inc.Incidentid into objinc
                        from inc in objinc.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid &&
                        (a.AssistOEMID == intOEMId || intOEMId == null) &&
                        (a.AssistCompanyID == intCompanyId || intCompanyId == null) &&
                        (a.Remark1 == RemarkId || string.IsNullOrEmpty(RemarkId)))
                        select new AssistanceMISReport
                        {                            
                            AgentName = user.Name,
                            RefNo = a.RefNo,
                            PolicyNo = t.CertificateNo,
                            dtRiskStartDateTime = t.CoverStartDate,
                            dtRiskEndDateTime = t.CoverEndDate,
                            RSACover = "Yes",
                            MakeAndModel = make.Name + "  " + model.Name + "  " + model.Variant,
                            State = state.State,
                            City = city.City,
                            RegistrationNo = t.RegistrationNo,
                            ContactNo = "",
                            dtReportedDateTime1 = t.CreatedDate,
                            dtReportedDateTime2 = a.AssistCreatedDate,
                            dtPlannedDateTime = a.PlannedDateTime,
                            ClosureDate = "",
                            ClosureTime = "",
                            VendorAssignedDate = "",
                            VendorAssignedTime = "",
                            VendorReachedIncidentLocDate = "",
                            VendorReachedIncidentLocTime = "",
                            VendorReachedDropLocDate = "",
                            VendorReachedDropLocTime = "",
                            ServiceTime = "",
                            Action = "",
                            Status = status.Status,
                            CompanyName = cmnType.Description,
                            VendorName = "",
                            BranchName = "",
                            IncidentLocation = a.Address,
                            DropLocation = (a.DropLocationAddress != null) ? a.DropLocationAddress : (a.Ven_DropLocation != null) ? a.Ven_DropLocation : "",
                            VendorToIncident = "",
                            IToD = (a.DropLocationKM != null) ? a.DropLocationKM : (a.Ven_TotalRunningKMs != null) ? a.Ven_TotalRunningKMs : "",
                            TotalKM = "",
                            TotalCost = "",
                            TollCharges = "",
                            WaitingHoursCharge = "",
                            VehicleCustodyHoursCharge = "",
                            OthersCharge = "",
                            CustomerPaidAmount = "",
                            SelectIncident = inc.IncidentType + " - " + incdetail.IncidentDetail,
                            CaseSummary = a.SummaryDetails,
                            TAT1 = "",
                            TAT2 = "",
                            TAT3 = "",
                            Remark = "",
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            IsPlanned = a.IsPlanned,
                            FirstName = (t.CustomerName != null) ? t.CustomerName : a.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,

                            AssistID = a.Id

                        }).OrderBy(o => o.AssistID).ToList();


                list.ForEach(x =>
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskStartDateTime)))
                    {
                        x.RiskStartDate = Convert.ToDateTime(x.dtRiskStartDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskStartDate = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskEndDateTime)))
                    {
                        x.RiskEndDate = Convert.ToDateTime(x.dtRiskEndDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskEndDate = "";
                    }

                    if (x.IsPlanned == true)
                    {
                        x.dtReportedDateTime = x.dtPlannedDateTime;
                    }
                    else
                    {
                        if (x.CertificateNo.ToUpper().Contains("AT"))
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime1;
                        }
                        else
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime2;
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)))
                    {
                        x.ReportedDate = Convert.ToDateTime(x.dtReportedDateTime).ToString("yyyy-MM-dd");
                        x.ReportedTime = Convert.ToDateTime(x.dtReportedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.ReportedDate = "";
                        x.ReportedTime = "";
                    }

                });
            }
            else if (statusid == CrossCutting_Constant.StatusID_Vendor_Assigned)
            {
                list = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join action in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals action.StatusID into objaction
                        from action in objaction.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        join makebased in db.tblMstMakes.AsNoTracking() on t.MakeID equals makebased.MakeID into objMake
                        from make in objMake.DefaultIfEmpty()
                        join modelbased in db.tblMstModels.AsNoTracking() on t.ModelID equals modelbased.ModelID into objModel
                        from model in objModel.DefaultIfEmpty()
                        join city in db.tblMstCities.AsNoTracking() on t.PermanentCityID equals city.CityID into objcity
                        from city in objcity.DefaultIfEmpty()
                        join state in db.tblMstStates.AsNoTracking() on city.StateID equals state.StateID into objstate
                        from state in objstate.DefaultIfEmpty()
                        join incdetail in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals incdetail.Detailid into objincdetail
                        from incdetail in objincdetail.DefaultIfEmpty()
                        join inc in db.tblMstIncidents.AsNoTracking() on incdetail.IncidentDetailid equals inc.Incidentid into objinc
                        from inc in objinc.DefaultIfEmpty()
                        join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID into objbranch
                        from branch in objbranch.DefaultIfEmpty()
                        join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                        from vendor in objvendor.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid &&
                        (a.AssistOEMID == intOEMId || intOEMId == null) &&
                        (a.AssistCompanyID == intCompanyId || intCompanyId == null) &&
                        (a.Remark1 == RemarkId || string.IsNullOrEmpty(RemarkId)))
                        select new AssistanceMISReport
                        {                            
                            AgentName = user.Name,
                            RefNo = a.RefNo,
                            PolicyNo = t.CertificateNo,
                            dtRiskStartDateTime = t.CoverStartDate,
                            dtRiskEndDateTime = t.CoverEndDate,
                            RSACover = "Yes",
                            MakeAndModel = make.Name + "  " + model.Name + "  " + model.Variant,
                            State = state.State,
                            City = city.City,
                            RegistrationNo = t.RegistrationNo,
                            ContactNo = (vendor.VendorMobileNo != null) ? vendor.VendorMobileNo : (vendor.CompanyPhoneNo != null) ? vendor.CompanyPhoneNo : "",
                            dtReportedDateTime1 = t.CreatedDate,
                            dtReportedDateTime2 = a.AssistCreatedDate,
                            dtPlannedDateTime = a.PlannedDateTime,
                            ClosureDate = "",
                            ClosureTime = "",
                            dtVendorAssignedDateTime = a.VendorAssignedDateTime,
                            dtVendorReachedIncidentLocDateTime1 = a.VenReachedIncLocDateTime,
                            dtVendorReachedIncidentLocDateTime2 = a.Ven_ReachedTime,
                            dtVendorReachedDropLocDateTime1 = a.VenReachedDropLocDateTime,
                            dtVendorReachedDropLocDateTime2 = a.Ven_DropTime,
                            ServiceTime = "",
                            Action = action.Status,
                            Status = status.Status,
                            CompanyName = cmnType.Description,
                            VendorName = vendor.VendorName,
                            BranchName = branch.BranchName,
                            IncidentLocation = a.Address,
                            DropLocation = (a.DropLocationAddress != null) ? a.DropLocationAddress : (a.Ven_DropLocation != null) ? a.Ven_DropLocation : "",
                            VendorToIncident = "",
                            IToD = (a.DropLocationKM != null) ? a.DropLocationKM : (a.Ven_TotalRunningKMs != null) ? a.Ven_TotalRunningKMs : "",
                            TotalKM = "",
                            TotalCost = "",
                            TollCharges = "",
                            WaitingHoursCharge = "",
                            VehicleCustodyHoursCharge = "",
                            OthersCharge = "",
                            CustomerPaidAmount = "",
                            SelectIncident = inc.IncidentType + " - " + incdetail.IncidentDetail,
                            CaseSummary = a.SummaryDetails,
                            TAT1 = "",
                            TAT2 = "",
                            TAT3 = "",
                            Remark = "",
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            IsPlanned = a.IsPlanned,
                            FirstName = (t.CustomerName != null) ? t.CustomerName : a.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,

                            AssistID = a.Id,
                            AssistLatitude = a.Lat,
                            AssistLongitude = a.Lon,
                            VendorLatitude = branch.Latitudes,
                            VendorLongitude = branch.Longitudes

                        }).OrderBy(o => o.AssistID).ToList();


                list.ForEach(x =>
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskStartDateTime)))
                    {
                        x.RiskStartDate = Convert.ToDateTime(x.dtRiskStartDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskStartDate = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskEndDateTime)))
                    {
                        x.RiskEndDate = Convert.ToDateTime(x.dtRiskEndDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskEndDate = "";
                    }

                    if (x.IsPlanned == true)
                    {
                        x.dtReportedDateTime = x.dtPlannedDateTime;
                    }
                    else
                    {
                        if (x.CertificateNo.ToUpper().Contains("AT"))
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime1;
                        }
                        else
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime2;
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)))
                    {
                        x.ReportedDate = Convert.ToDateTime(x.dtReportedDateTime).ToString("yyyy-MM-dd");
                        x.ReportedTime = Convert.ToDateTime(x.dtReportedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.ReportedDate = "";
                        x.ReportedTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)))
                    {
                        x.VendorAssignedDate = Convert.ToDateTime(x.dtVendorAssignedDateTime).ToString("yyyy-MM-dd");
                        x.VendorAssignedTime = Convert.ToDateTime(x.dtVendorAssignedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.VendorAssignedDate = "";
                        x.VendorAssignedTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        x.VendorReachedIncidentLocDate = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1).ToString("yyyy-MM-dd");
                        x.VendorReachedIncidentLocTime = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1).ToString("HH:mm:ss");
                    }
                    else
                    {                        
                        if (!string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            x.VendorReachedIncidentLocDate = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2).ToString("yyyy-MM-dd");
                            x.VendorReachedIncidentLocTime = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2).ToString("HH:mm:ss");
                        }
                        else
                        {
                            x.VendorReachedIncidentLocDate = "";
                            x.VendorReachedIncidentLocTime = "";
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedDropLocDateTime1)))
                    {
                        x.VendorReachedDropLocDate = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime1).ToString("yyyy-MM-dd");
                        x.VendorReachedDropLocTime = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime1).ToString("HH:mm:ss");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(x.dtVendorReachedDropLocDateTime2))
                        {
                            x.VendorReachedDropLocDate = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime2).ToString("yyyy-MM-dd");
                            x.VendorReachedDropLocTime = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime2).ToString("HH:mm:ss");
                        }
                        else
                        {
                            x.VendorReachedDropLocDate = "";
                            x.VendorReachedDropLocTime = "";
                        }
                    }

                    #region Calculate Distance Between Vendor Location To Incident Location in KM Logic Part

                    // Vendor Latitude and Longitude
                    double VenLat = 0;
                    double VenLon = 0;
                    var regex = new Regex("^[0-9.-]*$");

                    string strVenLat = x.VendorLatitude;
                    string strVenLon = x.VendorLongitude;

                    if (string.IsNullOrEmpty(strVenLat))
                    {
                        VenLat = 0;
                    }
                    else if (!regex.IsMatch(strVenLat))
                    {
                        VenLat = 0;
                    }
                    else
                    {
                        VenLat = Convert.ToDouble(x.VendorLatitude);
                    }

                    if (string.IsNullOrEmpty(strVenLon))
                    {
                        VenLon = 0;
                    }
                    else if (!regex.IsMatch(strVenLon))
                    {
                        VenLon = 0;
                    }
                    else
                    {
                        VenLon = Convert.ToDouble(x.VendorLongitude);
                    }

                    // Assist Latitude and Longitude
                    double AssistLat = 0;
                    double AssistLon = 0;
                    var regexAssist = new Regex("^[0-9.-]*$");

                    string strAssistLat = x.AssistLatitude;
                    string strAssistLon = x.AssistLongitude;

                    if (string.IsNullOrEmpty(strAssistLat))
                    {
                        AssistLat = 0;
                    }
                    else if (!regexAssist.IsMatch(strAssistLat))
                    {
                        AssistLat = 0;
                    }
                    else
                    {
                        AssistLat = Convert.ToDouble(x.AssistLatitude);
                    }

                    if (string.IsNullOrEmpty(strAssistLon))
                    {
                        AssistLon = 0;
                    }
                    else if (!regexAssist.IsMatch(strAssistLon))
                    {
                        AssistLon = 0;
                    }
                    else
                    {
                        AssistLon = Convert.ToDouble(x.AssistLongitude);
                    }

                    double Distance = DistanceInMeter(VenLat, VenLon, AssistLat, AssistLon);
                    double DistanceInKM = Distance / 1000;

                    x.VendorToIncident = Convert.ToString(DistanceInKM);

                    #endregion

                    #region Calculate Time Difference Between Two Dates Logic Part

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorAssignedDateTime);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT2 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);                        
                    }
                    else
                    {
                        x.TAT2 = "";
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtVendorAssignedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT3 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)) && !string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            DateTime dtDate1 = Convert.ToDateTime(x.dtVendorAssignedDateTime);
                            DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2);

                            TimeSpan TSpanDifference = dtDate2 - dtDate1;
                            decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                            int DiffInMinutes = TSpanDifference.Minutes;
                            int DiffInSeconds = TSpanDifference.Seconds;

                            x.TAT3 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                        }
                        else
                        {
                            x.TAT3 = "";
                        }
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.ServiceTime = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                            DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2);

                            TimeSpan TSpanDifference = dtDate2 - dtDate1;
                            decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                            int DiffInMinutes = TSpanDifference.Minutes;
                            int DiffInSeconds = TSpanDifference.Seconds;

                            x.ServiceTime = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                        }
                        else
                        {
                            x.ServiceTime = "";
                        }
                    }

                    #endregion

                });
            }
            else
            {
                list = (from a in db.tblAssistDetails
                        join t in db.tblTransactions.AsNoTracking() on a.TransactionId equals t.TransactionID
                        join user in db.AspNetUsers.AsNoTracking() on t.UserID equals user.Id
                        join s in db.tblMstWorkFlowStatus.AsNoTracking() on a.StatusId equals s.StatusID into objStatus
                        from status in objStatus.DefaultIfEmpty()
                        join action in db.tblMstWorkFlowStatus.AsNoTracking() on a.ActionId equals action.StatusID into objaction
                        from action in objaction.DefaultIfEmpty()
                        join cmntype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCompanyID equals cmntype.CommonTypeID into objcmntype
                        from cmnType in objcmntype.DefaultIfEmpty()
                        join makebased in db.tblMstMakes.AsNoTracking() on t.MakeID equals makebased.MakeID into objMake
                        from make in objMake.DefaultIfEmpty()
                        join modelbased in db.tblMstModels.AsNoTracking() on t.ModelID equals modelbased.ModelID into objModel
                        from model in objModel.DefaultIfEmpty()
                        join city in db.tblMstCities.AsNoTracking() on t.PermanentCityID equals city.CityID into objcity
                        from city in objcity.DefaultIfEmpty()
                        join state in db.tblMstStates.AsNoTracking() on city.StateID equals state.StateID into objstate
                        from state in objstate.DefaultIfEmpty()
                        join incdetail in db.tblMstIncidentDetails.AsNoTracking() on a.IncidentDetailid equals incdetail.Detailid into objincdetail
                        from incdetail in objincdetail.DefaultIfEmpty()
                        join inc in db.tblMstIncidents.AsNoTracking() on incdetail.IncidentDetailid equals inc.Incidentid into objinc
                        from inc in objinc.DefaultIfEmpty()
                        join branch in db.tblVendorBranchDetails on a.VendorId equals branch.BranchID into objbranch
                        from branch in objbranch.DefaultIfEmpty()
                        join vendor in db.tblMstVendors.AsNoTracking() on branch.VendorID equals vendor.VendorID into objvendor
                        from vendor in objvendor.DefaultIfEmpty()
                        join casecharges in db.tblCaseCharges.AsNoTracking() on a.Id equals casecharges.AssistID into objcasecharges
                        from casecharges in objcasecharges.DefaultIfEmpty()
                        join mctcasetype in db.tblMstCommonTypes.AsNoTracking() on a.AssistCaseTypeID equals mctcasetype.CommonTypeID into objmctcasetype
                        from mctcasetype in objmctcasetype.DefaultIfEmpty()
                        join mctsubcategory in db.tblMstCommonTypes.AsNoTracking() on a.AssistSubCategoryID equals mctsubcategory.CommonTypeID into objmctsubcategory
                        from mctsubcategory in objmctsubcategory.DefaultIfEmpty()
                        where
                        ((a.RefNo.Contains(refNo) || string.IsNullOrEmpty(refNo)) &&
                        (t.CustomerName.Contains(FirstName) || string.IsNullOrEmpty(FirstName)) &&
                        (t.CustomerLastName.Contains(LastName) || string.IsNullOrEmpty(LastName)) &&
                        (t.EngineNo.Contains(EngineNo) || string.IsNullOrEmpty(EngineNo)) &&
                        (t.ChassisNo.Contains(ChassisNo) || string.IsNullOrEmpty(ChassisNo)) &&
                        (t.CustomerContact.Contains(MobileNo) || string.IsNullOrEmpty(MobileNo)) &&
                        (t.CustomerEmail.Contains(EmailID) || string.IsNullOrEmpty(EmailID)) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) >= DbFunctions.TruncateTime(FromDate) || FromDate == null) &&
                        (DbFunctions.TruncateTime(a.AssistCreatedDate) <= DbFunctions.TruncateTime(ToDate) || ToDate == null) &&
                        a.StatusId == statusid &&
                        (a.AssistOEMID == intOEMId || intOEMId == null) &&
                        (a.AssistCompanyID == intCompanyId || intCompanyId == null) &&
                        (a.Remark1 == RemarkId || string.IsNullOrEmpty(RemarkId)))
                        select new AssistanceMISReport
                        {
                            AgentName = user.Name,
                            RefNo = a.RefNo,
                            PolicyNo = t.CertificateNo,
                            dtRiskStartDateTime = t.CoverStartDate,
                            dtRiskEndDateTime = t.CoverEndDate,
                            RSACover = "Yes",
                            MakeAndModel = make.Name + "  " + model.Name + "  " + model.Variant,
                            State = state.State,
                            City = city.City,
                            RegistrationNo = t.RegistrationNo,
                            ContactNo = (vendor.VendorMobileNo != null) ? vendor.VendorMobileNo : (vendor.CompanyPhoneNo != null) ? vendor.CompanyPhoneNo : "",
                            dtReportedDateTime1 = t.CreatedDate,
                            dtReportedDateTime2 = a.AssistCreatedDate,
                            dtPlannedDateTime = a.PlannedDateTime,
                            dtClosureDateTime = a.CreatedDate,
                            dtVendorAssignedDateTime = a.VendorAssignedDateTime,
                            dtVendorReachedIncidentLocDateTime1 = a.VenReachedIncLocDateTime,
                            dtVendorReachedIncidentLocDateTime2 = a.Ven_ReachedTime,
                            dtVendorReachedDropLocDateTime1 = a.VenReachedDropLocDateTime,
                            dtVendorReachedDropLocDateTime2 = a.Ven_DropTime,
                            ServiceTime = "",
                            Action = action.Status,
                            Status = status.Status,
                            CompanyName = cmnType.Description,
                            VendorName = vendor.VendorName,
                            BranchName = branch.BranchName,
                            IncidentLocation = a.Address,
                            DropLocation = (a.DropLocationAddress != null) ? a.DropLocationAddress : (a.Ven_DropLocation != null) ? a.Ven_DropLocation : "",
                            VendorToIncident = "",
                            IToD = (a.DropLocationKM != null) ? a.DropLocationKM : (a.Ven_TotalRunningKMs != null) ? a.Ven_TotalRunningKMs : "",
                            decTotalKM = casecharges.TotalKiloMeters,
                            decTotalCost = casecharges.FinalAmount,
                            decTollCharges = casecharges.TollCharges,
                            decWaitingHoursCharge = casecharges.WaitingHoursCharge,
                            decVehicleCustodyHoursCharge = casecharges.VehicleCustodyCharge,
                            decOthersCharge = casecharges.OtherCharges,
                            decCustomerPaidAmount = casecharges.FinalAmount,
                            decCustomerAmountPaid = casecharges.CustomerPaidAmount,
                            dtCustomerPaidDate = casecharges.CustomerPaidDate,
                            ReferenceNo = casecharges.ReferenceNo,
                            SelectIncident = inc.IncidentType + " - " + incdetail.IncidentDetail,
                            CaseSummary = a.SummaryDetails,
                            TAT1 = "",
                            TAT2 = "",
                            TAT3 = "",
                            Remark = (a.Remark1 != null) ? a.Remark1 : "",
                            CertificateNo = (t.CertificateNo != null) ? t.CertificateNo : "",
                            IsPlanned = a.IsPlanned,
                            FirstName = (t.CustomerName != null) ? t.CustomerName : a.CustomerName,
                            MiddleName = t.CustomerMiddleName,
                            LastName = t.CustomerLastName,
                            CaseType = mctcasetype.Description,
                            SubCategory = mctsubcategory.Description,

                            AssistID = a.Id,
                            AssistLatitude = a.Lat,
                            AssistLongitude = a.Lon,
                            VendorLatitude = branch.Latitudes,
                            VendorLongitude = branch.Longitudes

                        }).OrderBy(o => o.AssistID).ToList();


                list.ForEach(x =>
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskStartDateTime)))
                    {
                        x.RiskStartDate = Convert.ToDateTime(x.dtRiskStartDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskStartDate = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtRiskEndDateTime)))
                    {
                        x.RiskEndDate = Convert.ToDateTime(x.dtRiskEndDateTime).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.RiskEndDate = "";
                    }

                    if (x.IsPlanned == true)
                    {
                        x.dtReportedDateTime = x.dtPlannedDateTime;
                    }
                    else
                    {
                        if (x.CertificateNo.ToUpper().Contains("AT"))
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime1;
                        }
                        else
                        {
                            x.dtReportedDateTime = x.dtReportedDateTime2;
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)))
                    {
                        x.ReportedDate = Convert.ToDateTime(x.dtReportedDateTime).ToString("yyyy-MM-dd");
                        x.ReportedTime = Convert.ToDateTime(x.dtReportedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.ReportedDate = "";
                        x.ReportedTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)))
                    {
                        x.VendorAssignedDate = Convert.ToDateTime(x.dtVendorAssignedDateTime).ToString("yyyy-MM-dd");
                        x.VendorAssignedTime = Convert.ToDateTime(x.dtVendorAssignedDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.VendorAssignedDate = "";
                        x.VendorAssignedTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        x.VendorReachedIncidentLocDate = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1).ToString("yyyy-MM-dd");
                        x.VendorReachedIncidentLocTime = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1).ToString("HH:mm:ss");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            x.VendorReachedIncidentLocDate = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2).ToString("yyyy-MM-dd");
                            x.VendorReachedIncidentLocTime = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2).ToString("HH:mm:ss");
                        }
                        else
                        {
                            x.VendorReachedIncidentLocDate = "";
                            x.VendorReachedIncidentLocTime = "";
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedDropLocDateTime1)))
                    {
                        x.VendorReachedDropLocDate = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime1).ToString("yyyy-MM-dd");
                        x.VendorReachedDropLocTime = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime1).ToString("HH:mm:ss");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(x.dtVendorReachedDropLocDateTime2))
                        {
                            x.VendorReachedDropLocDate = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime2).ToString("yyyy-MM-dd");
                            x.VendorReachedDropLocTime = Convert.ToDateTime(x.dtVendorReachedDropLocDateTime2).ToString("HH:mm:ss");
                        }
                        else
                        {
                            x.VendorReachedDropLocDate = "";
                            x.VendorReachedDropLocTime = "";
                        }
                    }

                    #region Calculate Distance Between Vendor Location To Incident Location in KM Logic Part

                    // Vendor Latitude and Longitude
                    double VenLat = 0;
                    double VenLon = 0;
                    var regex = new Regex("^[0-9.-]*$");

                    string strVenLat = x.VendorLatitude;
                    string strVenLon = x.VendorLongitude;

                    if (string.IsNullOrEmpty(strVenLat))
                    {
                        VenLat = 0;
                    }
                    else if (!regex.IsMatch(strVenLat))
                    {
                        VenLat = 0;
                    }
                    else
                    {
                        VenLat = Convert.ToDouble(x.VendorLatitude);
                    }

                    if (string.IsNullOrEmpty(strVenLon))
                    {
                        VenLon = 0;
                    }
                    else if (!regex.IsMatch(strVenLon))
                    {
                        VenLon = 0;
                    }
                    else
                    {
                        VenLon = Convert.ToDouble(x.VendorLongitude);
                    }

                    // Assist Latitude and Longitude
                    double AssistLat = 0;
                    double AssistLon = 0;
                    var regexAssist = new Regex("^[0-9.-]*$");

                    string strAssistLat = x.AssistLatitude;
                    string strAssistLon = x.AssistLongitude;

                    if (string.IsNullOrEmpty(strAssistLat))
                    {
                        AssistLat = 0;
                    }
                    else if (!regexAssist.IsMatch(strAssistLat))
                    {
                        AssistLat = 0;
                    }
                    else
                    {
                        AssistLat = Convert.ToDouble(x.AssistLatitude);
                    }

                    if (string.IsNullOrEmpty(strAssistLon))
                    {
                        AssistLon = 0;
                    }
                    else if (!regexAssist.IsMatch(strAssistLon))
                    {
                        AssistLon = 0;
                    }
                    else
                    {
                        AssistLon = Convert.ToDouble(x.AssistLongitude);
                    }

                    double Distance = DistanceInMeter(VenLat, VenLon, AssistLat, AssistLon);
                    double DistanceInKM = Distance / 1000;

                    x.VendorToIncident = Convert.ToString(DistanceInKM);

                    #endregion

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtClosureDateTime)))
                    {
                        x.ClosureDate = Convert.ToDateTime(x.dtClosureDateTime).ToString("yyyy-MM-dd");
                        x.ClosureTime = Convert.ToDateTime(x.dtClosureDateTime).ToString("HH:mm:ss");
                    }
                    else
                    {
                        x.ClosureDate = "";
                        x.ClosureTime = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decTotalKM)))
                    {
                        x.TotalKM = Convert.ToString(x.decTotalKM);
                    }
                    else
                    {
                        x.TotalKM = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decTotalCost)))
                    {
                        x.TotalCost = Convert.ToString(x.decTotalCost);
                    }
                    else
                    {
                        x.TotalCost = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decTollCharges)))
                    {
                        x.TollCharges = Convert.ToString(x.decTollCharges);
                    }
                    else
                    {
                        x.TollCharges = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decWaitingHoursCharge)))
                    {
                        x.WaitingHoursCharge = Convert.ToString(x.decWaitingHoursCharge);
                    }
                    else
                    {
                        x.WaitingHoursCharge = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decVehicleCustodyHoursCharge)))
                    {
                        x.VehicleCustodyHoursCharge = Convert.ToString(x.decVehicleCustodyHoursCharge);
                    }
                    else
                    {
                        x.VehicleCustodyHoursCharge = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decOthersCharge)))
                    {
                        x.OthersCharge = Convert.ToString(x.decOthersCharge);
                    }
                    else
                    {
                        x.OthersCharge = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decCustomerPaidAmount)))
                    {
                        x.CustomerPaidAmount = Convert.ToString(x.decCustomerPaidAmount);
                    }
                    else
                    {
                        x.CustomerPaidAmount = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.decCustomerAmountPaid)))
                    {
                        x.CustomerAmountPaid = Convert.ToString(x.decCustomerAmountPaid);
                    }
                    else
                    {
                        x.CustomerAmountPaid = "";
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtCustomerPaidDate)))
                    {
                        x.CustomerPaidDate = Convert.ToDateTime(x.dtCustomerPaidDate).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        x.CustomerPaidDate = "";
                    }                   

                    #region Calculate Time Difference Between Two Dates Logic Part

                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtClosureDateTime)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtClosureDateTime);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT1 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);                        
                    }
                    else
                    {
                        x.TAT1 = "";
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorAssignedDateTime);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT2 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        x.TAT2 = "";
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtVendorAssignedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.TAT3 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(x.dtVendorAssignedDateTime)) && !string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            DateTime dtDate1 = Convert.ToDateTime(x.dtVendorAssignedDateTime);
                            DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2);

                            TimeSpan TSpanDifference = dtDate2 - dtDate1;
                            decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                            int DiffInMinutes = TSpanDifference.Minutes;
                            int DiffInSeconds = TSpanDifference.Seconds;

                            x.TAT3 = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                        }
                        else
                        {
                            x.TAT3 = "";
                        }
                    }


                    if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(Convert.ToString(x.dtVendorReachedIncidentLocDateTime1)))
                    {
                        DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                        DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime1);

                        TimeSpan TSpanDifference = dtDate2 - dtDate1;
                        decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                        int DiffInMinutes = TSpanDifference.Minutes;
                        int DiffInSeconds = TSpanDifference.Seconds;

                        x.ServiceTime = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(x.dtReportedDateTime)) && !string.IsNullOrEmpty(x.dtVendorReachedIncidentLocDateTime2))
                        {
                            DateTime dtDate1 = Convert.ToDateTime(x.dtReportedDateTime);
                            DateTime dtDate2 = Convert.ToDateTime(x.dtVendorReachedIncidentLocDateTime2);

                            TimeSpan TSpanDifference = dtDate2 - dtDate1;
                            decimal DiffInHours = decimal.Truncate(Convert.ToDecimal(TSpanDifference.TotalHours));
                            int DiffInMinutes = TSpanDifference.Minutes;
                            int DiffInSeconds = TSpanDifference.Seconds;

                            x.ServiceTime = string.Format("{0}:{1}:{2}", DiffInHours, DiffInMinutes, DiffInSeconds);
                        }
                        else
                        {
                            x.ServiceTime = "";
                        }
                    }

                    #endregion


                    if (!string.IsNullOrEmpty(x.Remark))
                    {
                        int intRemark = 0;
                        int.TryParse(x.Remark, out intRemark);

                        string dbRemark = db.tblMstCommonTypes.AsNoTracking().Where(w => w.CommonTypeID == intRemark).Select(s => s.Description).FirstOrDefault();
                        if (!string.IsNullOrEmpty(dbRemark))
                        {
                            x.Remark = dbRemark;
                        }
                        else
                        {
                            x.Remark = "";
                        }                          
                    }
                    else
                    {
                        x.Remark = "";
                    }

                });
            }

            return list;
        }

        public ActionResult DownloadAssistMISReport(string Registration, string RefNo, string FirstName, string LastName, string EngineNo, string ChassisNo, string MobileNo, string CertificateIssueDateFrom, string CertificateIssueDateTo,  int statusId, string EmailID, string OEMId, string CompanyId, string RemarkId)
        {
            DateTime? FromDate = null;
            DateTime? ToDate = null;

            if (!string.IsNullOrEmpty(CertificateIssueDateFrom))
                FromDate = DateTime.ParseExact(CertificateIssueDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (!string.IsNullOrEmpty(CertificateIssueDateTo))
                ToDate = DateTime.ParseExact(CertificateIssueDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            List<AssistanceMISReport> list = new List<AssistanceMISReport>();

            try
            {                            
                list = this.GetSearchAssistMISReport(Registration, RefNo, FirstName, LastName, EngineNo, ChassisNo, MobileNo, CertificateIssueDateFrom, CertificateIssueDateTo, statusId, EmailID, OEMId, CompanyId, RemarkId);

                if (list.Any())
                {
                    MemoryStream _MemoryStream = new MemoryStream();
                    
                    string[] HeaderArray = new string[]
                    {
                        "Agent Name",
                        "Reference No",
                        "Policy Number",
                        "Risk Start Date",
                        "Risk End Date",
                        "RSA Cover",
                        "Car Make and Model",
                        "State",
                        "City",
                        "Vehicle Registration",
                        "First Name",
                        "Middle Name",
                        "Last Name",
                        "Contact No",
                        "Reported Date",
                        "Reported Time",
                        "Closure Date",
                        "Closure Time",
                        "TAT1",
                        "Vendor Assign Date",
                        "Vendor Assign Time",
                        "TAT2",
                        "Vendor Reached in Incident Location Date",
                        "Vendor Reached in Incident Location Time",
                        "TAT3",
                        "Vendor Reached in Drop Location Date",
                        "Vendor Reached in Drop Location Time",
                        "Service Time TAT",
                        "Action",
                        "Status",
                        "Remark",
                        "Company Name",
                        "Vendor Name",
                        "Branch Name",
                        "Incident Location",
                        "Drop Location",
                        "Vendor to Incident",
                        "I to D",
                        "Total Km",
                        "Total Cost",
                        "Toll Charges",
                        "Waiting Hours Charge",
                        "Vehicle Custody Hours Charge",
                        "Other Charges",
                        "Customer Paid Amount",
                        "Customer Amount Paid",
                        "Customer Paid Date",
                        "Reference No",
                        "Select Incident",
                        "Case Summary",
                        "Case Type",
                        "Sub Category"

                    };

                    var listobj = list.Select(x => new
                    {
                        x.AgentName,
                        x.RefNo,
                        x.PolicyNo,
                        x.RiskStartDate,
                        x.RiskEndDate,
                        x.RSACover,
                        x.MakeAndModel,
                        x.State,
                        x.City,
                        x.RegistrationNo,
                        x.FirstName,
                        x.MiddleName,
                        x.LastName,
                        x.ContactNo,
                        x.ReportedDate,
                        x.ReportedTime,
                        x.ClosureDate,
                        x.ClosureTime,
                        x.TAT1,
                        x.VendorAssignedDate,
                        x.VendorAssignedTime,
                        x.TAT2,
                        x.VendorReachedIncidentLocDate,
                        x.VendorReachedIncidentLocTime,
                        x.TAT3,
                        x.VendorReachedDropLocDate,
                        x.VendorReachedDropLocTime,
                        x.ServiceTime,
                        x.Action,
                        x.Status,
                        x.Remark,
                        x.CompanyName,
                        x.VendorName,
                        x.BranchName,                        
                        x.IncidentLocation,
                        x.DropLocation,
                        x.VendorToIncident,
                        x.IToD,
                        x.TotalKM,
                        x.TotalCost,
                        x.TollCharges,
                        x.WaitingHoursCharge,
                        x.VehicleCustodyHoursCharge,
                        x.OthersCharge,
                        x.CustomerPaidAmount,
                        x.CustomerAmountPaid,
                        x.CustomerPaidDate,
                        x.ReferenceNo,
                        x.SelectIncident,
                        x.CaseSummary,
                        x.CaseType,
                        x.SubCategory

                    }).ToList();

                    using (StreamWriter write = new StreamWriter(_MemoryStream))
                    {
                        using (CsvWriter csw = new CsvWriter(write))
                        {
                            foreach (var header in HeaderArray)
                            {
                                csw.WriteField(header);
                            }
                            csw.NextRecord();

                            foreach (var row in listobj)
                            {
                                csw.WriteRecord<dynamic>(row);
                                csw.NextRecord();
                            }
                        }
                    }

                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AssistanceMISReport_" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + ".csv");
                    Response.Charset = "";
                    Response.ContentType = "application/x-msexcel";
                    Response.BinaryWrite(_MemoryStream.ToArray());
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    MemoryStream _MemoryStream = new MemoryStream();

                    string[] HeaderArray = new string[]
                    {
                        "Agent Name",
                        "Reference No",
                        "Policy Number",
                        "Risk Start Date",
                        "Risk End Date",
                        "RSA Cover",
                        "Car Make and Model",
                        "State",
                        "City",
                        "Vehicle Registration",
                        "First Name",
                        "Middle Name",
                        "Last Name",
                        "Contact No",
                        "Reported Date",
                        "Reported Time",
                        "Closure Date",
                        "Closure Time",
                        "TAT1",
                        "Vendor Assign Date",
                        "Vendor Assign Time",
                        "TAT2",
                        "Vendor Reached in Incident Location Date",
                        "Vendor Reached in Incident Location Time",
                        "TAT3",
                        "Vendor Reached in Drop Location Date",
                        "Vendor Reached in Drop Location Time",
                        "Service Time TAT",
                        "Action",
                        "Status",
                        "Remark",
                        "Company Name",
                        "Vendor Name",
                        "Branch Name",
                        "Incident Location",
                        "Drop Location",
                        "Vendor to Incident",
                        "I to D",
                        "Total Km",
                        "Total Cost",
                        "Toll Charges",
                        "Waiting Hours Charge",
                        "Vehicle Custody Hours Charge",
                        "Other Charges",
                        "Customer Paid Amount",
                        "Customer Amount Paid",
                        "Customer Paid Date",
                        "Reference No",
                        "Select Incident",
                        "Case Summary",
                        "Case Type",
                        "Sub Category"

                    };

                    var listobj = list;

                    using (StreamWriter write = new StreamWriter(_MemoryStream))
                    {
                        using (CsvWriter csw = new CsvWriter(write))
                        {
                            foreach (var header in HeaderArray)
                            {
                                csw.WriteField(header);
                            }
                            csw.NextRecord();

                            foreach (var row in listobj)
                            {
                                csw.WriteRecord<dynamic>(row);
                                csw.NextRecord();
                            }
                        }
                    }

                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=AssistanceMISReport_" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + ".csv");
                    Response.Charset = "";
                    Response.ContentType = "application/x-msexcel";
                    Response.BinaryWrite(_MemoryStream.ToArray());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return View();
        }
        #endregion        

        #region Bitly Shorten URL
        public string BitlyShorten(string ServiceURL, string AccessToken, string LongURL, ref string ErrorMessage)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            string post = "{ \"long_url\": \"" + LongURL + "\"}";
            string shortUrl = LongURL;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ServiceURL);
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                request.ServicePoint.Expect100Continue = false;
                request.Method = "POST";
                request.ContentLength = post.Length;
                request.ContentType = "application/json";
                request.Headers.Add("Cache-Control", "no-cache");
                //request.Host = "api-ssl.bitly.com";
                request.Headers.Add("Authorization", "Bearer " + AccessToken);

                using (Stream requestStream = request.GetRequestStream())
                {
                    byte[] postBuffer = Encoding.ASCII.GetBytes(post);
                    requestStream.Write(postBuffer, 0, postBuffer.Length);
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader responseReader = new StreamReader(responseStream))
                        {
                            string json = responseReader.ReadToEnd();
                            shortUrl = Regex.Match(json, @"""link"": ?""(?<link>[^,;]+)""").Groups["link"].Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }

            return shortUrl;
        }
        #endregion

        #region GetSubCategoryByCaseTypeIdForWeb
        public ActionResult GetSubCategoryByCaseTypeIdForWeb(int CaseTypeId)
        {
            try
            {
                List<SelectListItem> subcategoryListItem = new List<SelectListItem>();

                var lstData = (from CTSCT in db.tblAssistCaseTypeSubCaseTypeMapping.AsNoTracking()
                               join MCT in db.tblMstCommonTypes.AsNoTracking() on CTSCT.SubCaseTypeId equals MCT.CommonTypeID into SubCaseTypebased
                               from SubCaseTypebasedObj in SubCaseTypebased.DefaultIfEmpty()
                               where CTSCT.CaseTypeId == CaseTypeId && CTSCT.IsValid != false && SubCaseTypebasedObj.IsValid != false && SubCaseTypebasedObj.MasterType == "AssistanceSubCaseType"
                               select new
                               {
                                   SubCaseTypeID = SubCaseTypebasedObj.CommonTypeID,
                                   SubCaseType = SubCaseTypebasedObj.Description

                               }).OrderBy(o => o.SubCaseTypeID).ToList();

                foreach (var _item in lstData)
                {
                    subcategoryListItem.Add(new SelectListItem { Value = _item.SubCaseTypeID.ToString(), Text = _item.SubCaseType });
                }

                var jsonResult = Json(new
                {
                    subcategorylist = subcategoryListItem
                }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception e)
            {
                HandleException(e);
                return null;
            }
        }
        #endregion
    }

    public class clsReliance
    {
        public string Product { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string VehicleNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public bool? PolicyStatus { get; set; }
    }
}
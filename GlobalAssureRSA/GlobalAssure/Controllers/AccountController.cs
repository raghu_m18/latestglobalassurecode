﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using GlobalAssure.Models;
using System.Web.Security;
using System.Data.Entity;
using System.Configuration;
using GlobalAssure.Entity;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace GlobalAssure.Controllers
{
  /// <summary>
  /// WorkId    Date            Description
  /// 1         01/10/2019      For Validation User in New Admin  
  /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private GlobalAssureRSAEntities db = new GlobalAssureRSAEntities();
        Communication _c = new Communication();

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }


        //  POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            ViewBag.UserType = "";
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            AspNetUser _AspNetUser = db.AspNetUsers.Where(a => a.Email == model.Email).FirstOrDefault();

            if (_AspNetUser != null)
            {
                if (_AspNetUser.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_HERO_ID)
                {
                    ModelState.AddModelError("", "User is linked to Hero. you don't have access this portal. please try to login on HERO RSA PORTAL.");
                    return View(model);
                }

                bool IsLocked = Convert.ToBoolean(_AspNetUser.IsLocked);
                if (IsLocked)
                {
                    return View("Lockout");
                }
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true
                if (IsPasswordUpdateRequiredRepo(model.Password, model.Email))
                {
                    var user = await UserManager.FindByNameAsync(model.Email);
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    return RedirectToAction("ResetPassword", "Account", new { code = code });
                }
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    string LogoFilePathAppSetting = ConfigurationManager.AppSettings["TempLOGO"];
                    AspNetUser _user = db.AspNetUsers.Where(a => a.UserName == model.Email).FirstOrDefault();
                    //
                    string OtherAdminId = "OtherAdminId";
                    //
                    string CookieName = "UserType";
                    string FunctionalityLockCookieName = "FunctionalityLock";
                    string LogoPathCookie = "LogoPathCookie";
                    string BulkCertificationMenuCookieName = "BulkCertificationMenu";
                    string IsTVSTieUpCookie = "IsTVSTieUp";
                    string IsVendorLogin = "IsVendorLogin";
                    string IsAssistLogin = "IsAssistLogin";
                    string TieUpCompany = "TieUpCompany";
                    bool IsAdminUser = false;
                    bool IsTVSTieUp = false;
                    bool IsVendor = false;
                    bool IsAssist = false;
                    //
                    HttpCookie myOtherAdminId = Request.Cookies[OtherAdminId] ?? new HttpCookie(OtherAdminId);
                    //
                    HttpCookie myCookie = Request.Cookies[CookieName] ?? new HttpCookie(CookieName);
                    HttpCookie myFunctionalityLockCookie = Request.Cookies[FunctionalityLockCookieName] ?? new HttpCookie(FunctionalityLockCookieName);
                    HttpCookie myLogoPathCookie = Request.Cookies[LogoPathCookie] ?? new HttpCookie(LogoPathCookie);
                    HttpCookie myBulkCertificationMenuCookie = Request.Cookies[BulkCertificationMenuCookieName] ?? new HttpCookie(BulkCertificationMenuCookieName);
                    HttpCookie myIsTVSTieUpCookie = Request.Cookies[IsTVSTieUpCookie] ?? new HttpCookie(IsTVSTieUpCookie);
                    HttpCookie myIsVendorCookie = Request.Cookies[IsVendorLogin] ?? new HttpCookie(IsVendorLogin);
                    HttpCookie myIsAssistCookie = Request.Cookies[IsAssistLogin] ?? new HttpCookie(IsAssistLogin);
                    HttpCookie myTieUpCompanyCookie = Request.Cookies[TieUpCompany] ?? new HttpCookie(TieUpCompany);
                    int OtherAdmiId = 0;
                    string strOtherAdmin = "Normal";
                    int.TryParse(_user.OtherAdminId,out OtherAdmiId);
                    var ss = db.tblRoleMaster.Where(a =>a.Id == OtherAdmiId).FirstOrDefault();

                    DateTime dtcurrdatetime = DateTime.Now;
                    tblIPAddress adr = new tblIPAddress();
                    if (ModelState.IsValid)
                    {
                        adr.EmailId = model.Email;
                        adr.Created_DateTime = dtcurrdatetime;
                        string strIPAddress = _c.GetIPAddressValue();
                        adr.IPAdress = (!string.IsNullOrEmpty(strIPAddress)) ? strIPAddress : null;
                        adr.Status = true;
                        db.tblIPAddress.Add(adr);
                        db.SaveChanges();
                    }

                    if (ss != null)
                        strOtherAdmin = ss.Name;

                     //sa1
                    Session["UsrId"]= _AspNetUser.Email;
                    Session["UsrType"] = (_user.IsAdmin == true) ? "Admin" : strOtherAdmin;
                    //ea1
                    if (_user.IsAdmin != null)
                    {
                        IsAdminUser = Convert.ToBoolean(_user.IsAdmin);
                    }

                    if (_user.TieUpCompanyID == CrossCutting_Constant.TieUpCompany_TVS_ID)
                    {
                        IsTVSTieUp = true;
                    }
                    if (_user.TieUpCompanyID == CrossCutting_Constant.TieUpWithVendor)
                    {
                        IsVendor = true;
                    }
                    if (_user.TieUpCompanyID == CrossCutting_Constant.TieUpAssistance || _user.TieUpCompanyID == CrossCutting_Constant.TieUpRelianceAssist)
                    {
                        IsAssist = true;
                    }

                    myLogoPathCookie.Values["LogoPath"] = "";

                    if (!string.IsNullOrEmpty(_user.LogoPath))
                    {
                        string FileName = _user.LogoPath.Substring(_user.LogoPath.LastIndexOf('\\') + 1);
                        string FilePath = LogoFilePathAppSetting + "/" + FileName;
                        FilePath = FilePath.Replace("\\", "/");
                        FilePath = FilePath.Replace("~", "");
                        myLogoPathCookie.Values["LogoPath"] = FilePath;
                    }

                    myFunctionalityLockCookie.Values["IsFunctionalityLocked"] = "False";
                    myTieUpCompanyCookie.Values["TieUpCompany"] = Convert.ToString(_user.TieUpCompanyID);

                    if (_user.IsFunctionalityLocked != null)
                    {
                        myFunctionalityLockCookie.Values["IsFunctionalityLocked"] = (Convert.ToBoolean(_user.IsFunctionalityLocked) == true ? "True" : "False");
                    }

                    //
                    if (!string.IsNullOrEmpty(_user.OtherAdminId))
                    {
                        myOtherAdminId.Values["OthAdminId"] = _user.OtherAdminId;
                    }
                    else
                    {
                        myOtherAdminId.Values["OthAdminId"] = _user.OtherAdminId;
                    }                    
                    //

                    if (IsAdminUser)
                    {
                        myCookie.Values["User"] = "Admin";
                    }
                    else
                    {
                        myCookie.Values["User"] = "Normal";
                    }

                    if (IsTVSTieUp)
                    {
                        myIsTVSTieUpCookie.Values["IsTVSTieUp"] = "True";
                    }
                    else
                    {
                        myIsTVSTieUpCookie.Values["IsTVSTieUp"] = "False";
                    }

                    if (IsVendor)
                    {
                        myIsVendorCookie.Values["IsVendorLogin"] = "True";
                    }
                    else
                    {
                        myIsVendorCookie.Values["IsVendorLogin"] = "False";
                    }

                    if (IsAssist)
                    {
                        myIsAssistCookie.Values["IsAssistLogin"] = "True";
                    }
                    else
                    {
                        myIsAssistCookie.Values["IsAssistLogin"] = "False";
                    }

                    myBulkCertificationMenuCookie.Values["IsBulkCertificationMenu"] = "False";

                    if (Convert.ToBoolean(_user.IsBulkCertificationMenu))
                    {
                        myBulkCertificationMenuCookie.Values["IsBulkCertificationMenu"] = "True";
                    }

                    //
                    myOtherAdminId.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myOtherAdminId);
                    //

                    myCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myCookie);

                    myFunctionalityLockCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myFunctionalityLockCookie);

                    myLogoPathCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myLogoPathCookie);

                    myBulkCertificationMenuCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myBulkCertificationMenuCookie);

                    myIsTVSTieUpCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myIsTVSTieUpCookie);

                    myIsVendorCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myIsVendorCookie);

                    myIsAssistCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myIsAssistCookie);

                    myTieUpCompanyCookie.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(myTieUpCompanyCookie);

                    //
                    StringBuilder Sb = new StringBuilder();

                    int RoleId = 0;
                    int.TryParse(_user.OtherAdminId, out RoleId);

                    DataSet dsMenuData = new DataSet();

                    SqlConnection objsqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                    string qry = "select mm.Id,mm.Menu,mm.MenuURL,mm.Description,mm.OrderNo,mm.ParentId from tblRoleMenuMappingMaster rmm " +
                        "inner join tblMenuMaster mm on rmm.MenuId = mm.Id where rmm.RoleId = '"+ RoleId + "' and mm.IsValid = 1";

                    SqlCommand cmd = new SqlCommand(qry, objsqlCon);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dsMenuData);                                    

                    if (dsMenuData.Tables[0].Rows.Count > 0)
                    {
                        DataView dvMenu = dsMenuData.Tables[0].DefaultView;
                        dvMenu.RowFilter = "ParentId = 0";
                        dvMenu.Sort = "OrderNo ASC";

                        DataView dvSubMenu = dvMenu;

                        Sb.Append("<ul class=\"navbar-nav mx-auto\">");

                        foreach (DataRowView Menu in dvMenu)
                        {
                            DataRow drMenu = Menu.Row;

                            dvSubMenu.RowFilter = "ParentId = '" + drMenu["Id"] + "'";
                            dvSubMenu.Sort = "OrderNo ASC";

                            if (dvSubMenu.Count > 0)
                            {
                                Sb.Append("<li class=\"nav-item dropdown\"><a class=\"nav-link  dropdown-toggle\" href=\"" + drMenu["MenuURL"] + "\" data-toggle=\"dropdown\">" + drMenu["Menu"] + "</a>");
                                Sb.Append("<ul class=\"dropdown-menu\" style=\"font-size: 14px\">");                            
                            }
                            else
                            {
                                Sb.Append("<li class=\"nav-item \"><a class=\"nav-link\" href=\"" + drMenu["MenuURL"] + "\">" + drMenu["Menu"] + "</a></li>");                               
                            }

                            foreach (DataRowView SubMenu in dvSubMenu)
                            {
                                DataRow drSubMenu = SubMenu.Row;

                                int dvSubMenuCount = dvSubMenu.Count;

                                Sb.Append("<li><a class=\"dropdown-item\" href=\"" + drSubMenu["MenuURL"] + "\">" + drSubMenu["Menu"] + "</a></li>");
                            }

                            if (dvSubMenu.Count > 0)
                            {
                                Sb.Append("</ul>");
                                Sb.Append("</li>");
                            }
                        }

                        Sb.Append("<li class=\"nav-item \"><a class=\"nav-link\" href=\"javascript:document.getElementById('logoutForm').submit()\">Log off</a></li>");
                        Sb.Append("</ul>");

                        Session["Menus"] = Sb.ToString();
                    }
                    else
                    {                        
                        Sb.Append("<ul class=\"navbar-nav mx-auto\">");                        
                        Sb.Append("<li class=\"nav-item active\"><a class=\"nav-link\" href=\"javascript:document.getElementById('logoutForm').submit()\">Log off</a></li>");
                        Sb.Append("</ul>");

                        Session["Menus"] = Sb.ToString();
                    }
                    //

                    return RedirectToLocal(returnUrl);
                //case SignInStatus.LockedOut:
                //    return View("Lockout");
                //case SignInStatus.RequiresVerification:
                //    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                //case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");

                    DateTime dttCurrDateTime = DateTime.Now;
                    tblIPAddress adre = new tblIPAddress();
                    if (!string.IsNullOrEmpty(model.Email))
                    {
                        adre.EmailId = model.Email;
                        adre.Created_DateTime = dttCurrDateTime;
                        string strIPAddress = _c.GetIPAddressValue();
                        adre.IPAdress = (!string.IsNullOrEmpty(strIPAddress)) ? strIPAddress : null;
                        adre.Status = false;
                        db.tblIPAddress.Add(adre);
                        db.SaveChanges();
                    }

                    return View(model);
            }
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            Boolean validated = true;
        //            validated = ValidateUser(model.Email, model.Password);
        //            if (validated)
        //            {
        //                bool? isAdmin = db.AspNetUsers.Where(a => a.UserName == model.Email).Select(a => a.IsAdmin).FirstOrDefault();
        //                if ((bool)isAdmin)
        //                {
        //                    ViewBag.UserType = "Admin";
        //                }
        //                else
        //                {
        //                    ViewBag.UserType = "Normal";
        //                }

        //                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, model.Email, DateTime.Now, DateTime.Now.AddMinutes(1), true, model.Email, FormsAuthentication.FormsCookiePath);
        //                string ent = FormsAuthentication.Encrypt(ticket);
        //                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ent);
        //                cookie.HttpOnly = true;
        //                Response.Cookies.Add(cookie);
        //                FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe);
        //                return Redirect("/Home/Index");

        //            }
        //            else
        //            {
        //                ViewBag.Message = "Invalid Username or Password";
        //                return View(model);
        //            }
        //        }
        //        return View(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        //CommonException.HandleException(ex, CrossCutting_Constants.UIPolicy);
        //        return View(model);
        //    }
        //}


        public bool ValidateUser(string username, string password)
        {
            try
            {
                var membership = System.Web.Security.Membership.Providers["BAXAProvider"];
                var result = membership.ValidateUser(username, password);
                return result;
            }
            catch (Exception)
            {
                //auth_log = log4net.LogManager.GetLogger("Auth");
                //auth_log.Error("Login Failed for non AD user : " + username + ":" + ex.Message);
                //auth_log.Error("Login Failed for non AD user : " + ex.StackTrace);
                return false;
            }
        }




        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = await UserManager.FindByNameAsync(model.Email);
        //        if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
        //        {
        //            // Don't reveal that the user does not exist or is not confirmed
        //            string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
        //            var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //            await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
        //            return RedirectToAction("ForgotPasswordConfirmation", "Account");
        //            //    return View("ForgotPasswordConfirmation");
        //        }

        //        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
        //        // Send an email with this link
        //        // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
        //        // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
        //        // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
        //        // return RedirectToAction("ForgotPasswordConfirmation", "Account");
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                this.ModelState.Keys.Where(k => k.Contains("Password")).ToList().ForEach(k => this.ModelState.Remove(k));

                if (ModelState.IsValid)
                {
                    var user = await UserManager.FindByNameAsync(model.Email);
                    if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                    {
                        ModelState.AddModelError("modelError", "Given Credential does not exist, please try again with different credential");
                        return View(model);
                    }
                    else
                    {
                        string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        Communication _c = new Communication();
                        _c.SendingMail(model.Email, model.Email, null, "ForgotPassword", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                        return RedirectToAction("ForgotPasswordConfirmation", "Account");
                    }
                }
                else
                    return View(model);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return View(model);
            }
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                tblPasswordHistory _pass = new tblPasswordHistory();
                _pass.Password = model.Password;
                _pass.UserId = GetUserIDbyUserName(model.Email);
                _pass.Date = DateTime.Now;
                db.tblPasswordHistories.Add(_pass);
                db.SaveChanges();
                return RedirectToAction("ResetPasswordConfirmation", "Account");

            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }


        public bool IsPasswordUpdateRequiredRepo(string password, string userName)
        {
            string userID = GetUserIDbyUserName(userName);

            if (string.IsNullOrEmpty(userID))
            {
                return false;
            }

            DateTime threeMonthLaterDate = DateTime.Now.AddDays(-100).Date;
            var lastpasswordrecord = db.tblPasswordHistories.Where(x => x.UserId == userID).OrderByDescending(o => o.Id).FirstOrDefault();

            if (lastpasswordrecord == null)
            {
                return true;
            }

            if (password != lastpasswordrecord.Password)
            {
                return false;
            }

            if (Convert.ToDateTime(lastpasswordrecord.Date).Date < threeMonthLaterDate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetUserIDbyUserName(string userName)
        {
            return db.AspNetUsers.Where(a => a.UserName == userName).Select(a => a.Id).FirstOrDefault();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home", new { Usertype = ViewBag.UserType });
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        public static bool HandleException(Exception ex)
        {
            bool rethrow = false;
            rethrow = UIExceptionHandler.HandleException(ref ex);
            if (rethrow)
            {
                throw ex;
            }
            return false;
        }

        #endregion

        #region ResetPassword
        public ActionResult ResetUserPassword()
        {            
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetUserPassword(ResetUserPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                //|| !(await UserManager.IsEmailConfirmedAsync(user.Id))
                ModelState.AddModelError("modelError", "Given Credential does not exist, please try again with different credential");
                return View(model);
            }
            else
            {
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                ViewBag.Callback = callbackUrl;
                ViewBag.Msg = "Generate Successfully!";
            }
            
            return View();
        }
        #endregion
    }
}
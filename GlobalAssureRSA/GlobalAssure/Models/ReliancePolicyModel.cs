﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalAssure.Models
{
    public class ReliancePolicyModel
    {
        public string PolicyNumber { get; set; }
        public string PolicyStatus { get; set; }
        public string RiskStartDate { get; set; }
        public string RiskEndDate { get; set; }
        public string ProductCode { get; set; }
        public string InsuredName { get; set; }
        public string VehicleNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChasisNo { get; set; }
        public string RSAVendorName { get; set; }
        public string ResponseStatus { get; set; }
        public string InsuredAddressLine { get; set; }
        public string InsuredStateName { get; set; }
        public string InsuredDistrictName { get; set; }
        public string InsuredCityName { get; set; }
        public string InsuredphoneNumber { get; set; }
        public string InsuredMobileNumber { get; set; }
        public string InsuredEmailID { get; set; }        
    }

    public class RelianceAlternatePolicyNoModel
    {
        public string PolicyNo { get; set; }
        public string AlternatePolicyNo { get; set; }
        public string Remarks { get; set; }
    }
}
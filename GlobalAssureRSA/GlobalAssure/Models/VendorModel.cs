﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalAssure.Models
{
    public class VendorModel
    {
        public VendorModel()
        {
            CityListItem = new Dictionary<decimal, string>();
            StateListItem = new Dictionary<decimal, string>();
            lstActive = new Dictionary<decimal, string>();
            lstActive.Add(1, "Active");
            lstActive.Add(0, "Inactive");
        }

        public decimal VendorID { get; set; }
        public string VendorName { get; set; }
        public Nullable<System.DateTime> VendorDOB { get; set; }
        public string VendorEMail { get; set; }
        public string VendorMobileNo { get; set; }
        public string VendorPANCard { get; set; }
        public string VendorAadhaarCard { get; set; }
        public Nullable<int> VendorStateID { get; set; }
        public string VendorState { get; set; }
        public Dictionary<decimal, string> StateListItem { get; set; }
        public Nullable<int> VendorCityID { get; set; }     
        public Dictionary<decimal, string> CityListItem { get; set; }
        public string VendorCity { get; set; }
        public string VendorPincode { get; set; }
        public string VendorLandMark { get; set; }
        public string VendorAddress1 { get; set; }
        public string VendorAddress2 { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string CompanyPANCard { get; set; }
        public string CompanyGSTIN { get; set; }
        public string CompanyEMail { get; set; }
        public string CompanyPhoneNo { get; set; }
        public string CompanyFAX { get; set; }
        public string CompanyTIN { get; set; }
        public string CompanyTAN { get; set; }
        public string CompanyRegisterdOffice { get; set; }
        public string CompanyHeadOffice { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<bool> IsVlid { get; set; }
        public Nullable<int> isActive { get; set; }
        public Dictionary<decimal, string> lstActive { get; set; }
    }

    public class BranchDetails
    {
        public BranchDetails()
        {
            StateListItem = new Dictionary<decimal, string>();
            CityListItem = new Dictionary<decimal, string>();
            lstActive = new Dictionary<decimal, string>();
            lstActive.Add(1, "Active");
            lstActive.Add(0, "Inactive");

        }
        public decimal BranchID { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string BranchName { get; set; }
        public string VendorUserName { get; set; }
        public string CompanyName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public Nullable<decimal> StateID { get; set; }
        public string State { get; set; }
        public Dictionary<decimal, string> StateListItem { get; set; }
        public Nullable<decimal> CityID { get; set; }
        public string City { get; set; }
        public Dictionary<decimal, string> CityListItem { get; set; }
        public string Geo_autocomplete { get; set; }
        public string Pincode { get; set; }
        public string LandMark { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Latitudes { get; set; }
        public string Longitudes { get; set; }
        public Nullable<bool> Status { get; set; }
        public string userid { get; set; }
        public bool isValid { get; set; }
        public Nullable<int> isActive { get; set; }
        public Dictionary<decimal, string> lstActive { get; set; }
    }

    public class VendorBranchModel
    {
        public decimal VendorID { get; set; }
        public string VendorName { get; set; }
        public Nullable<System.DateTime> VendorDOB { get; set; }
        public string VendorEMail { get; set; }
        public string VendorMobileNo { get; set; }
        public string VendorPANCard { get; set; }
        public string VendorAadhaarCard { get; set; }
        public Nullable<int> VendorStateID { get; set; }
        public string VendorState { get; set; }
        public Dictionary<decimal, string> StateListItem { get; set; }
        public Nullable<int> VendorCityID { get; set; }
        public Dictionary<decimal, string> CityListItem { get; set; }
        public string VendorCity { get; set; }
        public string VendorPincode { get; set; }
        public string VendorLandMark { get; set; }
        public string VendorAddress1 { get; set; }
        public string VendorAddress2 { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string CompanyPANCard { get; set; }
        public string CompanyGSTIN { get; set; }
        public string CompanyEMail { get; set; }
        public string CompanyPhoneNo { get; set; }
        public string CompanyFAX { get; set; }
        public string CompanyTIN { get; set; }
        public string CompanyTAN { get; set; }
        public string CompanyRegisterdOffice { get; set; }
        public string CompanyHeadOffice { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<bool> IsVlid { get; set; }
        public Nullable<int> isActive { get; set; }
        public Dictionary<decimal, string> lstActive { get; set; }
       
        public decimal? BranchID { get; set; }
       // public string VendorID { get; set; }
       public string BranchVendorName { get; set; }
        public string BranchName { get; set; }

        public string  BranchState { get; set; }
        public string BranchCity { get; set; }
        public string VendorUserName { get; set; }

        public string VendorPassword { get; set; }       

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }      
        public string GeoLocation { get; set; }
        public string Pincode { get; set; }
        public string LandMark { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Locality { get; set; }
        public string Latitudes { get; set; }
        public string Longitudes { get; set; }
        public Nullable<bool> BranchStatus { get; set; }
        public string userid { get; set; }
        public bool isValid { get; set; }

        //
        public string ACCOUNTNO  { get; set; }
        public string IFSC_CODE { get; set; }
        public string BENEFICIARY_NAME { get; set; }

        public string BANKBRANCHNAME { get; set; }

        public string GSTNO { get; set; }

        public string ISAGREEMENTRECEIVEDWITHKYC { get; set; }

        public string CANCELLEDCHEQUERECIVED { get; set; }

        public string VENDOR_CODE { get; set; }


        //
        public string Vendor_Category { get; set; }
        public string Service_Type { get; set; }
        public decimal?  Base_Price { get; set; }
        public string KiloMeters { get; set; }
        public decimal? Per_km_Price { get; set; }
        public string Service_Time { get; set; }

        //

        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
        // public Nullable<int> isActive { get; set; }
        // public Dictionary<decimal, string> lstActive { get; set; }

    }


}
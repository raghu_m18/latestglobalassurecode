﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalAssure.Models
{
    public class QRCodeModel
    {
        public string QRCodeText { get; set; }
        public string QRCodeImagePath { get; set; }

    }
}
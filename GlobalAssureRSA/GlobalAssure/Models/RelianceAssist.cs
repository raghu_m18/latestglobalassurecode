﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalAssure.Models
{
    public class RelianceAssist
    {
        public string PolicyNumber { get; set; }

        public string PolicyStatus { get; set; }

        public string RiskStartDate { get; set; }

        public string RiskEndDate { get; set; }

        public string ProductCode { get; set; }

        public string InsuredName { get; set; }

        public string VehicleNo { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string EngineNo { get; set; }

        public string ChasisNo { get; set; }

        public string RSAVendorName { get; set; }

        public string ResponseStatus { get; set; }

        public string InsuredAddressLine { get; set; }

        public string InsuredStateName { get; set; }

        public string InsuredDistrictName { get; set; }

        public string InsuredCityName { get; set; }

        public string InsuredphoneNumber { get; set; }

        public string InsuredMobileNumber { get; set; }

        public string InsuredEmailID { get; set; }

        public string VehicleColor { get; set; }

        public string VehicleYear { get; set; }

        public string Incident { get; set; }

        public string Location { get; set; }

        public string AdditionalDetail { get; set; }

        public string LandMark { get; set; }

        public string Pincode { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Vendor { get; set; }

        public string Token { get; set; }

        public string UserId { get; set; }

        public int IncidentTypeId { get; set; }

        public string IncidentDetail { get; set; }

        public List<SelectListItem> lstIncidentType { get; set; }

        public List<SelectListItem> lstIncidentDetails { get; set; }

        public string RegistrationNo { get; set; }
    }
}
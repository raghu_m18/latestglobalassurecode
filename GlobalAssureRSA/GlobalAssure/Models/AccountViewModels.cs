﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GlobalAssure.Models
{
    /// <summary>
    /// IndexId     Date            Developer       Desc
    /// 1001        02/09/2019      Fardeen         MIS Report & Policy Copy Field Added   
    /// 1002        10/09/2019      Bikash          NIA Validation    
    /// 1003        04/10/2019      Bikash          Customer Last Name Added
    /// 1004        19/11/2019      Hassan          Show Insured Full Name instead of Branch Code in Certificate   
    /// 1005        10/12/2019      Hassan          AssistanceModel New Property Added
    /// </summary>

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public string Name { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }

    }

    public class ResetUserPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //[Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }

        public string Code { get; set; }

        public string CallbackUrl { get; set; }

    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class TransactionModel
    {
        public TransactionModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        [Required]
        [CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //
        public int? InsuredAge { get; set; }
        public int? NomineeAge { get; set; }

        public int IsDownloadCertificate { get; set; }

    }

    public class TransactionHCModel
    {
        public TransactionHCModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //
            HospiCashTypeListItem = new Dictionary<int, string>();
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        [Required]
        [CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //
        public int? InsuredAge { get; set; }
        public int? NomineeAge { get; set; }

        public Nullable<int> HospiCashTypeID { get; set; }
        public Dictionary<int, string> HospiCashTypeListItem { get; set; }
        public string NomineeTitle { get; set; }
        public string Pincode { get; set; }

        public int IsDownloadCertificate { get; set; }
    }

    public class TransModel
    {
        public TransModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public decimal? hdnProductID { get; set; }
        public decimal? hdnStateID { get; set; }
        public decimal? hdnCityID { get; set; }
    }

    public class AssistanceDataEntryModel
    {
        // sa 1005
        public AssistanceDataEntryModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CompanyListItem = new Dictionary<int, string>();
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerContact { get; set; }
        public string CustomerEmail { get; set; }
        public string CardLast4DigitFetched { get; set; }
        public string MobileNoLast4DigitFetched { get; set; }
        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string RegistrationNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        // ea 1005

        //// Table Property End ////   
        ///
        public string CompanyID { get; set; }
        public Dictionary<int, string> CompanyListItem { get; set; }
        public string CompanyDetails { get; set; }
        public int CompID { get; set; }
        public string hdnOEMID { get; set; }
        public string OEMID { get; set; }
        public Dictionary<int, string> OEMListItem { get; set; }
        public string OEMDetails { get; set; }
    }

    public class SearchCompanyDataForAssistanceModel
    {
        public string Product { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string VehicleNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string PolicyStatus { get; set; }
        public string CardLast4Digit { get; set; }
        public string MobileNoLast4Digit { get; set; }
    }

    public class PaymentSearchModel
    {
        public PaymentSearchModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            PaymentDetailModel = new PaymentDetailModel();
        }
        public Nullable<decimal> CertificateNo { get; set; }
        public Nullable<DateTime> CertificateIssueDateFrom { get; set; }
        public Nullable<DateTime> CertificateIssueDateTo { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public PaymentDetailModel PaymentDetailModel { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
    }

    public class PaymentDetailModel
    {
        public PaymentDetailModel()
        {
            PaymentTransactionModelList = new List<PaymentTransactionModel>();
            BankListItem = new Dictionary<decimal, string>();
            //
            PaymentListItem = new Dictionary<decimal, string>();
            //
        }

        public string Remark { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<DateTime> _ChequeDate { get; set; }
        public Nullable<DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public int PaymentTypeID { get; set; }
        public Nullable<decimal> TotalPlanAmount { get; set; }
        public Nullable<decimal> Total_CD_Balance { get; set; }
        //
        public int PaymentMethodID { get; set; }
        public string MobileNo { get; set; }
        //

        public Dictionary<decimal, string> BankListItem { get; set; }
        public List<PaymentTransactionModel> PaymentTransactionModelList { get; set; }
        //
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        //
    }

    public class PaymentTransactionModel
    {
        public decimal TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string ProductType { get; set; }
        public string Plan { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public decimal? Amount { get; set; }
        public bool IsPayment { get; set; }
        public string Status { get; set; }
        public string ChequeType { get; set; }
        public string PaymentMode { get; set; }
        public string PrintDownload { get; set; }
        public string CustomerFirstName { get; set; }

        public string CustomerMiddleName { get; set; }
        public string CustomerLastName { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? CertificateStartDate { get; set; }
        public string MMddyyyyCertificateStartDateString { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChequeNo { get; set; }
        public string ChallanNo { get; set; }
        public DateTime? ChallanCreatedDate { get; set; }
        public string ChallanCreatedDateString { get; set; }
        public bool SelectAll { get; set; }
        public string CertificatePath { get; set; }
        public decimal? PaymentTypeId { get; set; }

        //
        public string AlternatePolicyNo { get; set; }
        public string PrintDownload1 { get; set; }
        //
    }

    public class PlansSearchModel
    {
        public PlansSearchModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();
        }
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
    }

    public class PlansDetailModel
    {
        public PlansDetailModel()
        {
            PlansDetailModelList = new List<PlanModel>();
        }

        public List<PlanModel> PlansDetailModelList { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public bool IsSelectAllCity { get; set; }
    }

    public class PlansInActiveDetailModel
    {
        public PlansInActiveDetailModel()
        {
            PlansInActiveDetailModelList = new List<PlanInActiveModel>();
        }

        public List<PlanInActiveModel> PlansInActiveDetailModelList { get; set; }
    }

    public class DashboardModel
    {
        public DashboardModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
        }
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public string PendingPaymentCertificateCount { get; set; }
        public string PendingPaymentTotalAmount { get; set; }
        public string TodayPaymentReceivedCertificateCount { get; set; }
        public string TodayPaymentReceivedTotalAmount { get; set; }
        public string CertificateIssuedCount { get; set; }
        public string CertificateIssuedTotalAmount { get; set; }
    }

    public class PieChartModel
    {
        public string ProductName { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class BusinessMISSearchModel
    {
        public BusinessMISSearchModel()
        {
            BusinessMISModel = new BusinessMISModel();
        }

        public BusinessMISModel BusinessMISModel { get; set; }
    }

    public class BusinessMISModel
    {
        public BusinessMISModel()
        {
            BusinessMISModelList = new List<BusinessMISDataModel>();
        }

        public List<BusinessMISDataModel> BusinessMISModelList { get; set; }
    }

    public class BusinessMISDataModel
    {
        public string AgentName { get; set; }
        public string AgentEmail { get; set; }
        public string PolicyCountNew { get; set; }
        public string PolicyAmountNew { get; set; }
        public string PolicyCountOld { get; set; }
        public string PolicyAmountOld { get; set; }
        public string TotalPolicyCount { get; set; }
        public string TotalPolicyAmount { get; set; }

        public string SumPolicyCountNew { get; set; }
        public string SumPolicyAmountNew { get; set; }
        public string SumPolicyCountOld { get; set; }
        public string SumPolicyAmountOld { get; set; }
        public string SumTotalPolicyCount { get; set; }
        public string SumTotalPolicyAmount { get; set; }
    }

    public class PaymentOutStandingMISSearchModel
    {
        public PaymentOutStandingMISSearchModel()
        {
            PaymentOutStandingMISModel = new PaymentOutStandingMISModel();
        }

        public PaymentOutStandingMISModel PaymentOutStandingMISModel { get; set; }
    }

    public class PaymentOutStandingMISModel
    {
        public PaymentOutStandingMISModel()
        {
            PaymentOutStandingMISModelList = new List<PaymentOutStandingMISDataModel>();
        }

        public List<PaymentOutStandingMISDataModel> PaymentOutStandingMISModelList { get; set; }
    }

    public class PaymentOutStandingMISDataModel
    {
        public string AgentName { get; set; }
        public string AgentEmail { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonContactNo { get; set; }
        public string FifteenDays { get; set; }
        public string ThirtyDays { get; set; }
        public string SixtyDays { get; set; }
        public string MoreThanSixtyDays { get; set; }
        public string TotalAmount { get; set; }

        public string SumFifteenDays { get; set; }
        public string SumThirtyDays { get; set; }
        public string SumSixtyDays { get; set; }
        public string SumMoreThanSixtyDays { get; set; }
        public string SumTotalAmount { get; set; }
    }

    public class vPayWebInitiateTransactionResponse
    {
        public vPayWebInitiateTransactionResponse()
        {
            request = new request();
            payment = new payment();
        }

        public string version { get; set; }
        public request request { get; set; }
        public payment payment { get; set; }
        public string checksum { get; set; }
    }
    public class request
    {
        public string status { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string timestamp { get; set; }
    }
    public class payment
    {
        public string billno { get; set; }
        public string amount { get; set; }
        public string transaction_id { get; set; }
        public string transaction_url { get; set; }
    }

    public class OnlinePaymentResponseModel
    {
        public string Response { get; set; }
        public string Remark { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string AcknowledgementNo { get; set; }
        public string Status { get; set; }
        public string IMDCode { get; set; }
        public string CertificateNo { get; set; }
        public string CertificatePath { get; set; }
    }


    public class vPayOnlinePaymentResponseModel
    {
        public vPayOnlinePaymentResponseModel()
        {
            _vPayWebInitiateTransactionResponse = new vPayWebInitiateTransactionResponse();
        }

        public bool Flag { get; set; }
        public string Response { get; set; }
        public string vPayResponseCode { get; set; }
        public string vPayResponseCodeDescription { get; set; }
        public string vPayTransactionURL { get; set; }
        public string PaymentReferenceNo { get; set; }
        public vPayWebInitiateTransactionResponse _vPayWebInitiateTransactionResponse { get; set; }
    }

    public class vPayCallBackModel
    {
        public string Status { get; set; }
        public string transaction_id { get; set; }
        public string Amount { get; set; }
        public string Checksum { get; set; }
        public string udf1 { get; set; }
        public string udf2 { get; set; }
        public string udf3 { get; set; }
        public string udf4 { get; set; }
        public string udf5 { get; set; }
        public string paymentmode { get; set; }
        public string billno { get; set; }
        public string CertificatePath { get; set; }
    }

    public class OnlinePaymentModel
    {
        public OnlinePaymentModel()
        {
            TransactionIDs = new List<decimal>();
            _vPayOnlinePaymentModel = new vPayOnlinePaymentModel();
        }

        public decimal OnlinePaymentID { get; set; }
        public bool IsSingleSignOn { get; set; }
        public decimal Amount { get; set; }
        public string IMDCode { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ProductCode { get; set; }
        public string CustomerName { get; set; }
        public string PayerType { get; set; }
        public string ResponseURL { get; set; }
        public string CreatedBy { get; set; }
        public List<decimal> TransactionIDs { get; set; }
        public vPayOnlinePaymentModel _vPayOnlinePaymentModel { get; set; }
    }


    public class vPayOnlinePaymentModel
    {
        public string datetime { get; set; }
        public string partnerid { get; set; }
        public string billno { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string amount { get; set; }
        public string paymentmode { get; set; }
        public string description { get; set; }
        public string allowedmodes { get; set; }
        public string allowedwallets { get; set; }
        public string sendlink { get; set; }
        public string expiry { get; set; }
        public string checksum { get; set; }
        public string udf1 { get; set; }
        public string udf2 { get; set; }
        public string udf3 { get; set; }
        public string udf4 { get; set; }
        public string udf5 { get; set; }
        public string response_url { get; set; }
    }


    public class AccountAccessControl
    {
        public string UserName { get; set; }
        public string Status { get; set; }

        public bool IsLocked { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Remark { get; set; }

    }

    public class GenerateCertificateModel
    {
        public GenerateCertificateModel()
        {
            PlanFeatureDetailModelList = new List<Models.PlanFeatureDetailModel>();
        }

        public string InsuredAddressLine1 { get; set; }
        public string InvoiceDate { get; set; }
        public string Colour { get; set; }
        public string CertificateNo { get; set; }
        public string CoverStartDate { get; set; }
        public string CoverEndDate { get; set; }
        public string RegistrationNo { get; set; }
        public string ChassisNo { get; set; }
        public string EngineNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DealerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string Amount { get; set; }
        public string GSTAmount { get; set; }
        public string TotalAmount { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string Make { get; set; }
        public string ContactNo { get; set; }
        public string CurrentDateTime { get; set; }
        public string DealershipStatmAndSign { get; set; }
        public string Comma { get; set; }
        public string IsPlanFeature { get; set; }
        public string AmountInWords { get; set; }
        public string IGST { get; set; }
        public string CGST { get; set; }
        public string SGST { get; set; }
        public string IsStateUP { get; set; }
        public string GSTNoOfTheServiceRecipient { get; set; }
        public string PlaceOfSupply { get; set; }
        public string SACCode { get; set; }
        public string CertificateTemplateName { get; set; }
        public string PaymentStatus { get; set; }
        //sa 1001        

        public string NomineeGender { get; set; }

        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea 1001

        //sa 1004
        public string InsuredName { get; set; }
        //ea 1004

        public string GstNo { get; set; }

        public List<PlanFeatureDetailModel> PlanFeatureDetailModelList { get; set; }

        //Covid
        public string CrisisAssistNo { get; set; }
        public string InsurnceConfirmationNo { get; set; }
        public string SumInsured { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public string PreExistingDisease { get; set; }

        public string PolicyholderName { get; set; }
        public string IssueDate { get; set; }
        public string PolicyholderFullAddress { get; set; }
        public string Email { get; set; }
        public string DiseaseName { get; set; }
        public string InsuredDOB { get; set; }
        public string InsuredGender { get; set; }
        public string NomineeName { get; set; }
        public string NomineeRelationship { get; set; }
        public string NomineeDOB { get; set; }
        public string IssuedDateTime { get; set; }
        public string NomineeFullAddress { get; set; }
        //

        //
        public string PlanName { get; set; }
        //

        // TimeStamp
        public string CreatedDateTimeRSA { get; set; }
        public string CreatedDateTimePA { get; set; }
        //

        public string MasterPolicyNo { get; set; }

        public string InsuredAge { get; set; }
        public string IsInsuredDOB { get; set; }

        //
        public string LoanAmount { get; set; }
        public string EMIAmount { get; set; }
        public string EMITenure { get; set; }
        public string LoanAccountNo { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string VehicleClass { get; set; }
        public string Pincode { get; set; }
        //
    }

    public class PlanFeatureDetailModel
    {
        public string Feature { get; set; }
        public string Detail { get; set; }
    }

    public class GeneratePaySlipModel
    {
        public GeneratePaySlipModel()
        {
            PaySlipDetailModelList = new List<PaySlipDetailModel>();
        }

        public string DepositChallanNo { get; set; }
        public string BankAccountNo { get; set; }
        public string BankName { get; set; }
        public string PickUpPoint { get; set; }
        public string CurrentDate { get; set; }
        public string GrossDepositAmount { get; set; }
        public string NoOfInstruments { get; set; }

        public string UniqueChequeGrossDepositeAmount { get; set; }
        public string UniqueChequeNoOfInstruments { get; set; }

        public List<PaySlipDetailModel> PaySlipDetailModelList { get; set; }
    }

    public class PaySlipDetailModel
    {
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string Bank { get; set; }
        public string Branch { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string ChequeAmount { get; set; }
        public string Remark { get; set; }
    }

    public class UserListforFunctionalityUnlock
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string DealerName { get; set; }
        public string PickUpPoint { get; set; }
        public string ContactNo { get; set; }
    }

    public class PendingTransactionFunctionalityUnlockModel
    {
        public PendingTransactionFunctionalityUnlockModel()
        {
            PendingTransactionList = new List<PendingTransactionFunctionalityUnlockModel>();
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string IssueDate { get; set; }
        public string Status { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string Transaction { get; set; }
        public string Remark { get; set; }


        public List<PendingTransactionFunctionalityUnlockModel> PendingTransactionList { get; set; }

    }

    public class CDReplenishModel
    {
        public CDReplenishModel()
        {
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            UserListItem = new Dictionary<string, string>();
        }

        public string CDBelongTo { get; set; }
        public string Remark { get; set; }
        public decimal PaymentID { get; set; }
        public Nullable<decimal> CDAmount { get; set; }
        public Nullable<decimal> CashAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }

        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<string, string> UserListItem { get; set; }

        public bool IsChequeTypeLocal { get; set; }
        public string UserID { get; set; }
        public Nullable<decimal> PreviousCDBalanceAmount { get; set; }
        public Nullable<decimal> TotalCDBalanceAmount { get; set; }


    }




    #region Razorpay Transaction Response

    public class CustomerDetails
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public object gstin { get; set; }
        public object billing_address { get; set; }
        public object shipping_address { get; set; }
        public string customer_name { get; set; }
        public string customer_email { get; set; }
        public string customer_contact { get; set; }
    }

    public class RazorWebInitiateTransactionResponse
    {
        public string id { get; set; }
        public string entity { get; set; }
        public string receipt { get; set; }
        public string invoice_number { get; set; }
        public string customer_id { get; set; }
        public CustomerDetails customer_details { get; set; }
        public string order_id { get; set; }
        public IList<object> line_items { get; set; }
        public object payment_id { get; set; }
        public string status { get; set; }
        public int expire_by { get; set; }
        public int issued_at { get; set; }
        public object paid_at { get; set; }
        public object cancelled_at { get; set; }
        public object expired_at { get; set; }
        public object sms_status { get; set; }
        public object email_status { get; set; }
        public int date { get; set; }
        public object terms { get; set; }
        public bool partial_payment { get; set; }
        public int gross_amount { get; set; }
        public int tax_amount { get; set; }
        public int taxable_amount { get; set; }
        public int amount { get; set; }
        public int amount_paid { get; set; }
        public int amount_due { get; set; }
        public string currency { get; set; }
        public string currency_symbol { get; set; }
        public string description { get; set; }
        public IList<object> notes { get; set; }
        public object comment { get; set; }
        public string short_url { get; set; }
        public bool view_less { get; set; }
        public object billing_start { get; set; }
        public object billing_end { get; set; }
        public string type { get; set; }
        public bool group_taxes_discounts { get; set; }
        public int created_at { get; set; }
    }


    public class RazorPayRequestModel
    {

        public RazorPayCustomer customer { get; set; }
        public string type { get; set; }
        public int view_less { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public string receipt { get; set; }
        public bool reminder_enable { get; set; }
        public int sms_notify { get; set; }
        public int email_notify { get; set; }
        public int expire_by { get; set; }
        public string callback_url { get; set; }
        public string callback_method { get; set; }

    }

    public class RazorPayCustomer
    {
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
    }

    #endregion

    public class InActiveDealerModel
    {
        public InActiveDealerModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
        }
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public string ProductID { get; set; }
        public string ProductDetails { get; set; }

    }

    public class CompanyModel
    {
        public CompanyModel()
        {
            CompanyListItem = new Dictionary<int, string>();

            CompanyInsertListItem = new Dictionary<int, string>();

            CompanyUpdateListItem = new Dictionary<int, string>();
        }
        public Dictionary<int, string> CompanyListItem { get; set; }
        public Dictionary<int, string> CompanyUpdateListItem { get; set; }

        public Dictionary<int, string> CompanyInsertListItem { get; set; }


        public string CompanyID { get; set; }
        public string CompanyUpdateID { get; set; }
        public string CompanyInsertID { get; set; }

        public string CompanyDetails { get; set; }
        public string CompanyDetailsUpdate { get; set; }
        public string CompanyDetailsInsert { get; set; }
        public string Type { get; set; }
        public int CompID { get; set; }
    }





    public class LifeValueCareModel
    {
        public LifeValueCareModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();
            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();
            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        [Required]
        [CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //

        //Bank Fields
        public string LoanAccountNo { get; set; }
        public decimal? LoanAmount { get; set; }
        public int? EMITenure { get; set; }
        public decimal? EMIAmount { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }

        public int IsDownloadCertificate { get; set; }
    }

    public class CardProtectModel
    {
        public CardProtectModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //

            BankNameListItem = new Dictionary<decimal, string>();
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        [Required]
        [CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //

        //
        public string NoOfCards { get; set; }
        public Dictionary<decimal, string> BankNameListItem { get; set; }
        //

        //1
        public string CardNo1n1 { get; set; }
        public string CardNo1n2 { get; set; }
        public string CardType1 { get; set; }
        public decimal? BankNameID1 { get; set; }
        //

        //2
        public string CardNo2n1 { get; set; }
        public string CardNo2n2 { get; set; }
        public string CardType2 { get; set; }
        public decimal? BankNameID2 { get; set; }
        //

        //3
        public string CardNo3n1 { get; set; }
        public string CardNo3n2 { get; set; }
        public string CardType3 { get; set; }
        public decimal? BankNameID3 { get; set; }
        //

        //4
        public string CardNo4n1 { get; set; }
        public string CardNo4n2 { get; set; }
        public string CardType4 { get; set; }
        public decimal? BankNameID4 { get; set; }
        //

        //5
        public string CardNo5n1 { get; set; }
        public string CardNo5n2 { get; set; }
        public string CardType5 { get; set; }
        public decimal? BankNameID5 { get; set; }
        //

        //6
        public string CardNo6n1 { get; set; }
        public string CardNo6n2 { get; set; }
        public string CardType6 { get; set; }
        public decimal? BankNameID6 { get; set; }
        //

        //7
        public string CardNo7n1 { get; set; }
        public string CardNo7n2 { get; set; }
        public string CardType7 { get; set; }
        public decimal? BankNameID7 { get; set; }
        //

        //8
        public string CardNo8n1 { get; set; }
        public string CardNo8n2 { get; set; }
        public string CardType8 { get; set; }
        public decimal? BankNameID8 { get; set; }
        //

        //9
        public string CardNo9n1 { get; set; }
        public string CardNo9n2 { get; set; }
        public string CardType9 { get; set; }
        public decimal? BankNameID9 { get; set; }
        //

        //10
        public string CardNo10n1 { get; set; }
        public string CardNo10n2 { get; set; }
        public string CardType10 { get; set; }
        public decimal? BankNameID10 { get; set; }
        //

        public int IsDownloadCertificate { get; set; }
    }

    public class MasterPolicyNoModel
    {
        public MasterPolicyNoModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
        }

        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public decimal? ProductID { get; set; }
        public decimal? PlanID { get; set; }
        public string MasterPolicyNoInsert { get; set; }
        public DateTime? StartDateInsert { get; set; }
        public DateTime? EndDateInsert { get; set; }
        public string Product { get; set; }
        public string PlanName { get; set; }
        public string MasterPolicyNo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CreatedBy { get; set; }
    }

    public class HealthAssistanceModel
    {
        public HealthAssistanceModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        //[Required]
        //[CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //

        //
        public int? InsuredAge { get; set; }
        public int? NomineeAge { get; set; }
        //

        public int IsDownloadCertificate { get; set; }
    }

    public class HAHCModel
    {
        public HAHCModel()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        //[Required]
        //[CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //

        //
        public int? InsuredAge { get; set; }
        public int? NomineeAge { get; set; }
        //

        public string NomineeTitle { get; set; }
        public string Pincode { get; set; }

        public int IsDownloadCertificate { get; set; }
    }

    public class BatchUploadModel
    {
        public BatchUploadModel()
        {
            ProductListItem = new Dictionary<int, string>();
        }

        public Dictionary<int, string> ProductListItem { get; set; }
        public int? ProductID { get; set; }
    }

    public class DownloadBulkCertificateModel
    {
        public DownloadBulkCertificateModel()
        {
            ProductListItem = new Dictionary<int, string>();
        }

        public Dictionary<int, string> ProductListItem { get; set; }
        public int? ProductID { get; set; }
    }

    public class BulkUploadFileDataModel
    {
        public string UserID { get; set; }
        public string CertificateNo { get; set; }
        public string ProductID { get; set; }
        public string PlanID { get; set; }
        public string MakeID { get; set; }
        public string ModelID { get; set; }
        public string VehicleType { get; set; }
        public string CertificateStartDate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string StateID { get; set; }
        public string CityID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Landmark { get; set; }
        public string PaymentType { get; set; }
        public string IsSingleCheckMultipleCertificate { get; set; }
        public string BankID { get; set; }
        public string Branch { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeAmount { get; set; }
        public string ChequeDate { get; set; }
        public string ChequeType { get; set; }
        public string Remark { get; set; }

        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

        //
        public string InsuredGenderId { get; set; }
        public string InsuredDOB { get; set; }
        public string InsuredAge { get; set; }
        public string NomineeGenderId { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeAge { get; set; }
        public string NomineeName { get; set; }
        public string NomineeRelationshipId { get; set; }
        public string CoverStartDate { get; set; }
        public string CoverEndDate { get; set; }
        //

        public string SumInsuredID { get; set; }
        public string Disease { get; set; }

        public string GSTINNumber { get; set; }

        //
        public string VehicleClass { get; set; }
        public string Pincode { get; set; }
        public string RSALoanAmount { get; set; }
        public string EMIAmount { get; set; }
        public string EMILoanAmount { get; set; }
        public string EMITenure { get; set; }
        public string EMIAccountNo { get; set; }
        public string EMIBankName { get; set; }
        public string EMIBankBranch { get; set; }
        public string Tenure { get; set; }
        public string ContractNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string UploadedAmount { get; set; }
        public string InsuredGender { get; set; }
        public string NomineeGender { get; set; }
        public string NomineeRelationship { get; set; }
        public string Variant { get; set; }
        public decimal PlanAmount { get; set; }
        public decimal PlanGST { get; set; }
        public decimal PlanTotalAmount { get; set; }
        //
    }

    public class BatchUploadPedalCycleModel
    {
        public BatchUploadPedalCycleModel()
        {
            CompanyListItem = new Dictionary<decimal, string>();
        }

        public Dictionary<decimal, string> CompanyListItem { get; set; }
        public decimal CompanyID { get; set; }
    }

    public class TransactionModelPC
    {
        public TransactionModelPC()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //
            CompanyListItem = new Dictionary<decimal, string>();
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        [Required]
        [CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //
        public int? InsuredAge { get; set; }
        public int? NomineeAge { get; set; }

        public Dictionary<decimal, string> CompanyListItem { get; set; }
        public decimal? CompanyID { get; set; }
        public decimal? CycleCost { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public string InvoiceNo { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }

        public int IsDownloadCertificate { get; set; }

        public string strCurrentDate { get; set; }
        public string strPreviousDate { get; set; }
    }

    public class TransactionModelPCMP
    {
        public TransactionModelPCMP()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //
            CompanyListItem = new Dictionary<decimal, string>();
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        [Required]
        [CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //
        public int? InsuredAge { get; set; }
        public int? NomineeAge { get; set; }

        public Dictionary<decimal, string> CompanyListItem { get; set; }
        public decimal? CompanyID { get; set; }
        public decimal? CycleCost { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public string InvoiceNo { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }

        public int IsDownloadCertificate { get; set; }

        public decimal? hdn_Chk_PlanID { get; set; }
        public decimal? hdn_Chk_Multiple_PlanID { get; set; }

        public string strCurrentDate { get; set; }
        public string strPreviousDate { get; set; }
    }

    public class UploadPCCompanyLogoFileModel
    {
        public UploadPCCompanyLogoFileModel()
        {
            CompanyListItem = new Dictionary<decimal, string>();
        }

        public Dictionary<decimal, string> CompanyListItem { get; set; }
        public decimal? CompanyID { get; set; }
    }

    public class ICICILombardAuthentication
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
    }

    public class ICICILombardSMSRequest
    {
        public string CorrelationId { get; set; }
        public int UserId { get; set; }
        public string SenderId { get; set; }
        public string MSISDN { get; set; }
        public string Message { get; set; }
        public string MTag { get; set; }
        public int MsgType { get; set; }
        public string Costcenter { get; set; }
    }

    public class ICICILombardSMSResponse
    {
        public string message { get; set; }
        public bool status { get; set; }
        public string statusMessage { get; set; }
        public string correlationId { get; set; }
    }

    public class IPAddress
    {
        public string EmailId { get; set; }
        public DateTime Created_DateTime { get; set; }
        public string IPAdress { get; set; }
        public bool Status { get; set; }
    }

    public class TieUpCompanyAddOnModel
    {
        public TieUpCompanyAddOnModel()
        {
            TieUpCompanyListItem = new Dictionary<decimal, string>();
        }

        public Dictionary<decimal, string> TieUpCompanyListItem { get; set; }
        public decimal? TieUpCompanyID { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Status { get; set; }
        public decimal? AddOnID { get; set; }
        public string TieUpCompany { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedby { get; set; }
        public string strFromDate { get; set; }
        public string strToDate { get; set; }
    }

    public class TransactionModelMP
    {
        public TransactionModelMP()
        {
            ProductListItem = new Dictionary<decimal, string>();
            MakeListItem = new Dictionary<decimal, string>();
            ModelListItem = new Dictionary<decimal, string>();
            PlanListItem = new Dictionary<decimal, string>();
            PaymentListItem = new Dictionary<decimal, string>();
            BankListItem = new Dictionary<decimal, string>();
            TransactionTypeListItem = new Dictionary<int, string>();
            InsuredGenderListItem = new Dictionary<int, string>();
            NomineeGenderListItem = new Dictionary<int, string>();
            NomineeRelationshipListItem = new Dictionary<int, string>();

            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();

            CommunicationStateListItem = new Dictionary<decimal, string>();
            CommunicationCityListItem = new Dictionary<decimal, string>();

            //Covid Fields
            SumInsuredListItem = new Dictionary<int, string>();
            //

            //CovidIndemnity Fields
            ProposerRelationshipListItem = new Dictionary<int, string>();
            ProposerGenderListItem = new Dictionary<int, string>();
            //
            CompanyListItem = new Dictionary<decimal, string>();

            // New Properties
            PlatformListItem = new Dictionary<int, string>();
            PlanTypeListItem = new Dictionary<int, string>();
            StorageCapacityListItem = new Dictionary<int, string>();
            RAMListItem = new Dictionary<int, string>();
        }

        //// Dictionary Propertry Start ////
        public Dictionary<decimal, string> ProductListItem { get; set; }
        public Dictionary<decimal, string> MakeListItem { get; set; }
        public Dictionary<decimal, string> ModelListItem { get; set; }
        public Dictionary<decimal, string> PlanListItem { get; set; }
        public Dictionary<decimal, string> PaymentListItem { get; set; }
        public Dictionary<decimal, string> BankListItem { get; set; }
        public Dictionary<int, string> TransactionTypeListItem { get; set; }
        public Dictionary<int, string> InsuredGenderListItem { get; set; }
        public Dictionary<int, string> NomineeGenderListItem { get; set; }
        public Dictionary<int, string> NomineeRelationshipListItem { get; set; }
        //
        public Dictionary<int, string> ProposerRelationshipListItem { get; set; }
        public Dictionary<int, string> ProposerGenderListItem { get; set; }
        //

        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public Dictionary<decimal, string> CommunicationStateListItem { get; set; }
        public Dictionary<decimal, string> CommunicationCityListItem { get; set; }

        //Covid 
        public Dictionary<int, string> SumInsuredListItem { get; set; }
        //
        //// Dictionary Propertry End ////


        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTINNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        [Required]
        [CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public Nullable<System.DateTime> ProposerDOB { get; set; }
        public Nullable<int> ProposerGenderId { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }

        public string hdnStateID { get; set; }
        public string hdnCityID { get; set; }

        //CovidIndemnity
        public string ProposerName { get; set; }
        public Nullable<int> ProposerRelationshipId { get; set; }
        //
        public int? InsuredAge { get; set; }
        public int? NomineeAge { get; set; }

        public Dictionary<decimal, string> CompanyListItem { get; set; }
        public decimal? CompanyID { get; set; }
        public decimal? CycleCost { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public string InvoiceNo { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }

        public int IsDownloadCertificate { get; set; }

        public string strCurrentDate { get; set; }
        public string strPreviousDate { get; set; }

        // New Properties
        public Dictionary<int, string> PlatformListItem { get; set; }
        public Dictionary<int, string> PlanTypeListItem { get; set; }
        public int? PlatformID { get; set; }
        public int? PlanTypeID { get; set; }
        public decimal? MobileCost { get; set; }
        public string SerialNo { get; set; }
        public string IMEI1 { get; set; }
        public string IMEI2 { get; set; }
        public string Colour { get; set; }
        public DateTime? ProductPurchaseDate { get; set; }
        public string ProductInvoiceValue { get; set; }
        public string SalesInvoiceNo { get; set; }
        public DateTime? PlanPurchaseDate { get; set; }
        public string Pincode { get; set; }
        public string CustomerContactAlternate { get; set; }
        public string MakeText { get; set; }
        public string ModelText { get; set; }
        public string hdnPlanPurchaseDate { get; set; }
        public Dictionary<int, string> StorageCapacityListItem { get; set; }
        public Dictionary<int, string> RAMListItem { get; set; }
        public int? StorageCapacityID { get; set; }
        public int? RAMID { get; set; }
    }
}

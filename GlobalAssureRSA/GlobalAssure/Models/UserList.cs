﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalAssure.Models
{

    /// <summary>
    /// IndexId     Date            Developer       Desc
    /// 1001        02/09/2019      Fardeen          MIS Report & Policy Copy Field Added 
    /// 1002        09/10/2019      Bikash           MIS Report BalanceAmout added
    /// </summary>

    public class BulkCertificationUpload
    {
        public string Product { get; set; }
        public string UserID { get; set; }
        public string CertificateNo { get; set; }
        public string ProductID { get; set; }
        public string PlanID { get; set; }
        public string MakeID { get; set; }
        public string ModelID { get; set; }
        public string VehicleType { get; set; }
        public string CertificateStartDate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string StateID { get; set; }
        public string CityID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Landmark { get; set; }
        public string PaymentType { get; set; }
        public string IsSingleCheckMultipleCertificate { get; set; }
        public string BankID { get; set; }
        public string Branch { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeAmount { get; set; }
        public string ChequeDate { get; set; }
        public string ChequeType { get; set; }
        public string Remark { get; set; }

        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

        //
        public string InsuredGenderId { get; set; }
        public string InsuredDOB { get; set; }
        public string InsuredAge { get; set; }
        public string NomineeGenderId { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeAge { get; set; }
        public string NomineeName { get; set; }
        public string NomineeRelationshipId { get; set; }
        public string CoverStartDate { get; set; }
        public string CoverEndDate { get; set; }
        //

        public string SumInsuredID { get; set; }
        public string Disease { get; set; }

        public string GSTINNumber { get; set; }
    }

    public class BulkCertificationUploadBatch
    {
        public string UserID { get; set; }
        public string CertificateNo { get; set; }
        public string ProductID { get; set; }
        public string PlanID { get; set; }
        public string MakeID { get; set; }
        public string ModelID { get; set; }
        public string VehicleType { get; set; }
        public string CertificateStartDate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string StateID { get; set; }
        public string CityID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Landmark { get; set; }
        public string PaymentType { get; set; }
        public string IsSingleCheckMultipleCertificate { get; set; }
        public string BankID { get; set; }
        public string Branch { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeAmount { get; set; }
        public string ChequeDate { get; set; }
        public string ChequeType { get; set; }
        public string Remark { get; set; }

        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

        //
        public string InsuredGenderId { get; set; }
        public string InsuredDOB { get; set; }
        public string InsuredAge { get; set; }
        public string NomineeGenderId { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeAge { get; set; }
        public string NomineeName { get; set; }
        public string NomineeRelationshipId { get; set; }
        public string CoverStartDate { get; set; }
        public string CoverEndDate { get; set; }
        //

        public string SumInsuredID { get; set; }
        public string Disease { get; set; }

        public string GSTINNumber { get; set; }

        //
        public string VehicleClass { get; set; }
        public string Pincode { get; set; }
        public string RSALoanAmount { get; set; }
        public string EMIAmount { get; set; }
        public string EMILoanAmount { get; set; }
        public string EMITenure { get; set; }
        public string EMIAccountNo { get; set; }
        public string EMIBankName { get; set; }
        public string EMIBankBranch { get; set; }
        public string Tenure { get; set; }
        public string ContractNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Amount { get; set; }
        public string InsuredGender { get; set; }
        public string NomineeGender { get; set; }
        public string NomineeRelationship { get; set; }
        public string Variant { get; set; }
        public decimal PlanAmount { get; set; }
        public decimal PlanGST { get; set; }
        public decimal PlanTotalAmount { get; set; }
        //
    }

    public class CompanyUploadBhartiAXA
    {
        public string PolicyStatus { get; set; }
        public string CertificateNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string FirstName { get; set; }
        public DateTime? CertificateIssueDate { get; set; }
        public DateTime? CoverStartDate { get; set; }
        public DateTime? CoverEndDate { get; set; }

        public DateTime? InsertDate { get; set; }
        public string CustomerMobileNo { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentState { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
    }

    public class CompanyuploadUniversalShampoo
    {
        public string CertificateNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? CertificateIssueDate { get; set; }
        public DateTime? CoverStartDate { get; set; }
        public DateTime? CoverEndDate { get; set; }
        public string CustomerMobileNo { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentState { get; set; }
        public string PolicyStatus { get; set; }
        public DateTime? InsertDate { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
    }

    public class CompayuploadIDFCBank
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CardLast4Digit { get; set; }
        public string MobileNoLast4Digit { get; set; }
        public string Status { get; set; }
        public DateTime? InsertDate { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
    }

    public class CompanyuploadHDFCErgo
    {
        public string PRODUCT_NAME { get; set; }
        public string POLICY_NO { get; set; }
        public string FIRST_NAME { get; set; }
        public string SURNAME { get; set; }
        public string ADDRESS { get; set; }
        public string LOCALITY { get; set; }
        public string STATE { get; set; }
        public string VEHICLE_REG_NO { get; set; }
        public string CHASSIS_NUM { get; set; }
        public string CAR_MAKE { get; set; }
        public string MODEL { get; set; }
        public DateTime? POLICY_START_DATE { get; set; }
        public DateTime? POLICY_END_DATE { get; set; }
        public DateTime? REGISTRATION_DATE { get; set; }
        public DateTime?  dt_InserDate { get; set; }
        public string STATUS { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
    }

    public class CompanyuploadICICILombard
    {
        public int Id { get; set; }
        public DateTime? LotRecdDate { get; set; }
        public string POLICYNO { get; set; }
        public string InsuredName { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public DateTime? PolicyStartDate { get; set; }
        public DateTime? PolicyEndDate { get; set; }
        public string PolicyStatus { get; set; }
        public string STATUS { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
    }

    public class CompanyUploadPinnace //Added by Parag 15Apr2021
    {
        public string ChassisNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Plan { get; set; }
        public string Title { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredEmailID { get; set; }
        public DateTime? InsuredDOB { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }
        public DateTime? CoverStartDate { get; set; }
        public string Color { get; set; }
        public string NomineeName { get; set; }
        public DateTime? NomineeDOB { get; set; }
        public string NomineeRelationship { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string PolicyStatus { get; set; }
        public DateTime? InsertDate { get; set; }

        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }


    }
    public class CompanyuploadElectricOne
    {
        public string FrameNo { get; set; }
        public string BatteryNo { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Plan { get; set; }
        public string Title { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredEmailID { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }
        public string Color { get; set; }
        public string Status { get; set; }
        public string STATUS { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
    }

    public class CompanyUpdateModel
    {
        //Iffco
        public string Policy { get; set; }

        //BhartiAssist
        public string CertificateNo { get; set; }

        //TataAig
        public string PolicyCert { get; set; }
        public string PolicyStatus { get; set; }

        public string RESPONSE { get; set; }

        public string RESPONSE_REMARK { get; set; }

        //IDFC
        public string  CustomerName { get; set; }
        public string  Email { get; set; }

        //HDFC Ergo
        public string POLICY_NO { get; set; }
        public string STATUS { get; set; }
    }



    public class CompanyUploadDataIFFC
    {
        public string PolicyStatus { get; set; }
        public string Policy { get; set; }
        public string P400Policy { get; set; }
        public string TypeofPolicy { get; set; }
        public string PolicyType { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Policysubmitted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Inceptiondate { get; set; }
        public DateTime? Expirydate { get; set; }
        public string Branch { get; set; }
        public string Lateral { get; set; }
        public string BimaKendraCode { get; set; }
        public string Product { get; set; }
        public decimal? NetPremiumPayable { get; set; }
        public string RegistrationNo { get; set; }
        public string EngineNumber { get; set; }
        public string ChassisNumber { get; set; }
        public string Make { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public DateTime? InsertDate { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }       
    }

    public class InActiveDealers
    {
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string CreateDate { get; set; }
        public string UserName { get; set; }
    }

    public class CompanyUploadDataTATAAIG
    {
        //TATAAIG
        public string PolicyStatus { get; set; }
        public string CustomerName { get; set; }
        public string Surname { get; set; }
        public string PolicyCert { get; set; }
        public DateTime? PolInceptDate { get; set; }
        public DateTime? PolExpDate { get; set; }
        public DateTime? CarSalesDate { get; set; }

        public DateTime? InsertDate { get; set; }
        public string City { get; set; }
        public string state { get; set; }
        public string RegistrationNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public DateTime? IssuanceDate { get; set; }
        public string CancellationFlag { get; set; }
        public string Branch { get; set; }
        public string ProductName { get; set; }
        public string Company { get; set; }

        // IFFCO
        public string Policy { get; set; }
        public string P400Policy { get; set; }
        public string TypeofPolicy { get; set; }
        public string PolicyType { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string  MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? Policysubmitted { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Inceptiondate { get; set; }
        public DateTime? Expirydate { get; set; }
        // public string Branch { get; set; }
        public string Lateral { get; set; }
        public string BimaKendraCode { get; set; }
        public string Product { get; set; }
        public decimal? NetPremiumPayable { get; set; }
        //public string RegistrationNo { get; set; }
        public string EngineNumber { get; set; }
        public string ChassisNumber { get; set; }
        //public string Make { get; set; }
        public string Manufacturer { get; set; }
        //public string Model { get; set; }


        //BhartiAXA

        public string CertificateNo { get; set; }
       // public string EngineNo { get; set; }
      //  public string ChassisNo { get; set; }
        //public string RegistrationNo { get; set; }
        //public string Make { get; set; }
        //public string Model { get; set; }
        public string Variant { get; set; }
      //  public string FirstName { get; set; }
        public DateTime? CertificateIssueDate { get; set; }
        public DateTime? CoverStartDate { get; set; }
        public DateTime? CoverEndDate { get; set; }
        public string CustomerMobileNo { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentState { get; set; }
        //IDFC
        public string Email { get; set; }
        public string CardLast4Digit { get; set; }
        public string MobileNoLast4Digit { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

        //HDFC Ergo
        public string PRODUCT_NAME { get; set; }
        public string POLICY_NO { get; set; }
        public string FIRST_NAME { get; set; }
        public string SURNAME { get; set; }
        public string ADDRESS { get; set; }
        public string LOCALITY { get; set; }
        public string STATE { get; set; }
        public string VEHICLE_REG_NO { get; set; }
        public string CHASSIS_NUM { get; set; }
        public string CAR_MAKE { get; set; }
        public string MODEL { get; set; }
        public DateTime? POLICY_START_DATE { get; set; }
        public DateTime? POLICY_END_DATE { get; set; }
        public DateTime? REGISTRATION_DATE { get; set; }
        public DateTime? dt_InserDate { get; set; }
        public string STATUS { get; set; }
        
        //ICICI Lombard
        public DateTime? LotRecdDate { get; set; }
        public string POLICYNO { get; set; }
        public string InsuredName { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public DateTime? PolicyStartDate { get; set; }
        public DateTime? PolicyEndDate { get; set; }

        //Electric One
        public string FrameNo { get; set; }
        public string BatteryNo { get; set; }
        public string VehicleType { get; set; }
        public string EMake { get; set; }
        public string EModel { get; set; }
        public string Plan { get; set; }
        public string Title { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredEmailID { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }
        public string EColor { get; set; }
        public string EStatus { get; set; }

        //Pinnace
       // public string ChassisNo { get; set; }
        //public string Make { get; set; }
        //public string Model { get; set; }
        //public string Plan { get; set; }
        //public string Title { get; set; }
        //public string InsuredFirstName { get; set; }
        //public string InsuredLastName { get; set; }
        //public string InsuredMobileNo { get; set; }
        //public string InsuredEmailID { get; set; }
        public DateTime? InsuredDOB { get; set; }
        //public string InsuredAddress { get; set; }
       // public string InsuredState { get; set; }
        //public string InsuredCity { get; set; }
        //public DateTime? CoverStartDate { get; set; }
        //public string Color { get; set; }
        public string NomineeName { get; set; }
        public DateTime? NomineeDOB { get; set; }
        public string NomineeRelationship { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        //public string PolicyStatus { get; set; }
        //public DateTime? InsertDate { get; set; }
    }


    public class PaymentBulkCertificationUpload
    {       
        public string CertificateNo { get; set; }
        public string BankID { get; set; }
        public string Branch { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeAmount { get; set; }
        public string ChequeDate { get; set; }
        public string ChequeType { get; set; }
        public string Remark { get; set; }

        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
        public string PaymentReferenceNo { get; set; }
    }


    public class TransactionData
    {

        public decimal TransactionID { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? CoverStartDate { get; set; }
        public DateTime? CoverEndDate { get; set; }
        public string CertificateNo { get; set; }
        public int? StatusID { get; set; }
        public string PlanName { get; set; }
        public decimal? TotalPlanAmount { get; set; }
        public string UserName { get; set; }
        public string LoanAccountNo { get; set; }
        public Nullable<decimal> PlanID { get; set; }
    }


    public class UserDetails
    {
        public string Id { get; set; }
        public string UserID { get; set; }
        public decimal TotalUsedAmount { get; set; }
        public decimal? TotalFloatAmount { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        public decimal? TotalCDAmount { get; set; }
        public decimal? TotalCDUsedAmount { get; set; }
        public decimal? TotalCDBalanceAmount { get; set; }
    }

    public class ProductPlanMakeModelMaster
    {
        public decimal ProductID { get; set; }
        public decimal PlanID { get; set; }
        public decimal MakeID { get; set; }
        public decimal ModelID { get; set; }
        public string ProductName { get; set; }
        public string PlanName { get; set; }
        public string MakeName { get; set; }
        public string ModelName { get; set; }
        public string Varient { get; set; }
        public decimal? PlanAmount { get; set; }
        public decimal? GSTamount { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TieUpCompanyID { get; set; }
    }

    public class BankMaster
    {
        public decimal BankID { get; set; }
        public string BankName { get; set; }
    }

    public class StateCityMaster
    {
        public decimal StateID { get; set; }
        public string State { get; set; }
        public decimal CityID { get; set; }
        public string City { get; set; }
    }


    public class CustomerChequeStatusUpdate
    {
        public string CHEQUE_NO { get; set; }
        public int? STATUS { get; set; }
        public string PAYOUT_CODE { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
    }

    public class AgentChequeStatusUpdate
    {
        public string CERTIFICATE_NO { get; set; }
        public int? STATUS { get; set; }
        public decimal? AMOUNT { get; set; }
        public string PAYOUT_CODE { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
    }


    public class UsersList
    {
        public string NAME { get; set; }
        public string EMAIL { get; set; }
        public string CONTACT { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string PRODUCT { get; set; }
        public string PAYMENT { get; set; }
        public string PLAN { get; set; }
        public string MAKE { get; set; }
        public string MODEL { get; set; }
        public string PREFIX { get; set; }
        public string ISACTIVE { get; set; }
        public string ISADMIN { get; set; }
        public string UserLogo { get; set; }
    }

    public class MailModel
    {
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public List<string> Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        public string AttachmentFileName { get; set; }
        public string AttachmentFileFullPath { get; set; }
    }


    public class MakeModelModel
    {
        public string PRODUCT_TYPE { get; set; }
        public string MANUFACTURE { get; set; }
        public string MODEL { get; set; }
        public string VARIANT { get; set; }
        public string PLAN { get; set; }
        public string AMOUNT { get; set; }

    }

    public class DealerPayoutDownload
    {
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public decimal? PlanAmount { get; set; }
        public decimal? GST { get; set; }
        public decimal? TotalPlanAmount { get; set; }
        public DateTime? CertificateIssueDate { get; set; }
        public string CustomerMobileNo { get; set; }
        public string EmailID { get; set; }
        public string PaymentStatus { get; set; }
        public string PayoutStatus { get; set; }
        public string LoginID { get; set; }
        public string LoginName { get; set; }
        public decimal? DealerShare { get; set; }
        public decimal? GSTPayment { get; set; }
        public decimal? TDS { get; set; }
        public decimal? NetPaid { get; set; }
        public string PayoutPercentage { get; set; }
        public string DealerContact { get; set; }
        public string PaymentMade_Y_N { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string InvoiceReceived_Y_N { get; set; }
    }

    public class MISReport
    {
        public string LANAccountNo { get; set; }
        public string CertificateNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string Product { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string Plan { get; set; }
        public decimal? PlanAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalPlanAmount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerLastName { get; set; }

        public string CustomerMiddleName { get; set; }

        public string CustomerMobileNo { get; set; }
        public string EmailID { get; set; }

        public string CommAddress1 { get; set; }
        public string CommAddress2 { get; set; }
        public string CommAddress3 { get; set; }
        public string CommLandmark { get; set; }
        public string CommCity { get; set; }
        public string CommState { get; set; }

        public string PermAddress1 { get; set; }
        public string PermAddress2 { get; set; }
        public string PermAddress3 { get; set; }
        public string PermLandmark { get; set; }
        public string PermCity { get; set; }
        public string PermState { get; set; }


        public decimal? Amount { get; set; }
        public string PaymentMode { get; set; }
        public DateTime? ChequeDate { get; set; }
        public DateTime? CoverStartdate { get; set; }
        public DateTime? CoverEndDate { get; set; }

        public string Bank { get; set; }
        public string Branch { get; set; }

        public string PayoutStatus { get; set; }
        public string LoginID { get; set; }
        public string LoginName { get; set; }
        public string ChequeType { get; set; }
        public string ChequePaymentType { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Remark { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }

        public string ChequeDateString { get; set; }
        public string CoverStartDateString { get; set; }
        public string CoverEndDateString { get; set; }
        public string IssueDateString { get; set; }
        public string CustomerFullName { get; set; }
        public string IsCertificateUploaded { get; set; }
        public string strchqAmount { get; set; }
        public string strchqNo { get; set; }



        public string CityName { get; set; }
        public string StateName { get; set; }
        public Nullable<decimal> DealerShare { get; set; }
        public Nullable<decimal> TDS { get; set; }
        public Nullable<decimal> NetPaid { get; set; }
        public Nullable<bool> IsPaymentMade { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public string Paymentfilename { get; set; }
        public Nullable<decimal> GSTPayment { get; set; }
        public string PayoutPercentage { get; set; }
        public string DealerContact { get; set; }
        public Nullable<bool> IsInvoiceReceived { get; set; }

        public decimal? CityID { get; set; }

        public string PaymentMade_Y_N { get; set; }
        public string InvoiceReceived_Y_N { get; set; }
        public string PaymentDateString { get; set; }

        public string IMDCode { get; set; }
        public string TieUpCompany { get; set; }
        public decimal? OnlineTransactionID { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        public string SubpaymentMode { get; set; }
        public string OnlinePaymentStatus { get; set; }
        public string CertificateStatus { get; set; }
        //sa 1001
        public string InsuredGender { get; set; }
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public string NomineeName { get; set; }
        public string NomineeGender { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public string NomineeRelationship { get; set; }
        public string GSTINNumbar { get; set; }
        public string BranchCode { get; set; }
        public string AgentEmail { get; set; }
        public string AgentName { get; set; }
        //ea 1001
        //
        public string PId { get; set; }
        //
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CertificateIssueDate { get; set; }
        public string CoverStartDate { get; set; }
        //public string CoverEndDate { get; set; }
        public string PermanentAddress1 { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentState { get; set; }
        public string ChequeNo { get; set; }
        //public string ChequeDate { get; set; }
        public string PaymentStatus { get; set; }

        //
        public string PaymentRemarks { get; set; }
        //

        //new Properties added

        public string BRANCH_CODE { get; set; }
        public string CertificateNumber { get; set; }
        public string IMDcode { get; set; }
        public string PlanName { get; set; }
        public decimal? NetPremium { get; set; }
        public decimal? PremiumwithGST { get; set; }
        public DateTime? Policy_Start_Date { get; set; }
        public DateTime? Policy_End_Date { get; set; }
        public string Salutation { get; set; }
        public string Sum_Insured { get; set; }
        public string ProposerName { get; set; }
        public string ProposerGender { get; set; }
        public string Address_Line_1 { get; set; }
        public string Address_Line_2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Pin_code { get; set; }
        public string Email_ID { get; set; }
        public string Mobile_Number { get; set; }
        public string Registered_under_GST { get; set; }
        public string GSTIN { get; set; }
        public string Insured_RelationShipWith_Proposer { get; set; }
        public string Insured_Name { get; set; }
        public DateTime? Insured_DOB { get; set; }
        public string Identification_Number_Insured { get; set; }
        public string Insured_Gender { get; set; }
        public string Insured_PreExisting_Disease { get; set; }
        public string Insured_PreExisting_Disease_Name { get; set; }
        public string Insured_PreExisting_Disease_Since { get; set; }
        public string Insured_Living_sharing_sameAddressCovid19Diagnosed { get; set; }
        public string InsuredAnyfamilyMembers_EverbeenDetectedwithCOVID19 { get; set; }
        public string Nominee_Name { get; set; }
        public DateTime? Nominee_Date_of_Birth { get; set; }
        public string Relationship_with_Proposer { get; set; }
        public string PlaceofSupply { get; set; }        
        public string UserCategory { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        //

        //CovidIndemnity
        public string ProposerNameCI { get; set; }
        public string ProposerGenderCI { get; set; }
        public DateTime? ProposerDOBCI { get; set; }
        public string ProposerRelationshipCI { get; set; }
        //

        public decimal? CommisionPercentage { get; set; }
        public decimal? CommisionAmount { get; set; }

        public int? InsuredAge { get; set; }
        public int? NomineeAge { get; set; }

        public decimal? APIUserTransactionID { get; set; }

        public string BalanceAmount { get; set; }

        // Pedal Cycle Properties
        public DateTime? InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public string Color { get; set; }
    }

    public class CommisionReportMIS
    {
        public string LANAccountNo { get; set; }
        public string CertificateNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string Product { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string Plan { get; set; }
        public decimal? PlanAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalPlanAmount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerLastName { get; set; }

        public string CustomerMiddleName { get; set; }

        public string CustomerMobileNo { get; set; }
        public string EmailID { get; set; }

        public string CommAddress1 { get; set; }
        public string CommAddress2 { get; set; }
        public string CommAddress3 { get; set; }
        public string CommLandmark { get; set; }
        public string CommCity { get; set; }
        public string CommState { get; set; }

        public string PermAddress1 { get; set; }
        public string PermAddress2 { get; set; }
        public string PermAddress3 { get; set; }
        public string PermLandmark { get; set; }
        public string PermCity { get; set; }
        public string PermState { get; set; }


        public decimal? Amount { get; set; }
        public string PaymentMode { get; set; }
        public DateTime? ChequeDate { get; set; }
        public DateTime? CoverStartdate { get; set; }
        public DateTime? CoverEndDate { get; set; }

        public string Bank { get; set; }
        public string Branch { get; set; }

        public string PayoutStatus { get; set; }
        public string LoginID { get; set; }
        public string LoginName { get; set; }
        public string ChequeType { get; set; }
        public string ChequePaymentType { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Remark { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }

        public string ChequeDateString { get; set; }
        public string CoverStartDateString { get; set; }
        public string CoverEndDateString { get; set; }
        public string IssueDateString { get; set; }
        public string CustomerFullName { get; set; }
        public string IsCertificateUploaded { get; set; }
        public string strchqAmount { get; set; }
        public string strchqNo { get; set; }



        public string CityName { get; set; }
        public string StateName { get; set; }
        public Nullable<decimal> DealerShare { get; set; }
        public Nullable<decimal> TDS { get; set; }
        public Nullable<decimal> NetPaid { get; set; }
        public Nullable<bool> IsPaymentMade { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public string Paymentfilename { get; set; }
        public Nullable<decimal> GSTPayment { get; set; }
        public string PayoutPercentage { get; set; }
        public string DealerContact { get; set; }
        public Nullable<bool> IsInvoiceReceived { get; set; }

        public decimal? CityID { get; set; }

        public string PaymentMade_Y_N { get; set; }
        public string InvoiceReceived_Y_N { get; set; }
        public string PaymentDateString { get; set; }

        public string IMDCode { get; set; }
        public string TieUpCompany { get; set; }
        public decimal? OnlineTransactionID { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        public string SubpaymentMode { get; set; }
        public string OnlinePaymentStatus { get; set; }
        public string CertificateStatus { get; set; }
        //sa 1001
        public string InsuredGender { get; set; }
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public string NomineeName { get; set; }
        public string NomineeGender { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public string NomineeRelationship { get; set; }
        public string GSTINNumbar { get; set; }
        public string BranchCode { get; set; }
        public string AgentEmail { get; set; }
        public string AgentName { get; set; }
        //ea 1001
        //
        public string PId { get; set; }
        //
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CertificateIssueDate { get; set; }
        public string CoverStartDate { get; set; }
        //public string CoverEndDate { get; set; }
        public string PermanentAddress1 { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentState { get; set; }
        public string ChequeNo { get; set; }
        //public string ChequeDate { get; set; }
        public string PaymentStatus { get; set; }

        //
        public string PaymentRemarks { get; set; }
        //

        //new Properties added

        public string BRANCH_CODE { get; set; }
        public string CertificateNumber { get; set; }
        public string IMDcode { get; set; }
        public string PlanName { get; set; }
        public decimal? NetPremium { get; set; }
        public decimal? PremiumwithGST { get; set; }
        public DateTime? Policy_Start_Date { get; set; }
        public DateTime? Policy_End_Date { get; set; }
        public string Salutation { get; set; }
        public string Sum_Insured { get; set; }
        public string ProposerName { get; set; }
        public string ProposerGender { get; set; }
        public string Address_Line_1 { get; set; }
        public string Address_Line_2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Pin_code { get; set; }
        public string Email_ID { get; set; }
        public string Mobile_Number { get; set; }
        public string Registered_under_GST { get; set; }
        public string GSTIN { get; set; }
        public string Insured_RelationShipWith_Proposer { get; set; }
        public string Insured_Name { get; set; }
        public DateTime? Insured_DOB { get; set; }
        public string Identification_Number_Insured { get; set; }
        public string Insured_Gender { get; set; }
        public string Insured_PreExisting_Disease { get; set; }
        public string Insured_PreExisting_Disease_Name { get; set; }
        public string Insured_PreExisting_Disease_Since { get; set; }
        public string Insured_Living_sharing_sameAddressCovid19Diagnosed { get; set; }
        public string InsuredAnyfamilyMembers_EverbeenDetectedwithCOVID19 { get; set; }
        public string Nominee_Name { get; set; }
        public DateTime? Nominee_Date_of_Birth { get; set; }
        public string Relationship_with_Proposer { get; set; }
        public string PlaceofSupply { get; set; }
        public string UserCategory { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        //

        //CovidIndemnity
        public string ProposerNameCI { get; set; }
        public string ProposerGenderCI { get; set; }
        public DateTime? ProposerDOBCI { get; set; }
        public string ProposerRelationshipCI { get; set; }
        //

        public decimal? CommisionPercentage { get; set; }
        public decimal? CommisionAmount { get; set; }

        public bool? BoolIsCommision { get; set; }
        public string IsCommision { get; set; }
        public string Mode { get; set; }
    }

    public class MISReportDecrypt
    {
        public string LANAccountNo { get; set; }
        public string CertificateNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string Product { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Variant { get; set; }
        public string Plan { get; set; }
        public decimal? PlanAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalPlanAmount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerLastName { get; set; }

        public string CustomerMiddleName { get; set; }

        public string CustomerMobileNo { get; set; }
        public string EmailID { get; set; }

        public string CommAddress1 { get; set; }
        public string CommAddress2 { get; set; }
        public string CommAddress3 { get; set; }
        public string CommLandmark { get; set; }
        public string CommCity { get; set; }
        public string CommState { get; set; }

        public string PermAddress1 { get; set; }
        public string PermAddress2 { get; set; }
        public string PermAddress3 { get; set; }
        public string PermLandmark { get; set; }
        public string PermCity { get; set; }
        public string PermState { get; set; }


        public decimal? Amount { get; set; }
        public string PaymentMode { get; set; }
        //public DateTime? ChequeDate { get; set; }
        //public DateTime? CoverStartdate { get; set; }
        //public DateTime? CoverEndDate { get; set; }

        public string Bank { get; set; }
        public string Branch { get; set; }

        public string PayoutStatus { get; set; }
        public string LoginID { get; set; }
        public string LoginName { get; set; }
        public string ChequeType { get; set; }
        public string ChequePaymentType { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Remark { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }

        public string ChequeDateString { get; set; }
        public string CoverStartDateString { get; set; }
        public string CoverEndDateString { get; set; }
        public string IssueDateString { get; set; }
        public string CustomerFullName { get; set; }
        public string IsCertificateUploaded { get; set; }
        public string strchqAmount { get; set; }
        public string strchqNo { get; set; }



        public string CityName { get; set; }
        public string StateName { get; set; }
        public Nullable<decimal> DealerShare { get; set; }
        public Nullable<decimal> TDS { get; set; }
        public Nullable<decimal> NetPaid { get; set; }
        public Nullable<bool> IsPaymentMade { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public string Paymentfilename { get; set; }
        public Nullable<decimal> GSTPayment { get; set; }
        public string PayoutPercentage { get; set; }
        public string DealerContact { get; set; }
        public Nullable<bool> IsInvoiceReceived { get; set; }

        public decimal? CityID { get; set; }

        public string PaymentMade_Y_N { get; set; }
        public string InvoiceReceived_Y_N { get; set; }
        public string PaymentDateString { get; set; }

        public string IMDCode { get; set; }
        public string TieUpCompany { get; set; }
        public decimal? OnlineTransactionID { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDateTime { get; set; }
        public string SubpaymentMode { get; set; }
        public string OnlinePaymentStatus { get; set; }
        public string CertificateStatus { get; set; }
        //sa 1001
        public string InsuredGender { get; set; }
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public string NomineeName { get; set; }
        public string NomineeGender { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public string NomineeRelationship { get; set; }
        public string GSTINNumbar { get; set; }
        public string BranchCode { get; set; }
        public string AgentEmail { get; set; }
        public string AgentName { get; set; }
        //ea 1001
        //
        public string PId { get; set; }
        //
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CertificateIssueDate { get; set; }
        public string CoverStartDate { get; set; }
        public string CoverEndDate { get; set; }
        public string PermanentAddress1 { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public string PermanentCity { get; set; }
        public string PermanentState { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string PaymentStatus { get; set; }
        public string ChequeAmount { get; set; }
    }

    public class UserMISReport
    {
        public string ID { get; set; }
        public string NAME { get; set; }
        public DateTime? DOB { get; set; }
        public string DOBString { get; set; }
        public string EMAIL { get; set; }
        public string CONTACT { get; set; }
        public string PRODUCT { get; set; }
        public string PAYMENT { get; set; }
        public string PLAN { get; set; }
        public string MAKE { get; set; }
        public string MODEL { get; set; }
        public bool? ISACTIVE { get; set; }
        public bool? ISADMIN { get; set; }
        public string PREFIX { get; set; }
        public string DEALER { get; set; }
        public string PICKUP_POINT { get; set; }
        public decimal? TOTAL_FLOAT_AMOUNT { get; set; }
        public decimal? USED_FLOAT_AMOUNT { get; set; }
        public decimal? BALANCE_FLOAT_AMOUNT { get; set; }
        public decimal? TOTAL_PAYMENT_RECEIVED_AMOUNT { get; set; }
        public string Date_of_code_creation { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string AccountNo { get; set; }
        public string Ifsc_Code { get; set; }
        public string BeneficiaryName { get; set; }
        public string BankBranchName { get; set; }
        public string GSTno { get; set; }
        public string PANno { get; set; }
        public Nullable<bool> IsAgreementReceived { get; set; }
        public string PayoutPercentage { get; set; }
        public string TieUpCompany { get; set; }
        public string IMDCode { get; set; }

        public string BranchCode { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonNo { get; set; }
        public decimal? decContactPersonCommisionAmount { get; set; }
        public string ContactPersonCommisionAmount { get; set; }

        //
        public decimal? CommisionPercentage { get; set; }
        public string Mode { get; set; }
    }

    public class VendorReport
    {
        public string VendorName { get; set; }
        public string VendorEMail { get; set; }
        public string VendorMobileNo { get; set; }
        public string VendorPANCard { get; set; }
        public string VendorAadhaarCard { get; set; }
        public string VendorState { get; set; }
        public string VendorCity { get; set; }
        public string VendorPincode { get; set; }
        public string VendorLandMark { get; set; }
        public string VendorAddress1 { get; set; }
        public string VendorAddress2 { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string CompanyPANCard { get; set; }
        public string CompanyGSTIN { get; set; }
        public string CompanyEMail { get; set; }
        public string CompanyPhoneNo { get; set; }
        public string CompanyFAX { get; set; }
        public string CompanyTIN { get; set; }
        public string CompanyTAN { get; set; }
        public string CompanyRegisterdOffice { get; set; }
        public string CompanyHeadOffice { get; set; }
    }

    public class VendorBranchReport
    {
        public string VendorName { get; set; }
        public string BranchName { get; set; }
        public string Pincode { get; set; }
        public string LandMark { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Latitudes { get; set; }
        public string Longitudes { get; set; }
    }

    public class UserAuditLog
    {
        public string AuditBy { get; set; }
        public string NAME { get; set; }
        //public DateTime? DOB { get; set; }
        public string DOB { get; set; }
        public string DOBString { get; set; }
        public string EMAIL { get; set; }
        public string CONTACT { get; set; }
        public string DEALER { get; set; }
        public string PICKUP_POINT { get; set; }
        //public DateTime? AuditDateTime { get; set; }
        public string AuditDateTime { get; set; }
        public string AuditDateTimeString { get; set; }
    }

    public class FeedFileUpload
    {
        public string LoanAccountNo { get; set; }
        public string ProductID { get; set; }
        public string PlanID { get; set; }
        public string MakeID { get; set; }
        public string ModelID { get; set; }
        public string VehicleType { get; set; }
        public string CertificateStartDate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        public string MobileNo { get; set; }
        public string StateID { get; set; }
        public string CityID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Landmark { get; set; }
        public string Remark { get; set; }

        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }
        public string CertificateNo { get; set; }
    }


    public class CDReplenishHistory
    {
        public decimal CDReplenishDetailID { get; set; }
        public string UploadedBy { get; set; }
        public DateTime? UploadedDate { get; set; }
        public string CDBelongTo { get; set; }
        public string PaymentMode { get; set; }
        public decimal? CDAmount { get; set; }
        public string Bank { get; set; }
        public string Branch { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }
    }


    public class DealerPaymentUpload
    {
        public string CertificateNo { get; set; }
        public string DealerShare { get; set; }
        public string GSTPayment { get; set; }
        public string TDS { get; set; }
        public string NetPaid { get; set; }
        public string PayoutPercentage { get; set; }
        public string DealerContact { get; set; }
        public string PaymentMade_Y_N { get; set; }
        public string PaymentDate { get; set; }
        public string InvoiceReceived_Y_N { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

    }

    public class DealerPayoutDetails
    {
        public string CertificateNo { get; set; }
        public decimal? DealerShare { get; set; }
        public decimal? GSTPayment { get; set; }
        public decimal? TDS { get; set; }
        public decimal? NetPaid { get; set; }
        public string PayoutPercentage { get; set; }
        public string DealerContact { get; set; }
        public bool? PaymentMade_Y_N { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool? InvoiceReceived_Y_N { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

    }

    public class InvoiceUploadResponse
    {
        public string ActualFileName { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }

    public class USerFloatAmountDetails
    {
        public string UserID { get; set; }
        public decimal FloatAmount { get; set; }
        public decimal FloatAmountAllUsers { get; set; }
    }

    public class PCPolicyMISReport
    {
        public string CertificateNo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMobileNo { get; set; }
        public string Plan { get; set; }
        public decimal? Amount { get; set; }
        public decimal? GST { get; set; }
        public decimal? PremiumwithGST { get; set; }
        public string Status { get; set; }
        public DateTime? PolicyStartDate { get; set; }
        public DateTime? PolicyEndDate { get; set; }
        public DateTime? PolicyIssueDate { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        public string DealerEmail { get; set; }        
    }

    public class RSABulkCertificationUpload
    {
        public string UserID { get; set; }          
        public string CertificateNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string ProductID { get; set; }
        public string PlanID { get; set; }
        public string MakeID { get; set; }
        public string ModelID { get; set; }
        //public string VehicleType { get; set; }
        //public string CertificateStartDate { get; set; } 
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CertificateIssueDate { get; set; }
        public string CoverStartDate { get; set; }
        public string CoverEndDate { get; set; }
        public string PermanentAddress1 { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public string PermanentCityID { get; set; }
        public string PermanentStateID { get; set; }
        public string InsuredGender { get; set; }

        public string EmailID { get; set; }
        public string MobileNo { get; set; }    
       
        public string PaymentTypeID { get; set; }
        public string IsSingleCheckMultipleCertificate { get; set; }
        public string BankID { get; set; }
        public string Branch { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeAmount { get; set; }
        public string ChequeDate { get; set; }
        public string ChequeType { get; set; }
        public string Remark { get; set; }

        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

        //
        public string InsuredGenderId { get; set; }
        public string InsuredDOB { get; set; }
        public int InsuredAge { get; set; }
        public string NomineeGenderId { get; set; }
        public int NomineeAge { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeName { get; set; }
        public string NomineeRelationshipId { get; set; }
        //

        //public string SumInsuredID { get; set; }
        //public string Disease { get; set; }

        public string GSTINNumber { get; set; }
    }

    public class BulkCertificationUploadPedalCycle
    {        
        public string UserID { get; set; }
        public string CertificateNo { get; set; }
        public string ChassisNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string CycleCost { get; set; }
        public string Title { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredEmailID { get; set; }
        public string InsuredDOB { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }
        public string CoverStartDate { get; set; }
        public string Color { get; set; }
        public string NomineeName { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeRelationship { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

        public decimal CompID { get; set; }
        public string PlanID { get; set; }
        public string InsuredStateID { get; set; }
        public string InsuredCityID { get; set; }
        public string NomineeRelationshipId { get; set; }
        public decimal PlanAmount { get; set; }
        public decimal PlanGST { get; set; }
        public decimal PlanTotalAmount { get; set; }
    }

    public class FloatAmountReportMIS
    {
        public string Id { get; set; }
        public string UserID { get; set; }
        public string Email { get; set; }
        public string RefNo { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        public DateTime? FloatAssignDate { get; set; }
    }

    public class BulkCertificationUploadPedalCycleMultipleProduct
    {
        public string UserID { get; set; }
        public string CertificateNo { get; set; }
        public string ChassisNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Plan { get; set; }
        public string CompanyID { get; set; }
        public string CycleCost { get; set; }
        public string Title { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredEmailID { get; set; }
        public string InsuredDOB { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }
        public string CoverStartDate { get; set; }
        public string Color { get; set; }
        public string NomineeName { get; set; }
        public string NomineeDOB { get; set; }
        public string NomineeRelationship { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public string RESPONSE { get; set; }
        public string RESPONSE_REMARK { get; set; }

        public decimal CompID { get; set; }
        public string PlanID { get; set; }
        public string InsuredStateID { get; set; }
        public string InsuredCityID { get; set; }
        public string NomineeRelationshipId { get; set; }
        public decimal PlanAmount { get; set; }
        public decimal PlanGST { get; set; }
        public decimal PlanTotalAmount { get; set; }

        public string PlanID_Multi { get; set; }
        public string PlanID_PC { get; set; }
    }
}
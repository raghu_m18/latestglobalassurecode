﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalAssure.Models
{
    public class AssistanceDataEntryNewModel
    {
        public AssistanceDataEntryNewModel()
        {
            PermanentStateListItem = new Dictionary<decimal, string>();
            PermanentCityListItem = new Dictionary<decimal, string>();
        }
        public Dictionary<decimal, string> PermanentStateListItem { get; set; }
        public Dictionary<decimal, string> PermanentCityListItem { get; set; }

        public string PolicyNo { get; set; }
        public string CardNo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerLastName { get; set; }
        public string DOB { get; set; }
        public string LastTransDetails { get; set; }
        public string LastPayMade { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public string PermanentStateID { get; set; }
        public string PermanentCityID { get; set; }
        public string Pincode { get; set; }
    }
}
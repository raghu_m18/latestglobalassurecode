﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalAssure.Models
{

    public class AssistModel
    {
        public AssistModel()
        {
            lstIncidentDetails = new List<SelectListItem>();
            lstIncidentType = new List<SelectListItem>();
            lstVendorName = new List<SelectListItem>();
            lstBranchName = new List<SelectListItem>();

            //
            //RemarkListItem = new Dictionary<int, string>();
            RemarkListItem = new List<SelectListItem>();
            //
            lstStateItem = new Dictionary<decimal, string>();
            lstCityItem = new Dictionary<decimal, string>();
            //lstBranchItem = new Dictionary<decimal, string>();
            //
            CompanyListItem = new Dictionary<int, string>();

            VendorReachedIncidentLocationHoursListItem = new Dictionary<string, string>();
            VendorReachedIncidentLocationMinsListItem = new Dictionary<string, string>();
            VendorReachedIncidentLocationSecsListItem = new Dictionary<string, string>();

            RequestTypeListItem = new Dictionary<int, string>();

            CaseTypeListItem = new Dictionary<int, string>();
            SubCategoryListItem = new Dictionary<int, string>();
        }
        public decimal TransactionID { get; set; }
        public decimal? AssistID { get; set; }
        public string UserId { get; set; }
        public string CertificateNo { get; set; }
        public DateTime? CertificateStartDate { get; set; }
        public DateTime? CertificateEndDate { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string VehicleYear { get; set; }
        public string VehicleColor { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public int StatusID { get; set; }
        public string Action { get; set; }
        public int ActionID { get; set; }
        public string PaymentMode { get; set; }
        public string PaymentType { get; set; }
        public string Issue { get; set; }
        public string Remarks { get; set; }
        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string location { get; set; }
        public string AdditionalDetail { get; set; }
        public string AssistSummary { get; set; }
        public int StsId { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        public string Landmark { get; set; }
        public string PinCode { get; set; }       
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool Manual_Allocation { get; set; }
        public decimal Vendor_ID { get; set; }
        public decimal Vendor_BranchID { get; set; }
        public string VendorName { get; set; }
        public string BranchName { get; set; }
        public int IncidentTypeId { get; set; }


        public int IncidentDetailId { get; set; }
        public string IncidentDetail { get; set; }
        public List<SelectListItem> lstIncidentType { get; set; }
        public List<SelectListItem> lstIncidentDetails { get; set; }
        public List<SelectListItem> lstStatus { get; set; }
        public List<SelectListItem> lstVendorName { get; set; }
        public List<SelectListItem> lstBranchName { get; set; }
        public int Vendor_NameId { get; set; }      

        public bool isRecentAssistOpen{ get; set; }

        public string AssistanceRefNo { get; set; }

        //
        public int RemId { get; set; }
        public string RemarkId { get; set; }
        //public Dictionary<int, string> RemarkListItem { get; set; }
        public List<SelectListItem> RemarkListItem { get; set; }
        //
        public int StateID { get; set; }
        public Dictionary<decimal, string> lstStateItem { get; set; }

        public string Area { get; set; }
        public string Vendor { get; set; }
        public int CityID { get; set; }
        public Dictionary<decimal, string> lstCityItem { get; set; }
        public int BranchID { get; set; }
        public Dictionary<decimal, string> lstBranchItem { get; set; }
        //

        public string DropLocation { get; set; }
        public string DropLocationKM { get; set; }

        public double LatitudeDL { get; set; }
        public double LongitudeDL { get; set; }

        public double LatitudeLoc { get; set; }
        public double LongitudeLoc { get; set; }

        public string strLatitudeLoc { get; set; }
        public string strLongitudeLoc { get; set; }

        public int? AssistCompanyID { get; set; }
        public int? AssistOEMID { get; set; }
        public Dictionary<int, string> CompanyListItem { get; set; }
        public Dictionary<int, string> OEMListItem { get; set; }
        public int? hdnAssistCompanyID { get; set; }
        public int? hdnAssistOEMID { get; set; }
        public string AssistSummaryHistory { get; set; }
        public string CompanyName { get; set; }
        public DateTime? VendorReachedIncidentLocationDateTime { get; set; }
        public DateTime? VendorReachedDropLocationDateTime { get; set; }
        public string strVendorReachedIncidentLocationDateTime { get; set; }
        public string strVendorReachedDropLocationDateTime { get; set; }
        public Dictionary<string, string> VendorReachedIncidentLocationHoursListItem { get; set; }
        public string VendorReachedIncidentLocationHours { get; set; }
        public Dictionary<string, string> VendorReachedIncidentLocationMinsListItem { get; set; }
        public string VendorReachedIncidentLocationMins { get; set; }
        public Dictionary<string, string> VendorReachedIncidentLocationSecsListItem { get; set; }
        public string VendorReachedIncidentLocationSecs { get; set; }

        public Dictionary<string, string> VendorReachedDropLocationHoursListItem { get; set; }
        public string VendorReachedDropLocationHours { get; set; }
        public Dictionary<string, string> VendorReachedDropLocationMinsListItem { get; set; }
        public string VendorReachedDropLocationMins { get; set; }
        public Dictionary<string, string> VendorReachedDropLocationSecsListItem { get; set; }
        public string VendorReachedDropLocationSecs { get; set; }

        public Dictionary<int, string> RequestTypeListItem { get; set; }
        public int? RequestTypeID { get; set; }
        public DateTime? RequestPlannedDateTime { get; set; }
        public string strRequestPlannedDateTime { get; set; }
        public Dictionary<string, string> RequestPlannedDateTimeHoursListItem { get; set; }
        public string RequestPlannedDateTimeHours { get; set; }
        public Dictionary<string, string> RequestPlannedDateTimeMinsListItem { get; set; }
        public string RequestPlannedDateTimeMins { get; set; }
        public Dictionary<string, string> RequestPlannedDateTimeSecsListItem { get; set; }
        public string RequestPlannedDateTimeSecs { get; set; }
        public int IsPlanned { get; set; }
        public Dictionary<int, string> CaseTypeListItem { get; set; }
        public int? CaseTypeID { get; set; }
        public Dictionary<int, string> SubCategoryListItem { get; set; }
        public int? SubCategoryID { get; set; }
    }

    public class VendorForAssistModel
    {
        public VendorForAssistModel()
        {
            VendorForAssistModelList = new List<VendorsListModel>();
        }

        public List<VendorsListModel> VendorForAssistModelList { get; set; }
        public Nullable<decimal> AssistID { get; set; }
        public string AssistSummary { get; set; }
        public Nullable<int> StatusID { get; set; }
        public string RemarkID { get; set; }

        public string VendorMobileNo { get; set; }
    }

    public class VendorsListModel
    {
        public decimal VendorBranchID { get; set; }
        public string VendorName { get; set; }
        public string BranchName { get; set; }
        public string VendorContactNo { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Price { get; set; }
        public string KM { get; set; }
        public bool IsChecked { get; set; }
        public string TruckParkLocation { get; set; }
        public string VendorCategory { get; set; }
        public string ServiceType { get; set; }
        public string UptoKMs { get; set; }
        public string PerKMPrice { get; set; }
        public string ServiceTime { get; set; }
    }

    public class IncidentType
    {
        int TypeId { get; set; }
        int TypeName { get; set; }
    }

    public class IncidentDetail
    {
        int DetailId { get; set; }
        int DetailName { get; set; }
    }

    public class CaseChargeModel
    {
        public string AssistID { get; set; }
        public string ReferenceNo { get; set; }
        public string CloseTime { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public bool isRecentAssistOpen { get; set; }
        public int ActionID { get; set; }
        public string CompanyName { get; set; }
    }

    public class CloseCaseChargeModel
    {
        public decimal AssistID { get; set; }
        public decimal? TollCharges { get; set; }
        public decimal? WaitingHoursCharges { get; set; }
        public decimal? VehicalCustodyHoursCharges { get; set; }
        public decimal? OtherCharges { get; set; }
        public decimal? Kilometer { get; set; }
        public decimal? TotalKiloMeters { get; set; }
        public decimal? TotalKiloMetersCharge { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? FinalAmount { get; set; }
        public string RefNo { get; set; }
        public string Ven_ReachedLocation { get; set; }
        public string Ven_ReachedTime { get; set; }
        public string Ven_DropLocation { get; set; }
        public string Ven_DropTime { get; set; }
        public string Ven_TotalRunningKMs { get; set; }
        public decimal? CustomerPaidAmount { get; set; }
        public DateTime? CustomerPaidDate { get; set; }
        public string strCustomerPaidDate { get; set; }
        public string ReferenceNo { get; set; }
    }


    public class AssistAuditDetails
    {
        public string  UserName { get; set; }
        public string Summary { get; set; }
        public string TransactionType { get; set; }
        public decimal? TransactionID { get; set; }
        public string EmailID { get; set; }
        public decimal AssistId { get; set; }
        public string Issue { get; set; }
        public string RefNo { get; set; }
        public string CreatedDate { get; set; }      
        public string CertificateNo { get; set; }
        public DateTime? CertificateStartDate { get; set; }
        public DateTime? CertificateEndDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string VehicleYear { get; set; }
        public string VehicleColor { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }

        public string Action { get; set; }

        public string PaymentMode { get; set; }
        public string PaymentType { get; set; }        
        public string Remarks { get; set; }
        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Landmark { get; set; }
        public string PinCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool Manual_Allocation { get; set; }
        public decimal Vendor_BranchID { get; set; }
        public string VendorName { get; set; }
        public string BranchName { get; set; }
        public int IncidentTypeId { get; set; }
        public int IncidentDetailId { get; set; }
        public string IncidentDetail { get; set; }              
        public decimal AssistNo { get; set; }
        public string Registration { get; set; }
        public string ContactNo { get; set; }       
        public string Lat { get; set; }
        public string Lon { get; set; }
        public decimal? VendorId { get; set; }
        public int? ActionId { get; set; }
        public int? StatusId { get; set; }
        public int? StsId { get; set; }
        public string CustomerName { get; set; }
        public string ChasisNo { get; set; }
      
    }



    public class AssistVendroDetail 
    {
        public decimal AssistNo { get; set; }
        public decimal TransactionID { get; set; }        
        public string CertificateNo { get; set; }
        public DateTime? CertificateStartDate { get; set; }
        public DateTime? CertificateEndDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string VehicleYear { get; set; }
        public string VehicleColor { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        
        public string Action { get; set; }
        
        public string PaymentMode { get; set; }
        public string PaymentType { get; set; }
        public string Issue { get; set; }
        public string Remarks { get; set; }
        public string Address { get; set; }
        public string StreetAddress { get; set; }
        public string location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Landmark { get; set; }
        public string PinCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool Manual_Allocation { get; set; }
        public decimal Vendor_BranchID { get; set; }
        public string VendorName { get; set; }
        public string BranchName { get; set; }
        public int IncidentTypeId { get; set; }
        public int IncidentDetailId { get; set; }
        public string IncidentDetail { get; set; }
        public List<SelectListItem> lstIncidentType { get; set; }
        public List<SelectListItem> lstIncidentDetails { get; set; }

        public decimal AssistId { get; set; }
        public string Registration { get; set; }
        public string ContactNo { get; set; }
        public string RefNo { get; set; }
        public DateTime? CreatedDate1 { get; set; }
        public DateTime? CreatedDate2 { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public decimal? VendorId { get; set; }
        public int? ActionId { get; set; }
        public int? StatusId { get; set; }
        public int? StsId { get; set; }
        public string CustomerName { get; set; }
        public string ChasisNo { get; set; }
        public List<SelectListItem> lstAction { get; set; }
        public List<SelectListItem> lstStatus { get; set; }
        public string CompanyName { get; set; }
        public DateTime? VendorAssignedDateTime { get; set; }
        public DateTime? VendorReachedIncidentLocationDateTime { get; set; }
        public DateTime? VendorReachedDropLocationDateTime { get; set; }
        public DateTime? AssistCreatedDate { get; set; }
    }

    public class AssistMISReport
    {

        public string Registration { get; set; }
        public string CustomerName { get; set; }
        public string Customer_Email { get; set; }
        public string Customer_Contact { get; set; }
        public string Plan { get; set; }
        public string Certificateno { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string RefNo { get; set; }
        public string Issue { get; set; }
        public string Remarks { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string Landmark { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string VendorName { get; set; }
        public string VendorBranchName { get; set; }
        public string AssistanceSummary { get; set; }
    }

    public class AssistanceMISReport
    {        
        public string AgentName { get; set; }
        public string RefNo { get; set; }
        public string PolicyNo { get; set; }
        public DateTime? dtRiskStartDateTime { get; set; }
        public string RiskStartDate { get; set; }
        public DateTime? dtRiskEndDateTime { get; set; }
        public string RiskEndDate { get; set; }
        public string RSACover { get; set; }
        public string MakeAndModel { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string RegistrationNo { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ContactNo { get; set; }       
        public DateTime? dtReportedDateTime { get; set; }
        public DateTime? dtReportedDateTime1 { get; set; }
        public DateTime? dtReportedDateTime2 { get; set; }
        public string ReportedDate { get; set; }
        public string ReportedTime { get; set; }
        public DateTime? dtClosureDateTime { get; set; }
        public string ClosureDate { get; set; }
        public string ClosureTime { get; set; }
        public DateTime? dtVendorAssignedDateTime { get; set; }
        public string VendorAssignedDate { get; set; }
        public string VendorAssignedTime { get; set; }
        public DateTime? dtVendorReachedIncidentLocDateTime1 { get; set; }
        public string dtVendorReachedIncidentLocDateTime2 { get; set; }
        public string VendorReachedIncidentLocDate { get; set; }
        public string VendorReachedIncidentLocTime { get; set; }
        public DateTime? dtVendorReachedDropLocDateTime1 { get; set; }
        public string dtVendorReachedDropLocDateTime2 { get; set; }
        public string VendorReachedDropLocDate { get; set; }
        public string VendorReachedDropLocTime { get; set; }
        public string ServiceTime { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public string CompanyName { get; set; }
        public string VendorName { get; set; }
        public string BranchName { get; set; }
        public string IncidentLocation { get; set; }
        public string DropLocation { get; set; }
        public string VendorToIncident { get; set; }
        public string IToD { get; set; }
        public decimal? decTotalKM { get; set; }
        public string TotalKM { get; set; }
        public decimal? decTotalCost { get; set; }
        public string TotalCost { get; set; }
        public decimal? decTollCharges { get; set; }
        public string TollCharges { get; set; }
        public decimal? decWaitingHoursCharge { get; set; }
        public string WaitingHoursCharge { get; set; }
        public decimal? decVehicleCustodyHoursCharge { get; set; }
        public string VehicleCustodyHoursCharge { get; set; }
        public decimal? decOthersCharge { get; set; }
        public string OthersCharge { get; set; }
        public decimal? decCustomerPaidAmount { get; set; }
        public string CustomerPaidAmount { get; set; }
        public string SelectIncident { get; set; }
        public string CaseSummary { get; set; }
        public string TAT1 { get; set; }
        public string TAT2 { get; set; }
        public string TAT3 { get; set; }
        public string Remark { get; set; }
        public string CertificateNo { get; set; }

        public decimal AssistID { get; set; }
        public string AssistLatitude { get; set; }
        public string AssistLongitude { get; set; }
        public string VendorLatitude { get; set; }
        public string VendorLongitude { get; set; }
        public DateTime? dtPlannedDateTime { get; set; }
        public bool IsPlanned { get; set; }

        public decimal? decCustomerAmountPaid { get; set; }
        public string CustomerAmountPaid { get; set; }
        public DateTime? dtCustomerPaidDate { get; set; }
        public string CustomerPaidDate { get; set; }
        public string ReferenceNo { get; set; }
        public string CaseType { get; set; }
        public string SubCategory { get; set; }
    }

    public class EDataUploadModel
    {
        public EDataUploadModel()
        {
            EDataUploadModelList = new List<EDataUploadListModel>();
        }

        public List<EDataUploadListModel> EDataUploadModelList { get; set; }
    }

    public class EDataUploadListModel
    {
        public EDataUploadListModel()
        {
            VehicleTypeListItem = new Dictionary<int, string>();
        }

        public bool IsChecked { get; set; }
        public string FrameNo { get; set; }
        public string BatteryNo { get; set; }
        public Dictionary<int, string> VehicleTypeListItem { get; set; }
        public int? VehicleTypeID { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Plan { get; set; }
        public string Title { get; set; }        
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredEmailID { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }
        public string Color { get; set; }
    }
}

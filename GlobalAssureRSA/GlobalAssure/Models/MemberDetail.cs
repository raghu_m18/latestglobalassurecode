﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GlobalAssure;
using System.Web.Mvc;

namespace GlobalAssure.Models
{
    public class MemberDetail
    {
        public string PaxId { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredMiddleName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredGender { get; set; }
        public string PassportNo { get; set; }
        public string IdentificationNo { get; set; }
        public string NomineeName { get; set; }
        public string RelationshipOfTheNomineeWithInsured { get; set; }
        public string DateOfBirth { get; set; }
        public string AgeGroup { get; set; }
        public bool SufferingFromAnyPreExistingDisease { get; set; }
        public string NameOfDiseases { get; set; }
        public string AddressOfTheHomeToBeCoveredUnderHomeBurglaryPlan { get; set; }
    }
}
﻿using GlobalAssure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalAssure.Models
{
    public class ServiceResponse
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string status { get; set; }
    }


    public class GetAssistResponse
    {
        //public string VendorName { get; set; }

        //public string VendorBranch { get; set; }

        //public string AssistStatus { get; set; }

        //public string VendorAction { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public IEnumerable<GetAssistDetailsValue> getAssistResponseList { get; set; }

    }
    public class IssueCertificateResponse : ServiceResponse
    {
        public string CertificateNo { get; set; }
    }

    public class APICertificateResponse : ServiceResponse
    {
        public string CertificateNo { get; set; }
        public Nullable<long> TransId { get; set; }
        public string CertificateFileBase64str { get; set; }
        public string ErrorMessageDetail { get; set; }
        public string PaymentLink { get; set; }

    }

    public class APICustomerIssueCertificateResponse : ServiceResponse
    {
        public string CertificateNo { get; set; }
        public Nullable<long> TransId { get; set; }
        public string ErrorMessageDetail { get; set; }
        public string PaymentLink { get; set; }
    }

    public class APIGetCertificateCopyResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }       
        public string CertificateCopy { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGenericIssueCertificateResponse : ServiceResponse
    {
        public string CertificateNo { get; set; }
        public Nullable<long> TransId { get; set; }
        public string ErrorMessageDetail { get; set; }
        public string PaymentLink { get; set; }
    }

    public class APIGenericIssueCertificateWalletResponse : ServiceResponse
    {
        public string CertificateNo { get; set; }
        public Nullable<long> TransId { get; set; }
        public string ErrorMessageDetail { get; set; }
        public string PaymentLink { get; set; }
        public string CertificateFile { get; set; }
    }

    public class APIGenericIssueCertificateV1Response : ServiceResponse
    {
        public Nullable<long> TransId { get; set; }
        public string CertificateNo { get; set; }
        public string CertificateFile { get; set; }
        public string ErrorMessageDetail { get; set; }     
    }

    public class APIGetCertificatePaymentStatusV1Response : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string CertificatePaymentStatus { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetCertificateCopyV1Response : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string CertificateCopy { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APISaveCertificateResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string CertificateFile { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APISaveDocumentDataResponse : ServiceResponse
    {
        public string Message { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetDocumentDataResponse : ServiceResponse
    {
        public string Message { get; set; }
        public string ErrorMessageDetail { get; set; }
        public object DataList { get; set; }
    }

    public class APIGetCertificatePaymentStatusResponse : ServiceResponse
    {
        public string CertificatePaymentStatus { get; set; }
        public string ErrorMessageDetail { get; set; }       
    }

    public class APIGetStoreNameForUserResponse : ServiceResponse
    {
        public string UserName { get; set; }
        public string Token { get; set; }
        public string StoreName { get; set; }
        public decimal BalanceAmount { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyLogoPath { get; set; }
        public string PaymentDetailsApproved { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCGetQuoteResponse : ServiceResponse
    {
        public object DataList { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCGetPolicyCopyResponse : ServiceResponse
    {
        public string CertificateNo { get; set; }
        public string TransactionID { get; set; }
        public string PolicyCopy { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCGetPolicyPendingPaymentResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object PendingPaymentPolicyDetails { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCMakePaymentResponse : ServiceResponse
    {        
        public string TransactionID { get; set; }
        //public string CertificateNo { get; set; }
        //public string PaymentRefNo { get; set; }
        public string PaymentURL { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCCreateProposalResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCExternalLinkGenerationResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string Message { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetVendorStatusResponse : ServiceResponse
    {
        public string UserName { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetVendorAssistResponse : ServiceResponse
    {
        public string Message { get; set; }
        public object RefNoDataList { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetVendorAcceptedResponse : ServiceResponse
    {
        public string Message { get; set; }
        public object RefNoDataList { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetVendorRejectedResponse : ServiceResponse
    {
        public string Message { get; set; }
        public object RefNoDataList { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetVendorCompletedResponse : ServiceResponse
    {
        public string Message { get; set; }
        public object RefNoDataList { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIAssistRequestCompletionResponse : ServiceResponse
    {
        public string Message { get; set; }
        public string ErrorMessageDetail { get; set; }
    }    

    public class APIAssistLoctionDetailsResponse : ServiceResponse
    {
        public string Message { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIUpdateVendorLatLongResponse : ServiceResponse
    {
        public string Message { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class getCertificateResponse : ServiceResponse
    {
        public string Product { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Plan { get; set; }
        public string CertificateStartDate { get; set; }
        public string CertificateEndDate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PaymentStatus { get; set; }
        //public string CertificateURL { get; set; }
    }

    public class NewAssistResponse : ServiceResponse
    {
        public string AssistRefNo { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class getAssistResponse : GetAssistResponse
    {
        public string AssistRefNo { get; set; }
    }

    public class LoginResponse : ServiceResponse
    {
        public string UserName { get; set; }
        public string Token { get; set; }
    }

    public class MobileLoginResponse : ServiceResponse
    {
        public string userId { get; set; }
        public string Token { get; set; }
    }

    public class MobileUserRegistrationResponse : ServiceResponse
    {
        public bool status { get; set; }
    }


    public class AssistStatusResponse : ServiceResponse
    {
        public string UserId { get; set; }
        public string Token { get; set; }
    }

    public class APIMakeMultiplePaymentResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string PaymentURL { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCMakeMultiplePaymentResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string PaymentURL { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCMISDetailsResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object MISDetails { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APISendOTPResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string Message { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIValidateOTPResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string Message { get; set; }
        public string EmailID { get; set; }
        public string Token { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyLogoPath { get; set; }
        public string PaymentDetailsApproved { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetStateMasterResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object StateMaster { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetCityMasterResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object CityMaster { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetReliancePolicyStatusResponse : ServiceResponse
    {
        public object AssistanceRefrenceNoStatus { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetVendorLatLongResponse : ServiceResponse
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIAgentProfileResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string DealerName { get; set; }
        public string GSTNo { get; set; }
        public string PanNo { get; set; }
        public string AccountNo { get; set; }
        public string IFSCcode { get; set; }
        public string AccountHolderName { get; set; }
        public string BankBranchName { get; set; }

        public bool BankDetailStatus { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIDealerDashboardResponse : ServiceResponse
    {
        public string TransactionID { get; set; }       
        public string TotalPolicies { get; set; }
        public string TotalPrem { get; set; }
        public string TotalCommision { get; set; }         
        public object GetPolicyDetails { get; set; }
        public string ReportDownload { get; set; }
        public string ErrorMessageDetail { get; set; }        
    }    

    public class APIVendorAcceptRejectAssistanceResponse : ServiceResponse
    {
        public string CustomerLatitude { get; set; }
        public string CustomerLongitude { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetPCMakeMasterResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object MakeMaster { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetPCModelMasterResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object ModelMaster { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetMakeMasterResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object MakeMaster { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetModelMasterResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object ModelMaster { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetDealerPolicyCountResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object PolicyCount { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCUpdateDealerProfileResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string Message { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIIssueCovidCertificateResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentLink { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetPCRelationshipResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object RelationshipList { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetPCCertificateDataResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string CertificateFile { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetPCWalletDetailsResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object ListWalletDetails { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIInitClaimSearchResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string MobileNumber { get; set; }
        public string CertificateNo { get; set; }
        public string FrameNo { get; set; }
        public object GetPolicyDetails { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class PCGetPolicyDetailsResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object PolicyDetails { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class PCClaimIntimationResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string ClaimReferenceNo { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class PCGetClaimDetailsResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object ClaimDetails { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIAssistRequestGenerationResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string AssistRefNo { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class RootResponse
    {
        public string product_name { get; set; }
        public string product_code { get; set; }
        public string plan_code { get; set; }
        public string sum_assured { get; set; }
        public string start_date { get; set; }
        public string expiry_date { get; set; }
        public decimal premium { get; set; }
        public decimal gst { get; set; }
        public decimal total_premium { get; set; }
        public string policy_number { get; set; }
        public string carrier { get; set; }
        public string covers { get; set; }
        public string cover_codes { get; set; }
        public object coi { get; set; }
        public string customer_ref { get; set; }
        public string status { get; set; }
        public string reason { get; set; }
    }

    public class PartnerAssistRequestResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string AssistRefNo { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class RiskCovryGetStatusResponse
    {
        public Policy policy { get; set; }
    }

    public class Policy
    {
        public string coi { get; set; }
    }

    public class APIGetPCTieUpCompanyMasterResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object TieUpCompanyMaster { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIGetPCTieUpCompanyAddOnResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public object TieUpCompanyAddOnData { get; set; }
        public string ErrorMessageDetail { get; set; }
    }

    public class APIPCCreateProposalMultipleProductResponse : ServiceResponse
    {
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string CertificateFile { get; set; }
        public string ErrorMessageDetail { get; set; }
    }
}
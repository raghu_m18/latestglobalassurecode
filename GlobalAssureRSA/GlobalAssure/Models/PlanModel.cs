﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalAssure.Models
{
    public class PlanModel
    {
        public decimal PlanID { get; set; }
        public string ProductName { get; set; }
        public string Name { get; set; }
        public decimal? Amount { get; set; }
        public decimal? GST { get; set; }
        public decimal? Total { get; set; }
        public bool IsPayment { get; set; }
    }

    public class PlanInActiveModel
    {
        public decimal MappingID { get; set; }
        public string ProductName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PlanName { get; set; }
        public bool IsChecked { get; set; }
    }

    public class UsersModel
    {
        public string UserId { get; set; }       
    }

    public class PlansModel
    {
        public string PlanName { get; set; }
    }

    public class OutputExcelModel
    {
        public string UserIdRemarks { get; set; }
        public string PlanNameRemarks { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalAssure.Models
{
    public class CertificateRequest
    {
        public string FName { get; set; }
        public string MName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }        
        public string Make { get; set; }
        public string Model { get; set; }
        public string Reg { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }

    public class AssistRequest
    {
        public string CertificateNo { get; set; }       
        public string User { get; set; }
        public string Password { get; set; }
    }
}
﻿
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.datacontract.org/2004/07/", IsNullable = false)]
public partial class StateVendorMapping
{

    private string exceptionField;

    private string stateNameField;

    private string state_CodeField;

    private string vendorField;

    /// <remarks/>
    public string Exception
    {
        get
        {
            return this.exceptionField;
        }
        set
        {
            this.exceptionField = value;
        }
    }

    /// <remarks/>
    public string StateName
    {
        get
        {
            return this.stateNameField;
        }
        set
        {
            this.stateNameField = value;
        }
    }

    /// <remarks/>
    public string State_Code
    {
        get
        {
            return this.state_CodeField;
        }
        set
        {
            this.state_CodeField = value;
        }
    }

    /// <remarks/>
    public string Vendor
    {
        get
        {
            return this.vendorField;
        }
        set
        {
            this.vendorField = value;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalAssure.Models
{
    public class LoginInput
    {
        public string UserID { get; set; }
        public string Password { get; set; }
    }

    public class ServiceRequest
    {
        string Token { get; set; }
        string UserId { get; set; }
    }

    public class CertificateDataRequest
    {
        public string Product { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Plan { get; set; }
        public DateTime CertificateStartDate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
    }

    public class APICertificateDataRequest
    {
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }

        public string StateName { get; set; }
        public string PermanentCityName { get; set; }       

        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> ProductName { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public string Make { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CertificateNo { get; set; }
        public string CustomerName { get; set; }
        public string InsuredFirstName { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerLastName { get; set; }
        public string InsuredLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string InsuredEmailID { get; set; }
        public string CustomerContact { get; set; }
        public string InsuredMobileNo { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string InsuredAddressLine1 { get; set; }
        public string PermanentAddress2 { get; set; }
        public string InsuredAddressLine2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> StateID { get; set; }
        //public Nullable<int> InsuredState { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        //public Nullable<int> InsuredCity { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string PlanName { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }

        public Nullable<int> InsuredGenderId { get; set; }
        public Nullable<int> InsuredGender { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeGender { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public Nullable<int> NomineeRelationship { get; set; }
        public string GSTINNumber { get; set; }
        public string InsuredGSTINNumber { get; set; }
        public string BranchCode { get; set; }
        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CertificateStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }

        //[Required]
        //[CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CertificateStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }

        //// Table Property End ////
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }
        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }
        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        //public string Make { get; set; }
        //public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
    }

    public class APICustomerIssueCertificateRequest
    {
        public string InsuredState { get; set; }
        public string InsuredCity { get; set; }

        public string StateName { get; set; }
        public string PermanentCityName { get; set; }

        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> ProductName { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public string Make { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CertificateNo { get; set; }
        public string CustomerName { get; set; }
        public string InsuredFirstName { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerLastName { get; set; }
        public string InsuredLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string InsuredEmailID { get; set; }
        public string CustomerContact { get; set; }
        public string InsuredMobileNo { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string InsuredAddressLine1 { get; set; }
        public string PermanentAddress2 { get; set; }
        public string InsuredAddressLine2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> StateID { get; set; }
        //public Nullable<int> InsuredState { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        //public Nullable<int> InsuredCity { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string PlanName { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }

        public Nullable<int> InsuredGenderId { get; set; }
        public Nullable<int> InsuredGender { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeGender { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public Nullable<int> NomineeRelationship { get; set; }
        public string GSTINNumber { get; set; }
        public string InsuredGSTINNumber { get; set; }
        public string BranchCode { get; set; }
        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CertificateStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }

        //[Required]
        //[CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CertificateStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }

        //// Table Property End ////
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }
        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }
        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        //public string Make { get; set; }
        //public string Model { get; set; }
        public string State { get; set; }
        public string City { get; set; }

        //// Extra Propery End ////
    }

    public class APIGetCertificateCopyRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }       
        public string CertificateNo { get; set; }
        public string UserID { get; set; }
    }

    public class APIGenericIssueCertificateRequest
    {
        //public string InsuredState { get; set; }
        //public string InsuredCity { get; set; }

        //public string StateName { get; set; }
        //public string PermanentCityName { get; set; }

        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public decimal ProductID { get; set; }
        //public Nullable<decimal> ProductName { get; set; }
        //public Nullable<decimal> MakeID { get; set; }
        //public string Make { get; set; }
        //public Nullable<decimal> ModelID { get; set; }
        //public string Model { get; set; }
        //public string EngineNo { get; set; }
        //public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CertificateNo { get; set; }
        public string CustomerName { get; set; }
        public string InsuredFirstName { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerLastName { get; set; }
        public string InsuredLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string InsuredEmailID { get; set; }
        public string CustomerContact { get; set; }
        public string InsuredMobileNo { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        //public string InsuredAddressLine1 { get; set; }
        public string PermanentAddress2 { get; set; }
        //public string InsuredAddressLine2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        //public Nullable<int> StateID { get; set; }
        //public Nullable<int> InsuredState { get; set; }
        //public Nullable<int> PermanentCityID { get; set; }
        //public Nullable<int> InsuredCity { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string PlanName { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }

        //public Nullable<int> InsuredGenderId { get; set; }
        //public Nullable<int> InsuredGender { get; set; }        
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeGender { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public Nullable<int> NomineeRelationship { get; set; }
        //public string GSTINNumber { get; set; }
        //public string InsuredGSTINNumber { get; set; }
        //public string BranchCode { get; set; }
        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CertificateStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }

        //[Required]
        //[CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        //public Nullable<System.DateTime> CertificateStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        public DateTime? dtInsuredDOB { get; set; }
        public string InsuredDOB { get; set; }
        public DateTime? dtNomineeDOB { get; set; }
        public string NomineeDOB { get; set; }

        //// Table Property End ////
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }
        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }
        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public int PlanTypeID { get; set; }

        //public string VehicleType { get; set; }
        //public string Make { get; set; }
        //public string Model { get; set; }
        // string State { get; set; }
        //public string City { get; set; }

        //// Extra Propery End ////
        public Nullable<int> InsuredGenderId { get; set; }
        public Nullable<int> InsuredGender { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public Nullable<decimal> InsuredStateId { get; set; }
        public string InsuredCity { get; set; }
        public Nullable<decimal> InsuredCityId { get; set; }
        public string InsuredPincode { get; set; }
    }

    public class APIGenericIssueCertificateWalletRequest
    {
        //public string InsuredState { get; set; }
        //public string InsuredCity { get; set; }

        //public string StateName { get; set; }
        //public string PermanentCityName { get; set; }

        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public decimal ProductID { get; set; }
        //public Nullable<decimal> ProductName { get; set; }
        //public Nullable<decimal> MakeID { get; set; }
        //public string Make { get; set; }
        //public Nullable<decimal> ModelID { get; set; }
        //public string Model { get; set; }
        //public string EngineNo { get; set; }
        //public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CertificateNo { get; set; }
        public string CustomerName { get; set; }
        public string InsuredFirstName { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerLastName { get; set; }
        public string InsuredLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string InsuredEmailID { get; set; }
        public string CustomerContact { get; set; }
        public string InsuredMobileNo { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        //public string InsuredAddressLine1 { get; set; }
        public string PermanentAddress2 { get; set; }
        //public string InsuredAddressLine2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        //public Nullable<int> StateID { get; set; }
        //public Nullable<int> InsuredState { get; set; }
        //public Nullable<int> PermanentCityID { get; set; }
        //public Nullable<int> InsuredCity { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string PlanName { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }

        //public Nullable<int> InsuredGenderId { get; set; }
        //public Nullable<int> InsuredGender { get; set; }        
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeGender { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public Nullable<int> NomineeRelationship { get; set; }
        //public string GSTINNumber { get; set; }
        //public string InsuredGSTINNumber { get; set; }
        //public string BranchCode { get; set; }
        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CertificateStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }

        //[Required]
        //[CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        //public Nullable<System.DateTime> CertificateStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        public DateTime? dtInsuredDOB { get; set; }
        public string InsuredDOB { get; set; }
        public DateTime? dtNomineeDOB { get; set; }
        public string NomineeDOB { get; set; }

        //// Table Property End ////
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }
        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }
        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public int PlanTypeID { get; set; }

        //public string VehicleType { get; set; }
        //public string Make { get; set; }
        //public string Model { get; set; }
        // string State { get; set; }
        //public string City { get; set; }

        //// Extra Propery End ////
        public Nullable<int> InsuredGenderId { get; set; }
        public Nullable<int> InsuredGender { get; set; }
        public string InsuredAddress { get; set; }
        public string InsuredState { get; set; }
        public Nullable<decimal> InsuredStateId { get; set; }
        public string InsuredCity { get; set; }
        public Nullable<decimal> InsuredCityId { get; set; }
        public string InsuredPincode { get; set; }
        public string Wallet { get; set; }
    }

    public class APIGenericIssueCertificateV1Request
    {
        //public string InsuredState { get; set; }
        //public string InsuredCity { get; set; }

        //public string StateName { get; set; }
        //public string PermanentCityName { get; set; }

        //// Table Property Start ////
        public decimal TransactionID { get; set; }
        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public decimal ProductID { get; set; }
        //public Nullable<decimal> ProductName { get; set; }
        //public Nullable<decimal> MakeID { get; set; }
        //public string Make { get; set; }
        //public Nullable<decimal> ModelID { get; set; }
        //public string Model { get; set; }
        //public string EngineNo { get; set; }
        //public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CertificateNo { get; set; }
        public string CustomerName { get; set; }
        public string InsuredFirstName { get; set; }
        public string CustomerMiddleName { get; set; }
        public string CustomerLastName { get; set; }
        public string InsuredLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string InsuredEmailID { get; set; }
        public string CustomerContact { get; set; }
        public string InsuredMobileNo { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        //public string InsuredAddressLine1 { get; set; }
        public string PermanentAddress2 { get; set; }
        //public string InsuredAddressLine2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        //public Nullable<int> StateID { get; set; }
        //public Nullable<int> InsuredState { get; set; }
        //public Nullable<int> PermanentCityID { get; set; }
        //public Nullable<int> InsuredCity { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string PlanName { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }

        //public Nullable<int> InsuredGenderId { get; set; }
        //public Nullable<int> InsuredGender { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeGender { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public Nullable<int> NomineeRelationship { get; set; }
        //public string GSTINNumber { get; set; }
        //public string InsuredGSTINNumber { get; set; }
        //public string BranchCode { get; set; }
        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CertificateStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }

        //[Required]
        //[CurrentDate(ErrorMessage = "Date must be greater or equal to current date")]
        public Nullable<System.DateTime> CoverStartDate { get; set; }
        //public Nullable<System.DateTime> CertificateStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        public DateTime? dtInsuredDOB { get; set; }
        public string InsuredDOB { get; set; }
        public DateTime? dtNomineeDOB { get; set; }
        public string NomineeDOB { get; set; }

        //// Table Property End ////
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }
        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }
        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public int PlanTypeID { get; set; }

        //public string VehicleType { get; set; }
        //public string Make { get; set; }
        //public string Model { get; set; }
        // string State { get; set; }
        //public string City { get; set; }

        //// Extra Propery End ////
    }

    public class APIGetCertificatePaymentStatusV1Request
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsTransactionSuccess { get; set; }
    }

    public class APIGetCertificateCopyV1Request
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string CertificateNo { get; set; }
        public string UserID { get; set; }
    }
    public class APISaveCertificateRequest
    {
        public string TransactionID { get; set; }
        public string Token { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string MobileNo { get; set; }
        public string RegistrationNo { get; set; }
        public string State { get; set; }
        public decimal StateID { get; set; }      
        public string City { get; set; }
        public decimal CityID { get; set; }
        public int IsSentMail { get; set; }
        public decimal PlanID { get; set; }

        public string PlanName { get; set; }
        public int PlanTypeID { get; set; }
        public string NomineeName { get; set; }
        public string NomineeDOB { get; set; }
        public DateTime? dtNomineeDOB { get; set; }
        public string NomineeGender { get; set; }
        public int? NomineeGenderId { get; set; }
        public string NomineeRelationship { get; set; }
        public int? NomineeRelationshipId { get; set; }
    }

    public class APISaveDocumentDataRequest
    {
        public string Email { get; set; }
        public string DocName { get; set; }
        public string DocNo { get; set; }
        public string Notes { get; set; }
        public string Image { get; set; }
        public string ImageExtension { get; set; }
    }

    public class APIGetDocumentDataRequest
    {
        public string Email { get; set; }        
    }

    public class APIGetCertificatePaymentStatusRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string CertificateNo { get; set; }
    }

    //

    public class APIGetStoreNameForUserRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public decimal FloatAmount { get; set; }
    }

    public class APIPCGetQuoteRequest
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public int TotalCost { get; set; }
        public int PACover { get; set; }
        public int Tenure { get; set; }
        public decimal TieUpCompanyID { get; set; }
    }

    public class APIPCCreateProposalRequest
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string UniqueTransactionID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public Nullable<System.DateTime> CertificateStartDate { get; set; }
        public Nullable<System.DateTime> CertificateEndDate { get; set; }
        public string Title { get; set; }
        public string InsuredName { get; set; }
        public string InsuredEmailID { get; set; }
        public string AadharFront { get; set; }
        public string AadharBack { get; set; }
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredState { get; set; }
        public decimal InsuredStateID { get; set; }
        public string InsuredCity { get; set; }
        public decimal InsuredCityID { get; set; }
        public string InsuredAddressLine1 { get; set; }
        public string NomineeName { get; set; }
        public string NomineeRelationship { get; set; }
        public int NomineeRelationshipID { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public string Model { get; set; }
        public string ChassisNo { get; set; }
        public string Color { get; set; }
        public Nullable<decimal> CycleCost { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoicePhoto { get; set; }
        public string Accessories { get; set; }
        public string AccessoriesInvoice { get; set; }
        public Nullable<System.DateTime> AccessoriesInvoiceDate { get; set; }
        public string AccessoriesInvoiceNo { get; set; }
        public string Company { get; set; }
        public decimal PlanAmountTotal { get; set; }
        public string UserID { get; set; }
        public decimal TieUpCompanyID { get; set; }
    }

    public class APIPCExternalLinkGenerationRequest
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentMode { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal CertiTransID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerContact { get; set; }
        public string CustomerEmail { get; set; }
    }

    public class APIPCGetPolicyCopyRequest
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
    }

    public class APIPCGetPolicyPendingPaymentRequest
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string TransactionID { get; set; }
    }

    public class APIPCMakePaymentRequest
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string TransactionID { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentMode { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal CertiTransID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerContact { get; set; }
        public string CustomerEmail { get; set; }
    }    

    //

    public class APIGetVendorStatusRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class APIGetVendorAssistRequest
    {
        public string Username { get; set; }
    }

    public class APIGetVendorAcceptedRequest
    {
        public string Username { get; set; }
    }

    public class APIGetVendorRejectedRequest
    {
        public string Username { get; set; }
    }

    public class APIGetVendorCompletedRequest
    {
        public string Username { get; set; }
    }

    public class APIAssistRequestCompletionRequest
    {
        public string Username { get; set; }
        public string AssistanceRefNo { get; set; }
        public string ReachedLocation { get; set; }
        public string ReachedTime { get; set; }
        public string DropLocation { get; set; }
        public string DropTime { get; set; }
        public string TotalRunningKms { get; set; }
    }

    public class APIAssistLoctionDetailsRequest
    {
        public string AssistanceRefNo { get; set; }
        public string Address { get; set; }
        public string Latitudes { get; set; }
        public string Longitudes { get; set; }
    }

    public class APIUpdateVendorLatLongRequest
    {
        public string Username { get; set; }
        public string AsstRefrenceNo { get; set; }        
        public string VendorLat { get; set; }
        public string VendorLong { get; set; }
    }

    public class GetCertificateRequest
    {
        public string CertificateNo { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
    }

    public class ReturnSaveAssistRequest
    {
        public string ReturnValue { get; set; }

        public string ReffeNo { get; set; }

        public string Vendor { get; set; }

        public string Branch { get; set; }


    }

    public class AssistStatusRequest
    {
        public string UserId { get; set; }
        public string Token { get; set; }

    }
    public class AssistStatusData
    {
        public string AssistStatus { get; set; }

        public string VendorName { get; set; }

        public string VendorBranch { get; set; }

        public string Address { get; set; }

        public string PinCode { get; set; }

        public string LandMark { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        //public IEnumerable<AssistStatusData> assistStatusDataList { get; set; }
    }

    public class NewAssistRequest
    {
        public string CertificateNo { get; set; }
        public string VehicleColor { get; set; }
        public string VehicleYear { get; set; }
        public string RegistrationNo { get; set; }
        public string Incident { get; set; }
        public string AdditionalDetail { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Vendor { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
        public string Address { get; set; }
        public bool ManualAllocation { get; set; }

        public Nullable<decimal> TransactionId { get; set; }
        public int? IncidentDetailsId { get; set; }
        public string Remarks { get; set; }

        public string PinCode { get; set; }

        public string Landmark { get; set; }

        public string Issue { get; set; }

        public string StreetAddress { get; set; }

        public string City { get; set; }

        public string Location { get; set; }

        public string State { get; set; }

        public string UID { get; set; }

        public string MUserID { get; set; }
        


    }

    public class GetAssistDetailsValue
    {
        public string RefferenceNumber { get; set; }
        public string VendorName { get; set; }
        public string VendorBranch { get; set; }
        public string AssistStatus { get; set; }
        public string VendorAction { get; set; }
        public decimal? VendorId { get; set; }
    }


    public class CheckLattitudeandLongitude
    {
        public decimal? VendorId { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

    }

    public class GetAssistRequest
    {
        public string AsssistRefNo { get; set; }
        public string CertificateNo { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
    }

    public class MobilUserRegistration
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Password { get; set; }
    }


    public class ResetPassword
    {

        public string UserId { get; set; }

        public string Email { get; set; }

        public string MobileNumber { get; set; }

        public string NewPassword { get; set; }

        public string ReturnValueValidation { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }


    }


    public class ResetPasswordOut
    {
        public bool Status { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }
    }


    public class RelianceAssistRequest
    {
        public string PolicyNumber { get; set; }
        public string PolicyStatus { get; set; }
        public string RiskStartDate { get; set; }
        public string RiskEndDate { get; set; }
        public string ProductCode { get; set; }
        public string InsuredName { get; set; }
        public string VehicleNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChasisNo { get; set; }
        public string RSAVendorName { get; set; }
        public string ResponseStatus { get; set; }
        public string InsuredAddressLine { get; set; }
        public string InsuredStateName { get; set; }
        public string InsuredDistrictName { get; set; }
        public string InsuredCityName { get; set; }
        public string InsuredphoneNumber { get; set; }
        public string InsuredMobileNumber { get; set; }
        public string InsuredEmailID { get; set; }

        public string VehicleColor { get; set; }
        public string VehicleYear { get; set; }
        public string Incident { get; set; }
        public string Location { get; set; }
        public string AdditionalDetail { get; set; }
        public string LandMark { get; set; }
        public string Pincode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Vendor { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
        public int IncidentTypeId { get; set; }
        public string IncidentDetail { get; set; }
        public List<SelectListItem> lstIncidentType { get; set; }
        public List<SelectListItem> lstIncidentDetails { get; set; }
        public string RegistrationNo { get; set; }

        //
        public int IncidentId { get; set; }     
        //
    }

    public class APIMakeMultiplePaymentRequest
    {
        public string Username { get; set; }
        public string TransactionID { get; set; }       
        public string Password { get; set; }
        public string CertificateNos { get; set; }
        public decimal TotalAmount { get; set; }       
        public List<decimal> TransactionIDs { get; set; }
        public decimal PaymentAmountTotal { get; set; }
    }

    public class APIPCMakeMultiplePaymentRequest
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string TransactionID { get; set; }
        public string CertificateNos { get; set; }
        public decimal TotalAmount { get; set; }
        public List<decimal> TransactionIDs { get; set; }
        public decimal PaymentAmountTotal { get; set; }
    }

    public class APIPCMISDetailsRequest
    {
        public string TransactionID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class APISendOTPRequest
    {
        public string TransactionID { get; set; }
        public string Application { get; set; }
        public string MobileNo { get; set; }
    }

    public class APIValidateOTPRequest
    {
        public string TransactionID { get; set; }
        public string Application { get; set; }
        public string MobileNo { get; set; }
        public int OTP { get; set; }
    }

    public class APIGetStateMasterRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }        
    }

    public class APIGetCityMasterRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        //public decimal? StateID { get; set; }
        public string StateID { get; set; }
    }

    public class APIGetReliancePolicyStatusRequest
    {
        public string AssistanceRefrenceNos { get; set; }
    }

    public class APIGetVendorLatLongRequest
    {
        public string ReferenceNo { get; set; }
    }

    public class APIAgentProfileRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }

    public class APIGetDelaerDashboardRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string UserMobileNo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchorDownload { get; set; }
        public bool IsMobileSearch { get; set; }
        public bool IsDateSearch { get; set; }
    }

    public class APIVendorAcceptRejectAssistanceRequest
    {
        public string UserId { get; set; }
        public string AssistanceRefNo { get; set; }
        public string Accept { get; set; }
        public string Reject { get; set; }
        public string Remarks { get; set; }
    }

    public class APIGetPCMakeMasterRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string CompanyCode { get; set; }
    }

    public class APIGetPCModelMasterRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public decimal? MakeID { get; set; }
    }

    public class APIGetMakeMasterRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public decimal? ProductID { get; set; }
    }

    public class APIGetModelMasterRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string MakeName { get; set; }
        public decimal? ProductID { get; set; }
        public decimal? MakeID { get; set; }
    }

    public class APIGetDealerPolicyCountRequest
    {
        public string TransactionID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class APIPCDealerProfileUpdateRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string ChequeImage { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNo { get; set; }
        public string IFSCcode { get; set; }
    }

    public class APIIssueCovidCertificateRequest
    {
        public string TransactionID { get; set; }
        public string Token { get; set; }
        public string Password { get; set; }

        public string StateName { get; set; }
        public string PermanentCityName { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string State { get; set; }
        public int StateID { get; set; }
        public string City { get; set; }
        public int CityID { get; set; }

        public Nullable<int> TransactionTypeID { get; set; }
        public string UserID { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<decimal> ProductID { get; set; }
        public Nullable<decimal> MakeID { get; set; }
        public Nullable<decimal> ModelID { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string CertificateNo { get; set; }
        public string PaymentReferenceNo { get; set; }
        public string ChallanNo { get; set; }
        public string CustomerFirstName { get; set; }

        //sa 1003
        public string CustomerMiddleName { get; set; }
        //ea1003

        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public Nullable<bool> IsSameAsPermanentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string PermanentAddress2 { get; set; }
        public string PermanentAddress3 { get; set; }
        public string PermanentLandmark { get; set; }
        public Nullable<int> PermanentCityID { get; set; }
        public string CommunicationAddress { get; set; }
        public string CommunicationAddress2 { get; set; }
        public string CommunicationAddress3 { get; set; }
        public string CommunicationLandmark { get; set; }
        public Nullable<int> CommunicationCityID { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> PaymentID { get; set; }
        public Nullable<int> ChequeTypeID { get; set; }
        public Nullable<int> ChequeForPolicyTypeID { get; set; }
        public Nullable<int> ChequePaymentTypeID { get; set; }
        public string ChequeNo { get; set; }
        public Nullable<System.DateTime> IssueDate { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public Nullable<decimal> ChequeAmount { get; set; }
        public Nullable<decimal> BankID { get; set; }
        public string BranchName { get; set; }
        public int IssueByID { get; set; }
        public string Segment { get; set; }
        //sa1001
        public Nullable<int> InsuredGenderId { get; set; }
        public string NomineeName { get; set; }
        public Nullable<int> NomineeGenderId { get; set; }
        public Nullable<int> NomineeRelationshipId { get; set; }
        public string GSTNumber { get; set; }
        public string BranchCode { get; set; }
        //ea1001

        public Nullable<System.DateTime> _ChequeDate { get; set; }
        public Nullable<System.DateTime> _CoverStartDate { get; set; }
        public Nullable<System.DateTime> _CoverEndDate { get; set; }
        //sa101
        public Nullable<System.DateTime> _InsuredDOB { get; set; }
        public Nullable<System.DateTime> _NomineeDOB { get; set; }
        //ea1001

        public Nullable<System.DateTime> CoverStartDate { get; set; }
        public Nullable<System.DateTime> CoverEndDate { get; set; }
        //sa1001
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        //ea1001
        //// Table Property End ////
        //sa 1002
        public string PACoverStartDate { get; set; }
        //ea 1002
        //// Extra Property End////
        public bool ChkIsSameAsPermanentAddress { get; set; }
        public bool IsSingleChequeSinglePolicy { get; set; }
        public bool IsChequePaymentTypeIntermediary { get; set; }
        public bool IsOnlinePayNowPayLater { get; set; }
        public bool IsChequeTypeLocal { get; set; }
        public string DealerName { get; set; }
        public decimal TotalUsedPlanAmount { get; set; }
        public decimal? TotalFloatPlanAmount { get; set; }
        public decimal? TotalBalanceAmount { get; set; }
        public decimal? TotalPaymentReceivedAmount { get; set; }
        public decimal? GSTAmount { get; set; }
        public decimal? TotalAmount { get; set; }

        //CD payment///
        public decimal? Total_CD_Balance { get; set; }


        public Nullable<int> PermanentStateID { get; set; }
        public Nullable<int> CommunicationStateID { get; set; }

        public string CertStartDate { get; set; }
        public string CertEndDate { get; set; }
        public string Product { get; set; }
        public string Plan { get; set; }
        public string VehicleType { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }

        //// Extra Propery End ////
        ///

        //Covid
        public Nullable<int> SumInsuredID { get; set; }
        //public Nullable<int> PrimaryCover { get; set; }
        //public Nullable<int> SecondaryCover { get; set; }
        public string PrimaryCover { get; set; }
        public string SecondaryCover { get; set; }
        public Nullable<int> PreExDisease { get; set; }
        public string DiseaseName { get; set; }
        //

        //Covid External Param
        public string PId { get; set; }
        public string UserName { get; set; }
    }

    public class APIGetPCRelationshipRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }       
    }

    public class APIGetPCCertificateDataRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string CertificateNo { get; set; }
    }

    public class APIGetPCWalletDetailsRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
    }


    public class APIInitClaimSearchRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string UserMobileNo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CertificateNo { get; set; }
        public string FrameNo { get; set; }
        public string SearchText { get; set; }
        public string SearchType { get; set; }
        public string RefNo { get; set; }
    }

    public class IssueCertificateGAWebPortal
    {
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string RegistrationNo { get; set; }
        public string ProductId { get; set; }
        public string PlanId { get; set; }
        public DateTime CoverStartDate { get; set; }
        public DateTime CoverEndDate { get; set; }
    }

    public class PCGetPolicyDetailsRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string CertificateNo { get; set; }
    }

    public class PCClaimIntimationRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string CertificateNo { get; set; }
        public string MobileNo { get; set; }
        public string ClaimType { get; set; }
        public DateTime? ClaimIntimationDate { get; set; }
        public string ClaimDetails { get; set; }
        public string FIRImage { get; set; }
        public string FIRImageName { get; set; }
        public string SupportedDoc1 { get; set; }
        public string SupportedDoc1Name { get; set; }
        public string SupportedDoc2 { get; set; }
        public string SupportedDoc2Name { get; set; }
        public string SupportedDoc3 { get; set; }
        public string SupportedDoc3Name { get; set; }
        public string SupportedDoc4 { get; set; }
        public string SupportedDoc4Name { get; set; }
        public string SupportedDoc5 { get; set; }
        public string SupportedDoc5Name { get; set; }
        public decimal TransId { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string IfscCode { get; set; }
        public string BankBranch { get; set; }
        public string AccountNumber { get; set; }
        public string CancelledChequeCopy { get; set; }
    }

    public class PCGetClaimDetailsRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string ClaimReferenceNo { get; set; }
    }

    public class PedalClaimIntimationGAWebPortal
    {
        public PedalClaimIntimationGAWebPortal()
        {
            ClaimTypeListItem = new Dictionary<int, string>();
        }

        public Dictionary<int, string> ClaimTypeListItem { get; set; }
        public string CertificateNo { get; set; }
        public decimal TransactionId { get; set; }
        public int ClaimTypeID { get; set; }
        public DateTime? ClaimIntimationDate { get; set; }
        public string hdnClaimType { get; set; }
        public string ClaimDetails { get; set; }
        public string Name { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public string BankBranch { get; set; }
        public string AccountNo { get; set; }
        public string hdnQueryString { get; set; }
        public string ClaimReferenceNo { get; set; }
        public string Status { get; set; }
        public string ErrorMsg { get; set; }
    }

    public class APIAssistRequestGenerationRequest
    {       
        public string CertificateNo { get; set; }
        public string PolicyStatus { get; set; }
        public string CoverStartDate { get; set; }
        public string CoverEndDate { get; set; }
        public string ProductCode { get; set; }
        public string InsuredName { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string RSAVendorName { get; set; }
        public string ResponseStatus { get; set; }
        public string InsuredAddressLine { get; set; }
        public string InsuredStateName { get; set; }
        public string InsuredDistrictName { get; set; }
        public string InsuredCityName { get; set; }
        public string InsuredphoneNumber { get; set; }
        public string InsuredMobileNumber { get; set; }
        public string InsuredEmailID { get; set; }
        public string VehicleColor { get; set; }
        public string VehicleYear { get; set; }
        public string Incident { get; set; }
        public string Location { get; set; }
        public string AdditionalDetail { get; set; }
        public string LandMark { get; set; }
        public string Pincode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Vendor { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
        public int IncidentTypeId { get; set; }
        public string IncidentDetail { get; set; }
        public List<SelectListItem> lstIncidentType { get; set; }
        public List<SelectListItem> lstIncidentDetails { get; set; }
        public string RegistrationNo { get; set; }
        public int IncidentId { get; set; }

        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string TieUpCompanyId { get; set; }
        public decimal ProductID { get; set; }
        public decimal MakeID { get; set; }
        public decimal ModelID { get; set; }
        public string UserID { get; set; }
        public decimal decTieUpCompanyId { get; set; }
        public DateTime dtCoverStartDate { get; set; }
        public DateTime dtCoverEndDate { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Proposer
    {
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string aadhar { get; set; }
    }

    public class Address
    {
        public string address_line_1 { get; set; }
        public string address_line_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
    }

    public class Insured
    {
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string aadhar { get; set; }
        public Address address { get; set; }
    }

    public class Nominee
    {
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string relation { get; set; }
    }

    public class AdditionalInfo
    {
        public string test1 { get; set; }
    }

    public class RootRequest
    {
        public Proposer proposer { get; set; }
        public List<Insured> insured { get; set; }
        public List<Nominee> nominees { get; set; }
        public string plan_code { get; set; }
        public string product_code { get; set; }
        public string start_date { get; set; }
        public string customer_ref { get; set; }
        public string consent_type { get; set; }
        public string consent_time { get; set; }
        public string consent_data { get; set; }
        public string consent_request_time { get; set; }
        public AdditionalInfo additional_info { get; set; }
    }

    public class PartnerAssistRequestRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public string PartnerId { get; set; }
        public string PartnerAuthCode { get; set; }
        public string CertificateNo { get; set; }
        public string RegistrationNo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string Incident { get; set; }
        public string Location { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string AdditionalInfo { get; set; }        
        public string UserID { get; set; }
        public string MUserID { get; set; }
        public int intPartnerId { get; set; }
        public int IncidentId { get; set; }
    }

    public class RiskCovryGetStatusRequest
    {
        public string policy_no { get; set; }
    }

    public class APIGetPCTieUpCompanyMasterRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
    }

    public class APIGetPCTieUpCompanyAddOnRequest
    {
        public string TransactionID { get; set; }
        public string Username { get; set; }
        public decimal? TieUpCompanyID { get; set; }
    }

    public class APIPCCreateProposalMultipleProductRequest
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string UniqueTransactionID { get; set; }
        public Nullable<decimal> PlanID { get; set; }
        public Nullable<System.DateTime> CertificateStartDate { get; set; }
        public Nullable<System.DateTime> CertificateEndDate { get; set; }
        public string Title { get; set; }
        public string InsuredName { get; set; }
        public string InsuredEmailID { get; set; }
        public Nullable<System.DateTime> InsuredDOB { get; set; }
        public string InsuredMobileNo { get; set; }
        public string InsuredState { get; set; }
        public decimal InsuredStateID { get; set; }
        public string InsuredCity { get; set; }
        public decimal InsuredCityID { get; set; }
        public string InsuredAddressLine1 { get; set; }
        public string NomineeName { get; set; }
        public string NomineeRelationship { get; set; }
        public int NomineeRelationshipID { get; set; }
        public Nullable<System.DateTime> NomineeDOB { get; set; }
        public string Model { get; set; }
        public string ChassisNo { get; set; }
        public string Color { get; set; }
        public Nullable<decimal> CycleCost { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string Company { get; set; }
        public decimal PlanAmountTotal { get; set; }
        public string UserID { get; set; }
        public decimal TieUpCompanyID { get; set; }

        public string MultiProductType { get; set; }
        public decimal? MultiPlanID { get; set; }
    }
}
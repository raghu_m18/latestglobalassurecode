﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GlobalAssure;
using System.Web.Mvc;

namespace GlobalAssure.Models
{
    public class TravelModel
    {
        public TravelModel()
        {
            PlanListItem = new Dictionary<int, string>();
            StateListItem = new Dictionary<int, string>();
            CountryVisitingListItem = new Dictionary<int, string>();
            NomRelListItem = new Dictionary<int, string>();
            AgeGroupListItem = new Dictionary<int, string>();
            GenderListItem = new Dictionary<int, string>();
        }
        public Dictionary<int, string> PlanListItem { get; set; }
        public Dictionary<int, string> StateListItem { get; set; }
        public Dictionary<int, string> CountryVisitingListItem { get; set; }
        public Dictionary<int, string> NomRelListItem { get; set; }
        public Dictionary<int, string> AgeGroupListItem { get; set; }
        public Dictionary<int, string> GenderListItem { get; set; }

        //#region Old Properties        
        //public int PlanId { get; set; }

        //public string GSTINNO { get; set; }

        //public int NomRelId { get; set; }

        //public int AgeGroupId { get; set; }

        //public int CountryVisitingId { get; set; }

        //public int StateVisitingId { get; set; }

        //public string CityOfVisit { get; set; }

        //public string NameOfDiseaseDetails { get; set; }

        //public string AddOfHomeCoveredUnderBurgPlan { get; set; }

        //public int CustTitleId { get; set; }

        //public int InsTitleId { get; set; }

        //public int CustGenderId { get; set; }

        //public int InsGenderId { get; set; }

        //public int ComCountryId { get; set; }

        //public int PerCountryId { get; set; }

        //public bool PolicyIssuance { get; set; }

        //public bool PolicyInformation { get; set; }

        //public bool IndianCitizen { get; set; }

        //public bool Visa { get; set; }

        ////public string StartDate { get; set; }
        //public Nullable<System.DateTime> StartDate { get; set; }

        ////public string EndDate { get; set; }
        //public Nullable<System.DateTime> EndDate { get; set; }

        //public string TravelDays { get; set; }

        ////public string DOB { get; set; }
        //public Nullable<System.DateTime> DOB { get; set; }

        //public bool ActiveSports { get; set; }

        ////public bool Medical { get; set; }
        //public int MedicalConditionId { get; set; }       

        //public bool UsaCanada { get; set; }

        //public bool Standard { get; set; }

        //public bool Silver { get; set; }

        //public bool Gold { get; set; }

        //public bool Platinum { get; set; }

        //public bool CustomerDetailsAdhar { get; set; }

        //public string CustomerType { get; set; }

        //public string FirstName { get; set; }

        //public string MiddleName { get; set; }

        //public string LastName { get; set; }

        ////public string CustDOB { get; set; }
        //public Nullable<System.DateTime> CustDOB { get; set; }

        //public string Gender { get; set; }

        //public string Occupation { get; set; }

        //public string MartialStatus { get; set; }

        //public string Nationality { get; set; }

        //public string PassportNo { get; set; }

        //public string RegGST { get; set; }

        //public string RelParty { get; set; }

        //public string PanNo { get; set; }

        //public string AadharNo { get; set; }

        //public string GroupID { get; set; }

        //public string ComFlatNo { get; set; }

        //public string ComBuilding { get; set; }

        //public string ComStreet { get; set; }

        //public string ComLandMark { get; set; }

        //public int ComStateId { get; set; }

        //public string ComCountry { get; set; }

        //public string ComPincodeSearch { get; set; }

        //public string ComDistrict { get; set; }

        //public string ComTownCity { get; set; }

        //public string ComPincode { get; set; }

        //public string MobileNo { get; set; }

        //public string LandLine { get; set; }

        //public string Email { get; set; }

        //public bool IsSameasCommunication { get; set; }

        //public string PerFlatNo { get; set; }

        //public string PerBuilding { get; set; }

        //public string PerStreet { get; set; }

        //public string PerLandMark { get; set; }

        //public int PerStateId { get; set; }

        //public string PerCountry { get; set; }

        //public string PerPincodeSearch { get; set; }

        //public string PerDistrict { get; set; }

        //public string PerTownCity { get; set; }

        //public string PerPincode { get; set; }

        //public string RelProposer { get; set; }

        //public string InsPassportNo { get; set; }

        //public string InsFirstName { get; set; }

        //public string InsMiddleName { get; set; }

        //public string InsLastName { get; set; }

        //public string InsGender { get; set; }

        ////public string InsDOB { get; set; }
        //public Nullable<System.DateTime> InsDOB { get; set; }

        //public string InsOccupation { get; set; }

        //public string InsMobileNo { get; set; }

        //public string InsLandLine { get; set; }

        //public string InsEmail { get; set; }       

        //public string NomName { get; set; }

        ////public string NomDOB { get; set; }
        //public Nullable<System.DateTime> NomDOB { get; set; }

        //public string NomRelation { get; set; }

        //public bool DocDetails { get; set; }

        //public string BranchCode { get; set; }

        //public string AgentBroker { get; set; }

        //public string VerticalCode { get; set; }

        //public string Refcode { get; set; }

        //public string SMCode { get; set; }

        //public string Channel { get; set; }

        //public bool PropsalBy {get; set;}

        //public string POSPanNo { get; set; }

        //public string POSAdharNo { get; set; }

        //public bool Vertification { get; set; }
        //#endregion

        #region Old Properties        
        public string PlanName { get; set; }

        public string GSTINNO { get; set; }

        public string NomRel { get; set; }

        public string AgeGroup { get; set; }

        public string CountryVisitingName { get; set; }

        public string StateVisiting { get; set; }

        public string CityOfVisit { get; set; }

        public string NameOfDiseaseDetails { get; set; }

        public string AddOfHomeCoveredUnderBurgPlan { get; set; }

        public string CustTitle { get; set; }

        public string InsTitle { get; set; }

        public string CustGender { get; set; }

        public string InsGender { get; set; }

        public string ComCountry { get; set; }

        public string PerCountry { get; set; }

        public bool PolicyIssuance { get; set; }

        public bool PolicyInformation { get; set; }

        public bool IndianCitizen { get; set; }

        public bool Visa { get; set; }

        //public string StartDate { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }

        //public string EndDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }

        public string TravelDays { get; set; }

        //public string DOB { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }

        public bool ActiveSports { get; set; }

        //public bool Medical { get; set; }
        public string MedicalCondition { get; set; }

        public bool UsaCanada { get; set; }

        public bool Standard { get; set; }

        public bool Silver { get; set; }

        public bool Gold { get; set; }

        public bool Platinum { get; set; }

        public bool CustomerDetailsAdhar { get; set; }

        public string CustomerType { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        //public string CustDOB { get; set; }
        public Nullable<System.DateTime> CustDOB { get; set; }

        public string Gender { get; set; }

        public string Occupation { get; set; }

        public string MartialStatus { get; set; }

        public string Nationality { get; set; }

        public string PassportNo { get; set; }

        public string RegGST { get; set; }

        public string RelParty { get; set; }

        public string PanNo { get; set; }

        public string AadharNo { get; set; }

        public string GroupID { get; set; }

        public string ComFlatNo { get; set; }

        public string ComBuilding { get; set; }

        public string ComStreet { get; set; }

        public string ComLandLine { get; set; }

        public string ComLandMark { get; set; }

        public string ComState { get; set; }

        public string ComPincodeSearch { get; set; }

        public string ComDistrict { get; set; }

        public string ComTownCity { get; set; }

        public string ComPincode { get; set; }

        public string MobileNo { get; set; }

        public string LandLine { get; set; }

        public string Email { get; set; }

        public bool IsSameasCommunication { get; set; }

        public string PerFlatNo { get; set; }

        public string PerBuilding { get; set; }

        public string PerStreet { get; set; }

        public string PerLandMark { get; set; }

        public string PerState { get; set; }

        public string PerPincodeSearch { get; set; }

        public string PerDistrict { get; set; }

        public string PerTownCity { get; set; }

        public string PerPincode { get; set; }

        public string RelProposer { get; set; }

        public string InsPassportNo { get; set; }

        public string InsFirstName { get; set; }

        public string InsMiddleName { get; set; }

        public string InsLastName { get; set; }

        //public string InsDOB { get; set; }
        public Nullable<System.DateTime> InsDOB { get; set; }

        public string InsOccupation { get; set; }

        public string InsMobileNo { get; set; }

        public string InsLandLine { get; set; }

        public string InsEmail { get; set; }

        public string NomName { get; set; }

        //public string NomDOB { get; set; }
        public Nullable<System.DateTime> NomDOB { get; set; }

        public string NomRelation { get; set; }

        public bool DocDetails { get; set; }

        public string BranchCode { get; set; }

        public string AgentBroker { get; set; }

        public string VerticalCode { get; set; }

        public string Refcode { get; set; }

        public string SMCode { get; set; }

        public string Channel { get; set; }

        public bool PropsalBy { get; set; }

        public string POSPanNo { get; set; }

        public string POSAdharNo { get; set; }

        public bool Vertification { get; set; }
        #endregion     
    }

    public class TravelModelRequest
    {
        #region New Properties
        public string Product_Code { get; set; }
        public string AgentCode_BASCode { get; set; }
        public string UserName { get; set; }
        public string EncryptedPassword { get; set; }
        public string IntermediatoryBranchCode { get; set; }
        public string IntermediatoryDepartmentName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string CityName { get; set; }
        public string PinCode { get; set; }
        public string State { get; set; }
        public string EmailID { get; set; }
        public string MobileNumber { get; set; }
        public string LandLineNumber { get; set; }
        public string NameofPlan { get; set; }
        public string PolicyStartDate { get; set; }
        //public Nullable<System.DateTime> PolicyStartDate { get; set; }
        public string PolicyEndDate { get; set; }
        //public Nullable<System.DateTime> PolicyEndDate { get; set; }
        public string BusinessType { get; set; }
        public string PSONumber { get; set; }
        public string SenderName { get; set; }
        public string CountryVisiting { get; set; }
        public string StateVisit { get; set; }
        public string City_Of_visit { get; set; }
        public string IsRegGST { get; set; }
        public string Cust_GSTINNO { get; set; }
        //public List<MemberDetail> MemberDetails { get; set; }
        public object MemberDetails { get; set; }
        #endregion
    }

    public class TravelModelResponse 
    {
        public string PSONumber { get; set; }
        public object CustomerDetails { get; set; }
    }

    public class CustomerDetail
    {
        public string PaxId { get; set; }
        public string InsuredName { get; set; }
        public string WebServiceReturnStatus { get; set; }
        public string CertificateNumber { get; set; }
        public string DownloadFilePath { get; set; }
        public string ErrorCode { get; set; }
        public string Error_Message { get; set; }
    }

    public class RILTravelServiceResponse
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string status { get; set; }
    }
}
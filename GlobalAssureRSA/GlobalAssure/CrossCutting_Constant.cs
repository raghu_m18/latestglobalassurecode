﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalAssure
{
    public class CrossCutting_Constant
    {

        public const string ContentType1 = "application/vnd.ms-excel";
        public const string ContentType2 = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public const string ContentType3 = "application/octet-stream";

        public const string DocLocation = "~/Doc/";

        public const string XLSFormat = ".xls";
        public const string xlsxFormat = ".xlsx";

        public const string ChequePaymentType = "ChequePaymentType";
        public const string ChequeType = "ChequeType";
        public const string ChequeForPolicy = "ChequeForPolicy";
        public const string TransactionType = "TransactionType";
        public const string InsuredGender = "InsuredGender";
        public const string NomineeGender= "NomineeGender";
        public const string NomineeRelationship = "NomineeRelationship";
        public const string MakeModel = "MakeModel";
        public const string UserPermission = "UserPermission";
        public const string UpdateUserPermission = "UpdateUserPermission";
        public const string StateAndCityUpload = "StateAndCityUpload";
        public const string BankUpload = "BankUpload";


        //
        public const string Company_Name = "CompanyName";
        //

        public const string FloatAmountUpload = "FloatAmountUpload";
        public const string LockUnlockUpload = "LockUnlockUpload";
        public const string PlanUpload = "PlanUpload";

        public const int ChequeForPolicy_Single_Cheque_Single_Policy_ID = 1;
        public const int ChequeForPolicy_Single_Cheque_Multiple_Policy_ID = 2;
        public static string DefaultPassword = "Assist@123";
        public static string VendorPassword = "Vendor@123";

        public const int Cheque_Type_Local_ID = 3;
        public const int Cheque_Type_Upcountry_ID = 4;

        public const int ChequePaymentType_Intermediary_ID = 8;
        public const int ChequePaymentType_Customer_ID = 9;

        public const int IssueBy_Application_ID = 10;
        public const int IssueBy_BulkUpload_ID = 11;
        public const int IssueBy_WebService_ID = 12;
        public const int IssueBy_BrokerPortal_ID = 13;
        public const int IssueBy_FeedFile_ID = 16;
        public const int IssueBy_TVSPrintingPortal_ID = 17;
        public const int IssueBy_API_ID = 29;

        public const string ChequeType_Local = "Local";
        public const string ChequeType_Upcontry = "Upcountry";

        public const string ChequePaymentType_Intermediary = "Intermediary";
        public const string ChequePaymentType_Customer = "Customer";

        public const string Payment_Cheque = "Cheque";
        public const string Payment_Online = "Online";
        public const string Payment_Cash = "Cash";
        public const string Payment_NEFT = "NEFT";
        public const string Payment_Cash_Deposite = "Cash Deposite";
        public const string PayerType_Agent = "agent";

        public const string OnlinePaymentStatus_Pending = "pending";
        public const string OnlinePaymentStatus_Success = "success";
        public const string OnlinePaymentStatus_Failure = "failure";

        public const string NoResponseFromGateway = "No Response from Payment Gateway";

        public const string vPayResponseCode_200 = "200";
        public const string vPayResponseCode_400 = "400";
        public const string vPayResponseCode_502 = "502";
        public const string vPayResponseCode_503 = "503";
        public const string vPayResponseCode_506 = "506";

        public const string vPayResponseCode_200_Description = "Success";
        public const string vPayResponseCode_400_Description = "Bad Request";
        public const string vPayResponseCode_502_Description = "Authentication failed";
        public const string vPayResponseCode_503_Description = "Input missing";
        public const string vPayResponseCode_506_Description = "Invalid input value";

        public const string vPayCallback_Status_Success = "Success";
        public const string vPayCallback_Status_Failed = "Failed";
        public const string vPayCallback_Status_Pending = "Pending";

        public const int Payment_Cheque_ID = 1;
        public const int Payment_Online_ID = 2;
        public const int Payment_Cash_ID = 3;
        public const int Payment_NEFT_ID = 4;
        public const int Payment_Cash_Deposite_ID = 5;
        public const int Payment_DD_Deposite_ID = 6;
        public const int Payment_External_ID = 7;


        public const int TransactionType_NEW_ID = 5;
        public const int TransactionType_OLD_ID = 6;

        public const string Success = "Success";
        public const string Failure = "Failure";

        public const string CertificatePrefix = "C";
        public const string PaymentReferencePrefix = "P";
        public const string ChallanPrefix = "Challan";
        public const string BulkCertificatePrefix = "BC";
        public const string AssistReferencePrefix = "AT";
        public const string VendorReferencePrefix = "V";

        public const string UploadType_ChequeStatusUpdate = "ChequeStatusUpdate";
        public const string UploadType_ChequeStatusRecoveryUpdate = "ChequeStatusRecoveryUpdate";


        public const string UploadType_CustomerChequeStatusUpdate = "CustomerChequeStatusUpdate";
        public const string UploadType_AgentChequeStatusUpdate = "AgentChequeStatusUpdate";


        public const int StatusID_Payment_Entry = 1;
        public const int StatusID_Pending_Payment = 2;
        public const int StatusID_Payment_Deposition = 3;
        public const int StatusID_Cheque_Bounced = 4;
        public const int StatusID_Payment_Confirmation = 5;
        public const int StatusID_Certificate_Cancelled = 6;
        public const int StatusID_Dealer_Payment_Submitted = 7;

        public const int StatusID_Vendor_Not_Assign = 8;
        public const int StatusID_Vendor_Assigned = 9;
        public const int StatusID_Vendor_Closed_Issue = 10;

        public const int ActionID_Vendor_No_Action = 11;
        public const int ActionID_Vendor_Accepted_Request = 12;
        public const int ActionID_Vendor_Reject_Request = 13;
        public const int Certificate_Cancelled = 14;

        public const string UIPolicy = "UIPolicy";
        public const string FacadePolicy = "FacadePolicy";
        public const string BLPolicy = "BLPolicy";
        public const string AMPolicy = "AMPolicy";
        public const string RepositoryPolicy = "RepositoryPolicy";
        public const string ControllerLogicPolicy = "ControllerLogicPolicy";
        public const string PassThroughPolicy = "PassThroughPolicy";
        public const string ServiceSIPolicy = "ServiceSIPolicy";
        public const string ServiceHostPolicy = "ServiceHostPolicy";

        public const string ContentType = "application/excel";

        public const string ReportType_MIS = "MIS";
        public const string ReportType_UserMIS = "UserMIS";
        public const string ReportType_UserAuditLog = "UserAuditLog";
        public const string ReportType_FeedFileMIS = "FeedFileMIS";

        public const string ReportType_CustomerChequeStatusUpdateOutput = "CustomerChequeStatusUpdateOutput";
        public const string ReportType_AgentChequeStatusUpdateOutput = "AgentChequeStatusUpdateOutput";
        public const string ReportType_BulkCertificationUploadOutput = "BulkCertificationUploadOutput";
        public const string ReportType_PaymentBulkCertificationUploadOutput = "PaymentBulkCertificationUploadOutput";
        public const string ReportType_FeedFileUploadOutput = "FeedFileUploadOutput";
        public const string ReportType_DealerPaymentUpdateOutput = "DealerPaymentUpdateOutput";

        public const string MasterData_ProductPlanMakeModel = "ProductPlanMakeModel_Master";
        public const string MasterData_StateCity = "StateCity_Master";
        public const string MasterData_Bank = "Bank_Master";

        public const string PAYOUT_CODE_PR = "PR";
        public const string PAYOUT_CODE_PP = "PP";

        public const int PAYOUT_PAYMENT_RELEASED = 14;
        public const int PAYOUT_PAYMENT_PENDING = 15;

        public const int TieUpCompany_RSA_ID = 1;
        public const int TieUpCompany_HERO_ID = 2;
        public const int TieUpCompany_TVS_ID = 3;
        public const int TieUpWithVendor = 4;
        public const int TieUpWithPolicyBazar = 5;
        public const int TieUpAssistance = 6;
        public const int TieUpRelianceAssist = 7;


        public const int DealerPayment_CommunicationName_DealerPaymentUploadAutomaticMailIntimation_1 = 1;

        public const string DealerPayment_MessageReferenceCode_DPU_EMAIL = "DPU_EMAIL";
        public const string DealertPayment_Subject = "Global Assure – RSA Payouts";

        public const string State_UP_Name_Lower = "uttar pradesh";

        public const string LoginUserNomineeDetailsForMandatory = "BINSARAUTOMOBILES@YAHOO.COM,lalitgupta_binsar@yahoo.co.in";

        public const string RemarkAssistanceDataEntry = "NON GA";

        // <!--RIL Travel Constant Values-->
        public const string RILTravelPlan = "RILTravelPlan";

        public const string RILTravelAgeGroup = "RILTravelAgeGroup";

        public const string RILTravelGender = "RILTravelGender";
        //

        public const string NoAssistanceFound = "NoAssistanceFound";
    }
}
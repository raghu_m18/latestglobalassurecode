﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="simple" font-size="10pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <!--<fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1cm"/>-->
            <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">: Pedal Cycle Policy Details :</fo:inline>
          </fo:block>

        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          
          <fo:table layout="fixed" width="100%">
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="150pt"/>

            <fo:table-header ></fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="1mm" color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" border-width="0.1px" border-style="solid">
                  <fo:block>
                    <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">Issue Date</fo:inline>
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" border-width="0.1px" border-style="solid">
                  <fo:block>
                    <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">Plan Name</fo:inline>
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" border-width="0.1px" border-style="solid">
                  <fo:block>
                    <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">Plan Amount</fo:inline>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>                                

            </fo:table-body>

          </fo:table>

          <xsl:for-each select="PCDealerDashboardDetailsModel/PCDealerDashboardDetailsModelList/PCDealerDashboardDetails">
            <fo:table layout="fixed" width="100%">
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="150pt"/>

            <fo:table-header ></fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                  <fo:block>
                    <xsl:value-of select="IssueDate" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                  <fo:block>
                    <xsl:value-of select="PlanName" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                  <fo:block>
                    <xsl:value-of select="PlanAmount" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>

          </fo:table>
          </xsl:for-each>
          
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1.2cm"/>
          </fo:block>

        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Registered Address </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="260pt"/>
                      <fo:table-column column-width="260pt"/>
                      
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">F-206, SATYA SHANTI APARTMENT, SECTOR-13, ROHINI, DELHI, North West Delhi, Delhi, 110085 </fo:block>
                          </fo:table-cell>
                          
                          <fo:table-cell>
                            <fo:block text-align="right">

                              3rd Floor, AIHP Horizon, 445, Phase 5, Udyog Vihar, Gurgaon -122016
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      
                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>
                          
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="2pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Certificate Cum Invoice</fo:block>
                          </fo:table-cell>
                          <!--<fo:table-cell>
                            <fo:block text-align="right">Toll Free Number : 1800 102 0100</fo:block>
                          </fo:table-cell>-->
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Tax Invoice</fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="5pt">
                    This is to certify that Vehicle with the following details is covered under Assistance Program, as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--Dynamic Content When State IS NOT UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table layout="fixed" width="100%" font-size="6pt">
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-header></fo:table-header>
                <fo:table-body xsl:use-attribute-sets="myBorder">
                  <!-- xsl:use-attribute-sets="myBorder" -->
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Intermediary Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/DealerName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Start Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate End Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Engine Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Chassis Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row >
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Manufacturer</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Make" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Model</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Model" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row  background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Variant</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Vehicle Registration Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Amount</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Amount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amount Of Tax IGST (18%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/IGST" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Total Amount (Including Tax)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">GST No. of the Service Recipient</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amount In Words</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Place Of Supply</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">SAC Code</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">First Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Last Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address1</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address2</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address3</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">City</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/City" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">State</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/State" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Mobile No.</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <!--<fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Email ID</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Email" />
                      </fo:block>
                    </fo:table-cell>-->
                    <fo:table-cell padding="2pt">
                      <fo:block></fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block> </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->


          <!--Dynamic Content When State IS UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table layout="fixed" width="100%" font-size="6pt">
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-header></fo:table-header>
                <fo:table-body xsl:use-attribute-sets="myBorder">
                  <!-- xsl:use-attribute-sets="myBorder" -->
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Intermediary Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/DealerName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Start Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate End Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Engine Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Chassis Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row >
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Manufacturer</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Make" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Model</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Model" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row  background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Variant</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Vehicle Registration Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Amount</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Amount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row   background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amout Of CGST (9%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CGST" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amout Of SGST (9%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SGST" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Total Amount (Including Tax)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amount In Words</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row   background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">GST No. of the Service Recipient</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Place Of Supply</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">SAC Code</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">First Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Last Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address1</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address2</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address3</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">City</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/City" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">State</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/State" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Mobile No.</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                      </fo:block>
                    </fo:table-cell>
                    <!--<fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Email ID</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Email" />
                      </fo:block>
                    </fo:table-cell>-->
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->


          <!--<fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block padding-top="5pt" font-size="5pt" padding-bottom="2pt" font-family="Franklin Gothic Demi" font-weight="bold">
                    <fo:inline color="#FF0000">* Conditions Apply</fo:inline>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                     Plan Features
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="7pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>
    
              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Plan Features</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">T799</fo:block>
                </fo:table-cell>
               
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>Towing of Vehicle on accident</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> YES(Upto 20 km G 2 G)</fo:block>
                </fo:table-cell>
               
              </fo:table-row>
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block>Tyre Change</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
                
              </fo:table-row>
              <fo:table-row >
                <fo:table-cell padding="2pt">
                  <fo:block> Arrangement of spare keys </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                  
                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
                
              </fo:table-row>
              <fo:table-row  background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block> Extraction or Removal of vehicle </fo:block>
                </fo:table-cell>
                 <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                    
                <fo:table-cell padding="2pt">
                  <fo:block>  YES </fo:block>
                </fo:table-cell>
               
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>Message relay to relatives/colleagues/emergency numbers</fo:block>
                </fo:table-cell>
                 <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                
                <fo:table-cell padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                  
                <fo:table-cell padding="2pt">
                  <fo:block>YES </fo:block>
                </fo:table-cell>
               
              </fo:table-row>
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block>Hotel Accomodation</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Assistance</fo:block>
                </fo:table-cell>
                
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>Alternate travel (Air/Rail Mode)</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Assistance</fo:block>
                </fo:table-cell>
                
              </fo:table-row>
              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block>X Ray</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>450</fo:block>
                </fo:table-cell>
               
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>OPD</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>450</fo:block>
                </fo:table-cell>
                
              </fo:table-row>
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block>Ambulance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>500</fo:block>
                </fo:table-cell>
                
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>Proposed No. of Services</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>2</fo:block>
                </fo:table-cell>
                
              </fo:table-row>
              
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Plan Features
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    1)Towing of Vehicle on Accident
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    In the event Covered Vehicle suffers an immobilizing break down due to a mechanical or electrical fault or an accident which cannot be repaired on the spot, Global Assure will assist in making arrangement for the Vehicle to be towed to the nearest Authorised Service Center, using tow trucks in the cities and corresponding covered area where available. (Free towing  to and Fro upto 20 KM)
                  </fo:block>
                 <fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>
                  <!--<fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>-->
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    2) Tyre Change
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    In the event Covered Vehicle is immobilized due to a flat tyre, Global Assure will assist the Insured by organizing for a vehicle technician to replace the flat tyre with the spare stepney tyre of the Vehicle at the location of breakdown. Global assure will bear labour cost and round-trip conveyance costs of the provider. Material/spare parts if required to repair the Vehicle (including repair of flat spare stepney tyre) will be borne by the Insured. In case the spare tyre is not available in the covered Vehicle, the flat tyre will be taken to the nearest flat tyre repair shop for repairs and re-attached to the Vehicle. All incidental charges for the same shall be borne by the Insured.(SERVICE AVAILABLE ONLY IN CASE OF ACCIDENT BREAKDOWN)
                  </fo:block>
                 <fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>
                  <!--<fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>-->
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    3)Arrangement of spare keys
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                   If the keys of the covered vehicle are locked inside the vehicle, broken, lost, or misplaced, Global Assure (upon the request of the Insured) will arrange for the forwarding of another set from his/her place of residence or office by courier / in person by hand-delivery to the location of the vehicle after receiving the requisite authorizations from the Insured with regards to the person designated to hand over the same to Global assure. The Insured may be requested to submit an identity proof at the time of delivery of the keys.(SERVICE AVAILABLE ONLY IN CASE OF ACCIDENT BREAKDOWN)
                  </fo:block>
                   <fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>
                  <!--<fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>-->
              
                
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    4) Message relay to relatives/colleagues/emergency numbers
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    Global Assure will take charge of relaying urgent messages relating to the breakdown to the authorized workshop and/or service contacts. When requested, Global assure will relay urgent messages on behalf of the Insureds to a designated person of their choice.
                  </fo:block>
                 <fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>
                  <!--<fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>-->
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
            
           <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    5) Hotel Accomodation and Travel
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    Global assure will provide the assistance to book the same
                  </fo:block>
                 <fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>
                  <!--<fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>-->
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>  
                        
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    6) Extraction or Removal of vehicle
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                   In event of vehicle being stuck in a ditch/pit/valley, Global Assure will make the arrangement to get the vehicle retrieved and towed to the nearest authorised service centre at no cost to the Insured.(Free towing  to and Fro upto 20 KM)
                  </fo:block>
                 <fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>
                 
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

    <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    7) X-Ray and OPD
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                  Outpatient Medical Expenses means and limited to OPD Consultancy Charges and X-Ray charges which should be prescribed by qualified doctor.
                  </fo:block>
                 <fo:block  text-align="justify"  font-size="6pt" padding-top="5pt" >
                    
                  </fo:block>
                 
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
   
    <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    8) No of services
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                 Insured will get 2 service covered under 12 Months of contract
                  </fo:block>
                
                 
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
                   
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Injury
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    "Injury" means accidental physical bodily harm excluding illness or disease solely and directly caused by external, violent, visible and evident means which is verified and certified by a Medical Practitioner.
                  </fo:block>
                
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    OPD treatment
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                     “OPD treatment” means the one in which the Insured visits a clinic / hospital or associated facility like a consultation room for diagnosis and treatment based on the advice of a Medical Practitioner. The Insured is not admitted as a day care or in-patient.
                  </fo:block>
                 
                
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                   Territorial Limits
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                    The coverage under each of the sections of the policy shall be restricted to the Territorial limits i.e. within the city . All claims shall be payable in India in Indian Rupees only.
                  </fo:block>

              
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
            
                

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Special Exclusions and Terms and Conditions
                  </fo:block>
                 
                </fo:table-cell>


              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                    The following scenarios are special exclusions under this Program, and therefore we will not be responsible for any assistance costs as a result of any of the following:
                  </fo:block>
                 
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold"  >
                   1) Vehicle is involved in motor racing, rallies, speed or duration tests, practice runs or operated outside official roads
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold">
                    2) Assistance is required as a result of wars, riots, uprising, mass political demonstrations, pillage, strike, use for military purposes or acts of terrorism, earthquake damage, freak weather conditions, atmospheric phenomena, nuclear transformation phenomena or radiation caused by artificial acceleration of atomic particles
                  </fo:block>
                 
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                   3) Accident is caused by deliberate damage, vandalism or participation in a criminal act or offence
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                  4) The immobilization is resulting from damage caused by intervention of the police or other authorities
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold">
                  5) Losses/Damages won’t be covered if the valid Insured  is found riding  two wheeler  without a valid license, without helmet  or if the bike’s driven by an underage person or any other violation of traffic rules.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                    6) Any damage resulting from the use of the vehicle against the recommendations of the owner manual
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                    7)  Any consequential costs and/or damage to property as a result of a breakdown
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                   8) In case the vehicle is being towed from one state to another or from one city to another, any local tax or toll tax, like green tax in Delhi-NCR and etc. has to borne by Insured.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                  9) On occasion of adverse weather conditions such as floods, heavy rain, thunder / lightening or other external factors may affect our ability to provide services and it may become physically impossible to assist you until the weather improves. During such times, our main priority will be to ensure that you and your passengers are taken to a place of safety; the recovery of your vehicle may not be possible until weather conditions permit.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
  
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                   10) The roadside assistance is a nationwide service provided to mobilize vehicle which has become disabled due to unexpected breakdown. While providing roadside assistance, we carry out temporary repairs and not regular maintenance which are normally carried out at authorized workshops. 
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
           <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                   11) The date of commencement of coverage under the program. The program start date will be after 3 (Three) days from the program purchase date.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
            
           <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                  12) The coverage end date of the program. The Roadside Assistance Program shall be valid for a period of 12 (twelve) months from the Program start date as mentioned in the program certificate.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
           <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                   13) In case of a breakdown, you must be able to provide us with the correct location of your covered vehicle. Incorrect or incomplete information may cause a delay in provision of our services. You or an authorized representative must be present with the vehicle at the agreed meeting place when we arrive. If the vehicle is not attended when we arrive services cannot be provided
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          
           <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold">
                  14) In case the covered vehicle is not on gazette, bitumen road and if there is a need to arrange special equipment (any equipment other than standard towing equipment), the charges of such equipment are not part of the roadside assistance program. The charges for arrangements of special equipment are to be borne by Insured. Insured should understand that in case of usage of special equipment’s covered vehicle might sustain certain direct or consequential damages which extracting the vehicle. Insured agrees that such damage shall be sole responsibility of the Insured and AWP shall not be held liable for any such damages.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
           <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold">
                   15) That the Entitlements are available after 3 days, all the benefits under the roadside assistance program became available 3 days after from the purchase date of the program.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
            <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold" >
                   16) In cases where the police authorities have registered an FIR or are in a process of registering an FIR or are investigating a case or have taken possession of the vehicle, Assistance services can be activated only after Police authorities/courts have given a clearance to the vehicle / have released the possession of vehicle. In such cases it is the duty of the Insured to obtain such clearances. AWP can take handover of the vehicle only after clearances have been obtained and the Insured / Insured’s representative is available to provide appropriate handover of the vehicle to the towing representative from the police authorities 
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="6pt" font-family="Franklin Gothic Demi" font-weight="bold">
                   17) We will take every effort to reach you once you make the call however the response time may vary depending on, among other things, the breakdown location of the vehicle and the general demand for roadside assistance at the time of your request is received.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
              
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Special Conditions (applicable to all coverage)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt"  font-family="Franklin Gothic Demi">
                    a)  All additional expenses regarding replacement of a part, additional Fuel and any other service which does not form a part of the standard services provided would be on chargeable basis to the insured.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" font-family="Franklin Gothic Demi" >
                    b) This Certificate is valid subject to realization of the payment and is effective from the Payment realization date or certificate issue date, whichever is later.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" font-family="Franklin Gothic Demi" >
                    c) Claim Servicing would be subject to realization of payment.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Outpatient Medical Expenses 
                  </fo:block>
                  
                  <fo:block text-align="justify"  font-size="6pt"  padding-top="3pt" >
                    Outpatient Medical Expenses means  X-Ray charges which should be prescribed by qualified doctor.
                  </fo:block>
                 
                
                
                </fo:table-cell>


              </fo:table-row>
            </fo:table-body>
          </fo:table>

          
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Ambulance Cost
                  </fo:block>
                  
                  <fo:block text-align="justify"  font-size="6pt"  padding-top="3pt" >
                   If any valid covered member sustains Injury during the period of active service, the company agrees to reimburse expenses upto the amount of INR 500 and incurred on a surface transport ambulance offered by registered healthcare or ambulance service provider used to transfer the Insured Person to the nearest hospital requiring emergency care or if advised by medical practitioner following an accident.
                  </fo:block>
                 
                  <fo:block text-align="justify"  font-size="6pt"  padding-top="3pt" >
                   The Sum Insured limit applicable for Ambulance Cost Benefit shall be payable on per event basis
                  </fo:block>
                  
                
                
                </fo:table-cell>


              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:block padding-top=".5cm" text-align="center">

            <fo:external-graphic src="QrCode.jpg" />

          </fo:block>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
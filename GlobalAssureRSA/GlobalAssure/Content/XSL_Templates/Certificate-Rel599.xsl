﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="first-page"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
        <!--<fo:simple-page-master master-name="masterTwo"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="second-header" extent="4cm"/>
          <fo:region-after region-name="second-footer" extent="2cm"/>

        </fo:simple-page-master>-->
        <!--<page-sequence-master master-name="pages">
          <repeatable-page-master-alternatives>
            <conditional-page-master-reference page-position="first" master-reference="first-page"/>
            <conditional-page-master-reference master-reference="masterTwo"/>
          </repeatable-page-master-alternatives>
        </page-sequence-master>-->

      </fo:layout-master-set>



      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="first-page" font-size="7pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1.2cm"/>
          </fo:block>

        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Registered Address </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="260pt"/>
                      <fo:table-column column-width="260pt"/>
                      
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">F-206, SATYA SHANTI APARTMENT, SECTOR-13, ROHINI, DELHI, North West Delhi, Delhi, 110085 </fo:block>
                          </fo:table-cell>
                          
                          <fo:table-cell>
                            <fo:block text-align="right">

                              3rd Floor, AIHP Horizon, 445, Phase 5, Udyog Vihar, Gurgaon -122016
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      
                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>
                          
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left"  padding-bottom="2pt" text-indent="0pc" space-after="40pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!-- <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Certificate Cum Invoice</fo:block>
                          </fo:table-cell>
                          
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt" color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%" font-size="7pt">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Tax Invoice
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left" text-indent="0pc">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="5pt" ext-indent="0pc" space-after="20pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    This is to certify that Vehicle with the following details is covered under Assistance Program, as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--Dynamic Content When State IS NOT UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table layout="fixed" width="100%" font-size="6pt">
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-header></fo:table-header>
                <fo:table-body xsl:use-attribute-sets="myBorder">
                  <!-- xsl:use-attribute-sets="myBorder" -->
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block>Certificate Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                      </fo:block>
                    </fo:table-cell>
                    <!--<fo:table-cell padding="2pt">
                      <fo:block>Intermediary Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/DealerName" />
                      </fo:block>
                    </fo:table-cell>-->                   
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block>Certificate Start Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Certificate End Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block>Engine Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Chassis Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row >
                    <fo:table-cell padding="2pt">
                      <fo:block>Manufacturer</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Make" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Model</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Model" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row  background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block>Variant</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Vehicle Registration Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block>Plan Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Plan Amount</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Amount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block>Amount Of Tax IGST (18%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/IGST" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Total Amount (Including Tax)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block>GST No. of the Service Recipient</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Amount In Words</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block>Place Of Supply</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>SAC Code</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block>First Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Last Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block>Address1</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Address2</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block>Address3</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>City</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/City" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block>State</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/State" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>Mobile No.</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <!--<fo:table-cell padding="2pt">
                      <fo:block>Email ID</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Email" />
                      </fo:block>
                    </fo:table-cell>-->
                    <fo:table-cell padding="2pt">
                      <fo:block></fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block> </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table layout="fixed" width="100%" font-size="6pt">
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-header></fo:table-header>
                <fo:table-body xsl:use-attribute-sets="myBorder">
                  <!-- xsl:use-attribute-sets="myBorder" -->
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                      </fo:block>
                    </fo:table-cell>
                    <!--<fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Intermediary Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/DealerName" />
                      </fo:block>
                    </fo:table-cell>-->
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Start Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate End Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Engine Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Chassis Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row >
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Manufacturer</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Make" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Model</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Model" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row  background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Variant</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Vehicle Registration Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Amount</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Amount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row   background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amout Of CGST (9%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CGST" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amout Of SGST (9%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SGST" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Total Amount (Including Tax)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amount In Words</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row   background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">GST No. of the Service Recipient</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Place Of Supply</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">SAC Code</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">First Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Last Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address1</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address2</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address3</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">City</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/City" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">State</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/State" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Mobile No.</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                      </fo:block>
                    </fo:table-cell>
                    <!--<fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Email ID</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Email" />
                      </fo:block>
                    </fo:table-cell>-->
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Featured Benefits
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="7pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>
              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Serial Number</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Featured Benefits</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Type of Service</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Basic Plan(Hatchback Cars)</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> Coverage radius/ Scope</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> N A</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block> 2 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> Breakdown Support over Phone </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> On-phone Assistance </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row >
                <fo:table-cell padding="2pt">
                  <fo:block> 3 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> On site Minor Repair</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> On-site Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row  background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block> 4 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> Replacement of Keys </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> On-site Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:block>NA</fo:block>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 5 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> Locked/Lost Keys </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> On-site Assistance </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>NA</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block>6</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Flat tyre Support</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>On-site Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>7</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Battery Jump-Start</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>On-site Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block>8</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Fuel Delivery</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>On-site Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>NA</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>9</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Wrong Fuelling</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>On-site Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>NA</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block>10</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">

                  <fo:block>Cost of Fuel</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>On-site Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Payable</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>11</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Towing for mechanical and electrical breakdown one way</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Towing Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>NA</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block>12</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Towing for accidental cases one way</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Towing Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>NA</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>13</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Taxi Benefit in case of major breakdown</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Mobility Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>NA</fo:block>
                </fo:table-cell>
              </fo:table-row>


              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block>14</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Simple medical assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Mobility Assistance</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>15</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Insured satisfaction survey</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Insured Service</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>


              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block>16</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Toll free number / hotline</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Insured Service</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>17</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Proposed No. of Services</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>1</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>24X7 RSA (MRP Without Taxes)</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Pan India, 24X7</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>INR599</fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Product Coverage
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    1) No.of Services(Basic Plan (Hatchback Cars))
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Only One service is given to Insured
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    2) Towing Coverage(Basic Plan(Hatchback Cars))
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Towing Not Available
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="first-page" font-size="7pt" >
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    3) Alternate Battery or Jump Start
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    In the event Covered Vehicle is immobilized, while on trip, due to rundown battery, Global Assure will assist the Insured by organizing for a Vehicle technician to jump start the Vehicle with appropriate means. Global assure will bear labour and conveyance costs. If the run down battery has to be replaced with a new battery, the cost of such battery replacement and any costs to obtain the battery will be borne by the Insured.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    4) Tyre Change
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    In the event Covered Vehicle is immobilized due to a flat tyre, Global Assure will assist the Insured by organizing for a vehicle technician to replace the flat tyre with the spare stepney tyre of the Vehicle at the location of breakdown. Global assure will bear labour cost and round-trip conveyance costs of the provider. Material/spare parts if required to repair the Vehicle (including repair of flat spare stepney tyre) will be borne by the Insured. In case the spare tyre is not available in the covered Vehicle, the flat tyre will be taken to the nearest flat tyre repair shop for repairs and re-attached to the Vehicle. All incidental charges for the same shall be borne by the Insured.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    5) Breakdown support
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    In the event Covered Vehicle breaks down due to a minor mechanical / electrical fault / accident and immediate repair on the spot is deemed possible within the scope of services, Global Assure shall assist Insured by arranging for a vehicle technician to reach the breakdown location. Global assure will bear labour cost and conveyance costs. Cost of Material and Spare Parts if required to repair the vehicle on the spot and any other incidental conveyance to obtain such material and spare parts will be borne by the Insured.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    6) Message relay to relatives/colleagues/emergency numbers
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Global Assure will take charge of relaying urgent messages relating to the breakdown to the authorized workshop and/or service contacts. When requested, Global assure will relay urgent messages on behalf of the Insureds to a designated person of their choice.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    7) Legal advice and Medical coordination
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    If requested, Global Assure will assist the Insured in obtaining access to legal / medical advice or treatment. Global assure’s team will refer the Insured to the nearest appropriate medical centre for treatment. Consultation and treatment fees are the responsibility of the Insured. This service will be on a referral basis and all payments shall be made directly by the Insured to the provider.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    8) Medical Assistance
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    A simple medical assistane will be provided
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Terms and Conditions :
                  </fo:block>
                </fo:table-cell>


              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    1) City Tax and Other Charges
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    In case the vehicle is being towed from one state to another or from one city to another, any local tax or toll tax, like green tax in Delhi-NCR and etc. has to borne by Insured.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    2) Adverse Weather
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    On occasion of adverse weather conditions such as floods, heavy rain, thunder / lightening or other external factors may affect our ability to provide services and it may become physically impossible to assist you until the weather improves. During such times, our main priority will be to ensure that you and your passengers are taken to a place of safety; the recovery of your vehicle may not be possible until weather conditions permit.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    3) Coverage
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    The roadside assistance is a nationwide service provided to mobilize vehicle which has become disabled due to unexpected breakdown. While providing roadside assistance, we carry out temporary repairs and not regular maintenance which are normally carried out at authorized workshops.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    4) Program Start Date
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    the date of commencement of coverage under the program. The program start date will be after 3 (Three) days from the program purchase date.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    5) Program End Date
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    The coverage end date of the program. The Roadside Assistance Program shall be valid for a period of 12 (twelve) months from the Program start date as mentioned in the program certificate.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    6) Un-located or Unattended Vehicle
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    In case of a breakdown, you must be able to provide us with the correct location of your covered vehicle. Incorrect or incomplete information may cause a delay in provision of our services. You or an authorized representative must be present with the vehicle at the agreed meeting place when we arrive. If the vehicle is not attended when we arrive services cannot be provided
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    7) Covered Vehicle is off road
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    In case the covered vehicle is not on gazette, bitumen road and if there is a need to arrange special equipment (any equipment other than standard towing equipment), the charges of such equipment are not part of the roadside assistance program. The charges for arrangements of special equipment are to be borne by Insured. Insured should understand that in case of usage of special equipment’s covered vehicle might sustain certain direct or consequential damages which extracting the vehicle. Insured agrees that such damage shall be sole responsibility of the Insured and AWP shall not be held liable for any such damages.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    8) Cooling Period
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    That the Entitlements are available after 3 days, all the benefits under the roadside assistance program became available 3 days after from the purchase date of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    9) Accidental cases
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    In cases where the police authorities have registered an FIR or are in a process of registering an FIR or are investigating a case or have taken possession of the vehicle, Assistance services can be activated only after Police authorities/courts have given a clearance to the vehicle / have released the possession of vehicle. In such cases it is the duty of the Insured to obtain such clearances. AWP can take handover of the vehicle only after clearances have been obtained and the Insured / Insured’s representative is available to provide appropriate handover of the vehicle to the towing representative from the police authorities.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    10) External Factors
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    We will take every effort to reach you once you make the call however the response time may vary depending on, among other things, the breakdown location of the vehicle and the general demand for roadside assistance at the time of your request is received.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Special Conditions (applicable to all coverage)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    a)  All additional expenses regarding replacement of a part, additional Fuel and any other service which does not form a part of the standard services provided would be on chargeable basis to the insured.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    b) This Certificate is valid subject to realization of the payment and is effective from the Payment realization date or certificate issue date, whichever is later.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    c) Claim Servicing would be subject to realization of payment.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="first-page" font-size="7pt" >
        <!--<fo:static-content flow-name="page-footer">
        <fo:block text-align="center">
          
      <fo:external-graphic src="letterfooter.jpg" width="25cm" scaling="uniform"/>
      
        </fo:block>
      </fo:static-content>-->

        <fo:flow flow-name="xsl-region-body">

          <fo:block text-align="center">
            <fo:external-graphic src="Reliance.PNG" width="5cm"/>
          </fo:block>


          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt"  >
                  <fo:block text-align="center" font-size="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    Insurance Details
                  </fo:block>
                  <fo:block border-top-style="solid" text-align="center"></fo:block>

                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table table-layout="fixed" width="100%" border-width="1pt"
    border-style="solid" font-size="7pt" >
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">




            </fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Master Policy No : GPA/I3559379/23/03/005898
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Certificate No : <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Name of Master Policy Holder : GLOBAL ASSURE
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Period of Insurance :From: <xsl:value-of select="GenerateCertificateModel/CoverStartDate" /> To:<xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Name of Nominee : NOMINEE TEST



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Relationship of nominee with insured : MOTHER



                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="10pt">
                  <fo:block>

                  </fo:block>

                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>

          </fo:table>
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" >
                    Sum Insured: Rs.15,00,000/- (Accidental Death and Permanent Total Disability). Cover is valid only while the insured is driving the vehicle covered under the Road Side Assistance plan mentioned above
                  </fo:block>


                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="first-page" font-size="7pt">

        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Policyholder Global Assure
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc"  >
                    Insured person
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" text-indent="2pc">
                    All Two wheeler Customers of GLOBAL ASSURE. (Age : - 18 to 70 years)1 year - This Policy is for a period of one year from Policy Start Date till Policy End date. Coverage Personal Accident
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt"  space-before.minimum="6pt" space-before.optimum="8pt"
                             space-before.maximum="10pt">
                    Accidental Death (AD)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    if such injury shall within twelve calendar months of its occurrence be the sole and direct cause of the death of the Insured Person
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Permanent Total Disability (PTD)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    Sight of both eyes / Physical separation of Two entire hands / Physical separation of Two entire Feet / Physical separation of One entire hand and one entire foot / or of such loss of sight of one eye and such loss of one entire hand or one entire foot Use of two hands or two feet /One hand and one foot / Loss of sight of one eye and such loss of use of one hand or one foot , If such injury shall, as a direct consequence thereof, immediately, permanently, totally and absolutely, disable the Insured Person from engaging in being occupied with or giving attention to any employment or occupation of any description
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Rate
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="4pc" space-after="7pt"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt" >
                    Rs.15 per lac per person (Excluding GST )
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Condition
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    The policy will be valid to all Two wheeler customers of GLOBAL ASSURE
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    In case of Claim payment under Accidental Death (AD) or Permanent Total Disabilities (PTD) there will be no further claim accepted either for AD or PTD.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    The Company's total liability for an Individual in aggregate shall not exce d Individual Sum Insured issued in a single policy irrespective of no of Covers issued. Given

                    such scenario the first policy will be taken in to consideration
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    In case of any claim made under the policy no premium shall be refunded on cancellation of the policy.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    In the event of any incorrect representation, the liability shall be upon the Policyholder.
                  </fo:block>
                </fo:table-cell>

              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c" text-indent="0pc"
            space-before.minimum="6pt" space-before.optimum="8pt"
            space-before.maximum="10pt">
                    Exclusions
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" text-indent="6pc" >
                    Lives involved in the below mentioned activities/occupation shall be outside the scope of the policy:-
                  </fo:block>
                  <fo:block text-align="justify" >
                    Payment of benefits shall not be available in respect of death, injury or disablement directly or indirectly arising out of or contributed to by or traceable to any disability existing on the date of issue of the policy.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >


                    Intentional self-injury, suicide or attempted suicide or whilst under the influence of intoxicating liquor or drugs.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    Venereal disease or insanity.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    Servicing -on duty with any armed forces.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    War, war-like situation, invasion or in consequence thereof or nuclear risk.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    Payment of compensation in respect of death, permanent total disablement for Insured person arising or resulting from the Insured Person committing any breach of law with criminal intent.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    Crew of aircraft and ship; naval, military, airforce personnel, policemen, firemen, fishermen are excluded from scope of this policy.
                  </fo:block>

                  <fo:block text-align="justify"  font-size="7pt" >
                    Any loss sustained while performing or participating in any of the following occupations or events shall not be covered - Working in mines, explosives, electrical installations on high tension electric lines,racing, circus personnel, skiing, mountaineering, hunting, gliding, river hockey, polo and occupations of similar hazard.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:block padding-top=".5cm" text-align="center">
            <fo:external-graphic src="QrCode.jpg" />

          </fo:block>
        </fo:flow>
      </fo:page-sequence>

    </fo:root>
  </xsl:template>




  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>

﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="1cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >

          <fo:block text-align="left">
            <xsl:value-of select="GenerateCertificateModel/CreatedDateTimeRSA" />
          </fo:block>
          
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header>
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block text-align="left">
                    <fo:external-graphic src="GA_Logo_Large.jpg" width="3.5cm" height="1cm"/>
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block text-align="right">
                    <fo:external-graphic src="Finask.jpg" width="3.5cm" height="1cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->           
          </fo:block>
          <fo:block text-align="center" font-size="8pt">           
            Please visit Our website <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">www.globalassure.com</fo:inline> for any claim assistance / 24*7 assistance.
          </fo:block>
          <fo:block text-align="center" font-size="8pt">
            Email : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">support@globalassure.com</fo:inline>  |  Contact : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">0124-4092900</fo:inline> (24*7 assistance)
        </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Registered Address </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="260pt"/>
                      <fo:table-column column-width="260pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">F-206, SATYA SHANTI APARTMENT, SECTOR-13, ROHINI, DELHI, North West Delhi, Delhi, 110085 </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              3rd Floor, AIHP Horizon, 445, Phase 5, Udyog Vihar, Gurgaon -122016
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>






          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>

                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="0pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Tax Invoice</fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="0pt">
                    This is to certify that Pedal cycle / Electric Cycle with the following details is covered under Assistance Program, as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding="4pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Your Cycle Package Cover
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify" padding-top="0pt">
                    Dear <xsl:value-of select="GenerateCertificateModel/FirstName" />, Congratulations on the purchase of Pedal Cycle / Electric Cycle assistance package cover from Global Assure. As a valued customer, this package is uniquely designed for your needs. Here is the the summary of inclusions and exclusions.
                  </fo:block>                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        CERTIFICATE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" >
                                      <fo:block>
                                        Certificte Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Plan Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate Start Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate End Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Place Of Supply
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        SAC Code
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        PEDAL CYCLE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Make
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Make" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Model
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Model" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Variant
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Colour of Cycle
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Colour" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Invoice Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InvoiceDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Chassis Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PERSONAL DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="1pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        First Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Last Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Mobile No
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Email ID
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Email" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Address1
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InsuredAddressLine1" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address2
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InsuredAddressLine2" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address3
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InsuredAddressLine3" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        State
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/State" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        City
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/City" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PAYMENT DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--Dynamic Content When State IS NOT UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="1pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amount Of Tax IGST (18%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/IGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->

          <!--Dynamic Content When State IS UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="1pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of CGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/CGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of SGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/SGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->

          
          <!--<fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block padding-top="5pt" font-size="5pt" padding-bottom="2pt" font-family="Franklin Gothic Demi" font-weight="bold">
                    <fo:inline color="#FF0000">* Conditions Apply</fo:inline>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="8pt">What's Covered (Inclusions) ?</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="3pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell font-family="Franklin Gothic Demi" font-weight="bold">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="7pt">Complimentary Cycle theft or damage as well as accident-related death or disability insurance.</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="3pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold" text-align="left">Serial No.</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold" text-align="left">Plan Features</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold" text-align="center">Pedal Cycle</fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    24*7 Phone Support
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">
                    YES 
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left"> 2 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Doctor Consultation
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center"> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 3 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Message relay to relatives/colleagues/emergency numbers
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center"> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left"> 4 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Locating Nearest Police Station
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">
                    <fo:block>YES</fo:block>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 5 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Ambulance Referral
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">6</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Emergency Taxi Arrangement
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">7</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Proposed No. of Services
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">2</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">8</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Complimentary Cycle Theft Insurance
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">9</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Complimentary Cycle Damage from fire
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">10</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Complimentary Permanent Disability/Accidental Death Insurance Rs.2,00,000
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>

          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="8pt">What's Not Covered (Exclusions) ?</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="3pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell font-family="Franklin Gothic Demi" font-weight="bold">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="7pt">Normal wear and tear, negligence is not covered.</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="3pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold" text-align="left">Serial No.</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold" text-align="left"></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold" text-align="center"></fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Wear and Tear of bicycle
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">
                    NO
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left"> 2 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Theft due to cyclist's negligence  
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center"> NO </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 3 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Damage due to cleaning/sevicing
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center"> NO </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left"> 4 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Expenses under hospitalisation/OPD
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">
                    <fo:block>NO</fo:block>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 5 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Accidents under the effects of intoxicants
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">NO</fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>

          </fo:table>

          <fo:block text-align="right">
            For GLB Assure Private Limited
          </fo:block>
          <fo:block text-align="right">
            <fo:external-graphic src="AuthSign.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
          <fo:block text-align="right">
            Authorized Signatory
          </fo:block>

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <!--<fo:static-content flow-name="page-footer">
        <fo:block text-align="center">
          
      <fo:external-graphic src="letterfooter.jpg" width="25cm" scaling="uniform"/>
      
        </fo:block>
      </fo:static-content>-->

        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <xsl:value-of select="GenerateCertificateModel/CreatedDateTimePA" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" height="3cm" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="IFFCO-TOKIO.jpg" width="5cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    IFFCO - TOKIO GENERAL INSURANCE CO. LTD
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table table-layout="fixed" width="100%" border-width="1pt"
    border-style="solid" font-size="7pt" >
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">




            </fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    <!--Master Policy No : 46010047199400000002 / 46010042190100000106-->
                    Master Policy No : <xsl:value-of select="GenerateCertificateModel/MasterPolicyNo" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Certificate No : <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Name of Master Policy Holder : GLB ASSURE PRIVATE LIMITED
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Period of Insurance :From: <xsl:value-of select="GenerateCertificateModel/CoverStartDate" /> To:<xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Name of Nominee : <xsl:value-of select="GenerateCertificateModel/NomineeName" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Relationship of nominee with insured : <xsl:value-of select="GenerateCertificateModel/NomineeRelationship" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Nominee Gender : <xsl:value-of select="GenerateCertificateModel/NomineeGender" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Insured Name : <xsl:value-of select="GenerateCertificateModel/InsuredName" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Insured Gender : <xsl:value-of select="GenerateCertificateModel/InsuredGender" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Insured DOB : <xsl:value-of select="GenerateCertificateModel/InsuredDOB" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Branch Code : <xsl:value-of select="GenerateCertificateModel/BranchCode" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
            </fo:table-body>

          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" >
                    Pedal Cycle/Electric Cycle: Insurance covers Pedal Cycle / Electric Cycle up to Rs.55,000/- only
                  </fo:block>
                 <fo:block text-align="justify"  font-size="7pt" >
                    Personal Accident: Sum Insured Rs. 2,00,000/- only                 
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Restriction of Coverage
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="0pt">
                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Coverage is applicable to persons with age from 5 years up to max 70 years
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Coverage in Brief
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    1. Pedal Cycle/Electric Cycle: -
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    The Company will Indemnify the Insured against loss or damage to any pedal cycle described in the Schedule hereto (including its accessories whilst thereon) by fire external explosion or lightning or burglary, housebreaking, larceny or theft. 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    *Excess will be 15% of Sum Insured for any burglary, housebreaking, larceny or theft claim 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    *Time Excess of 15 Days will be applicable for any burglary, housebreaking, larceny or theft claim 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    *FIR is mandatory for any fire external explosion or lightning or burglary, housebreaking, larceny or theft Claim 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    *Coverage only in India
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    2. Personal Accident                  
                  </fo:block>                  
                  <fo:block text-align="justify"  font-size="6pt">
                    *Sum Insured: 2 Lakhs per person
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    * Cover: Accidental Death+ Permanent Total Disability
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    * All terms and Condition as per Annexure "A"
                  </fo:block>        
                  </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                  Exclusions in Brief
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  Pedal Cycle / Electric Cycle: -
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Accidental external means              
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • RSMD           
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • STFI                 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Liability to Third Parties                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Consequential loss                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Nuclear, War and Radioactive                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                                     
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  Personal Accident:-                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Communicable Disease Exclusion Clause: - Losses or damages caused directly or indirectly due to any infectious or contagious disease, pandemic /epidemics as declared by WHO
                    and / or Government of India will be an exclusion under this policy as per the attached clause.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding="2pt">
                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    2) FIR a must for every theft loss
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    The coverage is as Per Policy Wordings / Endorsements / Clauses attached. Please go through your Policy and in case of any discrepancy, Please inform us. In case of cheque dishonor, policy stands
                    Cancelled ab-intio.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Toll Free: 1-800-103-5499 ; Other : ( 0124) 428-5499 ; SMS "claim" to 56161
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Service Tax No :AAACI7573HST001
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Policy Issuing Office: Delhi
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Consolidated Stamp Duty deposited as per the order of Government of National Capital Territory of Delhi.
                  </fo:block>                  
                  <fo:block text-align="justify"  font-size="7pt" font-weight="bold">
                    All Risk Insurance Policy Wordings
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    This Policy is evidence of the Contract between YOU and US. The proposal along with any written statement(s), declaration(s) of YOURS for purpose of this Policy forms part of this contract.
                    This Policy witnesses that in consideration of YOUR having paid the premium for the period stated in the Schedule or for any further period for which WE may
                    accept the payment for renewal of this Policy. WE will insure YOUR properties as specified in the Schedule during the period of Insurance and accordingly WE will indemnify YOU in respect of events occurring during the Period of Insurance in the manner and to the extent set forth in the Policy, provided that all the terms,conditions and exemptions of this Policy in so far as they relate to anything to be done or complied with by YOU have been met.
                    The schedule shall form part of this Policy and the term â€oePolicyâ€• whenever used shall be read as including the â€oeScheduleâ€•.
                    Any word or expression to which a specific meaning has been attached in any part of this Policy or of Schedule shall bear such meaning wherever it may appear. YOUR Policy is based on information which YOU have given US and the truth of these information shall be condition precedent to YOUR right to recover under this Policy.
                  </fo:block>                  
                  <fo:block text-align="justify"  font-size="7pt" font-weight="bold">
                    Definition of Words
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    1.Proposal
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means any signed proposal by filling up the questionnaires and declarations, written statements and any information in addition thereto supplied to US by YOU or on YOUR behalf.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    2.Policy
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means the Policy Booklet, the Schedule and any applicable endorsements or memoranda. YOUR policy contains the details of the extent of the cover available to YOU, what is excluded from the cover and the conditions, warranties on which the Policy is issued.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    3.Schedule
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means the latest schedule issued by US as part of YOUR Policy. It provides details of YOUR Policy including full description of properties covered which are in force and the period of cover YOU have against the properties described.
                    A Revised Schedule will be sent at each renewal and whenever YOU request for a change in the cover.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    4.Sum Insured
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means the Monetary Amounts shown against any item.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    5.WE/OURS/US
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means THE IFFCO-TOKIO GENERAL INSURANCE COMPANY LTD.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    6.YOU/YOUR
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means the person(s)/the Company/the entity named as Insured in the Schedule.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    7.Period of Insurance
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means the duration of the Policy as shown in the Schedule.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    8.Market Value
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means the Replacement Value of insured property or item as New at the time of Damage or Loss less due allowance for betterment, wear and tear and/or
                    depreciation.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    9.Loss/Lost
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means the Damage or Loss.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    10. Excess
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means the first part of any claim for which YOU are responsible. Any Sum Insured/Limit will apply after the Excess has been deducted.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    11.Money
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It means Cash, current coins, Bank and Currency Notes, Cheques, Postal Order, Current postage stamps which are not part of a collection and luncheon Vouch
                    Exclusion: Losses or damages caused directly or indirectly due to any infectious or contagious disease, pandemic /epidemics as declared by WHO and / or
                    Government of India will be an exclusion under this policy.
                  </fo:block>                                 
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>
      
      <fo:page-sequence master-reference="simple" font-size="7pt" >
      <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" height="3cm" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="IFFCO-TOKIO.jpg" width="5cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    IFFCO - TOKIO GENERAL INSURANCE CO. LTD
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    General Conditions
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    1.Reasonable Precaution and Care of Property
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    YOU shall take all reasonable precautions for safety and soundness of Insured Property and to prevent the loss in order to minimise claims. YOU must comply with Makerâ€™ recommended actions for inspection and maintenance and shall comply all statutory requirements or other regulations and will employ only competent and honest employees.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    2.Notice
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    YOU will give every notice and communication in writing to OUR office through which this insurance is affected.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    3.Misdescription
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    This Policy shall be void and all premium paid by YOU to US shall be forfeited in the event of misrepresentation, misdescription or concealment of any material information.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    4.Changes in Circumstances
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    YOU must inform US, as soon as possible, of any change in information YOU have provided to US about yourself, the properties insured, location of risk which may affect the insurance cover provided e.g. change of address, period of unoccupancy, security arrangements etc. YOU must also notify US about any alteration made whereby risk of loss/damage is increased. In case of such alteration made and not accepted by US in writing, the cover under this policy shall cease.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    5.Claim Procedure and Requirements
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Upon happening of an event giving rise or which may give rise to a claim
                    a)YOU or YOUR authorised representative shall forthwith give notice in writing to OUR nearest office with a copy to Policy issuing office with full particulars. A
                    written statement of the claim will be required and a claim form will be provided. This written statement of claim along with supporting documents (estimates, bill and the like) prepared at your expense along with particulars of other Insurances covering the same risk must be delivered to US within 14 days of date of Loss.
                    b)YOU shall lodge a complaint with the Police at the earliest after happening of the incident and take all practicable steps to apprehend the guilty person and recover the property lost.
                    c)You must also notify the Railways, Steamship Company, Airline, Hotel proprietors or the authority in whose care the property was at the time of happening of any loss or damage.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    6.Claim Control and subrogation
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    We are entitled to
                    a) Enter any place where Loss has occurred and deal with salvage but this does not mean that property can be abandoned to US.
                    b) Receive all information, proof of damage and assistance from YOU and any other person seeking benefit under the Policy.
                    c)Take proceedings at OUR own expenses and for OUR own benefit, but in YOUR name or in name of any other person who is claiming or has received benefit, for the purpose of enforcing any rights and remedies or obtaining relief or indemnity from other parties to which WE shall be or would become entitled or subrogated upon, to recover any payment made or due under this Policy.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    7.Fraud
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    If any claim under this Policy is fraudulent in any respect with or without YOUR knowledge or if any fraudulent means or devices are used by YOU or YOUR behalf to obtain any benefit under this Policy, all benefits and rights under the Policy shall be forfeited.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    8.Contribution
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    If at the time of happening of any loss covered by this Policy, there shall be existing any other Insurance of any nature covering the same property, whether effected by YOU or not, then WE will pay only rateable proportion.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    9.Average
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    The Insurance under this Policy (except as regards damage done to the Premises as stated in the Schedule) is subject to the following condition of Average. If the property hereby insured shall at the time of any loss or damage be collectively of greater value than the sum insured thereon, then you shall be considered as being your own insurer for the difference and shall bear a rateable proportion of loss accordingly. Every item if more than one of the policy, shall be separately
                    subject to this condition.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    10.Cancellation
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    WE may cancel this policy by sending 7 days’ notice in writing by Regd.A.D. to YOU at YOUR last known address. YOU will then be entitled to a pro-rata refund of premium for the unexpired period of this policy from the date of cancellation, which WE are liable to repay on demand. YOU may cancel this Policy by sending written Notice through Registered A.D. to US. WE will then allow a refund after the premium based on the following retaining table.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Short Period Period of Cover upto Refund of Annual Premium rate (%)
                  </fo:block>
                  
                  <fo:table table-layout="fixed" width="100%" border-style="solid" font-size="7pt">
                    <fo:table-column column-width="1.5in"/>
                    <fo:table-column column-width="1.5in"/>
                    <fo:table-header text-align="center">
                    </fo:table-header>
                    <fo:table-body>                                           
                      <fo:table-row>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            1 Month
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            0.75
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                      <fo:table-row>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            3 Month
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            0.5
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                      <fo:table-row>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            6 Month
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            0.25
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                      <fo:table-row>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            Exceeding Six Months
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            NIL
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                    </fo:table-body>
                  </fo:table>

                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    11. Arbitration
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Should any dispute arise between YOU and US on quantum of amount payable (liability being otherwise admitted by US), such dispute will independently of all other questions be referred to the decision of Arbitrator(s) in accordance with statutory provision of the country in force at that time. Further, when any dispute is referable or referred to Arbitration, the making of an award by Arbitrator(s) shall be a condition precedent to any right of action by YOU against US.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    12.Disclaimer Clause
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    If WE shall disclaim OUR liability in any claim, and such claim shall not have been made subject matter of a suit in a court of law within 12 months from the date of disclaimer, then the claim shall for all purposes be deemed to have been abandoned and shall not thereafter be recoverable under this Policy.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    13.Interest/Penalty
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    No sum payable under this policy shall carry any interest or penalty.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    14.Geographical Scope
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    The geographical scope of this policy will be India unless the policy has been specifically extended for worldwide coverage in which case the claims shall be settled in India in Indian rupees. The laws of India shall govern the provisions of this policy for the time being in force. The parties hereto unconditionally submit to the jurisdiction of the courts in India.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    15.Renewal Notice
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    WE shall not be bound to accept any renewal premium or give notice that such premium is due.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    Scope of Contract
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Coverage
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    If the Property insured belonging to YOU or for which YOU are responsible at law is damaged by any cause listed under â€oe What is Cover, then WE will indemnify YOU to the extent of value of loss of contents.
                  </fo:block>

                  <fo:table table-layout="fixed" width="100%" border-style="solid" font-size="7pt">
                    <fo:table-column column-width="3.7in"/>
                    <fo:table-column column-width="3.7in"/>
                    <fo:table-header text-align="center">
                    </fo:table-header>
                    <fo:table-body>
                      <fo:table-row>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            What is covered
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            What is not covered
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                      <fo:table-row>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            The ambit of this cover is Loss of or Damage to Property insured caused
                            by fire, riot and strike, terrorist activity, theft or accident from any
                            fortuitous cause any time during the period of Insurance.
                            Limit of Liability: - Our liability shall in no case exceed in respect of each
                            item the sum insured thereon or in the whole the total sum insured or such
                            other sum or sums as may be substituted there for, by any endorsement
                            during any one period of insurance.
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            WE will not be liable for
                            i)Damage caused by any process of cleaning, dyeing or bleaching, restoring, repairing
                            or renovation or deterioration arising from wear and tear, moths, insects, vermin,
                            mildew, inherent defect or any other gradually operating cause.
                            ii)Damage due to breakage, cracking or scratching of household goods, foodstuff,
                            domestic appliances, crockery, glass, cameras, binoculars, lenses, sculptures, curios,pictures, musical instruments, sports gear and similar articles of brittle or fragile nature unless caused by fire or accident to conveyance by which it is conveyed.                           
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                    </fo:table-body>
                  </fo:table>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%" margin="0mm">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" height="3cm" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="IFFCO-TOKIO.jpg" width="5cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" margin="0mm">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    IFFCO - TOKIO GENERAL INSURANCE CO. LTD
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">                                  

                  <fo:table table-layout="fixed" width="100%" border-style="solid" font-size="7pt">
                    <fo:table-column column-width="3.7in"/>
                    <fo:table-column column-width="3.7in"/>
                    <fo:table-header text-align="center">
                    </fo:table-header>
                    <fo:table-body>
                      <fo:table-row>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="1mm" border-style="solid">
                          <fo:block>
                            iii)Damage
                            a) To property insured caused by its undergoing any heating process or any process involving the application of heat.
                            b) Due to theft or attempted theft by or in connivance with you or your family.
                            c)Due to any person obtaining property by deception.
                            d)Mechanical or electrical breakdown or failure.
                            e) Market depreciation or improper maintenance.
                            f) To electrical equipment by its short circuiting or over-running not resulting into fire.
                            g) To gaming and amusement machines or other machines or equipment which is provided for operation other than by you.
                            h)Due to theft from any unattended vehicle unless the vehicle and all the doors, windows and other openings are securely locked and properly fastened.
                            iv)Damage to money, securities, manuscripts, deeds, bonds, bills of exchange, promissory notes, stocks or share certificates, stamps and travelers cheques,business books or documents.
                            v)Any living creature.
                            vi)Cost of remaking any film, disc, tape or the value of any information contained in it.
                            vii)Loss directly or indirectly occasioned by or happening through or in consequence of
                            war, Invasion act of foreign enemy, hostilities (whether war be declared or not), Civil
                            war, rebellion, revolution, Insurrection, Military or usurped power, Confiscation,
                            nationalization or loot pillage in connection therewith.
                            viii)Any Loss arising from or in consequence of requisition or destruction by or under
                            order of any Public Authority.
                            ix)
                            a) Loss to any property whatsoever or any expenses whatsoever resulting or arising
                            there from or any consequential loss.
                            b)Any legal liability of whatsoever nature; Directly or indirectly caused by or
                            contributed to by or arising from ionising radiation or contamination by radio activity
                            from any source whatsoever.
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                    </fo:table-body>
                  </fo:table>

                  <fo:block text-align="justify"  font-size="6pt" padding="2pt">

                  </fo:block>
                  <fo:block text-align="justify"  font-size="8pt" font-weight="bold">
                    Special Conditions
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    1.Reinstatement of Sum Insured
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    No sum insured or limit will be reduced following a claim, but the payment for total loss for any one article will extinguish the cover for that and overall sum insured will be adjusted accordingly, but no refund of premium will be allowed for the remainder of the period of insurance.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    2.Indemnity
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    WE may at OUR option reinstate, replace or repair the property or any part thereof or pay the amount of Loss/damage or may join with any other Insurer(s) in doing so, but WE shall not be bound to reinstate exactly or completely but only as circumstances permit and in reasonably sufficient manner and in no case WE shall be bound to expend more in reinstatement than it would cost to reinstate such property as it was at the time of occurrence of such damage, nor more than the Sum
                    Insured thereon. Upon payment of any claim under this policy, the property in respect of which the payment is made shall belong to us.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    3.Transfer of Interest
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    This Policy shall cease to attach to any property in which YOUR interest shall pass from YOU otherwise than by will or operation of law, Unless OUR consent to the continuance of the Insurance is obtained and signified on the Policy.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    4.Onus of Proof
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    In any action, suit or other proceedings where WE allege that by reason of the above provisions any damage is not covered by this Insurance, the burden of proving that such damage is covered shall be upon YOU.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    5.Single article limit
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Unless specifically and separately stated, our liability in respect of each article or pairs of articles shall not exceed 10% of the total sum insured under this policy.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    6.Articles in pairs or sets
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Where any item insured under this policy consists of articles in pair or set, our liability in respect of such items shall not exceed the value of any particular part or parts which may be lost or damaged without reference to any special value which such articles or articles may have as part of such pair or set nor more than a
                    proportionate part of insured value of pair or set.
                  </fo:block>

                  <fo:block text-align="justify"  font-size="8pt" font-weight="bold">
                    Policy Clauses/Endorsement Wordings
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Terrorism Damage Exclusion Endorsement
                    Terrorism / Terrorist Incident : Means any actual or threatened use of force or violence directed at or causing damage, injury, harm or disruption, or the commission of an act dangerous to human life or property, against any individual, property or government, with the stated or unstated objective of pursuing economic, ethnic, nationalistic, political, racial or religious interests, whether such interests are declared or not. Robberies or other criminal acts, primarily committed for personal gain and acts arising primarily from prior personal relationships between perpetrator(s) and victim(s) shall not be considered terrorist activity. Terrorism shall also include any act, which is verified or recognized by the relevant Government as an act of terrorism.
                    Terrorism / Terrorist Incident of whatsoever nature directly or indirectly caused by, resulting from or in connection with any act of terrorism regardless of any other cause or event contributing concurrently or in any other sequence to the loss.
                    Terrorism Damage Coverage Endorsemet
                    Notwithstanding anything stated to the contrary It Is hereby declared and agreed that Terrorism Damage Exclusion Endorsement stands deleted if Terrorism Damage coverage endorsement is mentioned on the policy schedule and Terrorism / Terrorist Incident is held covered but excludes Nuclear / Chemical /
                    Biological Terrorism Attack.
                    Electrical / Mechanical breakdown Extension.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    This Endorsement is applicable only when mentioned on the schedule of the policy
                  </fo:block>

                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    1.Sum Insured
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It is a requirement of this insurance that the Sum Insured shall be equal to the cost of replacement of the insured property by new property of the same kind and same capacity, which shall mean its replacement cost including freight, dues and customs duties, if any and erection costs.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" font-weight="bold">
                    2.Basis of Indemnity
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    a) In cases where an item can be repaired the Company will pay expenses necessarily incurred to restore the damaged machine to its former state of serviceability plus the cost of dismantling and re-erection incurred for the purpose of effecting the repairs as well as ordinary freight to an from a repair shop customs duties and dues if any, to the extent such expenses have been included in the Sum Insured. If the repairs are executed at a workshop owned by the Insured, the Company will pay the cost of materials and wages incurred for the purpose of the repairs plus a reasonable percentage to cover overhead charges. No deduction shall be made for depreciation in respect of parts replaced, except those with limited life, but the value of any salvage will be taken into account. If the cost of repairs as detailed herein above equals or exceeds the actual value of the machinery insured immediately before the occurrence of the damage, the settlement shall be made on the basis provided for in (b) below.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    b)In cases where an insured item is a total loss, the Company will pay the actual value of the item immediately before the occurrence of the loss, including costs for ordinary freight, erection and customs duties if any, provided such expenses have been included in the sum insured, such actual value to be calculated by deducting proper depreciation from the replacement value of the item. The Company will also pay any normal charges for the dismantling of the machinery destroyed, but the salvage will be taken into account.
                    Any extra charges incurred for overtime, night-work, work on public holidays, express freight, are covered by this insurance only if especially agreed to in writing; In the event of the Maker's drawings, patterns and core boxes necessary for the execution of a repair not being available the Company shall not be liable for the cost of making any such drawings, patterns and core boxes.
                    The cost of any alterations, improvements or overhauls shall not be recoverable under this policy.
                    The cost of any provisional repairs will be borne by the Company if such repairs constitute part of the final repairs, and do not increase the total repair expenses.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    c)In cases where the insured item is subjected to total loss and meanwhile it becomes obsolete, all costs necessary to replace the lost or damaged insured item with a follow-up model (similar type) of similar structure / configuration (of similar quality) i.e. low, average or high capacity â€“ will be reimbursed. If the sum insured is less than the amount required to be insured as per Provision-1 hereinabove, the Company will pay only in such proportion as the sum insured bears to the amount required to be insured. Every item if more than one shall be subject to this condition separately.
                    The Company will make payments only after being satisfied, by production of the necessary bills and documents, that the repairs have been effected or replacements have taken place, as the case may be.
                    The Company may, however, not insist for bills and documents in case of total loss where the insured is unable to replace the damaged equipment for reasons beyond their control. In case of total loss where the insured is unable to replace the damaged equipment for reasons beyond their control. In such cases claims can be settled on â€˜Indemnity Basisâ€™.
                  </fo:block>                                   
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <fo:static-content flow-name="page-footer">         
          <fo:block text-align="center" font-size="8pt">
            Corporate Office : IFFCO Tower, Plot No. 3, Sector 29, Gurgaon - 122001, Haryana, India.
          </fo:block>         
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%" margin="0mm">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" height="3cm" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="IFFCO-TOKIO.jpg" width="5cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" margin="0mm">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    IFFCO - TOKIO GENERAL INSURANCE CO. LTD
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  
                  <fo:block text-align="justify"  font-size="6pt">
                    3.Reinstatement Premium At all times during the period of insurance of this Policy the insurance cover will be maintained to the full extent of the respective sum insured in consideration of which upon the settlement of any loss under this policy, pro-rata premium for the unexpired period from the date of such loss to the expiry of period of insurance for the amount of such loss shall be payable by the insured to the Company.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    The additional premium referred above shall be deducted from the net claim amount payable under the policy. This continuous cover to the full extent will be available notwithstanding any previous loss for which the company may have paid hereunder and irrespective of the fact whether the additional premium as mentioned above has been actually paid or not following such loss. The intention of this condition is to ensure continuity of the cover to the insured subject only to the right of the company for deduction from the claim amount, when settled, of pro-rata premium to be calculated from the date of loss till expiry of the policy.
                    Notwithstanding what is stated above, the Sum Insured shall stand reduced by the amount of loss in case the Insured immediately on occurrence of the loss exercises his option not to reinstate the Sun Insured as above.
                  </fo:block>
                                   
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>         

        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>

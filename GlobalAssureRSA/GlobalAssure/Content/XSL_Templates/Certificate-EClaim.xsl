﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Calibri">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
          <fo:block text-align="center" font-size="10pt" font-weight="bold" padding-top="5pt">
            E Claim Form For Motor Vehicle
          </fo:block>
        </fo:static-content>
        <!--<fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            -->
        <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
        <!--
          </fo:block>
        </fo:static-content>-->
        <fo:flow flow-name="xsl-region-body">

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="15pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="5pt" font-family="Calibri" border-width=".5pt" border-style="solid">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold">
                    INFORMATION ABOUT INSURED : POLICY/ COVER NOTE NO : 3001/196124200/00/000 <!--<xsl:value-of select="ClsEClaim/PolicyNo"/>-->
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold">
                    CLAIM NO. : MOT09883513
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Name : DYNACORP ENGINEERING PRIVATE LIMITED
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Correspondence Address : 1;2;3 VIJAY KIRAN APPTS;; TIDKE COLONY ROAD;
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Pin Code : 422002
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Res. Tel. No : 9999999999
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Off. Tel. No : 9999999999
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Mobile : 7045905235
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    EMail Id : ayaz.saadan@icicilombard.com
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>             
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="10pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="2pt" font-family="Calibri" border-width=".5pt" border-style="solid">
            <fo:table-column column-width="2in"/>
            <fo:table-column column-width="2in"/>
            <fo:table-column column-width="2in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm" number-columns-spanned="3">
                  <fo:block font-weight="bold">
                    INFORMATION ABOUT INSURED VEHICLE :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Registration No : MH15FT3027
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Make : HONDA
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Model :JAZZ 1.2 V CVT
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Date of Registration : 21/05/2020 00:00:00
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Chassis No : MAKGK785AH4106797
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Engine No : L12B42020358
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Class of Vehicle : Two Wheeler
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="5pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="2pt" font-family="Calibri">
            <fo:table-column column-width="6in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold" >
                    DETAILS ABOUT THE DRIVER (At time of accident)
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold">
                    Name : Parked
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Driving license number. :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Date of expiry : 21 May 2020
                  </fo:block> 
                </fo:table-cell>
              </fo:table-row>            
              
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="5pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="2pt" font-family="Calibri" border-width=".5pt" border-style="solid">
            <fo:table-column column-width="6in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold">
                    DETAILS OF ACCIDENT :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Date : 22/May/2020      Time : 09:53
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Exact location of accident (Address / Spot of Accident with landmark) : Kurla, mumbai
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Give brief description of the accident : Hit from rear
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Was any third party responsible / liable for the accident? : Yes/No
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    If yes, please provide a copy of FIR Details :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="5pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="2pt" font-family="Calibri">
            <fo:table-column column-width="6in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold">
                    DETAILS OF GARAGE :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Garage Name :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Garage Address :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Garage Phone Number :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Garage Contact Person :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="5pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="2pt" font-family="Calibri">
            <fo:table-column column-width="6in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold">
                    THIRD PARTY INJURY / THIRD PARTY VEHICLE DAMAGE : 
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    i) Name :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    ii) Address :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    iii) Full details of personal / vehicle damaged :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="5pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="2pt" font-family="Calibri">
            <fo:table-column column-width="6in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold">
                    WITNESS DETAILS (FOR THEFT AND THIRD PARTY INJURY / DAMAGE) :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    i) Give name and address of witness (if any) :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Correspondence Address :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Res. Tel. No. :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Mobile :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    ii) Was accident reported to Police? : Yes/No
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    iii) If yes to which Police station? :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    iv) FIR No. / CR Dairy Number :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    v) Name of attending inspector :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="5pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="2pt" font-family="Calibri">
            <fo:table-column column-width="2in"/>
            <fo:table-column column-width="2in"/>
            <fo:table-column column-width="2in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm" number-columns-spanned="3">
                  <fo:block font-weight="bold">
                    PARTIAL / TOTAL THEFT :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    i) Date : 21May2020
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Time : 21:21
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    ii) Place of theft :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>                             

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
          <fo:block text-align="center" font-size="10pt" font-weight="bold">
            E Claim Form For Motor Vehicle
          </fo:block>
        </fo:static-content>
        <!--<fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            -->
        <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
        <!--
          </fo:block>
        </fo:static-content>-->
        <fo:flow flow-name="xsl-region-body">

          <fo:table table-layout="fixed" width="100%" font-size="8pt" font-family="Calibri" padding-top="30pt">
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block font-weight="bold" font-family="Calibri">
                    I/We hereby agree, affirm and declare that :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row padding-top="5pt">
                <fo:table-cell padding="1mm">
                  <fo:block>
                    • The statements/information given/stated by me, us in this claim form are true, corrected and complete.
                  </fo:block>
                  <fo:block>
                    • The details of all persons having an interest in the property in respect of which the claim is being made are provided as per the proposal form or by way of an
                      endorsement in the policy. Furthermore, save and except as provided or disclosed in this claim form, no claim made hereunder (for the same/similar claim) has made or
                      lodged with any other insurance company.
                  </fo:block>
                  <fo:block>
                    • No material information, which is relevant to the processing of the claim, which in any manner has a bearing on the claim, has been withheld or not disclosed.
                  </fo:block>
                  <fo:block>
                    • If I/We have given/made any false or fraudulent statement /information, or suppressed or concealed or in any manner failed to disclose all information, the policy shall be
                      void and that I/We shall not be entitled to all/any rights to recover there under in respect of any or all claims, past, present or future.
                  </fo:block>
                  <fo:block>
                    • The receipt of this claim form / other supporting/related documents does not constitute or be deemed to constitute an agreement by the Company of the claim and the
                      Company reserves the right to process or reject or require further/additional information in respect of the claim.
                  </fo:block>
                  <fo:block>
                    • I/We will not take input credit of the service tax paid by ICICI Lombard General Insurance Company Ltd. in settlement of this motor insurance claim.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
                                  
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="10pt" padding-top="20pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Place : Mumbai
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    Date : 22/05/2020
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="10pt" padding-top="20pt" font-family="Calibri">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block>
                   
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" text-align="right">
                  <fo:block text-align="right">
                    For GLB Assure Private Limited
                  </fo:block>
                  <fo:block text-align="right">
                    <fo:external-graphic src="AuthSign.jpg" width="5cm" height="2cm"/>
                  </fo:block>
                  <fo:block text-align="right">
                    Authorized Signatory
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
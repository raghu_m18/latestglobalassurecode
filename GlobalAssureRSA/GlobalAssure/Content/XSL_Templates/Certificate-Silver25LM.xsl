﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Registered Address </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="260pt"/>
                      <fo:table-column column-width="260pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">F-206, SATYA SHANTI APARTMENT, SECTOR-13, ROHINI, DELHI, North West Delhi, Delhi, 110085 </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              3rd Floor, AIHP Horizon, 445, Phase 5, Udyog Vihar, Gurgaon -122016
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>

                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="0pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Tax Invoice</fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="0pt">

                    This is to certify that the subscriber is covered under the wellness program..., as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        CERTIFICATE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" >
                                      <fo:block>
                                        Certificate Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                          Crisis Assistance Membership No
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CrisisAssistNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Plan Start Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Plan End Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Insurance Confirmation No
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InsurnceConfirmationNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>                                 

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        PRODUCT DETAILS
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Wellness Cover
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        Assistance
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>                                
                                  
                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Diagnosis Cover
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        Yes
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Quarantine Cover
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/PrimaryCover" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Travel Excl Cover
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/SecondaryCover" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PERSONAL DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        First Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Last Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Mobile No
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Email ID
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Email" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Address1
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address2
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        State
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/State" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        City
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/City" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PAYMENT DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--Dynamic Content When State IS NOT UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amount Of Tax IGST (18%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/IGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->

          <!--Dynamic Content When State IS UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of CGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/CGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of SGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/SGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->          

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="#32419c">
                    GLOBAL ASSURE PLAN BENEFITS :
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    ASSISTANCE FEATURES
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="0pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>



              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Serial Number</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Plan Features</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Silver25LM  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Doctor Referral                                                                                            
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    YES
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block> 2 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Message relay to relatives/colleagues/emergency numbers             
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 3 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Toll free number / hotline
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block> 4 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Proposed No. of Services                                                            
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> 1 </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
            </fo:table-body>

          </fo:table>


          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">

                    PLAN FEATURES
                  </fo:block>

                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              1) Doctor Referral                                                                                                                                                               
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              2) Message relay to relatives/colleagues/emergency numbers                                                                       
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Global Assure will provide assistance in providing doctor details to the customer in case of any Emergency.
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Global Assure will take charge of relaying urgent messages in case of Emergency.
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              3) Toll free number / hotline
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              4) Proposed  Number of service
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Access to 24 / 7 hotline number for any Assistance.
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Insured will get 1 service covered under 12 Months of contract CRISIS.
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
         <fo:table border-collapse="collapse" width="100%" padding="20pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="center"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    COVID PROTECTION                                                                                                                                   
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

         <fo:table border-collapse="collapse" width="100%" padding-top="20pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="8pt">Policy Details</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

         <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="2pt">
            <fo:table-column column-width="1in"/>
            <fo:table-column column-width="1in"/>
            <fo:table-column column-width="1in"/>
            <fo:table-column column-width="1in"/>
            <fo:table-column column-width="1in"/>
            <fo:table-column column-width="1in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Premium Payment Mode
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Cover Type
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Sum Insured
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Policy Term
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Certificate Period (Start Date)
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Certificate Period (End Date)
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Single
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Individual
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    <xsl:value-of select="GenerateCertificateModel/SumInsured" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    1 year
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Registered Address </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="260pt"/>
                      <fo:table-column column-width="260pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">F-206, SATYA SHANTI APARTMENT, SECTOR-13, ROHINI, DELHI, North West Delhi, Delhi, 110085 </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              3rd Floor, AIHP Horizon, 445, Phase 5, Udyog Vihar, Gurgaon -122016
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>

                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="0pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center">

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="20pt">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="4in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid" color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c">
                  <fo:block text-align="left" font-size="8pt">
                    Benefits
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid" color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c">
                  <fo:block text-align="left" font-size="8pt">
                    Short Description
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Diagnosis Cover
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    During the Policy Period, the Company pays Lumpsum payment of 100% of Sum Insured if Insured Persons
                    diagnosis test confirms presence of COVID-19.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table table-layout="fixed" width="100%" font-size="7pt" padding-top="20pt">
            <fo:table-column column-width="6in"/>
            <fo:table-header text-align="center">
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm" color="black" font-family="Franklin Gothic Demi" font-weight="bold">
                  <fo:block text-align="left" font-size="8pt">
                    You will receive your Covid-19 Protection Insurance Certificate over SMS, Email within 3 working days from Insurance company.
                    Please go through the Policy Certificate once you receive it. In case you feel that there are any discrepancies/variations, you are requested to write back to Reliance General Insurance Company Ltd. immediately at rgicl.services@relianceada.com or call at 022 4890 3009 (paid) or 1800 3009 (toll-free) for necessary changes/rectification.
                  </fo:block>
                  
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center">
                    <fo:external-graphic src="QrCode.jpg" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

			<fo:block text-align="right">
				For GLB Assure Private Limited
			</fo:block>
			<fo:block text-align="right">
				<fo:external-graphic src="AuthSign.jpg" width="3.2cm" height="1cm"/>
			</fo:block>
			<fo:block text-align="right">
				Authorized Signatory
			</fo:block>

		</fo:flow>
      </fo:page-sequence>
    
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
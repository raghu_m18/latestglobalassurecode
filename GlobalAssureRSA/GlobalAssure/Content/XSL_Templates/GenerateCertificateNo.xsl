﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1.2cm"/>
          </fo:block>

        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Registered Address </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="260pt"/>
                      <fo:table-column column-width="260pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">F-206, SATYA SHANTI APARTMENT, SECTOR-13, ROHINI, DELHI, North West Delhi, Delhi, 110085 </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              3rd Floor, AIHP Horizon, 445, Phase 5, Udyog Vihar, Gurgaon -122016
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>






          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>

                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="2pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Certificate Cum Invoice</fo:block>
                          </fo:table-cell>
                          <!--<fo:table-cell>
                            <fo:block text-align="right">Toll Free Number : 1800 102 0100</fo:block>
                          </fo:table-cell>-->
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Tax Invoice</fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="5pt">
                    This is to certify that Vehicle with the following details is covered under Assistance Program, as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--Dynamic Content When State IS NOT UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table layout="fixed" width="100%" font-size="6pt">
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-header></fo:table-header>
                <fo:table-body xsl:use-attribute-sets="myBorder">
                  <!-- xsl:use-attribute-sets="myBorder" -->
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Intermediary Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/DealerName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Start Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate End Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Engine Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Chassis Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row >
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Manufacturer</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Make" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Model</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Model" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row  background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Variant</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Vehicle Registration Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Amount</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Amount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amount Of Tax IGST (18%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/IGST" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Total Amount (Including Tax)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">GST No. of the Service Recipient</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amount In Words</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Place Of Supply</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">SAC Code</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">First Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Last Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address1</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address2</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address3</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">City</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/City" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">State</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/State" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Mobile No.</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <!--<fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Email ID</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Email" />
                      </fo:block>
                    </fo:table-cell>-->
                    <fo:table-cell padding="2pt">
                      <fo:block></fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block> </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->


          <!--Dynamic Content When State IS UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table layout="fixed" width="100%" font-size="6pt">
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-column column-width="100pt"/>
                <fo:table-column column-width="150pt"/>
                <fo:table-header></fo:table-header>
                <fo:table-body xsl:use-attribute-sets="myBorder">
                  <!-- xsl:use-attribute-sets="myBorder" -->
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Intermediary Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/DealerName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate Start Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Certificate End Date</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Engine Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Chassis Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row >
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Manufacturer</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Make" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Model</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Model" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row  background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Variant</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Vehicle Registration Number</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Plan Amount</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Amount" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row   background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amout Of CGST (9%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/CGST" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amout Of SGST (9%)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SGST" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Total Amount (Including Tax)</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Amount In Words</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row   background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">GST No. of the Service Recipient</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Place Of Supply</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">SAC Code</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">First Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#eaeae1">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Last Name</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address1</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address2</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Address3</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row background-color="#D9D9D9">
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">City</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/City" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">State</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/State" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Mobile No.</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                      </fo:block>
                    </fo:table-cell>
                    <!--<fo:table-cell padding="2pt">
                      <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">Email ID</fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding="2pt">
                      <fo:block>
                        <xsl:value-of select="GenerateCertificateModel/Email" />
                      </fo:block>
                    </fo:table-cell>-->
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->


          <!--<fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block padding-top="5pt" font-size="5pt" padding-bottom="2pt" font-family="Franklin Gothic Demi" font-weight="bold">
                    <fo:inline color="#FF0000">* Conditions Apply</fo:inline>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify" padding-top="5pt" padding-bottom="2pt" color = "#32419c" font-family="Franklin Gothic Demi" font-weight="bold">
                    The services provided under the Assistance are as under:-
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!-- Static Content-->
          <xsl:choose>
            <xsl:when test="($IsPlanFeature = 'FALSE')">
              <fo:table border-collapse="collapse" width="100%">
                <fo:table-column column-width="220pt"/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell padding="2pt">
                      <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                        a) Repair Services for Minor Breakdowns
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        If your vehicle is immobilized whether at home or on the road for a minor breakdown (definition of “minor” being a breakdown that can likely be fixed within a period of maximum 30 minutes with no need for a specific spare part to be replaced unless immediately available at our mechanic’s workshop), we will dispatch a mechanic that will attempt to fix it on the spot/side of the road.
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
              <fo:table border-collapse="collapse" width="100%">
                <fo:table-column column-width="220pt"/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell padding="2pt">
                      <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                        b) Vehicle relocation to the nearest garage in case of Major breakdown.
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        If the attempt to remobilize the vehicle fails following a mechanical or electrical breakdown, a recovery vehicle can be sent to you, will collect the laid-off vehicle and take it to the nearest dealer or garage in measure to proceed to the repair at the workshop.
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
              <fo:table border-collapse="collapse" width="100%">
                <fo:table-column column-width="220pt"/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell padding="2pt">
                      <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                        c) Changing of Flat tyre
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        If the tire(s) has (have) a puncture which immobilizes the vehicle, we can send you a mechanic that can help you replace the punctured tyre, provided that the driver does have a spare tyre in a shape of order. If more than one tyre is punctured, while there is only one spare tyre per vehicle, the immobilization may be necessary by the time the tires are being repaired or replaced at the drivers’ expenses at the workshop.
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
              <fo:table border-collapse="collapse" width="100%">
                <fo:table-column column-width="220pt"/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell padding="2pt">
                      <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                        d) Assistance in case of Lockout/ lost keys
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        If you have lost your vehicle key or locked it inside the vehicle, we can dispatch a mechanic at the location of the incident and help you either unlock the vehicle without damaging the vehicle or, whenever and wherever possible, help you make a copy of the key or arrange delivery of key set from his/her place of residence. The cost of replacement of the key will be borne by the customer.
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
              <fo:table border-collapse="collapse" width="100%">
                <fo:table-column column-width="220pt"/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell padding="2pt">
                      <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                        e) Arrangement of emergency fuel in case the vehicle runs out of fuel.
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        If the vehicle has run short of fuel, we can dispatch a mechanic with an emergency tank of fuel. The cost of fuel will be borne by the customer.
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
              <fo:table border-collapse="collapse" width="100%">
                <fo:table-column column-width="220pt"/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell padding="2pt">
                      <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                        f) Alternative Transport assistance to the nearest safe location for the passengers of the vehicle
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        If the vehicle is being laid-off, we can dispatch an alternative transport to the passengers of the vehicle and take them to the nearest safe location. This arrangement will be taken forward on a referral basis, and all costs are to be approved and borne by the customer.
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
              <fo:table border-collapse="collapse" width="100%">
                <fo:table-column column-width="220pt"/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell padding="2pt">
                      <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                        Special Conditions (applicable to all coverage)
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        a)  All additional expenses regarding replacement of a part, additional Fuel and any other service which does not form a part of the standard services provided would be on chargeable basis to the insured.
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        b) This Certificate is valid subject to realization of the payment and is effective from the Payment realization date or certificate issue date, whichever is later.
                      </fo:block>
                      <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                        c) Claim Servicing would be subject to realization of payment.
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!-- Static Content-->
		 
          <!--Plan Feature-->
          <xsl:choose>
            <xsl:when test="($IsPlanFeature = 'TRUE')">
              <!--break-before="page"-->
              <xsl:for-each select="GenerateCertificateModel/PlanFeatureDetailModelList/PlanFeatureDetailModel">
                <fo:table border-collapse="collapse" width="100%">
                  <fo:table-column column-width="220pt"/>
                  <fo:table-body>
                    <fo:table-row keep-together="always">
                      <fo:table-cell padding="2pt">
                        <fo:block font-size="7pt" font-family="Franklin Gothic Demi"  color="white"  background-color = "#32419c"  text-align="justify" font-weight="bold">
                          <xsl:value-of select="Feature" />
                        </fo:block>
                        <fo:block font-size="6pt" text-align="justify" padding-top="1pt">
                          <xsl:value-of select="Detail" />
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                  </fo:table-body>
                </fo:table>
              </xsl:for-each>
            </xsl:when>
          </xsl:choose>
          <!--Plan Feature-->

     <fo:block padding-top=".5cm" text-align="center">

                <fo:external-graphic src="QrCode.jpg" />

              </fo:block>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="1cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >

          <fo:block text-align="left">
            <xsl:value-of select="GenerateCertificateModel/CreatedDateTimeRSA" />
          </fo:block>
          
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="3in"/>
            <fo:table-header>
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block text-align="center">
                    <fo:external-graphic src="GA_Logo_Large.jpg" width="3.5cm" height="1cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->           
          </fo:block>
          <fo:block text-align="center" font-size="8pt">           
            Please visit Our website <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">www.globalassure.com</fo:inline> for any claim assistance / 24*7 assistance.
          </fo:block>
          <fo:block text-align="center" font-size="8pt">
            Email : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">support@globalassure.com</fo:inline>  |  Contact : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold">0124-4092900</fo:inline> (24*7 assistance)
        </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Registered Address </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="260pt"/>
                      <fo:table-column column-width="260pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">F-206, SATYA SHANTI APARTMENT, SECTOR-13, ROHINI, DELHI, North West Delhi, Delhi, 110085 </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              3rd Floor, AIHP Horizon, 445, Phase 5, Udyog Vihar, Gurgaon -122016
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>






          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>

                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="0pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Tax Invoice</fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="0pt">
                    This is to certify that Pedal cycle with the following details is covered under Assistance Program, as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding="4pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Your Cycle Package Cover
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify" padding-top="0pt">
                    Dear <xsl:value-of select="GenerateCertificateModel/FirstName" />, congratulations on the purchase of Pedal Cycle assistance package cover from Global Assure.
                  </fo:block>
                  <fo:block text-align="justify" padding-top="0pt" padding-bottom="5pt">
                    As a valued customer, this package is uniquely designed for your needs. Here is the the summary of inclusions and exclusions.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        CERTIFICATE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" >
                                      <fo:block>
                                        Certificte Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Plan Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate Start Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate End Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Place Of Supply
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        SAC Code
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        PEDAL CYCLE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Make
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Make" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Model
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Model" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Variant
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Colour of Cycle
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Colour" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Invoice Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InvoiceDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Chassis Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PERSONAL DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="1pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        First Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Last Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Mobile No
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Email ID
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Email" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Address1
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InsuredAddressLine1" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address2
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InsuredAddressLine2" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address3
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/InsuredAddressLine3" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        State
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/State" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        City
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/City" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PAYMENT DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--Dynamic Content When State IS NOT UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="1pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amount Of Tax IGST (18%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/IGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->

          <!--Dynamic Content When State IS UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="1pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of CGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/CGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of SGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/SGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->

          
          <!--<fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block padding-top="5pt" font-size="5pt" padding-bottom="2pt" font-family="Franklin Gothic Demi" font-weight="bold">
                    <fo:inline color="#FF0000">* Conditions Apply</fo:inline>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="8pt">What's Covered (Inclusions) ?</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="3pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell font-family="Franklin Gothic Demi" font-weight="bold">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="7pt">Cycle theft or damage as well as accident-related death or disability.</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="3pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold" text-align="left">Serial No.</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold" text-align="left">Plan Features</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold" text-align="center">Pedal Cycle</fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    24*7 Phone Support
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">
                    YES 
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left"> 2 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Doctor Referral
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center"> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 3 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Message relay to relatives/colleagues/emergency numbers
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center"> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left"> 4 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Locating Nearest Police Station
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">
                    <fo:block>YES</fo:block>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 5 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Ambulance Referral
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">6</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Proposed No. of Services
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">2</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">7</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Cycle Theft
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">8</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Accidental Damage
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">9</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Cycle Damage from Natural Calamities
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">10</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Cashless Claims at Authorised Dealership
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">11</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Permanent Disability/Accidental Death Rs.2,00,000
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>

          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="8pt">What's Not Covered (Exclusions) ?</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="3pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell font-family="Franklin Gothic Demi" font-weight="bold">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left" font-size="7pt">Normal wear and tear, negligence is not covered.</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="3pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold" text-align="left">Serial No.</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold" text-align="left"></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#D9D9D9">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold" text-align="center"></fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Wear and Tear of bicycle
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">
                    NO
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left"> 2 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Theft due to cyclist's negligence  
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center"> NO </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 3 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Damage due to cleaning/sevicing
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center"> NO </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left"> 4 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="left">
                    Expenses under hospitalisation/OPD
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid" background-color="#eaeae1">
                  <fo:block text-align="center">
                    <fo:block>NO</fo:block>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left"> 5 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="left">
                    Accidents under the effects of intoxicants
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt" border-width="0.1px" border-style="solid">
                  <fo:block text-align="center">NO</fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>

          </fo:table>

          <fo:block text-align="right">
            For GLB Assure Private Limited
          </fo:block>
          <fo:block text-align="right">
            <fo:external-graphic src="AuthSign.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
          <fo:block text-align="right">
            Authorized Signatory
          </fo:block>

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <!--<fo:static-content flow-name="page-footer">
        <fo:block text-align="center">
          
      <fo:external-graphic src="letterfooter.jpg" width="25cm" scaling="uniform"/>
      
        </fo:block>
      </fo:static-content>-->

        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <xsl:value-of select="GenerateCertificateModel/CreatedDateTimePA" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="NIA.PNG" width="3cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    The New India Assurance Company Limited
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table table-layout="fixed" width="100%" border-width="1pt"
    border-style="solid" font-size="7pt" >
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">




            </fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Master Policy No : 46010047199400000002 / 46010042190100000106
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Certificate No : <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Name of Master Policy Holder : GLB ASSURE PRIVATE LIMITED
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Period of Insurance :From: <xsl:value-of select="GenerateCertificateModel/CoverStartDate" /> To:<xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Name of Nominee : <xsl:value-of select="GenerateCertificateModel/NomineeName" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Relationship of nominee with insured : <xsl:value-of select="GenerateCertificateModel/NomineeRelationship" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Nominee Gender : <xsl:value-of select="GenerateCertificateModel/NomineeGender" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Branch Code : <xsl:value-of select="GenerateCertificateModel/BranchCode" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Insured Gender : <xsl:value-of select="GenerateCertificateModel/InsuredGender" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Insured DOB : <xsl:value-of select="GenerateCertificateModel/InsuredDOB" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
            </fo:table-body>

          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" >
                    Pedal Cycle: Insurance covers Pedal Cycle up to Rs.40,000/- only                  
                  </fo:block>
                 <fo:block text-align="justify"  font-size="7pt" >
                    Personal Accident: Sum Insured Rs. 2,00,000/- only                   
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Restriction of Coverage
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="0pt">
                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Coverage is applicable to persons with age from 5 years up to max 70 years
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Coverage in Brief
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    1. Pedal Cycle :-                 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    The Company will Indemnify the Insured against loss or damage to any pedal cycle described in the Schedule hereto (including its accessories whilst thereon) by fire external explosion or lightning or burglary, housebreaking, larceny or theft. 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    *Excess will be 25% of Sum Insured for any burglary, housebreaking, larceny or theft claim 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    *Time Excess of 15 Days will be applicable for any burglary, housebreaking, larceny or theft claim 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    *FIR is mandatory for any fire external explosion or lightning or burglary, housebreaking, larceny or theft Claim 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    *Coverage only in India
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    2. Personal Accident                  
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    If the Insured person i.e the owner of the pedal cycle (in whose name invoice of Pedal cycle was issued) meets with an accident and sustains any bodily injury during the policy period which directly and independently of all other causes result in death / permanent total disablement within 12 months from the date of accident resulting solely and directly from accident then the company shall pay to the insured the sum set in the schedule to the insureds persons nominee, beneficiary or legal representative. 
                  </fo:block>
                <fo:block text-align="justify"  font-size="6pt">
                    Accidental Death: 
                  </fo:block>
                <fo:block text-align="justify"  font-size="6pt">
                    100% of cumulative Sum Insured (CSI) 
                  </fo:block>
                <fo:block text-align="justify"  font-size="6pt">
                    Permanent Total Disability- 
                  </fo:block>
          <fo:block text-align="justify"  font-size="6pt">
                    •Loss of Two Limbs/ Two Eyes or One Limb and One Eye: 100% of CSI 
                  </fo:block>
          <fo:block text-align="justify"  font-size="6pt">
                    •Loss of One Limb or One Eye : 50% of CSI 
                  </fo:block>
                    <fo:block text-align="justify"  font-size="6pt">
                    •Permanent Total Disablement from Injuries other than those named above: 100% of CSI 
                  </fo:block>
                    <fo:block text-align="justify"  font-size="6pt">
                    *Compensation will be paid either under Accidental Death or under Permanent Total Disability and in no case, compensation will be paid under both the heads. 
                  </fo:block>
                    <fo:block text-align="justify"  font-size="6pt">
                   *Coverage will be applicable to owner of the Pedal Cycle (can be child as well) in whose name invoice of Pedal cycle was issued  
                  </fo:block>
                 <fo:block text-align="justify"  font-size="6pt">
                   *coverage is on 24 Hr Basis only in India 
                  </fo:block>
           <fo:block text-align="justify"  font-size="6pt">
                    *Coverage mentioned will be as per Table C (Accidental Death and Permanent Total Disability) 
                  </fo:block>
                <fo:block text-align="justify"  font-size="6pt">
                   Please refer to policy for detail information on Coverages and other terms and conditions.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                  Exclusions in Brief
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  Pedal Cycle: -             
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Accidental external means              
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • RSMD           
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • STFI                 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Liability to Third Parties                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Consequential loss                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Nuclear, War and Radioactive                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                                     
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  Personal Accident:-                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  War, invasion, act of foreign enemy, hostilities (whether war be declared or not) civil war, rebellion, revolution, insurrection, mutiny military or usurped power, confiscation, seizure, capture, assault, restraint, nationalization, civil commotion or loot or pillage in connectionHerewith.                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Ionizing radiation or contamination by radioactivity from any nuclear fuel or from any nuclear waste from the combustion of nuclear fuel. For the purpose of this exclusion, combustion shall include any self sustaining process of nuclearfission.                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Theradioactive,toxic,explosiveorthehazardous propertiesofanynuclearassemblyornuclearcomponentornuclearweapons material.                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	WhilstengaginginAviationorBallooningwhilstmountinginto,dismountingfromortravelinginanyballoonoraircraft                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Participation in any kind of motor speed contest (including trial, training and qualifyingheats)                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	ThisInsurancedoesnotcoveranyloss,damage,costorexpensedirectlyorindirectlyarisingoutof-Biologicalorchemicalcontamination,Missiles,bombs,grenades,explosives                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Services on duty with any Armedforces                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Intentional self injury, suicide, or attemptedsuicide                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	venereal diseases, aids orinsanity                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Whilst under the influence of Alcohol or intoxicating liquor ordrugs.                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Medical or surgicaltreatment                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Whilst committing any breach of law with criminalintent.                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  •	Child birth, pregnancy or other physical cause peculiar to the femalesex                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  Please refer to policy for detail information on exclusions and other terms and conditions.                   
                  </fo:block>                  
                </fo:table-cell>

              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>
             <fo:page-sequence master-reference="simple" font-size="7pt" >
        <fo:flow flow-name="xsl-region-body">
            
             <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="NIA.PNG" width="3cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    The New India Assurance Company Limited
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Claims Process / Documentation
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  1. Pedal Cycle                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  Notice shall be given in writing to the Company immediately upon the occurrence of any accident or loss or damage and in the event of any claim and thereafter the Insured shall give all such information and assistance as the Company shall require.                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  In case of theft or other criminal act which may be the subject of a claim under this policy the Insured shall give immediate notice to the Police and co-operate with the Company in securing the conviction of the offender.                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  Following documents shall be required in the event of a claim.                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Policy Copy                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Duly filled up claim form                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Original F.I.R or F.I.R - Notarized/ Attested by a gazetted officer (FIR is compulsory)                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Police Final charge sheet/ Court Final order- Notarized/ attested by a Gazetted Officer - if applicable                   
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • If claim amount is more than 1lakh, AML Documents - Pan Card Copy, Residence Proof,2 Passport size colour photos of claimant                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Cancel Cheque with NEFT Mandate form - duly filled in by the claimant and bank                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  • Any other document as required by the Company to investigate the Claim or Our obligation to make payment for                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                                       
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                  2. Personal Accident                   
                  </fo:block>                  
                  <fo:block text-align="justify"  font-size="6pt">
                    Upon happening of any accident and/or injury which may give rise to a claim under this policy:
                  </fo:block>
                  <fo:block text-align="justify" >
                    • You shall give the notice to our call centre immediately and also intimate in writing to our policy issuing office. In case of Death, written notice also of Death must, unless reasonable cause is shown, be given before internment/ cremation and in any case, within one calendar month after the Death. In the event of loss of sight or amputation of limbs, written notice thereof must also be given within one calendar month after such loss of sight or amputation.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • All certificates, information and evidence from a Medical Practitioner or otherwise required by us shall be provided by you.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • On receipt of intimation from you regarding a claim under the policy, we are entitled to carry out examination and ascertain details and in the event of Death get the post-mortem examination done in respect of deceased person.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    Following documents shall be required in the event of a claim.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    For Death:
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Policy Copy
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Duly filled up claims form
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original Death Certificate or Death certificate - Notarized/ Attested by a gazetted officer, if applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original F.I.R or F.I.R - Notarized/ Attested by a gazetted officer
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Police Final charge sheet/ Court Final order - Notarized/ attested by a Gazetted Officer - if applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Spot Panchnama and Police Inquest report - Notarized/ Attested by a gazetted officer, if applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Post Mortem Report - Notarized/ Attested by a gazetted officer, if concluded
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Viscera Analysis Report/ Chemical analysis report/ Forensic Science Lab report notarized/ Attested by gazetted officer, if applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Other Document as per Case details – Complete medical records including Death Summary; if hospitalized, Website Links/ Newspaper cuttings, Other references
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • If claim amount is more than 1lakh, AML Documents - Pan Card Copy, Residence Proof,2 Passport size colour photos of claimant
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Cancel Cheque with NEFT Mandate form - duly filled in by the claimant and bank
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Any other document as required by the Company to investigate the Claim or Our obligation to make payment for
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    1. In respect of fatal claims, the payment is to be made to the assignee named under the policy. If there is no assignee, the payment is made to the legal representative as identified by Will / Probate / Letter of Administration / Succession Certificate.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    2. Where the above documents are not available, the following procedure may be followed: -
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    a) An affidavit from the Claimant(s) that he/she (they) is (are) the legal heir(s) of the deceased
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    b) An affidavit from other near family members and relatives of the deceased that they have no objection if the claim amount is paid to the claimant(s)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    For Permanent Total disablement/Permanent Partial Disablement
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Policy Copy
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Duly filled up claims form
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Attending Doctors Report
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Copy of medical records including Investigation/ Lab Reports (X Ray, MRI etc.)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original FIR, Panchnama, Police Report where applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Hospital discharge card
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original Certificate from Doctor of Govt. Hospital stating the degree of disability
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • If claim amount is more than 1lakh, AML Documents - Pan Card Copy, Residence Proof,2 Passport size colour photos of claimant
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Cancel Cheque with NEFT Mandate form - duly filled in by the claimant and bank
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Any other document as required by the Company to investigate the Claim or Our obligation to make payment for
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>

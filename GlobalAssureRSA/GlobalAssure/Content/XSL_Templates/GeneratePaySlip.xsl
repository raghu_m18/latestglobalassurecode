﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="1cm"
                    margin-bottom="1cm"
                    margin-left="0.5cm"
                    margin-right="0.5cm">
          <fo:region-body margin-top="1cm"/>
          <fo:region-before region-name="page-header" extent="1cm"/>
          <fo:region-after region-name="page-footer" extent="1cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="simple" font-size="9pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center" font-family="Franklin Gothic Demi" font-weight="bold" font-size="9pt">
            <!--<fo:external-graphic src="_GA_Logo_Medium.jpg"/>-->
            Bank Deposit Challan
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block>
            <!--<fo:external-graphic src="LetterFooter.png"/>-->
            <fo:table border-collapse="collapse" width="100%">
              <fo:table-column column-width="580pt"/>
              <fo:table-body>
                <fo:table-row keep-together="always">
                  <fo:table-cell padding="2pt">
                    <fo:block column-width="560pt">
                      <fo:table border-collapse="collapse" width="560pt">
                        <fo:table-column column-width="280pt"/>
                        <fo:table-column column-width="280pt"/>
                        <fo:table-body>
                          <fo:table-row keep-together="always">
                            <fo:table-cell>
                              <fo:block text-align="left">Customer's Signature</fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block text-align="right">Banker's Signature</fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
              </fo:table-body>
            </fo:table>
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:table layout="fixed" width="100%" font-size="8pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>

            <fo:table-header>

            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>Deposit Challan No</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <xsl:value-of select="GeneratePaySlipModel/DepositChallanNo" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Deposit Date</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <xsl:value-of select="GeneratePaySlipModel/CurrentDate" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>Bank Account No</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <xsl:value-of select="GeneratePaySlipModel/BankAccountNo" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>Gross Deposit Amount</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <xsl:value-of select="GeneratePaySlipModel/UniqueChequeGrossDepositeAmount" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>Bank Name</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <xsl:value-of select="GeneratePaySlipModel/BankName" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>No Of Instruments</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <xsl:value-of select="GeneratePaySlipModel/UniqueChequeNoOfInstruments" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>Pickup Point</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <xsl:value-of select="GeneratePaySlipModel/PickUpPoint" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table layout="fixed" width="100%" font-size="8pt" padding-top="5pt">
            <fo:table-column column-width="25pt"/>
            <fo:table-column column-width="60pt"/>
            <fo:table-column column-width="60pt"/>
            <fo:table-column column-width="60pt"/>
            <fo:table-column column-width="60pt"/>
            <fo:table-column column-width="50pt"/>
            <fo:table-column column-width="85pt"/>
            <fo:table-column column-width="85pt"/>
            <fo:table-column column-width="60pt"/>
            <fo:table-header>
              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Sr No.
                  </fo:block>
                </fo:table-cell >
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Instrument No
                  </fo:block>
                </fo:table-cell >
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Instrument Date
                  </fo:block>
                </fo:table-cell >
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Drawee Bank
                  </fo:block>
                </fo:table-cell >
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Bank Branch
                  </fo:block>
                </fo:table-cell >
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Amount (Rs.)
                  </fo:block>
                </fo:table-cell >
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Receipt No
                  </fo:block>
                </fo:table-cell >
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Certificate No
                  </fo:block>
                </fo:table-cell >
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block text-align="center" font-weight="bold">
                    Remark
                  </fo:block>
                </fo:table-cell >
              </fo:table-row>
            </fo:table-header>

            <fo:table-body>
              <xsl:for-each select="GeneratePaySlipModel/PaySlipDetailModelList/PaySlipDetailModel">
            <fo:table-row>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:number format="1" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:value-of select="ChequeNo"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:value-of select="ChequeDate"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:value-of select="Bank"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:value-of select="Branch"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:value-of select="ChequeAmount"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:value-of select="PaymentReferenceNo"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:value-of select="CertificateNo"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border="solid 0.1mm black">
                <fo:block text-align="center">
                  <xsl:value-of select="Remark"/>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </xsl:for-each>
            </fo:table-body>
          </fo:table>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border">solid 0.1mm black</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>
      <xsl:variable name="IsInsuredDOB" select="GenerateCertificateModel/IsInsuredDOB"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1cm"/>
          </fo:block>

        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="275pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left"> </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="left">

                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="275pt"/>
                      <fo:table-column column-width="260pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left"> </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="left">

                              Global Assure , 5th  Floor Tower A , BPTP Park Centra Building, Sector -30, Near Star Mall, Gurugram-122001, Haryana
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>






          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>-->
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>

                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="0pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Tax Invoice</fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="0pt">

                    This is to certify that Vehicle with the following details is covered under Assistance Program, as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        CERTIFICATE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" >
                                      <fo:block>
                                        Certificte Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Plan Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate Start Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate End Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Place Of Supply
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        SAC Code
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        VEHICLE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Vehicle Registration Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Manufacturer
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Make" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Model
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Model" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Variant
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Engine Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Chassis Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PERSONAL DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        First Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Last Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Mobile No
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <!--<fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Email ID
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Email" />
                                      </fo:block>
                                    </fo:table-cell>-->
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Address1
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address2
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address3
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        State
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/State" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        City
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/City" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PAYMENT DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--Dynamic Content When State IS NOT UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amount Of Tax IGST (18%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/IGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->

          <!--Dynamic Content When State IS UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of CGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/CGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of SGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/SGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->


          <!--<fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block padding-top="5pt" font-size="5pt" padding-bottom="2pt" font-family="Franklin Gothic Demi" font-weight="bold">
                    <fo:inline color="#FF0000">* Conditions Apply</fo:inline>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->


          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Plan Features
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="0pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>



              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Serial Number</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Plan Features</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold"></fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">NITW260 </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Towing of Vehicle on accident
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Yes (Upto 10 km G 2 G)
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block> 2</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Tyre Change
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Yes
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row >
                <fo:table-cell padding="2pt">
                  <fo:block> 3</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Arrangement of spare keys
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Yes
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row  background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block> 4</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Extraction or Removal of vehicle
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Yes
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 5</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Message relay to relatives/colleagues/emergency numbers
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Yes
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block> 6</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Fuel delivery
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Yes
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 7</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Hotel Accomodation
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Assistance
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block> 8</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Proposed No. of Services
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    1
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>             

            </fo:table-body>

          </fo:table>


          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">

                    Plan Features
                  </fo:block>

                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              1) Towing of Vehicle on Accident
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              2) Tyre Change
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              In the event Covered Vehicle suffers an immobilizing break down due to a mechanical or electrical fault or an accident which cannot be repaired on the spot, Global Assure will assist in making arrangement for the Vehicle to be towed to the nearest Authorised Service Center, using tow trucks in the cities and corresponding covered area where available. (Free towing  to and Fro upto 10 KM)
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              In the event Covered Vehicle is immobilized due to a flat tyre, Global Assure will assist the Customer by organizing for a vehicle technician to replace the flat tyre with the spare stepney tyre of the Vehicle at the location of breakdown. Global assure will bear labour cost and round-trip conveyance costs of the provider. Material/spare parts if required to repair the Vehicle (including repair of flat spare stepney tyre) will be borne by the Customer. In case the spare tyre is not available in the covered Vehicle, the flat tyre will be taken to the nearest flat tyre repair shop for repairs and re-attached to the Vehicle. All incidental charges for the same shall be borne by the Customer.
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              3) Arrangement of spare keys
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              4) Message relay to relatives/colleagues/emergency numbers
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              If the keys of the covered vehicle are locked inside the vehicle, broken, lost, or misplaced, Global Assure (upon the request of the customer) will arrange for the forwarding of another set from his/her place of residence or office by courier / in person by hand-delivery to the location of the vehicle after receiving the requisite authorizations from the Customer with regards to the person designated to hand over the same to Global assure. The Customer may be requested to submit an identity proof at the time of delivery of the keys.
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Global Assure will take charge of relaying urgent messages relating to the breakdown to the authorized workshop and/or service contacts. When requested, Global assure will relay urgent messages on behalf of the Customers to a designated person of their choice.
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              5) Hotel Accomodation and Travel
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              6) Extraction or Removal of vehicle
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Global assure will provide the assistance to book the same
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                              |
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              In event of vehicle being stuck in a ditch/pit/valley, Global Assure will make the arrangement to get the vehicle retrieved and towed to the nearest authorised service centre at no cost to the Customer.(Free towing  to and Fro upto 10 KM)
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              7) No of services
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              8) Arrangement of fuel
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              Customer will get 1 service covered under 12 Months of contract
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              In the event Covered Vehicle runs out of fuel and hence is immobilized while on a trip, Global Assure will assist Customer by organizing for a Vehicle technician to supply emergency fuel (up to 5 ltrs on a chargeable basis) at the location of breakdown. Global assure will bear labour and conveyance costs. The cost of the fuel will be borne by the customer.
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>        

          <!--</fo:flow>
      </fo:page-sequence>-->

          <!--<fo:page-sequence master-reference="simple" font-size="7pt" >-->

          <!--<fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
        </fo:static-content>-->

          <!--<fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            -->
          <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          <!--
          </fo:block>
        </fo:static-content>-->

          <!--<fo:flow flow-name="xsl-region-body">-->

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Terms and Conditions :
                  </fo:block>

                </fo:table-cell>


              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              1) City Tax and Other Charges
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              2) Adverse Weather
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              In case the vehicle is being towed from one state to another or from one city to another, any local tax or toll tax, like green tax in Delhi-NCR and etc. has to borne by Insured.
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              On occasion of adverse weather conditions such as floods, heavy rain, thunder / lightening or other external factors may affect our ability to provide services and it may become physically impossible to assist you until the weather improves. During such times, our main priority will be to ensure that you and your passengers are taken to a place of safety; the recovery of your vehicle may not be possible until weather conditions permit.
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              3) Coverage
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              4) Program Start Date
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              The roadside assistance is a nationwide service provided to mobilize vehicle which has become disabled due to unexpected breakdown. While providing roadside assistance, we carry out temporary repairs and not regular maintenance which are normally carried out at authorized workshops.
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              The date of commencement of coverage under the program. The program start date will be after 7 (SEVEN) days from the program purchase date
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              5) Program End Date
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              6) Un-located or Unattended Vehicle
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              The coverage end date of the program. The Roadside Assistance Program shall be valid for a period of 12 (twelve) months from the Program start date as mentioned in the program certificate.
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              In case of a breakdown, you must be able to provide us with the correct location of your covered vehicle. Incorrect or incomplete information may cause a delay in provision of our services. You or an authorized representative must be present with the vehicle at the agreed meeting place when we arrive. If the vehicle is not attended when we arrive services cannot be provided
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              7) Covered Vehicle is off road
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              8) Cooling Period
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              In case the covered vehicle is not on gazette, bitumen road and if there is a need to arrange special equipment (any equipment other than standard towing equipment), the charges of such equipment are not part of the roadside assistance program. The charges for arrangements of special equipment are to be borne by Insured. Insured should understand that in case of usage of special equipment’s covered vehicle might sustain certain direct or consequential damages which extracting the vehicle. Insured agrees that such damage shall be sole responsibility of the Insured and AWP shall not be held liable for any such damages.
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              That the Entitlements are available after 7 days, all the benefits under the roadside assistance program became available 7 days after from the purchase date of the program.
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              9) Accidental cases
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white">
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              10) External Factors
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="6pt" padding-top="2pt" >
                    <fo:table border-collapse="collapse"  width="100%">

                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="5pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="250pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">
                              In cases where the police authorities have registered an FIR or are in a process of registering an FIR or are investigating a case or have taken possession of the vehicle, Assistance services can be activated only after Police authorities/courts have given a clearance to the vehicle / have released the possession of vehicle. In such cases it is the duty of the Insured to obtain such clearances. AWP can take handover of the vehicle only after clearances have been obtained and the Insured / Insured’s representative is available to provide appropriate handover of the vehicle to the towing representative from the police authorities.
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="black">
                              |
                              |
                              |
                              |
                              |
                              |
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">
                              We will take every effort to reach you once you make the call however the response time may vary depending on, among other things, the breakdown location of the vehicle and the general demand for roadside assistance at the time of your request is received.
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>

                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify" font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Special Conditions (applicable to all coverage)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                    a)  All additional expenses regarding replacement of a part, additional Fuel and any other service which does not form a part of the standard services provided would be on chargeable basis to the insured.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                    b) This Certificate is valid subject to realization of the payment and is effective from the Payment realization date or certificate issue date, whichever is later.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="1pt" >
                    c) Claim Servicing would be subject to realization of payment.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <!--<fo:static-content flow-name="page-footer">
        <fo:block text-align="center">
          
      <fo:external-graphic src="letterfooter.jpg" width="25cm" scaling="uniform"/>
      
        </fo:block>
      </fo:static-content>-->

        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="NIA.PNG" width="3cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    The New India Assurance Company Limited
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table table-layout="fixed" width="100%" border-width="1pt"
    border-style="solid" font-size="7pt" >
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">




            </fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    <!-- Master Policy No : 46010042190100000065 -->
					Master Policy No : <xsl:value-of select="GenerateCertificateModel/MasterPolicyNo" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Certificate No : <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Name of Master Policy Holder : GLB ASSURE PRIVATE LIMITED
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Period of Insurance :From: <xsl:value-of select="GenerateCertificateModel/CoverStartDate" /> To:<xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Name of Nominee : <xsl:value-of select="GenerateCertificateModel/NomineeName" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Relationship of nominee with insured : <xsl:value-of select="GenerateCertificateModel/NomineeRelationship" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Nominee Gender : <xsl:value-of select="GenerateCertificateModel/NomineeGender" />



                  </fo:block>
                </fo:table-cell>                
              </fo:table-row>

              <fo:table-row>
                <!--Dynamic Content When InsuredDOB IS TRUE-->
                <xsl:choose>
                  <xsl:when test="($IsInsuredDOB = 'TRUE')">
                    <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                      <fo:block font-weight="bold">
                        Insured DOB / AGE : <xsl:value-of select="GenerateCertificateModel/InsuredDOB" />
                      </fo:block>
                    </fo:table-cell>
                  </xsl:when>
                </xsl:choose>
                <!--Dynamic Content-->

                <!--Dynamic Content When InsuredDOB IS False-->
                <xsl:choose>
                  <xsl:when test="($IsInsuredDOB = 'FALSE')">
                    <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                      <fo:block font-weight="bold">
                        Insured DOB / AGE : <xsl:value-of select="GenerateCertificateModel/InsuredAge" />
                      </fo:block>
                    </fo:table-cell>
                  </xsl:when>
                </xsl:choose>
                <!--Dynamic Content-->
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Name of Insured : <xsl:value-of select="GenerateCertificateModel/InsuredName" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>  
                  
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Insured Gender : <xsl:value-of select="GenerateCertificateModel/InsuredGender" />



                  </fo:block>
                </fo:table-cell>                
              </fo:table-row>
            </fo:table-body>

          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" >
                    Rs.15,00,000/- (Accidental Death and Permanent Total Disability). Cover is valid only while the insured is driving the vehicle covered under the Road Side Assistance plan mentioned above or accompanied the vehicle covered under the Road Side Assistance plan mentioned above as a passenger.
                  </fo:block>


                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Restriction of Coverage
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="0pt">
                    Accidental Death:
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It is strictly restricted to Death due to accident, while the owner of vehicle driving the vehicle or accompanied the vehicle as a passenger.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Permanent Total Disability-
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It is strictly restricted to Disability due to accident, while the owner of vehicle driving the vehicle or accompanied the vehicle as a passenger.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Coverage is applicable to persons with age up to 80 years
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Coverage in Brief
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    If the Insured person meets with an accident and sustains any bodily injury during the policy period which directly and independently of all other causes result in death / permanent total disablement within 12 months from the date of accident resulting solely and directly from accident then the company shall pay to the insured the sum set in the schedule to the insureds persons nominee, beneficiary or legal representative.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Accidental Death:
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    100% of cumulative Sum Insured (CSI)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Permanent Total Disability-
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    •Loss of Two Limbs/ Two Eyes or One Limb and One Eye: 100% of CSI
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    •Loss of One Limb or One Eye : 50% of CSI
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    •Permanent Total Disablement from Injuries other than those named above: 100% of CSI
                    Compensation will be paid either under Accidental Death or under Permanent Total Disability and in no case, compensation will be paid under both the heads.
                    Please refer to policy for detail information on Policy coverage
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Compensation will be paid either under Accidental Death or under Permanent Total Disability and in no case, compensation will be paid under both the heads.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Please refer to policy for detail information on Policy coverage
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Exclusions in Brief
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    • War, invasion, act of foreign enemy, hostilities (whether war be declared or not) civil war, rebellion, revolution, insurrection, mutiny military or usurped power, confiscation, seizure, capture, assault, restraint, nationalization, civil commotion or loot or pillage in connection Herewith.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Ionizing radiation or contamination by radioactivity from any nuclear fuel or from any nuclear waste from the combustion of nuclear fuel. For the purpose of this exclusion, combustion shall include any self sustaining process of nuclear fission.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • The radioactive, toxic, explosive or the hazardous properties of any nuclear assembly or nuclear component or nuclear weapons material.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Whilst engaging in Aviation or Ballooning whilst mounting into, dismounting from or traveling in any balloon or aircraft
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Participation in any kind of motor speed contest (including trial, training and qualifying heats)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • This Insurance does not cover any loss, damage, cost or expense directly or indirectly arising out of - Biological or chemical contamination, Missiles, bombs, grenades, explosives
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Services on duty with any Armed forces
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Intentional self injury, suicide, or attempted suicide
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • venereal diseases, aids or insanity
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Whilst under the influence of Alcohol or intoxicating liquor or drugs.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Medical or surgical treatment
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Whilst committing any breach of law with criminal intent.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Child birth, pregnancy or other physical cause peculiar to the female sex
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    Please refer to policy for detail information on exclusions and other terms and conditions.
                  </fo:block>
                </fo:table-cell>

              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>
      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="NIA.PNG" width="3cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    The New India Assurance Company Limited
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Claims Process / Documentation
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Upon happening of any accident and/or injury which may give rise to a claim under this policy:
                  </fo:block>
                  <fo:block text-align="justify" >
                    • You shall give the notice to our call centre immediately and also intimate in writing to our policy issuing office. In case of Death, written notice also of Death must, unless reasonable cause is shown, be given before internment/ cremation and in any case, within one calendar month after the Death. In the event of loss of sight or amputation of limbs, written notice thereof must also be given within one calendar month after such loss of sight or amputation.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • All certificates, information and evidence from a Medical Practitioner or otherwise required by us shall be provided by you.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • On receipt of intimation from you regarding a claim under the policy, we are entitled to carry out examination and ascertain details and in the event of Death get the post-mortem examination done in respect of deceased person.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    Following documents shall be required in the event of a claim.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    For Death:
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Policy Copy
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Duly filled up claims form
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original Death Certificate or Death certificate - Notarized/ Attested by a gazetted officer, if applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original F.I.R or F.I.R - Notarized/ Attested by a gazetted officer
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Police Final charge sheet/ Court Final order - Notarized/ attested by a Gazetted Officer - if applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Spot Panchnama and Police Inquest report - Notarized/ Attested by a gazetted officer, if applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Post Mortem Report - Notarized/ Attested by a gazetted officer, if concluded
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Viscera Analysis Report/ Chemical analysis report/ Forensic Science Lab report notarized/ Attested by gazetted officer, if applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Other Document as per Case details – Complete medical records including Death Summary; if hospitalized, Website Links/ Newspaper cuttings, Other references
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • If claim amount is more than 1lakh, AML Documents - Pan Card Copy, Residence Proof,2 Passport size colour photos of claimant
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Cancel Cheque with NEFT Mandate form - duly filled in by the claimant and bank
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Any other document as required by the Company to investigate the Claim or Our obligation to make payment for
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    1. In respect of fatal claims, the payment is to be made to the assignee named under the policy. If there is no assignee, the payment is made to the legal representative as identified by Will / Probate / Letter of Administration / Succession Certificate.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    2. Where the above documents are not available, the following procedure may be followed: -
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    a) An affidavit from the Claimant(s) that he/she (they) is (are) the legal heir(s) of the deceased
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    b) An affidavit from other near family members and relatives of the deceased that they have no objection if the claim amount is paid to the claimant(s)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    For Permanent Total disablement/Permanent Partial Disablement
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Policy Copy
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Duly filled up claims form
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Attending Doctors Report
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Copy of medical records including Investigation/ Lab Reports (X Ray, MRI etc.)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original FIR, Panchnama, Police Report where applicable
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Hospital discharge card
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original Certificate from Doctor of Govt. Hospital stating the degree of disability
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • If claim amount is more than 1lakh, AML Documents - Pan Card Copy, Residence Proof,2 Passport size colour photos of claimant
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Cancel Cheque with NEFT Mandate form - duly filled in by the claimant and bank
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Any other document as required by the Company to investigate the Claim or Our obligation to make payment for
                  </fo:block>
                  <fo:block text-align="justify"  font-size="7pt" >
                    Regd. and Head Office: New India Assurance Bldg., 87 M.G. Road, Fort, Mumbai - 400001
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>

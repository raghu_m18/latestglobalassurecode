﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>
      <xsl:variable name="IsInsuredDOB" select="GenerateCertificateModel/IsInsuredDOB"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >

          <fo:block text-align="left">
            <xsl:value-of select="GenerateCertificateModel/CreatedDateTimeRSA" />
          </fo:block>
          
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header>
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block text-align="left">
                    <fo:external-graphic src="GA_Logo_Large.jpg" width="3.5cm" height="1cm"/>
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block text-align="right">
                    <fo:external-graphic src="MobiSafar.png" width="4cm" height="1.5cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%" padding-top="10pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="275pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left"> Corporate Address </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="left"> </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>



          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="275pt"/>
                      <fo:table-column column-width="260pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left"> Global Assure , 5th  Floor Tower A , BPTP Park Centra Building, Sector -30, Near Star Mall, Gurugram-122001, Haryana </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="left"> </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>






          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>

                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="0pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Tax Invoice</fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="0pt">

                    This is to certify that Vehicle with the following details is covered under Assistance Program, as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        CERTIFICATE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" >
                                      <fo:block>
                                        Certificte Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Plan Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate Start Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate End Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Place Of Supply
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        SAC Code
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        VEHICLE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Vehicle Registration Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Manufacturer
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Make" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Model
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Model" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Variant
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Engine Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Chassis Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PERSONAL DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        First Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Last Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Mobile No
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <!--<fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Email ID
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Email" />
                                      </fo:block>
                                    </fo:table-cell>-->
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Address1
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address2
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address3
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        State
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/State" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        City
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/City" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PAYMENT DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--Dynamic Content When State IS NOT UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amount Of Tax IGST (18%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/IGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->

          <!--Dynamic Content When State IS UP-->
          <xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of CGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/CGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of SGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/SGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>
          <!--Dynamic Content-->


          <!--<fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block padding-top="5pt" font-size="5pt" padding-bottom="2pt" font-family="Franklin Gothic Demi" font-weight="bold">
                    <fo:inline color="#FF0000">* Conditions Apply</fo:inline>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Plan Features
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="0pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>

              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Serial Number</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Plan Features</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">HATAGICPAMOBI199</fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Doctor on call/teleconsultation
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    YES
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block> 2 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Ambulance Referral(Paid Basis)
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row >
                <fo:table-cell padding="2pt">
                  <fo:block> 3 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Book Blood Test(Paid Basis)
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row  background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block> 4 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Medicine Delivery(Paid basis)
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    <fo:block>YES</fo:block>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 5 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    PA (1 lac)
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block>6</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Doctor Consultations
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    2
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>
          </fo:table>

          <fo:block text-align="right">
            *Doctor Consultation – In Partnership with doctor 24*7
          </fo:block>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center">
                    <fo:external-graphic src="QrCode.jpg" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:block text-align="right">
            For GLB Assure Private Limited
          </fo:block>
          <fo:block text-align="right">
            <fo:external-graphic src="AuthSign.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
          <fo:block text-align="right">
            Authorized Signatory
          </fo:block>

        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <fo:static-content flow-name="page-footer">
        <fo:block text-align="center" font-size="8pt">
			Corporate Address:- Peninsula Business Park, Tower A, 15th Floor, G.K.Marg, Lower Parel, Mumbai - 400 013
        </fo:block>
      </fo:static-content>

        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <xsl:value-of select="GenerateCertificateModel/CreatedDateTimePA" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="left">
                    <fo:external-graphic src="QrCode.jpg" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-align="right">
                    <fo:external-graphic src="TATA_AIG.png" width="3cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt"  >
                  <fo:block text-align="center" font-size="17pt">
                    TATA AIG General Insurance Company Limited
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table table-layout="fixed" width="100%" border-width="1pt"
    border-style="solid" font-size="7pt" >
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header text-align="center">




            </fo:table-header>
            <fo:table-body>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    <!-- Master Policy No : 31150342200100000158 -->
					Master Policy No : <xsl:value-of select="GenerateCertificateModel/MasterPolicyNo" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block>
                    Certificate No : <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Name of Master Policy Holder : GLB ASSURE PRIVATE LIMITED
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Period of Insurance :From: <xsl:value-of select="GenerateCertificateModel/CoverStartDate" /> To:<xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Name of Nominee : <xsl:value-of select="GenerateCertificateModel/NomineeName" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Relationship of nominee with insured : <xsl:value-of select="GenerateCertificateModel/NomineeRelationship" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Nominee Gender : <xsl:value-of select="GenerateCertificateModel/NomineeGender" />



                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">

                    Branch Code : <xsl:value-of select="GenerateCertificateModel/BranchCode" />


                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">


                    Insured Gender : <xsl:value-of select="GenerateCertificateModel/InsuredGender" />



                  </fo:block>
                </fo:table-cell>
                <!--Dynamic Content When InsuredDOB IS TRUE-->
                <xsl:choose>
                  <xsl:when test="($IsInsuredDOB = 'TRUE')">
                    <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                      <fo:block font-weight="bold">
                        Insured DOB / AGE : <xsl:value-of select="GenerateCertificateModel/InsuredDOB" />
                      </fo:block>
                    </fo:table-cell>
                  </xsl:when>
                </xsl:choose>
                <!--Dynamic Content-->

                <!--Dynamic Content When InsuredDOB IS False-->
                <xsl:choose>
                  <xsl:when test="($IsInsuredDOB = 'FALSE')">
                    <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                      <fo:block font-weight="bold">
                        Insured DOB / AGE : <xsl:value-of select="GenerateCertificateModel/InsuredAge" />
                      </fo:block>
                    </fo:table-cell>
                  </xsl:when>
                </xsl:choose>
                <!--Dynamic Content-->
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell padding="1mm" border-width="1pt" border-style="solid">
                  <fo:block font-weight="bold">
                    Name of Insured : <xsl:value-of select="GenerateCertificateModel/InsuredName" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>

          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" >
                    Rs.1,00,000/- (Accidental).
                  </fo:block>


                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Restriction of Coverage
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" padding-top="0pt">
                    Accidental Death:
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    It is strictly restricted to Death due to accident.
                  </fo:block>                  
                  <fo:block text-align="justify"  font-size="6pt">
                    Coverage is applicable to persons with age up to 80 years
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Coverage in Brief
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                   If the Insured person meets with an accident during the policy period which directly and independently of all other causes result in death within 12 months from the date of accident resulting solely and directly from accident then the company shall pay to the insured the sum set in the schedule to the insureds persons nominee, beneficiary or legal representative.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Accidental Death:
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    100% of cumulative Sum Insured 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    (CSI) 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    Please refer to policy for detail information on Policy coverage
                  </fo:block>
                  <!--<fo:block text-align="justify"  font-size="6pt">
                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    
                  </fo:block>-->
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>


          <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Exclusions in Brief
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                    •	War, invasion, act of foreign enemy, hostilities (whether war be declared or not) civil war, rebellion, revolution, insurrection, mutiny military or usurped power, confiscation, seizure, capture, assault, restraint, nationalization, civil commotion or loot or pillage in connection Herewith.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	Ionizing radiation or contamination by radioactivity from any nuclear fuel or from any nuclear waste from the combustion of nuclear fuel. For the purpose of this exclusion, combustion shall include any self sustaining process of nuclear fission.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • The radioactive, toxic, explosive or the hazardous properties of any nuclear assembly or nuclear component or nuclear weapons material.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	Whilst engaging in Aviation or Ballooning whilst mounting into, dismounting from or traveling in any balloon or aircraft
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Participation in any kind of motor speed contest (including trial, training and qualifyingheats)
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	This Insurance does not cover any loss, damage, cost or expense directly or indirectly arising out of - Biological or chemical contamination, Missiles, bombs, grenades, explosives
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Services on duty with any Armed forces
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Intentional self injury, suicide, or attempted suicide
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • venereal diseases, aids or insanity
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Whilst under the influence of Alcohol or intoxicating liquor or drugs.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Medical or surgical treatment
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Whilst committing any breach of law with criminal intent.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Child birth, pregnancy or other physical cause peculiar to the female sex
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    Please refer to policy for detail information on exclusions and other terms and conditions.
                  </fo:block>
                </fo:table-cell>

              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
        <fo:table border-collapse="collapse" width="100%" font-size="7pt">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block text-align="justify"  font-size="7pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Claims Process / Documentation
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt">
                   Upon happening of any accident and/or injury which may give rise to a claim under this policy:
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	It is a condition precedent to Insurers liability hereunder that written notice of claim must be given by You to Insurer within 7 days after an actual or potential loss begins or as soon as reasonably possible and in any event not later than 30 Days after an actual or potential loss begins. 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	On receipt of intimation from you regarding a claim under the policy, insure is entitled to carry out examination and ascertain details.
                      Following documents shall be required in the event of a claim.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	Completed claim forms and written evidence of loss must be furnished to Insurer within thirty (30) Days after the intimation of such loss. Failure to furnish such evidence within the time required shall                                                           
                      not invalidate nor reduce any claim if You can satisfy Insurer that it was not reasonably possible for You to give proof within such time. The Company may accept claims where documents have been 
                      provided after a delayed interval only in special circumstances and for the reasons beyond the control of the insured.                                       
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    Following documents shall be required in the event of a claim.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Claim form
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original Death Certificate
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    • Original/ Attested Post Mortem Report, if conducted
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	Attested copy of FIR, Spot Panchanama and Police Inquest report, where applicable.
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	Complete medical records including Death Summary, in case of hospitalization
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	KYC Documents of insured and nominee / beneficiary
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                    •	Cancel Cheque with NEFT Mandate form - duly filled in by the claimant and bank
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                   •	Any other document as required by the Company to investigate the Claim or Insurers obligation to make payment 
                  </fo:block>
                  <fo:block text-align="justify"  font-size="6pt" >
                   Time of Payment of Claim
                  </fo:block>
                <fo:block text-align="justify"  font-size="6pt" >
                  •	The Company shall settle or reject a claim, as the case may be, within 30 days from the date of receipt of last necessary document.
                  </fo:block>
                </fo:table-cell>

              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:flow>
      </fo:page-sequence>    
      
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>
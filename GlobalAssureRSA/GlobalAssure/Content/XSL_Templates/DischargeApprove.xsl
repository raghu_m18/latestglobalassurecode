﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="GillSans Light">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                      page-height="29.7cm"
                      page-width="21cm"
                      margin-top="1cm"
                      margin-bottom="0cm"
                      margin-left="2.2cm"
                      margin-right="2cm">
          <fo:region-body margin-top="3cm" margin-bottom="3cm"/>
          <fo:region-before region-name="page-header" extent="3cm"/>
          <fo:region-after region-name="page-footer" extent="3cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>

      <xsl:variable name="strSIBenefitHoldCodes" select="DNFApproveData/strSIBenefitHoldCodes"/>
      <xsl:variable name="AnnexureIICount" select="DNFApproveData/AnnexureIICount"/>


      <fo:page-sequence master-reference="simple" font-size="9pt">
        <fo:static-content flow-name="page-header">
          <fo:block text-align="center">
            <fo:external-graphic src="ReligareTopLogo.png"/>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block  text-align="center">
            <fo:external-graphic src="LetterFooter.png"/>
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block text-align="center" font-family="Gill Sans MT" font-weight="bold">
            Authorization Letter
          </fo:block>
          <fo:block padding-top="15pt">
            <fo:table border="none" border-collapse="collapse" width="100%">
              <fo:table-column column-width="395pt"/>
              <fo:table-column column-width="65pt"/>
              <fo:table-column column-width="65pt"/>

              <fo:table-header>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>To,</fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>Date :</fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="DNFApproveData/Date" />
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
              </fo:table-header>
              <fo:table-body>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="DNFApproveData/HospitalName" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block font-family="Gill Sans MT" font-weight="bold">
                      <xsl:value-of select="DNFApproveData/AL_Intimation_Lable" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block font-family="Gill Sans MT" font-weight="bold">
                      <xsl:value-of select="DNFApproveData/ALNumber" />
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="DNFApproveData/AddressLine1" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>Admission Date :</fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="DNFApproveData/AdmissionDate" />
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="DNFApproveData/AddressLine2" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell number-columns-spanned="2">
                    <fo:block>
                      <fo:inline text-decoration="underline">
                        <xsl:value-of select="DNFApproveData/SIBenefit" />
                      </fo:inline>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="DNFApproveData/AddressLine3" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block/>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block/>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="DNFApproveData/AddressLine4" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block/>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block/>
                  </fo:table-cell>
                </fo:table-row>
              </fo:table-body>
            </fo:table>
          </fo:block>

          <fo:block text-align="justify" padding-top="15pt">
            In reference to the Pre-authorization Request submitted by you, we hereby authorize and guarantee payment
            up to Rs. <xsl:value-of select="DNFApproveData/TotalSanctionedAmount" /> The authorization details are as under:
          </fo:block>

          <fo:table layout="fixed" width="100%">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="200pt"/>
            <fo:table-column column-width="125pt"/>
            <fo:table-column column-width="100pt"/>

            <fo:table-header>
              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>Member ID</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black">
                  <fo:block>
                    <xsl:value-of select="DNFApproveData/RHIDNo" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>Claimed Amount</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>
                    <xsl:value-of select="DNFApproveData/ClaimedAmount" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>Class of Accommodation</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>
                    <xsl:value-of select="DNFApproveData/ClassOfAccomodation" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>Additional Sanction</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>
                    <xsl:value-of select="DNFApproveData/AdditionalSanction" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block >Provisional Diagnosis</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>
                    <xsl:value-of select="DNFApproveData/Diagnosis" />
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block >Initial Authorized Limit</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>
                    <xsl:value-of select="DNFApproveData/InitialAuthorizedLimit" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>


              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block ></fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>

                  </fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block >Total Sanctioned Amount</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>
                    <xsl:value-of select="DNFApproveData/TotalSanctionedAmount" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>


            </fo:table-body>
          </fo:table>


          <fo:block padding-top="10pt">Notes to the hospital</fo:block>
          <fo:block padding-top="10pt">

            <fo:list-block >
              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block>
                    <fo:inline>1)</fo:inline>
                  </fo:block>
                </fo:list-item-label>

                <fo:list-item-body start-indent="body-start()">
                  <fo:block>
                    KYC documents i.e. Identity Proof/Address Proof and Latest photo of the proposer to be sent if bill estimate is more than Rs. 1.0 Lakh.
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>

              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block>
                    <fo:inline>2)</fo:inline>
                  </fo:block>
                </fo:list-item-label>

                <fo:list-item-body start-indent="body-start()">
                  <fo:block>
                    If the hospital bill is estimated to be higher than the guarantee of payment, the additional amount would need to be sanctioned by RHICL
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>

              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block>
                    <fo:inline>3)</fo:inline>
                  </fo:block>
                </fo:list-item-label>

                <fo:list-item-body start-indent="body-start()">
                  <fo:block>
                    In absence of such additional guarantee, the hospital must collect the excess amount directly from the insured at the time of admission or prior to discharge.
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>

              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block>
                    <fo:inline>4)</fo:inline>
                  </fo:block>
                </fo:list-item-label>

                <fo:list-item-body start-indent="body-start()">
                  <fo:block>
                    The hospital bill summary and the detailed final bill will have to be authenticated with the insured's signature.This along with the original discharge summary and investigation reports will have to be submitted to the company.
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>

              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block>
                    <fo:inline>5)</fo:inline>
                  </fo:block>
                </fo:list-item-label>

                <fo:list-item-body start-indent="body-start()">
                  <fo:block>
                    Please collect an undertaking from the insured/patient for submitting his/her documents to RHICL in original.
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>

              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block>
                    <fo:inline>6)</fo:inline>
                  </fo:block>
                </fo:list-item-label>

                <fo:list-item-body start-indent="body-start()">
                  <fo:block>
                    Charges for the following miscellaneous services must be collected directly from the patient :
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>

            </fo:list-block>


          </fo:block>


          <fo:table table-layout="fixed" width="100%" padding-top="10pt">
            <fo:table-column column-width="260pt"/>
            <fo:table-column column-width="260pt"/>

            <fo:table-header>
              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>a) Registration charges</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>g) Charges for Tv, Laundry, Telephone, Fax etc</fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-header>
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>b) Attendant / Visitor charges</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>h) Food and Beverage for attendance/visitors</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>c) Ambulance charges unless authorized</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>i) Toiletries</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>d) Nursing charges not authorized</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>j) Medicines not related to treatment</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>e) Service charges </fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>k) Stationary and other charges</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block>f) Charges for extra bed</fo:block>
                </fo:table-cell>
                <fo:table-cell border="solid 0.1mm black" padding="2pt">
                  <fo:block/>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>

          </fo:table>
          <fo:table table-layout="fixed" width="100%" padding-top="10pt">
            <fo:table-column column-width="50pt"/>
            <fo:table-column column-width="470pt"/>

            <fo:table-header>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block>Remarks</fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block text-decoration="underline">
                    <xsl:call-template name="toUpperCase">
                      <xsl:with-param name="str">
                        <xsl:value-of select="DNFApproveData/Remark"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-header>
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block></fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:block padding-top="12pt">
            For Religare Health Insurance Company Ltd
          </fo:block>
          <fo:block padding-top="10pt">
            <fo:external-graphic src="KPSSign.png" width="2cm" scaling="uniform"/>
          </fo:block>
          <fo:block padding-top="10pt">
            Authorized Signatory
          </fo:block>


          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block padding-top="10pt" font-family="Gill Sans MT" font-weight="bold" font-size="6pt">Note:</fo:block>
                  <fo:block font-size="8pt">
                    <fo:list-block >
                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>
                            <fo:inline font-family="Symbol">•</fo:inline>
                          </fo:block>
                        </fo:list-item-label>

                        <fo:list-item-body start-indent="3mm">
                          <fo:block>
                            This authorization is valid for admission within 15 days from the date of issue or expiry / cancellation of the policy whichever is earlier.
                          </fo:block>
                        </fo:list-item-body>
                      </fo:list-item>

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>
                            <fo:inline font-family="Symbol">•</fo:inline>
                          </fo:block>
                        </fo:list-item-label>

                        <fo:list-item-body start-indent="3mm">
                          <fo:block>
                            The authorization will not be valid if the patient is discharged before the date of issue of this letter.
                          </fo:block>
                        </fo:list-item-body>
                      </fo:list-item>

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>
                            <fo:inline font-family="Symbol">•</fo:inline>
                          </fo:block>
                        </fo:list-item-label>

                        <fo:list-item-body start-indent="3mm">
                          <fo:block>
                            Co payment amount will be collected from insured.
                          </fo:block>
                        </fo:list-item-body>
                      </fo:list-item>

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>
                            <fo:inline font-family="Symbol">•</fo:inline>
                          </fo:block>
                        </fo:list-item-label>

                        <fo:list-item-body start-indent="3mm">
                          <fo:block>
                            Claim Settlement will be as per agreed tariff structure between RHICL &amp; the hospital.
                          </fo:block>
                        </fo:list-item-body>
                      </fo:list-item>

                      <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block>
                            <fo:inline font-family="Symbol">•</fo:inline>
                          </fo:block>
                        </fo:list-item-label>

                        <fo:list-item-body start-indent="3mm">
                          <fo:block>
                            This is an initial approval and stands cancel where Misinterpretation of Facts is noticed.
                          </fo:block>
                        </fo:list-item-body>
                      </fo:list-item>

                    </fo:list-block>
                  </fo:block>
                  <fo:block font-size="8pt">
                    All payment to hospital will be subject to deduction of tax at source as per prevailing government rates except where Nil/Low TDS certificates have been provided.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          
          
          <fo:block padding-top="10pt">Please note that hospitalization for Treatment of following conditions is not payable:</fo:block>
          
          <fo:block padding-top="10pt">

            <fo:list-block >

              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block>
                    <fo:inline>i)</fo:inline>
                  </fo:block>
                </fo:list-item-label>

                <fo:list-item-body start-indent="body-start()">
                  <fo:block>
                    Investigation and Evaluation, Genetic conditions, Infertility, STD, Self-inflicted Injury,
                    conditions caused by use of alcohol/tobacco/intoxicating drugs and others conditions as per policy terms.
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>

              <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                  <fo:block>
                    <fo:inline>ii)</fo:inline>
                  </fo:block>
                </fo:list-item-label>

                <fo:list-item-body start-indent="body-start()">
                  <fo:block>
                    Religare Health Insurance Company will not be liable in the event of any discrepancy between the facts presented
                    at the time of admission <![CDATA[&]]> at time of final discharge documentation.
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>

            </fo:list-block>


          </fo:block>

          <fo:block padding-top="15pt" font-size="10pt">

            <fo:table border-collapse="collapse" width="100%">
              <fo:table-column column-width="220pt"/>
              <fo:table-body>
                <fo:table-row keep-together="always">
                  <fo:table-cell>
                    <fo:block >
                      Annexure I
                    </fo:block>

                    <fo:block padding-top="10pt">
                      <fo:table border="solid 1pt" border-collapse="collapse" width="100%" >
                        <fo:table-column column-width="110pt"/>
                        <fo:table-column column-width="110pt"/>
                        <fo:table-column column-width="110pt"/>
                        <fo:table-column column-width="110pt"/>

                        <fo:table-body>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold"></fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">AL No.</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/ALNumber" />
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Final Diagnosis</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/longdescription" />
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Nature of Claim</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/LONGDESC" />
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Policy No.</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/PolicyNo" />
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Name of Patient</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/Patient_Name" />
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Hospital ID</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/HospitalID" />
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Policy Owner Name</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/Policy_Owner_Name" />
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Date of Admission</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/DateOfAdmission_Transactionstr" />
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Date of Discharge</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/DateOfDischarge_Transactionstr" />
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Total Approved Amount</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/TotalALAmount" />
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Claimed Amount</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/RequistedAmount" />
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Current Approved Amount</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/ApprovedAmount_Transaction" />
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Co-pay</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/CoPay" />
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                        

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">
                                <xsl:choose>
                                  <xsl:when test="$strSIBenefitHoldCodes = 'False'">
                                    Non Payable Items *
                                  </xsl:when>
                                </xsl:choose>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:choose>
                                  <xsl:when test="$strSIBenefitHoldCodes = 'False'">
                                    <xsl:value-of select="DNFApproveData/Amount" />
                                  </xsl:when>
                                </xsl:choose>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Hospital Discount (not to be collected from Patient) </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/TotalHospitalDiscount" />
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:choose>
                                  <xsl:when test="$strSIBenefitHoldCodes = 'True'">
                                    <xsl:value-of select="DNFApproveData/SIorBenefitLimitExhausted" />
                                  </xsl:when>
                                </xsl:choose>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:choose>
                                  <xsl:when test="$strSIBenefitHoldCodes = 'True'">
                                    <xsl:value-of select="DNFApproveData/SIBefitValue" />
                                  </xsl:when>
                                </xsl:choose>
                              </fo:block>
                            </fo:table-cell>


                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold"></fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                              </fo:block>
                            </fo:table-cell>

                          </fo:table-row>


                          <xsl:for-each select="DNFApproveData/HMODeductionsDetails/SettlementHMODeductionsDet">
                            <fo:table-row>

                              <fo:table-cell border="solid 1pt">
                                <fo:block text-align="center" font-weight="bold">
                                  <xsl:value-of select="DeductionDetails" />
                                </fo:block>
                              </fo:table-cell >

                              <fo:table-cell border="solid 1pt">
                                <fo:block text-align="center">
                                  <xsl:call-template name="toUpperCase">
                                    <xsl:with-param name="str">
                                      <xsl:value-of select="DeductedAmount"/>
                                    </xsl:with-param>
                                  </xsl:call-template>
                                </fo:block>
                              </fo:table-cell>

                            </fo:table-row>
                          </xsl:for-each>

                          <fo:table-row>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold">Total Deductions</fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center">
                                <xsl:value-of select="DNFApproveData/TotalDeductions" />
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center" font-weight="bold"></fo:block>
                            </fo:table-cell>
                            <fo:table-cell border="solid 1pt">
                              <fo:block text-align="center"></fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
              </fo:table-body>
            </fo:table>

            <xsl:choose>
              <xsl:when test="($AnnexureIICount = 'True' and  $strSIBenefitHoldCodes = 'False')">
                <fo:table border-collapse="collapse" width="100%">
                  <fo:table-column column-width="220pt"/>
                  <fo:table-body>
                    <fo:table-row keep-together="always">
                      <fo:table-cell>
                        <fo:block padding-top="30pt">
                          * Annexure II
                        </fo:block>
                        <fo:block padding-top="10pt">
                          <fo:table border="solid 1pt" border-collapse="collapse" width="100%">
                            <fo:table-column column-width="110pt"/>
                            <fo:table-column column-width="110pt"/>
                            <fo:table-column column-width="110pt"/>
                            <fo:table-column column-width="110pt"/>
                            <fo:table-header>
                              <fo:table-row>
                                <fo:table-cell border="solid 1pt">
                                  <fo:block text-align="center">Particulars</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="solid 1pt">
                                  <fo:block text-align="center">Reason for Deduction</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="solid 1pt">
                                  <fo:block text-align="center">Remarks</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="solid 1pt">
                                  <fo:block text-align="center">Amount</fo:block>
                                </fo:table-cell>
                              </fo:table-row>
                            </fo:table-header>

                            <fo:table-body>
                              <xsl:for-each select="DNFApproveData/AnnexureII/SettlementHospitalBillReport">
                                <fo:table-row>
                                  <fo:table-cell border="solid 1pt">
                                    <fo:block text-align="center">
                                      <xsl:value-of select="LevelOne" />
                                    </fo:block>
                                  </fo:table-cell>
                                  <fo:table-cell border="solid 1pt">
                                    <fo:block text-align="center">
                                      <xsl:value-of select="Reason" />
                                    </fo:block>
                                  </fo:table-cell >
                                  <fo:table-cell border="solid 1pt">
                                    <fo:block text-align="center">
                                      <xsl:call-template name="toUpperCase">
                                        <xsl:with-param name="str">
                                          <xsl:value-of select="Remarks"/>
                                        </xsl:with-param>
                                      </xsl:call-template>
                                    </fo:block>
                                  </fo:table-cell>
                                  <fo:table-cell border="solid 1pt">
                                    <fo:block text-align="center">
                                      <xsl:value-of select="Amount" />
                                    </fo:block>
                                  </fo:table-cell>
                                </fo:table-row>
                              </xsl:for-each>
                            </fo:table-body>
                          </fo:table>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                  </fo:table-body>
                </fo:table>

              </xsl:when>
            </xsl:choose>

          </fo:block>
        
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>
</xsl:stylesheet>
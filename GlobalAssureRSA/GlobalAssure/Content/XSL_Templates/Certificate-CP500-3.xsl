﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Franklin Gothic Book">

      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple"
                    page-height="29.7cm"
                    page-width="21cm"
                    margin-top="0.5cm"
                    margin-bottom="0.5cm"
                    margin-left="1cm"
                    margin-right="1cm">
          <fo:region-body margin-top="1.5cm" />
          <fo:region-before region-name="page-header" extent="4cm"/>
          <fo:region-after region-name="page-footer" extent="2cm"/>
        </fo:simple-page-master>
      </fo:layout-master-set>


      <xsl:variable name="IsPlanFeature" select="GenerateCertificateModel/IsPlanFeature"/>
      <xsl:variable name="IsStateUP" select="GenerateCertificateModel/IsStateUP"/>

      <fo:page-sequence master-reference="simple" font-size="7pt">
        <fo:static-content flow-name="page-header" >
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="3in"/>
            <fo:table-column column-width="3in"/>
            <fo:table-header>
            </fo:table-header>

            <fo:table-body>
              <fo:table-row>
                <fo:table-cell padding="1mm">
                  <fo:block text-align="left">
                    <fo:external-graphic src="GA_Logo_Large.jpg" width="3.5cm" height="1cm"/>
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="1mm">
                  <fo:block text-align="right">
                    <fo:external-graphic src="Finask.jpg" width="3.5cm" height="1cm"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <!--<fo:block text-align="center">
            <fo:external-graphic src="QrCode.jpg" width="1.2cm"/>
          </fo:block>-->
          
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="250pt"/>
                      <fo:table-column column-width="117pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Registered Address</fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">
                              Corporate Address
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="260pt"/>
                      <fo:table-column column-width="260pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">F-206, SATYA SHANTI APARTMENT, SECTOR-13, ROHINI, DELHI, North West Delhi, Delhi, 110085 </fo:block>
                          </fo:table-cell>

                          <fo:table-cell>
                            <fo:block text-align="right">

                              3rd Floor, AIHP Horizon, 445, Phase 5, Udyog Vihar, Gurgaon -122016
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="0pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>

                      <fo:table-body>
                        <fo:table-row keep-together="always" >
                          <fo:table-cell>
                            <!--<fo:block text-align="left" space-after="7pt">
                              GSTNO.-06AAHCG5630Q1Z6
                            </fo:block>-->
                            <fo:block text-align="left" space-after="7pt">
                              GSTNO :- <xsl:value-of select="GenerateCertificateModel/GstNo" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right" space-after="7pt">

                              CIN.-U93000DL2018PTC342117
                            </fo:block>
                          </fo:table-cell>

                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center"  padding-bottom="0pt">
                    In case of any queries/assistance,please call us on 0124 4092900 / 9643123731 or write to us on support@globalassure.com

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">Tax Invoice</fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="right">
                              Name of the Service Provider : <fo:inline font-family="Franklin Gothic Demi" font-weight="bold"> Global Assure</fo:inline>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="justify"  padding-bottom="5pt" padding-top="0pt">

                    This is to certify that Vehicle with the following details is covered under Assistance Program, as per the details defined under benefits, terms and condition of the program.
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        CERTIFICATE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" >
                                      <fo:block>
                                        Certificte Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CertificateNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Plan Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Plan" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate Start Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverStartDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Certificate End Date
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/CoverEndDate" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Place Of Supply
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/PlaceOfSupply" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        SAC Code
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/SACCode" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <!--<fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row keep-together="always">
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>
                                        VEHICLE
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="1mm">
                                      <fo:block>

                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Vehicle Registration Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/RegistrationNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Manufacturer
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Make" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Model
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Model" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Variant
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Variant" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Engine Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/EngineNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Chassis Number
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ChassisNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>-->
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PERSONAL DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="black" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#FFFFFF" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="240pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="10pt"/>
                      <fo:table-column column-width="20pt"/>
                      <fo:table-column column-width="240pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        First Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/FirstName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Last Name
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/LastName" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Mobile No
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/ContactNo" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <!--<fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Email ID
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Email" />
                                      </fo:block>
                                    </fo:table-cell>-->
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="center">

                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell>
                            <fo:block text-align="left">

                              <fo:table table-layout="fixed" width="240pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                <fo:table-column column-width="1in"/>
                                <fo:table-column column-width="1in"/>

                                <fo:table-header text-align="center">
                                </fo:table-header>

                                <fo:table-body>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block>
                                        Address1
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address1" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address2
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address2" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        Address3
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/Address3" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        State
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/State" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                  <fo:table-row>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                      <fo:block font-weight="bold">
                                        City
                                      </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                      <fo:block>
                                        <xsl:value-of select="GenerateCertificateModel/City" />
                                      </fo:block>
                                    </fo:table-cell>
                                  </fo:table-row>

                                </fo:table-body>

                              </fo:table>

                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!--<fo:table border-collapse="collapse" width="100%" padding-top="5pt">
            <fo:table-column/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell color="white" font-family="Franklin Gothic Demi" font-weight="bold" background-color = "#32419c" padding="2pt">
                  <fo:block>
                    <fo:table border-collapse="collapse"  width="100%">
                      <fo:table-column column-width="265pt"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell>
                            <fo:block text-align="left">PAYMENT DETAILS</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->

          <!--Dynamic Content When State IS NOT UP-->
          <!--<xsl:choose>
            <xsl:when test="($IsStateUP = 'FALSE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amount Of Tax IGST (18%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/IGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>-->
          <!--Dynamic Content-->

          <!--Dynamic Content When State IS UP-->
          <!--<xsl:choose>
            <xsl:when test="($IsStateUP = 'TRUE')">
              <fo:table border-collapse="collapse" width="100%" padding-top="5pt">
                <fo:table-column/>
                <fo:table-body>
                  <fo:table-row keep-together="always">
                    <fo:table-cell color="black" font-family="Franklin Gothic Demi" background-color = "#FFFFFF" padding="2pt">
                      <fo:block>
                        <fo:table border-collapse="collapse"  width="100%">
                          <fo:table-column column-width="530pt"/>

                          <fo:table-body>
                            <fo:table-row keep-together="always">
                              <fo:table-cell>
                                <fo:block text-align="left">

                                  <fo:table table-layout="fixed" width="530pt" border-width="0.1px" border-style="solid" font-size="6pt" >
                                    <fo:table-column column-width="1in"/>
                                    <fo:table-column column-width="1in"/>

                                    <fo:table-header text-align="center">
                                    </fo:table-header>

                                    <fo:table-body>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Plan Amount
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/Amount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of CGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/CGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row keep-together="always">
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block >
                                            Amout Of SGST (9%)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/SGST" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            GST No. of the Service Recipient
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/GSTNoOfTheServiceRecipient" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Total Amount (Including Tax)
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/TotalAmount" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                      <fo:table-row>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid">
                                          <fo:block font-weight="bold">
                                            Amount In Words
                                          </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell padding="1mm" border-width="0.1px" border-style="solid" font-weight="lighter">
                                          <fo:block>
                                            <xsl:value-of select="GenerateCertificateModel/AmountInWords" />
                                          </fo:block>
                                        </fo:table-cell>
                                      </fo:table-row>

                                    </fo:table-body>

                                  </fo:table>

                                </fo:block>
                              </fo:table-cell>
                            </fo:table-row>
                          </fo:table-body>
                        </fo:table>

                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </xsl:when>
          </xsl:choose>-->
          <!--Dynamic Content-->


          <!--<fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="2pt">
                  <fo:block padding-top="5pt" font-size="5pt" padding-bottom="2pt" font-family="Franklin Gothic Demi" font-weight="bold">
                    <fo:inline color="#FF0000">* Conditions Apply</fo:inline>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>-->

          <!--Plan Features-->
          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell padding="5pt">
                  <fo:block text-align="justify"  font-size="10pt" font-family="Franklin Gothic Demi" font-weight="bold" color="white" background-color = "#32419c">
                    Plan Features
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
          <fo:table layout="fixed" width="100%" font-size="6pt" padding-top="0pt">
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-column column-width="100pt"/>
            <fo:table-column column-width="150pt"/>
            <fo:table-header ></fo:table-header>
            <fo:table-body>



              <fo:table-row background-color="#D9D9D9">
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Serial Number</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block font-family="Franklin Gothic Demi" font-weight="bold">Plan Features</fo:block>
                </fo:table-cell>

                <fo:table-cell padding="2pt">
                  <fo:block  font-family="Franklin Gothic Demi" font-weight="bold">CP500-3</fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block> 1</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Lost Card liability including assistance for blocking of card
                  </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    YES
                  </fo:block>
                </fo:table-cell>

              </fo:table-row>
              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block> 2 </fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Emergency Hotel and Travel Assistance
                  </fo:block>
                </fo:table-cell>

                <fo:table-cell padding="2pt">
                  <fo:block> YES </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row >     

              </fo:table-row>
              
              <fo:table-row >
                <fo:table-cell padding="2pt">
                  <fo:block>3</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Emergency Cash Assistance
                  </fo:block>
                </fo:table-cell>

                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row background-color="#eaeae1">
                <fo:table-cell padding="2pt">
                  <fo:block>4</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Assistance for blocking SIM Card
                  </fo:block>
                </fo:table-cell>

                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell padding="2pt">
                  <fo:block>5</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="2pt">
                  <fo:block>
                    Pan Card Replacement
                  </fo:block>
                </fo:table-cell>

                <fo:table-cell padding="2pt">
                  <fo:block>YES</fo:block>
                </fo:table-cell>
              </fo:table-row>       
              
            </fo:table-body>

          </fo:table>        
         
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="15pt">
            FAQs
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="5pt">
            Why should I buy a Global Assure Card Protection membership?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            When you realise that your wallet is missing, it's a race against time to have all your cards stopped immediately. With GLOBAL ASSURE Card Protection, all it takes is one free call to our customer service team and they will contact your card issuers to have your cards cancelled within minute. So no matter where you are in the world, we will take care of it for you.
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            What does my membership cover?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            A summary of the benefits you are entitled to is as follows:
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	One free call to block all your cards
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	24-hour toll-free helpline
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Emergency travel and cash assistance
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Complimentary Fraud protection that ensures coverage from fraud that may take place on your cards due to loss/theft, skimming, counterfeiting, online usage, phishing and PIN based fraud
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Valuable document registration
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Lost card replacement assistance
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Lost PAN card replacement
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Assistance for blocking SIM Card
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            How can I purchase a Card Protection membership?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            You can purchase a Card Protection membership online or buy calling us on 0124-4092900
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            What is a welcome pack? By when will I receive it?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            The welcome pack outlines all the details of your membership and contains the following:
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	A confirmation letter outlining your membership details.
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Terms and conditions of the membership.
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	A membership guide listing all the benefits you are entitled to.
          </fo:block>         
        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="simple" font-size="7pt" >
        <fo:static-content flow-name="page-header" >
          <fo:block text-align="center">
            <fo:external-graphic src="GA_Logo_Large.jpg" width="3.2cm" height="1cm"/>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="page-footer">
          <fo:block text-align="center">
            <!--<fo:external-graphic src="LetterFooter.jpg" width="25cm" scaling="uniform"/>-->
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            How long is my membership active?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            Your membership is active for one year from the membership set-up date
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            How do I renew my membership?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            At the end of the membership period (which is 1 year), we automatically renew your membership by collecting payment from the payment method advised by you. We send you a reminder 45 days prior to your renewal date advising you that your membership is due for renewal
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            Can I cancel my membership anytime?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            Yes, you can cancel your membership anytime during your membership period by writing to us or calling us on our 24-hour helpline number. However, you are eligible for a full refund if you cancel within 7 days of membership set-up date and no claims have been made by you
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            If I lose my cards, how do I report the loss?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            As soon as you have discovered the loss of cards, please call our 24-hour helpline on 0124-4092900. We will then contact your card issuers immediately to cancel your cards
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            If I am abroad and I find that my wallet, travel tickets and passport have been stolen, what should I do?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            When you discover that these items are missing, immediately call our 24-hour helpline on 0124-4092900. We will immediately cancel your lost cards by contacting your card issuers. We will then advise you how and where to receive a replacement passport. Finally, we will assist in the re-issuing of travel tickets so that you may return home.
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            How can I pay for my hotel expenses?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            You should inform us that you would like assistance with the hotel payment. We will then liaise with the hotel and advance the expenses to pay the hotel directly.
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            How do I make a claim?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            If you need to make a claim, please call us on our 24-hour helpline numbers. We will send you a claim form which needs to be completed and returned to us with the following original documents for processing:
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Credit Card or Bank account statement
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	Subrogation Bond
          </fo:block>
          <fo:block text-align="left"  padding-top="2pt">
            •	FIR copy
          </fo:block>
          <fo:block text-align="left" font-weight="bold" font-family="Franklin Gothic Demi" padding-top="3pt">
            Is there any time period limitation for submitting a claim?
          </fo:block>
          <fo:block text-align="left"  padding-top="3pt">
            All claims must be received within 30 days from the date of reporting card loss. Please remember to include all the documents that we ask for, including original receipts (not photocopies) for any expenses claimed for. If you do not, we will not be able to process your claim until we have received them.
          </fo:block>

          <fo:table border-collapse="collapse" width="100%">
            <fo:table-column column-width="220pt"/>
            <fo:table-body>
              <fo:table-row keep-together="always">
                <fo:table-cell>
                  <fo:block text-align="center">
                    <fo:external-graphic src="QrCode.jpg" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <fo:block text-align="right">
            For GLB Assure Private Limited
          </fo:block>
          <fo:block text-align="right">
            <fo:external-graphic src="AuthSign.jpg" width="3.2cm" height="1cm"/>
          </fo:block>

          <fo:block text-align="right">
            Authorized Signatory
          </fo:block>

        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="toUpperCase">
    <xsl:param name="str" />
    <xsl:variable name="lowerCaseAlphabet">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name="upperCaseAlphabet">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:value-of select="translate($str,$lowerCaseAlphabet,$upperCaseAlphabet)"/>
  </xsl:template>

  <xsl:attribute-set name="myBorder">
    <xsl:attribute name="border-top">solid 0.2mm #32419c</xsl:attribute>
    <xsl:attribute name="border-bottom">solid 0.2mm #32419c</xsl:attribute>
  </xsl:attribute-set>

</xsl:stylesheet>